package org.binqua.forex.testclient.runner

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, DeathPactException, PostStop}
import akka.http.scaladsl.model.{HttpMethods, Uri}
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.Config.onlyAfterValidation
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.httpclient.HttpClient.{Response, Submit}
import org.binqua.forex.testclient.json.writes._
import org.binqua.forex.testclient.runner.TestRunner.Message
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete
import org.binqua.forex.testclient.utils.NewRequestIdForTesting.next1
import org.binqua.forex.testclient.utils.{ForTestingRequestIdFactory, referenceData}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.NewChildMaker.{ChildNamePrefix, ChildrenFactory}
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util.SmartMatcher.errorLog
import org.binqua.forex.util._
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.{JsValue, Json}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._

class TestRunnerSpec
    extends ScalaTestWithActorTestKit(ConfigFactory.parseString(""" akka{
      |  loglevel = "INFO"
      |  actor.testkit.typed.filter-leeway = 2s
      |} """.stripMargin))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val exceptionThatCrashedTester = new RuntimeException("I am Test actor and I am going to crash!!!")

  val exceptionThatCrashedHttpClient = new RuntimeException("I am HttpClient and I am going to crash!!!")

  "Given a cluster is formed, the actor" should "create the tester and send start" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.thatIgnoreAnyMessage(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed()
      )
      .whileRunning {

        testKit.spawn(behaviours.default(), randomActorName)

        fishExactlyOneMessageAndFailOnOthers(httpClientContext.probe, 1.seconds)((theBeMatched: Any) => {

          val HttpClient.Submit(id, request, maybeABody, replyTo) = theBeMatched
          id should be(next1(randomActorName))
          request.uri should be(Uri(referenceData.httpManagement.value))
          request.method should be(HttpMethods.GET)
          maybeABody should be(None)

          replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))

        })
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
  }

  "Given a cluster is formed, the actor" should "keep checking but create only one test actor" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectNoMessage()
  }

  "Given a cluster stopped to be formed, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext
      .dependingOnMessageCounterReturns(
        numberOfMessages => numberOfMessages == 3,
        replyTo => replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.down))
      ),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(
          supportMessages.clusterFormationFailed(List("member AkkaClusterUrl(akka://forex-cluster@feedReader:2551) is not Up and any replica is not Up"))
        ),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given a cluster stopped to be return cluster data, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext
      .dependingOnMessageCounterReturns(numberOfMessages => numberOfMessages == 3, replyTo => replyTo ! HttpClient.Done(Json.parse("""{ "a" : 1 }"""))),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(supportMessages.clusterDataParserFailed(Json.parse("""{ "a" : 1 }"""))),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given a cluster stopped to be return json data, the actor" should "stop the test and start again" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.dependingOnMessageCounterReturns(
      numberOfMessages => numberOfMessages == 3,
      replyTo => replyTo ! HttpClient.Failed(HttpClient.WrongStatusCode("dont care", 404, "body crap"))
    ),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        supportMessages.contactingHttpManagementServer(),
        supportMessages.clusterFormed(),
        supportMessages.retryingSoon(),
        errorLog.equal(supportMessages.clusterFormationPreconditionFailed(HttpClient.WrongStatusCode("dont care", 404, "body crap"))),
        supportMessages.retryingSoon(),
        supportMessages.clusterFormed()
      )
      .whileRunning {
        testKit.spawn(behaviours.default(), randomActorName)
      }

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.Stop)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given child Test actor crash, the actor" should "restart itself" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatCrashAfterStartTestWith(exceptionThatCrashedTester)
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        errorLog.equal(supportMessages.childCrashed(createThenDeleteContext.childNamePrefix.nameOfChild(instanceCounter = 1), exceptionThatCrashedTester)),
        errorLog.supervisorRestart(exceptionThatCrashedTester),
        errorLog.equal(supportMessages.childCrashed(createThenDeleteContext.childNamePrefix.nameOfChild(instanceCounter = 2), exceptionThatCrashedTester)),
        errorLog.supervisorRestart(exceptionThatCrashedTester)
      )
      .whileRunning {

        testKit.spawn(behaviours.default(), randomActorName)

        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
        createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

      }

  }

  "Given child Test actor stopped, the actor" should "restart it and keep sending start" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatKeepStopping()
  ) {

    testKit.spawn(behaviours.default(), randomActorName)

    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)
    createThenDeleteContext.probe.expectMessage(CreateThenDelete.StartTest)

  }

  "Given the http child actor crash, the actor" should "restart itself" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.thatCrashWith(exceptionThatCrashedHttpClient),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    PreciseLoggingTestKit
      .expectAtLeast(
        errorLog.equal(supportMessages.childCrashed(httpClientContext.childNamePrefix.nameOfChild(instanceCounter = 1), exceptionThatCrashedHttpClient)),
        errorLog.supervisorRestart(exceptionThatCrashedHttpClient),
        errorLog.equal(supportMessages.childCrashed(httpClientContext.childNamePrefix.nameOfChild(instanceCounter = 2), exceptionThatCrashedHttpClient)),
        errorLog.supervisorRestart(exceptionThatCrashedHttpClient)
      )
      .whileRunning {
        testKit.spawn(behaviours.default())
      }

  }

  "The actor" should "watch for http client termination and ignore it" in new Context(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    HttpClientContext.returnsClusterFormedAndStopItself(),
    CreateThenDeleteContext.thatIgnoreAnyMessage()
  ) {

    val newActorConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      referenceData.httpManagement,
      feedReaderUrl = referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 10.millis,
      referenceData.members.web.up.node
    )

    LoggingTestKit
      .error[DeathPactException]
      .withOccurrences(newOccurrences = 0)
      .expect {

        testKit.spawn(behaviours.default(newActorConfig))

        waitFor(actorConfig.retryClusterFormationInterval * 20, soThat("there is time to see a DeathPactException if it happens"))
      }
  }

  object HttpClientContext {

    type theType = NewActorCollaboratorTestContext[Message, HttpClient.Message]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = "HttpClient"

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("HttpClient"))

      override val probe: TestProbe[HttpClient.Message] = createTestProbe()

      override def behavior: Behavior[HttpClient.Message]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, HttpClient.Message] =
        throw new IllegalAccessException("Not used in this case")

      override val childrenFactoryContext: NewChildMaker.ChildrenFactory[Message, HttpClient.Message] =
        ChildrenFactory(childMakerFactory()(), childNamePrefix)

      override def childMakerFactory(): CHILD_MAKER_FACTORY[Message, HttpClient.Message] =
        smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

      override def ref: ActorRef[HttpClient.Message] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[HttpClient.Message] = Behaviors.ignore
      }
    }

    def returnsClusterFormedAndStopItself(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[HttpClient.Message] =
          Behaviors
            .receiveMessage[HttpClient.Message]({
              case Submit(uniqueId, httpRequest, maybeABody, replyTo) =>
                replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))
                Behaviors.stopped
              case _ => ???
            })
            .receiveSignal({
              case (context, PostStop) =>
                context.log.info(s"Stopped ${context.self.path.name}")
                Behaviors.same
            })
      }
    }

    def dependingOnMessageCounterReturns(messageMatcher: Int => Boolean, f: ActorRef[Response] => Unit): SmartDeadWatcher => theType = { smartDeadWatcher =>
      val messagesCounter = new AtomicInteger(0)
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[HttpClient.Message] =
          Behaviors
            .receiveMessage[HttpClient.Message](msg => {
              messagesCounter.set(messagesCounter.get() + 1)
              msg match {
                case Submit(_, _, _, replyTo) =>
                  if (messageMatcher(messagesCounter.get()))
                    f(replyTo)
                  else
                    replyTo ! HttpClient.Done(Json.toJson(referenceData.cluster.up))
                  Behaviors.same
                case _ => ???
              }
            })
      }
    }

    def thatCrashWith(throwable: Throwable): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[HttpClient.Message] =
          Behaviors.receiveMessage({
            case Submit(uniqueId, httpRequest, maybeABody, replyTo) =>
              throw throwable
              Behaviors.same
            case _ => ???
          })

      }
    }

  }

  object CreateThenDeleteContext {

    type theType = NewActorCollaboratorTestContext[Message, CreateThenDelete.Message]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PortfoliosChecker"))

      override val name: String = childNamePrefix.prefix.value

      override val probe: TestProbe[CreateThenDelete.Message] = createTestProbe()

      override val childrenFactoryContext: NewChildMaker.ChildrenFactory[Message, CreateThenDelete.Message] =
        ChildrenFactory(childMakerFactory()(), childNamePrefix)

      override def behavior: Behavior[CreateThenDelete.Message]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, CreateThenDelete.Message] =
        throw new IllegalAccessException("Not used in this case")

      override def childMakerFactory(): CHILD_MAKER_FACTORY[Message, CreateThenDelete.Message] =
        smartDeadWatcher.createAChildMakerX(childMakerNaming(childNamePrefix.prefix.value), probe.ref, behavior)

      override def ref: ActorRef[CreateThenDelete.Message] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    def thatCanBeStopped(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage({
            case CreateThenDelete.StartTest => Behaviors.same
            case CreateThenDelete.Stop      => Behaviors.stopped
            case _                          => ???
          })
      }
    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CreateThenDelete.Message] = Behaviors.ignore
      }
    }

    def thatCrashAfterStartTestWith(throwable: Throwable): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage({
            case CreateThenDelete.StartTest => throw throwable
            case CreateThenDelete.Stop      => Behaviors.stopped
            case _                          => ???
          })
      }
    }

    def thatKeepStopping(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CreateThenDelete.Message] =
          Behaviors.receiveMessage(_ => Behaviors.stopped)
      }
    }

  }

  object SupportMessagesContext {
    val justForAnIdeaSupportMessage = new SupportMessages {
      override def clusterFormationPreconditionFailed(failureReason: HttpClient.FailureReason): String = s"clusterFormationPreconditionFailed $failureReason"

      override def clusterFormed(): String = "clusterFormed"

      override def clusterDataParserFailed(body: JsValue): String = s"clusterDataParserFailed $body"

      override def contactingHttpManagementServer(): String = s"contactingHttpManagementServer"

      override def retryingSoon(): String = s"going to retry soon"

      override def clusterFormationFailed(errors: List[String]): String = s"clusterFormationFailed $errors"

      override def childCrashed(actorName: String, throwable: Throwable): String = s"childCrashed $actorName $throwable"
    }
  }

  case class Context(
      supportMessages: SupportMessages,
      httpClientContextMaker: SmartDeadWatcher => HttpClientContext.theType,
      createThenDeleteTestContextMaker: SmartDeadWatcher => CreateThenDeleteContext.theType
  )(implicit
      underTestNaming: UnderTestNaming
  ) extends BaseTestContext(underTestNaming) {

    val actorConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      referenceData.httpManagement,
      feedReaderUrl = referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 100.millis,
      referenceData.members.web.up.node
    )

    val randomActorName = s"actor-${Gen.uuid.sample.get.toString}"

    val requestId = s"$randomActorName-1"

    val httpClientContext: HttpClientContext.theType = httpClientContextMaker(actorsWatchers)

    val createThenDeleteContext: CreateThenDeleteContext.theType = createThenDeleteTestContextMaker(actorsWatchers)

    object behaviours {

      def default(configToBeUsed: Config): Behavior[Message] =
        TestRunner(_ => configToBeUsed, supportMessagesFactory = _ => supportMessages)(
          httpClientContext.childrenFactoryContext,
          createThenDeleteContext.childrenFactoryContext,
          ForTestingRequestIdFactory
        )

      def default(): Behavior[Message] = default(actorConfig)

    }

  }

}
