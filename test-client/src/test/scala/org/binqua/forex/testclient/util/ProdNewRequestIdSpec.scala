package org.binqua.forex.testclient.util

import org.scalacheck.Gen
import org.scalatest.matchers.should.Matchers._
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks.forAll

import scala.util.Random

class ProdNewRequestIdSpec extends AnyPropSpecLike {

  property("ProdRequestId should be stateless") {
    forAll(Gen.long) { aLong =>
      val requestId = DefaultRequestIdFactory(new Random(aLong)).of("test")

      requestId.id should be(requestId.id)
      requestId.id should be(requestId.id)

      requestId.newRequestId.id should be(requestId.newRequestId.id)
      requestId.newRequestId.id should be(requestId.newRequestId.id)
    }
  }

  property("id should start with the originator follow by a long number") {
    forAll(Gen.long) { aLong =>
      val requestId = DefaultRequestIdFactory(new Random(aLong)).of("test")
      requestId.id should (fullyMatch regex "test--?\\d.*")
    }
  }

}
