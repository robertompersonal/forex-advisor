package org.binqua.forex.testclient

import cats.Eq
import cats.data.Validated.{Invalid, Valid}
import cats.implicits.catsSyntaxOptionId
import eu.timepit.refined.predicates.all.Url
import eu.timepit.refined.refineV
import org.binqua.forex.testclient.Config.onlyAfterValidation
import org.binqua.forex.testclient.Member.{memberDown, memberUp}
import org.binqua.forex.testclient.utils.referenceData
import org.binqua.forex.util.TestingFacilities.because
import org.binqua.forex.util.core._
import org.binqua.forex.util.{TestingFacilities, Validation}
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import play.api.libs.json.{JsSuccess, JsValue, Json}

import scala.concurrent.duration._

class ClusterSpec extends AnyFlatSpecLike with Matchers with Validation with TestingFacilities with ScalaCheckPropertyChecks with GivenWhenThen {

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration = PropertyCheckConfiguration(minSuccessful = 40)

  "given a node is up in the cluster it" should "have no problems" in {

    val aNodeUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2553").unsafe

    val clusterUnderTest = Cluster("leader", Members(Seq(memberUp(node = aNodeUrl))))

    clusterUnderTest.problemOf(aNodeUrl) should be(None)

  }

  "given a node is not Up and any replica is not Up in the cluster it" should "have problems" in {

    val aNodeUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2553").unsafe

    val clusterUnderTest = Cluster("leader", Members(Seq(memberDown(node = aNodeUrl))))

    clusterUnderTest.problemOf(aNodeUrl) should be(Some(s"member $aNodeUrl is not Up and any replica is not Up"))

  }

  "given a node is not present at all in the cluster it" should "have problems" in {

    val aNodeUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2553").unsafe

    val clusterUnderTest = Cluster("leader", Members(Seq(memberUp(node = AkkaClusterUrl.from("akka://forex-cluster@anotherNode:2554").unsafe))))

    clusterUnderTest.problemOf(aNodeUrl) should be(Some(s"member $aNodeUrl is not Up and any replica is not Up"))

  }

  "given a node has 2 replicas and one is up in the cluster it" should "have no problems" in {

    val aNodeUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2553").unsafe
    val aNodeReplicaUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2554").unsafe

    val clusterUnderTest = Cluster(
      "leader",
      Members(
        Seq(
          memberUp(node = aNodeUrl),
          memberDown(node = aNodeReplicaUrl)
        )
      )
    )

    clusterUnderTest.problemOf(aNodeReplicaUrl) should be(None)
    clusterUnderTest.problemOf(aNodeUrl) should be(None)

  }

  "given a node has 2 replicas and both are down in the cluster it" should "have problems" in {

    val aNodeUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2553").unsafe
    val aNodeReplicaUrl = AkkaClusterUrl.from("akka://forex-cluster@web:2554").unsafe

    val clusterUnderTest = Cluster(
      "leader",
      Members(
        Seq(
          memberDown(node = aNodeUrl),
          memberDown(node = aNodeReplicaUrl)
        )
      )
    )

    clusterUnderTest.problemOf(aNodeReplicaUrl) should be(Some(s"member $aNodeReplicaUrl is not Up and any replica is not Up"))
    clusterUnderTest.problemOf(aNodeUrl) should be(Some(s"member $aNodeUrl is not Up and any replica is not Up"))

  }

  "given all config members are up, then cluster" should "be formed" in {

    val someConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      refineV[Url](s"http://localhost:1234/cluster/members").unsafe,
      referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 2.seconds,
      referenceData.members.web.up.node
    )

    Cluster.isFormed(
      Cluster(
        "someLeader",
        Members(
          Seq(
            referenceData.members.feedReader.up,
            referenceData.members.web.up,
            referenceData.members.clientToExternalSystem1.up,
            referenceData.members.clientToExternalSystem2.up
          )
        )
      ),
      someConfig
    ) match {
      case Valid(actualValue) => actualValue should be(true)
      case Invalid(errors)    => thisTestShouldNotHaveArrivedHere(because(s"cluster should be formed but we got errors $errors"))
    }

  }

  "given no replica and at least one member is down or missing, then cluster" should "be not formed" in {

    val someConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      refineV[Url](s"http://localhost:1234/cluster/members").unsafe,
      referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 2.seconds,
      referenceData.members.web.up.node
    )

    forAll(maybeDamagedTwice(Seq(referenceData.members.feedReader.up, referenceData.members.web.up, referenceData.members.clientToExternalSystem1.up)))(
      damagedMembersWithoutReplica => {
        Cluster.isFormed(Cluster("someLeader", damagedMembersWithoutReplica), someConfig) match {
          case Valid(_)        => thisTestShouldNotHaveArrivedHere(because(s"cluster should be not formed with members $damagedMembersWithoutReplica"))
          case Invalid(errors) => //test passed
        }

      }
    )

  }

  "given only 1 replica is down or missing, then cluster" should "be formed" in {

    val someConfig: Config = onlyAfterValidation.newConfig(
      clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
      clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
      referenceData.httpManagement,
      referenceData.members.feedReader.up.node,
      retryClusterFormationInterval = 2.seconds,
      referenceData.members.web.up.node
    )

    forAll(damagedSomehow(Seq(referenceData.members.clientToExternalSystem1.up, referenceData.members.clientToExternalSystem2.up)))(damagedOneReplica => {
      val validMembers = Members(damagedOneReplica.members :++ Seq(referenceData.members.feedReader.up, referenceData.members.web.up))
      Cluster.isFormed(Cluster("someLeader", validMembers), someConfig) match {
        case Valid(_)   => // test passed
        case Invalid(_) => thisTestShouldNotHaveArrivedHere(because(s"cluster should be formed with members $validMembers"))
      }

    })

  }

  "cluster" should "be serialised and deserialised" in {

    Given(s"A cluster to be serialised ${referenceData.cluster.up}")

    val cluster: JsValue = Json.toJson(referenceData.cluster.up)(json.writes.clusterWrites)

    When(s"It is serialised gives ${Json.prettyPrint(cluster)}")

    When(s"It is deserialised gives back ${referenceData.cluster.up}")
    Json.parse(Json.prettyPrint(cluster)).validate[Cluster](json.reads.cluster) should be(JsSuccess(referenceData.cluster.up))

  }

  def damagedSomehow(originalAllUpMembers: Seq[Member]): Gen[Members] = {
    def damaged(member: Member, typeOfDamage: Int): Option[Member] =
      typeOfDamage match {
        case 1 => member.copy(status = "Down").some
        case 2 => None
      }

    for {
      typeOfDamage <- Gen.oneOf(1, 2)
      indexToBeDamaged <- Gen.choose(0, originalAllUpMembers.size - 1)
      damaged <- Gen.const(damaged(originalAllUpMembers(indexToBeDamaged), typeOfDamage))
      damagedMembers <- Gen.const({
        val tempMembers: Seq[Member] = originalAllUpMembers.zipWithIndex.filter(p => Eq.neqv(p._2, indexToBeDamaged)).map(_._1)
        damaged match {
          case None               => tempMembers
          case Some(damageMember) => tempMembers :+ damageMember
        }
      })
    } yield Members(damagedMembers)
  }

  def maybeDamagedTwice(originalAllUpMembers: Seq[Member]): Gen[Members] = {
    for {
      members <- damagedSomehow(originalAllUpMembers)
      maybeDamageTwice <- Gen.oneOf(damagedSomehow(members.members), Gen.const(members))
    } yield maybeDamageTwice
  }
}
