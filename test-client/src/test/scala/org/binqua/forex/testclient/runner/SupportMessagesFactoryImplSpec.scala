package org.binqua.forex.testclient.runner

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.Config.onlyAfterValidation
import org.binqua.forex.testclient.httpclient.HttpClient.PreconditionException
import org.binqua.forex.testclient.utils.referenceData
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import play.api.libs.json.Json

import scala.concurrent.duration._

class SupportMessagesFactoryImplSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val actorConfig: Config = onlyAfterValidation.newConfig(
    clientsToExternalSystems1 = referenceData.members.clientToExternalSystem1.up.node,
    clientsToExternalSystems2 = referenceData.members.clientToExternalSystem2.up.node,
    referenceData.httpManagement,
    feedReaderUrl = referenceData.members.feedReader.up.node,
    retryClusterFormationInterval = 200.millis,
    referenceData.members.web.up.node
  )

  private val underTest: SupportMessages = SupportMessagesFactoryImpl.newSupportMessages(actorConfig)

  "clusterFormed" should "be clusterFormed" in {
    underTest.clusterFormed() should be("clusterFormed")
  }

  "childCrashed" should "work" in {
    underTest.childCrashed("pippo", new RuntimeException("boom")) should be(
      "Child actor pippo crashed. Throwable was java.lang.RuntimeException: boom"
    )
  }

  "clusterFormationFailed" should "work" in {
    underTest.clusterFormationFailed(List("error1", "error2")) should be(
      """Cluster formation failed. Details:
        |error1
        |error2""".stripMargin
    )
  }

  "clusterDataParserFailed" should "work" in {
    underTest.clusterDataParserFailed(Json.parse("""{"a": 1}""")) should be(
      """ClusterDataParserFailed formation failed. Body {
        |  "a" : 1
        |} is not a valid cluster response""".stripMargin
    )
  }

  "clusterFormationPreconditionFailed" should "work" in {
    underTest
      .clusterFormationPreconditionFailed(PreconditionException("123", new RuntimeException("boom"))) should be(
      "ClusterFormationPreconditionFailed. Reason: PreconditionException(123,java.lang.RuntimeException: boom)"
    )
  }

  "contactingHttpManagementServer" should "work" in {
    underTest
      .contactingHttpManagementServer() should be(
      s"Contacting http management server at ${actorConfig.clusterManagementUrl}"
    )
  }

  "retryingSoon" should "work" in {
    underTest
      .retryingSoon() should be(
      s"Going to retry to contact http management server in ${actorConfig.retryClusterFormationInterval}."
    )
  }

}
