package org.binqua.forex.testclient

import cats.data.Validated
import cats.data.Validated.Invalid
import eu.timepit.refined.refineV
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.binqua.forex.util.core.MakeEitherUnsafe
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration.DurationInt

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  val prefixWithA: String => String = toBePrefixed => s"a.$toBePrefixed"

  object keys {
    val clientsToExternalSystems1 = prefixWithA("clientsToExternalSystems1")
    val clientsToExternalSystems2 = prefixWithA("clientsToExternalSystems2")
    val clusterManagementUrl = prefixWithA("clusterManagementUrl")
    val retryClusterFormationInterval = prefixWithA("retryClusterFormationInterval")
    val feedReaderUrl = prefixWithA("feedReaderUrl")
    val webUrl = prefixWithA("webUrl")
  }

  private val validRows: Set[String] = Set(
    s"""${keys.clientsToExternalSystems1} = "akka://clusterName@a:2550" """,
    s"""${keys.clientsToExternalSystems2} = "akka://clusterName@a1:2551" """,
    s"""${keys.clusterManagementUrl} = "http://localhost:123/members/replica" """,
    s"""${keys.retryClusterFormationInterval} = 2s """,
    s"""${keys.feedReaderUrl} = "akka://clusterName@b:2552" """,
    s"""${keys.webUrl} = "akka://clusterName@c:2553" """
  )

  "Given all properties values are valid, we" should "get a valid config" in {

    val validConfig = toAkkaConfig(validRows)

    Config.ConfigValidatorBuilderImpl(prefixWithA)(validConfig) match {
      case Validated.Valid(actualConfig) =>
        actualConfig.clientsToExternalSystems1 should be(AkkaClusterUrl.from("akka://clusterName@a:2550").unsafe)
        actualConfig.clientsToExternalSystems2 should be(AkkaClusterUrl.from("akka://clusterName@a1:2551").unsafe)
        actualConfig.clusterManagementUrl should be(refineV[eu.timepit.refined.string.Url]("http://localhost:123/members/replica").unsafe)
        actualConfig.retryClusterFormationInterval should be(2.seconds)
        actualConfig.feedReaderUrl should be(AkkaClusterUrl.from("akka://clusterName@b:2552").unsafe)
        actualConfig.webUrl should be(AkkaClusterUrl.from("akka://clusterName@c:2553").unsafe)
      case Invalid(e) => thisTestShouldNotHaveArrivedHere(because(s"$validConfig should be valid but we got $e"))
    }
  }

  "An akka replica" should "not have more than 1 digit" in {
    val actualErrors: Seq[String] = Config
      .ConfigValidatorBuilderImpl(prefixWithA)(
        toAkkaConfig(
          validRows.map((row: String) =>
            row
              .replace("a:2550", "a12:2550")
              .replace("c:2553", "c123:2553")
          )
        )
      )
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualErrors should contain(toAkkaUrlErrorMessage("a.clientsToExternalSystems1", "akka://clusterName@a12:2550"))
    actualErrors should contain(toAkkaUrlErrorMessage("a.webUrl", "akka://clusterName@c123:2553"))
  }

  "Given config values are all wrong, we" should "get valid errors" in {
    val actualErrors: Seq[String] = Config
      .ConfigValidatorBuilderImpl(prefixWithA)(
        toAkkaConfig(
          validRows.map((row: String) =>
            row
              .replace("2s", "blabla")
              .replace("akka", "test")
              .replace("http", "")
          )
        )
      )
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualErrors.head should be("Configuration for Cluster Members Checker is invalid:")

    actualErrors.tail should contain(toAkkaUrlErrorMessage("a.clientsToExternalSystems1", "test://clusterName@a:2550"))
    actualErrors.tail should contain(toAkkaUrlErrorMessage("a.clientsToExternalSystems2", "test://clusterName@a1:2551"))
    actualErrors.tail should contain(toAkkaUrlErrorMessage("a.feedReaderUrl", "test://clusterName@b:2552"))
    actualErrors.tail should contain(toAkkaUrlErrorMessage("a.webUrl", "test://clusterName@c:2553"))
    actualErrors.tail should contain("a.clusterManagementUrl has to be an url. ://localhost:123/members/replica is not valid.")
    actualErrors.tail should contain("key a.retryClusterFormationInterval does not have a valid duration value")

    actualErrors.size shouldBe 7
  }

  private def toAkkaUrlErrorMessage(key: String, wrongValue: String) = {
    s"key $key is wrong. Details: An akka cluster url has to be akka://clusterName@nodeName(\\d?):port. $wrongValue is not valid."
  }
}
