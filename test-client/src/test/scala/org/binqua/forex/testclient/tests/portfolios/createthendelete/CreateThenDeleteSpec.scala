package org.binqua.forex.testclient.tests.portfolios.createthendelete

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import cats.data.Writer
import com.typesafe.config.ConfigFactory
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.{PortfoliosModel, StateGen}
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion._
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete.ChildrenMaker
import org.binqua.forex.testclient.{ParserProblems, Precondition}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util.core.{MakeItUnsafe, UnsafeConfigReader}
import org.binqua.forex.util.{AkkaTestingFacilities, PreciseLoggingTestKit, SmartMatcher, Validation}
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import org.slf4j.event.Level
import play.api.libs.json.JsError

import scala.concurrent.duration._

class CreateThenDeleteSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.parseString("""akka { loglevel = "INFO" } """)))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "given a portfolios has been created, the test" should "complete when deletion phase receive a response" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).unsafe

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = testKit.spawn(behaviours.aTester(theActorConfig))

    {
      val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
      val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

      val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

      actorUnderTest ! CreateThenDelete.StartTest

      fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
        val PortfoliosCreator.Start(replyTo) = toBeMatched
        replyTo ! portfolioCreated
      }

      portfolioDeletionTestContext.probe.expectNoMessage()

      manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

      val aNonEmptyPortfolioSummary = StateGen.nonEmptyPortfoliosSummary.sample.get

      PreciseLoggingTestKit
        .expectAtLeast(
          supportMessages
            .testComplete(Writer(TestFailed(aNonEmptyPortfolioSummary.size), TestReport(completed = 1, aborted = 0, success = 0, failure = 1)))
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
            actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
            replyTo ! PortfoliosDeletion.Succeeded(aNonEmptyPortfolioSummary)
          }
        )
    }

    manualTime.timePasses(theActorConfig.betweenTestsInterval.plus(1.millis))

    {
      val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
      val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

      val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

      fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis)(toBeMatched => {
        val PortfoliosCreator.Start(replyTo) = toBeMatched
        replyTo ! portfolioCreated
      })

      portfolioDeletionTestContext.probe.expectNoMessage()

      manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

      val aNonEmptyPortfolioSummary = StateGen.nonEmptyPortfoliosSummary.sample.get

      PreciseLoggingTestKit
        .expectAtLeast(
          supportMessages
            .testComplete(Writer(TestFailed(aNonEmptyPortfolioSummary.size), TestReport(completed = 2, aborted = 0, success = 0, failure = 2)))
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
            actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
            replyTo ! PortfoliosDeletion.Succeeded(aNonEmptyPortfolioSummary)
          }
        )
    }

  }

  "given a CreationFailed, the test" should "fail due to its precondition and retrying" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).unsafe

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = testKit.spawn(behaviours.aTester(theActorConfig))

    {
      val creationFailed = PortfoliosCreator.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problem"))))

      actorUnderTest ! CreateThenDelete.StartTest

      PreciseLoggingTestKit
        .expectAtLeast(
          (
            Level.ERROR,
            SmartMatcher
              .Equal(
                supportMessages.testPreconditionFailed(TestReport(completed = 1, aborted = 1, success = 0, failure = 0), preconditionFailed = creationFailed)
              )
          )
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosCreator.Start(replyTo) = toBeMatched
            replyTo ! creationFailed
          }
        )
    }

    PreciseLoggingTestKit
      .expectAtLeast(supportMessages.runningTest(TestReport(completed = 1, aborted = 1, success = 0, failure = 0)))
      .whileRunning(manualTime.timePasses(theActorConfig.retryInterval.plus(1.millis)))

    {
      val creationFailed = PortfoliosCreator.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problem"))))

      PreciseLoggingTestKit
        .expectAtLeast(
          (
            Level.ERROR,
            SmartMatcher
              .Equal(
                supportMessages.testPreconditionFailed(TestReport(completed = 2, aborted = 2, success = 0, failure = 0), preconditionFailed = creationFailed)
              )
          )
        )
        .whileRunning(
          fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
            val PortfoliosCreator.Start(replyTo) = toBeMatched
            replyTo ! creationFailed
          }
        )
    }

    portfolioDeletionTestContext.probe.expectNoMessage()

  }

  "given a portfolios has been created, but a PortfoliosDeletion.DeletionFailed the test" should "fail due to its precondition and retrying" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {
    portfoliosDeletionProblemTest(problems.deletion.failed)
  }

  "given a portfolios has been created but a deletion failed, the test" should "abort and start again" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {
    portfoliosDeletionProblemTest(problems.deletion.exception)
  }

  "A Stop message after the actor, create a portfolio" should "terminate all its children and terminate itself" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage,
    PortfoliosCreatorTestContext.thatIgnoreAnyMessage(),
    PortfoliosDeletionTestContext.thatIgnoreAnyMessage()
  ) {

    val theActorConfig: Config =
      Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).unsafe

    val actorUnderTest: ActorRef[CreateThenDelete.Message] = deadWatcher.watch(testKit.spawn(behaviours.aTester(theActorConfig)))

    val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
    val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

    actorUnderTest ! CreateThenDelete.StartTest

    fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
      val PortfoliosCreator.Start(replyTo) = toBeMatched
      replyTo ! portfolioCreated
    }

    PreciseLoggingTestKit
      .expectAtLeast(supportMessages.testStopped(TestReport(completed = 0, aborted = 0, success = 0, failure = 0)))
      .whileRunning(actorUnderTest ! CreateThenDelete.Stop)

    portfolioCreationTestContext.probe.expectTerminated(portfolioCreationTestContext.lastCreatedChild())

    deadWatcher.assertThatIsDead(actorUnderTest, 500.millis)

  }

  object PortfoliosCreatorTestContext {

    type theType = NewActorCollaboratorTestContext[CreateThenDelete.Message, PortfoliosCreator.Message]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = "PortfoliosCreator"

      override val probe: TestProbe[PortfoliosCreator.Message] = createTestProbe()

      override def behavior: Behavior[PortfoliosCreator.Message]

      override def childMakerFactory(
          actorsWatchers: SmartDeadWatcher
      ): CHILD_MAKER_FACTORY[CreateThenDelete.Message, PortfoliosCreator.Message] =
        throw new IllegalAccessException("Not used in this case")

      override def childMakerFactory(): CHILD_MAKER_FACTORY[CreateThenDelete.Message, PortfoliosCreator.Message] =
        smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

      override def ref: ActorRef[PortfoliosCreator.Message] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[PortfoliosCreator.Message] = Behaviors.ignore
      }
    }

  }

  object SupportMessagesContext {
    val justForAnIdeaSupportMessage = new SupportMessages {

      override def testComplete(testComplete: Writer[TestResult, TestReport]): String = s"test complete $testComplete"

      override def runningTest(testReport: TestReport): String = s"running test $testReport"

      override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosDeletion.Failed): String =
        s"testPreconditionFailed $testReport $preconditionFailed"

      override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosCreator.Failed): String =
        s"testPreconditionFailed $testReport $preconditionFailed"

      override def testStopped(testReport: TestReport): String = s"testStopped $testReport"
    }
  }

  object PortfoliosDeletionTestContext {

    type theType = NewActorCollaboratorTestContext[CreateThenDelete.Message, PortfoliosDeletion.Message]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = "PortfoliosDeletion"

      override val probe: TestProbe[PortfoliosDeletion.Message] = createTestProbe()

      override def behavior: Behavior[PortfoliosDeletion.Message]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[CreateThenDelete.Message, PortfoliosDeletion.Message] =
        throw new IllegalAccessException("Not used in this case")

      override def childMakerFactory(): CHILD_MAKER_FACTORY[CreateThenDelete.Message, PortfoliosDeletion.Message] =
        smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

      override def ref: ActorRef[PortfoliosDeletion.Message] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[PortfoliosDeletion.Message] = Behaviors.ignore
      }
    }

  }

  case class TestContext(
      supportMessages: SupportMessages,
      portfoliosCreationContextMaker: SmartDeadWatcher => PortfoliosCreatorTestContext.theType,
      portfoliosDeletionContextMaker: SmartDeadWatcher => PortfoliosDeletionTestContext.theType
  )(implicit underTestNaming: UnderTestNaming)
      extends ManualTimeBaseTestContext(underTestNaming) {

    val aRequestId: String = Gen.choose(1, Long.MaxValue).sample.get.toString

    val portfolioCreationTestContext: PortfoliosCreatorTestContext.theType = portfoliosCreationContextMaker(actorsWatchers)

    val portfolioDeletionTestContext: PortfoliosDeletionTestContext.theType = portfoliosDeletionContextMaker(actorsWatchers)

    def hardCodedConfigurationReaderTester(configToBeReturned: Config): ConfigurationReaderTester[Config] = {
      new ConfigurationReaderTester[Config] {
        override val configurationReader: UnsafeConfigReader[Config] = akkaConfig => {
          configToBeReturned
        }

        override def assertThatConfigurationHasBeenRead(): Boolean = ???
      }
    }

    object behaviours {

      def aTester(configToBeUsed: Config): Behavior[CreateThenDelete.Message] = {
        CreateThenDelete(hardCodedConfigurationReaderTester(configToBeUsed).configurationReader)(
          supportMessages,
          ChildrenMaker(portfolioCreationTestContext.childMakerFactory()(), portfolioDeletionTestContext.childMakerFactory()())
        )
      }
    }

    object problems {
      object deletion {
        val failed: Failed = PortfoliosDeletion.Failed(ParserProblems(JsError("parser problems")))
        val exception: Failed = PortfoliosDeletion.Failed(Precondition(HttpClient.PreconditionException(aRequestId, new RuntimeException("some problems"))))
      }
    }

    def portfoliosDeletionProblemTest(portfolioDeletionFailure: PortfoliosDeletion.Failed): Unit = {

      val theActorConfig: Config =
        Config.validated(betweenCreationAndDeletionInterval = 1.second, intervalBetweenTests = 2.second, retryInterval = 10.seconds).unsafe

      testKit.spawn(behaviours.aTester(theActorConfig)) ! CreateThenDelete.StartTest

      {
        val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
        val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

        fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
          val PortfoliosCreator.Start(replyTo) = toBeMatched
          replyTo ! portfolioCreated
        }

        portfolioDeletionTestContext.probe.expectNoMessage()

        manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

        PreciseLoggingTestKit
          .expectAtLeast(
            (
              Level.ERROR,
              SmartMatcher.Equal(
                supportMessages
                  .testPreconditionFailed(TestReport(completed = 1, aborted = 1, success = 0, failure = 0), preconditionFailed = portfolioDeletionFailure)
              )
            )
          )
          .whileRunning {
            fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
              val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
              actualPortfoliosToBeDeleted should be(portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName))
              replyTo ! portfolioDeletionFailure
            }
          }
      }

      manualTime.timePasses(theActorConfig.retryInterval.plus(1.millis))

      {
        val thePortfolioSummaryExpected: PortfoliosSummary = StateGen.nonEmptyPortfoliosSummary.sample.get
        val portfolioCreated = PortfoliosCreator.Succeeded(thePortfolioSummaryExpected)

        val expPortfoliosToBeDeleted: Set[PortfoliosModel.PortfolioName] = portfolioCreated.portfolioSummary.portfolios.map(_.portfolioName)

        fishExactlyOneMessageAndFailOnOthers(portfolioCreationTestContext.probe, 500.millis) { toBeMatched =>
          val PortfoliosCreator.Start(replyTo) = toBeMatched
          replyTo ! portfolioCreated
        }

        portfolioDeletionTestContext.probe.expectNoMessage()

        manualTime.timePasses(theActorConfig.betweenCreationAndDeletionInterval.plus(1.millis))

        PreciseLoggingTestKit
          .expectAtLeast(
            (
              Level.ERROR,
              SmartMatcher.Equal(
                supportMessages
                  .testPreconditionFailed(TestReport(completed = 2, aborted = 2, success = 0, failure = 0), preconditionFailed = portfolioDeletionFailure)
              )
            )
          )
          .whileRunning(
            fishExactlyOneMessageAndFailOnOthers(portfolioDeletionTestContext.probe, 500.millis) { toBeMatched =>
              val PortfoliosDeletion.Delete(actualPortfoliosToBeDeleted, replyTo) = toBeMatched
              actualPortfoliosToBeDeleted should be(expPortfoliosToBeDeleted)
              replyTo ! portfolioDeletionFailure
            }
          )
      }
    }

  }
}
