package org.binqua.forex.testclient.utils

import org.binqua.forex.testclient.util.{RequestId, RequestIdFactory}
import org.binqua.forex.testclient.utils.NewRequestIdForTesting.toNextId

case class NewRequestIdForTesting(originator: String, value: Long) extends RequestId {
  override def id: String = toNextId(originator, value)

  def newRequestId: RequestId = NewRequestIdForTesting(originator, value + 1)
}

object NewRequestIdForTesting {
  def toNextId(originator: String, value: Long): String = s"$originator-$value"

  def next1(originator: String): String = toNextId(originator, 1)
}

object ForTestingRequestIdFactory extends RequestIdFactory {
  override def of(originator: String): RequestId = NewRequestIdForTesting(originator, 1)
}
