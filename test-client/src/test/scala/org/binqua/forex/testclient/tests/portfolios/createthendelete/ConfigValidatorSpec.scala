package org.binqua.forex.testclient.tests.portfolios.createthendelete

import com.typesafe.config
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers.{be, contain, _}

import scala.concurrent.duration._

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  val defaultPrefixer: String => String = toBePrefixed => s"a.$toBePrefixed"

  object keys {

    val betweenCreationAndDeletionInterval = defaultPrefixer("betweenCreationAndDeletionInterval")
    val intervalBetweenTests = defaultPrefixer("intervalBetweenTests")
    val retryInterval = defaultPrefixer("retryInterval")
  }

  private val validRows: Set[String] = Set(
    s"${keys.betweenCreationAndDeletionInterval}  = 1s",
    s"${keys.intervalBetweenTests}  = 2s",
    s"${keys.retryInterval} = 3s"
  )

  "Given all properties values are valid, we" should "get a valid config" in {
    val actualConfig = Config.ConfigValidatorBuilderImpl(defaultPrefixer)(toAkkaConfig(validRows)).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.betweenCreationAndDeletionInterval should be(1.second)
    actualConfig.betweenTestsInterval should be(2.seconds)
    actualConfig.retryInterval should be(3.seconds)
  }

  "Given config values are all wrong, we" should "get valid errors" in {
    val wrongConfig: config.Config = toAkkaConfig(validRows.map(_.replaceAll(""" = \ds""", " = wrongValue")))

    val actualErrors: Seq[String] = Config
      .ConfigValidatorBuilderImpl(defaultPrefixer)(wrongConfig)
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere(because(s"config $wrongConfig should be wrong but result to be correct!")))

    actualErrors.head should be("Configuration for Test Client is invalid:")

    actualErrors.tail should contain(s"key ${keys.betweenCreationAndDeletionInterval} does not have a valid duration value")
    actualErrors.tail should contain(s"key ${keys.intervalBetweenTests} does not have a valid duration value")
    actualErrors.tail should contain(s"key ${keys.retryInterval} does not have a valid duration value")

    actualErrors.size shouldBe 4
  }
}
