package org.binqua.forex.testclient

import org.binqua.forex.testclient.utils.referenceData
import org.binqua.forex.util.TestingFacilities.because
import org.binqua.forex.util.core.MakeEitherUnsafe
import org.binqua.forex.util.{TestingFacilities, Validation}
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class MemberSpec extends AnyFlatSpecLike with Matchers with Validation with TestingFacilities {

  "akka://forex-cluster@web:2553" should "be valid" in {

    val validAkkaUrl = "akka://forex-cluster@web:2553"

    AkkaClusterUrl.from(validAkkaUrl) match {
      case Right(_)    =>
      case Left(error) => thisTestShouldNotHaveArrivedHere(because(s"$validAkkaUrl should be valid: $error"))
    }

  }

  "akka://forex-cluster@web-1:12345" should "be valid" in {
    val validAkkaUrl = "akka://forex-cluster@web-1:12345"
    AkkaClusterUrl.from(validAkkaUrl) match {
      case Right(_)    =>
      case Left(error) => thisTestShouldNotHaveArrivedHere(because(s"$validAkkaUrl should be valid: $error"))
    }
  }

  "http://forex-cluster@web-1:12345" should "be valid" in {
    val invalidAkkaUrl = "http://forex-cluster@web-1:12345"
    AkkaClusterUrl.from(invalidAkkaUrl) match {
      case Right(_) => thisTestShouldNotHaveArrivedHere(because(s"$invalidAkkaUrl should be invalid"))
      case Left(_)  =>
    }
  }

  "http://localhost:12345" should "be valid" in {
    val invalidAkkaUrl = "http://localhost:12345"
    AkkaClusterUrl.from(invalidAkkaUrl) match {
      case Right(_) => thisTestShouldNotHaveArrivedHere(because(s"$invalidAkkaUrl should be invalid"))
      case Left(_)  =>
    }
  }

  "given a member with status up, it" should "be up" in {

    Member(referenceData.members.web.up.node, "up").isUp should be(true)
    Member(referenceData.members.web.up.node, "uP").isUp should be(true)

    Member(referenceData.members.web.up.node, "down").isUp should be(false)
    Member(referenceData.members.web.up.node, "Down").isUp should be(false)

  }

  "to be è a replica of it" should "have only the port different and a number at the end of the node name" in {

    Member(referenceData.members.clientToExternalSystem1.up.node, "Up").isReplicaOf(referenceData.members.clientToExternalSystem2.up.node) should be(true)
    Member(AkkaClusterUrl.from("akka://name@tag:2552").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@tag1:2553").unsafe) should be(true)
    Member(AkkaClusterUrl.from("akka://name@tag1:2552").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@tag:2553").unsafe) should be(true)
    Member(AkkaClusterUrl.from("akka://name@tag1:2552").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@tag3:2553").unsafe) should be(true)
    Member(AkkaClusterUrl.from("akka://name@tag:10001").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@tag:10003").unsafe) should be(true)

    Member(AkkaClusterUrl.from("akka://name@pippo:10001").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@pluto:10003").unsafe) should be(false)
    Member(AkkaClusterUrl.from("akka://name@pippo1:10001").unsafe, "Up").isReplicaOf(AkkaClusterUrl.from("akka://name@pluto2:10003").unsafe) should be(false)
    referenceData.members.web.up.node.isReplicaOf(referenceData.members.feedReader.up.node) should be(false)
    referenceData.members.clientToExternalSystem1.up.node.isReplicaOf(referenceData.members.web.up.node) should be(false)

  }

}
