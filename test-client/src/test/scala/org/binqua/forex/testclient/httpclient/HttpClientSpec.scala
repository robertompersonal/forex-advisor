package org.binqua.forex.testclient.httpclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.actor.{ActorPath, ActorSystem => ClassicActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import cats.implicits.catsSyntaxOptionId
import com.typesafe.config.ConfigFactory
import org.binqua.forex.testclient.httpclient.HttpClient._
import org.binqua.forex.testclient.utils.{InstructableHttpServer, ServerMockingInstructions}
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util._
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import play.api.libs.json.{Json => PlayJson}
import spray.json._

import java.net.InetSocketAddress
import scala.concurrent.{ExecutionContext, Future}

class HttpClientSpec
    extends ScalaTestWithActorTestKit(ConfigFactory.parseString(""" akka{ loglevel = "INFO" } """))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  def withHttpServer(testCode: (ServerMockingInstructions => Unit, Int) => Any): Any = {
    val server = new InstructableHttpServer()
    val serverStarted: (Http.ServerBinding, ActorSystem[Nothing], ServerMockingInstructions => Unit, InetSocketAddress) = server.start()
    try {
      testCode(serverStarted._3, serverStarted._4.getPort)
    } finally {
      server.stop(serverStarted._1, serverStarted._2, serverStarted._4)
    }
  }

  "Once submitted the request, the actor" should "wait for the json response, send it back, be stopped and release all http connections" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    withHttpServer((instructTheHttpServer, serverPort) => {

      val theRequestBody: String = """{ "request":1 }"""

      val theHttpRequestToBeExecuted = HttpRequest(
        method = HttpMethods.POST,
        uri = s"http://localhost:$serverPort/someUrl",
        entity = HttpEntity(ContentTypes.`application/json`, theRequestBody),
        headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
      )

      val theResponseBody = """ {"response":1} """

      instructTheHttpServer(
        ServerMockingInstructions(
          pathPart = "someUrl",
          requestBody = theRequestBody.parseJson,
          responseBodyToReturn = theResponseBody,
          statusCodeToReturn = 200
        )
      )

      val actorUnderTest = testKit.spawn(behaviours.aHttpClient(ProdHttpBodyExtractor))

      import PreciseLoggingTestKit._

      PreciseLoggingTestKit
        .expectAtLeast(
          supportMessages.submittingRequest(aRequestId, httpRequest = theHttpRequestToBeExecuted, replyTo = responseProbe.ref, body = theJsonRequestBody.some),
          supportMessages.stoppedAndReleasedHttpConnections(actorUnderTest.path)
        )
        .whileRunning {

          actorUnderTest ! Submit(aRequestId, theHttpRequestToBeExecuted, PlayJson.parse(theRequestBody).some, responseProbe.ref)

          createTestProbe().expectTerminated(actorUnderTest)
        }

      responseProbe.expectMessage(Done(PlayJson.parse(theResponseBody)))

    })

  }

  "the actor" should "be stoppable and release all the http connections event before starting" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    val actorUnderTest = testKit.spawn(behaviours.aHttpClient(ProdHttpBodyExtractor))

    import PreciseLoggingTestKit._

    PreciseLoggingTestKit
      .expectAtLeast(supportMessages.stoppedAndReleasedHttpConnections(actorUnderTest.path))
      .whileRunning {
        testKit.stop(actorUnderTest)
        createTestProbe().expectTerminated(actorUnderTest)
      }
  }

  "Given a success http response status code with a wrong body, the actor" should "notify the problem and be stopped" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    withHttpServer((instructTheHttpServerWithThisBehavior, serverPort) => {

      val theRequestBody: String = """{ "request":1 }"""

      val aNonJsonBodyResponse: String = "crappy body"

      val theHttpRequest = HttpRequest(
        method = HttpMethods.POST,
        uri = s"http://localhost:$serverPort/someUrl",
        entity = HttpEntity(ContentTypes.`application/json`, PlayJson.prettyPrint(theJsonRequestBody)),
        headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
      )

      instructTheHttpServerWithThisBehavior(ServerMockingInstructions("someUrl", theRequestBody.parseJson, aNonJsonBodyResponse, 200))

      val actorUnderTest = testKit.spawn(behaviours.aHttpClient(ProdHttpBodyExtractor))

      PreciseLoggingTestKit
        .expectAtLeast(supportMessages.receivedResponse(aRequestId, responseProbe.ref, StatusCodes.OK, aNonJsonBodyResponse))
        .whileRunning {
          actorUnderTest ! Submit(aRequestId, theHttpRequest, theJsonRequestBody.some, responseProbe.ref)
        }

      val response: Response = responseProbe.receiveMessage()

      response should be(a[Failed])
      val actualCreationFailedResponse: Failed = response.asInstanceOf[Failed]
      actualCreationFailedResponse.failureReason should be(a[BodyParserException])

      val actualBodyParserException = actualCreationFailedResponse.failureReason.asInstanceOf[BodyParserException]
      actualBodyParserException.statusCode should be(200)
      actualBodyParserException.throwable.getMessage should include(aNonJsonBodyResponse)
      actualBodyParserException.responseBody should be(aNonJsonBodyResponse)

      createTestProbe().expectTerminated(actorUnderTest)
    })

  }

  "Given a non success http response status code, the actor" should "notify the problem and be stopped" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    withHttpServer((instructTheHttpServerWithThisBehavior, serverPort) => {

      val theHttpRequest = HttpRequest(
        method = HttpMethods.POST,
        uri = s"http://localhost:$serverPort/thisPathDoesNotExist",
        entity = HttpEntity(ContentTypes.`application/json`, PlayJson.prettyPrint(theJsonRequestBody)),
        headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
      )

      val actorUnderTest = testKit.spawn(behaviours.aHttpClient(ProdHttpBodyExtractor))

      actorUnderTest ! Submit(aRequestId, theHttpRequest, theJsonRequestBody.some, responseProbe.ref)

      responseProbe.expectMessage(Failed(WrongStatusCode(aRequestId, 404, "The requested resource could not be found.")))

      createTestProbe().expectTerminated(actorUnderTest)
    })
  }

  "Given some problem contacting http server, actor" should "notify the problem and be stopped" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    val theHttpRequest = HttpRequest(
      method = HttpMethods.POST,
      uri = "thisWillCreateAnException",
      entity = HttpEntity(ContentTypes.`application/json`, PlayJson.prettyPrint(theJsonRequestBody)),
      headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
    )

    val actorUnderTest = testKit.spawn(behaviours.aHttpClient(ProdHttpBodyExtractor))

    actorUnderTest ! Submit(aRequestId, theHttpRequest, theJsonRequestBody.some, responseProbe.ref)

    responseProbe.expectMessage(Failed(PreconditionException(aRequestId, illegalUriException("thisWillCreateAnException"))))

    createTestProbe().expectTerminated(actorUnderTest)

  }

  "Given HttpBodyReceptionFailed, the actor" should "notify the problem and be stopped" in new TestContext(
    SupportMessagesContext.justForAnIdeaSupportMessage
  ) {

    withHttpServer((instructTheHttpServerWithThisBehavior, serverPort) => {

      val extractingHttpBodyProblemsMessage = "extracting body problems"

      val theRequestBody: String = """{ "request":1 }"""

      val theResponseBody = """ {"response":1} """

      val theHttpRequest = HttpRequest(
        method = HttpMethods.POST,
        uri = s"http://localhost:$serverPort/someUrl",
        entity = HttpEntity(ContentTypes.`application/json`, theRequestBody),
        headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
      )

      instructTheHttpServerWithThisBehavior(ServerMockingInstructions("someUrl", theRequestBody.parseJson, theResponseBody, 203))

      val actorUnderTest = testKit.spawn(behaviours.aHttpClient(new HttpBodyExtractor {
        override def eventuallyABody(entity: HttpEntity)(implicit actorSystem: ClassicActorSystem, executionContext: ExecutionContext): Future[String] =
          Future.failed(new RuntimeException(extractingHttpBodyProblemsMessage))
      }))

      actorUnderTest ! Submit("123", theHttpRequest, theJsonRequestBody.some, responseProbe.ref)

      val response: Response = responseProbe.receiveMessage()

      response should be(a[Failed])

      val actualCreationFailedResponse: Failed = response.asInstanceOf[Failed]
      actualCreationFailedResponse.failureReason should be(a[BodyReceptionException])

      val actualBodyReceptionException = actualCreationFailedResponse.failureReason.asInstanceOf[BodyReceptionException]
      actualBodyReceptionException.statusCode should be(203)
      actualBodyReceptionException.throwable.getMessage should be(extractingHttpBodyProblemsMessage)

      createTestProbe().expectTerminated(actorUnderTest)
    })
  }

  object SupportMessagesContext {
    val justForAnIdeaSupportMessage = new SupportMessages {
      override def stoppedAndReleasedHttpConnections(actorPath: ActorPath): String = s"stoppedAndReleasedHttpConnections $actorPath"

      override def receivedResponse(uniqueId: String, replyTo: ActorRef[Response], statusCode: StatusCode, body: String): String =
        s"receivedResponseBody $uniqueId $statusCode $body"

      override def submittingRequest(
          uniqueId: String,
          httpRequest: HttpRequest,
          replyTo: ActorRef[Response],
          body: Option[play.api.libs.json.JsValue]
      ): String =
        s"submittingRequest $uniqueId $httpRequest $replyTo $body"
    }
  }

  case class TestContext(supportMessages: SupportMessages)(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(underTestNaming) {

    val aRequestId = Gen.choose(1L, Long.MaxValue).sample.get.toString

    val theJsonRequestBody: play.api.libs.json.JsValue = play.api.libs.json.Json.parse(""" { "request" : 1 } """)

    val responseProbe = createTestProbe[Response]()

    object behaviours {

      def aHttpClient(httpBodyExtractor: HttpBodyExtractor): Behavior[HttpClient.Message] =
        HttpClient(supportMessages, httpBodyExtractor)

    }

    def illegalUriException(details: String) =
      new IllegalUriException(
        new ErrorInfo(s"Cannot determine request scheme and target endpoint as HttpMethod(POST) request to $details doesn't have an absolute URI")
      )

  }

}
