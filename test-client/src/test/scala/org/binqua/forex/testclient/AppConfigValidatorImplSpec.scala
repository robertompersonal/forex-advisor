package org.binqua.forex.testclient

import com.typesafe.config.ConfigFactory
import org.binqua.forex.util.TestingFacilities
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers._
import org.scalatest.wordspec.AnyWordSpec

class AppConfigValidatorImplSpec extends AnyWordSpec with BeforeAndAfterEach with TestingFacilities {

  val anInvalidConfigFile = "./src/test/resources/anInvalidExternalSystemClientsApp.conf"

  val aValidConfigFile = "./src/test/resources/aValidExternalSystemClientsApp.conf"

  "SystemConfigValidator" should {
    "create right configuration for a valid file" in {

      val actualValue = new AppConfigValidatorImpl {}.buildConfiguration(Array(s"-configFileName=$aValidConfigFile"))
      actualValue shouldBe Right(ConfigFactory.load("aValidExternalSystemClientsApp"))

    }

    "clear config library cache between configuration reading" in {
      for {
        config1 <- new AppConfigValidatorImpl {}.buildConfiguration(Array(s"-configFileName=$aValidConfigFile"))
        config2 <- new AppConfigValidatorImpl {}.buildConfiguration(Array(s"-configFileName=$anInvalidConfigFile"))
      } yield config1 should (not be config2)
    }

    "create right error message for an invalid file" in {

      val actualValue = new AppConfigValidatorImpl {}.buildConfiguration(Array(s"-configFileName=$anInvalidConfigFile"))

      actualValue.isLeft shouldBe true

      val actualError = actualValue.swap.getOrElse(thisTestShouldNotHaveArrivedHere)

      actualError should include("Configuration for Cluster Members Checker is invalid:")
      actualError should include("Configuration for Test Client is invalid:")

    }

  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      System.clearProperty("config.file")
    }
  }

}
