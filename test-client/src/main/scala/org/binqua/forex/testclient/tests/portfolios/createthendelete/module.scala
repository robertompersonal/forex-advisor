package org.binqua.forex.testclient.tests.portfolios.createthendelete

import akka.actor.typed.Behavior
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreatorUsingHttpClientModule
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletionModule
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete.{ChildrenMaker, Message}
import org.binqua.forex.util.core.MakeItUnsafe

trait CreateThenDeleteModule {

  def createThenDelete(): Behavior[Message]

}

trait DefaultCreateThenDeleteModule extends CreateThenDeleteModule {

  this: PortfoliosCreatorUsingHttpClientModule with PortfoliosDeletionModule =>

  def createThenDelete(): Behavior[Message] =
    CreateThenDelete(akkaConfig => Config.theConfigValidator(akkaConfig).unsafe)(
      SupportMessagesImpl,
      ChildrenMaker(
        org.binqua.forex.util.ChildMaker.fromContext[Message](childNamePrefix = "portfoliosCreation").withBehavior(portfoliosCreation())(),
        org.binqua.forex.util.ChildMaker.fromContext[Message](childNamePrefix = "portfoliosDeletion").withBehavior(portfoliosDeletion())()
      )
    )

}
