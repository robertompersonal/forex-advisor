package org.binqua.forex.testclient.portfolios.deletion

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import cats.data.NonEmptySet
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.PortfoliosModel
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.portfolios.creation.Config
import org.binqua.forex.testclient.util.RequestIdFactory
import org.binqua.forex.testclient.{FailureReason, ParserProblems, Precondition}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER
import org.binqua.forex.util.core.UnsafeConfigReader
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext.jsonwrites._
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

object PortfoliosDeletion {

  sealed trait Message

  case class Delete(portfoliosToBeDeleted: NonEmptySet[PortfoliosModel.PortfolioName], replyTo: ActorRef[Response]) extends Message

  sealed trait InternalMessage extends Message

  case class WrappedHttpClientResponse(response: HttpClient.Response) extends InternalMessage

  sealed trait Response

  case class Succeeded(portfolioSummary: PortfoliosSummary) extends Response

  case class Failed(reason: FailureReason) extends Response

  def deletionRequest(appURl: String, body: JsValue): HttpRequest =
    HttpRequest(
      method = HttpMethods.DELETE,
      uri = appURl,
      entity = HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(body)),
      headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
    )

  def apply(
      configReader: UnsafeConfigReader[Config]
  )(implicit
      jsonHttpClientChildMaker: CHILD_MAKER[Message, HttpClient.Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors.setup(implicit context => {

      implicit val config: Config = configReader(context.system.settings.config)

      implicit val adapters = Adapters(context)

      waitingForDeleteBehavior()

    })

  def startedBehavior(
      replyTo: ActorRef[Response]
  )(implicit
      context: ActorContext[Message],
      config: Config
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Delete(portfoliosToBeDeleted, replyTo) =>
          Behaviors.empty
        case WrappedHttpClientResponse(httpClientResponse) =>
          replyTo ! toResponse(httpClientResponse)(context)
          Behaviors.stopped
      })

  private def toResponse(response: HttpClient.Response)(implicit context: ActorContext[Message]): Response =
    response match {
      case HttpClient.Done(jsValue) =>
        PortfoliosSummary.jsonReads.reads(jsValue) match {
          case JsSuccess(portfolioSummary, _) => Succeeded(portfolioSummary)
          case jsError: JsError               => Failed(ParserProblems(jsError))
        }
      case HttpClient.Failed(failureReason) => Failed(Precondition(failureReason))
    }

  def waitingForDeleteBehavior()(implicit
      context: ActorContext[Message],
      config: Config,
      httpClientChildMaker: CHILD_MAKER[Message, HttpClient.Message],
      adapters: Adapters,
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Delete(portfoliosToBeDeleted, replyTo) =>
          val body: JsValue = Json.toJson(portfoliosToBeDeleted)
          val request = deletionRequest(config.appUrl.value, body)
          val id = requestIdFactory.of(context.self.path.name).id
          httpClientChildMaker(context) ! HttpClient.Submit(id, request, body.some, adapters.httpClient)
          startedBehavior(replyTo)
        case _ =>
          Behaviors.empty
      })

  case class Adapters(private val context: ActorContext[Message]) {
    val httpClient = context.messageAdapter[HttpClient.Response](WrappedHttpClientResponse)
  }

}
