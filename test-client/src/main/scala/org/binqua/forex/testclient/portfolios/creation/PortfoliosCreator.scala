package org.binqua.forex.testclient.portfolios.creation

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.util.RequestIdFactory
import org.binqua.forex.testclient.{FailureReason, ParserProblems, Precondition}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER
import org.binqua.forex.util.core.UnsafeConfigReader
import org.scalacheck.Gen
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

object PortfoliosCreator {

  sealed trait Message

  case class Start(replyTo: ActorRef[Response]) extends Message

  sealed trait InternalMessage extends Message

  case class WrappedHttpClientResponse(response: HttpClient.Response) extends InternalMessage

  sealed trait Response

  case class Succeeded(portfolioSummary: PortfoliosSummary) extends Response

  case class Failed(reason: FailureReason) extends Response

  def httpPostRequest(appURl: String, body: JsValue): HttpRequest =
    HttpRequest(
      method = HttpMethods.POST,
      uri = appURl,
      entity = HttpEntity(ContentTypes.`application/json`, Json.prettyPrint(body)),
      headers = List(Accept(MediaRange(MediaTypes.`application/json`)))
    )

  def apply(
      configReader: UnsafeConfigReader[Config]
  )(implicit
      portfolioCreationFactory: Gen[JsValue],
      httpClientChildMaker: CHILD_MAKER[Message, HttpClient.Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors.setup(implicit context => {

      implicit val config: Config = configReader(context.system.settings.config)

      implicit val thisActorAsHttpClientResponseAdapter: ActorRef[HttpClient.Response] =
        context.messageAdapter[HttpClient.Response](WrappedHttpClientResponse)

      waitingToStartBehavior()

    })

  def startedBehavior(
      replyTo: ActorRef[Response]
  )(implicit
      context: ActorContext[Message],
      config: Config,
      portfolioCreationFactory: Gen[JsValue]
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Start(_) =>
          Behaviors.empty
        case WrappedHttpClientResponse(httpClientResponse) =>
          replyTo ! toResponse(httpClientResponse)(context)
          Behaviors.stopped
      })

  private def toResponse(response: HttpClient.Response)(implicit context: ActorContext[Message]): Response = {
    response match {
      case HttpClient.Done(jsValue) =>
        PortfoliosSummary.jsonReads.reads(jsValue) match {
          case JsSuccess(portfolioSummary, _) => Succeeded(portfolioSummary)
          case jsError: JsError               => Failed(ParserProblems(jsError))
        }
      case HttpClient.Failed(failureReason) => Failed(Precondition(failureReason))
    }
  }

  def waitingToStartBehavior()(implicit
      context: ActorContext[Message],
      config: Config,
      portfolioCreationJsonFactory: Gen[JsValue],
      httpClientChildMaker: CHILD_MAKER[Message, HttpClient.Message],
      thisActorAsChildAdapter: ActorRef[HttpClient.Response],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Start(replyTo) =>
          val requestBody: JsValue = portfolioCreationJsonFactory.sample.get
          httpClientChildMaker(context) ! HttpClient
            .Submit(
              requestIdFactory.of(context.self.path.name).id,
              httpPostRequest(config.appUrl.value, requestBody),
              requestBody.some,
              thisActorAsChildAdapter
            )
          startedBehavior(replyTo)
        case _ =>
          Behaviors.empty
      })

}
