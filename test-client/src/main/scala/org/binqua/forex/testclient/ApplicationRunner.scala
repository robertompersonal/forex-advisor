package org.binqua.forex.testclient

import akka.actor.typed.ActorSystem
import org.binqua.forex.testclient.httpclient.DefaultHttpClientModule
import org.binqua.forex.testclient.portfolios.creation.DefaultPortfoliosCreatorUsingHttpClientModule
import org.binqua.forex.testclient.portfolios.deletion.DefaultPortfoliosDeletionModule
import org.binqua.forex.testclient.runner.DefaultClusterMembersCheckerModule

object ApplicationRunner
    extends AppConfigValidatorImpl
    with DefaultClusterMembersCheckerModule
    with tests.portfolios.createthendelete.DefaultCreateThenDeleteModule
    with DefaultPortfoliosCreatorUsingHttpClientModule
    with DefaultPortfoliosDeletionModule
    with DefaultHttpClientModule {

  def main(args: Array[String]): Unit = {
    buildConfiguration(args) match {
      case Left(error)       => println(error)
      case Right(akkaConfig) => ActorSystem(testRunner(), "testClient", akkaConfig)
    }
  }

}
