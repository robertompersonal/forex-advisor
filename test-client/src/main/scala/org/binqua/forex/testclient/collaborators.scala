package org.binqua.forex.testclient

import cats.Eq
import cats.data.Validated
import cats.implicits._
import com.typesafe.config
import eu.timepit.refined.predicates.all.Url
import eu.timepit.refined.refineV
import org.binqua.forex.testclient.Config.onlyAfterValidation.newConfig
import org.binqua.forex.util.Validation._
import org.binqua.forex.util.core.{ConfigValidator, ConfigValidatorBuilder}
import org.binqua.forex.util.{ValidateConfigByReference, core}
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.{JsPath, Reads, Writes, _}

import scala.concurrent.duration.FiniteDuration
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

sealed abstract case class Config(
    clientsToExternalSystems1: AkkaClusterUrl,
    clientsToExternalSystems2: AkkaClusterUrl,
    clusterManagementUrl: core.Url,
    feedReaderUrl: AkkaClusterUrl,
    retryClusterFormationInterval: FiniteDuration,
    webUrl: AkkaClusterUrl
)

sealed abstract case class AkkaClusterUrl(value: String) {

  def isReplicaOf(anotherAkkaCluster: AkkaClusterUrl): Boolean = Eq.eqv(nodeNamePrefix(anotherAkkaCluster.value), nodeNamePrefix(value))

  private def nodeNamePrefix(toBeAmended: String): String =
    toBeAmended match {
      case AkkaClusterUrl.reg(clusterName, nodeName, replicaId, port) => nodeName
      case _                                                          => value
    }
}

object AkkaClusterUrl {

  val reg: Regex = """akka:\/\/(.*)\@([^0-9]*)(\d?):([0-9]{1,5})$""".r

  object afterValidation {
    def newAkkaClusterUrl(url: String): AkkaClusterUrl = new AkkaClusterUrl(url) {}
  }

  implicit val eq: Eq[AkkaClusterUrl] = (x: AkkaClusterUrl, y: AkkaClusterUrl) => Eq.eqv(x.value, y.value)

  def from(url: String): ErrorOr[AkkaClusterUrl] =
    url match {
      case reg(clusterName, nodeName, replicaId, port) => afterValidation.newAkkaClusterUrl(url).asRight
      case _                                           => s"An akka cluster url has to be akka://clusterName@nodeName(\\d?):port. $url is not valid.".asLeft
    }

}

object Config {

  object onlyAfterValidation {
    def newConfig(
        clientsToExternalSystems1: AkkaClusterUrl,
        clientsToExternalSystems2: AkkaClusterUrl,
        clusterManagementUrl: core.Url,
        feedReaderUrl: AkkaClusterUrl,
        retryClusterFormationInterval: FiniteDuration,
        webUrl: AkkaClusterUrl
    ): Config = new Config(clientsToExternalSystems1, clientsToExternalSystems2, clusterManagementUrl, feedReaderUrl, retryClusterFormationInterval, webUrl) {}
  }

  val theConfigValidator: ConfigValidator[Config] = ConfigValidatorBuilderImpl(prefixConfigKey => s"org.binqua.forex.testclient.$prefixConfigKey")

  private[testclient] object ConfigValidatorBuilderImpl extends ConfigValidatorBuilder[Config] {
    override def apply(prefixConfigKey: String => String): ConfigValidator[Config] =
      (akkaConfig: config.Config) => {
        val clientsToExternalSystems1 = prefixConfigKey("clientsToExternalSystems1")
        val clientsToExternalSystems2 = prefixConfigKey("clientsToExternalSystems2")
        val clusterManagementUrl = prefixConfigKey("clusterManagementUrl")
        val feedReaderUrl = prefixConfigKey("feedReaderUrl")
        val retryClusterFormationInterval = prefixConfigKey("retryClusterFormationInterval")
        val webUrl = prefixConfigKey("webUrl")

        def akkaClusterUrl(key: String): Validated[List[String], AkkaClusterUrl] = {
          (Try(akkaConfig.getString(key)) match {
            case Success(value) => AkkaClusterUrl.from(value).leftMap(originalError => s"key $key is wrong. Details: $originalError")
            case Failure(_)     => Left(s"$key has to exist and has to be akka://clusterName@nodeName:port")
          }).leftMap(List(_)).toValidated
        }

        def url(key: String): Validated[List[String], core.Url] = {
          (Try(akkaConfig.getString(key)) match {
            case Success(value) => refineV[Url](value).leftMap(_ => s"$key has to be an url. $value is not valid.")
            case Failure(_)     => Left(s"$key has to exist and has to be an url")
          }).leftMap(List(_)).toValidated
        }

        (
          akkaClusterUrl(clientsToExternalSystems1),
          akkaClusterUrl(clientsToExternalSystems2),
          url(clusterManagementUrl),
          akkaClusterUrl(feedReaderUrl),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, retryClusterFormationInterval),
          akkaClusterUrl(webUrl)
        ).mapN(newConfig)
          .leftMap((errors: Seq[String]) => errors.+:("Configuration for Cluster Members Checker is invalid:").toList)
      }
  }

}

case class Members(members: Seq[Member])

case class Member(node: AkkaClusterUrl, status: String) {

  def isReplicaOf(urlToCheck: AkkaClusterUrl): Boolean =
    node.isReplicaOf(urlToCheck)

  val isUp: Boolean = Eq.eqv("up", status.toLowerCase)
}

object Member {

  def memberUp(node: AkkaClusterUrl): Member = Member(node, "up")

  def memberDown(node: AkkaClusterUrl): Member = Member(node, "down")
}

object json {

  object jsonPaths {
    val leader: JsPath = JsPath \ "leader"
    val members: JsPath = JsPath \ "members"
    val node: JsPath = JsPath \ "node"
    val status: JsPath = JsPath \ "status"
  }

  object writes {
    implicit val akkaClusterUrl: Writes[AkkaClusterUrl] = (o: AkkaClusterUrl) => JsString(o.value)

    implicit val memberWrites: Writes[Member] = {
      (jsonPaths.node.write[AkkaClusterUrl] and jsonPaths.status.write[String])(unlift(Member.unapply))
    }

    implicit val membersWrites: Writes[Members] = (members: Members) =>
      JsArray(
        members.members.map(Json.toJson(_)(memberWrites))
      )

    implicit val clusterWrites: Writes[Cluster] = {
      (jsonPaths.leader.write[String] and
        jsonPaths.members.write[Members])(unlift(Cluster.unapply))
    }

  }

  object reads {

    implicit val akkaClusterUrl: Reads[AkkaClusterUrl] = jsonPaths.node
      .read[String]
      .flatMap(nodeAsString =>
        AkkaClusterUrl.from(nodeAsString) match {
          case Right(value) => Reads.pure(value)
          case Left(error)  => Reads.failed(error)
        }
      )

    implicit val member: Reads[Member] = (akkaClusterUrl and jsonPaths.status.read[String])((node, status) => Member(node, status))

    implicit val members: Reads[Members] = jsonPaths.members.read[Seq[Member]].map(Members)

    implicit val cluster: Reads[Cluster] = (jsonPaths.leader.read[String] and members)((leader, members) => Cluster(leader, members))

  }

}

object Cluster {
  def isFormed(cluster: Cluster, config: Config): Validated[List[String], Boolean] =
    (
      cluster.problemOf(config.feedReaderUrl).toLeft(true).toListOfErrors.toValidated,
      cluster.problemOf(config.webUrl).toLeft(true).toListOfErrors.toValidated,
      cluster.problemOf(config.clientsToExternalSystems1).toLeft(true).toListOfErrors.toValidated
    ).mapN((feedReaderUrlNoProblem, webUrlNoProblem, clientsToExternalSystemsNoProblem) =>
      feedReaderUrlNoProblem && webUrlNoProblem && clientsToExternalSystemsNoProblem
    )
}

case class Cluster(leader: String, members: Members) {

  def isUp(urlToCheck: AkkaClusterUrl, members: Members): Boolean = members.members.exists(member => Eq.eqv(urlToCheck, member.node) && member.isUp)

  def anotherReplicaIsUp(urlToCheck: AkkaClusterUrl, members: Members): Boolean =
    members.members.exists(member => member.node.isReplicaOf(urlToCheck) && member.isUp)

  def problemOf(urlToCheck: AkkaClusterUrl): Option[String] =
    if (isUp(urlToCheck, members) || anotherReplicaIsUp(urlToCheck, members)) None
    else Some(s"member $urlToCheck is not Up and any replica is not Up")

}
