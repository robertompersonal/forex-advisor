package org.binqua.forex.testclient.portfolios.creation

import akka.actor.typed.Behavior
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.testclient.httpclient.HttpClientModule
import org.binqua.forex.testclient.portfolios.creation
import org.binqua.forex.testclient.util.DefaultRequestIdFactory
import org.binqua.forex.util.ChildMaker
import org.binqua.forex.util.core.MakeItUnsafe
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.scalacheck.Gen
import play.api.libs.json.JsValue
import play.api.libs.json.Json.toJson

import java.time.LocalDateTime
import scala.util.Random

trait PortfoliosCreatorUsingHttpClientModule {

  def portfoliosCreation(): Behavior[PortfoliosCreator.Message]

}

trait DefaultPortfoliosCreatorUsingHttpClientModule extends PortfoliosCreatorUsingHttpClientModule {

  this: HttpClientModule =>

  val randomJsonRequestBody: Gen[JsValue] = (for {
    idempotentKey <- PortfoliosGen.idempotentKey
    portfolioName <- PortfoliosGen.portfolioName
    numberOfPositions <- Gen.choose(1, 10)
    positions <- Gen.listOfN(numberOfPositions, PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(LocalDateTime.of(2020, 3, 12, 20, 0, 1)))
  } yield PortfoliosJsonTestContext.CreatePortfolioJsonBuilder(
    idempotentKey.key.toString,
    portfolioName.name.value,
    positions = positions
  )).map(builder => toJson(builder)(PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites))

  def portfoliosCreation(): Behavior[PortfoliosCreator.Message] =
    creation.PortfoliosCreator(akkaConfig => Config.theConfigValidator(akkaConfig).unsafe)(
      randomJsonRequestBody,
      ChildMaker
        .fromContext[PortfoliosCreator.Message](childNamePrefix = "HttpClient-for-creation")
        .withBehavior(httpClient())(),
      DefaultRequestIdFactory(Random)
    )

}
