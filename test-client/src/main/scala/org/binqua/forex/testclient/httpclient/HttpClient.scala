package org.binqua.forex.testclient.httpclient

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, PostStop}
import akka.http.scaladsl.model._
import akka.http.scaladsl.{Http, HttpExt}
import play.api.libs.json.{JsValue, Json}

import scala.util.{Failure, Success, Try}

object HttpClient {

  sealed trait Message

  case class Submit(uniqueId: String, httpRequest: HttpRequest, maybeABody: Option[JsValue], replyTo: ActorRef[Response]) extends Message

  sealed trait InternalMessage extends Message

  case class HttpBodyReceptionFailed(statusCode: StatusCode, errors: Throwable) extends InternalMessage

  case class HttpBodyReceptionSuccess(statusCode: StatusCode, body: String) extends InternalMessage

  case class HttpInvocationException(throwable: Throwable) extends InternalMessage

  case class HttpResponseReceived(httpResponse: HttpResponse) extends InternalMessage

  sealed trait Response

  case class Done(jsValue: JsValue) extends Response

  case class Failed(failureReason: FailureReason) extends Response

  sealed trait FailureReason {
    val requestId: String
  }

  case class PreconditionException(requestId: String, throwable: Throwable) extends FailureReason

  case class BodyParserException(requestId: String, statusCode: Int, throwable: Throwable, responseBody: String) extends FailureReason

  case class BodyReceptionException(requestId: String, statusCode: Int, throwable: Throwable) extends FailureReason

  case class WrongStatusCode(requestId: String, statusCode: Int, responseBody: String) extends FailureReason

  def apply(implicit
      supportMessages: SupportMessages,
      httpBodyExtractor: HttpBodyExtractor
  ): Behavior[Message] =
    Behaviors.setup(implicit context => {

      implicit val untypedActorSystem: ActorSystem = context.system.toClassic

      implicit val http: HttpExt = Http(untypedActorSystem)

      waitingForSubmitBehavior()

    })

  def waitingForResponseBehavior(
      replyTo: ActorRef[Response],
      uniqueId: String
  )(implicit
      context: ActorContext[Message],
      http: HttpExt,
      supportMessages: SupportMessages,
      httpBodyExtractor: HttpBodyExtractor
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Submit(_, _, _, _) => Behaviors.empty
        case HttpBodyReceptionSuccess(statusCode, body) =>
          context.log.info(supportMessages.receivedResponse(uniqueId, replyTo, statusCode, body))
          if (statusCode.isSuccess()) {
            Try(Json.parse(body)) match {
              case Success(validJson) => replyTo ! Done(validJson)
              case Failure(throwable) => replyTo ! Failed(BodyParserException(uniqueId, statusCode.intValue(), throwable, body))
            }
          } else
            replyTo ! Failed(WrongStatusCode(uniqueId, statusCode.intValue(), body))
          Behaviors.stopped
        case HttpBodyReceptionFailed(statusCode, throwable) =>
          replyTo ! Failed(BodyReceptionException(uniqueId, statusCode.intValue(), throwable))
          Behaviors.stopped
        case HttpInvocationException(throwable) =>
          replyTo ! Failed(PreconditionException(uniqueId, throwable))
          Behaviors.stopped
        case HttpResponseReceived(httpResponse) =>
          context.pipeToSelf(httpBodyExtractor.eventuallyABody(httpResponse.entity)(context.system.toClassic, context.system.executionContext)) {
            case Success(body) => HttpBodyReceptionSuccess(httpResponse.status, body)
            case Failure(ex)   => HttpBodyReceptionFailed(httpResponse.status, ex)
          }
          Behaviors.same
      })
      .receiveSignal({
        case (context, PostStop) =>
          context.log.info(supportMessages.stoppedAndReleasedHttpConnections(context.self.path))
          http.shutdownAllConnectionPools()
          Behaviors.same
      })

  def waitingForSubmitBehavior()(implicit
      context: ActorContext[Message],
      http: HttpExt,
      supportMessages: SupportMessages,
      httpBodyExtractor: HttpBodyExtractor
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case Submit(uniqueId, httpRequest, maybeABody, replyTo) =>
          context.log.info(supportMessages.submittingRequest(uniqueId, httpRequest, replyTo, maybeABody))
          context.pipeToSelf(http.singleRequest(httpRequest)) {
            case Success(httpResponse) => HttpResponseReceived(httpResponse)
            case Failure(ex)           => HttpInvocationException(ex)
          }
          waitingForResponseBehavior(replyTo, uniqueId)
        case _ =>
          Behaviors.empty
      })
      .receiveSignal({
        case (context, PostStop) =>
          context.log.info(supportMessages.stoppedAndReleasedHttpConnections(context.self.path))
          http.shutdownAllConnectionPools()
          Behaviors.same
      })

}
