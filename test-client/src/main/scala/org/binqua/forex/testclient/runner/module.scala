package org.binqua.forex.testclient.runner

import akka.actor.typed.Behavior
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.testclient.Config
import org.binqua.forex.testclient.httpclient.HttpClientModule
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDeleteModule
import org.binqua.forex.testclient.util.DefaultRequestIdFactory
import org.binqua.forex.util.NewChildMaker
import org.binqua.forex.util.NewChildMaker.ChildNamePrefix
import org.binqua.forex.util.core.MakeItUnsafe

import scala.util.Random

trait TestRunnerModule {

  def testRunner(): Behavior[TestRunner.Message]

}

trait DefaultClusterMembersCheckerModule extends TestRunnerModule {

  this: CreateThenDeleteModule with HttpClientModule =>

  def testRunner(): Behavior[TestRunner.Message] =
    TestRunner(akkaConfig => Config.theConfigValidator(akkaConfig).unsafe, SupportMessagesFactoryImpl)(
      NewChildMaker.fromContext[TestRunner.Message](ChildNamePrefix(refineMV[NonEmpty]("clusterFormationHttpClient"))).withBehavior(httpClient()),
      NewChildMaker.fromContext[TestRunner.Message](ChildNamePrefix(refineMV[NonEmpty]("createThenDeletePortfolios"))).withBehavior(createThenDelete()),
      DefaultRequestIdFactory(Random)
    )

}
