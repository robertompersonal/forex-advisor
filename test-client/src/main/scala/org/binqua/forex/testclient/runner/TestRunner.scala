package org.binqua.forex.testclient.runner

import akka.actor.typed._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.http.scaladsl.model._
import cats.data.Validated.{Invalid, Valid}
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.testclient.httpclient.HttpClient
import org.binqua.forex.testclient.tests.portfolios.createthendelete.CreateThenDelete
import org.binqua.forex.testclient.util.RequestIdFactory
import org.binqua.forex.testclient.{Cluster, Config, json}
import org.binqua.forex.util.NewChildMaker
import org.binqua.forex.util.NewChildMaker.ChildrenFactory
import org.binqua.forex.util.core.UnsafeConfigReader
import play.api.libs.json.{JsError, JsSuccess, JsValue}

import scala.concurrent.duration.DurationInt

object TestRunner {

  sealed trait Message

  sealed trait InternalMessage extends Message

  case object ContactHttpManagementServer extends InternalMessage

  case class WrappedClusterHttpResponse(httpClientResponse: HttpClient.Response) extends InternalMessage

  case object CheckClusterFormationAgain extends InternalMessage

  def apply(configReader: UnsafeConfigReader[Config], supportMessagesFactory: SupportMessagesFactory)(implicit
      httpClientChildMaker: ChildrenFactory[Message, HttpClient.Message],
      createThenDeleteChildMaker: ChildrenFactory[Message, CreateThenDelete.Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors
      .supervise[Message] {
        Behaviors.withTimers(implicit timers =>
          Behaviors.setup(implicit context => {

            implicit val config: Config = configReader(context.system.settings.config)

            implicit val supportMessages = supportMessagesFactory.newSupportMessages(config)

            implicit val childrenMaker = ChildrenMaker(context, httpClientChildMaker, createThenDeleteChildMaker)

            implicit val adapters = Adapters(context)

            context.self ! ContactHttpManagementServer

            behavior(maybeACreateThenDeleteTestChild = None)

          })
        )
      }
      .onFailure[Exception](SupervisorStrategy.restartWithBackoff(500.millis, 10.seconds, randomFactor = 0.2))

  def clusterFormationHandler(jsonCluster: JsValue, maybeACreateThenDeleteTestChild: Option[ActorRef[CreateThenDelete.Message]])(implicit
      adapters: Adapters,
      childrenMaker: ChildrenMaker,
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      config: Config,
      timers: TimerScheduler[Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] = {
    context.self ! CheckClusterFormationAgain
    jsonCluster.validate[Cluster](json.reads.cluster) match {
      case JsSuccess(clusterData, _) =>
        Cluster.isFormed(clusterData, config) match {
          case Valid(_) =>
            context.log.info(supportMessages.clusterFormed())
            if (maybeACreateThenDeleteTestChild.isDefined) {
              Behaviors.same
            } else {
              val createThenDeleteTestChild: ActorRef[CreateThenDelete.Message] = childrenMaker.newCreateThenDeletePortfolios()
              context.watch(createThenDeleteTestChild)
              createThenDeleteTestChild ! CreateThenDelete.StartTest
              behavior(createThenDeleteTestChild.some)
            }
          case Invalid(errors) =>
            context.log.error(supportMessages.clusterFormationFailed(errors))
            maybeACreateThenDeleteTestChild.foreach(child => child ! CreateThenDelete.Stop)
            behavior(None)
        }
      case _: JsError =>
        context.log.error(supportMessages.clusterDataParserFailed(jsonCluster))
        maybeACreateThenDeleteTestChild.foreach(child => child ! CreateThenDelete.Stop)
        behavior(None)
    }
  }

  def clusterFormationPreconditionFailed(
      failureReason: HttpClient.FailureReason,
      maybeACreateThenDeletePorfoliosChild: Option[ActorRef[CreateThenDelete.Message]]
  )(implicit
      adapters: Adapters,
      childrenMaker: ChildrenMaker,
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      config: Config,
      timers: TimerScheduler[Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] = {
    context.log.error(supportMessages.clusterFormationPreconditionFailed(failureReason))
    maybeACreateThenDeletePorfoliosChild.foreach(child => child ! CreateThenDelete.Stop)
    context.self ! CheckClusterFormationAgain
    behavior(None)
  }

  def onSignal()(implicit
      adapters: Adapters,
      childrenMaker: ChildrenMaker,
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      config: Config,
      timers: TimerScheduler[Message],
      requestIdFactory: RequestIdFactory
  ): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
    case (context, ChildFailed(ref, throwable)) =>
      context.log.error(supportMessages.childCrashed(ref.path.name, throwable))
      throw throwable
      Behaviors.same
    case (_, Terminated(ref)) =>
      if (childrenMaker.createThenDeletePortfoliosPrefix.isRelatedTo(ref)) {
        behavior(None)
      } else
        Behaviors.same
  }

  def behavior(maybeACreateThenDeleteTestChild: Option[ActorRef[CreateThenDelete.Message]])(implicit
      adapters: Adapters,
      childrenMaker: ChildrenMaker,
      context: ActorContext[Message],
      supportMessages: SupportMessages,
      config: Config,
      timers: TimerScheduler[Message],
      requestIdFactory: RequestIdFactory
  ): Behavior[Message] =
    Behaviors
      .receiveMessage[Message]({
        case CheckClusterFormationAgain =>
          context.log.info(supportMessages.retryingSoon())
          timers.startSingleTimer(ContactHttpManagementServer, config.retryClusterFormationInterval)
          Behaviors.same
        case ContactHttpManagementServer =>
          context.log.info(supportMessages.contactingHttpManagementServer())
          val clusterFormationHttpClientChild = childrenMaker.newClusterFormationHttpClient()
          context.watch(clusterFormationHttpClientChild)
          clusterFormationHttpClientChild ! HttpClient.Submit(
            requestIdFactory.of(context.self.path.name).id,
            HttpRequest(HttpMethods.GET, config.clusterManagementUrl.value),
            None,
            adapters.clusterFormationHttpClient
          )
          behavior(maybeACreateThenDeleteTestChild)
        case WrappedClusterHttpResponse(clusterHttpResponse) =>
          clusterHttpResponse match {
            case HttpClient.Done(jsCluster)       => clusterFormationHandler(jsCluster, maybeACreateThenDeleteTestChild)
            case HttpClient.Failed(failureReason) => clusterFormationPreconditionFailed(failureReason, maybeACreateThenDeleteTestChild)
          }
      })
      .receiveSignal(onSignal())

  case class ChildrenMaker(
      private val context: ActorContext[Message],
      private val httpClientChildrenFactoryContext: ChildrenFactory[Message, HttpClient.Message],
      private val createThenDeletePortfoliosChildrenFactoryContext: ChildrenFactory[Message, CreateThenDelete.Message]
  ) {

    def newClusterFormationHttpClient(): ActorRef[HttpClient.Message] = httpClientChildrenFactoryContext.newChild(context)

    val createThenDeletePortfoliosPrefix: NewChildMaker.ChildNamePrefix = createThenDeletePortfoliosChildrenFactoryContext.childNamePrefix

    def newCreateThenDeletePortfolios(): ActorRef[CreateThenDelete.Message] = createThenDeletePortfoliosChildrenFactoryContext.newChild(context)
  }

  case class Adapters(private val context: ActorContext[Message]) {
    val clusterFormationHttpClient = context.messageAdapter[HttpClient.Response](WrappedClusterHttpResponse)
  }

}
