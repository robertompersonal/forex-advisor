package org.binqua.forex.testclient.portfolios.deletion

import akka.actor.typed.Behavior
import org.binqua.forex.testclient.httpclient.HttpClientModule
import org.binqua.forex.testclient.portfolios.creation.Config
import org.binqua.forex.testclient.util.DefaultRequestIdFactory
import org.binqua.forex.util.ChildMaker
import org.binqua.forex.util.core.MakeItUnsafe

import scala.util.Random

trait PortfoliosDeletionModule {

  def portfoliosDeletion(): Behavior[PortfoliosDeletion.Message]

}

trait DefaultPortfoliosDeletionModule extends PortfoliosDeletionModule {

  this: HttpClientModule =>

  def portfoliosDeletion(): Behavior[PortfoliosDeletion.Message] =
    PortfoliosDeletion(akkaConfig => Config.theConfigValidator(akkaConfig).unsafe)(
      ChildMaker
        .fromContext[PortfoliosDeletion.Message](childNamePrefix = "HttpClient-for-deletion")
        .withBehavior(httpClient())(),
      DefaultRequestIdFactory(Random)
    )

}
