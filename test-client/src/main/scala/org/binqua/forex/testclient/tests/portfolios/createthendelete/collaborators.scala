package org.binqua.forex.testclient.tests.portfolios.createthendelete

import cats.Show
import cats.data.{Validated, Writer}
import cats.implicits.{catsSyntaxTuple3Semigroupal, catsSyntaxValidatedId}
import cats.syntax.show._
import com.typesafe.config
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.testclient.portfolios.creation.PortfoliosCreator
import org.binqua.forex.testclient.portfolios.deletion.PortfoliosDeletion
import org.binqua.forex.util.ValidateConfigByReference
import org.binqua.forex.util.core.{ConfigValidator, ConfigValidatorBuilder}

import scala.concurrent.duration.FiniteDuration

trait SupportMessages {
  def testStopped(testReport: TestReport): String

  def testComplete(testComplete: Writer[TestResult, TestReport]): String

  def runningTest(testReport: TestReport): String

  def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosDeletion.Failed): String

  def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosCreator.Failed): String

}

private[createthendelete] object SupportMessagesImpl extends SupportMessages {
  override def runningTest(testReport: TestReport): String = s"Running test number ${testReport.completed + 1}"

  override def testComplete(testComplete: Writer[TestResult, TestReport]): String = {

    val testReport = testComplete.value
    s"Test ${testComplete.value.completed} ${if (testComplete.written.passed) "passed"
    else "failed"} because ${testComplete.written.reason}.\n${testReport.show}"
  }

  override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosDeletion.Failed): String =
    s"Test aborted due to $preconditionFailed.\n${testReport.show}"

  override def testPreconditionFailed(testReport: TestReport, preconditionFailed: PortfoliosCreator.Failed): String =
    s"Test aborted due to $preconditionFailed.\n${testReport.show}"

  override def testStopped(testReport: TestReport): String = s"Test has been stopped. Final report:\n${testReport.show}"
}

sealed trait TestResult {
  val passed: Boolean
  val reason: String
}

object TestPassed extends TestResult {
  override val passed: Boolean = true
  override val reason: String = "portfolio summary has no portfolios"
}

case class TestFailed(private val numberOfPortfolios: Int) extends TestResult {
  override val passed: Boolean = false
  override val reason: String = s"portfolio summary should have no portfolios but has $numberOfPortfolios instead"
}

object TestReport {
  val empty = TestReport(completed = 0, aborted = 0, success = 0, failure = 0)
  implicit val showImpl: Show[TestReport] = testReport =>
    s"${testReport.completed} tests ${testReport.success} success ${testReport.failure} failure ${testReport.aborted} aborted"
}

case class TestReport(completed: Int, aborted: Int, success: Int, failure: Int) {
  def testAborted(): TestReport = this.copy(aborted = this.aborted + 1).copy(completed = completed + 1)

  def testComplete(portfolioSummary: PortfoliosSummary): Writer[TestResult, TestReport] =
    if (portfolioSummary.size == 0)
      Writer(TestPassed, this.copy(completed = completed + 1, success = success + 1))
    else
      Writer(TestFailed(portfolioSummary.size), this.copy(completed = completed + 1, failure = failure + 1))
}

sealed abstract case class Config(
    betweenCreationAndDeletionInterval: FiniteDuration,
    betweenTestsInterval: FiniteDuration,
    retryInterval: FiniteDuration
)

object Config {

  object onlyAfterValidation {
    def newConfig(betweenCreationAndDeletionInterval: FiniteDuration, intervalBetweenTests: FiniteDuration, retryInterval: FiniteDuration): Config =
      new Config(betweenCreationAndDeletionInterval, intervalBetweenTests, retryInterval) {}
  }

  def validated(
      betweenCreationAndDeletionInterval: FiniteDuration,
      intervalBetweenTests: FiniteDuration,
      retryInterval: FiniteDuration
  ): Validated[List[String], Config] =
    onlyAfterValidation.newConfig(betweenCreationAndDeletionInterval, intervalBetweenTests, retryInterval).valid

  val theConfigValidator: ConfigValidator[Config] =
    ConfigValidatorBuilderImpl(prefixConfigKey => s"org.binqua.forex.testclient.tests.portfolios.createthendelete.$prefixConfigKey")

  private[createthendelete] object ConfigValidatorBuilderImpl extends ConfigValidatorBuilder[Config] {
    override def apply(prefixConfigKey: String => String): ConfigValidator[Config] =
      (akkaConfig: config.Config) => {
        val betweenCreationAndDeletionInterval = prefixConfigKey("betweenCreationAndDeletionInterval")
        val intervalBetweenTests = prefixConfigKey("intervalBetweenTests")
        val retryInterval = prefixConfigKey("retryInterval")

        (
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, betweenCreationAndDeletionInterval),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, intervalBetweenTests),
          ValidateConfigByReference.validateADurationConfigValue(akkaConfig, retryInterval)
        ).mapN(onlyAfterValidation.newConfig)
          .leftMap((errors: Seq[String]) => errors.+:("Configuration for Test Client is invalid:").toList)
      }
  }

}
