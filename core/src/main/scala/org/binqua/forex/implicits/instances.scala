package org.binqua.forex.implicits

import cats.Show
import cats.kernel.Eq
import org.binqua.forex.advisor.model._
import org.binqua.forex.feed.notifier.collaborators.UpdatingAccount
import org.binqua.forex.feed.reader.model.{Quote, Rates}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId

import scala.language.implicitConversions

object instances {

  object accountCurrency {

    import id._

    class ToIdentifier(accountCurrency: AccountCurrency) {
      def toId: Id = Id.idFor(accountCurrency.name)
    }

    implicit def fromAccountCurrencyToIdentifier(accountCurrency: AccountCurrency): ToIdentifier = new ToIdentifier(accountCurrency)

    implicit val accountCurrencyShow: Show[AccountCurrency] = Show.show(accountCurrency => s"account currency ${accountCurrency.name}")

    implicit val accountCurrencyEq: Eq[AccountCurrency] = (x: AccountCurrency, y: AccountCurrency) => Eq.eqv(x.toId, y.toId)

  }

  object baseCurrency {

    import cats.instances.boolean._
    import cats.instances.string._
    import cats.kernel.Eq

    class ToIdentifier(baseCurrency: BaseCurrency) {
      def toId: Id = Id.idFor(baseCurrency.name)
    }

    implicit def fromBaseCurrencyToIdentifier(baseCurrency: BaseCurrency): ToIdentifier = new ToIdentifier(baseCurrency)

    implicit val baseCurrencyEq: Eq[BaseCurrency] = (a: BaseCurrency, b: BaseCurrency) => {
      Eq.eqv(a.name, b.name) && Eq.eqv(a.index, b.index)
    }
  }

  object currencyPair {

    import baseCurrency._
    import cats.Show
    import cats.kernel.Eq
    import cats.syntax.show._
    import id._
    import quoteCurrency._

    implicit val currencyPairEq: Eq[CurrencyPair] = (x, y) =>
      baseCurrencyEq.eqv(x.baseCurrency, y.baseCurrency) && quoteCurrencyEq.eqv(x.quoteCurrency, y.quoteCurrency)

    implicit val currencyPairShow: Show[CurrencyPair] =
      Show.show(currencyPair => s"currency pair ${currencyPair.baseCurrency.name}/${currencyPair.quoteCurrency.baseCurrency.name}")

    class ToIdentifier(cp: CurrencyPair) {
      def toId: Id =
        Id.idFor(
          if (cp.baseCurrency.index) {
            cp.baseCurrency.toId.show
          } else
            s"${cp.baseCurrency.toId.show}/${cp.quoteCurrency.toId.show}"
        )
    }

    class ToIdentifiers(currencyPair: CurrencyPair) {
      def toIds: Ids =
        new Ids {
          override val ids: (Id, Id) = (currencyPair.baseCurrency.toId, currencyPair.quoteCurrency.toId)
        }
    }

    implicit def fromCurrencyPairToIdentifiers(currencyPair: CurrencyPair): ToIdentifiers = new ToIdentifiers(currencyPair)

    implicit def fromCurrencyPairToIdentifier(currencyPair: CurrencyPair): ToIdentifier = new ToIdentifier(currencyPair)

  }

  object id {

    import cats.instances.string._
    import cats.syntax.show._

    implicit val idEq: Eq[Id] = (x: Id, y: Id) => Eq.eqv(x.id, y.id)

    implicit val idsEq: Eq[(Id, Id)] = (x: (Id, Id), y: (Id, Id)) => Eq.eqv(x._1, y._1) && Eq.eqv(x._2, y._2)

    implicit val idShow: Show[Id] = Show.show(id => s"${id.id}")

    implicit val idsShow: Show[(Id, Id)] = Show.show(id => s"(${id._1.show},${id._2.show})")
  }

  object quoteCurrency {

    import baseCurrency._
    import cats.kernel.Eq

    class ToIdentifier(quoteCurrency: QuoteCurrency) {
      def toId: Id = Id.idFor(quoteCurrency.baseCurrency.name)
    }

    implicit val quoteCurrencyEq: Eq[QuoteCurrency] = (a: QuoteCurrency, b: QuoteCurrency) => Eq.eqv(a.baseCurrency, b.baseCurrency)

    implicit def fromQuoteCurrencyToIdentifier(quoteCurrency: QuoteCurrency): ToIdentifier = new ToIdentifier(quoteCurrency)

  }

  object quote {

    import cats.syntax.show._
    import rates._

    implicit val quoteShow: Show[Quote] = Show.show(quote => s"${quote.rates.show} updated=${quote.updated}")

  }

  object option {

    implicit def optionShow[A](implicit showOfA: Show[A]): Show[Option[A]] =
      (t: Option[A]) =>
        t match {
          case Some(value) => showOfA.show(value)
          case None        => "No Optional Value :("
        }
  }

  object rates {

    implicit val ratesShow: Show[Rates] =
      Show.show(rates => s"Rates sell=${rates.sell} buy=${rates.buy} max=${rates.maxBuyOfTheDay}  min=${rates.minSellOfTheDay} ${rates.currencyPair}")

  }

  object socketId {

    import cats.instances.string._

    implicit val socketIdShow: Show[SocketId] = Show.show(socketId => s"socketId ${socketId.id}")
    implicit val socketIdEq: Eq[SocketId] = (x: SocketId, y: SocketId) => Eq.eqv(x.id, y.id)

  }

  object updatingAccount {

    import accountCurrency._
    import cats.syntax.show._
    import option._
    import quote._

    implicit val updatingAccountShow: Show[UpdatingAccount] = Show.show(updatingAccount =>
      s"updatingAccount updatedQuote=${updatingAccount.incomingUpdatedQuote.show} and extraQuote=${updatingAccount.maybeAnExtraQuote.show} and ${updatingAccount.theAccountCurrency.show}"
    )

  }

}
