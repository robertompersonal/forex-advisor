package org.binqua.forex.advisor.model

object CurrencySymbol extends Enumeration {

  protected case class CustomVal(i: Int, name: String, pipValue: Int, trueCurrency: Boolean) extends super.Val

  import scala.language.implicitConversions

  implicit def valueToVal(value: CustomVal): CustomVal = value.asInstanceOf[CustomVal]

  val Gbp = CustomVal(0, "Gbp", 10, true)
  val Eur = CustomVal(1, "Eur", 10, true)
  val Usd = CustomVal(2, "Usd", 10, true)
  val US30 = CustomVal(3, "US30", 1, false)
  val SPX500 = CustomVal(4, "SPX500", 1, false)
}
