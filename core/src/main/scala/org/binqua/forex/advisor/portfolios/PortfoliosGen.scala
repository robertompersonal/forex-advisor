package org.binqua.forex.advisor.portfolios

import cats.data.NonEmptySeq
import monocle.Lens
import monocle.macros.GenLens
import org.binqua.forex.advisor.model._
import org.binqua.forex.advisor.newportfolios.State.COMMAND_HASH
import org.binqua.forex.advisor.newportfolios.events
import org.binqua.forex.advisor.portfolios.PortfoliosModel.events._
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{
  Add,
  Buy,
  CreatePortfolio,
  Delete,
  PortfolioName,
  PortfolioSingleUpdate,
  Position,
  PositionId,
  PositionType,
  RecordedPosition,
  Sell,
  Update,
  UpdatePortfolio
}
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.binqua.forex.util.StringUtils
import org.scalacheck.Gen

import java.time.LocalDateTime
import java.util.UUID

object PortfoliosGen {

  val idempotentKey: Gen[service.IdempotentKey] = Gen.uuid.map(IdempotentKey)

  val commandHash: Gen[COMMAND_HASH] = Gen.chooseNum(Int.MinValue, Int.MaxValue)

  val portfolioName: Gen[PortfoliosModel.PortfolioName] = StringUtils.ofLengthBetween(min = 3, max = 30).map(PortfolioName.unsafe)

  val error: Gen[String] = StringUtils.ofLengthBetween(min = 10, max = 20).map(e => s"some random error $e")

  val createPortfolio: Gen[CreatePortfolio] = createPortfolio(1, 4)

  def createPortfolio(numberOfPositions: Int): Gen[CreatePortfolio] = createPortfolio(numberOfPositions, numberOfPositions)

  def createPortfolio(minNumberOfPositions: Int, maxNumberOfPositions: Int): Gen[CreatePortfolio] = {
    require(minNumberOfPositions > 0 && minNumberOfPositions <= maxNumberOfPositions)
    for {
      id <- idempotentKey
      name <- portfolioName
      numberOfPositions <- Gen.chooseNum(minNumberOfPositions, maxNumberOfPositions)
      positions <- Gen.listOfN(numberOfPositions, PortfoliosGen.position)
    } yield CreatePortfolio(
      id,
      name,
      NonEmptySeq.fromSeqUnsafe(positions)
    )
  }

  val newPortfolioCreatedEvent: Gen[org.binqua.forex.advisor.newportfolios.events.PortfolioCreated] = newPortfolioCreatedEvent(minNumberOfPositions = 1)

  val portfolioCreatedEvent: Gen[PortfoliosModel.events.PortfolioCreated] = portfolioCreatedEvent(minNumberOfPositions = 1)

  def toPortfolioUpdates(recordedPositions: NonEmptySeq[RecordedPosition]): NonEmptySeq[SingleUpdate] =
    recordedPositions.map(rp =>
      Gen.oneOf(0, 1, 2).sample.get match {
        case 0 => Added(rp)
        case 1 => Updated(rp)
        case 2 => Deleted(rp.id)
      }
    )

  def toNewPortfolioUpdates(recordedPositions: NonEmptySeq[RecordedPosition]): NonEmptySeq[org.binqua.forex.advisor.newportfolios.events.SingleUpdate] =
    recordedPositions.map(rp =>
      Gen.oneOf(0, 1, 2).sample.get match {
        case 0 => org.binqua.forex.advisor.newportfolios.events.Added(rp)
        case 1 => org.binqua.forex.advisor.newportfolios.events.Updated(rp)
        case 2 => org.binqua.forex.advisor.newportfolios.events.Deleted(rp.id)
      }
    )

  def addedPortfolioUpdates(recordedPositions: NonEmptySeq[RecordedPosition]): NonEmptySeq[org.binqua.forex.advisor.newportfolios.events.SingleUpdate] =
    recordedPositions.map(rp => org.binqua.forex.advisor.newportfolios.events.Added(rp))

  def updatedAndDeletedOnlyEvents(
      recordedPositions: NonEmptySeq[RecordedPosition]
  ): NonEmptySeq[org.binqua.forex.advisor.newportfolios.events.SingleUpdate] =
    recordedPositions.map(rp =>
      Gen.oneOf(0, 1).sample.get match {
        case 0 => org.binqua.forex.advisor.newportfolios.events.Updated(rp)
        case 1 => org.binqua.forex.advisor.newportfolios.events.Deleted(rp.id)
      }
    )

  def portfolioUpdated: Gen[PortfolioUpdated] =
    for {
      name <- PortfoliosGen.portfolioName
      numberOfPositions <- Gen.chooseNum(1, 7)
      positions <- Gen.listOfN(numberOfPositions, recordedPosition)
    } yield PortfolioUpdated.unsafe(name, toPortfolioUpdates(NonEmptySeq.fromSeqUnsafe(positions)))

  def newPortfolioUpdates(maxNumberOfPositions: Int): Gen[NonEmptySeq[org.binqua.forex.advisor.newportfolios.events.SingleUpdate]] = {
    require(maxNumberOfPositions > 1)
    for {
      numberOfPositions <- Gen.chooseNum(minT = 1, maxT = maxNumberOfPositions)
      positions <- Gen.listOfN(numberOfPositions, recordedPosition)
    } yield toNewPortfolioUpdates(NonEmptySeq.fromSeqUnsafe(positions))
  }

  def recordedPositions(maxNumberOfPositions: Int): Gen[Seq[RecordedPosition]] = {
    require(maxNumberOfPositions > 0)
    for {
      numberOfPositions <- Gen.chooseNum(minT = 0, maxT = maxNumberOfPositions)
      positions <- Gen.listOfN(numberOfPositions, recordedPosition)
    } yield positions
  }

  def maybeSomeDeletedEvents(maxNumberOfPositions: Int): Gen[Seq[events.SingleUpdate]] =
    recordedPositions(maxNumberOfPositions).map(positions => positions.map(rp => org.binqua.forex.advisor.newportfolios.events.Deleted(rp.id)))

  def maybeSomeUpdatedEvents(maxNumberOfPositions: Int): Gen[Seq[events.SingleUpdate]] =
    recordedPositions(maxNumberOfPositions).map(positions => positions.map(rp => org.binqua.forex.advisor.newportfolios.events.Updated(rp)))

  def portfolioCreatedEvent(minNumberOfPositions: Int): Gen[PortfoliosModel.events.PortfolioCreated] =
    for {
      name <- portfolioName
      numberOfPositions <- Gen.chooseNum(minNumberOfPositions, 3)
      recordedPositions <- Gen.listOfN(numberOfPositions, PortfoliosGen.recordedPosition)
    } yield PortfoliosModel.events.PortfolioCreated.unsafe(
      name,
      NonEmptySeq.fromSeqUnsafe(recordedPositions)
    )

  def newPortfolioCreatedEvent(minNumberOfPositions: Int): Gen[org.binqua.forex.advisor.newportfolios.events.PortfolioCreated] =
    for {
      idempotentKey <- idempotentKey
      commandHash <- Gen.chooseNum(Int.MinValue, Int.MaxValue)
      name <- portfolioName
      numberOfPositions <- Gen.chooseNum(minNumberOfPositions, maxT = minNumberOfPositions + 3)
      recordedPositions <- Gen.listOfN(numberOfPositions, PortfoliosGen.recordedPosition)
    } yield {
      org.binqua.forex.advisor.newportfolios.events.PortfolioCreated.unsafe(
        idempotentKey,
        commandHash,
        name,
        NonEmptySeq.fromSeqUnsafe(recordedPositions)
      )
    }

  val dateGen: Gen[LocalDateTime] = for {
    year <- Gen.chooseNum(2018, 2050)
    month <- Gen.chooseNum(1, 12)
    minutes <- Gen.chooseNum(1, 59)
  } yield LocalDateTime.of(year, month, 1, 10, minutes, 0)

  val lotSizeGen: Gen[LotSize] = for {
    amount <- Gen.chooseNum(1, 100)
    index <- Gen.oneOf(1, 2, 3)
  } yield index match {
    case 1 => StandardLot(amount)
    case 2 => MiniLot(amount)
    case 3 => MicroLot(amount)
  }

  val portfolioNames: Gen[String] = for {
    p <- Gen.alphaNumChar
    s <- Gen.alphaNumStr
  } yield {
    s"$p${if (s.length > 30) s.substring(0, 30) else s}"
  }

  val soldOrBoughtGen: Gen[PositionType] = Gen.oneOf(Sell, Buy)

  def aPositionWithGivenPair(currencyPair: CurrencyPair): Gen[RecordedPosition] = positionWithPairsChosenFrom(List(currencyPair))

  def aRecordPositionWithPriceALittleBitDifferentFrom(recordedPosition: RecordedPosition): Gen[RecordedPosition] =
    for {
      position <- positionWithPairsChosenFrom(List(recordedPosition.data.tradingPair.currencyPair))
      distanceInPips <- Gen.chooseNum(minT = -100, maxT = 100)
      newPosition <- TradingPair.priceIncreasedByPipUnits(position.data.tradingPair, distanceInPips) match {
        case Right(newTradingPair) =>
          val positionData: Lens[RecordedPosition, Position] = GenLens[RecordedPosition](_.data)
          val tradingPair: Lens[Position, TradingPair] = GenLens[Position](_.tradingPair)
          Gen.const((positionData composeLens tradingPair).modify(_ => newTradingPair)(position))
        case Left(_) => Gen.fail[RecordedPosition]
      }
    } yield newPosition

  def positionWithPairsChosenFrom(currencyPairs: Iterable[CurrencyPair]): Gen[RecordedPosition] =
    for {
      soldOrBought <- soldOrBoughtGen
      tradingPair <- TradingPairGen.tradingPairsOneOf(Gen.oneOf(currencyPairs))
      lotSize <- lotSizeGen
      transactionDateTime <- dateGen
      recordedDateTime <- dateGen
    } yield generateARecordedPositionWith(
      soldOrBought,
      tradingPair,
      lotSize,
      transactionDateTime,
      recordedDateTime
    )

  def aTradingPairWithNewPrice(recordedPosition: RecordedPosition, distanceInPips: Int): Either[String, TradingPair] = {
    val positionData: Lens[RecordedPosition, Position] = GenLens[RecordedPosition](_.data)
    val tradingPair: Lens[Position, TradingPair] = GenLens[Position](_.tradingPair)
    TradingPair.priceIncreasedByPipUnits((positionData composeLens tradingPair).get(recordedPosition), distanceInPips)
  }

  def positionRelatedToGivenPair(recordedPosition: Gen[RecordedPosition]): Gen[RecordedPosition] =
    for {
      soldOrBought <- soldOrBoughtGen
      distanceInPips <- Gen.chooseNum(-200, 200)
      givenRecordedPosition <- recordedPosition
      lotSize <- lotSizeGen
      transactionDateTime <- dateGen
      recordedDateTime <- dateGen
      newTradingPair <- aTradingPairWithNewPrice(givenRecordedPosition, distanceInPips) match {
        case Right(value) => Gen.const(value)
        case Left(_)      => Gen.fail[TradingPair]
      }
    } yield {
      generateARecordedPositionWith(
        soldOrBought,
        newTradingPair,
        lotSize,
        transactionDateTime,
        recordedDateTime
      )
    }

  val recordedPosition: Gen[RecordedPosition] = positionWithPairsChosenFrom(CurrencyPair.values)

  val recordedPositions: Gen[NonEmptySeq[RecordedPosition]] = for {
    numberOfPositions <- Gen.chooseNum(1, 6)
    positions <- Gen.listOfN(numberOfPositions, recordedPosition)
  } yield NonEmptySeq.fromSeqUnsafe(positions)

  val position: Gen[Position] = for {
    position <- recordedPosition
  } yield Position(
    position.data.positionType,
    position.data.tradingPair,
    position.data.lotSize,
    position.data.transactionDateTime,
    position.data.recordedDateTime
  )

  val updatePortfolio: Gen[UpdatePortfolio] = {
    val add: Gen[Add] = position.map(Add)
    val delete: Gen[Delete] = Gen.uuid.map(Delete)
    val update: Gen[Update] = for {
      uuid <- Gen.uuid
      newPosition <- position
    } yield Update(uuid, newPosition)

    val updateGen: Gen[PortfolioSingleUpdate] = Gen.oneOf(
      add,
      delete,
      update
    )

    for {
      idempotentKeyUUID <- Gen.uuid
      name <- portfolioName
      numberOfUpdates <- Gen.chooseNum(1, 4)
      updates <- Gen.listOfN(numberOfUpdates, updateGen)
    } yield {
      import cats.syntax.option._
      UpdatePortfolio.unsafe(
        if (updates.exists(_.isInstanceOf[PortfoliosModel.Add])) IdempotentKey(idempotentKeyUUID).some else None,
        name,
        NonEmptySeq.fromSeqUnsafe(updates)
      )
    }
  }

  def generateARecordedPositionWithId(newId: PositionId): Gen[RecordedPosition] =
    recordedPosition.map(data => data.copy(id = newId))

  private def generateARecordedPositionWith(
      soldOrBought: PortfoliosModel.PositionType,
      pair: TradingPair,
      lotSize: LotSize,
      transactionDateTime: LocalDateTime,
      recordedDateTime: LocalDateTime
  ): RecordedPosition =
    RecordedPosition(
      id = UUID.randomUUID(),
      Position(soldOrBought, pair, lotSize, transactionDateTime, recordedDateTime)
    )

  def newPositionWithMicroLotWithRecordedDateTime(recordedDateTime: LocalDateTime): Gen[Position] =
    position.map(positionData => positionData.copy(lotSize = LotSize.toMicro(positionData.lotSize)).copy(recordedDateTime = recordedDateTime))

}
