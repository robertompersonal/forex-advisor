package org.binqua.forex.advisor.model

import org.binqua.forex.advisor.model.CurrencyPair.{EurGbp, EurJpy, EurUsd, GbpJpy, GbpUsd, Spx500, Us30, UsdJpy}
import org.scalacheck.Gen

object TradingPairGen {

  case class ReferenceData(min: PosBigDecimal, max: PosBigDecimal, currencyPair: CurrencyPair) {
    require(min.value < max.value)
  }

  val min = PosBigDecimal.unsafeFrom(0.2)
  val max = PosBigDecimal.unsafeFrom(50000)

  val tradingPairsReferenceData: Map[CurrencyPair, ReferenceData] = Map(
    EurGbp -> ReferenceData(min, PosBigDecimal.unsafeFrom(1.3), EurGbp),
    EurUsd -> ReferenceData(PosBigDecimal.unsafeFrom(value = 0.5), PosBigDecimal.unsafeFrom(1.8), EurUsd),
    EurJpy -> ReferenceData(PosBigDecimal.unsafeFrom(50), PosBigDecimal.unsafeFrom(200), EurJpy),
    GbpUsd -> ReferenceData(PosBigDecimal.unsafeFrom(1), PosBigDecimal.unsafeFrom(2), GbpUsd),
    GbpJpy -> ReferenceData(PosBigDecimal.unsafeFrom(50), PosBigDecimal.unsafeFrom(300), GbpJpy),
    Spx500 -> ReferenceData(PosBigDecimal.unsafeFrom(1000), PosBigDecimal.unsafeFrom(5000), Spx500),
    UsdJpy -> ReferenceData(PosBigDecimal.unsafeFrom(30), PosBigDecimal.unsafeFrom(300), UsdJpy),
    Us30 -> ReferenceData(PosBigDecimal.unsafeFrom(10000), max, Us30)
  )

  val currencyPairReferenceDataGen: Gen[(CurrencyPair, ReferenceData)] = Gen.oneOf(tradingPairsReferenceData.toList)

  val currencyPair: Gen[CurrencyPair] = Gen.oneOf(tradingPairsReferenceData.keys)

  val tradingPairs: Gen[TradingPair] = tradingPairsOneOf(currencyPair)

  val EurGbpGen: Gen[TradingPair] = tradingPairsOf(EurGbp)

  val EurUsdGen: Gen[TradingPair] = tradingPairsOf(EurUsd)

  val GbpUsdGen: Gen[TradingPair] = tradingPairsOf(GbpUsd)

  def currencyPairBy(wantedCurrencyPair: CurrencyPair => Boolean): Gen[CurrencyPair] = Gen.oneOf(tradingPairsReferenceData.keys.filter(wantedCurrencyPair))

  def someCurrencyPairsBy(wantedCurrencyPair: CurrencyPair => Boolean): Gen[List[CurrencyPair]] =
    Gen.const(
      tradingPairsReferenceData.keys
        .filter(wantedCurrencyPair)
        .foldLeft(List[CurrencyPair]())((acc, e) => {
          if (Gen.oneOf(true, false).sample.get) acc
          else acc.:+(e)
        })
    )

  def tradingPairsOf(currencyPair: CurrencyPair): Gen[TradingPair] = tradingPairsOneOf(Gen.const(currencyPair))

  def tradingPairsOneOf(currencyPairs: Gen[CurrencyPair]): Gen[TradingPair] = {
    for {
      currencyPair <- currencyPairs
      ReferenceData(minIntegerPart, maxIntegerPart, currencyPair) <- Gen.const(tradingPairsReferenceData(currencyPair))
      (price, _) <- PosBigDecimalGen.withRange(minIntegerPart, maxIntegerPart)(Gen.const(currencyPair.quoteCurrency.scale))
      maybeATradingPair <- TradingPair.maybeATradingPair(currencyPair, price) match {
        case Right(value) => Gen.const(value)
        case Left(_)      => Gen.fail
      }
    } yield maybeATradingPair
  }

}
