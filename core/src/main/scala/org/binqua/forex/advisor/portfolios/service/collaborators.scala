package org.binqua.forex.advisor.portfolios.service

import akka.actor.typed.ActorRef
import cats.Eq
import cats.data.Validated
import cats.implicits._
import com.typesafe.config.{Config => AkkaConfig}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.{NonNegative, Positive}
import eu.timepit.refined.refineV
import org.binqua.forex.advisor.newportfolios.{DeliveryGuaranteedPortfolios, PortfoliosServiceModel}
import org.binqua.forex.advisor.portfolios.service.Config.{MaxInFlightMessages, RetryAttempts}
import org.binqua.forex.advisor.portfolios.service.PortfoliosService.Response
import org.binqua.forex.advisor.util.Model
import org.binqua.forex.util.core.{ConfigValidator, UnsafeConfigReader}
import org.binqua.forex.util.{Bug, ValidateConfigByReference, core}

import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success, Try}

object InFlightCommandRecord {

  def increaseNumberOfRetriesCounterOf(old: InFlightCommandRecord): InFlightCommandRecord =
    old.copy(numberOfRetriesStarted = {
      refineV[NonNegative](old.numberOfRetriesStarted.value + 1) match {
        case Left(error)  => throw new Bug(error)
        case Right(value) => value
      }
    })
}

final case class InFlightCommandRecord(
    command: DeliveryGuaranteedPortfolios.ExternalCommand,
    replyTo: ActorRef[Response],
    shardedEntityId: Model.ShardedEntityId,
    numberOfRetriesStarted: PortfoliosServiceModel.NumberOfRetries
) {

  import PortfoliosServiceModel._

  def maxRetriesReached(maxRetryAttempts: NumberOfRetries): Boolean = Eq.eqv(this.numberOfRetriesStarted, maxRetryAttempts)
}

trait SupportMessagesFactory {
  def newSupportMessages(config: Config): SupportMessages
}

trait SupportMessages {
  def maxInFlightMessagesReached(): String

  def maxRetriesReached(inFlightCommand: InFlightCommandRecord): String

  def messageConfirmed(inFlightCommandRecord: InFlightCommandRecord): String

  def retryIntervalExpired(inFlightCommand: InFlightCommandRecord): String
}

private[service] object DefaultSupportMessagesFactory extends SupportMessagesFactory {
  override def newSupportMessages(config: Config): SupportMessages = new DefaultSupportMessages(config)
}

private[service] class DefaultSupportMessages(config: Config) extends SupportMessages {
  override def messageConfirmed(inFlightCommandRecord: InFlightCommandRecord): String =
    s"message with id ${inFlightCommandRecord.command.messageId.id} confirmed after ${inFlightCommandRecord.numberOfRetriesStarted} retries"

  override def retryIntervalExpired(inFlightCommandRecord: InFlightCommandRecord): String =
    s"after ${config.retryIntervalInMillis} I did not received a confirmation message for message with id ${inFlightCommandRecord.command.messageId.id}. I am going to retry now: retry number ${inFlightCommandRecord.numberOfRetriesStarted} of ${config.maxRetryAttempts}"

  override def maxRetriesReached(inFlightCommandRecord: InFlightCommandRecord): String =
    s"${config.maxRetryAttempts.value} max retries reached for message with id ${inFlightCommandRecord.command.messageId.id}. Message has not be confirmed and I am going to stop retrying"

  override def maxInFlightMessagesReached(): String =
    s"${config.maxInFlightMessages.value} max inflight messages reached. Going to reject extra messages until inflight messages number is below ${config.maxInFlightMessages.value}"

}

object unsafeConfigReader extends UnsafeConfigReader[Config] {
  override def apply(akkaConfig: AkkaConfig): Config = core.makeItUnsafe(ConfigValidator(akkaConfig))
}

sealed abstract case class Config(retryIntervalInMillis: FiniteDuration, maxRetryAttempts: RetryAttempts, maxInFlightMessages: MaxInFlightMessages)

object Config {

  type RetryAttempts = Int Refined NonNegative

  type MaxInFlightMessages = Int Refined Positive

  def safe(retryInterval: FiniteDuration, maxRetryAttempts: RetryAttempts, maxInFlightMessages: MaxInFlightMessages): Config =
    new Config(retryInterval, maxRetryAttempts, maxInFlightMessages) {}

}

object ConfigValidator extends ConfigValidator[Config] {

  override def apply(akkaConfig: AkkaConfig): Validated[List[String], Config] = {

    val List(retryIntervalKey, maxRetryAttemptsKey, maxInFlightMessages) = buildConfigKeys(List("retryInterval", "maxRetryAttempts", "maxInFlightMessages"))

    def toValidated[A](arg: Either[String, A]): Validated[List[String], A] = arg.leftMap(List(_)).toValidated

    def nonNegativeValueOf(key: String): Either[String, RetryAttempts] = {
      Try(akkaConfig.getInt(key)) match {
        case Success(value) =>
          refineV[NonNegative](value).leftMap(_ => s"$key has to be >= 0. $value is not valid.")
        case Failure(_) => Left(s"$key has to exist and has to be an integer >= 0.")
      }
    }

    def positiveValueOf(key: String): Either[String, MaxInFlightMessages] = {
      Try(akkaConfig.getInt(key)) match {
        case Success(value) =>
          refineV[Positive](value).leftMap(_ => s"$key has to be > 0. $value is not valid.")
        case Failure(_) => Left(s"$key has to exist and has to be an integer > 0.")
      }
    }

    (
      ValidateConfigByReference.validateADurationConfigValue(akkaConfig, retryIntervalKey),
      toValidated(nonNegativeValueOf(maxRetryAttemptsKey)),
      toValidated(positiveValueOf(maxInFlightMessages))
    ).mapN((retryInterval, maxRetryAttempts, maxInFlightMessages) => Config.safe(retryInterval, maxRetryAttempts, maxInFlightMessages))

  }

  private def buildConfigKeys(keys: List[String]): List[String] =
    keys.map(x => s"org.binqua.forex.advisor.portfolios.service.$x")
}
