package org.binqua.forex.advisor.newportfolios

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import cats.data.{NonEmptySeq, NonEmptySet}
import cats.syntax.option._
import cats.{Eq, Foldable, data}
import com.fasterxml.jackson.annotation.{JsonSubTypes, JsonTypeName}
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonPOJOBuilder}
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.{PositionIdFactory, ShowPortfoliosPayload}
import org.binqua.forex.advisor.newportfolios.events.{Event, PortfolioCreated, PortfolioUpdated, PortfoliosDeleted, _}
import org.binqua.forex.advisor.portfolios.PortfoliosModel._
import org.binqua.forex.advisor.portfolios.service.{IdempotentKey, MessageId}
import org.binqua.forex.util.Bug
import org.binqua.forex.util.core.{UnsafeConfigReader, checkThat}

import scala.collection.Iterable
import scala.collection.immutable.TreeSet
import scala.util.Either

object DeliveryGuaranteedPortfolios {

  def apply(entityId: PersistenceId)(implicit
      positionIdFactory: PositionIdFactory,
      supportMessages: SupportMessages,
      errorMessagesFactory: ErrorMessagesFactory,
      configReader: UnsafeConfigReader[Config],
      hasher: Hasher
  ): Behavior[Command] =
    Behaviors.withTimers(implicit timers => {
      Behaviors.setup { context =>
        implicit val config: Config = configReader(context.system.settings.config)
        implicit val errorMessages = errorMessagesFactory.newErrorMessages(config)

        timers.startTimerWithFixedDelay(key = "DeleteIdempotentEntries", msg = DeleteIdempotentEntries, delay = config.garbageCollectorInterval)

        EventSourcedBehavior[Command, Event, State](
          persistenceId = entityId,
          emptyState = State.empty,
          commandHandler = commandHandler,
          eventHandler = eventHandler(context)
        )

      }
    })

  type PositionIdFactory = () => PositionId

  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[CreatePortfolioPayload], name = "createPortfolio"),
      new JsonSubTypes.Type(value = classOf[DeletePortfoliosPayload], name = "deletePortfolio"),
      new JsonSubTypes.Type(value = classOf[ShowPortfoliosPayload], name = "showPortfolios"),
      new JsonSubTypes.Type(value = classOf[UpdatePortfolioPayload], name = "updatePortfolio")
    )
  )
  sealed trait Payload extends org.binqua.forex.JsonSerializable

  sealed trait Command extends org.binqua.forex.JsonSerializable

  private[newportfolios] sealed trait InternalCommand extends Command

  private[newportfolios] object DeleteIdempotentEntries extends InternalCommand

  @JsonTypeName("command")
  final case class ExternalCommand(messageId: MessageId, payload: Payload, replyTo: ActorRef[Response]) extends Command

  final case class CreatePortfolioPayload(portfolioToBeCreated: CreatePortfolio) extends Payload

  @JsonDeserialize(builder = classOf[DeletePortfoliosPayload.BuilderForJsonDeserialize])
  @JsonTypeName("deletePortfolio")
  final case class DeletePortfoliosPayload(portfolioNames: NonEmptySet[PortfolioName]) extends Payload

  object DeletePortfoliosPayload {
    @JsonPOJOBuilder
    case class BuilderForJsonDeserialize(portfolioNames: List[Map[String, String]]) {
      def build(): DeletePortfoliosPayload =
        DeletePortfoliosPayload(data.NonEmptySet.fromSetUnsafe(TreeSet.from(portfolioNames.map(mapEntry => mapEntry.head._2).map(PortfolioName.unsafe).toSet)))
    }

  }

  final case class UpdatePortfolioPayload(updatePortfolio: UpdatePortfolio) extends Payload

  @JsonDeserialize(using = classOf[ShowPortfoliosPayloadDeserializer])
  trait ShowPortfoliosPayload extends Payload

  @JsonTypeName("showPortfolios")
  object ShowPortfoliosPayload extends ShowPortfoliosPayload

  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[Success], name = "success"),
      new JsonSubTypes.Type(value = classOf[Error], name = "error")
    )
  )
  sealed trait ResponseType extends org.binqua.forex.JsonSerializable

  final case class Success(portfolioSummary: PortfoliosSummary) extends ResponseType

  final case class Error(reason: String) extends ResponseType

  @JsonTypeName("response")
  final case class Response(retryId: MessageId, responseType: ResponseType) extends org.binqua.forex.JsonSerializable

  def updatePortfolioValidation(update: UpdatePortfolio, state: State)(implicit
      errorMessages: ErrorMessages,
      config: Config,
      hasher: Hasher,
      positionIdFactory: PositionIdFactory
  ): Either[String, PortfolioUpdated] =
    for {
      portfolio <- portfolioUpdates.checks.portfolioExist(state, update, errorMessages)
      _ <- portfolioUpdates.checks.allPositionIdsExist(update.updatesToBeExecuted, portfolio.positions.map(_.id), errorMessages)
      _ <- portfolioUpdates.checks.noTooManyPositions(update, portfolio.positions, errorMessages, config)
      portfolioUpdated <- portfolioUpdates.commandToUpdatedEvent(update, hasher, positionIdFactory)
    } yield portfolioUpdated

  def updatePortfolioHandlerWithIdempotentKey(cmd: ExternalCommand, actualState: State, updatePortfolio: UpdatePortfolio, idempotentKey: IdempotentKey)(implicit
      positionIdFactory: PositionIdFactory,
      supportMessages: SupportMessages,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      config: Config
  ): Effect[Event, State] =
    actualState.hasBeenAlreadyApplied(idempotentKey) match {
      case Some(alreadyAppliedCommandHash) =>
        Effect.none.thenReply(cmd.replyTo)((_: State) =>
          Response(
            cmd.messageId,
            calculateResponseType(
              alreadyAppliedCommandHash,
              actualState,
              actualPayload = updatePortfolio,
              hasher = hasher,
              () => supportMessages.idempotentKeyMismatch(updatePortfolio)
            )
          )
        )
      case None =>
        tryToUpdatePortfolio(cmd, actualState, updatePortfolio)
    }

  def updatePortfolioHandlerWithoutIdempotentKey(cmd: ExternalCommand, actualState: State, updatePortfolio: UpdatePortfolio)(implicit
      positionIdFactory: PositionIdFactory,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      config: Config
  ): Effect[Event, State] = tryToUpdatePortfolio(cmd, actualState, updatePortfolio)

  def tryToUpdatePortfolio(cmd: ExternalCommand, actualState: State, updatePortfolio: UpdatePortfolio)(implicit
      positionIdFactory: PositionIdFactory,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      config: Config
  ): Effect[Event, State] =
    updatePortfolioValidation(updatePortfolio, actualState) match {
      case Left(error) => Effect.none.thenReply[Response](cmd.replyTo)((_: State) => Response(cmd.messageId, Error(error)))
      case Right(portfolioUpdated) =>
        Effect.persist(portfolioUpdated).thenReply(cmd.replyTo)((state: State) => Response(cmd.messageId, Success(state.portfoliosSummary)))
    }

  def updatePortfolioHandler(command: ExternalCommand, actualState: State, updatePortfolio: UpdatePortfolio)(implicit
      positionIdFactory: PositionIdFactory,
      supportMessages: SupportMessages,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      config: Config
  ): Effect[Event, State] =
    updatePortfolio.maybeAnIdempotentKey match {
      case Some(idempotentKey) =>
        updatePortfolioHandlerWithIdempotentKey(
          command,
          actualState,
          updatePortfolio,
          idempotentKey
        )
      case None =>
        updatePortfolioHandlerWithoutIdempotentKey(command, actualState, updatePortfolio)
    }

  def deleteIdempotentEntriesHandler(state: State): Effect[Event, State] = Effect.persist(OldIdempotentEntriesDeleted).thenNoReply()

  def commandHandler(implicit
      config: Config,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      positionIdFactory: PositionIdFactory,
      supportMessages: SupportMessages
  ): (State, Command) => Effect[Event, State] = { (state, cmd) =>
    cmd match {
      case extCmd @ ExternalCommand(_, payload, _) =>
        payload match {
          case CreatePortfolioPayload(createPortfolio) => createPortfolioHandler(createPortfolio, state, extCmd)
          case DeletePortfoliosPayload(portfolioNames) => deletePortfolioHandler(portfolioNames, extCmd)
          case _: ShowPortfoliosPayload                => showPortfolioHandler(state, extCmd)
          case UpdatePortfolioPayload(updatePortfolio) => updatePortfolioHandler(extCmd, state, updatePortfolio)
        }
      case DeleteIdempotentEntries => deleteIdempotentEntriesHandler(state)
    }
  }

  def eventHandler[T](context: ActorContext[T]): (State, Event) => State = { (state, incomingEvent) =>
    state.updated(incomingEvent) match {
      case Right(detailedState) =>
        val (details, state) = detailedState.run
        if (details.nonEmpty) context.log.info(details)
        state
      case Left(error) => throw new Bug(error)
    }
  }

  private def deletePortfolioHandler(portfolioNames: data.NonEmptySet[PortfolioName], command: ExternalCommand): Effect[PortfoliosDeleted, State] =
    Effect
      .persist(PortfoliosDeleted(portfolioNames))
      .thenReply(command.replyTo)((state: State) => Response(command.messageId, Success(state.portfoliosSummary)))

  private def showPortfolioHandler(state: State, command: ExternalCommand): Effect[PortfolioCreated, State] =
    Effect.none.thenRun(_ => command.replyTo ! Response(command.messageId, Success(state.portfoliosSummary)))

  def createPortfolioHandler(actualCreatePortfolio: CreatePortfolio, actualState: State, cmd: ExternalCommand)(implicit
      config: Config,
      errorMessages: ErrorMessages,
      hasher: Hasher,
      positionIdFactory: PositionIdFactory,
      supportMessages: SupportMessages
  ): Effect[PortfolioCreated, State] =
    actualState.hasBeenAlreadyApplied(actualCreatePortfolio.idempotentKey) match {
      case Some(alreadyAppliedCommandHash) =>
        Effect.none.thenReply(cmd.replyTo)((state: State) =>
          Response(
            cmd.messageId,
            calculateResponseType(
              alreadyAppliedCommandHash,
              state,
              actualPayload = actualCreatePortfolio,
              hasher = hasher,
              message = () => supportMessages.idempotentKeyMismatch(actualCreatePortfolio)
            )
          )
        )
      case None =>
        portfolioCreationChecks.createPortfolioCmdValidation(config, actualState, errorMessages, actualCreatePortfolio, hasher, positionIdFactory) match {
          case Left(error) => Effect.none.thenReply(cmd.replyTo)((_: State) => Response(cmd.messageId, Error(error)))
          case Right(portfolioCreated) =>
            Effect.persist(portfolioCreated).thenReply(cmd.replyTo)((state: State) => Response(cmd.messageId, Success(state.portfoliosSummary)))
        }
    }

  def calculateResponseType(
      alreadyAppliedCreatePortfolioHash: Int,
      actualState: State,
      actualPayload: Product,
      hasher: Hasher,
      message: () => String
  ): ResponseType =
    if (Eq[Int].eqv(hasher.hash(actualPayload), alreadyAppliedCreatePortfolioHash))
      Success(actualState.portfoliosSummary)
    else
      Error(message())

}

class ShowPortfoliosPayloadDeserializer extends StdDeserializer[ShowPortfoliosPayload](ShowPortfoliosPayload.getClass) {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): ShowPortfoliosPayload = ShowPortfoliosPayload
}

object portfolioUpdates {
  object checks {
    def portfolioExist(state: State, update: UpdatePortfolio, errorMessages: ErrorMessages): Either[String, Portfolio] =
      state
        .findPortfolioByName(update.portfolioName)
        .toRight(errorMessages.portfolioDoesNotExist(update))

    def noTooManyPositions(
        update: UpdatePortfolio,
        positionsAlreadyRecorded: Set[RecordedPosition],
        errorMessages: ErrorMessages,
        config: Config
    ): Either[String, Boolean] = {
      val newNumberOfPositions = update.updatesToBeExecuted.filter(_.isInstanceOf[Add]).length + positionsAlreadyRecorded.size
      checkThat(
        ifTrueRight = newNumberOfPositions <= config.maxNumberOfPositions,
        otherwiseLeft = errorMessages.tooManyPositionsForASinglePortfolio(update.portfolioName, newNumberOfPositions)
      )
    }

    def allPositionIdsExist(
        updates: NonEmptySeq[PortfolioSingleUpdate],
        existingPositionIds: Iterable[PositionId],
        errorMessages: ErrorMessages
    ): Either[String, Boolean] = {
      val nonExistingPositionIds = Foldable[Seq]
        .foldLeft(updates.toSeq, List[PositionId]())((uuids, update) =>
          update match {
            case _: Add    => uuids
            case _: Delete => uuids
            case Update(positionIdsToBeUpdated, _) =>
              if (existingPositionIds.exists(Eq[PositionId].eqv(_, positionIdsToBeUpdated))) uuids
              else positionIdsToBeUpdated :: uuids
          }
        )
      checkThat(ifTrueRight = nonExistingPositionIds.isEmpty, otherwiseLeft = errorMessages.positionsToBeUpdatedDoesNotExist(nonExistingPositionIds))
    }
  }

  def commandToUpdatedEvent(
      updatePortfolio: UpdatePortfolio,
      hasher: Hasher,
      positionIdFactory: PositionIdFactory
  ): Either[String, PortfolioUpdated] =
    PortfolioUpdated.validated(
      updatePortfolio.maybeAnIdempotentKey,
      if (updatePortfolio.maybeAnIdempotentKey.nonEmpty) hasher.hash(updatePortfolio).some else None,
      portfolioName = updatePortfolio.portfolioName,
      updates = updatePortfolio.updatesToBeExecuted.map({
        case Add(incomingPosition)                   => Added(RecordedPosition(positionIdFactory(), incomingPosition))
        case Update(thePositionId, incomingPosition) => Updated(RecordedPosition(thePositionId, incomingPosition))
        case Delete(thePositionId)                   => Deleted(thePositionId)
      })
    )

}

object portfolioCreationChecks {
  def commandToCreateEvent(actualCreatePortfolio: CreatePortfolio, hasher: Hasher, positionIdFactory: PositionIdFactory): Either[String, PortfolioCreated] =
    PortfolioCreated.validated(
      actualCreatePortfolio.idempotentKey,
      hasher.hash(actualCreatePortfolio),
      actualCreatePortfolio.portfolioName,
      recordedPositions = actualCreatePortfolio.positions.map(RecordedPosition(positionIdFactory(), _))
    )

  def createPortfolioCmdValidation(
      config: Config,
      actualState: State,
      errorMessages: ErrorMessages,
      actualCreatePortfolio: CreatePortfolio,
      hasher: Hasher,
      positionIdFactory: PositionIdFactory
  ): Either[String, PortfolioCreated] =
    for {
      _ <- portfolioCreationChecks.noTooManyPortfolios(actualState, config, errorMessages)
      _ <- portfolioCreationChecks.noDuplicatedPortfolioNames(actualState, errorMessages, actualCreatePortfolio)
      _ <- portfolioCreationChecks.noTooManyPositions(config, errorMessages, actualCreatePortfolio)
      portfolioCreated <- portfolioCreationChecks.commandToCreateEvent(actualCreatePortfolio, hasher, positionIdFactory)
    } yield portfolioCreated

  def noTooManyPortfolios(actualState: State, config: Config, errorMessages: ErrorMessages): Either[String, Boolean] =
    checkThat(actualState.portfoliosSummary.size < config.maxNumberOfPortfolios, errorMessages.maxNumberOfPortfoliosReached())

  def noDuplicatedPortfolioNames(actualState: State, errorMessages: ErrorMessages, actualCreatePortfolio: CreatePortfolio): Either[String, Boolean] =
    checkThat(
      actualState.findPortfolioByName(actualCreatePortfolio.portfolioName).isEmpty,
      errorMessages.portfolioAlreadyExist(actualCreatePortfolio.portfolioName)
    )

  def noTooManyPositions(config: Config, errorMessages: ErrorMessages, actualCreatePortfolio: CreatePortfolio): Either[String, Boolean] =
    checkThat(
      ifTrueRight = actualCreatePortfolio.positions.length <= config.maxNumberOfPositions,
      otherwiseLeft = errorMessages.tooManyPositionsForASinglePortfolio(actualCreatePortfolio.portfolioName, actualCreatePortfolio.positions.length)
    )

}
