package org.binqua.forex.advisor.portfolios
import cats.Eq
import cats.data.Validated.{Invalid, Valid}
import cats.data.{NonEmptySeq, Validated}
import cats.implicits.toShow
import cats.kernel.Order
import cats.syntax.either._
import com.fasterxml.jackson.annotation.{JsonIgnore, JsonSubTypes}
import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonPOJOBuilder, JsonSerialize}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.{MaxSize, MinSize}
import eu.timepit.refined.string.Trimmed
import org.binqua.forex.advisor.model.{CurrencyPair, LotSize, PosBigDecimal, TradingPair}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName.PortfolioNameType
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition.jsonPaths._
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition.jsonReads._
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.binqua.forex.util.core.makeItUnsafe
import org.binqua.forex.util.{Validation, core}
import org.scalactic.Equality
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads, Writes, _}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID
import scala.collection.immutable.Seq
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

object PortfoliosModel {

  type PositionId = UUID

  implicit val positionIdEq = new Eq[PositionId] {
    override def eqv(x: PositionId, y: PositionId): Boolean = x.equals(y)
  }

  final case class Position(
      positionType: PositionType,
      tradingPair: TradingPair,
      lotSize: LotSize,
      transactionDateTime: java.time.LocalDateTime,
      recordedDateTime: java.time.LocalDateTime
  )

  final case class RecordedPosition(id: PositionId, data: Position) {

    override def toString: String = asString(data)

    private def asString(data: Position) =
      s"id $id ${if (data.positionType.isABuyPosition) "Bought"
      else "Sold"} ${data.lotSize.amount} ${data.lotSize.name} of ${data.tradingPair.currencyPair} at ${data.tradingPair.price} ${DateTimeFormatter
        .ofPattern("dd-MMM-yyyy HH:mm:ss")
        .format(data.transactionDateTime)}"

  }

  object RecordedPosition {

    object jsonPaths {
      val amount: JsPath = JsPath \ "amount"
      val buy: JsPath = JsPath \ "buy"
      val id: JsPath = JsPath \ "id"
      val openDateTime: JsPath = JsPath \ "openDateTime"
      val pair: JsPath = JsPath \ "pair"
      val price: JsPath = JsPath \ "price"
      val recordedDateTime: JsPath = JsPath \ "recordedDateTime"
      val size: JsPath = JsPath \ "size"
    }

    object jsonReads {

      def dateTime(path: JsPath): Reads[java.time.LocalDateTime] =
        path
          .read[String]
          .flatMap(maybeAValidDateTime =>
            Try(LocalDateTime.parse(maybeAValidDateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME)) match {
              case Success(value) => (_: JsValue) => JsSuccess(value, path)
              case Failure(_)     => cannotParseReadsError[LocalDateTime](path, details = s"$maybeAValidDateTime is wrong")
            }
          )

      val openDateTime: Reads[java.time.LocalDateTime] = dateTime(jsonPaths.openDateTime)

      val recordedDateTime: Reads[java.time.LocalDateTime] = dateTime(jsonPaths.recordedDateTime)

      implicit val id: Reads[UUID] = {
        jsonPaths.id
          .read[String]
          .flatMap(id =>
            Try(UUID.fromString(id)) match {
              case Success(value) => (_: JsValue) => JsSuccess(value)
              case Failure(_)     => cannotParseReadsError[UUID](jsonPaths.id, details = s"$id is wrong")
            }
          )
      }

      def cannotParseReadsError[T](path: JsPath, details: String)(implicit tag: ClassTag[T]): Reads[T] =
        cannotParseReadsError(List(path), details)

      def cannotParseReadsError[T](paths: List[JsPath], details: String)(implicit tag: ClassTag[T]): Reads[T] =
        (_: JsValue) =>
          JsError(JsonValidationError(s"Cannot parse json path ${paths.mkString(" , ")} into a valid ${tag.runtimeClass.getSimpleName}. $details"))

      implicit val price: Reads[PosBigDecimal] = {
        jsonPaths.price
          .read[String]
          .flatMap(PosBigDecimal.maybeAPosBigDecimal(_) match {
            case Right(value) => (_: JsValue) => JsSuccess(value)
            case Left(error)  => cannotParseReadsError[PosBigDecimal](jsonPaths.price, details = error)
          })
      }

      def pair(allValidExternalIdentifiers: Map[String, CurrencyPair]): Reads[CurrencyPair] = {
        jsonPaths.pair
          .read[String]
          .flatMap(CurrencyPair.maybeACurrencyPair(_, allValidExternalIdentifiers) match {
            case Right(value) => (_: JsValue) => JsSuccess(value)
            case Left(error)  => cannotParseReadsError[CurrencyPair](jsonPaths.pair, details = error)
          })
      }

      implicit val pair: Reads[CurrencyPair] = pair(CurrencyPair.fromExternalIdentifierMapping)

      implicit val tradingPair: Reads[TradingPair] = {
        (pair and price)((pair, price) => TradingPair.maybeATradingPair(pair, price)).flatMap {
          case Right(value) => (_: JsValue) => JsSuccess(value)
          case Left(theErrorReason: String) =>
            cannotParseReadsError[TradingPair](List(jsonPaths.pair, jsonPaths.price), details = theErrorReason.show.show)
        }
      }

      implicit val lotSize: Reads[LotSize] = {
        (amount.read[Int] and size.read[String])((amount, sizeId) => LotSize.maybeALotSize(sizeId, amount)).flatMap {
          case Valid(value)    => (_: JsValue) => JsSuccess(value)
          case Invalid(errors) => cannotParseReadsError[LotSize](List(jsonPaths.amount, jsonPaths.size), details = errors.mkString("\n"))

        }
      }

      implicit val recordedPosition: Reads[RecordedPosition] = (buy.read[Boolean] and
        id and
        openDateTime and
        tradingPair and
        recordedDateTime and
        lotSize)((buyOrSell, id, openDateTime, tradingPair, recordedDateTime, lotSize) =>
        RecordedPosition(
          id,
          Position(
            if (buyOrSell) Buy else Sell,
            tradingPair,
            lotSize,
            openDateTime,
            recordedDateTime
          )
        )
      )
    }

    implicit val jsonWrites: Writes[RecordedPosition] = {
      (jsonPaths.amount.write[Int] and
        jsonPaths.buy.write[Boolean] and
        jsonPaths.id.write[String] and
        jsonPaths.openDateTime.write[String] and
        jsonPaths.pair.write[String] and
        jsonPaths.price.write[String] and
        jsonPaths.recordedDateTime.write[String] and
        jsonPaths.size.write[String])((recordedData: RecordedPosition) =>
        (
          recordedData.data.lotSize.amount,
          recordedData.data.positionType.isABuyPosition,
          recordedData.id.toString,
          recordedData.data.transactionDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
          CurrencyPair.toExternalIdentifier(recordedData.data.tradingPair.currencyPair),
          recordedData.data.tradingPair.price.value.toString(),
          recordedData.data.recordedDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
          recordedData.data.lotSize.id
        )
      )
    }

  }

  @JsonDeserialize(using = classOf[PositionType.PositionTypeDeserializer])
  @JsonSerialize(using = classOf[PositionType.PositionTypeSerializer])
  sealed trait PositionType {
    val id: String
    val isASellPosition: Boolean

    def isABuyPosition: Boolean = !isASellPosition
  }

  object PositionType {
    def unsafe(value: String): PositionType =
      value match {
        case "buy"         => Buy
        case "sell"        => Sell
        case somethingElse => throw new IllegalArgumentException(s"only buy or sell are accepted for positionType: $somethingElse is not valid")
      }

    class PositionTypeSerializer extends JsonSerializer[PositionType] {
      override def serialize(positionType: PositionType, gen: JsonGenerator, serializers: SerializerProvider): Unit = gen.writeString(positionType.id)
    }

    class PositionTypeDeserializer extends JsonDeserializer[PositionType] {
      override def deserialize(p: JsonParser, ctxt: DeserializationContext): PositionType = {
        val node: JsonNode = p.getCodec.readTree(p)
        PositionType.unsafe(node.asText())
      }
    }

  }

  final case object Buy extends PositionType {
    override val id: String = "buy"
    override val isASellPosition: Boolean = false
  }

  final case object Sell extends PositionType {
    override val id: String = "sell"
    override val isASellPosition: Boolean = true
  }

  val byNameOrdering: Ordering[Portfolio] = (firstPortfolio: Portfolio, secondPortfolio: Portfolio) =>
    firstPortfolio.portfolioName.name.value compare secondPortfolio.portfolioName.name.value

  object Portfolio {

    implicit val portfolioOrdering: Ordering[Portfolio] = (x: Portfolio, y: Portfolio) => Ordering[PortfolioName].compare(x.portfolioName, y.portfolioName)

    object paths {
      val name: JsPath = JsPath \ "name"
      val positions: JsPath = JsPath \ "positions"
    }

    implicit val jsonWrites: Writes[Portfolio] = (
      paths.name.write[String] and
        paths.positions.write[Seq[RecordedPosition]]
    )(portfolio => (portfolio.portfolioName.name.value, portfolio.positions.toSeq))

    val portfolioNameReads: Reads[PortfoliosModel.PortfolioName] =
      paths.name
        .read[String]
        .flatMap(aString =>
          PortfolioName.maybeAPortfolioName(aString) match {
            case Right(value) => (_: JsValue) => JsSuccess(value, paths.name)
            case Left(errors) => (_: JsValue) => JsError(paths.name -> JsonValidationError(errors))
          }
        )

    val portfolioRecordPositionsRead: Reads[Seq[RecordedPosition]] = paths.positions.read[Seq[RecordedPosition]]

    implicit val jsonReads: Reads[PortfoliosModel.Portfolio] =
      (portfolioNameReads and portfolioRecordPositionsRead)((portfolioName, positions) => PortfoliosModel.Portfolio(portfolioName, positions.toSet))

    implicit val portfolioEq = new Equality[Portfolio] {
      override def areEqual(a: Portfolio, b: Any): Boolean =
        b match {
          case p: Portfolio => b == p
          case _            => false
        }
    }
  }

  final case class Portfolio(portfolioName: PortfolioName, positions: Set[RecordedPosition]) {

    @JsonIgnore
    private val internalPositions: Map[PositionId, RecordedPosition] = positions.map(p => (p.id, p)).toMap

    def updated(updates: NonEmptySeq[events.SingleUpdate]): Portfolio = {
      val newMappedPositions = updates.foldLeft(internalPositions)((zero, event) =>
        event match {
          case events.Added(newPosition)   => zero.updated(newPosition.id, newPosition)
          case events.Deleted(positionId)  => zero.removed(positionId)
          case events.Updated(newPosition) => zero.updated(newPosition.id, newPosition)
        }
      )
      Portfolio(portfolioName, newMappedPositions.values.toSet)
    }

    def newUpdated(updates: NonEmptySeq[org.binqua.forex.advisor.newportfolios.events.SingleUpdate]): Portfolio = {
      val newMappedPositions = updates.foldLeft(internalPositions)((zero, event) =>
        event match {
          case org.binqua.forex.advisor.newportfolios.events.Added(newPosition)   => zero.updated(newPosition.id, newPosition)
          case org.binqua.forex.advisor.newportfolios.events.Deleted(positionId)  => zero.removed(positionId)
          case org.binqua.forex.advisor.newportfolios.events.Updated(newPosition) => zero.updated(newPosition.id, newPosition)
        }
      )
      Portfolio(portfolioName, newMappedPositions.values.toSet)
    }

  }

  object CreatePortfolio {

    implicit val equality: Eq[CreatePortfolio] = Eq.fromUniversalEquals

    def validated(key: IdempotentKey, name: PortfolioName, positions: Seq[Position]): Validated[List[String], CreatePortfolio] =
      Either.cond(positions.nonEmpty, positions, List(s"Positions cannot be empty")) match {
        case Right(_)    => Valid(new CreatePortfolio(key, name, NonEmptySeq.fromSeqUnsafe(positions)))
        case Left(error) => Invalid(error)
      }
  }

  final case class CreatePortfolio(idempotentKey: IdempotentKey, portfolioName: PortfolioName, positions: NonEmptySeq[Position])

  final case class PortfolioName(name: PortfolioNameType)

  object PortfolioName {

    type PortfolioNamePredicate = Trimmed And MinSize[3] And MaxSize[30]

    type PortfolioNameType = Refined[String, PortfolioNamePredicate]

    implicit val portfolioNameOrdering: Ordering[PortfolioName] = (x: PortfolioName, y: PortfolioName) => x.name.value.compare(y.name.value)

    implicit val portfolioNameOrder: Order[PortfolioName] = (x: PortfolioName, y: PortfolioName) => portfolioNameOrdering.compare(x, y)

    implicit val equality: Eq[PortfolioName] = (x: PortfolioName, y: PortfolioName) => Eq[String].eqv(x.name.value, y.name.value)

    import eu.timepit.refined._

    def maybeAPortfolioName(name: String): Either[String, PortfolioName] =
      refineV[PortfolioNamePredicate](name)
        .bimap(
          _ => "Portfolio name has to be minimum 3 chars and maximum 30 without leading or trailing whitespaces",
          (ref: Refined[String, PortfolioNamePredicate]) => PortfolioName(ref)
        )

    def unsafe(name: String): PortfolioName = core.makeItUnsafe(maybeAPortfolioName(name))

  }

  @JsonDeserialize(builder = classOf[UpdatePortfolio.BuilderForJsonDeserialize])
  abstract case class UpdatePortfolio(
      maybeAnIdempotentKey: Option[IdempotentKey],
      portfolioName: PortfolioName,
      updatesToBeExecuted: NonEmptySeq[PortfolioSingleUpdate]
  )

  object UpdatePortfolio extends Validation {

    implicit val equality: Eq[UpdatePortfolio] = (x: UpdatePortfolio, y: UpdatePortfolio) => x == y

    def deleteWithDuplicateIdsCheck(positions: Seq[PortfolioSingleUpdate]): ErrorOr[Seq[PortfolioSingleUpdate]] = {
      val ids = idsToBeDeleted(positions)
      if (ids.size == ids.toSet.size) positions.asRight else s"In all position ids to be deleted $ids there are duplicated entries".asLeft
    }

    private def idsToBeUpdated(positions: Seq[PortfolioSingleUpdate]): Seq[PositionId] = {
      val filtered: Seq[PortfolioSingleUpdate] = positions.filter({
        case _: Update => true
        case _         => false
      })
      filtered.map(_.asInstanceOf[Update]).map(_.thePositionId).toList
    }

    private def idsToBeDeleted(positions: Seq[PortfolioSingleUpdate]): Seq[PositionId] = {
      val filtered: Seq[PortfolioSingleUpdate] = positions.filter({
        case _: Delete => true
        case _         => false
      })
      filtered.map(_.asInstanceOf[Delete]).map(_.thePositionId).toList
    }

    def updatesWithDuplicateIdsCheck(positions: Seq[PortfolioSingleUpdate]): ErrorOr[Seq[PortfolioSingleUpdate]] = {
      val ids = idsToBeUpdated(positions)
      if (ids.size == ids.toSet.size) positions.asRight else s"In all position ids to be updates $ids there are duplicated entries".asLeft
    }

    def duplicateIdsCheck(positions: Seq[PortfolioSingleUpdate]): ErrorOr[Seq[PortfolioSingleUpdate]] = {
      val ids = idsToBeDeleted(positions) concat idsToBeUpdated(positions)
      if (ids.size == ids.toSet.size) positions.asRight else s"In all position ids to be deleted and updated combined $ids there are duplicated entries".asLeft
    }

    def noAddsWithNoIdempotentKeyCheck(
        positions: Seq[PortfolioSingleUpdate],
        maybeAnIdempotentKey: Option[IdempotentKey]
    ): ErrorOr[Seq[PortfolioSingleUpdate]] = {

      val allAddUpdates = positions.filter({
        case _: Add => true
        case _      => false
      })
      maybeAnIdempotentKey match {
        case Some(_) =>
          if (allAddUpdates.nonEmpty) Right(positions) else "At least one add update has to be present if idempotent key is present".asLeft
        case None =>
          if (allAddUpdates.isEmpty) Right(positions) else "No adds update are allowed if idempotent key is not present".asLeft
      }
    }

    private def positionsValidation(positions: Seq[PortfolioSingleUpdate], maybeAnIdempotentKey: Option[IdempotentKey]): ErrorOr[Seq[PortfolioSingleUpdate]] =
      for {
        _ <- nonEmptyPositionsCheck(positions)
        _ <- deleteWithDuplicateIdsCheck(positions)
        _ <- updatesWithDuplicateIdsCheck(positions)
        _ <- duplicateIdsCheck(positions)
        _ <- noAddsWithNoIdempotentKeyCheck(positions, maybeAnIdempotentKey)
      } yield positions

    private def nonEmptyPositionsCheck(positions: Seq[PortfolioSingleUpdate]): Either[ErrorMessage, Seq[PortfolioSingleUpdate]] =
      Either.cond(positions.nonEmpty, positions, "Positions cannot be empty")

    def validated(
        maybeAnIdempotentKey: Option[IdempotentKey],
        portfolioName: PortfolioName,
        positions: Seq[PortfolioSingleUpdate]
    ): Validated[List[String], UpdatePortfolio] =
      positionsValidation(positions, maybeAnIdempotentKey) match {
        case Right(valid) => Valid(new UpdatePortfolio(maybeAnIdempotentKey, portfolioName, NonEmptySeq.fromSeqUnsafe(valid)) {})
        case Left(error)  => Invalid(List(error))
      }

    def unsafe(maybeAnIdempotentKey: Option[IdempotentKey], portfolioName: PortfolioName, updates: NonEmptySeq[PortfolioSingleUpdate]): UpdatePortfolio =
      makeItUnsafe(validated(maybeAnIdempotentKey, portfolioName, updates.toSeq))

    @JsonPOJOBuilder
    case class BuilderForJsonDeserialize(maybeAnIdempotentKey: String, portfolioName: PortfolioName, updatesToBeExecuted: Seq[PortfolioSingleUpdate]) {

      import cats.syntax.option._

      def build(): UpdatePortfolio =
        unsafe(
          if (maybeAnIdempotentKey == null) None else IdempotentKey(UUID.fromString(maybeAnIdempotentKey)).some,
          portfolioName,
          NonEmptySeq.fromSeqUnsafe(updatesToBeExecuted)
        )
    }

  }

  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[Add], name = "add"),
      new JsonSubTypes.Type(value = classOf[Delete], name = "delete"),
      new JsonSubTypes.Type(value = classOf[Update], name = "update")
    )
  )
  sealed trait PortfolioSingleUpdate extends org.binqua.forex.JsonSerializable

  final case class Add(thePositionData: Position) extends PortfolioSingleUpdate

  final case class Delete(thePositionId: PositionId) extends PortfolioSingleUpdate

  final case class Update(thePositionId: PositionId, thePositionData: Position) extends PortfolioSingleUpdate

  object events {

    sealed trait Event extends org.binqua.forex.JsonSerializable

    @JsonDeserialize(builder = classOf[PortfolioCreated.BuilderForJsonDeserialize])
    abstract case class PortfolioCreated(portfolioName: PortfolioName, recordedPositions: NonEmptySeq[RecordedPosition]) extends Event

    object PortfolioCreated {
      private def validated(portfolioName: PortfolioName, recordedPositions: NonEmptySeq[RecordedPosition]): Either[String, PortfolioCreated] = {
        val ids = recordedPositions.map(_.id).toSeq
        Either.cond(
          Eq[Int].eqv(ids.length, ids.toSet.size),
          new PortfolioCreated(portfolioName, recordedPositions) {},
          s"It is not possible to have a duplicated ids in record position ids ${ids.mkString("[", ",", "]")}"
        )
      }

      def unsafe(portfolioName: PortfolioName, recordedPositions: NonEmptySeq[RecordedPosition]): PortfolioCreated =
        makeItUnsafe(validated(portfolioName, recordedPositions))

      @JsonPOJOBuilder
      case class BuilderForJsonDeserialize(portfolioName: PortfolioName, recordedPositions: Seq[RecordedPosition]) {

        def build(): PortfolioCreated = {
          unsafe(portfolioName, NonEmptySeq.fromSeqUnsafe(recordedPositions))
        }
      }

    }

    @JsonDeserialize(builder = classOf[PortfolioUpdated.BuilderForJsonDeserialize])
    abstract case class PortfolioUpdated(portfolioName: PortfolioName, updates: NonEmptySeq[SingleUpdate]) extends Event

    object PortfolioUpdated {
      private def validated(portfolioName: PortfolioName, updates: NonEmptySeq[SingleUpdate]): Either[String, PortfolioUpdated] = {
        val ids: Seq[PositionId] = updates
          .map({
            case a: Added   => a.position.id
            case d: Deleted => d.positionId
            case u: Updated => u.position.id
          })
          .toSeq

        Either.cond(
          Eq[Int].eqv(ids.length, ids.toSet.size),
          new PortfolioUpdated(portfolioName, updates) {},
          s"It is not possible to have a duplicated ids in updates ids ${ids.mkString("[", ",", "]")}"
        )
      }

      def unsafe(portfolioName: PortfolioName, updates: NonEmptySeq[SingleUpdate]): PortfolioUpdated = makeItUnsafe(validated(portfolioName, updates))

      @JsonPOJOBuilder
      case class BuilderForJsonDeserialize(portfolioName: PortfolioName, updates: Seq[SingleUpdate]) {
        def build(): PortfolioUpdated = unsafe(portfolioName, NonEmptySeq.fromSeqUnsafe(updates))
      }

    }

    @JsonSubTypes(
      Array(
        new JsonSubTypes.Type(value = classOf[Added], name = "added"),
        new JsonSubTypes.Type(value = classOf[Updated], name = "updated"),
        new JsonSubTypes.Type(value = classOf[Deleted], name = "deleted")
      )
    )
    sealed trait SingleUpdate extends org.binqua.forex.JsonSerializable

    case class Added(position: RecordedPosition) extends SingleUpdate

    case class Updated(position: RecordedPosition) extends SingleUpdate

    case class Deleted(positionId: PositionId) extends SingleUpdate

  }

}
