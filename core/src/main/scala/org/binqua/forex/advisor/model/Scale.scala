package org.binqua.forex.advisor.model

import cats.Show
import cats.syntax.either._
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.types.numeric.NonNegInt
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.util.core._
import org.scalactic.Equality
import org.scalactic.TripleEquals._

import scala.language.implicitConversions

object Scale {

  implicit def unsafe(value: Int): Scale = Scale(makeItUnsafe(refineV[NonNegative](value).leftMap(_ => s"Scale value has to be >= 0. $value is not allowed")))

  implicit val scaleShow: Show[Scale] = Show.show(scale => s"${scale.value}")

  implicit val scaleEq = new Equality[Scale] {
    override def areEqual(a: Scale, b: Any): Boolean =
      b match {
        case scale: Scale => scale.value === a.value
        case _            => false
      }
  }
}

case class Scale(value: NonNegInt) extends Ordered[Scale] {

  import PosBigDecimal._

  def +(increaseBy: Int): Either[String, Scale] = NonNegInt.from(value.value + increaseBy).map(Scale(_))

  override def compare(that: Scale): Int = value.value compare that.value.value

  def powerOf10: PosBigDecimal = PosBigDecimal.tenPowerOf(value)

  def asFractionalPartUnit: PosBigDecimal = refineMV[NonNegative](1) / PosBigDecimal.tenPowerOf(value)

}
