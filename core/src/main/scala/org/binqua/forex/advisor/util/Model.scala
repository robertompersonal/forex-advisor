package org.binqua.forex.advisor.util

import cats.Eq
import eu.timepit.refined.types.string.NonEmptyString

object Model {

  type ShardedEntityId = NonEmptyString

  implicit val shardedEntityIdsEq: Eq[ShardedEntityId] = (x: ShardedEntityId, y: ShardedEntityId) => Eq[String].eqv(x.value, y.value)

}
