package org.binqua.forex.advisor.portfolios.service

import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import org.binqua.forex.advisor.newportfolios.{NewPortfoliosShardingProductionModule, PortfoliosEntityFinderModule}
import org.binqua.forex.util.core

import java.util.UUID

trait PortfoliosServiceBehaviorModule {

  def portfoliosServiceBehavior(initializedPortfoliosModule: PortfoliosEntityFinderModule): Behavior[PortfoliosService.Message]

}

trait PortfoliosServiceModule {

  def init(clusterSharding: ClusterSharding): Behavior[PortfoliosService.Message]

}

trait DefaultPortfoliosServiceBehaviorModule extends PortfoliosServiceBehaviorModule {

  final def portfoliosServiceBehavior(initializedPortfoliosModule: PortfoliosEntityFinderModule): Behavior[PortfoliosService.Message] =
    PortfoliosService(
      () => UUIDMessageId(UUID.randomUUID()),
      initializedPortfoliosModule,
      DefaultSupportMessagesFactory,
      akkaConfig => core.makeItUnsafe(ConfigValidator(akkaConfig))
    )
}

trait AbstractPortfoliosServiceModule extends PortfoliosServiceModule {

  this: org.binqua.forex.advisor.newportfolios.PortfoliosShardingModule with PortfoliosServiceBehaviorModule =>

  def init(clusterSharding: ClusterSharding): Behavior[PortfoliosService.Message] = {

    portfoliosServiceBehavior(initialisePortfoliosSharding(clusterSharding))

  }

}

trait ProdReadyNewPortfoliosServiceModule
    extends AbstractPortfoliosServiceModule
    with DefaultPortfoliosServiceBehaviorModule
    with NewPortfoliosShardingProductionModule {}
