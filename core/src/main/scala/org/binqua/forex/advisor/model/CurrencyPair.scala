package org.binqua.forex.advisor.model

import cats.kernel.Eq
import cats.syntax.show._
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonPOJOBuilder, JsonSerialize}
import com.fasterxml.jackson.databind.{JsonSerializer, SerializerProvider}
import org.binqua.forex.implicits.instances
import org.binqua.forex.implicits.instances.id._
import org.binqua.forex.util.Validation

@JsonSerialize(using = classOf[CurrencyPairSerializer])
@JsonDeserialize(builder = classOf[CurrencyPair.BuilderForJsonDeserialize])
trait CurrencyPair {
  val baseCurrency: BaseCurrency
  val quoteCurrency: QuoteCurrency
}

object CurrencyPair extends Validation {

  import org.binqua.forex.implicits.instances.accountCurrency._
  import org.binqua.forex.implicits.instances.currencyPair._
  import org.binqua.forex.implicits.instances.quoteCurrency._

  @JsonPOJOBuilder
  case class BuilderForJsonDeserialize(pair: String) {
    def build(): CurrencyPair = CurrencyPairByJsonRepresentation.fromJsonStringUnsafe(pair)
  }

  def toExternalIdentifier(currencyPair: CurrencyPair): String =
    s"${currencyPair.baseCurrency.name}".concat(if (currencyPair.baseCurrency.index) "" else s"/${currencyPair.quoteCurrency.baseCurrency.name}").toUpperCase()

  def toCurrencyPair(externalIdentifier: String, currencyPairs: List[CurrencyPair]): Option[CurrencyPair] = {
    import cats.instances.string._
    import cats.kernel.Eq
    currencyPairs.find(cp => {
      val identifier = s"${cp.baseCurrency.name}".concat(if (cp.baseCurrency.index) "" else s"/${cp.quoteCurrency.baseCurrency.name}").toUpperCase()
      Eq.eqv(externalIdentifier, identifier)
    })
  }

  def maybeACurrencyPair(externalIdentifier: String, allValidExternalIdentifiers: Map[String, CurrencyPair]): ErrorOr[CurrencyPair] = {
    allValidExternalIdentifiers
      .get(externalIdentifier)
      .toRight(s"Cannot find $externalIdentifier in the list of allowed identifiers: ${allValidExternalIdentifiers.mkString(",")}")
  }

  def byBaseAndQuoteIdentifiersInAnyOrder(toBeFound: (Id, Id), currencyPairs: List[CurrencyPair]): Option[CurrencyPair] =
    currencyPairs.find(cp => {
      import org.binqua.forex.implicits.instances.id._
      val ids = cp.toIds.ids
      Eq.eqv(ids, toBeFound) || Eq.eqv(ids.swap, toBeFound)
    })

  def currencyPairNeededForPipValueCalculation(
      accountCurrency: AccountCurrency
  )(quoteCurrency: QuoteCurrency)(currencyPairs: List[CurrencyPair]): ErrorOr[CurrencyPair] = {

    val pseudoCurrencyPair = (quoteCurrency.toId, accountCurrency.toId)

    val currencyPairsToBeLookingFor: List[(Id, Id)] = List(pseudoCurrencyPair, pseudoCurrencyPair.swap)

    currencyPairs
      .filter(!_.baseCurrency.index)
      .find(cp => {
        currencyPairsToBeLookingFor.contains(cp.toIds.ids)
      })
      .toRight(errorMessage(currencyPairs, pseudoCurrencyPair))
  }

  private def errorMessage(currencyPairs: List[CurrencyPair], pseudoCurrencyPairMadeOfQuoteAndAccountCurrency: (Id, Id)) = {
    s"Sorry!!! I could not find ${pseudoCurrencyPairMadeOfQuoteAndAccountCurrency.show} or ${pseudoCurrencyPairMadeOfQuoteAndAccountCurrency.swap.show} in currency pairs ${currencyPairs
      .mkString("[", ",", "]")}. Maybe some currency pair are not supported!"
  }

  case object EurUsd extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Eur
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Usd
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object EurGbp extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Eur
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Gbp
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object GbpUsd extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Gbp
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Usd
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object GbpJpy extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Gbp
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Jpy
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object Spx500 extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Spx500
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.UsdForSpx500
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object Us30 extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Us30
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.UsdForUs30
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object UsdJpy extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Usd
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Jpy
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  case object EurJpy extends CurrencyPair {
    override val baseCurrency: BaseCurrency = BaseCurrency.Eur
    override val quoteCurrency: QuoteCurrency = QuoteCurrency.Jpy
    require(baseCurrency != quoteCurrency.baseCurrency)
  }

  val values: List[CurrencyPair] = List(EurUsd, EurGbp, EurJpy, GbpUsd, GbpJpy, Spx500, UsdJpy, Us30)

  val fromExternalIdentifierMapping: Map[String, CurrencyPair] =
    values.map(cp => (s"${cp.baseCurrency.name}".concat(if (cp.baseCurrency.index) "" else s"/${cp.quoteCurrency.baseCurrency.name}").toUpperCase(), cp)).toMap

}

object CurrencyPairByJsonRepresentation {

  import instances.currencyPair._
  import org.binqua.forex.implicits.instances.baseCurrency._
  import org.binqua.forex.implicits.instances.id._
  import org.binqua.forex.implicits.instances.quoteCurrency._

  val currencyPairByJsonRep = CurrencyPairByJsonRepresentation.constructCurrencyPairByJsonRep(CurrencyPair.values)

  def toJsonStringUnsafe(currencyPair: CurrencyPair): String = {
    currencyPairByJsonRep.find(kv => Eq.eqv(kv._2, currencyPair)) match {
      case Some(cp) => cp._1
      case None     => throw new IllegalArgumentException(s"I dont know how to serialise $currencyPair using $currencyPairByJsonRep")
    }
  }

  def fromJsonStringUnsafe(currencyPair: String): CurrencyPair = {
    CurrencyPairByJsonRepresentation.currencyPairByJsonRep.get(currencyPair) match {
      case Some(currencyPair) => currencyPair
      case None               => throw new IllegalStateException(s"Could not find $currencyPair to obtain a currency pair")
    }
  }

  private def constructCurrencyPairByJsonRep(values: List[CurrencyPair]): Map[String, CurrencyPair] = {
    values.foldLeft(Map[String, CurrencyPair]())((acc, cp) => {
      if (cp.baseCurrency.index) {
        acc.updated(cp.baseCurrency.toId.show, cp)
      } else
        acc.updated(s"${cp.baseCurrency.toId.show}/${cp.quoteCurrency.toId.show}", cp)
    })

  }

}

class CurrencyPairSerializer extends JsonSerializer[CurrencyPair] {
  override def serialize(currencyPair: CurrencyPair, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeStartObject()
    gen.writeStringField("pair", CurrencyPairByJsonRepresentation.toJsonStringUnsafe(currencyPair))
    gen.writeEndObject()
  }
}
