package org.binqua.forex.advisor.portfolios.service

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import eu.timepit.refined.auto._
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.Response
import org.binqua.forex.advisor.newportfolios.{DeliveryGuaranteedPortfolios, PortfoliosEntityFinderModule, PortfoliosSummary}
import org.binqua.forex.advisor.portfolios.service.InFlightCommandRecord.increaseNumberOfRetriesCounterOf
import org.binqua.forex.advisor.util.Model
import org.binqua.forex.util.core.UnsafeConfigReader

object PortfoliosService {

  sealed trait Message extends org.binqua.forex.JsonSerializable

  sealed trait ExternalMessage extends Message

  final case class Envelope(
      shardedEntityId: Model.ShardedEntityId,
      payload: DeliveryGuaranteedPortfolios.Payload,
      replyTo: ActorRef[Response]
  ) extends ExternalMessage

  sealed trait Response extends org.binqua.forex.JsonSerializable

  final case class DeliverySucceeded(portfolios: PortfoliosSummary) extends Response

  final case class ValidationFailed(error: NonEmptyString) extends Response

  final case class TooManyRetries(retries: Config.RetryAttempts) extends Response

  final case class Rejected(tooManyInFlightMessages: Config.MaxInFlightMessages) extends Response

  object TimedOut extends Response

  object ServerError extends Response

  sealed trait InternalMessage extends Message

  final case class WrappedDeliveryConfirm(portfolioResponse: DeliveryGuaranteedPortfolios.Response) extends InternalMessage

  final case class Retry(messageId: MessageId) extends InternalMessage

  def apply(
      retryIdGenerator: MessageIdGenerator,
      portfoliosFinder: PortfoliosEntityFinderModule,
      supportMessagesFactory: SupportMessagesFactory,
      configReader: UnsafeConfigReader[Config]
  ): Behavior[Message] =
    Behaviors.withTimers(timers => {
      Behaviors.setup((context: ActorContext[Message]) => {
        val config: Config = configReader(context.system.settings.config)
        newBehavior(
          portfoliosFinder,
          Map(),
          retryIdGenerator,
          portfoliosResponseAdapter = context.messageAdapter[DeliveryGuaranteedPortfolios.Response](WrappedDeliveryConfirm),
          timers,
          config,
          context,
          supportMessagesFactory.newSupportMessages(config)
        )
      })
    })

  def maxNumberOfRetriesReachedMessage(maxRetryAttempts: Config.RetryAttempts): String = s"max number of retries reached: ${maxRetryAttempts.value}"

  def toResponse(result: DeliveryGuaranteedPortfolios.ResponseType): Response =
    result match {
      case DeliveryGuaranteedPortfolios.Success(state) => DeliverySucceeded(state)
      case DeliveryGuaranteedPortfolios.Error(error) =>
        ValidationFailed(refineV[NonEmpty](error) match {
          case Left(_)      => "Some validation error not specified"
          case Right(value) => value
        })
    }

  def newBehavior(
      portfoliosFinder: PortfoliosEntityFinderModule,
      messagesWaitingForConfirmation: Map[MessageId, InFlightCommandRecord],
      messageIdGenerator: MessageIdGenerator,
      portfoliosResponseAdapter: ActorRef[DeliveryGuaranteedPortfolios.Response],
      timers: TimerScheduler[Message],
      config: Config,
      context: ActorContext[Message],
      supportMessages: SupportMessages
  ): Behavior[Message] =
    Behaviors.receiveMessage({
      case Envelope(_, _, replyTo) if messagesWaitingForConfirmation.size >= config.maxInFlightMessages.value =>
        context.log.info(supportMessages.maxInFlightMessagesReached())
        replyTo ! Rejected(config.maxInFlightMessages)
        newBehavior(
          portfoliosFinder,
          messagesWaitingForConfirmation,
          messageIdGenerator,
          portfoliosResponseAdapter,
          timers,
          config,
          context,
          supportMessages
        )
      case Envelope(shardedEntityId, content, replyTo) =>
        val messageId = messageIdGenerator.newId()
        val messageToBeSent: DeliveryGuaranteedPortfolios.ExternalCommand =
          DeliveryGuaranteedPortfolios.ExternalCommand(messageId, content, portfoliosResponseAdapter)
        portfoliosFinder.ref(shardedEntityId) ! messageToBeSent
        timers.startSingleTimer(messageId.id, Retry(messageId), config.retryIntervalInMillis)
        newBehavior(
          portfoliosFinder,
          messagesWaitingForConfirmation
            .updated(messageId, InFlightCommandRecord(messageToBeSent, replyTo, shardedEntityId, numberOfRetriesStarted = refineMV[NonNegative](0))),
          messageIdGenerator,
          portfoliosResponseAdapter,
          timers,
          config,
          context,
          supportMessages
        )
      case Retry(messageId) =>
        messagesWaitingForConfirmation
          .get(messageId)
          .fold[Behavior[Message]](ifEmpty = Behaviors.same)(oldInFlightCommand => {
            if (oldInFlightCommand.maxRetriesReached(config.maxRetryAttempts)) {
              context.log.info(supportMessages.maxRetriesReached(oldInFlightCommand))
              oldInFlightCommand.replyTo ! TooManyRetries(config.maxRetryAttempts)
              Behaviors.same
            } else {
              val newInFlightCommand = increaseNumberOfRetriesCounterOf(oldInFlightCommand)
              context.log.info(supportMessages.retryIntervalExpired(newInFlightCommand))
              portfoliosFinder.ref(newInFlightCommand.shardedEntityId) ! newInFlightCommand.command
              timers.startSingleTimer(messageId.id, Retry(messageId), config.retryIntervalInMillis)
              newBehavior(
                portfoliosFinder,
                messagesWaitingForConfirmation.updated(messageId, newInFlightCommand),
                messageIdGenerator,
                portfoliosResponseAdapter,
                timers,
                config,
                context,
                supportMessages
              )
            }
          })

      case WrappedDeliveryConfirm(reponse @ Response(messageId, result)) =>
        messagesWaitingForConfirmation
          .get(messageId)
          .foreach(inFlightCommandRecord => {
            context.log.info(supportMessages.messageConfirmed(inFlightCommandRecord))
            timers.cancel(messageId.id)
            inFlightCommandRecord.replyTo ! toResponse(result)
          })
        newBehavior(
          portfoliosFinder,
          messagesWaitingForConfirmation.removed(messageId),
          messageIdGenerator,
          portfoliosResponseAdapter,
          timers,
          config,
          context,
          supportMessages
        )
    })

}
