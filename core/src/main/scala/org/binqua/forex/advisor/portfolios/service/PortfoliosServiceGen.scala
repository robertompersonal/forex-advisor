package org.binqua.forex.advisor.portfolios.service

import akka.actor.typed.ActorRef
import eu.timepit.refined.api.RefType.refinedRefType
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.scalacheck.all.chooseRefinedNum
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios._
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.scalacheck.Gen

import scala.collection.immutable.TreeSet
object PortfoliosServiceGen {

  object PayloadsGen {
    val createPortfolioPayloadGen = PortfoliosGen.createPortfolio.map(CreatePortfolioPayload)
    val deletePortfolioPayload: Gen[DeletePortfoliosPayload] = for {
      numberOfPortfolios <- Gen.choose(2, 4)
      portfolioNames <- Gen.listOfN(numberOfPortfolios, PortfoliosGen.portfolioName)
      result <- cats.data.NonEmptySet.fromSet(TreeSet(portfolioNames: _*)) match {
        case Some(value) => Gen.const(value)
        case None        => Gen.fail
      }
    } yield DeletePortfoliosPayload(result)

    val showPortfoliosPayload = Gen.const(ShowPortfoliosPayload)
    val updatePortfolioPayload = PortfoliosGen.updatePortfolio.map(UpdatePortfolioPayload)
  }

  def command(replyTo: ActorRef[DeliveryGuaranteedPortfolios.Response]): Gen[ExternalCommand] =
    for {
      messageId <- Gen.uuid
      payload <- Gen.oneOf(
        PayloadsGen.createPortfolioPayloadGen,
        PayloadsGen.deletePortfolioPayload,
        PayloadsGen.showPortfoliosPayload,
        PayloadsGen.updatePortfolioPayload
      )
    } yield ExternalCommand(UUIDMessageId(messageId), payload, replyTo)

  val nonEmptyStringGen: Gen[NonEmptyString] = {
    for {
      maybeEmptyFiniteString <- Gen.alphaNumStr
      result <- Gen.const(refineV[NonEmpty](maybeEmptyFiniteString))
    } yield result.getOrElse(refineMV[NonEmpty]("test"))
  }

  def inFlightCommandRecord(commandGen: Gen[ExternalCommand], replyTo: ActorRef[PortfoliosService.Response]): Gen[InFlightCommandRecord] = {
    for {
      command <- commandGen
      nonEmptyString <- nonEmptyStringGen
      numberOfRetriesStarted <- chooseRefinedNum(refineMV[NonNegative](0), refineMV[NonNegative](5))
    } yield InFlightCommandRecord(command, replyTo, nonEmptyString, numberOfRetriesStarted)
  }

}
