package org.binqua.forex.advisor.model

import cats.Show
import cats.syntax.either._
import cats.syntax.show._
import com.fasterxml.jackson.core._
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonSerializer, SerializerProvider}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.numeric.{NonNegInt, NonNegLong}
import org.binqua.forex.util.core.ABugIfNotRight
import org.binqua.forex.util.{Bug, core}
import org.scalactic.Equality

import java.lang.Math.pow
import scala.language.implicitConversions
import scala.math.BigDecimal.RoundingMode
import scala.util.Try

object PosBigDecimal {

  object safeOnlyAfterValidation {
    def newPosBigDecimal(value: BigDecimal): PosBigDecimal = new PosBigDecimal(value) {}
  }

  val ten: PosBigDecimal = aPosBigDecimal(refineMV[NonNegative](10))

  object RescaleFailedBecauseNewScaleTooSmall {
    implicit val show: Show[RescaleFailedBecauseNewScaleTooSmall] = (context: RescaleFailedBecauseNewScaleTooSmall) =>
      s"Scale of ${context.toBeRescaled.show} is ${context.toBeRescaled.scale.show} and it is too big compare to new scale ${context.newScale.show}. I will lose precision"
  }

  final case class RescaleFailedBecauseNewScaleTooSmall(toBeRescaled: PosBigDecimal, newScale: Scale)

  implicit val posBigDecimalEq = new Equality[PosBigDecimal] {
    override def areEqual(a: PosBigDecimal, b: Any): Boolean =
      b match {
        case pbd: PosBigDecimal => (a.value - pbd.value).toDouble < 0.01
        case _                  => false
      }
  }

  implicit val posBigDecimalShow: Show[PosBigDecimal] = Show.show(posBigDecimal => s"${posBigDecimal.value}")

  case class DivideAnIntToPosBigDecimal(nonNegative: Refined[Int, NonNegative]) {
    def /(posBigDecimal: PosBigDecimal): PosBigDecimal = PosBigDecimal.aPosBigDecimal(nonNegative) / posBigDecimal
  }

  case class MultipleAnIntToPosBigDecimal(nonNegative: Refined[Int, NonNegative]) {
    def *(posBigDecimal: PosBigDecimal): PosBigDecimal = PosBigDecimal.aPosBigDecimal(nonNegative) * posBigDecimal
  }

  implicit def divideAnIntByPosBigDecimal(int: Refined[Int, NonNegative]): DivideAnIntToPosBigDecimal = DivideAnIntToPosBigDecimal(int)

  implicit def multipleAnIntToPosBigDecimal(int: Refined[Int, NonNegative]): MultipleAnIntToPosBigDecimal = MultipleAnIntToPosBigDecimal(int)

  def unsafeFrom(value: BigDecimal): PosBigDecimal = core.makeItUnsafe(maybeAPosBigDecimal(value))

  def tenPowerOf(value: NonNegInt): PosBigDecimal = PosBigDecimal.safeOnlyAfterValidation.newPosBigDecimal(BigDecimal(pow(10, value.value)).setScale(0))

  def reverseOf10PowerOf(value: NonNegInt): PosBigDecimal = refineMV[NonNegative](1) / PosBigDecimal.tenPowerOf(value)

  def rescale(toBeRescaled: PosBigDecimal, theNewScale: Scale): Either[String, PosBigDecimal] =
    Either.cond(
      toBeRescaled.scale <= theNewScale,
      safeOnlyAfterValidation.newPosBigDecimal(toBeRescaled.value.setScale(theNewScale.value.value)),
      RescaleFailedBecauseNewScaleTooSmall.show.show(RescaleFailedBecauseNewScaleTooSmall(toBeRescaled, theNewScale))
    )

  def maybeAPosBigDecimal(input: String): Either[String, PosBigDecimal] =
    for {
      aBigDecimal <- Try(input).map(BigDecimal(_)).toEither.leftMap(_ => errorMessage(s"Cannot parse $input into a PosBigDecimal"))
      aPosBigDecimal <- maybeAPosBigDecimal(aBigDecimal)
    } yield aPosBigDecimal

  def errorMessage(reason: String): String =
    s"A PosBigDecimal value has to be a decimal number >= 0. Cannot create a PositiveBigDecimal. Reason: $reason"

  def wrongBigDecimal(wrongBigDecimal: BigDecimal): String = s"$wrongBigDecimal is wrong"

  def aPosBigDecimal(nonNegative: Refined[Int, NonNegative]): PosBigDecimal = safeOnlyAfterValidation.newPosBigDecimal(nonNegative.value)

  def maybeAPosBigDecimal(aBigDecimal: BigDecimal): Either[String, PosBigDecimal] =
    Either.cond(aBigDecimal >= 0, aBigDecimal, errorMessage(wrongBigDecimal(aBigDecimal))).map(safeOnlyAfterValidation.newPosBigDecimal)

  def increasedByScaleUnit(posBigDecimal: PosBigDecimal, numberOfScaleUnit: Int): PosBigDecimal =
    unsafeFrom(posBigDecimal.value.ulp * numberOfScaleUnit + posBigDecimal.value)

  object TryingToRemoveTooManyPips {

    implicit val show: Show[TryingToRemoveTooManyPips] = (t: TryingToRemoveTooManyPips) =>
      s"From ${t.posBigDecimal.show} you can remove max ${t.maxNumberOfPipsThatCanBeRemoved} with pip position of ${t.pipPosition.value}. You cannot remove ${t.pipsYouTriedToRemove} pips!"

  }

  final case class TryingToRemoveTooManyPips(
      posBigDecimal: PosBigDecimal,
      maxNumberOfPipsThatCanBeRemoved: NonNegLong,
      pipsYouTriedToRemove: NonNegLong,
      pipPosition: NonNegInt
  )

  final case class TryingToRemoveAGreaterPosBiDecimal(
      initialPosBigDecimal: PosBigDecimal,
      theGreaterPosBigDecimal: PosBigDecimal
  )

}

@JsonSerialize(using = classOf[PosBigDecimalSerializer])
@JsonDeserialize(using = classOf[PosBigDecimalDeserializer])
sealed abstract case class PosBigDecimal(value: BigDecimal) {

  import PosBigDecimal._

  val scale: Scale = Scale.unsafe(value.scale)

  def scaled(action: => BigDecimal): PosBigDecimal = maybeAPosBigDecimal(action).aBugIfNotRight

  def *(anotherPosBigDecimal: PosBigDecimal): PosBigDecimal = scaled(value * anotherPosBigDecimal.value)

  def /(anotherPosBigDecimal: PosBigDecimal): PosBigDecimal = scaled(value / anotherPosBigDecimal.value)

  def +(anotherPosBigDecimal: PosBigDecimal): PosBigDecimal = scaled(value + anotherPosBigDecimal.value)

  def -(anotherPosBigDecimal: PosBigDecimal): Either[TryingToRemoveAGreaterPosBiDecimal, PosBigDecimal] =
    PosBigDecimal
      .maybeAPosBigDecimal(value - anotherPosBigDecimal.value)
      .leftMap(_ => TryingToRemoveAGreaterPosBiDecimal(this, anotherPosBigDecimal))
      .map(p => scaled(p.value))

  def reverse: PosBigDecimal = refineMV[NonNegative](1) / this

  def increaseByPipUnits(numberOfPips: Long, aGivenPipPosition: NonNegInt): Either[String, PosBigDecimal] = {
    if (numberOfPips > 0) {
      PosBigDecimal.maybeAPosBigDecimal(numberOfPips * reverseOf10PowerOf(aGivenPipPosition).value).map(_ + this)
    } else {
      for {
        theOtherPosBigDecimal <- PosBigDecimal.maybeAPosBigDecimal(math.abs(numberOfPips) / tenPowerOf(aGivenPipPosition).value)
        maxNumberOfPipsToBeRemoved <- {
          val maxNumberOfPipsToBeRemoved = (this * tenPowerOf(aGivenPipPosition)).value.toLong
          NonNegLong
            .from(maxNumberOfPipsToBeRemoved)
            .leftMap(_ => s"It Looks like maxNumberOfPipsToBeRemoved $maxNumberOfPipsToBeRemoved should be a non negative long but was not")
        }
        result <- (this - theOtherPosBigDecimal).leftMap(_ =>
          TryingToRemoveTooManyPips.show.show(
            TryingToRemoveTooManyPips(this, maxNumberOfPipsToBeRemoved, NonNegLong.unsafeFrom(math.abs(numberOfPips)), aGivenPipPosition)
          )
        )
      } yield result
    }
  }

  def rounded(scale: Scale): PosBigDecimal = PosBigDecimal.unsafeFrom(value.setScale(scale.value.value, RoundingMode.HALF_UP))

}

class PosBigDecimalSerializer extends JsonSerializer[PosBigDecimal] {

  override def serialize(posBigDecimal: PosBigDecimal, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeString(posBigDecimal.value.toString)
  }

}

class PosBigDecimalDeserializer extends JsonDeserializer[PosBigDecimal] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): PosBigDecimal = {
    PosBigDecimal.maybeAPosBigDecimal(p.getText()) match {
      case Right(value) => value
      case Left(error)  => throw new Bug(error)
    }
  }
}
