package org.binqua.forex.advisor.model

import cats.Eq
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import eu.timepit.refined.scalacheck.all.chooseRefinedNum
import eu.timepit.refined.types.numeric.{NonNegInt, NonNegLong}
import org.apache.commons.lang.StringUtils
import org.binqua.forex.advisor.model.PosBigDecimal._
import org.scalacheck.Gen

import java.text.DecimalFormat
import scala.annotation.tailrec

object PosBigDecimalGen {

  implicit class BigDecimalWrapper[T](constGen: T) {
    def or(theOtherGen: Gen[T]): Gen[T] = Gen.oneOf(Gen.const(constGen), theOtherGen)
  }

  private val numberOfFractionalPartUnits: Gen[Refined[Int, NonNegative]] = chooseRefinedNum(refineMV[NonNegative](1), refineMV[NonNegative](1000))

  val posBigDecimal: Gen[PosBigDecimal] =
    configurablePosBigDecimalGen(min = TradingPairGen.min, max = TradingPairGen.max)(Scale(refineMV[NonNegative](8))).map(_._1)

  def posBigDecimalWithScale(scaleGen: Gen[Scale]): Gen[PosBigDecimal] =
    configurablePosBigDecimalGen(TradingPairGen.min, TradingPairGen.max)(scaleGen).map(_._1)

  implicit class GenWrapper[T](oneGen: Gen[T]) {
    def or(theOtherGen: Gen[T]): Gen[T] = Gen.oneOf(oneGen, theOtherGen)
  }

  def greaterThan(initialValue: PosBigDecimal, currencyPair: CurrencyPair): Gen[PosBigDecimal] =
    numberOfFractionalPartUnits.map(initialValue + _ * currencyPair.quoteCurrency.scale.asFractionalPartUnit)

  def smallerThan(initialValue: PosBigDecimal, currencyPair: CurrencyPair): Gen[PosBigDecimal] = {
    for {
      newValue <- numberOfFractionalPartUnits.map((refined: Refined[Int, NonNegative]) => initialValue.value - (refined.value * initialValue.value.ulp))
    } yield PosBigDecimal.unsafeFrom(newValue)
  }

  def withRange(min: PosBigDecimal, max: PosBigDecimal): Gen[Scale] => Gen[(PosBigDecimal, Scale)] = configurablePosBigDecimalGen(min, max)

  def configurablePosBigDecimalGen(min: PosBigDecimal, max: PosBigDecimal)(scaleGen: Gen[Scale]): Gen[(PosBigDecimal, Scale)] =
    for {
      scale <- scaleGen
      lowest <- Gen.const((min * scale.powerOf10).value.toLong)
      highest <- Gen.const((max * scale.powerOf10).value.toLong)
      asIntWithoutDecimalPoint <- Gen.choose(lowest, highest)
    } yield {
      (PosBigDecimal.unsafeFrom((asIntWithoutDecimalPoint / scale.powerOf10.value).setScale(scale.value.value)), scale)
    }

  @tailrec private def divideBy10XTimes(toBeDivided: PosBigDecimal, numberOfTimes: NonNegInt): PosBigDecimal = {
    if (Eq.eqv(numberOfTimes.value, 0)) toBeDivided
    else divideBy10XTimes(toBeDivided / PosBigDecimal.ten, NonNegInt.unsafeFrom(numberOfTimes.value - 1))
  }

  def posBigDecimalPipsInfoAware: Gen[(PosBigDecimal, NonNegLong, NonNegInt)] =
    for {
      pipPosition <- chooseRefinedNum(refineMV[NonNegative](0), refineMV[NonNegative](5))
      maxNumberOfPips <- chooseRefinedNum(refineMV[NonNegative](1L), refineMV[NonNegative](10000L))
      theNumberPrefix = divideBy10XTimes(PosBigDecimal.unsafeFrom(maxNumberOfPips.value), pipPosition)
      theNumberPrefixFormatted = formatToHaveRightDecimalPart(numberOfDecimalPlaces = pipPosition, toBeFormatted = theNumberPrefix)
      aNumberSuffixToBeMoreGeneric <- Gen.listOfN(4, Gen.numChar).map(_.mkString)
      aPosBigDecimal <- Gen.const(PosBigDecimal.unsafeFrom(BigDecimal(s"$theNumberPrefixFormatted$aNumberSuffixToBeMoreGeneric")))
    } yield (aPosBigDecimal, maxNumberOfPips, pipPosition)

  private def formatToHaveRightDecimalPart(numberOfDecimalPlaces: NonNegInt, toBeFormatted: PosBigDecimal): String = {
    new DecimalFormat(s"#.${StringUtils.repeat("0", numberOfDecimalPlaces.value)}").format(toBeFormatted.value)
  }

  def withScaleGen(min: PosBigDecimal, max: PosBigDecimal, scale: Scale): Gen[(PosBigDecimal, Scale)] = configurablePosBigDecimalGen(min, max)(Gen.const(scale))

}
