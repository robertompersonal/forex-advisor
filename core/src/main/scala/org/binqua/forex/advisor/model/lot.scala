package org.binqua.forex.advisor.model

import cats.data.Validated
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonPOJOBuilder, JsonSerialize}
import com.fasterxml.jackson.databind.{JsonSerializer, SerializerProvider}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import org.binqua.forex.util.Bug
import org.binqua.forex.util.core._

@JsonSerialize(using = classOf[LotSizeSerializer])
@JsonDeserialize(builder = classOf[LotSize.BuilderForJsonDeserialize])
sealed abstract class LotSize {
  val id: String
  val name: String
  val multiplier: Int
  val amount: Int

  def numberOfUnitsTraded: Int = multiplier * amount
}

final case class StandardLot(amount: Int) extends LotSize {
  val id: String = "standard"
  val name: String = "Standard Contract"
  val multiplier: Int = 100000
}

final case class MiniLot(amount: Int) extends LotSize {
  val id: String = "mini"
  val name: String = "Mini Contract"
  val multiplier: Int = 10000
}

final case class MicroLot(amount: Int) extends LotSize {
  val id: String = "micro"
  val name: String = "Micro Contract"
  val multiplier: Int = 1000
}

object LotSize {

  import cats.implicits._

  val allowedIds = List("micro", "mini", "standard")

  type AMOUNT = Int Refined NonNegative

  def maybeALotSize(id: String, amount: Int): Validated[List[String], LotSize] =
    (
      Either.cond(amount >= 0, amount, List(s"Amount has to be >= 0. $amount is wrong")).toValidated,
      Either.cond(allowedIds.contains(id), id, List(s"Only ${allowedIds.mkString(",")} are allowed. $id as lot size identifier is wrong")).toValidated
    ).mapN((validAmount, validId) =>
      validId match {
        case "micro"    => MicroLot(validAmount)
        case "mini"     => MiniLot(validAmount)
        case "standard" => StandardLot(validAmount)
        case _          => throw new Bug(details = "this cannot happened")
      }
    )

  def toMicro(lotSize: LotSize): MicroLot =
    lotSize match {
      case standardLot: StandardLot => MicroLot(standardLot.amount * 100)
      case miniLot: MiniLot         => MicroLot(miniLot.amount * 10)
      case micro: MicroLot          => micro
    }

  @JsonPOJOBuilder
  case class BuilderForJsonDeserialize(id: String, amount: Int) {
    def build(): LotSize = maybeALotSize(id, amount).unsafe
  }

}

class LotSizeSerializer extends JsonSerializer[LotSize] {

  override def serialize(lotSize: LotSize, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeStartObject()
    gen.writeStringField("id", lotSize.id)
    gen.writeNumberField("amount", lotSize.amount)
    gen.writeEndObject()
  }

}
