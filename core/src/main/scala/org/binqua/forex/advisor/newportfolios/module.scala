package org.binqua.forex.advisor.newportfolios

import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import org.binqua.forex.advisor.util.Model
import org.binqua.forex.util.core

import java.util.UUID
import scala.util.hashing.MurmurHash3

trait PortfoliosBehaviorModule {

  def portfoliosBehavior(persistenceId: PersistenceId): Behavior[DeliveryGuaranteedPortfolios.ExternalCommand]

}

trait PortfoliosEntityFinderModule {

  def ref(entityId: Model.ShardedEntityId): EntityRef[DeliveryGuaranteedPortfolios.ExternalCommand]

}

trait PortfoliosShardingModule {

  def initialisePortfoliosSharding(clusterSharding: ClusterSharding): PortfoliosEntityFinderModule

}

trait DefaultPortfoliosBehaviorModule extends PortfoliosBehaviorModule {

  final def portfoliosBehavior(persistenceId: PersistenceId): Behavior[DeliveryGuaranteedPortfolios.ExternalCommand] =
    DeliveryGuaranteedPortfolios(persistenceId)(
      positionIdFactory = () => UUID.randomUUID(),
      DefaultSupportMessages,
      DefaultErrorMessagesFactory,
      configReader = akkaConfig => core.makeItUnsafe(Config.Validator(akkaConfig)),
      hasher = DefaultHasher
    ).narrow
}

object DefaultHasher extends Hasher {
  override def hash(product: Product): Int = MurmurHash3.productHash(product)
}

trait PortfoliosShardingInitializationModuleWithoutBehavior extends PortfoliosShardingModule {

  this: PortfoliosBehaviorModule =>

  def initialisePortfoliosSharding(clusterSharding: ClusterSharding): PortfoliosEntityFinderModule = {

    val portfoliosEntityKey = EntityTypeKey[DeliveryGuaranteedPortfolios.ExternalCommand]("portfolios")

    clusterSharding.init(
      Entity(portfoliosEntityKey)(createBehavior = entityContext => portfoliosBehavior(PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)))
        .withRole(portfoliosEntityKey.name)
    )

    entityId: Model.ShardedEntityId => clusterSharding.entityRefFor(portfoliosEntityKey, entityId.value)

  }

}

trait NewPortfoliosShardingProductionModule extends PortfoliosShardingInitializationModuleWithoutBehavior with DefaultPortfoliosBehaviorModule {}
