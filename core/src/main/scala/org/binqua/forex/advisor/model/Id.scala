package org.binqua.forex.advisor.model

sealed trait Id {
  val id: String
}

trait Ids {
  val ids: (Id, Id)
}

object Id {

  def idFor(id: String): Id = new SimpleId(id.toLowerCase) {}

  abstract case class SimpleId(id: String) extends Id

  val eur = idFor("eur")

  val gbp = idFor("gbp")

  val jpy = idFor("jpy")

  val spx500 = idFor("spx500")

  val us30 = idFor("us30")

  val usd = idFor("usd")

  val values = List(eur, gbp, jpy, spx500, us30, usd)

}
