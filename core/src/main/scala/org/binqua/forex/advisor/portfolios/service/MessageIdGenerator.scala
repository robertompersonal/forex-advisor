package org.binqua.forex.advisor.portfolios.service

import cats.Eq
import com.fasterxml.jackson.annotation.JsonTypeName
import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonSerializer, SerializerProvider}

import java.util.UUID

trait MessageIdGenerator {

  def newId(): MessageId

}

@JsonDeserialize(using = classOf[MessageIdDeserializer])
@JsonSerialize(using = classOf[MessageIdSerializer])
@JsonTypeName("messageId")
trait MessageId {
  val id: String
}

final case class UUIDMessageId(private val uuid: UUID) extends MessageId {
  val id = uuid.toString
}

object MessageId {
  implicit val equality: Eq[MessageId] = (x: MessageId, y: MessageId) => Eq[String].eqv(x.id, y.id)
}

class MessageIdSerializer extends JsonSerializer[MessageId] {

  override def serialize(messageId: MessageId, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeString(messageId.id)
  }

}

class MessageIdDeserializer extends JsonDeserializer[MessageId] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): MessageId = {
    UUIDMessageId(UUID.fromString(p.getText))
  }
}
