package org.binqua.forex.advisor.model

import cats.Show
import cats.syntax.either._
import cats.syntax.show._
import com.fasterxml.jackson.annotation.JsonTypeName
import com.fasterxml.jackson.databind.annotation._
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.Validation
import org.binqua.forex.util.core.MakeEitherUnsafe

object TradingPair extends Validation {

  object PriceScaleTooBigForCurrencyPair {
    implicit val show: Show[PriceScaleTooBigForCurrencyPair] = (context: PriceScaleTooBigForCurrencyPair) =>
      s"Cannot build a trading pair for ${context.currencyPair.show} with price ${context.price.show}. Details: ${context.reason.show}"
  }

  final case class PriceScaleTooBigForCurrencyPair(
      currencyPair: CurrencyPair,
      price: PosBigDecimal,
      reason: String
  )

  def maybeATradingPair(currencyPair: CurrencyPair, price: PosBigDecimal): Either[String, TradingPair] = {
    PosBigDecimal.rescale(price, theNewScale = currencyPair.quoteCurrency.scale) match {
      case Right(posBigDecimal) => afterValidation.newTradingPair(currencyPair, posBigDecimal).asRight
      case Left(reason)         => PriceScaleTooBigForCurrencyPair(currencyPair, price, reason).show.asLeft
    }
  }

  def priceIncreasedByPipUnits(tradingPair: TradingPair, pipsToBeAdded: Long): Either[String, TradingPair] =
    for {
      theNewValue <- tradingPair.price.increaseByPipUnits(pipsToBeAdded, tradingPair.currencyPair.quoteCurrency.pipPosition)
      theNewTradingPair <- TradingPair.maybeATradingPair(tradingPair.currencyPair, theNewValue)
    } yield theNewTradingPair

  object afterValidation {
    def newTradingPair(currencyPair: CurrencyPair, posBigDecimal: PosBigDecimal) = new TradingPair(currencyPair, posBigDecimal) {}
  }

  @JsonPOJOBuilder
  case class BuilderForJsonDeserialize(currencyPair: CurrencyPair, price: PosBigDecimal) {
    def build(): TradingPair = maybeATradingPair(currencyPair, price).unsafe
  }

}

@JsonDeserialize(builder = classOf[TradingPair.BuilderForJsonDeserialize])
@JsonTypeName("tradingPair")
sealed abstract case class TradingPair(currencyPair: CurrencyPair, price: PosBigDecimal) extends org.binqua.forex.JsonSerializable
