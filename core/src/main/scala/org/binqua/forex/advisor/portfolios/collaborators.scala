package org.binqua.forex.advisor.portfolios

import akka.persistence.typed.PersistenceId
import cats.data.Validated
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.advisor.portfolios.PortfoliosModel._
import org.binqua.forex.util.core
import org.binqua.forex.util.core.{ConfigValidator, UnsafeConfigReader}

import java.util.UUID
import scala.util.{Failure, Success, Try}

trait SupportMessages {
  def portfolioToBeDeletedDoesNotExist(thisPortfolioDoesNotExist: PortfolioName): String

  def portfolioAlreadyExist(portfolioName: PortfolioName): String

  def startingPortfolios(entityId: PersistenceId): String

  def positionToBeDeletedDoesNotExist(toBeDeleted: PositionId): String

  def positionToBeUpdatedDoesNotExist(wrongPositionId: UUID): String

  def portfolioDoesNotExist(name: PortfolioName): String

  def maxNumberOfPortfoliosReached(maxNumberOfPortfoliosAllowed: Int): String

  def tooManyPositions(maxNumberOfPositions: Int): String
}

private[portfolios] object DefaultSupportMessages extends SupportMessages {

  override def portfolioAlreadyExist(portfolioName: PortfolioName): String = s"Portfolio with name ${portfolioName.name} already exist."

  override def portfolioDoesNotExist(portfolioName: PortfolioName): String = s"Portfolio with name ${portfolioName.name} does not exist"

  override def positionToBeDeletedDoesNotExist(toBeDeleted: PositionId): String = s"PositionId to be deleted $toBeDeleted does not exist"

  override def portfolioToBeDeletedDoesNotExist(thisPortfolioDoesNotExist: PortfolioName): String =
    s"Portfolio to be deleted with name ${thisPortfolioDoesNotExist.name} does not exist"

  override def positionToBeUpdatedDoesNotExist(wrongPositionId: PositionId): String = s"PositionId to be updated $wrongPositionId does not exist"

  override def startingPortfolios(persistenceId: PersistenceId): String = s"Portfolio actors with persistenceId ${persistenceId.id} started"

  override def tooManyPositions(maxNumberOfPositions: Int): String =
    s"Sorry!!! Too many positions for one portfolios. You can create max $maxNumberOfPositions per portfolio"

  override def maxNumberOfPortfoliosReached(maxNumberOfPortfoliosAllowed: Int): String =
    s"Sorry!!! Too many portfolios. You can create max $maxNumberOfPortfoliosAllowed portfolios"

}

private[portfolios] object unsafeConfigReader extends UnsafeConfigReader[Config] {
  override def apply(akkaConfig: AkkaConfig): Config = core.makeItUnsafe(ConfigValidator(akkaConfig))
}

sealed abstract case class Config(maxNumberOfPortfolios: Int, maxNumberOfPositions: Int)

object Config {

  import cats.data.Validated
  import cats.instances.list._
  import cats.syntax.apply._
  import cats.syntax.either._

  def unsafe(maxNumberOfPortfolios: Int, maxNumberOfPositions: Int): Config = core.makeItUnsafe(validated(maxNumberOfPortfolios, maxNumberOfPositions))

  private def validated(maxNumberOfPortfolios: Int, maxNumberOfPositions: Int): Validated[List[String], Config] =
    (
      Either.cond(maxNumberOfPortfolios > 0, maxNumberOfPortfolios, List("maxNumberOfPortfolios host has to be > 0")).toValidated,
      Either.cond(maxNumberOfPositions > 0, maxNumberOfPositions, List("maxNumberOfPositions host has to be > 0")).toValidated
    ).mapN((maxNumberOfPortfolios, maxNumberOfPositions) => new Config(maxNumberOfPortfolios, maxNumberOfPositions) {})

}

object ConfigValidator extends ConfigValidator[Config] {

  override def apply(akkaConfig: AkkaConfig): Validated[List[String], Config] = {

    import cats.data.Validated
    import cats.instances.list._
    import cats.syntax.apply._
    import cats.syntax.either._

    val keyPrefix = "org.binqua.forex.advisor.portfolios"

    def toValidated[A](arg: Either[String, A]): Validated[List[String], A] = arg.leftMap(List(_)).toValidated

    def maxNumberOfPortfolios: Either[String, Int] =
      positiveBiggerThanZero(key = s"$keyPrefix.maxNumberOfPortfolios")

    def positiveBiggerThanZero(key: String): Either[String, Int] =
      Try(akkaConfig.getInt(key)) match {
        case Success(value) => Either.cond(value > 0, value, s"$key has to be > 0. $value is not valid.")
        case Failure(_)     => Left(s"$key has to exist and has to be an integer > 0.")
      }

    def maxNumberOfPositions: Either[String, Int] =
      positiveBiggerThanZero(key = s"$keyPrefix.maxNumberOfPositions")

    (
      toValidated(maxNumberOfPortfolios),
      toValidated(maxNumberOfPositions)
    ).mapN((maxNumberOfPortfolios, maxNumberOfPositions) => new Config(maxNumberOfPortfolios, maxNumberOfPositions) {})
      .leftMap(errors => "Portfolios configuration is invalid." :: errors)
  }

}
