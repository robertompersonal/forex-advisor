package org.binqua.forex.advisor.portfolios

import org.binqua.forex.advisor.newportfolios.State.COMMAND_HASH
import org.binqua.forex.advisor.newportfolios.{IdempotenceJournal, PortfoliosSummary, State}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.Portfolio
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.scalacheck.Gen

import java.util.UUID

object StateGen {

  val portfolioGen: Gen[Portfolio] = for {
    numberOfPortfolios <- Gen.chooseNum(1, 10)
    positions <- Gen.listOfN(numberOfPortfolios, PortfoliosGen.recordedPosition)
    aName <- PortfoliosGen.portfolioName
  } yield Portfolio(aName, positions.toSet)

  val journalIdempotencyMapEntryGen: Gen[(String, COMMAND_HASH)] = for {
    key <- Gen.const(IdempotentKey(UUID.randomUUID()))
    hash <- Gen.choose(Int.MinValue, Int.MaxValue)
  } yield (key.key.toString, hash)

  val portfoliosState: Gen[State] = (for {
    numberOfPortfolios <- Gen.chooseNum(2, 3)
    portfolios <- Gen.listOfN(numberOfPortfolios, portfolioGen)
    numberOfJournalEntries <- Gen.chooseNum(3, 10)
    journal <- Gen.listOfN(numberOfJournalEntries, journalIdempotencyMapEntryGen)
  } yield PortfoliosSummary.validate(portfolios.toSet).map(State(_, IdempotenceJournal.withMostRecentOnly(journal.toMap)))).flatMap {
    case Right(value) => Gen.const(value)
    case Left(_)      => Gen.fail
  }

  val portfoliosSummary: Gen[PortfoliosSummary] = portfoliosSummary(0, 3)

  def nonEmptyPortfoliosSummary: Gen[PortfoliosSummary] = portfoliosSummary(1, 3)

  def emptyPortfoliosSummary: Gen[PortfoliosSummary] = portfoliosSummary(0, 0)

  private def portfoliosSummary(minNumberOfPortfolios: Int, maxNumberOfPortfolios: Int): Gen[PortfoliosSummary] = {
    require(minNumberOfPortfolios <= maxNumberOfPortfolios)
    (for {
      numberOfPortfolios <- Gen.chooseNum(minNumberOfPortfolios, maxNumberOfPortfolios)
      portfolios <- Gen.listOfN(numberOfPortfolios, portfolioGen)
    } yield PortfoliosSummary.validate(portfolios.toSet)).flatMap {
      case Right(value) => Gen.const(value)
      case Left(_)      => Gen.fail
    }
  }

}
