package org.binqua.forex.running.services.httpclient

import akka.actor.typed.{Behavior, RecipientRef}
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol

trait HttpClientSubscriberServiceModule {

  def httpClientSubscriberServiceBehavior(
      httpSubscriberRef: RecipientRef[HttpClientSubscriberProtocol.SubscribeTo]
  ): Behavior[HttpClientSubscriberServiceProtocol.Command]

}

trait HttpClientSubscriberServiceModuleImpl extends HttpClientSubscriberServiceModule {

  override def httpClientSubscriberServiceBehavior(
      httpSubscriberRef: RecipientRef[HttpClientSubscriberProtocol.SubscribeTo]
  ): Behavior[HttpClientSubscriberServiceProtocol.Command] = HttpClientSubscriberService(httpSubscriberRef, SupportMessages).narrow

}
