package org.binqua.forex.running.services.internalfeed

import akka.actor.typed.Behavior

trait InternalFeedServiceModule {

  def internalFeedServiceBehavior(): Behavior[InternalFeedService.PrivateMessage]

}

trait InternalFeedServiceModuleImpl extends InternalFeedServiceModule {

  override def internalFeedServiceBehavior(): Behavior[InternalFeedService.PrivateMessage] = InternalFeedService(SupportMessages).narrow

}
