package org.binqua.forex.running.services.internalfeed

import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol

trait SupportMessages {
  def serviceStopped(serviceKey: ServiceKey[SocketIOClientProtocol.FeedContent], self: ActorRef[InternalFeedService.PrivateMessage]): String

  def serviceRegistered(key: ServiceKey[_], value: ActorRef[SocketIOClientProtocol.FeedContent]): String
}

private[internalfeed] object SupportMessages extends SupportMessages {

  override def serviceRegistered(key: ServiceKey[_], ref: ActorRef[SocketIOClientProtocol.FeedContent]): String = s"Service with key ${key.id} name ${ref.path.name} path ${ref.path} has been registered"

  override def serviceStopped(key: ServiceKey[SocketIOClientProtocol.FeedContent], ref: ActorRef[InternalFeedService.PrivateMessage]): String = s"Service with key ${key.id} name ${ref.path.name} path ${ref.path} has been stopped"
}
