package org.binqua.forex.running.common

import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol

object ServicesKeys {

  val SocketIOClientServiceKey: ServiceKey[SocketIOClientProtocol.Command] = ServiceKey[SocketIOClientProtocol.Command]("SocketIOClientService")

  val HttpClientSubscriberServiceKey: ServiceKey[HttpClientSubscriberServiceProtocol.SubscribeTo] = ServiceKey[HttpClientSubscriberServiceProtocol.SubscribeTo]("HttpClientSubscriberService")

  val InternalFeedServiceKey: ServiceKey[SocketIOClientProtocol.FeedContent] = ServiceKey[SocketIOClientProtocol.FeedContent]("InternalFeedService")

}
