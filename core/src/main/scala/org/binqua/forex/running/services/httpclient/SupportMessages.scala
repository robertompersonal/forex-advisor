package org.binqua.forex.running.services.httpclient

import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol.SubscribeTo

private[services] trait SupportMessages {

  def serviceStopped(key: ServiceKey[_], self: ActorRef[SubscribeTo]): String

  def serviceRegistered(key: ServiceKey[_], value: ActorRef[SubscribeTo]): String

}

private[services] object SupportMessages extends SupportMessages {

  override def serviceStopped(key: ServiceKey[_], self: ActorRef[SubscribeTo]): String = s"Service with key ${key.id} name ${self.path.name} path ${self.path} has been stopped"

  override def serviceRegistered(key: ServiceKey[_], self: ActorRef[SubscribeTo]): String = s"Service with key ${key.id} name ${self.path.name} path ${self.path} has been registered"

}
