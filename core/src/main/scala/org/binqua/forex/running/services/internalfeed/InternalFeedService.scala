package org.binqua.forex.running.services.internalfeed

import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, PostStop}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, UnhandledBehavior}

private[internalfeed] object InternalFeedService {

  sealed trait PrivateMessage

  final case class WrappedReceptionistResponse(registered: Receptionist.Registered) extends PrivateMessage

  final case class WrappedSocketIOClientFeedContent(feedContent: SocketIOClientProtocol.FeedContent) extends PrivateMessage

  def apply(supportMessages: SupportMessages): Behavior[PrivateMessage] = Behaviors.setup[PrivateMessage] { context =>

    def setUpBehavior(serviceKey: ServiceKey[SocketIOClientProtocol.FeedContent],
                      receptionistAdapter: ActorRef[Receptionist.Registered],
                      socketIOClientAdapter: ActorRef[SocketIOClientProtocol.FeedContent],
                      unhandledBehavior: UnhandledBehavior[PrivateMessage]): Behavior[PrivateMessage] = {

      def serviceRegisteredBehavior(serviceRegistered: Boolean): Behavior[PrivateMessage] = Behaviors.receiveMessage[PrivateMessage] {
        case WrappedSocketIOClientFeedContent(SocketIOClientProtocol.FeedContent(content)) =>
          context.log.info(content)
          Behaviors.same
        case msg: WrappedReceptionistResponse =>
          if (serviceRegistered)
            unhandledBehavior.withMsg[PrivateMessage](msg)(TheOnlyHandledMessages(WrappedSocketIOClientFeedContent))
          else {
            context.log.info(supportMessages.serviceRegistered(msg.registered.key, msg.registered.serviceInstance(serviceKey)))
            serviceRegisteredBehavior(serviceRegistered = true)
          }
      }.receiveSignal {
        case (_, PostStop) =>
          context.log.info(supportMessages.serviceStopped(serviceKey, context.self))
          Behaviors.ignore
      }

      context.system.receptionist ! Receptionist.Register(serviceKey, socketIOClientAdapter, receptionistAdapter)

      serviceRegisteredBehavior(serviceRegistered = false)
    }

    setUpBehavior(
      serviceKey = ServicesKeys.InternalFeedServiceKey,
      receptionistAdapter = context.messageAdapter(WrappedReceptionistResponse),
      socketIOClientAdapter = context.messageAdapter(WrappedSocketIOClientFeedContent),
      unhandledBehavior = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[PrivateMessage](context)
    )
  }

}
