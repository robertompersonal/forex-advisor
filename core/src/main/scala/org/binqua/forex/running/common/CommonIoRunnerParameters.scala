package org.binqua.forex.running.common

import java.io.{PrintWriter, StringWriter}

import org.apache.commons.cli.{CommandLine, Options}

import scala.util.{Failure, Success, Try}

class CommonIoRunnerParameters(args: Array[String]) extends RunnerParameters {

  import org.apache.commons.cli.{DefaultParser, HelpFormatter}

  private val configFileNameTag = "configFileName"

  val options = new Options

  val configFileNameArg = new org.apache.commons.cli.Option(configFileNameTag, configFileNameTag, true, "")
  configFileNameArg.setRequired(true)
  options.addOption(configFileNameArg)

  val parser = new DefaultParser
  val formatter = new HelpFormatter

  val commandLine: Try[CommandLine] = Try(parser.parse(options, args))

  override val configFileName: Either[String, String] = extract(configFileNameTag)(commandLine)

  def extract(tag: String): Try[CommandLine] => Either[String, String] = {
    case Success(value) => Right(value.getOptionValue(tag)).withLeft[String]
    case Failure(_) => usage
  }

  private def usage = {
    val stringWriter = new StringWriter()
    formatter.printUsage(new PrintWriter(stringWriter), 100, "", options)
    stringWriter.close()
    Left(stringWriter.toString).withRight[String]
  }
}
