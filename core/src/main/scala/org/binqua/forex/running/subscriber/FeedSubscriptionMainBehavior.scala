package org.binqua.forex.running.subscriber

import akka.NotUsed
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.management.scaladsl.AkkaManagement
import org.binqua.forex.feed.httpclient.{ProductionShardedModule, ShardedHttpClientSubscriberModule}
import org.binqua.forex.feed.socketio.connectionregistry.{
  ProductionShardedModule => ConnectionRegistryProductionShardedModule,
  ShardedModule => ConnectionRegistryShardedModule
}
import org.binqua.forex.feed.starter.{ProductionSubscriptionStarterModule, SubscriptionStarterModule, SubscriptionStarterProtocol}
import org.binqua.forex.running.common.MainBehavior
import org.binqua.forex.running.services.httpclient.{HttpClientSubscriberServiceModule, HttpClientSubscriberServiceModuleImpl}
import org.binqua.forex.running.services.internalfeed.{InternalFeedServiceModule, InternalFeedServiceModuleImpl}

trait FeedSubscriptionMainBehavior extends MainBehavior[NotUsed] {

  this: SubscriptionStarterModule
    with ShardedHttpClientSubscriberModule
    with ConnectionRegistryShardedModule
    with HttpClientSubscriberServiceModule
    with InternalFeedServiceModule =>

  override def mainBehavior(): Behavior[NotUsed] =
    Behaviors.setup[NotUsed] { context =>
      AkkaManagement(context.system).start()

      val sharding = ClusterSharding(context.system)

      context.spawn(internalFeedServiceBehavior(), name = "internalFeedService")

      context.spawn(httpClientSubscriberServiceBehavior(httpSubscriberRef = initialisedHttpSubscriber(sharding).ref), name = "httpClientSubscriberService")

      val subscriptionStarter =
        context.spawn(subscriptionStarterBehavior(connectionRegistryRef = initialiseConnectionRegistry(sharding).ref), name = "feedSubscriptionStarter")

      subscriptionStarter ! SubscriptionStarterProtocol.StartASubscription

      Behaviors.ignore

    }

}

trait FeedSubscriptionAppBehavior
    extends FeedSubscriptionMainBehavior
    with ProductionSubscriptionStarterModule
    with ProductionShardedModule
    with ConnectionRegistryProductionShardedModule
    with HttpClientSubscriberServiceModuleImpl
    with InternalFeedServiceModuleImpl
