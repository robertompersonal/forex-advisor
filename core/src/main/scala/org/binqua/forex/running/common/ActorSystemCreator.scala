package org.binqua.forex.running.common

import akka.actor.typed.ActorSystem

trait ActorSystemCreator[T] {

  this: ActorSystemSpawner[T] =>

  def runApplicationWith(args: Array[String]): Unit = {
    runApplicationWith(args, (behavior, actorSystemConfig) => ActorSystem(behavior, "forex-cluster", actorSystemConfig)) match {
      case Left(error) => println(error)
      case Right(_) => println("ActorSystem starting!")
    }

  }

}
