package org.binqua.forex.feed.starter.controller

import akka.actor.typed.Behavior
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.{CollaboratorsWatcherModule, CollaboratorsWatcherProtocol}
import org.binqua.forex.util.ChildMaker

import scala.concurrent.duration._

trait SubscriptionControllerModule {

  def subscriptionControllerBehavior(): Behavior[SubscriptionControllerProtocol.Command]

}

trait SubscriptionControllerModuleImpl extends SubscriptionControllerModule {

  this: CollaboratorsWatcherModule =>

  override def subscriptionControllerBehavior(): Behavior[SubscriptionControllerProtocol.Command] = SubscriptionController(
    heartBeatInterval = 7.seconds,
    collaboratorsWatcherChildMaker = ChildMaker.fromContext[SubscriptionControllerProtocol.Message]("collaboratorsWatcher").withBehavior[CollaboratorsWatcherProtocol.Start](collaboratorsWatcherBehavior())(),
    supportMessagesMaker = SupportMessages.supportMessagesMaker
  ).narrow

}


