package org.binqua.forex.feed.reader.model

import java.time.LocalDateTime

import org.binqua.forex.feed.reader

object Quote {

  def from(incomingQuoteRecordedEvent: reader.Reader.IncomingQuoteRecordedEvent): Quote =
    new Quote(Rates.nonValidated(incomingQuoteRecordedEvent), incomingQuoteRecordedEvent.updated) {}

}

sealed abstract case class Quote(rates: Rates, updated: LocalDateTime)



