package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, PostStop, Signal}
import io.socket.client.{IO, Socket}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.LogProtocol.Info
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol._
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.core.UnsafeConfigReader

object CustomisableSocketIOClientActor {
  type LogActorMaker = ActorContext[Command] => Behavior[LogProtocol.Log] => ActorRef[LogProtocol.Log]
}

trait CustomisableSocketIOClientActor {

  this: LogBehavior =>

  def apply(unsafeConfigReader: UnsafeConfigReader[Config], logActorFactory: CustomisableSocketIOClientActor.LogActorMaker): Behavior[Command] =
    Behaviors.withTimers[Command](timers =>
      Behaviors.setup { context =>
        val socketIOClientConfig = unsafeConfigReader(context.system.settings.config)

        context.log.info(
          s"Using following config for connect to socketIO: loginToken ${socketIOClientConfig.loginToken} connectionUrl ${socketIOClientConfig.connectionUrl}"
        )

        def unhandledBehavior = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Command](context)

        val logActor: ActorRef[LogProtocol.Log] = logActorFactory(context)(logBehavior)

        def connectingBehavior(socket: Socket, replyToAboutConnection: ActorRef[ConnectionStatus]): Behavior[Command] = {

          def waitForPostStopSignal: PartialFunction[(ActorContext[Command], Signal), Behavior[Command]] = {
            case (_, PostStop) =>
              socket.disconnect()
              context.log.info(s"Socket with id ${socket.id()} disconnected. SocketIOClientActor Actor Terminated")
              Behaviors.same
          }

          def internalConnectedReceivedBehavior(): Behavior[Command] = {
            implicit val handledMessages = TheOnlyHandledMessages(InternalConnected, InternalConnectionErrorMustToReconnect, SubscribeTo)

            Behaviors
              .receiveMessage[Command] {
                case InternalConnected(_) =>
                  replyToAboutConnection ! Connected(SocketId(socket.id()))
                  Behaviors.same

                case InternalConnectionErrorMustToReconnect(actualAttemptToConnect) =>
                  retryToConnectToSocketIOHandler(actualAttemptToConnect)

                case SubscribeTo(currencyPair, requestedSocketId, subscriptionReplyTo, internalFeedActor) =>
                  if (socket.id() == requestedSocketId.id) {

                    val socketIOCurrencyPairKey = CurrencyPair.toExternalIdentifier(currencyPair)

                    socket.off(socketIOCurrencyPairKey)

                    socket.once(
                      socketIOCurrencyPairKey,
                      (_: AnyRef) => {
                        logActor ! Info(s"$currencyPair has been successfully subscribed to socket id ${requestedSocketId.id}")
                        subscriptionReplyTo ! Subscribed(result = true, currencyPair, SocketId(requestedSocketId.id))
                      }
                    )
                    socket.on(socketIOCurrencyPairKey, (rates: Array[AnyRef]) => toOption(rates).foreach(internalFeedActor ! FeedContent(_)))

                  } else {
                    context.log
                      .info(s"Ignoring subscription request for $currencyPair with old socket id ${requestedSocketId.id}. New socket id ${socket.id()}")
                    subscriptionReplyTo ! Subscribed(result = false, currencyPair, requestedSocketId)
                  }
                  Behaviors.same
                case u: Connect                                => unhandledBehavior.withMsg[Command](u)(handledMessages)
                case u: InternalRetryToConnectToSocketIOServer => unhandledBehavior.withMsg[Command](u)(handledMessages)
              }
              .receiveSignal(
                waitForPostStopSignal
              )
          }

          def retryToConnectToSocketIOHandler(attemptToConnect: Int): Behavior[Command] = {
            logActor ! Info(s"Retrying to connect in ${socketIOClientConfig.retryToConnectInterval}")
            timers.startSingleTimer("serverTempError", InternalRetryToConnectToSocketIOServer(attemptToConnect), socketIOClientConfig.retryToConnectInterval)

            implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect, InternalConnected, InternalConnectionErrorMustToReconnect)
            Behaviors
              .receiveMessage[Command] {
                case InternalRetryToConnectToSocketIOServer(attemptToConnect) =>
                  logActor ! Info(s"Retrying to connect now. Attempt number $attemptToConnect")
                  connectingBehavior(
                    connectToSocketIOServer(logActor, context.self, replyToAboutConnection, socketIOClientConfig, attemptToConnect),
                    replyToAboutConnection
                  )

                case u: Connect                                => unhandledBehavior.withMsg(u)
                case u: InternalConnected                      => unhandledBehavior.withMsg(u)
                case u: InternalConnectionErrorMustToReconnect => unhandledBehavior.withMsg(u)
                case u: SubscribeTo                            => unhandledBehavior.withMsg(u)
              }
              .receiveSignal(
                waitForPostStopSignal
              )
          }

          {
            implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect, InternalConnected, InternalConnectionErrorMustToReconnect)
            Behaviors
              .receiveMessage[Command]({
                case InternalConnected(_) =>
                  replyToAboutConnection ! Connected(SocketId(socket.id()))
                  internalConnectedReceivedBehavior()

                case InternalConnectionErrorMustToReconnect(actualAttemptToConnect) =>
                  retryToConnectToSocketIOHandler(actualAttemptToConnect)

                case u: Connect                                => unhandledBehavior.withMsg(u)
                case u: InternalRetryToConnectToSocketIOServer => unhandledBehavior.withMsg(u)
                case u: SubscribeTo                            => unhandledBehavior.withMsg(u)
              })
              .receiveSignal(
                waitForPostStopSignal
              )
          }
        }

        implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect)
        Behaviors
          .receiveMessage[Command]({
            case Connect(replyToAboutConnection) =>
              replyToAboutConnection ! Connecting
              connectingBehavior(
                connectToSocketIOServer(logActor, context.self, replyToAboutConnection, socketIOClientConfig, attemptToConnect = 0),
                replyToAboutConnection
              )

            case u: InternalConnected                      => unhandledBehavior.withMsg(u)
            case u: InternalConnectionErrorMustToReconnect => unhandledBehavior.withMsg(u)
            case u: InternalRetryToConnectToSocketIOServer => unhandledBehavior.withMsg(u)
            case u: SubscribeTo                            => unhandledBehavior.withMsg(u)
          })
          .receiveSignal {
            case (_, PostStop) =>
              context.log.info("SocketIOClientActor Actor terminated without connecting to SocketIOServer")
              Behaviors.same
          }
      }
    )

  private def connectToSocketIOServer(
      logActor: ActorRef[LogProtocol.Log],
      self: ActorRef[Command],
      actorWaitingForConnection: ActorRef[Connected],
      socketIOClientConfig: Config,
      attemptToConnect: Int
  ): Socket = {
    val socket = createSocket(socketIOClientConfig)

    def toMessage(key: String, args: Array[AnyRef]): String = {
      key
        .replace("_", " ")
        .concat(if (args != null && args.length > 0) args.mkString(" with args [", ",", "]") else " with no args")
    }

    def reportEventInfo(event: String, args: Array[AnyRef]): Unit = logActor ! LogProtocol.Info(toMessage(event, args))

    def reportEventError(event: String, args: Array[AnyRef]): Unit = logActor ! LogProtocol.Error(toMessage(event, args))

    socket.on(
      Socket.EVENT_CONNECT,
      (args: Array[AnyRef]) => {
        reportEventInfo(Socket.EVENT_CONNECT, Array(socket.id()))
        self ! InternalConnected(actorWaitingForConnection)
      }
    )

    socket
      .on(
        Socket.EVENT_CONNECT_TIMEOUT,
        (args: Array[AnyRef]) => {
          reportEventInfo(Socket.EVENT_CONNECT_TIMEOUT, args)
        }
      )
      .on(
        Socket.EVENT_RECONNECT,
        (args: Array[AnyRef]) => {
          reportEventInfo(Socket.EVENT_RECONNECT, args)
        }
      )
      .on(
        Socket.EVENT_RECONNECTING,
        (args: Array[AnyRef]) => {
          reportEventInfo(Socket.EVENT_RECONNECTING, args)
        }
      )
      .on(
        Socket.EVENT_RECONNECT_ATTEMPT,
        (args: Array[AnyRef]) => {
          reportEventInfo(Socket.EVENT_RECONNECT_ATTEMPT, args)
        }
      )
      .on(
        Socket.EVENT_RECONNECT_FAILED,
        (args: Array[AnyRef]) => {
          reportEventInfo(Socket.EVENT_RECONNECT_FAILED, args)
        }
      )

    socket
      .on(
        Socket.EVENT_CONNECT_ERROR,
        (args: Array[AnyRef]) => {
          reportEventError(Socket.EVENT_CONNECT_ERROR, args)
        }
      )
      .on(
        Socket.EVENT_ERROR,
        (args: Array[AnyRef]) => {
          reportEventError(Socket.EVENT_ERROR, args)
          if (!socket.connected())
            self ! InternalConnectionErrorMustToReconnect(attemptToConnect + 1)
        }
      )

    socket.connect()
  }

  def toOption(rates: Array[AnyRef]): Option[String] = if (rates != null && rates.length > 0) Some(rates(0).toString) else None

  private def createSocket(socketIOClientConfig: Config): Socket = {
    val options = new IO.Options
    options.forceNew = true
    options.query = "access_token=" + socketIOClientConfig.loginToken
    IO.socket(socketIOClientConfig.connectionUrl, options)
  }

}

private object SocketIOClientActor extends CustomisableSocketIOClientActor with ProductionLogBehavior
