package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.typed.Behavior
import org.binqua.forex.util.core

trait SocketIOClientModule {

  def socketIOClient(): Behavior[SocketIOClientProtocol.Command]

}

trait ProductionSocketIOClientModule extends SocketIOClientModule {

  object SocketIOClientActor extends CustomisableSocketIOClientActor with ProductionLogBehavior {}

  override def socketIOClient(): Behavior[SocketIOClientProtocol.Command] =
    SocketIOClientActor(akkaConfig => core.makeItUnsafe(ConfigValidator(akkaConfig)), ResumableLogActor.defaultLogActorMaker)

}
