package org.binqua.forex.feed.httpclient

import cats.data.Validated
import cats.syntax.either._
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.util.ValidateConfigByReference._
import org.binqua.forex.util.core.ConfigValidator
import org.binqua.forex.util.{ErrorSituation, ErrorSituationHandler}

sealed abstract case class Config(apiHost: String, apiPort: String, connectionUrl: String, loginToken: String)

object Config {
  private def toConfig(apiHost: String, apiPort: String, connectionUrl: String, loginToken: String): Config =
    new Config(apiHost = apiHost, apiPort = apiPort, connectionUrl = connectionUrl, loginToken = loginToken) {}

  def validated(apiHost: String, apiPort: String, connectionUrl: String, loginToken: String): Validated[List[String], Config] = {
    import cats.instances.list._
    import cats.syntax.apply._
    (
      Either.cond(apiHost.trim.nonEmpty, apiHost, List("apiHost has to be not empty")).toValidated,
      Either.cond(apiPort.trim.nonEmpty, apiPort, List("apiPort has to be not empty")).toValidated,
      Either.cond(connectionUrl.trim.nonEmpty, connectionUrl, List("connectionUrl has to be not empty")).toValidated,
      Either.cond(loginToken.trim.nonEmpty, loginToken, List("loginToken has to be not empty")).toValidated
    ).mapN((apiHost, apiPort, connectionUrl, loginToken) =>
      toConfig(apiHost = apiHost, apiPort = apiPort, connectionUrl = connectionUrl, loginToken = loginToken)
    )
  }
}

object ConfigValidator extends ConfigValidator[Config] {

  import cats.syntax.either._

  override def apply(akkaConfig: AkkaConfig): Validated[List[String], Config] = {

    def prefix(toBePrefixed: String) = s"org.binqua.forex.feed.httpclient.$toBePrefixed"

    val apiHost = prefix("apihost")
    val apiPort = prefix("apiport")
    val connectionUrl = prefix("connectionUrl")
    val loginToken = "org.binqua.forex.feed.loginToken"

    val referenceConfigurationToBeParsed: String =
      s"""
         |{
         |$apiHost= ""
         |$apiPort= 1
         |$connectionUrl = ""
         |$loginToken =  ""
         |}
    """.stripMargin

    def customValidation(resolvedConfig: AkkaConfig): Either[List[String], Config] =
      Config
        .validated(
          resolvedConfig.getString(apiHost),
          resolvedConfig.getString(apiPort),
          resolvedConfig.getString(connectionUrl),
          resolvedConfig.getString(loginToken)
        )
        .toEither

    (for {
      resolvedConfig <- basicValidation(configurationIdentifier = "HttpClientSubscriber", akkaConfig, referenceConfigurationToBeParsed)
      aValidConfig <- customValidation(resolvedConfig)
    } yield aValidConfig).toValidated
  }

}

private object ErrorSituationHandlerImpl extends ErrorSituationHandler {
  override def show(theErrorSituation: ErrorSituation): String =
    theErrorSituation match {
      case se: HttpSubscriptionFailedErrorSituation =>
        s"Received a response from server but parser failed:\nbody=${se.body} currencyPair=${se.currencyPair} socketId=${se.socketId} jsError=${se.jsError}"
      case se: HttpSubscriptionWrongHttpCodeErrorSituation =>
        s"Received a response from server but error code is not 200:\nstatusCode=${se.statusCode} currencyPair=${se.currencyPair} socketId=${se.socketId}"
    }
}
