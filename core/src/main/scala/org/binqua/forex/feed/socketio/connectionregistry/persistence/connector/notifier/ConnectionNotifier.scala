package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import cats.Eq
import cats.syntax.option._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol.{NewSocketIdAvailable, Notify, _}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.implicits.instances.socketId._
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages

import scala.util.{Failure, Success}

private[notifier] object ConnectionNotifier {

  def apply(implicit timeout: Timeout, supportMessages: SupportMessages): Behavior[Message] =
    Behaviors.setup(parentContext => {

      def firstBehavior(unhandledBehavior: AkkaUtil.UnhandledBehavior[Message]): Behavior[Message] = {

        def notifyFeedSubscriptionStarter(actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId], socketId: SocketId): Unit = {
          parentContext.ask(actorToBeNotify, SubscriptionStarterProtocol.NewSocketId(_, socketId))({
            case Success(SubscriptionStarterProtocol.NewFeedSocketIdUpdated(theSocketId)) => PrivateSubscriptionStarted(theSocketId)
            case Failure(_)                                                               => RetryToStartASubscriptionFailedDueToTimeout(socketId, actorToBeNotify)
          })
        }

        def noFullyInitializedBehavior(
            maybeTheActorToBeNotify: Option[ActorRef[SubscriptionStarterProtocol.NewSocketId]],
            maybeSocketId: Option[SocketId]
        ): Behavior[Message] = {
          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Notify, NewSocketIdAvailable)
          Behaviors.receiveMessage({
            case Notify(actorToBeNotify) =>
              maybeSocketId match {
                case Some(socketId) =>
                  notifyFeedSubscriptionStarter(actorToBeNotify, socketId)
                  fullyInitializedBehavior(actorToBeNotify, socketId, retryAttempts = 0)
                case None =>
                  noFullyInitializedBehavior(actorToBeNotify.some, None)
              }
            case NewSocketIdAvailable(socketId) =>
              parentContext.log.info(supportMessages.newSocketIdAvailable(socketId))

              maybeTheActorToBeNotify match {
                case Some(actorToBeNotify) =>
                  notifyFeedSubscriptionStarter(actorToBeNotify, socketId)
                  fullyInitializedBehavior(actorToBeNotify, socketId, retryAttempts = 0)
                case None =>
                  noFullyInitializedBehavior(None, socketId.some)
              }

            case u: PrivateSubscriptionStarted                  => unhandledBehavior.withMsg(u)
            case u: RetryToStartASubscriptionFailedDueToTimeout => unhandledBehavior.withMsg(u)
          })
        }

        def fullyInitializedBehavior(
            lastActorToBeNotifyReferenceReceived: ActorRef[SubscriptionStarterProtocol.NewSocketId],
            lastSocketIdReceived: SocketId,
            retryAttempts: Int
        ): Behavior[Message] =
          Behaviors.receiveMessage({

            case Notify(newActorToBeNotify) =>
              parentContext.log.info(supportMessages.actorRequestedNewSocketIdNotification(lastSocketIdReceived, newActorToBeNotify))

              notifyFeedSubscriptionStarter(newActorToBeNotify, lastSocketIdReceived)

              fullyInitializedBehavior(newActorToBeNotify, lastSocketIdReceived, retryAttempts = 0)

            case PrivateSubscriptionStarted(socketId) =>
              parentContext.log.info(supportMessages.newSocketIdNotified(socketId))
              Behaviors.same

            case RetryToStartASubscriptionFailedDueToTimeout(aPreviousSocketId, actorToBeNotify) =>
              if (Eq.eqv(aPreviousSocketId, lastSocketIdReceived)) {
                parentContext.log.info(
                  supportMessages.startSubscriptionAttemptFailedRetry(aPreviousSocketId, actorToBeNotify, retryAttempts + 1, timeout.duration)
                )
                notifyFeedSubscriptionStarter(lastActorToBeNotifyReferenceReceived, aPreviousSocketId)
                fullyInitializedBehavior(lastActorToBeNotifyReferenceReceived, aPreviousSocketId, retryAttempts + 1)
              } else {
                parentContext.log.info(
                  supportMessages.startSubscriptionAttemptWithOldSocketIdAborted(newSocketId = lastSocketIdReceived, oldSocketId = aPreviousSocketId)
                )
                fullyInitializedBehavior(lastActorToBeNotifyReferenceReceived, lastSocketIdReceived = lastSocketIdReceived, retryAttempts = retryAttempts + 0)
              }

            case NewSocketIdAvailable(newSocketId) =>
              parentContext.log.info(supportMessages.newSocketIdAvailable(newSocketId))

              notifyFeedSubscriptionStarter(lastActorToBeNotifyReferenceReceived, newSocketId)

              fullyInitializedBehavior(lastActorToBeNotifyReferenceReceived, newSocketId, retryAttempts = 0)
          })

        noFullyInitializedBehavior(maybeTheActorToBeNotify = None, maybeSocketId = None)
      }

      firstBehavior(AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext))

    })
}

object ConnectionNotifierProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Notify(subscriptionStarterRef: ActorRef[SubscriptionStarterProtocol.NewSocketId]) extends Command

  final case class NewSocketIdAvailable(socketId: SocketId) extends Command

  private[notifier] sealed trait PrivateCommand extends Message

  private[notifier] final case class PrivateSubscriptionStarted(socketId: SocketId) extends PrivateCommand

  private[notifier] final case class RetryToStartASubscriptionFailedDueToTimeout(
      socketId: SocketId,
      actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId]
  ) extends PrivateCommand

}
