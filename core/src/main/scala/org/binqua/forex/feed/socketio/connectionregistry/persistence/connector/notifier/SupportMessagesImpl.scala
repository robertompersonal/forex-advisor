package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol

import scala.concurrent.duration.FiniteDuration

private[notifier] object SupportMessagesImpl extends SupportMessages {

  override def actorRequestedNewSocketIdNotification(
      actualSocketId: SocketId,
      actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId]
  ): String = s"Actor with name ${actorToBeNotify.path.name} (${actorToBeNotify.path}) requested to be notify of new socketId $actualSocketId"

  override def newSocketIdAvailable(socketId: SocketId): String = s"New socketId $socketId available"

  override def newSocketIdNotified(socketId: SocketId): String =
    s"New socketId $socketId has been notified. Subscription maybe started"

  override def startSubscriptionAttemptFailedRetry(
      socketId: SocketId,
      actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId],
      attempts: Int,
      timeout: FiniteDuration
  ): String =
    s"Notification of new socketId $socketId failed. No ack from actor ${actorToBeNotify.path.name} (${actorToBeNotify.path}) before $timeout. This is attempt $attempts"

  override def startSubscriptionAttemptWithOldSocketIdAborted(
      newSocketId: SocketId,
      oldSocketId: SocketId
  ): String = s"Notification of new socketId $oldSocketId failed, but meanwhile a new socketId is available $newSocketId"
}
