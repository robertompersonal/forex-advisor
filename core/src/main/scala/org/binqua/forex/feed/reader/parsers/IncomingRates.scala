package org.binqua.forex.feed.reader.parsers

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.instances.list._
import cats.syntax.apply._
import cats.syntax.either._
import cats.syntax.show._
import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonSerializer, SerializerProvider}
import org.binqua.forex.advisor.model
import org.binqua.forex.advisor.model.PosBigDecimal
import org.binqua.forex.advisor.model.PosBigDecimal.posBigDecimalShow
import org.binqua.forex.util.Validation
import play.api.libs.json._

import scala.util.Either.cond

object IncomingRates extends Validation {

  def validated(rates: Seq[BigDecimal]): Validated[List[String], IncomingRates] = validated(rates(0), rates(1), rates(2), rates(3))

  def validatedX(rates: Seq[PosBigDecimal]): Validated[List[String], IncomingRates] = validated(rates.map(_.value))

  def unsafe(rates: List[PosBigDecimal]): IncomingRates =
    validated(rates.map(_.value)) match {
      case Valid(incomingRates)     => incomingRates
      case Invalid(errors: List[_]) => throw new IllegalArgumentException(errors.mkString("[", ",", "]"))
    }

  private def validated(sell: BigDecimal, buy: BigDecimal, maxBuyOfTheDay: BigDecimal, minSellOfTheDay: BigDecimal): Validated[List[String], IncomingRates] = {
    def toError(bigDecimalTag: String): String => String = (error: String) => toValidatedError(error, bigDecimalTag)

    validated(
      aSeqOf(
        model.PosBigDecimal.maybeAPosBigDecimal(minSellOfTheDay).leftMap(toError("minSellOfTheDay")),
        model.PosBigDecimal.maybeAPosBigDecimal(sell).leftMap(toError("sell")),
        model.PosBigDecimal.maybeAPosBigDecimal(buy).leftMap(toError("buy")),
        model.PosBigDecimal.maybeAPosBigDecimal(maxBuyOfTheDay).leftMap(toError("maxBuyOfTheDay"))
      )
    )
  }

  def toValidatedError(errorReason: String, field: String): String = s"$field is wrong: $errorReason"

  private def aSeqOf(
      sell: ErrorOr[PosBigDecimal],
      buy: ErrorOr[PosBigDecimal],
      maxBuyOfTheDay: ErrorOr[PosBigDecimal],
      minSellOfTheDay: ErrorOr[PosBigDecimal]
  ): Validated[List[String], Seq[PosBigDecimal]] = {
    (
      toValidated(sell),
      toValidated(buy),
      toValidated(maxBuyOfTheDay),
      toValidated(minSellOfTheDay)
    ).mapN((minSellOfTheDay, sell, buy, maxBuyOfTheDay) => Seq(sell, buy, maxBuyOfTheDay, minSellOfTheDay))
  }

  private def validated(toBeValidated: Validated[List[String], Seq[PosBigDecimal]]): Validated[List[String], IncomingRates] = {
    toBeValidated match {
      case invalid @ Invalid(_) => invalid
      case Valid(a) =>
        val Seq(sell: PosBigDecimal, buy: PosBigDecimal, maxBuyOfTheDay: PosBigDecimal, minSellOfTheDay: PosBigDecimal) = a
        (
          toValidated(sellValidation(sell, buy)),
          toValidated(buyValidation(buy, maxBuyOfTheDay)),
          toValidated(minSellValidation(minSellOfTheDay, sell))
        ).mapN((sell, buy, minSellOfTheDay) => new IncomingRates(sell, buy, maxBuyOfTheDay, minSellOfTheDay) {})
    }

  }

  private def sellValidation(sell: PosBigDecimal, buy: PosBigDecimal) =
    cond(sell.value < buy.value, sell, s"sell ${sell.show} has to be < than buy ${buy.show}")

  private def buyValidation(buy: PosBigDecimal, maxBuyOfTheDay: PosBigDecimal) =
    cond(buy.value <= maxBuyOfTheDay.value, buy, s"buy ${buy.show} has to be <= than maxBuyOfTheDay ${maxBuyOfTheDay.show}")

  private def minSellValidation(minSellOfTheDay: PosBigDecimal, sell: PosBigDecimal) =
    cond(minSellOfTheDay.value <= sell.value, minSellOfTheDay, s"minSellOfTheDay ${minSellOfTheDay.show} has to be <= than sell ${sell.show}")

}

@JsonDeserialize(using = classOf[IncomingRatesDeserializer])
@JsonSerialize(using = classOf[IncomingRatesSerializer])
sealed abstract case class IncomingRates(sell: PosBigDecimal, buy: PosBigDecimal, maxBuyOfTheDay: PosBigDecimal, minSellOfTheDay: PosBigDecimal)

class IncomingRatesDeserializer extends JsonDeserializer[IncomingRates] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): IncomingRates = {
    import IncomingRatesSerializer._
    import com.fasterxml.jackson.databind.JsonNode
    import play.api.libs.functional.syntax._

    val node: JsonNode = p.getCodec.readTree(p)

    implicit val incomingRatesRead: Reads[IncomingRates] = (
      (JsPath \ sellIdentifer).read[BigDecimal] and
        (JsPath \ buyIdentifer).read[BigDecimal] and
        (JsPath \ maxIdentifer).read[BigDecimal] and
        (JsPath \ minIdentifer).read[BigDecimal]
    )((sell, buy, max, min) =>
      IncomingRates.unsafe(
        List(PosBigDecimal.unsafeFrom(sell), PosBigDecimal.unsafeFrom(buy), PosBigDecimal.unsafeFrom(max), PosBigDecimal.unsafeFrom(min))
      )
    )

    val toBeParsed = node.asText()

    incomingRatesRead.reads(Json.parse(toBeParsed)) match {
      case JsSuccess(value, _) => value
      case JsError(errors)     => throw new IllegalArgumentException(s"problem parsing $toBeParsed. Details $errors")
    }

  }

}

object IncomingRatesSerializer {
  val sellIdentifer: String = "s"
  val buyIdentifer: String = "b"
  val maxIdentifer: String = "max"
  val minIdentifer: String = "min"
}

class IncomingRatesSerializer extends JsonSerializer[IncomingRates] {

  import IncomingRatesSerializer._

  override def serialize(incomingRates: IncomingRates, gen: JsonGenerator, serializers: SerializerProvider): Unit = {

    implicit val posBigDecimalWrites = new Writes[PosBigDecimal] {
      def writes(posBigDecimal: PosBigDecimal) = JsNumber(posBigDecimal.value)
    }

    implicit val incomingRatesWrites = new Writes[IncomingRates] {
      def writes(incomingRates: IncomingRates) =
        Json.obj(
          sellIdentifer -> incomingRates.sell,
          buyIdentifer -> incomingRates.buy,
          maxIdentifer -> incomingRates.maxBuyOfTheDay,
          minIdentifer -> incomingRates.minSellOfTheDay
        )
    }
    gen.writeString(Json.stringify(Json.toJson(incomingRates)))
  }
}
