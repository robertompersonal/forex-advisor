package org.binqua.forex.feed.notifier

import cats.kernel.Eq
import org.binqua.forex.advisor.model.{AccountCurrency, PosBigDecimal}
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.implicits.instances.accountCurrency._
import org.binqua.forex.implicits.instances.baseCurrency._
import org.binqua.forex.implicits.instances.id._
import org.binqua.forex.implicits.instances.quoteCurrency._
import org.binqua.forex.util.{Bug, Validation}

object collaborators {

  sealed trait ChangeType

  case object DueToUpdatedQuoteDirectly extends ChangeType

  case object DueToPipValue extends ChangeType

  trait UpdatingAccountFactory {

    def validate(updatedQuote: Quote, extraQuoteForPipCalculation: Quote, accountCurrency: AccountCurrency, updateDueTo: ChangeType): Validation.ErrorOr[UpdatingAccount]

    def create(updatedQuote: Quote, accountCurrency: AccountCurrency): Option[UpdatingAccount]

  }

  object UpdatingAccount extends UpdatingAccountFactory {

    import cats.Eq

    def validate(updatedQuote: Quote, extraQuoteForPipCalculation: Quote, accountCurrency: AccountCurrency, updateDueTo: ChangeType): Validation.ErrorOr[UpdatingAccount] = {

      def validationMessage: String = s"Cannot create an instance of UpdatingAccount with:" +
        s"\nupdatedQuote= $updatedQuote\nextraQuoteForPipCalculation= " +
        s"$extraQuoteForPipCalculation\naccountCurrency= $accountCurrency\nupdateDueTo= $updateDueTo"

      def assureThatUpdatedQuoteIsNotRelatedToAccountCurrency: Option[Boolean] = Option.when(accountCurrency.isNotEqualToACurrencyIn(updatedQuote.rates.currencyPair))(true)

      def assureThatExtraQuoteIsRelatedToAccountCurrency: Option[Boolean] = Option.when(accountCurrency.isEqualToACurrencyIn(extraQuoteForPipCalculation.rates.currencyPair))(true)

      def assureThatACurrencyInExtraQuoteIsEqToUpdatedQuoteCurrency: Option[Boolean] = Option.when(
        Eq.eqv(extraQuoteForPipCalculation.rates.currencyPair.baseCurrency, updatedQuote.rates.currencyPair.quoteCurrency.baseCurrency) ||
          Eq.eqv(extraQuoteForPipCalculation.rates.currencyPair.quoteCurrency.baseCurrency, updatedQuote.rates.currencyPair.quoteCurrency.baseCurrency))(true)

      (for {
        _ <- assureThatUpdatedQuoteIsNotRelatedToAccountCurrency
        _ <- assureThatExtraQuoteIsRelatedToAccountCurrency
        _ <- assureThatACurrencyInExtraQuoteIsEqToUpdatedQuoteCurrency
      } yield {
        new UpdatingAccount(updatedQuote, Some(extraQuoteForPipCalculation), accountCurrency, updateDueTo) {}
      }).toRight(validationMessage)

    }

    def create(updatedQuote: Quote, accountCurrency: AccountCurrency): Option[UpdatingAccount] = {
      val acId = accountCurrency.toId
      Option.when(
        Eq.eqv(updatedQuote.rates.currencyPair.baseCurrency.toId, acId) ||
          Eq.eqv(updatedQuote.rates.currencyPair.quoteCurrency.toId, acId))(new UpdatingAccount(updatedQuote, None, accountCurrency, DueToUpdatedQuoteDirectly) {})
    }

  }

  sealed abstract case class UpdatingAccount(incomingUpdatedQuote: Quote, maybeAnExtraQuote: Option[Quote], theAccountCurrency: AccountCurrency, updateDueTo: ChangeType) {

    import org.binqua.forex.implicits.instances.currencyPair._

    val pipValue: PosBigDecimal = {
      maybeAnExtraQuote match {
        case None =>
          incomingUpdatedQuote.rates.currencyPair.toIds.ids match {
            case (baseCurrency, _) if Eq.eqv(baseCurrency, theAccountCurrency.toId) => incomingUpdatedQuote.rates.currencyPair.quoteCurrency.pipValue / incomingUpdatedQuote.rates.buy
            case (_, quoteCurrency) if Eq.eqv(quoteCurrency, theAccountCurrency.toId) => incomingUpdatedQuote.rates.currencyPair.quoteCurrency.pipValue
            case _ => throw new Bug(s"this should not have happened $this")
          }
        case Some(extraQuoteValue) =>
          extraQuoteValue.rates.currencyPair.toIds.ids match {
            case (exqBc, _) if Eq.eqv(exqBc, theAccountCurrency.toId) => incomingUpdatedQuote.rates.currencyPair.quoteCurrency.pipValue / extraQuoteValue.rates.buy
            case (_, exqQc) if Eq.eqv(exqQc, theAccountCurrency.toId) => incomingUpdatedQuote.rates.currencyPair.quoteCurrency.pipValue * extraQuoteValue.rates.buy
            case _ => throw new Bug(s"this should not have happened $this")
          }
      }
    }

  }

}
