package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import cats.syntax.option._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

object Connector {

  sealed trait Message

  sealed trait Command extends Message

  final case class Notify(ref: ActorRef[SubscriptionStarterProtocol.NewSocketId]) extends Command

  case object Connect extends Command

  sealed trait PrivateCommand extends Message

  private[connector] final case class WrappedSocketIOClientServiceResponse(response: SocketIOClientServiceProtocol.NewSocketId) extends PrivateCommand

  private[connector] def apply(
      socketIOClientServiceMaker: CHILD_MAKER[Message, SocketIOClientServiceProtocol.Connect],
      connectionNotifierMaker: CHILD_MAKER[Message, ConnectionNotifierProtocol.Command]
  ): Behavior[Message] =
    Behaviors.setup[Message](parentContext => {

      def unhandledBehavior: AkkaUtil.UnhandledBehavior[Message] = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext)

      val thisAsSocketIOClientServiceRegistryAdapter: ActorRef[SocketIOClientServiceProtocol.NewSocketId] =
        parentContext.messageAdapter[SocketIOClientServiceProtocol.NewSocketId](WrappedSocketIOClientServiceResponse)

      def spawnAndWatchConnectionNotifier(): ActorRef[ConnectionNotifierProtocol.Command] = {
        val connectionNotifier = connectionNotifierMaker(parentContext)
        parentContext.watch(connectionNotifier)
        connectionNotifier
      }

      def connectReceivedBehavior(
          actualSocketIOClientServiceRef: ActorRef[SocketIOClientServiceProtocol.Connect],
          actualConnectionNotifier: ActorRef[ConnectionNotifierProtocol.Command],
          maybeASubscriptionStarterToBeNotify: Option[ActorRef[SubscriptionStarterProtocol.NewSocketId]],
          maybeASocketId: Option[SocketId]
      ): Behavior[Message] =
        Behaviors
          .receiveMessage[Message] {
            case Notify(toBeNotify) =>
              actualConnectionNotifier ! ConnectionNotifierProtocol.Notify(toBeNotify)
              connectReceivedBehavior(actualSocketIOClientServiceRef, actualConnectionNotifier, toBeNotify.some, maybeASocketId)

            case WrappedSocketIOClientServiceResponse(SocketIOClientServiceProtocol.NewSocketId(aNewSocketId)) =>
              actualConnectionNotifier ! ConnectionNotifierProtocol.NewSocketIdAvailable(aNewSocketId)
              connectReceivedBehavior(actualSocketIOClientServiceRef, actualConnectionNotifier, maybeASubscriptionStarterToBeNotify, aNewSocketId.some)

            case Connect => unhandledBehavior.withMsg[Message](Connect)(TheOnlyHandledMessages(Notify, WrappedSocketIOClientServiceResponse))

          }
          .receiveSignal({
            case (_, ChildFailed(`actualSocketIOClientServiceRef`, _)) =>
              val newSocketIOClientService: ActorRef[SocketIOClientServiceProtocol.Connect] = socketIOClientServiceMaker(parentContext)
              parentContext.watch(newSocketIOClientService)

              newSocketIOClientService ! SocketIOClientServiceProtocol.Connect(thisAsSocketIOClientServiceRegistryAdapter)

              connectReceivedBehavior(newSocketIOClientService, actualConnectionNotifier, maybeASubscriptionStarterToBeNotify, None)

            case (_, ChildFailed(`actualConnectionNotifier`, _)) =>
              val newConnectionNotifier = connectionNotifierMaker(parentContext)
              parentContext.watch(newConnectionNotifier)

              maybeASubscriptionStarterToBeNotify.foreach(newConnectionNotifier ! ConnectionNotifierProtocol.Notify(_))
              maybeASocketId.foreach(newConnectionNotifier ! ConnectionNotifierProtocol.NewSocketIdAvailable(_))

              connectReceivedBehavior(actualSocketIOClientServiceRef, newConnectionNotifier, maybeASubscriptionStarterToBeNotify, maybeASocketId)
          })

      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect)

      Behaviors.receiveMessage[Message] {
        case Connect =>
          val newSocketIOClientService = socketIOClientServiceMaker(parentContext)
          parentContext.watch(newSocketIOClientService)
          newSocketIOClientService ! SocketIOClientServiceProtocol.Connect(thisAsSocketIOClientServiceRegistryAdapter)
          connectReceivedBehavior(
            actualSocketIOClientServiceRef = newSocketIOClientService,
            actualConnectionNotifier = spawnAndWatchConnectionNotifier(),
            maybeASubscriptionStarterToBeNotify = None,
            maybeASocketId = None
          )

        case u: Notify                               => unhandledBehavior.withMsg(u)
        case u: WrappedSocketIOClientServiceResponse => unhandledBehavior.withMsg(u)
      }
    })

}
