package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.ActorRef
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol

trait Support[T] {

  def bothCollaboratorsTerminated(attempt: Int): T

  def newSubscriptionResponse(response: SubscriberProtocol.Response): T

  def collaboratorsTerminationInProgressMessageIgnored(subscriberResponse: SubscriberProtocol.Response): T

  def currencyPairsSubscriberTerminated(currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command], subscriberRef: ActorRef[SubscriberProtocol.Command]): T

  def subscriberTerminated(currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command], subscriberRef: ActorRef[SubscriberProtocol.Command]): T

  def oneCollaboratorStillNotTerminated(iterable: Iterable[String], attempt: Int): T

}
