package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.ActorRef
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.manager.externalreader.currencypairs.{ChildRef, SubscriptionsType}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModel.State.Summary
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.{Subscribe, Subscriptions}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.CurrencyPairSubscriberConfig
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol

import scala.concurrent.duration.FiniteDuration

object CurrencyPairsSubscriberModel {

  def actorNameDecorator(toBeDecorated: String, currencyPair: CurrencyPair): String =
    s"$toBeDecorated-${CurrencyPair.toExternalIdentifier(currencyPair).toLowerCase().replace("/", "_")}"

  object StateUnderConstruction {
    def emptyState: StateUnderConstruction = new StateUnderConstruction(Map())
  }

  case class StateUnderConstruction(private val subscriptions: SubscriptionsType) {
    def record(currencyPair: CurrencyPair, actorRef: ChildRef): StateUnderConstruction =
      StateUnderConstruction(subscriptions.updated(currencyPair, (Some(actorRef), None)))

    def done(): StatePartiallyConstructed = StatePartiallyConstructed(this.subscriptions)
  }

  case class StatePartiallyConstructed(subscriptions: SubscriptionsType)

  object State {

    case class Summary(socketId: SocketId, waitingResult: Set[CurrencyPair], subscribed: Set[CurrencyPair], unsubscribed: Set[CurrencyPair]) {
      def currencyPairs: Set[CurrencyPair] = waitingResult ++ subscribed ++ unsubscribed
    }

    def createAState(statePartiallyConstructed: StatePartiallyConstructed, subscribe: Subscribe): State =
      State(statePartiallyConstructed.subscriptions, subscribe)

  }

  case class State private (private val subscriptions: SubscriptionsType, subscribe: Subscribe) {
    def withTheNewState[B](f: State => B): B = f(this)

    require(
      subscriptions.keys.iterator.sameElements(subscribe.currencyPairs),
      s"Currencies to subscribe to:\n${subscribe.currencyPairs}\nand currencies recorded:\n${subscriptions.keys.iterator} are not the same!"
    )

    def summary: Summary = Summary(subscribe.socketId, subscriptions.filter(_._2._2.isEmpty).keys.toSet, subscribed, unsubscribed)

    def isAlreadySubscribed(currencyPair: CurrencyPair): Boolean = waitingForResult.contains(currencyPair)

    def hasSubscriptionsResultFor(currencyPair: CurrencyPair): Boolean = !waitingForResult.contains(currencyPair)

    private def bySubscribe(subscribe: Boolean): ((CurrencyPair, (Option[ChildRef], Option[Boolean]))) => Boolean =
      kv =>
        kv._2._2 match {
          case Some(`subscribe`) => true
          case _                 => false
        }

    def asSubscription: CurrencyPairsSubscriberProtocol.Subscriptions =
      Subscriptions(this.subscribe.socketId, this.subscribed, this.unsubscribed ++ waitingForResult)

    def subscribed: Set[CurrencyPair] = subscriptions.filter(bySubscribe(subscribe = true)).keys.toSet

    def unsubscribed: Set[CurrencyPair] = subscriptions.filter(bySubscribe(subscribe = false)).keys.toSet

    def waitingForResult: Set[CurrencyPair] = subscriptions.filter(_._2._2.isEmpty).keys.toSet

    def childrenWaitingForResult: Set[ActorRef[CurrencyPairSubscriberProtocol.Command]] = subscriptions.values.filter(_._2.isEmpty).map { x => x._1.get }.toSet

    def someSubscriptionsAreStillMissing: Boolean = subscriptions.exists(_._2._2.isEmpty)

    def currencyPairSubscribed(result: Boolean, currencyPair: CurrencyPair): State =
      changeSubscriptionToSubscribe(currencyPair, subscriptionResult = result)

    def subscriberOf(currencyPair: CurrencyPair): Option[ChildRef] = subscriptions.get(currencyPair).flatMap(_._1)

    private def changeSubscriptionToSubscribe(currencyPair: CurrencyPair, subscriptionResult: Boolean): State = {
      subscriptions.get(currencyPair) match {
        case Some(_) => State(subscriptions.updated(currencyPair, (None, Some(subscriptionResult))), subscribe)
        case None    => this
      }
    }
  }

  case class Config(currencyPairSubscriberConfig: CurrencyPairSubscriberConfig, timeout: FiniteDuration)

}
