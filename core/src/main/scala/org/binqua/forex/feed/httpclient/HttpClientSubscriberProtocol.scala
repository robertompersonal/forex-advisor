package org.binqua.forex.feed.httpclient

import akka.actor.typed.ActorRef
import akka.http.scaladsl.model.HttpResponse
import com.fasterxml.jackson.annotation.{JsonSubTypes, JsonTypeName}
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import org.binqua.forex.JsonSerializable
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId

object HttpClientSubscriberProtocol {

  sealed trait Message

  sealed trait Command extends Message

  sealed trait PrivateMessage extends Message

  final case class SubscribeTo(currencyPair: CurrencyPair, socketId: SocketId, replyTo: ActorRef[Response]) extends Command with JsonSerializable

  private[httpclient] final case class InternalHttpBodyReceivedFailed(
      ex: Throwable,
      currencyPair: CurrencyPair,
      socketId: SocketId,
      replyTo: ActorRef[Response]
  ) extends PrivateMessage

  private[httpclient] final case class InternalHttpBodyReceivedSuccessfully(
      body: String,
      currencyPair: CurrencyPair,
      socketId: SocketId,
      replyTo: ActorRef[Response]
  ) extends PrivateMessage

  private[httpclient] final case class InternalSubscriptionException(ex: Throwable, currencyPair: CurrencyPair, socketId: SocketId, replyTo: ActorRef[Response])
      extends PrivateMessage

  private[httpclient] final case class InternalSubscriptionResponse(
      httpResponse: HttpResponse,
      currencyPair: CurrencyPair,
      socketId: SocketId,
      replyTo: ActorRef[Response]
  ) extends PrivateMessage

  sealed trait Response extends JsonSerializable

  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[ServerError], name = "serverError"),
      new JsonSubTypes.Type(value = classOf[ParserError], name = "parserError")
    )
  )
  sealed trait FailureReason extends JsonSerializable

  @JsonDeserialize(using = classOf[ServerErrorDeserializer])
  sealed trait ServerError extends FailureReason

  class ServerErrorDeserializer extends StdDeserializer[FailureReason](classOf[ServerError]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): ServerError = ServerError
  }

  @JsonTypeName("serverError")
  final case object ServerError extends ServerError

  @JsonDeserialize(using = classOf[ParserErrorDeserializer])
  sealed trait ParserError extends FailureReason

  @JsonTypeName("parserError")
  final case object ParserError extends ParserError

  class ParserErrorDeserializer extends StdDeserializer[FailureReason](classOf[ParserError]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): ParserError = ParserError
  }

  final case class SubscriptionSuccess(currencyPair: CurrencyPair, socketId: SocketId) extends Response

  final case class SubscriptionFailure(currencyPair: CurrencyPair, socketId: SocketId, failureReason: FailureReason) extends Response

}
