package org.binqua.forex.feed.manager.externalreader

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext
import cats.syntax.show._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.{CurrencyPairsSubscriberModel, CurrencyPairsSubscriberProtocol}
import org.binqua.forex.implicits.instances.socketId._

package object currencypairs {

  type CurrencyPairSubscriberActorMaker = (
      ActorContext[CurrencyPairsSubscriberProtocol.Message],
      CurrencyPair,
      SocketId,
      ActorRef[SocketIOClientProtocol.SubscriptionsResult]
  ) => ActorRef[CurrencyPairSubscriberProtocol.Command]

  type ChildRef = ActorRef[CurrencyPairSubscriberProtocol.Command]

  type SubscriptionsType = Map[CurrencyPair, (Option[ChildRef], Option[Boolean])]

  type SupportMessages = (Boolean, CurrencyPair, CurrencyPairsSubscriberModel.State.Summary) => String

  val reportNewSubscription: SupportMessages = (subscribeResult, currencyPair, summary: CurrencyPairsSubscriberModel.State.Summary) =>
    s"${firstPart(subscribeResult, currencyPair, summary)}\n${secondPart(summary)}"

  def firstPart(subscribeResult: Boolean, currencyPair: CurrencyPair, summary: CurrencyPairsSubscriberModel.State.Summary): String =
    s"Subscription to ${CurrencyPair.toExternalIdentifier(currencyPair)} feed via ${summary.socketId.show} ${if (subscribeResult) "successful"
    else "unsuccessful"}"

  def secondPart(summary: CurrencyPairsSubscriberModel.State.Summary): String = {
    def toString(currencyPairs: Set[CurrencyPair]) = currencyPairs.map(CurrencyPair.toExternalIdentifier).mkString("[", ",", "]")

    def summaryReport(summary: CurrencyPairsSubscriberModel.State.Summary): String =
      s"${toString(summary.subscribed)} subscribed\n${toString(summary.unsubscribed)} not subscribed\n${toString(summary.waitingResult)} waiting to be subscribed"

    val socketId = summary.socketId.show

    if (summary.waitingResult.isEmpty) {
      if (summary.unsubscribed.isEmpty)
        s"Subscription attempt complete: subscribed to all feeds ${toString(summary.currencyPairs)} via $socketId"
      else
        s"Subscription attempt complete:\n${summaryReport(summary)}"
    } else {
      s"Subscription attempt underway:\n${summaryReport(summary)}"
    }

  }

}
