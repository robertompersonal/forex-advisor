package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.receptionist.Receptionist.{Register, Registered}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

private[service] object SocketIOClientService {

  def apply(socketIOClientMaker: CHILD_MAKER[Message, SocketIOClientProtocol.Command], supportMessages: SupportMessages): Behavior[Message] =
    Behaviors.setup[Message](parentContext => {

      def firstBehavior(
          thisAsSocketIOClientResponseReceiver: ActorRef[SocketIOClientProtocol.ConnectionStatus],
          thisAsReceptionistResponseReceiver: ActorRef[Receptionist.Registered],
          unhandledBehavior: AkkaUtil.UnhandledBehavior[Message]
      ): Behaviors.Receive[Message] = {

        def connectRequestedHandler(replyToWhenConnected: ActorRef[NewSocketId]): Behavior[Message] = {

          def connectRequestedBehavior(newSocketIOClient: ActorRef[SocketIOClientProtocol.Connect], handled: TheOnlyHandledMessages): Behavior[Message] =
            Behaviors
              .receiveMessage[Message] {

                case msg @ WrappedReceptionistRegistered(registered) =>
                  if (handled.contains(WrappedReceptionistRegistered)) {
                    parentContext.log
                      .info(supportMessages.socketIOClientServiceRegistered(registered.getServiceInstance(SocketIOClientServiceModule.SocketIOClientKey)))
                    connectRequestedBehavior(newSocketIOClient, handled.minus(WrappedReceptionistRegistered))
                  } else
                    unhandledBehavior.withMsg[Message](msg)(handled)

                case msg @ WrappedSocketIOClientResponse(response) =>
                  response match {
                    case SocketIOClientProtocol.Connecting =>
                      if (handled.contains(SocketIOClientProtocol.Connecting)) {
                        parentContext.log.info(supportMessages.connecting)
                        connectRequestedBehavior(newSocketIOClient, handled.minus(SocketIOClientProtocol.Connecting))
                      } else
                        unhandledBehavior.withMsg[Message](msg)(handled)

                    case SocketIOClientProtocol.Connected(aNewSocketId) =>
                      replyToWhenConnected ! NewSocketId(aNewSocketId)
                      connectRequestedBehavior(newSocketIOClient, handled)
                  }
                case u: Connect => unhandledBehavior.withMsg[Message](u)(handled)

              }
              .receiveSignal({
                case (_, ChildFailed(`newSocketIOClient`, _)) =>
                  connectRequestedHandler(replyToWhenConnected)
              })

          val newSocketIOClient = socketIOClientMaker(parentContext)
          parentContext.watch(newSocketIOClient)

          parentContext.system.receptionist ! Register(SocketIOClientServiceModule.SocketIOClientKey, newSocketIOClient, thisAsReceptionistResponseReceiver)

          newSocketIOClient ! SocketIOClientProtocol.Connect(thisAsSocketIOClientResponseReceiver)

          connectRequestedBehavior(
            newSocketIOClient,
            TheOnlyHandledMessages(WrappedReceptionistRegistered, SocketIOClientProtocol.Connecting, SocketIOClientProtocol.Connected)
          )
        }

        implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Connect)
        Behaviors.receiveMessage[Message] {
          case Connect(replyToWhenConnected) =>
            connectRequestedHandler(replyToWhenConnected)

          case u: WrappedSocketIOClientResponse => unhandledBehavior.withMsg(u)
          case u: WrappedReceptionistRegistered => unhandledBehavior.withMsg(u)
        }
      }

      firstBehavior(
        parentContext.messageAdapter[SocketIOClientProtocol.ConnectionStatus](WrappedSocketIOClientResponse),
        parentContext.messageAdapter[Registered](WrappedReceptionistRegistered),
        AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext)
      )
    })

}

object SocketIOClientServiceProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Connect(replyToWhenConnected: ActorRef[NewSocketId]) extends Command

  sealed trait PrivateCommand extends Message

  final case class WrappedSocketIOClientResponse(response: SocketIOClientProtocol.ConnectionStatus) extends PrivateCommand

  final case class WrappedReceptionistRegistered(response: Registered) extends PrivateCommand

  final case class NewSocketId(socketId: SocketId)

}
