package org.binqua.forex.feed.reader

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, RetentionCriteria}
import akka.persistence.typed.{PersistenceId, RecoveryCompleted}
import monocle.macros.GenLens
import org.binqua.forex.JsonSerializable
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.notifier
import org.binqua.forex.feed.notifier.NewIncomingQuoteManager
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.feed.reader.parsers.{IncomingQuoteReader, IncomingRates}
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.ChildMaker.CHILD_MAKER
import play.api.libs.json._

import java.time.LocalDateTime

object Reader {

  sealed trait Command

  final case class RecordIncomingQuoteCommand(quoteAsJson: String) extends Command

  sealed trait Event extends JsonSerializable

  final case class IncomingQuoteRecordedEvent(updated: LocalDateTime, incomingRates: IncomingRates, currencyPair: CurrencyPair) extends Event

  var quoteUpdatedManagerChild: ActorRef[NewIncomingQuoteManager.Command] = _

  val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("QuotesFeed")

  def apply(
      entityId: String,
      incomingQuoteReader: IncomingQuoteReader,
      quoteUpdatedManagerChildMaker: CHILD_MAKER[Command, NewIncomingQuoteManager.Command]
  ): Behavior[Command] =
    Behaviors.setup { context =>
      context.log.info("Starting QuotesFeedActor with persistenceId {}", entityId)

      quoteUpdatedManagerChild = quoteUpdatedManagerChildMaker(context)

      context.watch(quoteUpdatedManagerChild)

      EventSourcedBehavior[Command, IncomingQuoteRecordedEvent, QuotesFeedState](
        persistenceId = PersistenceId.ofUniqueId(entityId),
        emptyState = QuotesFeedState(Map()),
        commandHandler = commandHandler(context, incomingQuoteReader),
        eventHandler = (state, recordedQuoteEvent) => state.updated(recordedQuoteEvent)
      ).withRetention(RetentionCriteria.snapshotEvery(numberOfEvents = numbOfEvents(context), keepNSnapshots = numOfSnapshotsToKeep(context)))
        .receiveSignal {
          case (state, RecoveryCompleted) =>
            state.quoteMap.values.foreach(incomingQuoteRecordedEvent =>
              quoteUpdatedManagerChild ! notifier.NewIncomingQuoteManager.QuoteUpdated(Quote.from(incomingQuoteRecordedEvent))
            )
          case (_, Terminated(_)) =>
            quoteUpdatedManagerChild = quoteUpdatedManagerChildMaker(context)
            context.watch(quoteUpdatedManagerChild)
            Behaviors.same
        }
    }

  def commandHandler(
      actorContext: ActorContext[Command],
      incomingQuoteReader: IncomingQuoteReader
  ): (QuotesFeedState, Command) => Effect[IncomingQuoteRecordedEvent, QuotesFeedState] = { (state, command) =>
    command match {
      case RecordIncomingQuoteCommand(incomingQuoteAsJson) =>
        incomingQuoteReader.parse(incomingQuoteAsJson) match {
          case JsSuccess(incomingQuote, _) =>
            if (isTheMostRecent(incomingQuote, state))
              Effect
                .persist(incomingQuote)
                .thenRun((_: QuotesFeedState) => {
                  quoteUpdatedManagerChild ! notifier.NewIncomingQuoteManager.QuoteUpdated(Quote.from(incomingQuote))
                })
            else
              Effect.none
          case JsError(seqOfErrors) =>
            actorContext.log.error(seqOfErrors.toString())
            Effect.none
        }
    }
  }

  private def numOfSnapshotsToKeep(context: ActorContext[Command]): Int =
    context.system.settings.config.getInt("org.binqua.forex.number.of.snapshots.to.keep")

  private def numbOfEvents(context: ActorContext[Command]): Int =
    context.system.settings.config.getInt("org.binqua.forex.number.of.messages.for.snapshot")

  def isTheMostRecent(newIncomingQuote: IncomingQuoteRecordedEvent, state: QuotesFeedState): Boolean =
    state.quoteMap.get(newIncomingQuote.currencyPair.toId.id) match {
      case None                                 => true
      case Some(lastIncomingQuoteRecordedEvent) => lastIncomingQuoteRecordedEvent.updated.isBefore(newIncomingQuote.updated)
    }

}

object QuotesFeedState {
  val emptyState = QuotesFeedState(Map())
}

case class QuotesFeedState(quoteMap: Map[String, IncomingQuoteRecordedEvent]) extends JsonSerializable {
  def updated(incomingQuoteRecordedEvent: IncomingQuoteRecordedEvent): QuotesFeedState =
    GenLens[QuotesFeedState](_.quoteMap)
      .modify(initialMap => initialMap + (incomingQuoteRecordedEvent.currencyPair.toId.id -> incomingQuoteRecordedEvent))(this)
}
