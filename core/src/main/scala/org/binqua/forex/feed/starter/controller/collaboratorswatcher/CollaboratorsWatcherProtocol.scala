package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol

object CollaboratorsWatcherProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Start(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  private[collaboratorswatcher] final case object InternalCheckThatBothCollaboratorsHaveBeenTerminated extends PrivateCommand

  private[collaboratorswatcher] final case class WrappedSubscriberResponse(response: SubscriberProtocol.Response) extends PrivateCommand

  sealed trait Response

  final case class SubscriptionDone(socketId: SocketId) extends Response

  final case class SubscriptionRunning(socketId: SocketId) extends Response

}
