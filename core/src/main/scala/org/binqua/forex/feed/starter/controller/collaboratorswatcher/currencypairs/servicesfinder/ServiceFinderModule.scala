package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder

import akka.actor.typed.Behavior

trait ServiceFinderModule {

  def serviceFinderBehavior(): Behavior[ServicesFinderProtocol.Command]

}

trait ServiceFinderModuleImpl extends ServiceFinderModule {

  def serviceFinderBehavior(): Behavior[ServicesFinderProtocol.Command] = ServicesFinder(SupportMessagesImpl).narrow

}

