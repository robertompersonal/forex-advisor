package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.syntax.either._
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.util.ValidateConfigByReference
import org.binqua.forex.util.core.ConfigValidator

import scala.concurrent.duration.FiniteDuration

final case class SocketId(id: String)

sealed abstract case class Config(connectionUrl: String, loginToken: String, retryToConnectInterval: FiniteDuration)

object Config {
  private def toConfig(connectionUrl: String, loginToken: String, retryToConnectInterval: FiniteDuration): Config =
    new Config(connectionUrl, loginToken, retryToConnectInterval) {}

  def validated(connectionUrl: String, loginToken: String, retryToConnectInterval: FiniteDuration): Validated[List[String], Config] = {
    import cats.instances.list._
    import cats.syntax.apply._
    (
      Either.cond(connectionUrl.trim.nonEmpty, connectionUrl, List("connectionUrl has to be not empty")).toValidated,
      Either.cond(loginToken.trim.nonEmpty, loginToken, List("loginToken has to be not empty")).toValidated
    ).mapN((connectionUrl, loginToken) => toConfig(connectionUrl, loginToken, retryToConnectInterval))
  }
}

object ConfigValidator extends ConfigValidator[Config] {

  override def apply(akkaConfig: AkkaConfig): Validated[List[String], Config] = {

    val connectionUrl = "org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.connectionUrl"
    val loginToken = "org.binqua.forex.feed.loginToken"
    val retryToConnectIntervalKeyDuration =
      "org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.retryToConnectInterval"

    val referenceConfigurationToBeParsed: String =
      s"""
         |{
         |$connectionUrl = ""
         |$loginToken =  ""
         |$retryToConnectIntervalKeyDuration = ""
         |}
    """.stripMargin

    import cats.syntax.either._

    def customValidation(resolvedConfig: com.typesafe.config.Config): Either[List[String], Config] =
      ValidateConfigByReference.validateADurationConfigValue(resolvedConfig, retryToConnectIntervalKeyDuration) match {
        case Invalid(e) => Left(e)
        case Valid(finiteDuration) =>
          Config.validated(resolvedConfig.getString(connectionUrl), resolvedConfig.getString(loginToken), finiteDuration).toEither
      }

    (for {
      akkaConfig <- ValidateConfigByReference.basicValidation("SocketIOClient", akkaConfig, referenceConfigurationToBeParsed)
      aValidConfig <- customValidation(akkaConfig)
    } yield aValidConfig).toValidated
  }
}
