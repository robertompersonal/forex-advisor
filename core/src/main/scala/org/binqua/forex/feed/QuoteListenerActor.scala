package org.binqua.forex.feed

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair, QuoteSubscriptionFinder}
import org.binqua.forex.feed.protocol.{Command, UpdatedCommand, _}

object QuoteListenerActor {

  def apply(currencyPair: CurrencyPair,
            accountCurrency: AccountCurrency,
            actorToBeNotified: ActorRef[Notification],
            quoteSubscriptionFinder: QuoteSubscriptionFinder): Behavior[Command] = Behaviors.setup { context =>

    quoteSubscriptionFinder.lookup(currencyPair, accountCurrency)(context.self).foreach(context.system.eventStream ! _)

    newB(None, None, context)
  }

  def newB[F, S](fm: Option[UpdatedCommand], sm: Option[UpdatedCommand], context: ActorContext[Command]): Behavior[Command] = {
    Behaviors.receiveMessage[Command] {
      case NewPosition(portfolioIdentifier, typedRecordedPositionData) =>
        context.log.info(s"received NewPosition $typedRecordedPositionData")
        Behaviors.same
      case uc: UpdatedCommand =>

        context.log.info(s"received an UpdatedCommand $uc")
        Behaviors.same
    }
  }

  //  def calculatePipValue(firstPair: UpdatedCommand, secondPair: UpdatedCommand): BigDecimal = firstPair.recordedQuoteEvent.quoteCurrency.pipValue / secondPair.recordedQuoteEvent.rate.buy

  def nextBehavior(fm: Option[UpdatedCommand], sm: Option[UpdatedCommand], context: ActorContext[Command]): Behavior[Command] =
    (fm, sm) match {
      case (Some(firstPair), Some(secondPair)) =>
        // we got both, we can calculate the value
        //        val secondBaseCurrencyPipValue = calculatePipValue(firstPair, secondPair)
        newB(fm, sm, context)
      case _ =>
        Behaviors.same
    }


}
