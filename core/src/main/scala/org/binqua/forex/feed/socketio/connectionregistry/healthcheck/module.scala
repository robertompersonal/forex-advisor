package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import akka.actor.typed.Behavior
import org.binqua.forex.util.core.ConfigValidator

trait CassandraHealthCheckModule {

  def healthCheckBehavior(): Behavior[CassandraHealthCheck.NotifyWhenHealthy]

}

trait DefaultModule extends CassandraHealthCheckModule {
  override def healthCheckBehavior(): Behavior[CassandraHealthCheck.NotifyWhenHealthy] =
    CassandraHealthCheck(
      unsafeConfigReader = UnsafeConfigReader,
      supportMessagesFactory = SimpleSupportMessagesFactory
    ).narrow

}

object CassandraHealthCheckExternal {

  val configValidator: ConfigValidator[Config] = akkaConfig => Config.Validator(akkaConfig)

}
