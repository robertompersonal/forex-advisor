package org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber

import akka.actor.typed.Behavior
import org.binqua.forex.advisor.model.CurrencyPair

import scala.concurrent.duration._

trait SubscriberModule {

  def subscriberBehavior(): Behavior[SubscriberProtocol.Command]

}

trait SubscriberModuleImpl extends SubscriberModule {

  val currencyPairsToSubscribeTo: Set[CurrencyPair] = Set(
    CurrencyPair.EurUsd,
    CurrencyPair.EurGbp,
    CurrencyPair.GbpJpy,
    CurrencyPair.Spx500,
    CurrencyPair.Us30,
    CurrencyPair.UsdJpy
  )

  override def subscriberBehavior(): Behavior[SubscriberProtocol.Command] = Subscriber(currencyPairs = currencyPairsToSubscribeTo, retryToSubscribeIntervalAfterPartialSubscription = 3.seconds).narrow

}
