package org.binqua.forex.feed.reader.parsers

import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.model.PosBigDecimal.{increasedByScaleUnit, unsafeFrom}
import org.binqua.forex.advisor.model.PosBigDecimalGen.{greaterThan, _}
import org.binqua.forex.advisor.model._
import org.binqua.forex.feed.reader.model.Rates
import org.binqua.forex.feed.reader.parsers.ScaleGen._
import org.scalacheck.Gen

object IncomingRatesGen {

  val withValidIncomingRates: Gen[(List[PosBigDecimal], CurrencyPair)] = for {
    (currencyPair, referenceData) <- TradingPairGen.currencyPairReferenceDataGen
    (minOfTheDaySellRate, _) <- PosBigDecimalGen.withScaleGen(referenceData.min, referenceData.max, currencyPair.quoteCurrency.scale)
    sellRate <- minOfTheDaySellRate or greaterThan(minOfTheDaySellRate, currencyPair)
    buyRate <- greaterThan(sellRate, currencyPair)
    maxOfTheDayBuyRate <- buyRate or greaterThan(buyRate, currencyPair)
  } yield (List(sellRate, buyRate, maxOfTheDayBuyRate, minOfTheDaySellRate), currencyPair)

  def withValidIncomingRatesCurrencyPairBased(wantedCurrencyPair: CurrencyPair => Boolean): Gen[(List[PosBigDecimal], CurrencyPair)] =
    for {
      currencyPair <- TradingPairGen.currencyPairBy(wantedCurrencyPair)
      scale <- Gen.choose[Scale](Scale(refineMV[NonNegative](1)), Scale(refineMV[NonNegative](10)))
      (minOfTheDaySellRate, _) <- PosBigDecimalGen.withScaleGen(
        TradingPairGen.tradingPairsReferenceData(currencyPair).min,
        TradingPairGen.tradingPairsReferenceData(currencyPair).max,
        scale
      )
      sellRate <- minOfTheDaySellRate or greaterThan(minOfTheDaySellRate, currencyPair)
      buyRate <- greaterThan(sellRate, currencyPair)
      maxOfTheDayBuyRate <- buyRate or greaterThan(buyRate, currencyPair)
    } yield (List(sellRate, buyRate, maxOfTheDayBuyRate, minOfTheDaySellRate), currencyPair)

  val withMinSellOfTheDayGreaterThanSell: Gen[List[PosBigDecimal]] = for {
    (List(sellRate: PosBigDecimal, buyRate: PosBigDecimal, maxOfTheDay: PosBigDecimal, _: PosBigDecimal), currencyPair) <- withValidIncomingRates
    newMinSellOfTheDay <- greaterThan(sellRate, currencyPair)
  } yield List(sellRate, buyRate, maxOfTheDay, newMinSellOfTheDay)

  val withSellGreaterOrEqualThanBuy: Gen[List[PosBigDecimal]] = for {
    (List(_: PosBigDecimal, buy: PosBigDecimal, max: PosBigDecimal, min: PosBigDecimal), currencyPair) <- withValidIncomingRates
    newSell <- buy or greaterThan(buy, currencyPair)
  } yield List(newSell, buy, max, min)

  val withMaxBuyOfTheDaySmallerThanBuy: Gen[List[PosBigDecimal]] = for {
    (List(sell: PosBigDecimal, buy: PosBigDecimal, _: PosBigDecimal, min: PosBigDecimal), currencyPair) <- withValidIncomingRates
    newMax <- PosBigDecimalGen.smallerThan(buy, currencyPair)
  } yield List(sell, buy, newMax, min)

  val withScaleEqualOrSmallerThanCurrencyPairScale: TradingPair => Gen[(List[PosBigDecimal], Rates)] = (tradingPair: TradingPair) =>
    for {
      rawRates <- withMinSellRateAt(tradingPair)
    } yield (
      rawRates,
      Rates.unsafe(rawRates, tradingPair.currencyPair)
    )

  def withMinSellRateAt(minSellRate: TradingPair): Gen[List[PosBigDecimal]] =
    for {
      sellRate <- greaterThan(minSellRate.price, minSellRate.currencyPair)
      buyRate <- greaterThan(sellRate, minSellRate.currencyPair)
      maxOfTheDayBuyRate <- greaterThan(buyRate, minSellRate.currencyPair)
    } yield List(sellRate, buyRate, maxOfTheDayBuyRate, minSellRate.price)

  def withBuyAt(buyPrice: TradingPair): Gen[List[PosBigDecimal]] =
    for {
      sellRate <- PosBigDecimalGen.smallerThan(buyPrice.price, buyPrice.currencyPair)
      minOfTheDaySellRate <- sellRate or PosBigDecimalGen.smallerThan(sellRate, buyPrice.currencyPair)
      maxOfTheDayBuyRate <- buyPrice.price or PosBigDecimalGen.greaterThan(buyPrice.price, buyPrice.currencyPair)
    } yield List(sellRate, buyPrice.price, maxOfTheDayBuyRate, minOfTheDaySellRate)

  def withDigitToTheRightOfTheScalePositionBetween(min: Int, max: Int): TradingPair => Gen[(List[PosBigDecimal], Rates)] =
    (tradingPair: TradingPair) => {
      require(min <= max && min >= 0 && min <= 9 && max >= 0 && max <= 9)
      for {
        (initialRates, _) <- withScaleEqualOrSmallerThanCurrencyPairScale(tradingPair)
        aTenthOfTheScaleDigit <- Gen.oneOf((min to max).toList)
        extraDigitsToTheRight <- Gen.chooseNum[Long](0, 100000)
      } yield {
        val expectedRates: Rates =
          if (aTenthOfTheScaleDigit >= 0 && aTenthOfTheScaleDigit <= 4)
            Rates.unsafe(initialRates, tradingPair.currencyPair)
          else
            Rates.unsafe(initialRates.map(increasedByScaleUnit(_, numberOfScaleUnit = 1)), tradingPair.currencyPair)
        (
          initialRates.map(rate => unsafeFrom(BigDecimal(s"${rate.value}$aTenthOfTheScaleDigit$extraDigitsToTheRight"))),
          expectedRates
        )
      }
    }
}
