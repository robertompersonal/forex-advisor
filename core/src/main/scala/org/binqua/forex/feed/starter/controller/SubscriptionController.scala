package org.binqua.forex.feed.starter.controller

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcherProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, UnhandledBehavior}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

import scala.concurrent.duration.FiniteDuration

private[controller] object SubscriptionController {

  val runningSubscriptionHeartBeatTimerKey = "runningSubscriptionHeartBeatTimerKey"

  def apply(
      heartBeatInterval: FiniteDuration,
      collaboratorsWatcherChildMaker: CHILD_MAKER[Message, CollaboratorsWatcherProtocol.Start],
      supportMessagesMaker: (SocketId, FiniteDuration) => SupportMessages
  ): Behavior[Message] =
    Behaviors.withTimers[Message](timers =>
      Behaviors.setup[Message] { parentContext =>
        def initialBehavior(unhandledBehavior: UnhandledBehavior[Message])(implicit handledMessages: TheOnlyHandledMessages): Behavior[Message] = {

          def afterReceivedStartBehavior(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId, supportMessages: SupportMessages)(implicit
              handledMessages: TheOnlyHandledMessages
          ): Behavior[Message] = {

            def runningSubscriptionBehavior(didItReceiveHeartBeat: Boolean, initialHearBeatChecksCounter: Int)(implicit
                handledMessages: TheOnlyHandledMessages
            ): Behavior[Message] =
              Behaviors.receiveMessage({

                case PrivateCheckSubscriptionIsStillRunning(`socketIdToBeSubscribedTo`) =>
                  if (didItReceiveHeartBeat) {
                    timers.startSingleTimer(
                      runningSubscriptionHeartBeatTimerKey,
                      PrivateCheckSubscriptionIsStillRunning(socketIdToBeSubscribedTo),
                      heartBeatInterval
                    )
                    runningSubscriptionBehavior(didItReceiveHeartBeat = false, initialHearBeatChecksCounter + 1)
                  } else {
                    parentContext.log.info(supportMessages.heartBeatMissing())
                    replyToWhenDone ! SubscriptionControllerProtocol.ExecutionFailed(socketIdToBeSubscribedTo)
                    Behaviors.receiveMessage(unhandledBehavior.withMsg[Message](_)(TheOnlyHandledMessages.NoMessagesBecauseIAmWaitingToBeStopped))
                  }

                case WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(`socketIdToBeSubscribedTo`)) =>
                  parentContext.log.info(supportMessages.running(initialHearBeatChecksCounter))
                  runningSubscriptionBehavior(didItReceiveHeartBeat = true, initialHearBeatChecksCounter)

                case WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(`socketIdToBeSubscribedTo`)) =>
                  parentContext.log.info(supportMessages.completed())
                  Behaviors.stopped[Message]

                case u: Start                                  => unhandledBehavior.withMsg[Message](u)
                case u: PrivateCheckSubscriptionIsStillRunning => unhandledBehavior.withMsg[Message](u)
                case u: WrappedCollaboratorsWatcherResponse    => unhandledBehavior.withMsg[Message](u)
              })

            runningSubscriptionBehavior(didItReceiveHeartBeat = false, initialHearBeatChecksCounter = 1)
          }

          Behaviors.receiveMessage {
            case Start(replyToOfProgress, socketIdToBeSubscribedTo) =>
              collaboratorsWatcherChildMaker(parentContext) ! CollaboratorsWatcherProtocol
                .Start(parentContext.messageAdapter(WrappedCollaboratorsWatcherResponse), socketIdToBeSubscribedTo)
              replyToOfProgress ! ExecutionStarted(socketIdToBeSubscribedTo)
              timers.startSingleTimer(runningSubscriptionHeartBeatTimerKey, PrivateCheckSubscriptionIsStillRunning(socketIdToBeSubscribedTo), heartBeatInterval)

              afterReceivedStartBehavior(replyToOfProgress, socketIdToBeSubscribedTo, supportMessagesMaker(socketIdToBeSubscribedTo, heartBeatInterval))(
                handledMessages = TheOnlyHandledMessages(
                  PrivateCheckSubscriptionIsStillRunning(socketIdToBeSubscribedTo),
                  WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketIdToBeSubscribedTo)),
                  WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(socketIdToBeSubscribedTo))
                )
              )

            case u: PrivateCheckSubscriptionIsStillRunning => unhandledBehavior.withMsg(u)
            case u: WrappedCollaboratorsWatcherResponse    => unhandledBehavior.withMsg(u)
          }
        }

        initialBehavior(unhandledBehavior = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext))(handledMessages =
          TheOnlyHandledMessages(Start)
        )

      }
    )

}

object SubscriptionControllerProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class Start(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  final case class PrivateCheckSubscriptionIsStillRunning(socketId: SocketId) extends PrivateCommand

  final case class WrappedCollaboratorsWatcherResponse(response: CollaboratorsWatcherProtocol.Response) extends PrivateCommand

  sealed trait Response

  final case class ExecutionStarted(socketId: SocketId) extends Response

  final case class ExecutionFailed(socketId: SocketId) extends Response

}
