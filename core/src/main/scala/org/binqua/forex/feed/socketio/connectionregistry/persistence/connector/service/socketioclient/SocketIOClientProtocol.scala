package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.typed.ActorRef
import org.binqua.forex.JsonSerializable
import org.binqua.forex.advisor.model.CurrencyPair

object SocketIOClientProtocol {

  sealed trait Command extends JsonSerializable

  final case class Connect(replyTo: ActorRef[ConnectionStatus]) extends Command

  final case class SubscribeTo(currencyPair: CurrencyPair, socketId: SocketId, replyTo: ActorRef[SubscriptionsResult], internalFeedActor: ActorRef[FeedContent])
      extends Command

  private[socketioclient] case class InternalConnected(actorWaitingForConnection: ActorRef[Connected]) extends Command

  private[socketioclient] case class InternalConnectionErrorMustToReconnect(attemptToConnect: Int) extends Command

  private[socketioclient] case class InternalRetryToConnectToSocketIOServer(attemptToConnect: Int) extends Command

  sealed trait Response extends JsonSerializable

  sealed trait SubscriptionsResult extends Response

  sealed trait ConnectionStatus extends Response

  final case class Connected(socketId: SocketId) extends ConnectionStatus

  final case object Connecting extends ConnectionStatus

  final case class FeedContent(content: String) extends Response

  final case class Subscribed(result: Boolean, currencyPair: CurrencyPair, socketId: SocketId) extends SubscriptionsResult

}
