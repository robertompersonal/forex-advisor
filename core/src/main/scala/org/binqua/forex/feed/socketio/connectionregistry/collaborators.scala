package org.binqua.forex.feed.socketio.connectionregistry

private[connectionregistry] trait SupportMessages {

  def connectionRegisterHealthy(): String

  def connectionRegisterUnderInitialization(): String

}

private[connectionregistry] object DefaultSupportMessages extends SupportMessages {

  override def connectionRegisterHealthy(): String = "connection register is healthy and should work properly"

  override def connectionRegisterUnderInitialization(): String = "connection register is initializing .... it will takes few seconds"
}


