package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import cats.Foldable
import cats.instances.list._
import cats.instances.option._
import cats.syntax.apply._
import cats.syntax.option._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModel.{State, StateUnderConstruction}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

import scala.concurrent.duration.FiniteDuration

private[currencypairs] object CurrencyPairsSubscriberActor {

  def apply(timeoutBeforeReportingSubscriptionResults: FiniteDuration,
            servicesFinderChildMaker: CHILD_MAKER[Message, ServicesFinderProtocol.Command],
            childMaker: CurrencyPair => CHILD_MAKER[Message, CurrencyPairSubscriberProtocol.Command],
            supportMessageUtil: SupportMessageUtil.NewSubscriptionSummaryMessages,
            supportMessage: SupportMessage): Behavior[Message] = Behaviors.withTimers(timers => Behaviors.setup(parentContext => {

    def setUpBehavior(thisAsServiceFinderResponseReceiver: ActorRef[ServicesFinderProtocol.Response],
                      thisAsSocketIOClientResponseReceiver: ActorRef[SocketIOClientProtocol.SubscriptionsResult],
                      unhandledBehavior: AkkaUtil.UnhandledBehavior[Message])(implicit handled: TheOnlyHandledMessages): Behavior[Message] = {

      def subscribeReceivedBehavior(state: State): Behavior[Message] = Behaviors.receiveMessage({
        case WrappedSocketIOClientResponse(subscribed@SocketIOClientProtocol.Subscribed(subscriptionResult, currencyPair, _)) =>
          state.subscriberOf(currencyPair) match {
            case Some(childRef) =>
              childRef ! CurrencyPairSubscriberProtocol.Stop
              state.currencyPairSubscribed(subscriptionResult, currencyPair).withTheNewState(newState => {

                parentContext.log.info(supportMessageUtil(subscribed.result, subscribed.currencyPair, newState.summary))

                if (newState.someSubscriptionsAreStillMissing)
                  subscribeReceivedBehavior(newState)
                else {
                  newState.subscribe.replyTo ! newState.asSubscription
                  Behaviors.stopped(() => {
                    parentContext.log.info(supportMessage.jobDone())
                  })
                }
              })
            case None => Behaviors.same
          }
        case PrivateReportSubscriptionResult(_, replyTo) =>
          state.childrenWaitingForResult.foreach(childRef => childRef ! CurrencyPairSubscriberProtocol.Stop)
          replyTo ! state.asSubscription

          parentContext.log.info(supportMessage.timeAvailablePassed(timeoutBeforeReportingSubscriptionResults, state.waitingForResult))
          servicesFinderChildMaker(parentContext) ! ServicesFinderProtocol.GetServiceReferences(thisAsServiceFinderResponseReceiver)
          waitToBeFullyInitialised(None, None)

        case u: Subscribe => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(PrivateReportSubscriptionResult, WrappedSocketIOClientResponse))
        case u: WrappedServicesFinderResponse => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(PrivateReportSubscriptionResult, WrappedSocketIOClientResponse))
      })

      def subscribeHandler(subscriptionParameters: SubscriptionParameters): Behavior[Message] = {

        parentContext.log.info(supportMessage.startSubscription(timeoutBeforeReportingSubscriptionResults, subscriptionParameters.subscribeMessage.currencyPairs))

        val socketId = subscriptionParameters.subscribeMessage.socketId

        val stateUnderConstruction = Foldable[List].foldLeft(subscriptionParameters.subscribeMessage.currencyPairs.toList, StateUnderConstruction.emptyState)(
          (acc, currencyPair) => {

            val childForCurrencyPair: ActorRef[CurrencyPairSubscriberProtocol.Command] = childMaker(currencyPair)(parentContext)

            childForCurrencyPair ! CurrencyPairSubscriberProtocol.StartSubscription(
              whoWantsToKnoAboutSubscriptionResult = thisAsSocketIOClientResponseReceiver,
              currencyPair,
              socketId = socketId,
              socketIOClientRef = subscriptionParameters.allServicesNeeded.socketIOClientRef,
              httpSubscribeRef = subscriptionParameters.allServicesNeeded.httpClientRef,
              internalFeedRef = subscriptionParameters.allServicesNeeded.feedContentRef
            )
            acc.record(currencyPair, childForCurrencyPair)
          }
        ).done()

        timers.startSingleTimer(socketId, PrivateReportSubscriptionResult(socketId, subscriptionParameters.subscribeMessage.replyTo), timeoutBeforeReportingSubscriptionResults)

        State.createAState(stateUnderConstruction, subscriptionParameters.subscribeMessage).withTheNewState(subscribeReceivedBehavior)
      }

      def waitToBeFullyInitialised(maybeASubscribeMessage: Option[CurrencyPairsSubscriberProtocol.Subscribe], maybeAllServicesNeeded: Option[ServicesFinderProtocol.ServicesReferences]): Behaviors.Receive[Message] = {
        Behaviors.receiveMessage({
          case subscribe: Subscribe =>
            (subscribe.some, maybeAllServicesNeeded).mapN(SubscriptionParameters.apply) match {
              case Some(subscriptionParameters) => subscribeHandler(subscriptionParameters)
              case None => waitToBeFullyInitialised(subscribe.some, maybeAllServicesNeeded)
            }
          case WrappedServicesFinderResponse(allServicesNeeded: ServicesFinderProtocol.ServicesReferences) =>
            (maybeASubscribeMessage, allServicesNeeded.some).mapN(SubscriptionParameters.apply) match {
              case Some(subscriptionParameters) => subscribeHandler(subscriptionParameters)
              case None => waitToBeFullyInitialised(maybeASubscribeMessage, allServicesNeeded.some)
            }
          case u: PrivateReportSubscriptionResult => unhandledBehavior.withMsg(u)
          case u: WrappedSocketIOClientResponse => unhandledBehavior.withMsg(u)
        })
      }

      servicesFinderChildMaker(parentContext) ! ServicesFinderProtocol.GetServiceReferences(thisAsServiceFinderResponseReceiver)

      waitToBeFullyInitialised(maybeASubscribeMessage = None, maybeAllServicesNeeded = None)
    }

    setUpBehavior(
      parentContext.messageAdapter(WrappedServicesFinderResponse),
      parentContext.messageAdapter(WrappedSocketIOClientResponse),
      AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext))(handled = TheOnlyHandledMessages(Subscribe)
    )

  }))
}

case class SubscriptionParameters(subscribeMessage: CurrencyPairsSubscriberProtocol.Subscribe, allServicesNeeded: ServicesFinderProtocol.ServicesReferences)
