package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder

import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey

private[servicesfinder] trait SupportMessages {

  def tooManyServices[T](InternalFeedServiceKey: ServiceKey[T], value: List[ActorRef[T]]): String

  def allServicesNotAvailableYet(): String

  def socketIOClientServiceDiscoverySubscribed(): String

  def httpClientSubscriberServiceDiscoverySubscribed(): String

  def internalFeedServiceDiscoverySubscribed(): String

  def serviceDiscovered[T](head: ActorRef[T]): String

}

private[servicesfinder] object SupportMessagesImpl extends SupportMessages {

  val subscribeToMessage: String => String = details => s"Subscribed to $details service discovery"

  override def allServicesNotAvailableYet(): String = "All required services are not available yet. Give me some time to find them"

  override def httpClientSubscriberServiceDiscoverySubscribed(): String = subscribeToMessage("httpClientSubscriber")

  override def internalFeedServiceDiscoverySubscribed(): String = subscribeToMessage("internalFeedService")

  override def serviceDiscovered[T](ref: ActorRef[T]): String = s"Service ${ref.path.name} ($ref) has been discovered"

  override def socketIOClientServiceDiscoverySubscribed(): String = subscribeToMessage("socketIOClientService")

  override def tooManyServices[T](serviceKey: ServiceKey[T], refs: List[ActorRef[T]]): String = s"Too many actors services $refs with same key ${serviceKey.id}. Maybe some service has not been deregister yet. I will wait for other receptionist events until there is only one service x key"
}
