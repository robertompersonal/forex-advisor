package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.FailureReason
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure
import org.binqua.forex.util.{ErrorSituation, ErrorSituationHandler}

import scala.concurrent.duration.FiniteDuration

package object currencypair {

  private object ErrorSituationHandlerImpl extends ErrorSituationHandler {
    override def show(theErrorSituation: ErrorSituation): String =
      theErrorSituation match {
        case subscriptionFailed: RetryingDueToHttpServerFailure =>
          s"Http subscription failed ${suffix(subscriptionFailed)}"
      }

    private def suffix(
        currencyPair: CurrencyPair,
        socketId: SocketId,
        retryInterval: FiniteDuration,
        retryAttemptCounter: Int,
        failureReason: FailureReason
    ): String =
      s"due to ${toString(failureReason)} while attempting to subscribe $currencyPair with $socketId. Going to retry at $retryInterval interval. This is attempt number $retryAttemptCounter"

    private def suffix(sf: RetryingDueToHttpServerFailure): String =
      suffix(sf.hse.currencyPair, sf.hse.socketId, sf.retryInterval, sf.retryAttemptCounter, sf.hse.failureReason)

    private def toString(failureReason: FailureReason) =
      failureReason match {
        case HttpClientSubscriberProtocol.ServerError => "a server error"
        case HttpClientSubscriberProtocol.ParserError => "a parser error"
      }

  }

  private[currencypairs] val defaultSupportMessages: SupportMessages = new SupportMessages {

    override def waitingToBeStoppedBehavior(currencyPair: CurrencyPair): String =
      s"I contacted SocketIOServer. I did everything I was suppose to do for currency pair $currencyPair. I am waiting to be terminated"

    override def message(failure: CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure): String = ErrorSituationHandlerImpl.show(failure)

  }

}
