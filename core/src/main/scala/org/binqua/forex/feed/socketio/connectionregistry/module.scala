package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef, EntityTypeKey}
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.util.NewChildMaker
import org.binqua.forex.util.NewChildMaker.ChildNamePrefix

trait Module {

  def connectionRegistryBehavior(): Behavior[Manager.Command]

}

trait DefaultModule extends Module {

  this: persistence.Module with healthcheck.CassandraHealthCheckModule =>

  override def connectionRegistryBehavior(): Behavior[Manager.Command] =
    Manager(
      NewChildMaker.fromContext[Manager.Message](ChildNamePrefix(refineMV[NonEmpty]("persistence"))).withBehavior(persistenceBehavior()),
      NewChildMaker.fromContext[Manager.Message](ChildNamePrefix(refineMV[NonEmpty]("healthCheck"))).withBehavior(healthCheckBehavior()),
      supportMessages = DefaultSupportMessages
    ).narrow

}

trait InitialisedModule {

  def ref: EntityRef[Manager.Command]

}

trait ShardedModule {

  def initialiseConnectionRegistry(clusterSharding: ClusterSharding): InitialisedModule

}

trait ShardedModuleImpl extends ShardedModule {

  this: Module =>

  final override def initialiseConnectionRegistry(clusterSharding: ClusterSharding): InitialisedModule = {

    val connectionRegistryTypeKey = EntityTypeKey[Manager.Command](name = "connectionRegistry")

    clusterSharding.init(Entity(connectionRegistryTypeKey)(createBehavior = _ => connectionRegistryBehavior()).withRole(connectionRegistryTypeKey.name))

    new InitialisedModule {
      def ref: EntityRef[Manager.Command] = clusterSharding.entityRefFor(connectionRegistryTypeKey, connectionRegistryTypeKey.name)
    }
  }
}

trait ProductionShardedModule extends ShardedModuleImpl with DefaultModule with persistence.ProductionModule with healthcheck.DefaultModule {}
