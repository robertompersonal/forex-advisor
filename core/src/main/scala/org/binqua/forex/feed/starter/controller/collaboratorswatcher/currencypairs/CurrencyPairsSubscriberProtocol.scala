package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.ActorRef
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol

object CurrencyPairsSubscriberProtocol {

  sealed trait Message

  sealed trait Command extends Message

  sealed trait PrivateMessage extends Message

  final case class Subscribe(currencyPairs: Set[CurrencyPair], socketId: SocketId, replyTo: ActorRef[Subscriptions]) extends Command

  private[currencypairs] final case class PrivateReportSubscriptionResult(socketId: SocketId, replyTo: ActorRef[Subscriptions]) extends PrivateMessage

  private[currencypairs] final case class WrappedSocketIOClientResponse(response: SocketIOClientProtocol.SubscriptionsResult) extends PrivateMessage

  private[currencypairs] final case class WrappedServicesFinderResponse(response: ServicesFinderProtocol.Response) extends PrivateMessage

  sealed trait Response

  final case class Subscriptions(socketId: SocketId, subscribe: Set[CurrencyPair], unsubscribe: Set[CurrencyPair]) extends Response

}
