package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol

import scala.concurrent.duration.FiniteDuration

trait SupportMessages {

  def actorRequestedNewSocketIdNotification(actualSocketId: SocketId, newActorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId]): String

  def startSubscriptionAttemptWithOldSocketIdAborted(newSocketId: SocketId, oldSocketId: SocketId): String

  def startSubscriptionAttemptFailedRetry(
      socketId: SocketId,
      actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId],
      attempts: Int,
      timeout: FiniteDuration
  ): String

  def newSocketIdAvailable(socketId: SocketId): String

  def newSocketIdNotified(socketId: SocketId): String

}
