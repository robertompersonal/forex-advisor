package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.typed.Behavior
import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{ProductionSocketIOClientModule, SocketIOClientModule, SocketIOClientProtocol}
import org.binqua.forex.util.ChildMaker

trait SocketIOClientServiceModule {

  def socketIOClientService(): Behavior[Command]

}

trait CustomizableSocketIOClientServiceModule extends SocketIOClientServiceModule {

  this: SocketIOClientModule =>

  override def socketIOClientService(): Behavior[Command] = SocketIOClientService(
    ChildMaker.fromContext[Message](childNamePrefix = "socketIOClient").withBehavior(socketIOClient())(),
    DefaultSupportMessages
  ).narrow

}

object SocketIOClientServiceModule {
  val SocketIOClientKey: ServiceKey[SocketIOClientProtocol.Command] = ServiceKey[SocketIOClientProtocol.Command]("SocketIOClientService")
}

trait ProductionSocketIOClientServiceModule extends CustomizableSocketIOClientServiceModule with ProductionSocketIOClientModule

