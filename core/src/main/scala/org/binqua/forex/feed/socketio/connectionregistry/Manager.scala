package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.JsonSerializable
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, UnhandledBehavior}
import org.binqua.forex.util.NewChildMaker.ChildrenFactory

object Manager {

  sealed trait Message

  sealed trait Command extends Message with JsonSerializable

  final case class Register(
      registrationRequester: ActorRef[Persistence.Response],
      subscriptionStarter: ActorRef[SubscriptionStarterProtocol.NewSocketId],
      requesterAlias: String
  ) extends Command

  sealed trait PrivateMessage extends Message

  case class WrappedHealthy(healthy: CassandraHealthCheck.Response) extends PrivateMessage

  private[connectionregistry] def apply(
      persistenceChildMaker: ChildrenFactory[Message, Persistence.Command],
      healthCheckChildMaker: ChildrenFactory[Message, CassandraHealthCheck.NotifyWhenHealthy],
      supportMessages: SupportMessages
  ): Behavior[Manager.Message] =
    Behaviors.setup(implicit context => {

      def initialBehavior(unhandledBehavior: UnhandledBehavior[Message], adapters: Adapters, childrenMaker: ChildrenMaker): Behavior[Message] = {

        childrenMaker.newCassandraHealthCheck() ! CassandraHealthCheck.NotifyWhenHealthy(adapters.cassandraHealthCheck)

        def becomeHealthy(ref: ActorRef[Persistence.Register]): Behavior[Manager.Message] =
          Behaviors.receiveMessage({
            case r: Register =>
              ref ! Persistence.Register(r.registrationRequester, r.subscriptionStarter, r.requesterAlias)
              Behaviors.same
            case m: WrappedHealthy => unhandledBehavior.withMsg[Message](m)(TheOnlyHandledMessages(Register))
          })

        Behaviors.receiveMessage {
          case _: Register =>
            context.log.info(supportMessages.connectionRegisterUnderInitialization())
            Behaviors.same
          case WrappedHealthy(_) =>
            context.log.info(supportMessages.connectionRegisterHealthy())
            becomeHealthy(childrenMaker.newConnectionRegistryPersistence())
        }
      }

      initialBehavior(
        AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](context),
        Adapters(context),
        ChildrenMaker(context, persistenceChildMaker, healthCheckChildMaker)
      )

    })

  case class Adapters(private val context: ActorContext[Message]) {
    val cassandraHealthCheck = context.messageAdapter[CassandraHealthCheck.Response](WrappedHealthy)
  }

  case class ChildrenMaker(
      private val context: ActorContext[Message],
      private val connectionRegistryPersistence: ChildrenFactory[Message, Persistence.Command],
      private val cassandraHealthCheck: ChildrenFactory[Message, CassandraHealthCheck.NotifyWhenHealthy]
  ) {

    def newConnectionRegistryPersistence(): ActorRef[Persistence.Register] = connectionRegistryPersistence.newChild(context)

    def newCassandraHealthCheck(): ActorRef[CassandraHealthCheck.NotifyWhenHealthy] = cassandraHealthCheck.newChild(context)
  }

}
