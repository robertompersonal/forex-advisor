package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import org.binqua.forex.advisor.model.CurrencyPair

import scala.concurrent.duration.FiniteDuration

trait SupportMessage {

  def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, waitingResult: Set[CurrencyPair]): String

  def startSubscription(timeout: FiniteDuration, unsubscribed: Set[CurrencyPair]): String

  def jobDone(): String

}
