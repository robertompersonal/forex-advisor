package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.Behavior
import org.binqua.forex.feed.starter.controller.collaboratorswatcher
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModule
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberModule
import org.binqua.forex.util.ChildMaker

import scala.concurrent.duration._

trait CollaboratorsWatcherModule {

  def collaboratorsWatcherBehavior(): Behavior[CollaboratorsWatcherProtocol.Start]

}

trait CollaboratorsWatcherImpl extends CollaboratorsWatcherModule {

  this: CurrencyPairsSubscriberModule with SubscriberModule =>

  override def collaboratorsWatcherBehavior(): Behavior[CollaboratorsWatcherProtocol.Start] = {

    val timeoutBeforeCheckingCollaboratorsTermination = 2.seconds

    CollaboratorsWatcher(
      ChildMaker.fromContext[CollaboratorsWatcherProtocol.Message](childNamePrefix = "currencyPairsSubscriber").withBehavior(currencyPairsSubscriberBehavior())(),
      ChildMaker.fromContext[CollaboratorsWatcherProtocol.Message](childNamePrefix = "subscriber").withBehavior(subscriberBehavior())(),
      timeoutBeforeCheckingCollaboratorsTermination,
      supportMaker = collaboratorswatcher.logAsInfoSupportMaker(timeoutBeforeCheckingCollaboratorsTermination)
    ).narrow
  }


}
