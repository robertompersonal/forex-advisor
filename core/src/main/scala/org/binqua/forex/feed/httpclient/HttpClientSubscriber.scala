package org.binqua.forex.feed.httpclient

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, PostStop, Signal}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Accept, Authorization, OAuth2BearerToken, RawHeader}
import akka.http.scaladsl.{Http, HttpExt}
import akka.util.ByteString
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.{PrivateMessage, _}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.core.UnsafeConfigReader
import org.binqua.forex.util.{AkkaUtil, ErrorSituation, ErrorSituationHandler}
import play.api.libs.json.{JsError, JsSuccess}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

private[httpclient] object HttpClientSubscriber {

  type ON_SIGNAL = PartialFunction[(ActorContext[Message], Signal), Behavior[Message]]

  def apply(unsafeConfigReader: UnsafeConfigReader[Config], bodyReader: BodyReader, errorSituationHandler: ErrorSituationHandler): Behavior[Message] =
    Behaviors.setup { implicit context: ActorContext[Message] =>
      val unhandledBehavior: AkkaUtil.UnhandledBehavior[Message] = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](context)

      implicit val untypedActorSystem = context.system.toClassic
      implicit val ec: ExecutionContextExecutor = context.system.executionContext

      val http: HttpExt = Http(untypedActorSystem)

      val config = unsafeConfigReader(context.system.settings.config)

      def runningBehavior(onSignal: ON_SIGNAL, http: HttpExt): Behavior[Message] =
        Behaviors
          .receiveMessage[Message] {
            case subscribeTo: SubscribeTo =>
              postRequestToHttpServer(subscribeTo, http)
              Behaviors.same
            case InternalSubscriptionResponse(httpResponse, currencyPair, socketId, replyTo) =>
              handleHttpResponse(currencyPair, socketId, replyTo)(httpResponse)
              Behaviors.same
            case InternalHttpBodyReceivedSuccessfully(body, currencyPair, socketId, replyTo) =>
              bodyReader.parse(body) match {
                case JsSuccess(_, _) =>
                  replyTo ! SubscriptionSuccess(currencyPair, socketId)
                case jsr: JsError =>
                  context.log.error(errorSituationHandler.show(HttpSubscriptionFailedErrorSituation(body, currencyPair, jsr, socketId)))
                  replyTo ! SubscriptionFailure(currencyPair, socketId, HttpClientSubscriberProtocol.ParserError)
              }
              Behaviors.same
            case InternalHttpBodyReceivedFailed(ex, currencyPair, socketId, replyTo) =>
              context.log.error(ex.getMessage, ex.getCause)
              replyTo ! SubscriptionFailure(currencyPair, socketId, HttpClientSubscriberProtocol.ParserError)
              Behaviors.same
            case InternalSubscriptionException(ex, currencyPair, socketId, replyTo) =>
              context.log.error(ex.getMessage, ex.getCause)
              replyTo ! SubscriptionFailure(currencyPair, socketId, HttpClientSubscriberProtocol.ServerError)
              Behaviors.same
          }
          .receiveSignal(onSignal)

      def handleHttpResponse(currencyPair: CurrencyPair, socketId: SocketId, replyTo: ActorRef[Response]): PartialFunction[HttpResponse, Unit] = {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          context.pipeToSelf(aFutureEntityBody(entity)) {
            case Success(body) => InternalHttpBodyReceivedSuccessfully(body, currencyPair, socketId, replyTo)
            case Failure(ex)   => InternalHttpBodyReceivedFailed(ex, currencyPair, socketId, replyTo)
          }
        case resp @ HttpResponse(code, _, _, _) =>
          resp.discardEntityBytes()
          context.log.error(errorSituationHandler.show(HttpSubscriptionWrongHttpCodeErrorSituation(code, currencyPair, socketId)))
          replyTo ! SubscriptionFailure(currencyPair, socketId, HttpClientSubscriberProtocol.ServerError)
      }

      def postRequestToHttpServer(subscribeToIncomingCommand: SubscribeTo, http: HttpExt): Unit = {
        val SubscribeTo(currencyPair, socketId, replyTo) = subscribeToIncomingCommand
        val aFutureHttpResponse = http.singleRequest(buildSubscriptionRequest(extractSubscriptionParameter(config, socketId), currencyPair))
        context.pipeToSelf(aFutureHttpResponse) {
          case Success(httpResponse) => InternalSubscriptionResponse(httpResponse, currencyPair, socketId, replyTo)
          case Failure(ex)           => InternalSubscriptionException(ex, currencyPair, socketId, replyTo)
        }
      }

      Behaviors
        .receiveMessage[Message] {
          case subscribeTo: SubscribeTo =>
            postRequestToHttpServer(subscribeTo, http)
            runningBehavior(onSignal(http, context), http)

          case u: PrivateMessage => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(SubscribeTo))
        }
        .receiveSignal(onSignal(http, context))
    }

  def onSignal(http: HttpExt, context: ActorContext[Message]): ON_SIGNAL = {
    case (_, PostStop) =>
      context.log.info("HttpClientSubscriber terminated and connection released")
      http.shutdownAllConnectionPools()
      Behaviors.same
  }

  private def aFutureEntityBody(entity: ResponseEntity)(implicit actorSystem: ActorSystem, executionContext: ExecutionContext): Future[String] =
    entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)

  private[httpclient] final case class SocketSubscriptionParameters(
      tradingApiHost: String,
      tradingApiPort: String,
      connectionUrl: String,
      loginToken: String,
      bearerToken: String
  )

  def extractSubscriptionParameter(config: Config, socketId: SocketId): SocketSubscriptionParameters = {
    val loginToken: String = config.loginToken
    SocketSubscriptionParameters(config.apiHost, config.apiPort, config.connectionUrl, loginToken, socketId.id + loginToken)
  }

  def buildSubscriptionRequest(subscriptionParameters: SocketSubscriptionParameters, currencyPair: CurrencyPair): HttpRequest = {
    val body = s"pairs=${CurrencyPair.toExternalIdentifier(currencyPair)}"
    HttpRequest(
      method = HttpMethods.POST,
      uri = s"${subscriptionParameters.connectionUrl}/subscribe",
      entity = HttpEntity(ContentTypes.`application/x-www-form-urlencoded`, body),
      headers = List(
        RawHeader("Content-Language", "en-US"),
        Authorization(OAuth2BearerToken(subscriptionParameters.bearerToken)),
        Accept(MediaRange(MediaTypes.`application/json`)),
        RawHeader("host", subscriptionParameters.tradingApiHost),
        RawHeader("port", subscriptionParameters.tradingApiPort),
        RawHeader("path", "/subscribe")
      )
    )
  }

}

final case class HttpSubscriptionFailedErrorSituation(body: String, currencyPair: CurrencyPair, jsError: JsError, socketId: SocketId) extends ErrorSituation

final case class HttpSubscriptionWrongHttpCodeErrorSituation(statusCode: StatusCode, currencyPair: CurrencyPair, socketId: SocketId) extends ErrorSituation
