package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcherProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

import scala.concurrent.duration.FiniteDuration

object CollaboratorsWatcher {

  val checkThatBothCollaboratorsAreTerminatedTimerKey: String = "checkThatBothCollaboratorsAreTerminatedKey"

  def apply(
      currencyPairsSubscriberChildMaker: CHILD_MAKER[Message, CurrencyPairsSubscriberProtocol.Command],
      subscriberChildMaker: CHILD_MAKER[Message, SubscriberProtocol.Command],
      timeoutBeforeCheckingCollaboratorsTermination: FiniteDuration,
      supportMaker: SUPPORT_MAKER
  ): Behavior[Message] =
    Behaviors.withTimers[Message](timers =>
      Behaviors.setup[Message](parentContext => {

        def firstBehavior(
            support: Support[Unit],
            thisActorAsSubscriberResponseReceiver: ActorRef[SubscriberProtocol.Response],
            unhandledBehavior: AkkaUtil.UnhandledBehavior[Message]
        ): Behavior[Message] = {

          def startHandler(replyToWhenDone: ActorRef[Response], socketIdToBeSubscribedTo: SocketId): Behavior[Message] = {

            def stopTheOtherChildHandler[T](childToBeStopped: ActorRef[T]): Behavior[Message] = {
              parentContext.unwatch(childToBeStopped)
              parentContext.stop(childToBeStopped)

              timers.startSingleTimer(
                checkThatBothCollaboratorsAreTerminatedTimerKey,
                InternalCheckThatBothCollaboratorsHaveBeenTerminated,
                timeoutBeforeCheckingCollaboratorsTermination
              )

              implicit val handled: TheOnlyHandledMessages =
                TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse)
              Behaviors.receiveMessage[Message]({
                case InternalCheckThatBothCollaboratorsHaveBeenTerminated => checkThatBothCollaboratorsHaveBeenTerminatedHandler(attempt = 1)
                case WrappedSubscriberResponse(subscriberResponse) =>
                  support.collaboratorsTerminationInProgressMessageIgnored(subscriberResponse); Behaviors.same

                case u: Start => unhandledBehavior.withMsg(u)
              })
            }

            def checkThatBothCollaboratorsHaveBeenTerminatedHandler(attempt: Int): Behavior[Message] = {
              if (parentContext.children.isEmpty) {
                support.bothCollaboratorsTerminated(attempt)
                startHandler(replyToWhenDone, socketIdToBeSubscribedTo)
              } else {
                support.oneCollaboratorStillNotTerminated(parentContext.children.map(_.path.name), attempt)

                timers.startSingleTimer(
                  checkThatBothCollaboratorsAreTerminatedTimerKey,
                  InternalCheckThatBothCollaboratorsHaveBeenTerminated,
                  timeoutBeforeCheckingCollaboratorsTermination
                )

                implicit val handled: TheOnlyHandledMessages =
                  TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse)

                Behaviors.receiveMessage[Message]({
                  case InternalCheckThatBothCollaboratorsHaveBeenTerminated => checkThatBothCollaboratorsHaveBeenTerminatedHandler(attempt + 1)
                  case WrappedSubscriberResponse(subscriberResponse) =>
                    support.collaboratorsTerminationInProgressMessageIgnored(subscriberResponse); Behaviors.same

                  case u: Start => unhandledBehavior.withMsg(u)
                })
              }
            }

            val currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberChildMaker(parentContext)
            parentContext.watch(currencyPairsSubscriberRef)

            val subscriberRef: ActorRef[SubscriberProtocol.Command] = subscriberChildMaker(parentContext)
            parentContext.watch(subscriberRef)

            subscriberRef ! SubscriberProtocol.Start(socketIdToBeSubscribedTo, currencyPairsSubscriberRef, thisActorAsSubscriberResponseReceiver)

            implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(WrappedSubscriberResponse)

            Behaviors
              .receiveMessage[Message]({
                case WrappedSubscriberResponse(subscriberResponse) =>
                  support.newSubscriptionResponse(subscriberResponse)
                  subscriberResponse match {
                    case fs: SubscriberProtocol.FullySubscribed         => replyToWhenDone ! SubscriptionDone(fs.socketId)
                    case rsn: SubscriberProtocol.RunningSubscriptionNow => replyToWhenDone ! SubscriptionRunning(rsn.socketId)
                  }
                  Behaviors.same
                case u: Start                                             => unhandledBehavior.withMsg(u)
                case InternalCheckThatBothCollaboratorsHaveBeenTerminated => unhandledBehavior.withMsg(InternalCheckThatBothCollaboratorsHaveBeenTerminated)

              })
              .receiveSignal({
                case (_, ChildFailed(`currencyPairsSubscriberRef`, _)) =>
                  support.currencyPairsSubscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
                  stopTheOtherChildHandler(childToBeStopped = subscriberRef)
                case (_, ChildFailed(`subscriberRef`, _)) =>
                  support.subscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
                  stopTheOtherChildHandler(childToBeStopped = currencyPairsSubscriberRef)
                case _ => Behaviors.same
              })
          }

          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Start)
          Behaviors.receiveMessage({

            case Start(replyToWhenDone, socketIdToBeSubscribedTo) => startHandler(replyToWhenDone, socketIdToBeSubscribedTo)

            case InternalCheckThatBothCollaboratorsHaveBeenTerminated => unhandledBehavior.withMsg(InternalCheckThatBothCollaboratorsHaveBeenTerminated)
            case u: WrappedSubscriberResponse                         => unhandledBehavior.withMsg(u)
          })

        }

        firstBehavior(
          support = supportMaker(parentContext),
          thisActorAsSubscriberResponseReceiver = parentContext.messageAdapter(WrappedSubscriberResponse),
          unhandledBehavior = AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext)
        )

      })
    )
}
