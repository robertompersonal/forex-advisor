package org.binqua.forex.feed.starter.controller

import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId

import scala.concurrent.duration.FiniteDuration

trait SupportMessages {

  def heartBeatMissing(): String

  def running(heartBeatChecksCounter: Int): String

  def completed(): String

}

object SupportMessages {

  val supportMessagesMaker: (SocketId, FiniteDuration) => SupportMessages = (socketId, heartBeatInterval) =>
    new SupportMessages {

      override def heartBeatMissing(): String =
        s"$heartBeatInterval passed without receiving info about subscription. For me execution failed! Waiting to be stopped"

      override def running(heartBeatChecksCounter: Int): String =
        s"Subscription is running okay. HeartBeatChecksCounter number $heartBeatChecksCounter received. Going to check in $heartBeatInterval"

      override def completed(): String = s"Subscription execution for socket id ${socketId.id} has been completed successfully"
    }

}
