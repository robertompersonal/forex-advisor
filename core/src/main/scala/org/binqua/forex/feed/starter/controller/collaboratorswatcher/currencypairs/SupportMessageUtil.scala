package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import cats.syntax.show._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.implicits.instances.socketId._

import scala.concurrent.duration.FiniteDuration

object SupportMessageUtil {

  type FirstPart = (Boolean, CurrencyPair, CurrencyPairsSubscriberModel.State.Summary) => String

  type SecondPart = CurrencyPairsSubscriberModel.State.Summary => String

  type NewSubscriptionSummaryMessages = (Boolean, CurrencyPair, CurrencyPairsSubscriberModel.State.Summary) => String

  type SupportMessagesFactory = FirstPart => SecondPart => NewSubscriptionSummaryMessages

  val wholeSubscriptionMessage: SupportMessagesFactory = firstPart => secondPart => (subscribeResult, currencyPair, summary) =>
    s"${firstPart(subscribeResult, currencyPair, summary)}\n${secondPart(summary)}"

  val newSubscriptionMessage: FirstPart = (subscribeResult, currencyPair, summary) =>
    s"Subscription to ${CurrencyPair.toExternalIdentifier(currencyPair)} feed via ${summary.socketId.show} ${if (subscribeResult) "successful" else "unsuccessful"}"

  val summaryMessage: SecondPart = summary => {
    def toString(currencyPairs: Set[CurrencyPair]) = currencyPairs.map(CurrencyPair.toExternalIdentifier).mkString("[", ",", "]")

    def summaryReport(summary: CurrencyPairsSubscriberModel.State.Summary): String =
      s"${toString(summary.subscribed)} subscribed\n${toString(summary.unsubscribed)} not subscribed\n${toString(summary.waitingResult)} waiting to be subscribed"

    val socketId = summary.socketId.show

    if (summary.waitingResult.isEmpty) {
      if (summary.unsubscribed.isEmpty)
        s"Subscription attempt complete: subscribed to all feeds ${toString(summary.currencyPairs)} via $socketId"
      else
        s"Subscription attempt complete:\n${summaryReport(summary)}"
    }
    else {
      s"Subscription attempt underway:\n${summaryReport(summary)}"
    }

  }

  val defaultSupportMessage = new SupportMessage {

    override def jobDone(): String = "Subscription completed"

    override def startSubscription(timeout: FiniteDuration, currencyPairs: Set[CurrencyPair]): String = s"I am going to start subscription now for currency pairs ${currencyPairs.mkString("-")}. I have $timeout before report results"

    override def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, currencyPairs: Set[CurrencyPair]): String = s"After $timeoutBeforeReportingSubscriptionResults I did not complete all subscriptions. Waiting to start again with the missing one: ${currencyPairs.mkString("-")}"
  }

}
