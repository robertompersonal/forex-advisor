package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.typed.Behavior
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.{ConnectionNotifierModule, ProductionConnectionNotifierModule}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.{ProductionSocketIOClientServiceModule, SocketIOClientServiceModule}
import org.binqua.forex.util.ChildMaker

trait SocketIOManagerModule {

  def socketIOManager(): Behavior[Connector.Command]

}

trait CustomizableSocketIOManagerModule extends SocketIOManagerModule {

  this: ConnectionNotifierModule with SocketIOClientServiceModule =>

  override def socketIOManager(): Behavior[Connector.Command] = Connector(
    socketIOClientServiceMaker = ChildMaker.fromContext[Connector.Message](childNamePrefix = "socketIOClientService").withBehavior(socketIOClientService())(),
    connectionNotifierMaker = ChildMaker.fromContext[Connector.Message](childNamePrefix = "connectionNotifier").withBehavior(connectionNotifier())(),
  ).narrow

}

trait ProductionSocketIOManagerModule extends CustomizableSocketIOManagerModule
  with ProductionConnectionNotifierModule
  with ProductionSocketIOClientServiceModule

