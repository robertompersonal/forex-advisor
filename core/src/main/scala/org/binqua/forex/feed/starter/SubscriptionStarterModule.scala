package org.binqua.forex.feed.starter

import akka.actor.typed.{Behavior, RecipientRef}
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcherImpl
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModuleImpl
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModuleImpl
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServiceFinderModuleImpl
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberModuleImpl
import org.binqua.forex.feed.starter.controller.{SubscriptionControllerModule, SubscriptionControllerModuleImpl}
import org.binqua.forex.util.ChildMaker

import scala.concurrent.duration._

trait SubscriptionStarterModule {

  def subscriptionStarterBehavior(connectionRegistryRef: RecipientRef[connectionregistry.Manager.Command]): Behavior[SubscriptionStarterProtocol.Command]

}

trait SubscriptionStarterModuleImpl extends SubscriptionStarterModule {

  this: SubscriptionControllerModule =>

  override def subscriptionStarterBehavior(connectionRegistryRef: RecipientRef[connectionregistry.Manager.Command]): Behavior[SubscriptionStarterProtocol.Command] = SubscriptionStarter(
    timeoutToConnectToTheConnectionRegistry = 30.seconds,
    subscriptionControllerChildMaker = ChildMaker.fromContext[SubscriptionStarterProtocol.Message](childNamePrefix = "subscriptionController").withBehavior(subscriptionControllerBehavior())(),
    connectionRegistryRef = connectionRegistryRef,
    supportMessages = SupportMessagesImpl
  ).narrow

}

trait ProductionSubscriptionStarterModule
  extends SubscriptionStarterModuleImpl
    with SubscriptionControllerModuleImpl
    with CollaboratorsWatcherImpl
    with CurrencyPairsSubscriberModuleImpl
    with CurrencyPairSubscriberModuleImpl
    with ServiceFinderModuleImpl
    with SubscriberModuleImpl
