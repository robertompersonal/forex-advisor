package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.typed.Behavior
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModel.actorNameDecorator
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.Message
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModule
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.{ServiceFinderModule, ServicesFinderProtocol}
import org.binqua.forex.util.ChildMaker

trait CurrencyPairsSubscriberModule {

  def currencyPairsSubscriberBehavior(): Behavior[CurrencyPairsSubscriberProtocol.Command]

}

object CurrencyPairsSubscriberModule {

  def currencyPairsSubscriberActorName(socketId: SocketId): String = s"MultipleCurrencyPairSubscriber-socketId-${socketId.id}"

  def currencyPairSubscriberActorName(socketId: SocketId, currencyPair: CurrencyPair): String =
    s"SingleCurrencyPairSubscriber-socketId-${socketId.id}-currencyPair-$currencyPair"

}

trait CurrencyPairsSubscriberModuleImpl extends CurrencyPairsSubscriberModule {

  this: CurrencyPairSubscriberModule with ServiceFinderModule =>

  override def currencyPairsSubscriberBehavior(): Behavior[CurrencyPairsSubscriberProtocol.Command] = {
    import scala.concurrent.duration._

    CurrencyPairsSubscriberActor(
      timeoutBeforeReportingSubscriptionResults = 4.seconds,
      servicesFinderChildMaker =
        ChildMaker.fromContext[Message](childNamePrefix = "ServicesFinder").withBehavior[ServicesFinderProtocol.Command](serviceFinderBehavior())(),
      childMaker = currencypair =>
        ChildMaker.fromContext[Message](actorNameDecorator("currencyPairSubscriber", currencypair)).withBehavior(currencyPairSubscriberBehavior(4.seconds))(),
      supportMessageUtil = SupportMessageUtil.wholeSubscriptionMessage(SupportMessageUtil.newSubscriptionMessage)(SupportMessageUtil.summaryMessage),
      SupportMessageUtil.defaultSupportMessage
    ).narrow

  }

}
