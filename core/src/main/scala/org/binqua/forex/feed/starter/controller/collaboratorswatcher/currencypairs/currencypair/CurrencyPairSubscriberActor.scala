package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, RecipientRef}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol.{PrivateRetryNow, _}
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages

import scala.concurrent.duration.FiniteDuration

private[currencypair] object CurrencyPairSubscriberActor {

  def apply(httpRetryInterval: FiniteDuration, supportMessages: SupportMessages): Behavior[Message] =
    Behaviors.withTimers(timers =>
      Behaviors.setup[Message] { parentContext =>
        def setUpBehaviour(
            httpClientSubscriberAdapter: ActorRef[HttpClientSubscriberProtocol.Response],
            unhandledBehavior: AkkaUtil.UnhandledBehavior[Message]
        )(implicit
            handledMessages: TheOnlyHandledMessages
        ): Behavior[Message] = {

          def waitingForRetryNowBehavior(
              incomingSubscribeParameters: StartSubscription,
              subscribeToMessage: HttpClientSubscriberServiceProtocol.SubscribeTo,
              nextRetryAttemptCounter: Int
          ): Behavior[Message] = {
            Behaviors.receiveMessage({
              case Stop => Behaviors.stopped

              case PrivateRetryNow =>
                incomingSubscribeParameters.httpSubscribeRef ! subscribeToMessage

                subscribeBehavior(incomingSubscribeParameters, subscribeToMessage, nextRetryAttemptCounter + 1)

              case u: WrappedHttpClientSubscriberResponse => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(PrivateRetryNow))
              case u: StartSubscription                   => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(PrivateRetryNow))

            })
          }

          def waitingToBeStoppedBehavior(currencyPair: CurrencyPair): Behavior[Message] = {
            parentContext.log.info(supportMessages.waitingToBeStoppedBehavior(currencyPair))
            Behaviors.receiveMessage({
              case Stop => Behaviors.stopped

              case PrivateRetryNow                        => unhandledBehavior.withMsg[Message](PrivateRetryNow)(TheOnlyHandledMessages(Stop))
              case u: StartSubscription                   => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(Stop))
              case u: WrappedHttpClientSubscriberResponse => unhandledBehavior.withMsg[Message](u)(TheOnlyHandledMessages(Stop))
            })
          }

          def subscribeBehavior(
              incomingSubscriptionParameter: StartSubscription,
              subscribeToMessage: HttpClientSubscriberServiceProtocol.SubscribeTo,
              nextRetryAttemptCounter: Int
          ): Behavior[Message] = {
            implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Stop, WrappedHttpClientSubscriberResponse)
            Behaviors.receiveMessage({
              case Stop => Behaviors.stopped

              case WrappedHttpClientSubscriberResponse(httpResponse) =>
                httpResponse match {
                  case HttpClientSubscriberProtocol.SubscriptionSuccess(_, _) =>
                    incomingSubscriptionParameter.socketIOClientRef ! SocketIOClientProtocol.SubscribeTo(
                      incomingSubscriptionParameter.currencyPair,
                      incomingSubscriptionParameter.socketId,
                      incomingSubscriptionParameter.whoWantsToKnoAboutSubscriptionResult,
                      incomingSubscriptionParameter.internalFeedRef
                    )
                    waitingToBeStoppedBehavior(incomingSubscriptionParameter.currencyPair)

                  case msg @ HttpClientSubscriberProtocol.SubscriptionFailure(_, _, _) =>
                    parentContext.log.info(supportMessages.message(RetryingDueToHttpServerFailure(nextRetryAttemptCounter, httpRetryInterval, msg)))
                    timers.startSingleTimer("retryTimer", PrivateRetryNow, httpRetryInterval)

                    waitingForRetryNowBehavior(incomingSubscriptionParameter, subscribeToMessage, nextRetryAttemptCounter)
                }

              case PrivateRetryNow      => unhandledBehavior.withMsg[Message](PrivateRetryNow)(handled)
              case u: StartSubscription => unhandledBehavior.withMsg[Message](u)(handled)
            })
          }

          Behaviors.receiveMessage({
            case startSubscription: StartSubscription =>
              val subscribeToMessage: HttpClientSubscriberServiceProtocol.SubscribeTo =
                HttpClientSubscriberServiceProtocol.SubscribeTo(startSubscription.currencyPair, startSubscription.socketId, httpClientSubscriberAdapter)
              startSubscription.httpSubscribeRef ! subscribeToMessage
              subscribeBehavior(startSubscription, subscribeToMessage, nextRetryAttemptCounter = 1)
            case Stop => Behaviors.stopped

            case PrivateRetryNow                        => unhandledBehavior.withMsg[Message](PrivateRetryNow)
            case u: WrappedHttpClientSubscriberResponse => unhandledBehavior.withMsg[Message](u)
          })
        }

        setUpBehaviour(
          parentContext.messageAdapter(WrappedHttpClientSubscriberResponse),
          AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext)
        )(TheOnlyHandledMessages(StartSubscription, Stop))

      }
    )

}

object CurrencyPairSubscriberProtocol {

  sealed trait Message

  sealed trait Command extends Message

  final case class StartSubscription(
      whoWantsToKnoAboutSubscriptionResult: ActorRef[SocketIOClientProtocol.SubscriptionsResult],
      currencyPair: CurrencyPair,
      socketId: SocketId,
      socketIOClientRef: ActorRef[SocketIOClientProtocol.Command],
      httpSubscribeRef: RecipientRef[HttpClientSubscriberServiceProtocol.SubscribeTo],
      internalFeedRef: ActorRef[SocketIOClientProtocol.FeedContent]
  ) extends Command

  final case object Stop extends Command

  sealed trait PrivateMessage extends Message

  final case class WrappedHttpClientSubscriberResponse(response: HttpClientSubscriberProtocol.Response) extends PrivateMessage

  final case object PrivateRetryNow extends PrivateMessage

}
