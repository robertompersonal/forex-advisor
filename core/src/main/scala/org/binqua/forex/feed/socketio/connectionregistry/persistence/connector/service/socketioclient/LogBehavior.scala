package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, SupervisorStrategy}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.LogProtocol.{Error, Info, Log}

private[socketioclient] trait LogBehavior {
  val logBehavior: Behavior[LogProtocol.Log]
}

private[socketioclient] trait ProductionLogBehavior extends LogBehavior {
  val logBehavior: Behavior[LogProtocol.Log] = Behaviors.receive[Log] { (context, message) =>
    message match {
      case Info(m) =>
        context.log.info(m)
        Behaviors.same
      case Error(m) =>
        context.log.error(m)
        Behaviors.same
    }
  }
}

private[socketioclient] object ResumableLogActor {
  val defaultLogActorMaker: CustomisableSocketIOClientActor.LogActorMaker = parentContext =>
    behavior => {
      parentContext.spawn(Behaviors.supervise(behavior).onFailure[Exception](SupervisorStrategy.resume), "actorLog")
    }
}

object LogProtocol {

  sealed trait Log

  final case class Info(messageKey: String) extends Log

  final case class Error(message: String) extends Log

}
