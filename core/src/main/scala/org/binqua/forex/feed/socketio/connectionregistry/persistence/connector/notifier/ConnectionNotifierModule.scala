package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.actor.typed.Behavior

import scala.concurrent.duration._

trait ConnectionNotifierModule {

  def connectionNotifier(): Behavior[ConnectionNotifierProtocol.Command]

}

trait ProductionConnectionNotifierModule extends ConnectionNotifierModule {

  override def connectionNotifier(): Behavior[ConnectionNotifierProtocol.Command] = ConnectionNotifier(timeout = 2.seconds, supportMessages = SupportMessagesImpl).narrow

}
