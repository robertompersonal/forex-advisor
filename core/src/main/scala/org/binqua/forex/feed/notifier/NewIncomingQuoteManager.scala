package org.binqua.forex.feed.notifier

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import cats.kernel.Eq
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair, QuoteCurrency}
import org.binqua.forex.feed.notifier.collaborators.{DueToUpdatedQuoteDirectly, UpdatingAccount, UpdatingAccountFactory}
import org.binqua.forex.feed.notifier.publisher.MultipleAccountCurrenciesPublisher
import org.binqua.forex.feed.notifier.publisher.MultipleAccountCurrenciesPublisher.NotifyAccounts
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.implicits.instances.quoteCurrency._
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

object NewIncomingQuoteManager {

  sealed trait Command

  final case class QuoteUpdated(updatedQuote: Quote) extends Command

  type State = Map[CurrencyPair, Quote]

  final case class HandlerContext(
      allAccountCurrencies: List[AccountCurrency],
      quoteUpdatedNotifierActor: ActorRef[MultipleAccountCurrenciesPublisher.Command],
      updatingAccountFactory: UpdatingAccountFactory,
      allCurrencyPairs: List[CurrencyPair],
      context: ActorContext[Command]
  )

  def apply(
      allAccountCurrencies: List[AccountCurrency],
      multipleAccountCurrenciesPublisher: CHILD_MAKER[Command, MultipleAccountCurrenciesPublisher.Command],
      updatingAccountInstances: UpdatingAccountFactory,
      allCurrencyPairs: List[CurrencyPair]
  ): Behavior[Command] =
    Behaviors.setup { actorContext =>
      theBehaviour(state = Map())(
        HandlerContext(allAccountCurrencies, multipleAccountCurrenciesPublisher(actorContext), updatingAccountInstances, allCurrencyPairs, actorContext)
      )
    }

  def theBehaviour(state: State)(implicit hc: HandlerContext): Behavior[Command] =
    Behaviors.receiveMessage[Command] {

      case QuoteUpdated(incomingQuote) =>
        val newState = state - incomingQuote.rates.currencyPair

        calculateUpdatingAccountsDueTo(incomingQuote, newState).foreach(updatingAccount => hc.quoteUpdatedNotifierActor ! NotifyAccounts(updatingAccount))

        theBehaviour(newState + (incomingQuote.rates.currencyPair -> incomingQuote))
    }

  private def findExchangeRateInPreviousReceivedQuotes(accountCurrency: AccountCurrency, quoteCurrency: QuoteCurrency, state: State)(implicit
      hc: HandlerContext
  ): Option[Quote] = {
    import org.binqua.forex.implicits.instances.accountCurrency._
    CurrencyPair.byBaseAndQuoteIdentifiersInAnyOrder((accountCurrency.toId, quoteCurrency.toId), hc.allCurrencyPairs).flatMap(state.get)
  }

  private def calculateUpdatingAccountLinkedDirectlyToTheUpdatedQuote(updatedQuote: Quote, accountCurrency: AccountCurrency, state: State)(implicit
      hc: HandlerContext
  ): Option[UpdatingAccount] =
    hc.updatingAccountFactory.create(updatedQuote, accountCurrency) match {
      case s @ Some(_) => s
      case None =>
        findExchangeRateInPreviousReceivedQuotes(accountCurrency, updatedQuote.rates.currencyPair.quoteCurrency, state).flatMap(quote =>
          hc.updatingAccountFactory.validate(updatedQuote, quote, accountCurrency, DueToUpdatedQuoteDirectly) match {
            case r @ Right(_) => r.toOption
            case Left(error) =>
              hc.context.log.error(error)
              None
          }
        )
    }

  private def calculateUpdatingAccountsDueTo(updatedQuote: Quote, state: State)(implicit hc: HandlerContext): Option[List[UpdatingAccount]] = {
    val updatingAccountSet: List[UpdatingAccount] = hc.allAccountCurrencies.flatMap(accountCurrency => {

      val updatingAccountsLinkedToPipValueChanges = calculateUpdatingAccountsLinkedToPipValueChange(updatedQuote, accountCurrency, state, hc.context)

      calculateUpdatingAccountLinkedDirectlyToTheUpdatedQuote(updatedQuote, accountCurrency, state)
        .map(_ :: updatingAccountsLinkedToPipValueChanges)
        .getOrElse(updatingAccountsLinkedToPipValueChanges)
    })

    updatingAccountSet match {
      case Nil     => None
      case entries => Some(entries)
    }
  }

  private def calculateUpdatingAccountsLinkedToPipValueChange(
      updatedQuote: Quote,
      accountCurrency: AccountCurrency,
      state: State,
      context: ActorContext[Command]
  ): List[UpdatingAccount] = {
    val quoteCurrencyAffected: Option[QuoteCurrency] = accountCurrency.extractQuoteCurrencyAffectedByQuoteUpdatedOf(updatedQuote.rates.currencyPair)

    val previousUpdatedQuotesWhosePipValueMayChange = findPreviousQuotesByQuoteCurrency(state, quoteCurrencyAffected)

    previousUpdatedQuotesWhosePipValueMayChange.foldLeft(List[UpdatingAccount]())((acc, previousUpdatedQuote) =>
      UpdatingAccount.validate(previousUpdatedQuote, updatedQuote, accountCurrency, collaborators.DueToPipValue) match {
        case Right(value) =>
          value :: acc
        case Left(error) =>
          context.log.error(error)
          acc
      }
    )
  }

  private def findPreviousQuotesByQuoteCurrency(state: Map[CurrencyPair, Quote], optionalQuoteCurrencyAffected: Option[QuoteCurrency]): List[Quote] =
    optionalQuoteCurrencyAffected
      .map(quoteCurrencyAffected =>
        state
          .filter(i => {
            val (currencyPairAlreadyReceived, _) = i
            Eq.eqv(currencyPairAlreadyReceived.quoteCurrency, quoteCurrencyAffected)
          })
          .values
          .toList
      )
      .getOrElse(Nil)

}
