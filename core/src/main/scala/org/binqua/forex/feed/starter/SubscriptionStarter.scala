package org.binqua.forex.feed.starter

import akka.actor.typed._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.util.Timeout
import cats.syntax.option._
import org.binqua.forex.JsonSerializable
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol.{
  NewSocketId,
  RegistrationComplete,
  RetryRegistration,
  WrappedSubscriptionsControllerResponse,
  _
}
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

import scala.util.{Failure, Success}

object SubscriptionStarter {

  private val dontDoAnything: () => Unit = () => {}

  def apply(
      timeoutToConnectToTheConnectionRegistry: Timeout,
      subscriptionControllerChildMaker: CHILD_MAKER[Message, SubscriptionControllerProtocol.Start],
      connectionRegistryRef: RecipientRef[connectionregistry.Manager.Command],
      supportMessages: SupportMessages
  ): Behavior[Message] =
    Behaviors.setup[Message] { parentContext =>
      def setUp(adapters: Adapters, unhandledBehavior: AkkaUtil.UnhandledBehavior[Message])(implicit handled: TheOnlyHandledMessages): Behavior[Message] = {

        def unwatchAndStop[M](aChild: ActorRef[M]): () => Unit =
          () => {
            parentContext.unwatch(aChild)
            parentContext.stop(aChild)
          }

        def register(registrationAttempt: Int): Unit = {
          parentContext.log.info(supportMessages.registrationStarted(registrationAttempt))
          implicit val timeout: Timeout = timeoutToConnectToTheConnectionRegistry
          parentContext.ask(
            connectionRegistryRef,
            (replyTo: ActorRef[Persistence.Response]) => connectionregistry.Manager.Register(replyTo, parentContext.self, "FeedSubscriptionStarter")
          )({
            case Success(Persistence.Registered) => RegistrationComplete
            case Failure(_)                      => RetryRegistration
          })
        }

        def createNewChildAndRunAnotherSubscription(theMostRecentSocketId: SocketId): ActorRef[SubscriptionControllerProtocol.Start] = {
          val subscriptionsManagerRef = subscriptionControllerChildMaker(parentContext)
          parentContext.watch(subscriptionsManagerRef)

          subscriptionsManagerRef ! SubscriptionControllerProtocol.Start(adapters.subscriptionsController, theMostRecentSocketId)
          subscriptionsManagerRef
        }

        def waitForChildTerminated(mostRecentSocketId: SocketId): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
          case (_, Terminated(_)) =>
            parentContext.log.info(supportMessages.subscriptionExecutionCompleted(mostRecentSocketId))
            waitForANewSocketIdBehavior
        }

        def waitForChildFailed(mostRecentSocketId: SocketId): PartialFunction[(ActorContext[Message], Signal), Behavior[Message]] = {
          case (_, ChildFailed(_, _)) =>
            parentContext.log.info(supportMessages.subscriptionExecutorFailed(mostRecentSocketId))
            val runningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
            waitForExecutionStartedBehavior(mostRecentSocketId, runningChild, doThisBeforeCreateANewChild = unwatchAndStop(runningChild))
        }

        def waitForExecutionStartedBehavior(
            mostRecentSocketId: SocketId,
            theRunningChild: ActorRef[SubscriptionControllerProtocol.Start],
            doThisBeforeCreateANewChild: () => Unit
        ): Behavior[Message] = {
          implicit val handled: TheOnlyHandledMessages =
            TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionStarted(mostRecentSocketId))
          Behaviors
            .receiveMessage[Message] {
              case NewSocketId(replyTo, theNewSocketId) =>
                newSocketIdHandler(replyTo, theNewSocketId, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))

              case WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(`mostRecentSocketId`)) =>
                parentContext.log.info(supportMessages.subscriptionExecutionStarted(mostRecentSocketId))
                subscriptionIsRunningBehavior(mostRecentSocketId, theRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))

              case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
              case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
              case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
              case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
            }
            .receiveSignal(
              waitForChildFailed(mostRecentSocketId)
            )
        }

        def subscriptionIsRunningBehavior(
            mostRecentSocketId: SocketId,
            theOldRunningChild: ActorRef[SubscriptionControllerProtocol.Start],
            doThisBeforeCreateANewChild: () => Unit
        ): Behavior[Message] = {
          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionFailed(mostRecentSocketId))
          Behaviors
            .receiveMessage[Message] {
              case NewSocketId(replyTo, theNewSocketId) =>
                newSocketIdHandler(replyTo, theNewSocketId, doThisBeforeCreateANewChild = unwatchAndStop(theOldRunningChild))

              case WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(`mostRecentSocketId`)) =>
                parentContext.log.info(supportMessages.subscriptionExecutionFailed(mostRecentSocketId))
                unwatchAndStop(theOldRunningChild)()
                val theNewRunningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
                waitForExecutionStartedBehavior(mostRecentSocketId, theNewRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theNewRunningChild))

              case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
              case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
              case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
              case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
            }
            .receiveSignal(
              waitForChildFailed(mostRecentSocketId)
                .orElse(waitForChildTerminated(mostRecentSocketId))
            )
        }

        def newSocketIdHandler(
            replyTo: ActorRef[NewFeedSocketIdUpdated],
            theNewSocketId: SocketId,
            doThisBeforeCreateANewChild: () => Unit
        ): Behavior[Message] = {
          parentContext.log.info(supportMessages.newSocketIdReceived(theNewSocketId, None))
          replyTo ! NewFeedSocketIdUpdated(theNewSocketId)

          doThisBeforeCreateANewChild()

          val theRunningChild = createNewChildAndRunAnotherSubscription(theNewSocketId)
          waitForExecutionStartedBehavior(theNewSocketId, theRunningChild, unwatchAndStop(theRunningChild))
        }

        def waitForANewSocketIdBehavior: Behavior[Message] = {
          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId)
          Behaviors.receiveMessage {
            case NewSocketId(replyTo, theNewSocketId) => newSocketIdHandler(replyTo, theNewSocketId, dontDoAnything)

            case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
            case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
            case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
            case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
          }
        }

        def waitForRegistrationCompletedBehavior(maybeAnOlderSocketId: Option[SocketId], registrationAttempt: Int): Behavior[Message] = {
          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(NewSocketId, RegistrationComplete, RetryRegistration)
          Behaviors.receiveMessage({
            case NewSocketId(replyTo, newSocketId) =>
              replyTo ! NewFeedSocketIdUpdated(newSocketId)
              waitForRegistrationCompletedBehavior(newSocketId.some, registrationAttempt)

            case RegistrationComplete =>
              parentContext.log.info(supportMessages.registrationCompleted())

              maybeAnOlderSocketId match {
                case None =>
                  waitForANewSocketIdBehavior
                case Some(mostRecentSocketId) =>
                  val theRunningChild = createNewChildAndRunAnotherSubscription(mostRecentSocketId)
                  waitForExecutionStartedBehavior(mostRecentSocketId, theRunningChild, doThisBeforeCreateANewChild = unwatchAndStop(theRunningChild))
              }

            case RetryRegistration =>
              parentContext.log.info(supportMessages.registrationTimeout(registrationAttempt, timeoutToConnectToTheConnectionRegistry))
              register(registrationAttempt + 1)
              waitForRegistrationCompletedBehavior(maybeAnOlderSocketId, registrationAttempt + 1)

            case StartASubscription                        => unhandledBehavior.withMsg(StartASubscription)
            case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
          })
        }

        Behaviors.receiveMessage[Message]({
          case StartASubscription =>
            parentContext.log.info(supportMessages.subscriptionStarted())
            register(registrationAttempt = 1)
            waitForRegistrationCompletedBehavior(maybeAnOlderSocketId = None, registrationAttempt = 1)

          case u: NewSocketId                            => unhandledBehavior.withMsg(u)
          case RegistrationComplete                      => unhandledBehavior.withMsg(RegistrationComplete)
          case RetryRegistration                         => unhandledBehavior.withMsg(RetryRegistration)
          case u: WrappedSubscriptionsControllerResponse => unhandledBehavior.withMsg(u)
        })
      }

      setUp(Adapters(parentContext), AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext))(
        TheOnlyHandledMessages(StartASubscription)
      )

    }

  private case class Adapters(private val context: ActorContext[SubscriptionStarterProtocol.Message]) {
    val subscriptionsController: ActorRef[SubscriptionControllerProtocol.Response] = context.messageAdapter(WrappedSubscriptionsControllerResponse)
  }

}

object SubscriptionStarterProtocol {

  sealed trait Message extends JsonSerializable

  sealed trait Command extends Message

  final case object StartASubscription extends Command

  final case class NewSocketId(replyTo: ActorRef[NewFeedSocketIdUpdated], socketId: SocketId) extends Command

  sealed trait PrivateCommand extends Message

  final case class WrappedSubscriptionsControllerResponse(response: SubscriptionControllerProtocol.Response) extends PrivateCommand

  final case object RegistrationComplete extends PrivateCommand

  final case object RetryRegistration extends PrivateCommand

  sealed trait Response extends JsonSerializable

  final case class NewFeedSocketIdUpdated(socketId: SocketId) extends Response

}
