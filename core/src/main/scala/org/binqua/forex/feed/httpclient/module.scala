package org.binqua.forex.feed.httpclient

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef, EntityTypeKey}
import org.binqua.forex.util.core

trait HttpClientSubscriberModule {

  def httpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo]

}

trait DefaultHttpClientSubscriberModule extends HttpClientSubscriberModule {

  override def httpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo] =
    HttpClientSubscriber(
      akkaConfig => core.makeItUnsafe(ConfigValidator(akkaConfig)),
      BodyReaderPlayImpl,
      ErrorSituationHandlerImpl
    ).narrow[HttpClientSubscriberProtocol.SubscribeTo]

}

trait SupervisedHttpSubscriberModule {

  def supervisedHttpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo]

}

trait HttpSubscriberInitializedModule {

  def ref: EntityRef[HttpClientSubscriberProtocol.SubscribeTo]

}

trait ShardedHttpClientSubscriberModule {

  def initialisedHttpSubscriber(clusterSharding: ClusterSharding): HttpSubscriberInitializedModule

}

trait ResumeHttpSubscriberModule extends SupervisedHttpSubscriberModule {

  this: HttpClientSubscriberModule =>

  final def supervisedHttpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo] =
    Behaviors.supervise(httpClientSubscriber()).onFailure[Exception](SupervisorStrategy.resume)

}

trait ShardedHttpClientSubscriberModuleImpl extends ShardedHttpClientSubscriberModule with ResumeHttpSubscriberModule {

  this: HttpClientSubscriberModule =>

  final override def initialisedHttpSubscriber(clusterSharding: ClusterSharding): HttpSubscriberInitializedModule = {

    val HttpClientTypeKey = EntityTypeKey[HttpClientSubscriberProtocol.SubscribeTo]("httpClientSubscriber")

    clusterSharding.init(Entity(HttpClientTypeKey)(createBehavior = _ => supervisedHttpClientSubscriber()).withRole(HttpClientTypeKey.name))

    new HttpSubscriberInitializedModule {
      def ref: EntityRef[HttpClientSubscriberProtocol.SubscribeTo] = clusterSharding.entityRefFor(HttpClientTypeKey, HttpClientTypeKey.name)
    }
  }
}

trait ProductionShardedModule extends ShardedHttpClientSubscriberModuleImpl with DefaultHttpClientSubscriberModule {}
