package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import cats.data.Validated
import cats.syntax.either._
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheckExternal.configValidator
import org.binqua.forex.util.core.{ConfigValidator, UnsafeConfigReader}
import org.binqua.forex.util.{ValidateConfigByReference, core}

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

trait SupportMessages {

  def cassandraNotInitYet(): String

  def cassandraIsHealthy(): String
}

object UnsafeConfigReader extends UnsafeConfigReader[Config] {
  override def apply(akkaConfig: AkkaConfig): Config = core.makeItUnsafe(configValidator(akkaConfig))
}

trait SupportMessagesFactory {
  def createSupportMessages(configuration: Config): SupportMessages
}

private[healthcheck] object SimpleSupportMessagesFactory extends SupportMessagesFactory {
  override def createSupportMessages(configuration: Config): SupportMessages =
    new SupportMessages {
      override def cassandraNotInitYet(): String = message(s"does not have all tables akka needs yet. Retry in ${configuration.retryDelay}")

      override def cassandraIsHealthy(): String = message("has all tables akka needs")

      private def message(toBeAppended: String): String =
        s"Cassandra located at ${configuration.host}:${configuration.port} with local datacenter ${configuration.localDataCenter} $toBeAppended"
    }
}

sealed abstract case class Config(host: String, port: Int, localDataCenter: String, retryDelay: FiniteDuration)

object Config {

  import cats.instances.list._
  import cats.syntax.apply._

  private def toConfig(cassandraHost: String, cassandraPort: Int, localDataCenter: String, retryDelay: FiniteDuration): Config =
    new Config(cassandraHost, cassandraPort, localDataCenter, retryDelay) {}

  def validated(cassandraHost: String, cassandraPort: Int, localDataCenter: String, retryDelay: FiniteDuration): Validated[List[String], Config] = {
    (
      Either.cond(cassandraHost.trim.nonEmpty, cassandraHost, List("cassandra host has to be not empty")).toValidated,
      Either.cond(cassandraPort > 0, cassandraPort, List("cassandra port has to be not empty")).toValidated,
      Either.cond(localDataCenter.trim.nonEmpty, localDataCenter, List("cassandra dataCenter has to be not empty")).toValidated
    ).mapN((cassandraHost, cassandraPort, localDataCenter) => toConfig(cassandraHost, cassandraPort, localDataCenter, retryDelay))
  }

  object Validator extends ConfigValidator[Config] {

    import cats.instances.list._
    import cats.syntax.apply._

    private def toValidated[A](arg: Either[String, A]): Validated[List[String], A] = arg.leftMap(List(_)).toValidated

    override def apply(akkaConfig: AkkaConfig): Validated[List[String], Config] = {
      object configurationKeys {
        val hostUrl = "datastax-java-driver.basic.contact-points"
        val dataCenter = "datastax-java-driver.basic.load-balancing-policy.local-datacenter"
        val delay = "org.binqua.forex.feed.socketio.connectionregistry.healthcheck.retryDelay"
      }

      def cassandraHost: Either[String, String] =
        for {
          hostAsString <- Try(hostPlusPort(0)).toEither.leftMap(_ => cannotFindKey("host", s"${configurationKeys.hostUrl} = [host:port]"))
          _ <- Either.cond(hostAsString.trim.nonEmpty, hostAsString, s"host in ${configurationKeys.hostUrl} has to be non empty")
        } yield hostAsString

      def hostPlusPort: Array[String] = akkaConfig.getStringList(configurationKeys.hostUrl).get(0).split(":")

      def cassandraPort: Either[String, Int] =
        for {
          port <- Try(hostPlusPort(1).toInt).toEither.leftMap(_ => cannotFindKey("port", s"${configurationKeys.hostUrl} = [host:port]"))
          _ <- Either.cond(port > 0, port, s"port in ${configurationKeys.hostUrl} has to be > 0")
        } yield port

      def cannotFindKey(custom: String, details: String) = s"cannot find cassandra $custom value ... key $details is missing"

      def dataCenter: Either[String, String] =
        for {
          dataCenter <- Try(akkaConfig.getString(configurationKeys.dataCenter)).toEither.leftMap(_ => cannotFindKey("dataCenter", configurationKeys.dataCenter))
          _ <- Either.cond(dataCenter.trim.nonEmpty, dataCenter, s"dataCenter in ${configurationKeys.dataCenter} has to be non empty")
        } yield dataCenter

      def delay: Validated[List[String], FiniteDuration] =
        ValidateConfigByReference.validateADurationConfigValue(akkaConfig, configurationKeys.delay)

      (
        toValidated(cassandraHost),
        toValidated(cassandraPort),
        toValidated(dataCenter),
        delay
      ).mapN((host, port, dataCenter, delay) => toConfig(host, port, dataCenter, delay))
        .leftMap(errors => "Cassandra health check configuration is invalid." :: errors)

    }

  }
}
