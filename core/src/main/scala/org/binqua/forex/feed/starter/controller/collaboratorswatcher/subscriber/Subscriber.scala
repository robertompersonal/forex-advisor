package org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, RecipientRef}
import cats.kernel.Eq
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol._
import org.binqua.forex.implicits.instances.socketId._
import org.binqua.forex.util.AkkaUtil
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages

import scala.concurrent.duration.FiniteDuration

object Subscriber {

  def apply(currencyPairs: Set[CurrencyPair], retryToSubscribeIntervalAfterPartialSubscription: FiniteDuration): Behavior[Message] =
    Behaviors.withTimers(timers => {
      Behaviors.setup(parentContext => {

        def firstBehavior(
            thisActorAsSubscriberResponseReceiver: ActorRef[CurrencyPairsSubscriberProtocol.Subscriptions],
            unhandledBehavior: AkkaUtil.UnhandledBehavior[Message]
        ): Behavior[Message] = {

          def afterStartBehavior(
              socketId: SocketId,
              currencyPairsSubscribersRef: RecipientRef[CurrencyPairsSubscriberProtocol.Command],
              replyToDuringSubscription: ActorRef[Response]
          ): Behavior[Message] = {

            def waitingForInternalRetrySubscriptionBehavior(state: SubscriberModel.State): Behaviors.Receive[Message] = {
              implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(InternalRetrySubscription)
              Behaviors.receiveMessage[Message]({
                case InternalRetrySubscription =>
                  val newState = state.increaseNumberOfAttempts()
                  replyToDuringSubscription ! RunningSubscriptionNow(socketId, attempt = newState.attempt)
                  currencyPairsSubscribersRef ! CurrencyPairsSubscriberProtocol.Subscribe(state.unsubscribed, socketId, thisActorAsSubscriberResponseReceiver)
                  subscribeSentBehavior(newState)

                case u: Start                                  => unhandledBehavior.withMsg(u)
                case u: WrappedCurrencyPairsSubscriberResponse => unhandledBehavior.withMsg(u)
              })
            }

            def currencyPairsSubscriberResponseHandler(
                response: CurrencyPairsSubscriberProtocol.Subscriptions,
                state: SubscriberModel.State
            ): Behavior[Message] = {

              val CurrencyPairsSubscriberProtocol.Subscriptions(subscriptionSocketId, subscribed, _) = response

              if (Eq.eqv(subscriptionSocketId, socketId)) {

                val newState = state.recordSubscribed(subscribed)

                if (newState.isPartiallySubscribed) {

                  timers.startSingleTimer("retry", InternalRetrySubscription, retryToSubscribeIntervalAfterPartialSubscription)

                  waitingForInternalRetrySubscriptionBehavior(newState)

                } else {
                  replyToDuringSubscription ! FullySubscribed(socketId, state.attempt, currencyPairs)
                  Behaviors.stopped
                }
              } else
                Behaviors.same
            }

            def subscribeSentBehavior(state: SubscriberModel.State): Behaviors.Receive[Message] = {
              implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(WrappedCurrencyPairsSubscriberResponse)
              Behaviors.receiveMessage[Message]({
                case WrappedCurrencyPairsSubscriberResponse(response) => currencyPairsSubscriberResponseHandler(response, state)

                case u: Start                  => unhandledBehavior.withMsg(u)
                case InternalRetrySubscription => unhandledBehavior.withMsg(InternalRetrySubscription)
              })
            }

            subscribeSentBehavior(SubscriberModel.State.emptyState(currencyPairs))
          }

          implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Start)

          Behaviors.receiveMessage[Message]({
            case Start(socketId, currencyPairsSubscribersRef, replyToDuringProcess) =>
              replyToDuringProcess ! RunningSubscriptionNow(socketId, attempt = 1)
              currencyPairsSubscribersRef ! CurrencyPairsSubscriberProtocol.Subscribe(currencyPairs, socketId, thisActorAsSubscriberResponseReceiver)
              afterStartBehavior(socketId, currencyPairsSubscribersRef, replyToDuringProcess)

            case InternalRetrySubscription                 => unhandledBehavior.withMsg(InternalRetrySubscription)
            case u: WrappedCurrencyPairsSubscriberResponse => unhandledBehavior.withMsg(u)
          })

        }

        firstBehavior(
          parentContext.messageAdapter(WrappedCurrencyPairsSubscriberResponse),
          AkkaUtil.commandUnhandledLoggedAsInfoBehavior[Message](parentContext)
        )

      })

    })

}

object SubscriberProtocol {

  sealed trait Message

  sealed trait Command extends Message

  sealed trait PrivateMessage extends Message

  final case class Start(
      socketId: SocketId,
      currencyPairsSubscribersRef: RecipientRef[CurrencyPairsSubscriberProtocol.Command],
      replyToDuringSubscription: ActorRef[Response]
  ) extends Command

  private[subscriber] final case class WrappedCurrencyPairsSubscriberResponse(response: CurrencyPairsSubscriberProtocol.Subscriptions) extends PrivateMessage

  private[subscriber] final case object InternalRetrySubscription extends PrivateMessage

  sealed trait Response

  final case class FullySubscribed(socketId: SocketId, attempt: Int, currencyPairs: Set[CurrencyPair]) extends Response

  final case class RunningSubscriptionNow(socketId: SocketId, attempt: Int) extends Response

}

object SubscriberModel {

  object State {
    def emptyState(currencyPairs: Set[CurrencyPair]) = State(currencyPairs, Set.empty, attempt = 1)
  }

  case class State(currencyPairs: Set[CurrencyPair], subscribed: Set[CurrencyPair], attempt: Int) {

    require(attempt >= 1, s"subscriptionAttempts must be >= 1. $attempt is wrong")
    require(!currencyPairs.isEmpty, "empty currency pairs is not allowed")
    require(
      subscribed.forall(currencyPairs.contains),
      s"cannot have subscribed ${subscribed.find(!currencyPairs.contains(_)).get} that are not in currency pairs $currencyPairs"
    )

    val unsubscribed: Set[CurrencyPair] = currencyPairs -- subscribed

    val isPartiallySubscribed: Boolean = currencyPairs.exists(!subscribed.contains(_))

    def increaseNumberOfAttempts(): State = copy(attempt = this.attempt + 1)

    def recordSubscribed(subscribed: Set[CurrencyPair]): State = copy(subscribed = this.subscribed ++ subscribed)
  }

}
