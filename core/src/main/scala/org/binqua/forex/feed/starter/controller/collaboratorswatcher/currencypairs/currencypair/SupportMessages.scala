package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import org.binqua.forex.advisor.model.CurrencyPair

trait SupportMessages {

  def waitingToBeStoppedBehavior(currencyPair: CurrencyPair): String

  def message(failure: CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure): String


}
