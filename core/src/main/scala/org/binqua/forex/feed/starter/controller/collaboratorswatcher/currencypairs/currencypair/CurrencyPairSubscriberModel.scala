package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol
import org.binqua.forex.util.ErrorSituation

import scala.concurrent.duration.FiniteDuration

object CurrencyPairSubscriberModel {

  sealed trait Failure extends ErrorSituation

  case class RetryingDueToHttpServerFailure(retryAttemptCounter: Int, retryInterval: FiniteDuration, hse: HttpClientSubscriberProtocol.SubscriptionFailure)
      extends Failure

  case class CurrencyPairSubscriberConfig(httpRetryInterval: FiniteDuration)

}
