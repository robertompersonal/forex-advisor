package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.typed.Behavior
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.{ProductionSocketIOManagerModule, SocketIOManagerModule}
import org.binqua.forex.util.ChildMaker

trait Module {

  def persistenceBehavior(): Behavior[Persistence.Command]

}

trait DefaultModule extends Module {

  this: SocketIOManagerModule =>

  override def persistenceBehavior(): Behavior[Persistence.Command] =
    Persistence(
      uniqueId = "PersistenceConnectionRegistryActor",
      connectorChildMaker = ChildMaker.fromContext[Persistence.Message](childNamePrefix = "socketIOManager").withBehavior(socketIOManager())(),
      supportMessages = SimpleSupportMessages
    ).narrow

}

trait ProductionModule extends DefaultModule with ProductionSocketIOManagerModule {}
