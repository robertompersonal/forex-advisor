package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, ChildFailed}
import akka.persistence.typed.scaladsl.{Effect, EffectBuilder, EventSourcedBehavior}
import akka.persistence.typed.{PersistenceId, RecoveryCompleted}
import org.binqua.forex.JsonSerializable
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.ChildMaker.CHILD_MAKER

object Persistence {

  sealed trait Message

  sealed trait Command extends Message

  final case class Register(
      registrationRequester: ActorRef[Response],
      subscriptionStarter: ActorRef[SubscriptionStarterProtocol.NewSocketId],
      requesterAlias: String
  ) extends Command

  sealed trait Response extends JsonSerializable

  final case object Registered extends Response

  sealed trait Event extends JsonSerializable

  final case class Registered(subscriptionStarter: ActorRef[SubscriptionStarterProtocol.NewSocketId], requesterAlias: String) extends Event

  private[connectionregistry] def apply(
      uniqueId: String,
      connectorChildMaker: CHILD_MAKER[Message, Connector.Command],
      supportMessages: SupportMessages
  ): Behavior[Message] =
    Behaviors.setup[Message](parentContext => {

      var connector = connectorChildMaker(parentContext)

      parentContext.watch(connector)

      connector ! Connector.Connect

      def persist(registerCommand: Register): EffectBuilder[Event, State] = {
        val Register(refThatRequestToRegister, subscriptionStarter, requesterAlias) = registerCommand
        Effect
          .persist(Registered(subscriptionStarter, requesterAlias))
          .thenRun(state => {
            parentContext.log.info(supportMessages.actorRegistered(state))
            refThatRequestToRegister ! Registered
            connector ! Connector.Notify(subscriptionStarter)
          })
      }

      val theCommandHandler: (State, Message) => Effect[Event, State] = (state, incomingCommand) => {
        incomingCommand match {
          case registerCommand @ Register(refThatRequestToRegister, newSubscriptionStarter, _) =>
            state.theMostRecent match {
              case Some(Registered(previousSubscriptionStarterRef, _)) =>
                if (previousSubscriptionStarterRef == newSubscriptionStarter) {
                  parentContext.log.info(supportMessages.actorAlreadyRegistered(registerCommand, state))
                  refThatRequestToRegister ! Registered
                  Effect.none
                } else {
                  persist(registerCommand)
                }
              case None =>
                persist(registerCommand)
            }
        }
      }

      val theEventHandler: (State, Event) => State = (state, evt) => {
        evt match {
          case newEvent: Registered => state.register(newEvent)
        }
      }

      EventSourcedBehavior[Message, Event, State](
        PersistenceId.ofUniqueId(uniqueId),
        emptyState = State.empty,
        commandHandler = theCommandHandler,
        eventHandler = theEventHandler
      ).receiveSignal {
        case (restoredState, RecoveryCompleted) =>
          restoredState.theMostRecent match {
            case None =>
            case Some(Registered(subscriptionStarter, _)) =>
              parentContext.log.info(supportMessages.stateRestored(restoredState))
              connector ! Connector.Notify(subscriptionStarter)
          }
        case (theState, ChildFailed(_, _)) =>
          connector = connectorChildMaker(parentContext)
          parentContext.watch(connector)
          connector ! Connector.Connect

          theState.theMostRecent.foreach(registered => connector ! Connector.Notify(registered.subscriptionStarter))
      }
    })

}
