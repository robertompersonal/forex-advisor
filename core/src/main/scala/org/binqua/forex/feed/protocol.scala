package org.binqua.forex.feed

import cats.kernel.Eq
import org.binqua.forex.advisor.model.AccountCurrency.{Eur, Gbp, Usd}
import org.binqua.forex.advisor.model.CurrencyPair._
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition
import org.binqua.forex.feed.notifier.collaborators.UpdatingAccount
import org.binqua.forex.feed.protocol.UpdatedCommandFactoryRepository.{
  CurrencyPairNotFound,
  UpdatedCommandFactoryRepositoryMismatchArguments,
  UpdatedCommandNotFound
}
import org.binqua.forex.util.{ErrorSituation, Validation}

object protocol {

  sealed trait Command

  sealed trait UpdatedCommand extends Command {
    val updatingAccount: UpdatingAccount
  }

  final case class NewPosition(portfolioIdentifier: String, recordedPosition: RecordedPosition) extends Command

  sealed trait Notification

  sealed abstract case class EurGbpUpdatedForEurAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class EurGbpUpdatedForGbpAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class EurGbpUpdatedForUsdAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class EurUsdUpdatedForEurAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class EurUsdUpdatedForGbpAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class EurUsdUpdatedForUsdAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class GbpUsdUpdatedForEurAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class GbpUsdUpdatedForGbpAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class GbpUsdUpdatedForUsdAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Spx500UpdatedForEurAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Spx500UpdatedForGbpAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Spx500UpdatedForUsdAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Us30UpdatedForEurAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Us30UpdatedForGbpAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  sealed abstract case class Us30UpdatedForUsdAccount(updatingAccount: UpdatingAccount) extends UpdatedCommand

  object withoutValidation {

    type ValidationParameters = (CurrencyPair, AccountCurrency, UpdatingAccount => UpdatedCommand)
    type UpdatedCommandCreator = UpdatingAccount => UpdatedCommand

    val eurGbpUpdatedForEurAccount: UpdatedCommandCreator = updatingAccount => new EurGbpUpdatedForEurAccount(updatingAccount) {}
    val eurGbpUpdatedForGbpAccount: UpdatedCommandCreator = updatingAccount => new EurGbpUpdatedForGbpAccount(updatingAccount) {}
    val eurGbpUpdatedForUsdAccount: UpdatedCommandCreator = updatingAccount => new EurGbpUpdatedForUsdAccount(updatingAccount) {}

    val eurUsdUpdatedForEurAccount: UpdatedCommandCreator = updatingAccount => new EurUsdUpdatedForEurAccount(updatingAccount) {}
    val eurUsdUpdatedForGbpAccount: UpdatedCommandCreator = updatingAccount => new EurUsdUpdatedForGbpAccount(updatingAccount) {}
    val eurUsdUpdatedForUsdAccount: UpdatedCommandCreator = updatingAccount => new EurUsdUpdatedForUsdAccount(updatingAccount) {}

    val gbpUsdUpdatedForEurAccount: UpdatedCommandCreator = updatingAccount => new GbpUsdUpdatedForEurAccount(updatingAccount) {}
    val gbpUsdUpdatedForGbpAccount: UpdatedCommandCreator = updatingAccount => new GbpUsdUpdatedForGbpAccount(updatingAccount) {}
    val gbpUsdUpdatedForUsdAccount: UpdatedCommandCreator = updatingAccount => new GbpUsdUpdatedForUsdAccount(updatingAccount) {}

    val spx500UpdatedForEurAccount: UpdatedCommandCreator = updatingAccount => new Spx500UpdatedForEurAccount(updatingAccount) {}
    val spx500UpdatedForGbpAccount: UpdatedCommandCreator = updatingAccount => new Spx500UpdatedForGbpAccount(updatingAccount) {}
    val spx500UpdatedForUsdAccount: UpdatedCommandCreator = updatingAccount => new Spx500UpdatedForUsdAccount(updatingAccount) {}

    val us30UpdatedForEurAccount: UpdatedCommandCreator = updatingAccount => new Us30UpdatedForEurAccount(updatingAccount) {}
    val us30UpdatedForGbpAccount: UpdatedCommandCreator = updatingAccount => new Us30UpdatedForGbpAccount(updatingAccount) {}
    val us30UpdatedForUsdAccount: UpdatedCommandCreator = updatingAccount => new Us30UpdatedForUsdAccount(updatingAccount) {}

    val eurAccountCurrencyMapping: Map[CurrencyPair, UpdatedCommandCreator] = Map(
      CurrencyPair.EurGbp -> eurGbpUpdatedForEurAccount,
      CurrencyPair.EurUsd -> eurUsdUpdatedForEurAccount,
      CurrencyPair.GbpUsd -> gbpUsdUpdatedForEurAccount,
      CurrencyPair.Spx500 -> spx500UpdatedForEurAccount,
      CurrencyPair.Us30 -> us30UpdatedForEurAccount
    )

    val gbpAccountCurrencyMapping: Map[CurrencyPair, UpdatedCommandCreator] = Map(
      CurrencyPair.EurGbp -> eurGbpUpdatedForGbpAccount,
      CurrencyPair.EurUsd -> eurUsdUpdatedForGbpAccount,
      CurrencyPair.GbpUsd -> gbpUsdUpdatedForGbpAccount,
      CurrencyPair.Spx500 -> spx500UpdatedForGbpAccount,
      CurrencyPair.Us30 -> us30UpdatedForGbpAccount
    )

    private val eurGbpCommandsByAccountCurrency = Map[AccountCurrency, ValidationParameters](
      Eur -> (EurGbp, Eur, eurGbpUpdatedForEurAccount),
      Gbp -> (EurGbp, Gbp, eurGbpUpdatedForGbpAccount),
      Usd -> (EurGbp, Usd, eurGbpUpdatedForUsdAccount)
    )
    private val eurUsdCommandsByAccountCurrency = Map[AccountCurrency, ValidationParameters](
      Eur -> (EurUsd, Eur, eurUsdUpdatedForEurAccount),
      Gbp -> (EurUsd, Gbp, eurUsdUpdatedForGbpAccount),
      Usd -> (EurUsd, Usd, eurUsdUpdatedForUsdAccount)
    )
    private val gbpUsdCommandsByAccountCurrency = Map[AccountCurrency, ValidationParameters](
      Eur -> (GbpUsd, Eur, gbpUsdUpdatedForEurAccount),
      Gbp -> (GbpUsd, Gbp, gbpUsdUpdatedForGbpAccount),
      Usd -> (GbpUsd, Usd, gbpUsdUpdatedForUsdAccount)
    )

    private val spx500CommandsByAccountCurrency = Map[AccountCurrency, ValidationParameters](
      Eur -> (Spx500, Eur, spx500UpdatedForEurAccount),
      Gbp -> (Spx500, Gbp, spx500UpdatedForGbpAccount),
      Usd -> (Spx500, Usd, spx500UpdatedForUsdAccount)
    )

    private val us30CommandsByAccountCurrency = Map[AccountCurrency, ValidationParameters](
      Eur -> (Us30, Eur, us30UpdatedForEurAccount),
      Gbp -> (Us30, Gbp, us30UpdatedForGbpAccount),
      Usd -> (Us30, Usd, us30UpdatedForUsdAccount)
    )

    val currencyPairToAccountCurrencyMap = Map[CurrencyPair, Map[AccountCurrency, ValidationParameters]](
      CurrencyPair.EurGbp -> eurGbpCommandsByAccountCurrency,
      CurrencyPair.EurUsd -> eurUsdCommandsByAccountCurrency,
      CurrencyPair.GbpUsd -> gbpUsdCommandsByAccountCurrency,
      CurrencyPair.Spx500 -> spx500CommandsByAccountCurrency,
      CurrencyPair.Us30 -> us30CommandsByAccountCurrency
    )

  }

  trait UpdatedCommandFactoryRepository extends Validation {

    type UpdatedCommandFactory = UpdatingAccount => ErrorSituationOr[UpdatedCommand]

    def by(currencyPair: CurrencyPair, accountCurrency: AccountCurrency): ErrorSituationOr[UpdatedCommandFactory]

  }

  object UpdatedCommandFactoryRepository {

    final case class UpdatedCommandNotFound(pair: CurrencyPair, currency: AccountCurrency) extends ErrorSituation

    final case class CurrencyPairNotFound(pair: CurrencyPair) extends ErrorSituation

    final case class UpdatedCommandFactoryRepositoryMismatchArguments(
        updatingAccount: UpdatingAccount,
        expCurrencyPair: CurrencyPair,
        expAccountCurrency: AccountCurrency
    ) extends ErrorSituation

  }

  object mapBasedUpdatedCommandFactory extends UpdatedCommandFactoryRepository {

    override def by(currencyPair: CurrencyPair, accountCurrency: AccountCurrency): ErrorSituationOr[UpdatedCommandFactory] = {

      def handleAccountCurrencyMapEntry
          : PartialFunction[Option[(CurrencyPair, AccountCurrency, UpdatingAccount => UpdatedCommand)], ErrorSituationOr[UpdatedCommandFactory]] = {
        case Some((expCurrencyPair, expAccountCurrency, updatedCommandBuilder)) =>
          Right(handleUpdatedCommandFactory(expCurrencyPair, expAccountCurrency, updatedCommandBuilder))
        case None =>
          Left(UpdatedCommandNotFound(currencyPair, accountCurrency))
      }

      def handleCurrencyPairMapEntry
          : PartialFunction[Option[Map[AccountCurrency, withoutValidation.ValidationParameters]], ErrorSituationOr[UpdatedCommandFactory]] = {
        case Some(builderFactoryByAccountCurrencyMap) => handleAccountCurrencyMapEntry(builderFactoryByAccountCurrencyMap.get(accountCurrency))
        case None                                     => Left(CurrencyPairNotFound(currencyPair))
      }

      handleCurrencyPairMapEntry(withoutValidation.currencyPairToAccountCurrencyMap.get(currencyPair))

    }

    private def handleUpdatedCommandFactory(
        expCurrencyPair: CurrencyPair,
        expAccountCurrency: AccountCurrency,
        updatedCommandFactory: UpdatingAccount => UpdatedCommand
    ): UpdatingAccount => Either[ErrorSituation, UpdatedCommand] = { updatingAccount: UpdatingAccount =>
      {
        validation(updatingAccount, expCurrencyPair, expAccountCurrency) match {
          case None               => Right(updatedCommandFactory(updatingAccount))
          case Some(errorMessage) => Left(errorMessage).withRight[UpdatedCommand]
        }
      }
    }

    def validation(updatingAccount: UpdatingAccount, expCurrencyPair: CurrencyPair, expAccountCurrency: AccountCurrency): Option[ErrorSituation] = {
      import org.binqua.forex.implicits.instances.accountCurrency._
      import org.binqua.forex.implicits.instances.currencyPair._

      Option.unless(
        Eq.eqv(updatingAccount.incomingUpdatedQuote.rates.currencyPair, expCurrencyPair) && Eq.eqv(updatingAccount.theAccountCurrency, expAccountCurrency)
      )(
        UpdatedCommandFactoryRepositoryMismatchArguments(updatingAccount, expCurrencyPair, expAccountCurrency)
      )
    }
  }

}
