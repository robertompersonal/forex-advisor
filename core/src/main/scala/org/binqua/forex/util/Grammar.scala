package org.binqua.forex.util

object Grammar {

  def in[A](a: A): A = a

  def of[A](a: A): A = a

}
