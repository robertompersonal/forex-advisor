package org.binqua.forex.util

import cats.data.Validated
import cats.syntax.either._

trait Validation {

  type ErrorMessage = String

  type ErrorOr[A] = Either[ErrorMessage, A]

  type ErrorsOr[A] = Either[List[ErrorMessage], A]

  type ErrorSituationOr[A] = Either[ErrorSituation, A]

  type ErrorSituationsOr[A] = Either[List[ErrorSituation], A]

  def toValidated[A](toBeValidated: ErrorOr[A]): Validated[List[ErrorMessage], A] = toListOfErrors(toBeValidated).toValidated

  def toListOfErrors[A](someValidation: ErrorOr[A]): ErrorsOr[A] = someValidation.leftMap((msg: String) => List(msg))

  implicit class ToListOfErrors[A](someValidation: ErrorOr[A]) {
    def toListOfErrors: ErrorsOr[A] = someValidation.leftMap((msg: String) => List(msg))
  }

}

object Validation extends Validation

trait ErrorSituation
