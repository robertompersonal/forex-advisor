package org.binqua.forex.util

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.syntax.either._
import com.typesafe.config.{Config => AkkaConfig}
import eu.timepit.refined.api.Refined
import org.binqua.forex.util.Validation.ErrorOr

object core {

  type Url = String Refined eu.timepit.refined.string.Url

  type ConfigValidator[A] = AkkaConfig => Validated[List[String], A]

  type PrefixedConfigValidator[A] = (AkkaConfig, String => String) => Validated[List[String], A]

  type ConfigKeyPrefixer = String => String

  type ConfigValidatorBuilder[A] = ConfigKeyPrefixer => ConfigValidator[A]

  type UnsafeConfigReader[A] = AkkaConfig => A

  implicit class MakeItUnsafe[A](validated: Validated[List[String], A]) {
    def unsafe: A = makeItUnsafe(validated)
  }

  implicit class MakeEitherUnsafe[A](either: Either[String, A]) {
    def unsafe: A = makeItUnsafe(either)
  }

  implicit class ABugIfNotRight[A](either: Either[String, A]) {
    def aBugIfNotRight: A = makeItUnsafe(either)
  }

  def makeItUnsafe[A](validated: Validated[List[String], A]): A =
    validated match {
      case Valid(validConfig) => validConfig
      case Invalid(errors)    => throw new IllegalArgumentException(separated(errors))
    }

  def makeItUnsafe[A](either: Either[String, A]): A = makeItUnsafe[A](either.leftMap(List(_)).toValidated)

  def makeItUnsafeF[A]: ErrorOr[A] => A = (either: ErrorOr[A]) => makeItUnsafe[A](either.leftMap(List(_)).toValidated)

  def separated(errors: List[String]): String = errors.mkString(" - ")

  def checkThat(ifTrueRight: Boolean, otherwiseLeft: => String): Either[String, Boolean] = Either.cond(ifTrueRight, true, otherwiseLeft)

  def show(product: Product): String = {
    val className = product.productPrefix
    val fieldNames = product.productElementNames.toList
    val fieldValues = product.productIterator.toList
    val fields = fieldNames.zip(fieldValues).map { case (name, value) => s"$name = $value" }
    fields.mkString(s"$className(", ", ", ")")
  }

}
