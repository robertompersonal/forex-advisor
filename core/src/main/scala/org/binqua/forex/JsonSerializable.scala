package org.binqua.forex

import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "className")
trait JsonSerializable extends Serializable
