const server = require('http').createServer();

const io = require('socket.io')(server, {
    pingInterval: 2000,
    pingTimeout: 1000,
    wsEngine: 'ws'
});

const firstArgIndex = 2

let port = process.argv[firstArgIndex];

let connectionAttempt = 0

io.use((socket, next) => {
    connectionAttempt++
    if (connectionAttempt >= 4) return next()
    else return next(new Error('Authentication error'));
});

io.of("/").on('connection', function (socket) {

    socket.on('requestDisconnect', function () {
        socket.disconnect();
    });

    socket.on('error', function () {
        console.log('error: ', arguments);
    });

});


function before(context, name, fn) {
    var method = context[name];
    context[name] = function () {
        fn.apply(this, arguments);
        return method.apply(this, arguments);
    };
}

before(io.engine, 'handleRequest', function (req, res) {
    var value = req.headers['x-socketio'];
    if (!value) return;
    res.setHeader('X-SocketIO', value);
});

before(io.engine, 'handleUpgrade', function (req, socket, head) {
    console.log("handleUpgrade actual.request.url=" + req.url);
    var value = req.headers['x-socketio'];
    if (!value) return;
    this.ws.once('headers', function (headers) {
        headers.push('X-SocketIO: ' + value);
    });
});


server.listen(port, function () {
    console.log('Socket.IO server listening on port', port);
});



