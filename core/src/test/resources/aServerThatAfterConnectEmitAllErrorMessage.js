var server = require('http').createServer();

var io = require('socket.io')(server, {
    pingInterval: 2000,
    pingTimeout: 100,
    wsEngine: 'ws'
});

const firstArgIndex = 2
let port = process.argv[firstArgIndex];

let currentSocket = null

io.of("/").on('connection', function (socket) {

    currentSocket = socket;

    socket.on('requestDisconnect', function () {
        socket.disconnect();
    });

    socket.on('error', function () {
        console.log('error: ', arguments);
    });

});

function before(context, name, fn) {
    var method = context[name];
    context[name] = function () {
        fn.apply(this, arguments);
        return method.apply(this, arguments);
    };
}

before(io.engine, 'handleRequest', function (req, res) {

    console.log("handleRequest actual.request.url=" + req.url);

    if (currentSocket && req.url === ("/socket.io/?access_token=theLoggingToken&EIO=3&transport=polling&sid=" + currentSocket.id)) {

        let emissionInterval = 100

        console.info("handleRequest url recognised as ", req.url, ". Start emitting in ", emissionInterval, " millis");

        setInterval(emitAllMessages, emissionInterval, currentSocket);

    }
    var value = req.headers['x-socketio'];
    if (!value) return;
    res.setHeader('X-SocketIO', value);
});

before(io.engine, 'handleUpgrade', function (req, socket, head) {
    console.log("handleUpgrade actual.request.url=" + req.url);
    var value = req.headers['x-socketio'];
    if (!value) return;
    this.ws.once('headers', function (headers) {
        headers.push('X-SocketIO: ' + value);
    });
});


server.listen(port, function () {
    console.log('Socket.IO server listening on port', port);
});

function emitAllMessages(socket) {
    ["connect_timeout", "reconnect", "reconnecting", "reconnect_attempt", "reconnect_failed"].forEach(m => {
        socket.emit(m, "1", "2")
        socket.emit(m, "")
        socket.emit(m)
    })
}


