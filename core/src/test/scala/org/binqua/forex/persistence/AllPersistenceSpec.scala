package org.binqua.forex.persistence

import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfoliosSpec
import org.binqua.forex.feed.reader.ReaderDontRunAlone
import org.binqua.forex.feed.socketio.connectionregistry.persistence.PersistenceDontRunAlone
import org.scalatest._

class AllPersistenceSpec extends Suites(new DeliveryGuaranteedPortfoliosSpec, new PersistenceDontRunAlone, new ReaderDontRunAlone)
