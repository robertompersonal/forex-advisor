package org.binqua.forex

import org.binqua.forex.running.common.CommonIoRunnerParameters
import org.scalatest.matchers.should.Matchers._
import org.scalatest.wordspec.AnyWordSpec

class CommonIoRunnerParametersSpec extends AnyWordSpec {

  "CommonIoRunnerParameterReader" should {

    "parses -configFileName=myapp p1 p2 parameters correctly" in {

      val runnerParameters = new CommonIoRunnerParameters(Array("-configFileName=myapp", "p1", "p2"))

      val actualConfigFileName = runnerParameters.configFileName
      actualConfigFileName.isRight shouldBe true
      actualConfigFileName.toOption shouldBe Some("myapp")

    }

    "gives output help for no configFileName parameter" in {

      val parserResult = new CommonIoRunnerParameters(Array("p1", "p2")).configFileName

      parserResult.isLeft shouldBe true

      parserResult.swap.toOption shouldBe Some("usage:  -configFileName <arg>\n")

    }

  }

}
