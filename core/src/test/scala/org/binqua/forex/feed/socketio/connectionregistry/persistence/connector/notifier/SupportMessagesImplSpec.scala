package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration._

class SupportMessagesImplSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  val underTest = SupportMessagesImpl

  val actorToBeNotified: ActorRef[SubscriptionStarterProtocol.NewSocketId] = createTestProbe("test").ref

  val socketId = SocketId("1")

  "actorRequestedNewSocketIdNotification" should "be implemented" in {
    underTest.actorRequestedNewSocketIdNotification(
      socketId,
      actorToBeNotify = actorToBeNotified
    ) shouldBe s"Actor with name ${actorToBeNotified.path.name} (${actorToBeNotified.path}) requested to be notify of new socketId $socketId"
  }

  "newSocketIdAvailable" should "be implemented" in {
    underTest.newSocketIdAvailable(socketId) shouldBe s"New socketId $socketId available"
  }

  "newSocketIdNotified" should "be implemented" in {
    underTest.newSocketIdNotified(socketId) shouldBe s"New socketId $socketId has been notified. Subscription maybe started"
  }

  "startSubscriptionAttemptFailedRetry" should "be implemented" in {
    underTest.startSubscriptionAttemptFailedRetry(
      socketId,
      actorToBeNotified,
      attempts = 10,
      timeout = 1.second
    ) shouldBe s"Notification of new socketId $socketId failed. No ack from actor ${actorToBeNotified.path.name} (${actorToBeNotified.path}) before ${1.second}. This is attempt 10"
  }

  "startSubscriptionAttemptWithOldSocketIdAborted" should "be implemented" in {
    val oldSocketId = SocketId("oldSocketId")
    underTest.startSubscriptionAttemptWithOldSocketIdAborted(
      newSocketId = socketId,
      oldSocketId = oldSocketId
    ) shouldBe s"Notification of new socketId $oldSocketId failed, but meanwhile a new socketId is available $socketId"
  }

}
