package org.binqua.forex.feed.httpclient

import akka.http.scaladsl.model.StatusCodes
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.scalatest.flatspec.AnyFlatSpec
import play.api.libs.json.JsError

class ErrorSituationHandlerImplSpec extends AnyFlatSpec {

  private val errorSituationHandler = ErrorSituationHandlerImpl
  "HttpSubscriptionFailedErrorSituation" should "create right string" in {
    assertResult(
      "Received a response from server but parser failed:\nbody=aBody currencyPair=Us30 socketId=SocketId(123) jsError=JsError(List((,List(JsonValidationError(List(some json error),ArraySeq())))))"
    )(errorSituationHandler.show(HttpSubscriptionFailedErrorSituation("aBody", CurrencyPair.Us30, JsError("some json error"), SocketId("123"))))
  }

  "HttpSubscriptionWrongHttpCodeErrorSituation" should "create right string" in {
    assertResult("Received a response from server but error code is not 200:\nstatusCode=404 Not Found currencyPair=EurJpy socketId=SocketId(456)")(
      errorSituationHandler.show(HttpSubscriptionWrongHttpCodeErrorSituation(StatusCodes.NotFound, CurrencyPair.EurJpy, SocketId("456")))
    )
  }

}
