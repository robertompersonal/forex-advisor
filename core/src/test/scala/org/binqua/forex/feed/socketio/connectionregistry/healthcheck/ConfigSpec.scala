package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import org.binqua.forex.util.TestingFacilities
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration._

class ConfigSpec extends AnyFlatSpec with TestingFacilities {

  "given all valid parameters, unsafe" should "return a correct Config" in {
    val cassandraHost = "localhost"
    val cassandraPort = 123
    val localDataCenter = "dataCenter"
    val retryDelay = 11.seconds

    val actualConfig: Config = Config.validated(cassandraHost, cassandraPort, localDataCenter, retryDelay).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.host should be(cassandraHost)
    actualConfig.port should be(cassandraPort)
    actualConfig.localDataCenter should be(localDataCenter)
    actualConfig.retryDelay should be(retryDelay)
  }

  "given all invalid parameters, unsafe" should "throw illegalArgumentException" in {
    val errors = Config
      .validated(
        cassandraHost = "",
        cassandraPort = -1,
        localDataCenter = "",
        retryDelay = 11.seconds
      )
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)
    errors should be(List("cassandra host has to be not empty", "cassandra port has to be not empty", "cassandra dataCenter has to be not empty"))
  }

}
