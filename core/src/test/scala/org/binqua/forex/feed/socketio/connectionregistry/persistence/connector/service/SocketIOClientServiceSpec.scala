package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, TestProbe}
import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.receptionist.Receptionist.{Listing, Registered}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.BaseManualTimeActorTestKit
import org.binqua.forex.util.ChildMaker.{CHILD_MAKER, CHILD_MAKER_FACTORY}

class SocketIOClientServiceSpec extends BaseManualTimeActorTestKit {

  "connect" should "spawn a SocketIOClient, connect it and register it with the receptionist" in
    new TestContext(socketIOClientContextMaker = SocketIOClientContext.thatIgnoreAnyMessage(), supportMessages = SupportMessagesContext.justForAnIdeaSupport) {

      matchDistinctLogs(messages = SupportMessagesContext.socketIOClientServiceRegisteredFormatter(socketIOClientContext.childNameByIndex(childIndex = 1)))
        .expect(
          underTest ! Connect(underTestProbe.ref)
        )

      matchDistinctLogs(messages = supportMessages.connecting).expect(
        fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
          val SocketIOClientProtocol.Connect(replyTo) = toBeMatched
          replyTo ! SocketIOClientProtocol.Connecting
          replyTo ! SocketIOClientProtocol.Connected(socketId)
        })
      )

      underTestProbe.expectMessage(NewSocketId(socketId))

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientServiceSubscriber)(toBeMatched => {
        val SocketIOClientServiceModule.SocketIOClientKey.Listing(listings) = toBeMatched
        listings.foreach(ps => ps == socketIOClientContext.lastCreatedChild())
      })

      socketIOClientContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

    }

  "when a connected child fails, the actor" should "spawn a new SocketIOClient, connect it and register it with the receptionist" in
    new TestContext(
      socketIOClientContextMaker = SocketIOClientContext.thatFailWithASpecialMessage(),
      supportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! Connect(underTestProbe.ref)

      socketIOClientContext.probe.expectMessageType[SocketIOClientProtocol.Connect]

      val iAmGoingToFailSoonChild: ActorRef[SocketIOClientProtocol.Command] = socketIOClientContext.lastCreatedChild()

      iAmGoingToFailSoonChild ! SocketIOClientContext.aDummyMessageThatMakeTheChildFail

      socketIOClientContext.probe.expectMessageType[SocketIOClientProtocol.Connect]

      waitALittleBit(soThat("a new child can be created"))

      socketIOClientContext.probe.expectTerminated(iAmGoingToFailSoonChild)

      matchDistinctLogs(messages = supportMessages.connecting).expect(
        fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
          val SocketIOClientProtocol.Connect(replyTo) = toBeMatched
          replyTo ! SocketIOClientProtocol.Connecting
          replyTo ! SocketIOClientProtocol.Connected(socketId)
        })
      )

      underTestProbe.expectMessage(NewSocketId(socketId))

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientServiceSubscriber)(toBeMatched => {
        val SocketIOClientServiceModule.SocketIOClientKey.Listing(listings) = toBeMatched
        listings.foreach(ps => ps == socketIOClientContext.lastCreatedChild())
      })

      socketIOClientContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

    }

  "Once Connecting has been received, an extra Connecting" should "be unhandled" in
    new TestContext(
      socketIOClientContextMaker = SocketIOClientContext.thatFailWithASpecialMessage(),
      supportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! Connect(underTestProbe.ref)

      val connecting = supportMessages.connecting
      LoggingTestKit
        .custom(event =>
          event.message match {
            case `connecting` => true
          }
        )
        .withOccurrences(newOccurrences = 1)
        .expect(
          fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
            val SocketIOClientProtocol.Connect(replyTo) = toBeMatched
            replyTo ! SocketIOClientProtocol.Connecting
            replyTo ! SocketIOClientProtocol.Connecting
            replyTo ! SocketIOClientProtocol.Connecting
            replyTo ! SocketIOClientProtocol.Connected(socketId)
          })
        )

      underTestProbe.expectMessage(NewSocketId(socketId))

    }

  "Once the service has been registered, an extra registration response" should "be unhandled" in
    new TestContext(socketIOClientContextMaker = SocketIOClientContext.thatIgnoreAnyMessage(), supportMessages = SupportMessagesContext.justForAnIdeaSupport) {

      matchDistinctLogs(messages = SupportMessagesContext.socketIOClientServiceRegisteredFormatter(socketIOClientContext.childNameByIndex(childIndex = 1)))
        .expect(
          underTest ! Connect(underTestProbe.ref)
        )

      private val unhandled = WrappedReceptionistRegistered(Registered(SocketIOClientServiceModule.SocketIOClientKey, null))
      matchDistinctLogs(messages =
        commandIgnoredMessage(unhandled)(TheOnlyHandledMessages(SocketIOClientProtocol.Connecting, SocketIOClientProtocol.Connected))
      ).expect(
        underTest ! unhandled
      )

      matchDistinctLogs(messages = supportMessages.connecting).expect(
        underTest ! WrappedSocketIOClientResponse(SocketIOClientProtocol.Connecting)
      )

      val newSocketId = SocketId(randomString)
      underTest ! WrappedSocketIOClientResponse(SocketIOClientProtocol.Connected(newSocketId))
      underTestProbe.expectMessage(NewSocketId(newSocketId))

      matchDistinctLogs(messages = commandIgnoredMessage(unhandled)(TheOnlyHandledMessages(SocketIOClientProtocol.Connected))).expect(
        underTest ! unhandled
      )
    }

  "Once connect has been received, an extra connect" should "be unhandled" in
    new TestContext(
      socketIOClientContextMaker = SocketIOClientContext.thatFailWithASpecialMessage(),
      supportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! Connect(underTestProbe.ref)

      List(
        Connect(underTestProbe.ref),
        Connect(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(SocketIOClientProtocol.Connecting, SocketIOClientProtocol.Connected)))

    }

  "The first message" should "be connect" in
    new TestContext(socketIOClientContextMaker = SocketIOClientContext.thatIgnoreAnyMessage(), supportMessages = SupportMessagesContext.justForAnIdeaSupport) {

      List(
        WrappedSocketIOClientResponse(null),
        WrappedReceptionistRegistered(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Connect)))

    }

  object SocketIOClientContext {

    val aDummyMessageThatMakeTheChildFail: SocketIOClientProtocol.Connect = SocketIOClientProtocol.Connect(createTestProbe().ref)

    val actorPrefixName = "socketIOClient"

    type theType = NewActorCollaboratorTestContext[Message, SocketIOClientProtocol.Command]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = actorPrefixName

      override val probe: TestProbe[SocketIOClientProtocol.Command] = createTestProbe()

      override def behavior: Behavior[SocketIOClientProtocol.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, SocketIOClientProtocol.Command] =
        throw new IllegalAccessException("Not used in this case")

      override def childMakerFactory(): CHILD_MAKER_FACTORY[Message, SocketIOClientProtocol.Command] =
        smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

      override def ref: ActorRef[SocketIOClientProtocol.Command] = throw new IllegalAccessException("Not used in this case")

    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SocketIOClientProtocol.Command] = Behaviors.ignore
      }
    }

    def thatFailWithASpecialMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SocketIOClientProtocol.Command] =
          Behaviors.receiveMessage({ m =>
            if (m == aDummyMessageThatMakeTheChildFail)
              throwIAmGoingToFailedException(actorPrefixName, m)
            else
              Behaviors.same
          })
      }
    }

  }

  object SupportMessagesContext {

    val justForAnIdeaSupport: SupportMessages = new SupportMessages {

      override def connecting: String = "Connecting"

      override def socketIOClientServiceRegistered(actor: ActorRef[SocketIOClientProtocol.Command]): String =
        socketIOClientServiceRegisteredFormatter(actor.path.name)
    }

    def socketIOClientServiceRegisteredFormatter(actor: String) = s"socketIOClientServiceRegistered $actor"

  }

  case class TestContext(socketIOClientContextMaker: SmartDeadWatcher => SocketIOClientContext.theType, supportMessages: SupportMessages)(implicit
      underTestNaming: UnderTestNaming
  ) extends ManualTimeBaseTestContext(underTestNaming) {

    val socketId = SocketId("123")

    val socketIOClientContext: SocketIOClientContext.theType = socketIOClientContextMaker(actorsWatchers)

    val underTestProbe = createTestProbe[NewSocketId]()

    private val childMaker: CHILD_MAKER[Message, SocketIOClientProtocol.Command] = socketIOClientContext.childMakerFactory()()

    val underTest = spawn(SocketIOClientService(socketIOClientMaker = childMaker, supportMessages), underTestNaming.next())

    val socketIOClientServiceSubscriber: TestProbe[Listing] = createTestProbe()

    testKit.system.receptionist ! Receptionist.Subscribe(SocketIOClientServiceModule.SocketIOClientKey, socketIOClientServiceSubscriber.ref)

  }

}
