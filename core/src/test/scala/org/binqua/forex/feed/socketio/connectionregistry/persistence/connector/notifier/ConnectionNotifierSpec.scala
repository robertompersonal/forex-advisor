package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier

import akka.NotUsed
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.Moments._
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration.FiniteDuration

class ConnectionNotifierSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "given the actor receives Notify and then SocketIOClientProtocol.Connected, it" should "send SlimFeedSubscriptionStarterProtocol.StartSubscription and log the response" in
    new TestContext(FeedSubscriptionStarterTestContext.thatIgnoreAnyMessage(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      underTest ! Notify(feedSubscriptionStarter.probe.ref)

      feedSubscriptionStarter.probe.expectNoMessage()

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      matchDistinctLogs(messages = justForAnIdea.newSocketIdNotified(socketId)).expect {
        fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
          val SubscriptionStarterProtocol.NewSocketId(replyTo, socketId) = toBeMatched
          replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(socketId)
        })
      }
    }

  "given the actor received Notify, when a new socket Id is available then it" should "send SlimFeedSubscriptionStarterProtocol.StartSubscription and log the messages" in
    new TestContext(FeedSubscriptionStarterTestContext.thatIgnoreAnyMessage(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      underTest ! Notify(feedSubscriptionStarter.probe.ref)

      feedSubscriptionStarter.probe.expectNoMessage()

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      matchDistinctLogs(messages = justForAnIdea.newSocketIdNotified(socketId)).expect {
        fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
          val SubscriptionStarterProtocol.NewSocketId(replyTo, socketId) = toBeMatched
          replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(socketId)
        })
      }
    }

  "given the actor receives SocketIOClientProtocol.Connected and then Notify, it" should "send SlimFeedSubscriptionStarterProtocol.StartSubscription. If the subscription did not start the actor keeps retrying" in
    new TestContext(FeedSubscriptionStarterTestContext.thatReplyAfter3FailedAttempts(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(retryStartSubscriptionInterval.duration)

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      private val notification = Notify(subscriptionStarterRef = feedSubscriptionStarter.ref)

      underTest ! notification

      feedSubscriptionStarter.probe.expectMessageType[SubscriptionStarterProtocol.NewSocketId]

      private val str: String =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 1, retryStartSubscriptionInterval.duration)
      println(str)
      matchDistinctLogs(messages = str).expect {
        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)
        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)
      }

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 2, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)
        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)
      }

      matchDistinctLogs(
        messages = justForAnIdea
          .startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 3, retryStartSubscriptionInterval.duration),
        justForAnIdea.newSocketIdNotified(socketId)
      ).expect {
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
        manualClock.timeAdvances(JustAfterThirdIntervalHasExpired)
      }

    }

  "given the actor receives Notify and then SocketIOClientProtocol.Connected, it" should "send SlimFeedSubscriptionStarterProtocol.StartSubscription. If the subscription did not start the actor keeps retrying" in
    new TestContext(FeedSubscriptionStarterTestContext.thatReplyAfter3FailedAttempts(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(retryStartSubscriptionInterval.duration)

      private val notification = Notify(subscriptionStarterRef = feedSubscriptionStarter.ref)

      underTest ! notification

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      feedSubscriptionStarter.probe.expectMessageType[SubscriptionStarterProtocol.NewSocketId]

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 1, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)
        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)
      }

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 2, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)
        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)
      }

      matchDistinctLogs(
        messages = justForAnIdea
          .startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 3, retryStartSubscriptionInterval.duration),
        justForAnIdea.newSocketIdNotified(socketId)
      ).expect {
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
        manualClock.timeAdvances(JustAfterThirdIntervalHasExpired)
      }

    }

  "Given a series of retry and success trigger by a new socketId, the counter attempt" should "be reset" in
    new TestContext(FeedSubscriptionStarterTestContext.thatReplyAfter2FailedAttemptsThanReset(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(retryStartSubscriptionInterval.duration)

      private val notification = Notify(subscriptionStarterRef = feedSubscriptionStarter.ref)
      underTest ! notification

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)
      manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, 2, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)
        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)
      }

      manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
      manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

      private val newSocketID = SocketId("newSocketId")

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(newSocketID)).expect {
        underTest ! NewSocketIdAvailable(newSocketID)
      }

      matchDistinctLogs(messages =
        justForAnIdea
          .startSubscriptionAttemptFailedRetry(newSocketID, notification.subscriptionStarterRef, attempts = 1, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
      }

    }

  "Given a series of retry and success trigger by a new actor to be notify, the counter attempt" should "be reset" in
    new TestContext(FeedSubscriptionStarterTestContext.thatReplyAfter2FailedAttemptsThanReset(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(retryStartSubscriptionInterval.duration)

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      private val notification = Notify(subscriptionStarterRef = feedSubscriptionStarter.ref)
      underTest ! notification

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)
      manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, 2, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)
        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)
      }

      manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
      manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

      val newNotification = Notify(createTestProbe[SubscriptionStarterProtocol.NewSocketId]().ref)

      matchDistinctLogs(messages = justForAnIdea.actorRequestedNewSocketIdNotification(socketId, newNotification.subscriptionStarterRef)).expect {
        underTest ! newNotification
      }

      matchDistinctLogs(messages =
        justForAnIdea
          .startSubscriptionAttemptFailedRetry(socketId, newNotification.subscriptionStarterRef, attempts = 1, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)
      }

    }

  "given the actor receives a new SocketIOClientProtocol.Connected(newSocketId) during a retry with a previous socketId, it" should "stop retrying with the old socketId" in
    new TestContext(FeedSubscriptionStarterTestContext.thatIgnoreAnyMessage(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(retryStartSubscriptionInterval.duration)

      private val notification = Notify(subscriptionStarterRef = feedSubscriptionStarter.ref)

      underTest ! notification

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(socketId)).expect {
        underTest ! NewSocketIdAvailable(socketId)
      }

      fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeFished => {
        val SubscriptionStarterProtocol.NewSocketId(_, `socketId`) = toBeFished
      })

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 1, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
      }

      fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
        val SubscriptionStarterProtocol.NewSocketId(_, `socketId`) = toBeMatched
      })

      matchDistinctLogs(messages =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 2, retryStartSubscriptionInterval.duration)
      ).expect {
        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)
      }

      fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
        val SubscriptionStarterProtocol.NewSocketId(_, `socketId`) = toBeMatched
      })

      private val newSocketId = SocketId("newSocketId")

      matchDistinctLogs(messages = justForAnIdea.newSocketIdAvailable(newSocketId)).expect {
        underTest ! NewSocketIdAvailable(newSocketId)
      }

      matchDistinctLogs(messages = justForAnIdea.newSocketIdNotified(newSocketId)).expect {
        fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
          val SubscriptionStarterProtocol.NewSocketId(replyTo, `newSocketId`) = toBeMatched
          replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(newSocketId)
        })
      }

      val attempt3LogWithOldSocketId =
        justForAnIdea.startSubscriptionAttemptFailedRetry(socketId, notification.subscriptionStarterRef, attempts = 3, retryStartSubscriptionInterval.duration)

      matchDistinctLogs(messages = justForAnIdea.startSubscriptionAttemptWithOldSocketIdAborted(newSocketId, socketId)).expect {
        LoggingTestKit
          .custom(event =>
            event.message match {
              case `attempt3LogWithOldSocketId` => true
            }
          )
          .withOccurrences(newOccurrences = 0)
          .expect {
            manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)
          }
      }

      underTest ! Notify(feedSubscriptionStarter.ref)

      fishExactlyOneMessageAndIgnoreOthers(feedSubscriptionStarter.probe)(toBeMatched => {
        val SubscriptionStarterProtocol.NewSocketId(replyTo, `newSocketId`) = toBeMatched
        replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(newSocketId)
      })

    }

  "non fully initialised actor" should "treat PrivateSubscriptionStarted and RetryToStartASubscriptionFailedDueToTimeout as unhandled" in
    new TestContext(FeedSubscriptionStarterTestContext.thatIgnoreAnyMessage(), SupportMessagesTestContext.justForAnIdea, underTestNaming) {

      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Notify, NewSocketIdAvailable)

      {
        val unhandled = ConnectionNotifierProtocol.PrivateSubscriptionStarted(socketId)
        LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
          underTest ! unhandled
        }
      }

      {
        val unhandled = RetryToStartASubscriptionFailedDueToTimeout(socketId, feedSubscriptionStarter.ref)
        LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
          underTest ! unhandled
        }
      }

    }

  object SupportMessagesTestContext {

    val justForAnIdea: SupportMessages = new SupportMessages {

      override def newSocketIdAvailable(socketId: SocketId): String = s"newConnectionAvailable $socketId"

      override def newSocketIdNotified(socketId: SocketId): String = s"newConnectionNotified $socketId"

      override def startSubscriptionAttemptFailedRetry(
          socketId: SocketId,
          actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId],
          attempts: Int,
          timeout: FiniteDuration
      ): String = s"attempt $attempts failed to notify ${actorToBeNotify.path.name}. Retry now! $socketId"

      override def startSubscriptionAttemptWithOldSocketIdAborted(newSocketId: SocketId, oldSocketId: SocketId): String =
        s"startSubscriptionAttemptWithOldSocketIdAborted new $newSocketId old $oldSocketId"

      override def actorRequestedNewSocketIdNotification(socketId: SocketId, actorToBeNotify: ActorRef[SubscriptionStarterProtocol.NewSocketId]): String =
        s"newActorRequestedNotification $socketId $actorToBeNotify"
    }
  }

  object FeedSubscriptionStarterTestContext {

    val actorPrefixName = "feedSubscriptionStarter"

    type FeedSubscriptionStarterType = ActorCollaboratorTestContext[NotUsed, SubscriptionStarterProtocol.NewSocketId]

    trait Base extends FeedSubscriptionStarterType {

      override val name: String = FeedSubscriptionStarterTestContext.actorPrefixName

      override val probe: TestProbe[SubscriptionStarterProtocol.NewSocketId] = createTestProbe()

      override def behavior: Behavior[SubscriptionStarterProtocol.NewSocketId]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[NotUsed, SubscriptionStarterProtocol.NewSocketId] =
        throw new IllegalAccessException("Not used in this case")

      override def ref: ActorRef[SubscriptionStarterProtocol.NewSocketId] = spawn(Behaviors.monitor(probe.ref, behavior), s"$actorPrefixName-$randomString")
    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[SubscriptionStarterProtocol.NewSocketId] = Behaviors.ignore

    }

    case class thatReplyAfter3FailedAttempts() extends Base {

      override def behavior: Behavior[SubscriptionStarterProtocol.NewSocketId] =
        Behaviors.setup(_ => {
          var counter = 0
          Behaviors.receiveMessage({
            case SubscriptionStarterProtocol.NewSocketId(replyTo, socketId) =>
              counter += 1
              counter match {
                case 1 | 2 | 3 => Behaviors.same
                case 4 =>
                  replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(socketId)
                  Behaviors.stopped
                case 5 => throw new RuntimeException("This should not happen")
              }
            case _ =>
              throw new RuntimeException("This should not happen")
          })
        })

    }

    case class thatReplyAfter2FailedAttemptsThanReset() extends Base {

      override def behavior: Behavior[SubscriptionStarterProtocol.NewSocketId] =
        Behaviors.setup(_ => {
          var counter = 0
          Behaviors.receiveMessage({
            case SubscriptionStarterProtocol.NewSocketId(replyTo, socketId) =>
              counter += 1
              counter match {
                case 1 | 2 => Behaviors.same
                case 3 =>
                  replyTo ! SubscriptionStarterProtocol.NewFeedSocketIdUpdated(socketId)
                  counter = 0
                  Behaviors.same
                case 4 => throw new RuntimeException("This should not happen")
              }
            case _ =>
              throw new RuntimeException("This should not happen")
          })
        })

    }

  }

  case class TestContext(
      feedSubscriptionStarter: FeedSubscriptionStarterTestContext.FeedSubscriptionStarterType,
      justForAnIdea: SupportMessages,
      underTestNaming: UnderTestNaming
  ) extends ManualTimeBaseTestContext(underTestNaming) {

    val socketId = SocketId("123")

    import scala.concurrent.duration._

    val retryStartSubscriptionInterval: Timeout = 1.second

    val underTest = spawn(ConnectionNotifier(retryStartSubscriptionInterval, justForAnIdea))

  }

}
