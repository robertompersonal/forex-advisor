package org.binqua.forex.feed.starter

import akka.NotUsed
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol._
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.Moments.{JumpAfterFirstIntervalHasExpired, JumpAfterSecondIntervalHasExpired, JumpAfterThirdIntervalHasExpired}
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._

class SubscriptionStarterSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  val socketId = SocketId("theLastOne")

  "Start" should "commence a subscription. Once Registered the registration is complete" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      matchDistinctLogs(
        messages = justForAnIdea.subscriptionStarted(),
        justForAnIdea.registrationStarted(registrationAttempt = 1),
        justForAnIdea.registrationCompleted()
      ).expect {

        underTest ! StartASubscription

        fishOnlyOneMessage(persistenceConnectionRegistryTestContext.probe)((theBeMatched: Any) => {
          val connectionregistry.Manager.Register(registrationRequester, _, "FeedSubscriptionStarter") = theBeMatched
          registrationRequester ! Persistence.Registered
        })
      }

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Start" should "be the only handled message of a newly spawned actor" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      List(
        RegistrationComplete,
        RetryRegistration,
        SubscriptionStarterProtocol.NewSocketId(null, null),
        WrappedSubscriptionsControllerResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(StartASubscription)))

    }

  "Given a Registration started, and given does not complete before timeout, when the timeout occurs then the actor" should "retry to register" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatReplyAfter3Attempts(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val registrationTimeout = 500.millis

      val manualClock = manualClockBuilder.withInterval(registrationTimeout)

      matchDistinctLogs(
        messages = justForAnIdea.registrationTimeout(attempt = 1, registrationTimeout = registrationTimeout),
        justForAnIdea.registrationTimeout(attempt = 2, registrationTimeout = registrationTimeout),
        justForAnIdea.registrationTimeout(attempt = 3, registrationTimeout = registrationTimeout)
      ).expect {

        val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

        underTest ! StartASubscription

        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)

        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)

      }

      unhandledMessageSubscriber.expectNoMessage()
    }

  "Given the actor received a NewFeedSocketId during registration, the actor" should "start a subscription with the most recent socketId when the registration is completed" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatReplyAfter3Attempts(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val registrationTimeout = 500.millis

      val manualClock = manualClockBuilder.withInterval(registrationTimeout)

      val anotherSocketId: SocketId = SocketId("anotherSocketId")

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      matchDistinctLogs(
        messages = justForAnIdea.registrationStarted(registrationAttempt = 1),
        justForAnIdea.registrationTimeout(attempt = 1, registrationTimeout = registrationTimeout),
        justForAnIdea.registrationStarted(registrationAttempt = 2)
      ).expect {
        underTest ! StartASubscription
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
      }

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      matchDistinctLogs(
        messages = justForAnIdea.registrationTimeout(attempt = 2, registrationTimeout = registrationTimeout),
        justForAnIdea.registrationStarted(registrationAttempt = 3)
      ).expect {
        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)
      }

      underTest ! NewSocketId(theActorThatGenerateSocketId, anotherSocketId)

      matchDistinctLogs(
        messages = justForAnIdea.registrationTimeout(attempt = 3, registrationTimeout = registrationTimeout),
        justForAnIdea.registrationStarted(registrationAttempt = 4)
      ).expect {
        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)
      }

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `anotherSocketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(anotherSocketId)
      })

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor completes a registration, it" should "start a subscription execution when it receives NewFeedSocketId. No NewFeedSocketId no subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val registrationTimeout = 500.millis

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      underTest ! StartASubscription

      subscriptionExecutorTestContext.probe.expectNoMessage()

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `socketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor completes a registration, and start a subscription, it" should "accept only NewFeedSocketId and ExecutionStarted" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val registrationTimeout = 500.millis

      val underTest = spawnInstanceUnderTest.withRegistrationTimeout(registrationTimeout)

      underTest ! StartASubscription

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionStarted(socketId))))

    }

  "Once a registration is running but not completed, the actor" should "accept only NewFeedSocketId, RegistrationComplete, RetryRegistration" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatIgnoreAnyMessage(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(persistenceConnectionRegistryTestContext.probe)((theBeMatched: Any) => {
        val connectionregistry.Manager.Register(_, _, "FeedSubscriptionStarter") = theBeMatched
      })

      List(
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, RegistrationComplete, RetryRegistration)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once a registration is completed, the actor" should "accept only NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once a subscription execution is started, the actor" should "accept only ExecutionFailed or NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)
      underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(socketId))

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(SocketId("this is wrong"))),
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(SocketId("this is wrong")))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId, SubscriptionControllerProtocol.ExecutionFailed(socketId))))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given the actor sent SubscriptionExecutorProtocol.Start but did not received Started yet, when the actor receives a newSocketId, it" should "stop the running child and create a new execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(_, `socketId`) = toBeMatched
      })

      val thisChildIsGoingToBeStopped = lastMonitoredChildSpawn()

      {
        val newSocketId: SocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
          val SubscriptionControllerProtocol.Start(replyToWhenDone, `newSocketId`) = toBeMatched
          replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })
      }

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 2)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(thisChildIsGoingToBeStopped)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution has started and the actor receives a NewFeedSocketId then the actor" should "stop the running child and create a new one" in
    new TestContext(
      SubscriptionExecutorTestContext.thatIgnoreAnyMessage(),
      ConnectionRegistryTestContext.thatAlwaysRegister(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("registration can complete"))

      underTest ! NewSocketId(theActorThatGenerateSocketId, socketId)

      fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
        val SubscriptionControllerProtocol.Start(replyToWhenDone, `socketId`) = toBeMatched
        replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      val thisChildIsGoingToBeStopped = lastMonitoredChildSpawn()

      {
        val newSocketId: SocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishExactlyOneMessageAndIgnoreOthers(subscriptionExecutorTestContext.probe)(toBeMatched => {
          val SubscriptionControllerProtocol.Start(replyToWhenDone, `newSocketId`) = toBeMatched
          replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })
      }

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 2)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(thisChildIsGoingToBeStopped)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Once an execution is completed, the actor" should "accept only NewFeedSocketId" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("subscription can complete"))

      List(
        RegistrationComplete,
        RetryRegistration,
        StartASubscription,
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionStarted(null)),
        WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(null))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NewSocketId)))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution completes, the actor" should "have no more alive child" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 1)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())

      unhandledMessageSubscriber.expectNoMessage()

    }

  "An actor" should "complete subscriptions one after another" in
    new TestContext(
      SubscriptionExecutorTestContext.thatStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 1)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())

      {
        val newSocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
          val SubscriptionControllerProtocol.Start(replyTo, `newSocketId`) = theBeMatched
          replyTo ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })

        assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 2)(because("for each new socketId a child is created"))

        subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())
      }

      {
        val newSocketId = SocketId(randomString)
        underTest ! NewSocketId(theActorThatGenerateSocketId, newSocketId)

        fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
          val SubscriptionControllerProtocol.Start(replyTo, `newSocketId`) = theBeMatched
          replyTo ! SubscriptionControllerProtocol.ExecutionStarted(newSocketId)
        })

        assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 3)(because("for each new socketId a child is created"))

        subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())
      }

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution fails, the actor" should "stop the child that executed it and start a new one with same socketId. Execution failed are logged" in
    new TestContext(
      SubscriptionExecutorTestContext.thatDoesNotCompleteTheSubscriptions2Times(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 1)(because("for each new socketId a child is created"))

      matchDistinctLogs(messages = justForAnIdea.subscriptionExecutionFailed(socketId)).expect({
        val lastChild = lastMonitoredChildSpawn()
        underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(socketId))
        subscriptionExecutorTestContext.probe.expectTerminated(lastChild)
      })

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 2)(because("for each new socketId a child is created"))

      matchDistinctLogs(messages = justForAnIdea.subscriptionExecutionFailed(socketId)).expect({
        val lastChild = lastMonitoredChildSpawn()
        underTest ! WrappedSubscriptionsControllerResponse(SubscriptionControllerProtocol.ExecutionFailed(socketId))
        subscriptionExecutorTestContext.probe.expectTerminated(lastChild)
      })

      fishOnlyOneMessage(subscriptionExecutorTestContext.probe)((theBeMatched: Any) => {
        val SubscriptionControllerProtocol.Start(replyTo, `socketId`) = theBeMatched
        replyTo ! SubscriptionControllerProtocol.ExecutionStarted(socketId)
      })

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 3)(because("for each new socketId a child is created"))

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given an actor is waiting for SubscriptionExecutorProtocol.ExecutionStarted and the child fails, then the actor" should "start a new subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatCrash2TimesBeforeStartAndCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! StartASubscription

      waitALittleBit(soThat("failed children can be recreated"))

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 3)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())

      unhandledMessageSubscriber.expectNoMessage()

    }

  "Given a subscription execution is running and the child fails, then the actor" should "start a new subscription execution" in
    new TestContext(
      SubscriptionExecutorTestContext.thatSendStartAndThenCrash2TimesBeforeCompleteASubscription(),
      ConnectionRegistryTestContext.thatAlwaysRegisteredAndSendASocketId(),
      SupportMessagesTestContext.justForAnIdea,
      underTestNaming
    ) {

      val subscriptionExecutorFailed: String = justForAnIdea.subscriptionExecutorFailed(socketId)
      val subscriptionExecutionCompleted: String = justForAnIdea.subscriptionExecutionCompleted(socketId)

      LoggingTestKit
        .custom(event =>
          event.message match {
            case `subscriptionExecutorFailed`     => true
            case `subscriptionExecutionCompleted` => true
          }
        )
        .withOccurrences(newOccurrences = 2 + 1)
        .expect {

          val underTest = spawnInstanceUnderTest.withDefaultParameters()

          underTest ! StartASubscription

        }

      assertNumberOfActorMadeWithNamePrefixedBy(subscriptionExecutorTestContext.name)(_ shouldBe 3)(because("for each new socketId a child is created"))

      subscriptionExecutorTestContext.probe.expectTerminated(lastMonitoredChildSpawn())

      unhandledMessageSubscriber.expectNoMessage()
    }

  object ConnectionRegistryTestContext {

    val persistenceConnectionRegistry = "persistenceConnectionRegistry"
    val alias = "FeedSubscriptionStarter"

    trait Base extends ActorCollaboratorTestContext[NotUsed, connectionregistry.Manager.Command] {

      val name = ConnectionRegistryTestContext.persistenceConnectionRegistry

      val probe: TestProbe[connectionregistry.Manager.Command] = createTestProbe()

      def behavior: Behavior[connectionregistry.Manager.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[NotUsed, connectionregistry.Manager.Command] =
        throw new IllegalAccessError("Not used in this case")

      override def ref: ActorRef[connectionregistry.Manager.Command] = spawn(Behaviors.monitor(probe.ref, behavior))

    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[connectionregistry.Manager.Command] = Behaviors.ignore

    }

    case class thatAlwaysRegister() extends Base {

      def behavior: Behavior[connectionregistry.Manager.Command] =
        Behaviors.setup(context => {
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, _, `alias`) =>
              replyTo ! Persistence.Registered
              Behaviors.same
            case _ =>
              throw new RuntimeException("This should not happen")
          })
        })
    }

    case class thatAlwaysRegisteredAndSendASocketId() extends Base {

      override def behavior: Behavior[connectionregistry.Manager.Command] =
        Behaviors.setup(context => {
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, underTest, `alias`) =>
              replyTo ! Persistence.Registered
              underTest ! SubscriptionStarterProtocol.NewSocketId(createTestProbe[SubscriptionStarterProtocol.NewFeedSocketIdUpdated]().ref, socketId)
              Behaviors.same
            case _ =>
              throw new RuntimeException("This should not happen")
          })
        })
    }

    case class thatReplyAfter3Attempts() extends Base {

      override def behavior: Behavior[connectionregistry.Manager.Command] =
        Behaviors.setup(context => {
          var counter = 0
          Behaviors.receiveMessage({
            case connectionregistry.Manager.Register(replyTo, _, `alias`) =>
              counter += 1
              counter match {
                case 1 | 2 | 3 => Behaviors.same
                case 4 =>
                  replyTo ! Persistence.Registered
                  Behaviors.same
                case 5 => throw new RuntimeException("This should not happen")
              }
            case _ =>
              throw new RuntimeException("This should not happen")
          })
        })

    }

  }

  object SupportMessagesTestContext {

    val justForAnIdea: SupportMessages = new SupportMessages {
      override def registrationTimeout(attempt: Int, registrationTimeout: Timeout): String = s"registrationTimeout $attempt $registrationTimeout"

      override def newSocketIdReceived(socketId: SocketId, mostRecentSocketIdHolder: Option[SocketId]): String =
        mostRecentSocketIdHolder match {
          case None        => s"new socket ${socketId.id}"
          case Some(value) => s"new socket ${socketId.id} replaces socket ${value.id}"
        }

      override def registrationStarted(registrationAttempt: Int): String = s"registrationStarted $registrationAttempt"

      override def subscriptionStarted(): String = "subscriptionStarted"

      override def subscriptionExecutionStarted(socketId: SocketId): String = s"subscriptionExecutionStarted $socketId"

      override def registrationCompleted(): String = "registrationComplete"

      override def subscriptionExecutionCompleted(socketId: SocketId): String = s"subscriptionCompleted $socketId"

      override def subscriptionExecutorFailed(mostRecentSocketId: SocketId): String = s"subscriptionExecutorFailed $mostRecentSocketId"

      override def subscriptionExecutionFailed(mostRecentSocketId: SocketId): String = s"subscriptionExecutionFailed $mostRecentSocketId"
    }

  }

  object SubscriptionExecutorTestContext {

    val actorPrefixName = "subscriptionExecutor"

    val socketIdToMakeTheChildFail = SocketId("thisMakeTheChildFail")

    val aDummyMessageThatMakeThisChildFailed = SubscriptionControllerProtocol.Start(null, socketIdToMakeTheChildFail)

    trait Base extends ActorCollaboratorTestContext[Message, SubscriptionControllerProtocol.Start] {

      override val name: String = SubscriptionExecutorTestContext.actorPrefixName

      override val probe: TestProbe[SubscriptionControllerProtocol.Start] = createTestProbe()

      override def behavior: Behavior[SubscriptionControllerProtocol.Start]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, SubscriptionControllerProtocol.Start] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[SubscriptionControllerProtocol.Start] = throw new IllegalAccessException("Not used in this case")
    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[SubscriptionControllerProtocol.Start] = Behaviors.ignore

    }

    case class thatDoesNotCompleteTheSubscriptions2Times() extends Base {
      override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
        Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
          var counter = 0
          Behaviors.receiveMessage({
            case SubscriptionControllerProtocol.Start(_, _) =>
              counter += 1
              counter match {
                case 1 | 2 => Behaviors.same
                case 3 =>
                  Behaviors.stopped
                case 4 => throw new RuntimeException("This should not happen")
              }
          })
        })
    }

    case class thatStartAndCompleteASubscription() extends Base {
      override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
        Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
          Behaviors.receiveMessage({
            case SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
              replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
              Behaviors.stopped
            case m =>
              throw new IllegalArgumentException(s"this should not have happened $m")
          })
        })
    }

    case class thatCrash2TimesBeforeStartAndCompleteASubscription() extends Base {
      val counter = new AtomicInteger(0)

      override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
        Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
          counter.getAndIncrement()
          Behaviors.receiveMessage({
            case msg @ SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
              if (counter.get() <= 2)
                throwIAmGoingToFailedException(name, msg)
              else {
                replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
                Behaviors.stopped
              }
            case _ =>
              throw new IllegalArgumentException("this should not have happened")
          })
        })
    }

    case class thatSendStartAndThenCrash2TimesBeforeCompleteASubscription() extends Base {
      val counter = new AtomicInteger(0)

      override def behavior: Behavior[SubscriptionControllerProtocol.Start] =
        Behaviors.setup[SubscriptionControllerProtocol.Start](context => {
          counter.getAndIncrement()
          Behaviors.receiveMessage({
            case SubscriptionControllerProtocol.Start(replyToWhenDone, socketIdToBeSubscribedTo) =>
              replyToWhenDone ! SubscriptionControllerProtocol.ExecutionStarted(socketIdToBeSubscribedTo)
              if (counter.get() <= 2)
                throw new IllegalArgumentException("I am going to crash")
              else {
                Behaviors.stopped
              }
            case _ =>
              throw new IllegalArgumentException("this should not have happened")
          })
        })
    }

  }

  case class TestContext(
      subscriptionExecutorTestContext: ActorCollaboratorTestContext[Message, SubscriptionControllerProtocol.Start],
      persistenceConnectionRegistryTestContext: ActorCollaboratorTestContext[NotUsed, connectionregistry.Manager.Command],
      justForAnIdea: SupportMessages,
      instanceUnderCounter: UnderTestNaming
  ) extends ManualTimeBaseTestContext(instanceUnderCounter) {

    val theActorThatGenerateSocketId = createTestProbe[SubscriptionStarterProtocol.NewFeedSocketIdUpdated]().ref

    def lastMonitoredChildSpawn() =
      eventually(actorsWatchers.lastCreatedInstanceOf[SubscriptionControllerProtocol.Command](subscriptionExecutorTestContext.name))

    object spawnInstanceUnderTest {

      val tooHighToTimeout: Timeout = 100.seconds

      private def spawnInstanceUnderTest(registrationTimeout: Timeout, subscriptionTimeout: Timeout, name: String) =
        spawn(
          SubscriptionStarter(
            registrationTimeout,
            subscriptionExecutorTestContext.childMakerFactory(actorsWatchers)(),
            persistenceConnectionRegistryTestContext.ref,
            justForAnIdea
          ),
          name
        )

      def withRegistrationTimeout(registrationTimeout: Timeout) = spawnInstanceUnderTest(registrationTimeout, tooHighToTimeout, instanceUnderCounter.next())

      val withDefaultParameters = () => spawnInstanceUnderTest(tooHighToTimeout, tooHighToTimeout, instanceUnderCounter.next())

    }

  }

}
