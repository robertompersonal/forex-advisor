package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.SerializeAndDeserializeSpec
import org.scalatest.flatspec.AnyFlatSpecLike

class ProtocolSerializationSpec extends ScalaTestWithActorTestKit
  with AnyFlatSpecLike
  with SerializeAndDeserializeSpec {

  "Manager.Register commands" can "be serialize and deserialize" in {
    val toBeTested: Set[AnyRef] = Set(
      Manager.Register(createTestProbe[Persistence.Response]().ref, createTestProbe[SubscriptionStarterProtocol.NewSocketId]().ref, "anAlias"),
    )

    toBeTested.foreach(canBeSerialiseAndDeserialize)
  }


}
