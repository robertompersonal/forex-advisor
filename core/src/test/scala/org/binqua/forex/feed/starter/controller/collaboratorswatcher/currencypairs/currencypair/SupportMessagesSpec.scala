package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.{ParserError, ServerError, SubscriptionFailure}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration._

class SupportMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "RetryingDueToHttpServerError" should "be implemented" in {
    defaultSupportMessages.message(RetryingDueToHttpServerFailure(12, 1.second, SubscriptionFailure(CurrencyPair.Us30, SocketId("2"), ServerError))) shouldBe
      "Http subscription failed due to a server error while attempting to subscribe Us30 with SocketId(2). Going to retry at 1 second interval. This is attempt number 12"
  }

  "RetryingDueToIncomingHttpSubscriptionFailed" should "be implemented" in {
    defaultSupportMessages.message(
      RetryingDueToHttpServerFailure(10, 2.second, SubscriptionFailure(CurrencyPair.Spx500, SocketId("123"), ParserError))
    ) shouldBe
      "Http subscription failed due to a parser error while attempting to subscribe Spx500 with SocketId(123). Going to retry at 2 seconds interval. This is attempt number 10"
  }

  "waitingToBeStoppedBehavior" should "be implemented" in {
    defaultSupportMessages.waitingToBeStoppedBehavior(
      CurrencyPair.Us30
    ) shouldBe s"I contacted SocketIOServer. I did everything I was suppose to do for currency pair ${CurrencyPair.Us30}. I am waiting to be terminated"
  }

}
