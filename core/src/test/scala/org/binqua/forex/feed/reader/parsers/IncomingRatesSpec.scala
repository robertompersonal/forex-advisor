package org.binqua.forex.feed.reader.parsers

import cats.data.Validated.Invalid
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import org.binqua.forex.advisor.model.PosBigDecimal
import org.binqua.forex.advisor.model.PosBigDecimal.wrongBigDecimal
import org.scalacheck.Shrink
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class IncomingRatesSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 50)

  property("Incoming rates with values less than zero cannot be created and errors are accumulated") {

    forAll(IncomingRatesGen.withValidIncomingRates) { incomingRatesAndCurrencyPair =>
      val (incomingRates, _) = incomingRatesAndCurrencyPair

      val ratesUnderTest: Seq[BigDecimal] = incomingRates.map(_.value * -1)

      IncomingRates.validated(ratesUnderTest) match {
        case Invalid(List(firstError: String, secondError: String, thirdError: String, forthError: String)) =>
          assertResult(IncomingRates.toValidatedError(PosBigDecimal.errorMessage(wrongBigDecimal(ratesUnderTest(3))), field = "minSellOfTheDay"))(firstError)
          assertResult(IncomingRates.toValidatedError(PosBigDecimal.errorMessage(wrongBigDecimal(ratesUnderTest(0))), field = "sell"))(secondError)
          assertResult(IncomingRates.toValidatedError(PosBigDecimal.errorMessage(wrongBigDecimal(ratesUnderTest(1))), field = "buy"))(thirdError)
          assertResult(IncomingRates.toValidatedError(PosBigDecimal.errorMessage(wrongBigDecimal(ratesUnderTest(2))), field = "maxBuyOfTheDay"))(forthError)
        case _ => fail(s"should fail $ratesUnderTest")
      }
    }
  }

  property("toValidatedError produce right error") {

    forAll(IncomingRatesGen.withValidIncomingRates) { incomingRatesAndCurrencyPair =>
      val (incomingRates, _) = incomingRatesAndCurrencyPair

      val ratesUnderTest: Seq[BigDecimal] = incomingRates.map(_.value * -1)

      IncomingRates.toValidatedError(PosBigDecimal.errorMessage(wrongBigDecimal(ratesUnderTest.head)), "Test") should be(
        s"Test is wrong: A PosBigDecimal value has to be a decimal number >= 0. Cannot create a PositiveBigDecimal. Reason: ${ratesUnderTest.head} is wrong"
      )

    }
  }

  property("Incoming rates with min sell of the day greater than sell cannot be created and pretty message is available") {

    forAll(IncomingRatesGen.withMinSellOfTheDayGreaterThanSell.map(_.map(_.value))) { posIncomingRates =>
      val List(sellRate: BigDecimal, _: BigDecimal, _: BigDecimal, minSellOfTheDay: BigDecimal) = posIncomingRates

      info("----")

      val expectedMessage = s"minSellOfTheDay $minSellOfTheDay has to be <= than sell $sellRate"

      info(expectedMessage)

      IncomingRates.validated(posIncomingRates) match {
        case Invalid(errorMessage :: Nil) => assertResult(expectedMessage)(errorMessage)
        case _                            => fail(s"this test should fail")
      }
    }
  }

  property("Incoming rates with sell greater or equal than buy cannot be created and pretty message is available") {

    forAll(IncomingRatesGen.withSellGreaterOrEqualThanBuy.map(_.map(_.value))) { posIncomingRates =>
      val List(sell: BigDecimal, buy: BigDecimal, _: BigDecimal, _: BigDecimal) = posIncomingRates

      info("----")

      val expectedMessage = s"sell $sell has to be < than buy $buy"

      info(expectedMessage)

      IncomingRates.validated(posIncomingRates) match {
        case Invalid(errorMessage :: Nil) => assertResult(expectedMessage)(errorMessage)
        case _                            => fail(s"this test should fail")
      }
    }

  }

  property("Incoming rates with max buy of the day smaller than buy cannot be created and pretty message is available") {

    forAll(IncomingRatesGen.withMaxBuyOfTheDaySmallerThanBuy.map(_.map(_.value))) { posIncomingRates =>
      val List(_: BigDecimal, buy: BigDecimal, max: BigDecimal, _: BigDecimal) = posIncomingRates

      info("----")

      val expectedMessage = s"buy $buy has to be <= than maxBuyOfTheDay $max"

      info(expectedMessage)

      IncomingRates.validated(posIncomingRates) match {
        case Invalid(errorMessage :: Nil) => assertResult(expectedMessage)(errorMessage)
        case _                            => fail(s"this test should fail")
      }
    }
  }

  property("An instance should be serialised and deserialised correctly") {

    val mapper = new ObjectMapper()
    mapper.registerModule(
      new SimpleModule()
        .addSerializer(classOf[IncomingRates], new IncomingRatesSerializer())
        .addDeserializer(classOf[IncomingRates], new IncomingRatesDeserializer())
    )

    forAll((IncomingQuoteGen.validIncomingQuotes, "incomingQuotes")) { exp =>
      val (expIncomingQuoteRecordedEvent, _) = exp

      val expIncomingRates = expIncomingQuoteRecordedEvent.incomingRates

      info("-----")
      Given(s"a valid $expIncomingRates")

      val str = mapper.writeValueAsString(expIncomingRates)
      When(s"it is serialised in $str")

      val actual = mapper.readValue(str, classOf[IncomingRates])

      Then(s"can be deserialised to get back the initial instance")
      assertResult(expIncomingRates)(actual)

    }
  }

}
