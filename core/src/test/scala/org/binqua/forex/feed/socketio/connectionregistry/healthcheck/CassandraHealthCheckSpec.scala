package org.binqua.forex.feed.socketio.connectionregistry.healthcheck
import akka.actor.ActorInitializationException
import akka.actor.testkit.typed.LoggingEvent
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.ActorRef
import akka.persistence.cassandra.testkit.CassandraLauncher
import com.datastax.oss.driver.api.core.CqlSession
import com.typesafe.config.{Config => AkkaConfig, ConfigFactory => AKKAConfigFactory}
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck._
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.core.{MakeItUnsafe, UnsafeConfigReader}
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import java.io.File
import java.net.InetSocketAddress
import scala.concurrent.duration._

object CassandraHealthCheckSpec {
  val healtcheckConfig = AKKAConfigFactory.parseString(s"""
       | org.binqua.forex.feed.socketio.connectionregistry.healthcheck.retryDelay = 11s
       | datastax-java-driver {
       |  basic.contact-points = ["persistenceJournal:9042"]
       |  basic.load-balancing-policy.local-datacenter = datacenter1
       |}
       |
    """.stripMargin)
}

class CassandraHealthCheckSpec
    extends ScalaTestWithActorTestKit(AKKAConfigFactory.load(CassandraHealthCheckSpec.healtcheckConfig))
    with AnyFlatSpecLike
    with BeforeAndAfterEach
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation
    with TestingFacilities {

  "Given configuration is wrong, the actor" should "not be created" in new TestContext() {

    LoggingTestKit.error[ActorInitializationException].expect {
      spawn(
        CassandraHealthCheck(
          unsafeConfigReader = _ => throw new IllegalArgumentException("some error"),
          supportMessagesFactory = _ => supportMessages
        ),
        underTestNaming.next()
      )
    }

  }

  "Given cassandra does not have akka keyspace then the client" should "not receive healthy message" in new TestContext() {

    underTest ! NotifyWhenHealthy(clientProbe.ref)

    clientProbe.expectNoMessage(500.millis)

  }

  "actor" should "accept only NotifyWhenHealthy before start" in new TestContext() {

    List(
      InternalCqlSession(null),
      CheckIfNamespaceHasBeenCreated,
      MaybeInternalHealthy(true),
      MaybeInternalHealthy(false)
    ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(NotifyWhenHealthy)))

  }

  "Given cassandra has akka keyspace and all required tables, then the client" should "receive healthy message as soon as it send a Notify and then stop itself" in new TestContext() {

    createAllNecessaryAkkaTablesSync()

    matchDistinctLogs(supportMessages.cassandraIsHealthy()).expect {
      LoggingTestKit.info(supportMessages.cassandraNotInitYet()).withOccurrences(0).expect {

        underTest ! NotifyWhenHealthy(clientProbe.ref)

        clientProbe.expectMessage(Healthy)

        clientProbe.expectTerminated(underTest)
      }
    }

  }

  "Given cassandra does not have an akka keyspace yet, the actor" should "keep query cassandra until keyspace becomes available. Some messages are unhandled!" in new TestContext() {

    def collectOnlyAGivenNumberOfEvents(expNumberOfEvents: Int): Function[LoggingEvent, Boolean] = {
      var interestingEventCounter: Int = 1
      ev: LoggingEvent => {
        if (ev.message == supportMessages.cassandraNotInitYet() && interestingEventCounter <= expNumberOfEvents) {
          interestingEventCounter += 1
          true
        } else
          false
      }
    }

    LoggingTestKit.info(supportMessages.cassandraNotInitYet()).withCustom(collectOnlyAGivenNumberOfEvents(3)).withOccurrences(3).expect {

      underTest ! NotifyWhenHealthy(clientProbe.ref)

      waitFor(2.second, soThat("cql session is created and we are connected to cassandra"))

      List(
        NotifyWhenHealthy(null),
        InternalCqlSession(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(CheckIfNamespaceHasBeenCreated, MaybeInternalHealthy)))

      createAllNecessaryAkkaTablesSync()

      clientProbe.expectMessage(Healthy)

      clientProbe.expectTerminated(underTest)

    }
  }

  "during cql session creation, the actor" should "accept only InternalCqlSession" in new TestContext() {

    underTest ! NotifyWhenHealthy(clientProbe.ref)

    List(
      CheckIfNamespaceHasBeenCreated,
      MaybeInternalHealthy(true),
      MaybeInternalHealthy(false)
    ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(InternalCqlSession)))

  }

  override def afterEach(): Unit = {
    super.afterEach()
    CassandraLauncher.stop()
  }

  case class TestContext()(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(underTestNaming) {

    val supportMessages = new SupportMessages {

      override def cassandraNotInitYet(): String = "Cassandra not init yet"

      override def cassandraIsHealthy(): String = "Cassandra is healthy"
    }

    CassandraLauncher.start(
      cassandraDirectory = new File("target/" + system.name),
      configResource = CassandraLauncher.DefaultTestConfigResource,
      clean = true,
      port = 0,
      classpath = CassandraLauncher.classpathForResources("logback-test.xml")
    )

    val actorConfiguration: Config = Config
      .validated(
        cassandraHost = "localhost",
        cassandraPort = CassandraLauncher.randomPort,
        localDataCenter = "datacenter1",
        retryDelay = 500.millis
      )
      .unsafe

    def testConfigurationButReturn(returnThisConfig: Config): UnsafeConfigReader[Config] =
      (akkaConfig: AkkaConfig) => {

        val actualConfiguration = UnsafeConfigReader(akkaConfig)

        s"${actualConfiguration.host}:${actualConfiguration.port}" shouldBe "persistenceJournal:9042"

        actualConfiguration.localDataCenter shouldBe actorConfiguration.localDataCenter

        returnThisConfig
      }

    val underTest: ActorRef[Message] = spawn(
      CassandraHealthCheck(
        unsafeConfigReader = testConfigurationButReturn(returnThisConfig = actorConfiguration),
        (_: Config) => supportMessages
      ),
      underTestNaming.next()
    )

    val clientProbe: TestProbe[Response] = createTestProbe()

    def createAllNecessaryAkkaTablesSync(): Unit = {
      val session = CqlSession
        .builder()
        .withLocalDatacenter(actorConfiguration.localDataCenter)
        .addContactPoint(new InetSocketAddress(actorConfiguration.host, CassandraLauncher.randomPort))
        .build()
      try {
        session.execute("CREATE KEYSPACE IF NOT EXISTS akka WITH REPLICATION = { 'class' : 'SimpleStrategy','replication_factor': 1 };").one()
        List("messages", "tag_views", "tag_scanning", "tag_write_progress", "tag_scanning", "metadata", "all_persistence_ids").foreach { tableName =>
          session.execute(s"CREATE TABLE IF NOT EXISTS akka.$tableName (id text PRIMARY KEY)").one()
        }
        eventually {
          if (!session.getMetadata.getKeyspace("akka").get().getTable("all_persistence_ids").isPresent) throw new IllegalStateException("not ready yet")
        }
      } finally {
        session.close()
      }
    }

  }

}
