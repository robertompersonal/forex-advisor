package org.binqua.forex.feed

import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair, TradingPairGen}
import org.binqua.forex.feed.notifier.collaborators.{DueToPipValue, UpdatingAccount}
import org.binqua.forex.feed.reader.QuoteGen
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.Gen
import org.scalatest.prop.Tables

object UpdatingAccountGen extends TestingFacilities {

  def anUpdatingAccountFor(currencyPair: CurrencyPair, accountCurrency: AccountCurrency): Gen[UpdatingAccount] = {
    anUpdatingAccountWith(QuoteGen.quotesOf(cats.kernel.Eq.eqv(_, currencyPair)).sample.get, accountCurrency)
  }

  def anUpdatingAccountWith(quote: Quote, accountCurrency: AccountCurrency): Gen[UpdatingAccount] = {
    Gen.const(if (accountCurrency.isEqualToACurrencyIn(quote.rates.currencyPair)) {
      UpdatingAccount.create(quote, accountCurrency).get
    } else {
      val currencyPairs = TradingPairGen.tradingPairsReferenceData.keys.toList
      CurrencyPair.currencyPairNeededForPipValueCalculation(accountCurrency)(quote.rates.currencyPair.quoteCurrency)(currencyPairs) match {
        case Right(value) =>
          val extraQuote = QuoteGen.quotesOf(cats.kernel.Eq.eqv(_, value)).sample.get
          UpdatingAccount.validate(quote, extraQuote, accountCurrency, DueToPipValue).getOrElse(thisTestShouldNotHaveArrivedHere)
        case Left(errorMessage) =>
          throw new IllegalArgumentException(errorMessage)
      }
    })
  }

  def allUpdatingAccountsIncompatibleWith(
      givenCurrencyPair: CurrencyPair,
      givenAccountCurrency: AccountCurrency
  ): org.scalatest.prop.TableFor1[UpdatingAccount] = {
    import cats.kernel.Eq
    import org.binqua.forex.implicits.instances.accountCurrency._
    import org.binqua.forex.implicits.instances.currencyPair._

    val allTuples: List[(CurrencyPair, AccountCurrency)] = for {
      cp <- CurrencyPair.values
      ac <- AccountCurrency.values
    } yield (cp, ac)

    val tuplesWithGivenArgsRemoved = allTuples.filterNot(cpAndAc => {
      val (cp, ac) = cpAndAc
      Eq.eqv(cp, givenCurrencyPair) && Eq.eqv(ac, givenAccountCurrency)
    })

    val invalidUpdatingAccounts: List[UpdatingAccount] = tuplesWithGivenArgsRemoved.map(cpAndAc => {
      val (cp, ac) = cpAndAc
      anUpdatingAccountFor(cp, ac).sample.get
    })

    Tables.Table("Invalid Updating Account").++(invalidUpdatingAccounts)
  }

  def allIncompatibleUpdatingAccounts(
      wantedCurrencyPair: CurrencyPair => Boolean,
      wantedAccountCurrency: AccountCurrency => Boolean
  ): org.scalatest.prop.TableFor2[UpdatingAccount, (CurrencyPair, AccountCurrency)] = {
    import cats.kernel.Eq
    import org.binqua.forex.implicits.instances.accountCurrency._
    import org.binqua.forex.implicits.instances.currencyPair._

    val allTuples: List[(CurrencyPair, AccountCurrency)] = for {
      cp <- CurrencyPair.values.filter(wantedCurrencyPair)
      ac <- AccountCurrency.values.filter(wantedAccountCurrency)
    } yield (cp, ac)

    def allInvalidUpdatingAccountFor(
        givenCurrencyPair: CurrencyPair,
        givenAccountCurrency: AccountCurrency
    ): List[(UpdatingAccount, (CurrencyPair, AccountCurrency))] = {
      val tuplesWithGivenArgsRemoved = allTuples.filterNot(cpAndAc => {
        val (cp, ac) = cpAndAc
        Eq.eqv(cp, givenCurrencyPair) && Eq.eqv(ac, givenAccountCurrency)
      })

      val invalidUpdatingAccounts: List[UpdatingAccount] = tuplesWithGivenArgsRemoved.map(cpAndAc => {
        val (cp, ac) = cpAndAc
        anUpdatingAccountFor(cp, ac).sample.get
      })

      invalidUpdatingAccounts.map { iua => (iua, (givenCurrencyPair, givenAccountCurrency)) }
    }

    val allInvalidUpdatingAccounts: List[(UpdatingAccount, (CurrencyPair, AccountCurrency))] = allTuples.flatMap { cpAndAc =>
      val (givenCurrencyPair, givenAccountCurrency) = cpAndAc
      allInvalidUpdatingAccountFor(givenCurrencyPair, givenAccountCurrency)
    }

    Tables.Table(("Invalid Updating Account", "Given CurrencyPair and AccountCurrency"), allInvalidUpdatingAccounts: _*)
  }

}
