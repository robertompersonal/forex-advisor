package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import com.typesafe.config.ConfigFactory
import org.binqua.forex.util.TestingFacilities
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{Assertion, GivenWhenThen}

import scala.concurrent.duration._

class SocketIOClientModelSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  private val connectionUrlKey = "org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.connectionUrl"

  private val retryToConnectIntervalKey =
    "org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.retryToConnectInterval"

  private val validRows: Set[String] = Set(
    connectionUrlKey + " = \"https://localhost:8080\" ",
    "org.binqua.forex.feed.loginToken = \"thisIsAToken\" ",
    retryToConnectIntervalKey + " = 2s "
  )

  "Given all properties specified, we" should "get a valid config" in {
    val actualConfig = ConfigValidator(ConfigFactory.parseString(toConfigFormat(validRows))).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.connectionUrl shouldBe "https://localhost:8080"
    actualConfig.loginToken shouldBe "thisIsAToken"
    actualConfig.retryToConnectInterval shouldBe 2.seconds
  }

  "an empty config" should "be spotted" in {
    val actualError: Seq[String] = ConfigValidator(ConfigFactory.parseString("{}")).swap.getOrElse(thisTestShouldNotHaveArrivedHere)

    actualError.head should include("Configuration for SocketIOClient is invalid")
    actualError.tail.head should include("No all configuration values have been found")
    actualError.size shouldBe 2
  }

  "A config without connectionUrl" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, connectionUrlKey), ConfigValidator)
  }

  "A config without loginToken" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, "org.binqua.forex.feed.loginToken"), ConfigValidator)
  }

  "A config without retryToConnectInterval" should "be invalid" in {
    assertConfigurationIsInvalid(RowsFilter(validRows, retryToConnectIntervalKey), ConfigValidator)
  }

  "retryToConnectInterval" should "be validated as finite duration" in {
    val actualError: Seq[String] = ConfigValidator(ConfigFactory.parseString(toConfigFormat(validRows).replace("2s", "xxx"))).swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualError.head should include("key " + retryToConnectIntervalKey + " does not have a valid duration value")
    actualError.size shouldBe 1
  }

  "Config.validated" should "know when its arguments are invalid" in new ConfigInvalidContext() {
    val actualError: Seq[String] = Config
      .validated(connectionUrl = "", loginToken = "", retryToConnectInterval = 2.seconds)
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualError shouldBe expErrors
  }

  case class ConfigInvalidContext() {
    val expErrors = List(
      "connectionUrl has to be not empty",
      "loginToken has to be not empty"
    )
  }

  case class ConfigValidContext() {

    def isValid(actualConfig: Config): Assertion = {
      actualConfig.connectionUrl shouldBe context.connectionUrl
      actualConfig.loginToken shouldBe context.loginToken
      actualConfig.retryToConnectInterval shouldBe context.retryToConnectInterval
    }

    object context {
      val connectionUrl = "a"
      val loginToken = "b"
      val retryToConnectInterval = 2.seconds
    }

  }

  "Config.validated" should "know when its arguments are valid" in new ConfigValidContext() {
    isValid(
      Config
        .validated(context.connectionUrl, context.loginToken, context.retryToConnectInterval)
        .getOrElse(thisTestShouldNotHaveArrivedHere)
    )
  }

  "Error" should "contain info about the actor" in {
    ConfigValidator(toAkkaConfig(removeConfigurationFor(RowsFilter(validRows, retryToConnectIntervalKey)))).swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .head should be("Configuration for SocketIOClient is invalid.")

  }

}
