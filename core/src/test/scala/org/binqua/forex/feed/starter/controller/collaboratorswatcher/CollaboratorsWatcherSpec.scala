package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.UnhandledMessage
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.eventstream.EventStream
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, PostStop}
import com.typesafe.config.ConfigFactory
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcherProtocol._
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.Moments.{JumpAfterFirstIntervalHasExpired, JumpAfterSecondIntervalHasExpired, JumpAfterThirdIntervalHasExpired}
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import scala.collection.immutable.Set
import scala.concurrent.duration._

class CollaboratorsWatcherSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  val timeoutBeforeCheckingCollaboratorsTermination: FiniteDuration = 500.millis

  "After received Start, the actor" should "starts the subscriber, log all messages received during the subscription and messages to the client" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val wrappedSubscriberResponse = WrappedSubscriberResponse(SubscriberProtocolSamples.subscriptionRunning)
      val underTestClientExpReceivedMessage = SubscriptionRunning(SubscriberProtocolSamples.subscriptionRunning.socketId)

      val subscriptionRunning: String = justForAnIdeaSupportMessages.newSubscriptionResponse(SubscriberProtocolSamples.subscriptionRunning)
      val fullySubscribed: String = justForAnIdeaSupportMessages.newSubscriptionResponse(SubscriberProtocolSamples.fullySubscribed)

      LoggingTestKit
        .custom(event =>
          event.message match {
            case `subscriptionRunning` => true
            case `fullySubscribed`     => true
          }
        )
        .withOccurrences(newOccurrences = 3 + 1)
        .expect {

          underTest ! aDefaultStartMessage

          underTestClientProbe.expectNoMessage()

          underTest ! wrappedSubscriberResponse

          underTestClientProbe.expectMessage(underTestClientExpReceivedMessage)

          underTest ! wrappedSubscriberResponse

          underTestClientProbe.expectMessage(underTestClientExpReceivedMessage)

          underTest ! wrappedSubscriberResponse

          underTestClientProbe.expectMessage(underTestClientExpReceivedMessage)

          underTest ! WrappedSubscriberResponse(SubscriberProtocolSamples.fullySubscribed)

          underTestClientProbe.expectMessage(SubscriptionDone(SubscriberProtocolSamples.fullySubscribed.socketId))

        }

    }

  "Once started, the actor" should "accept only WrappedSubscriberResponse" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! aDefaultStartMessage

      List(
        Start(null, SocketId(randomString)),
        InternalCheckThatBothCollaboratorsHaveBeenTerminated
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(WrappedSubscriberResponse)))

      testKit.stop(underTest)
    }

  "Start" should "be the only accepted message after the actor has been spawn" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      List(
        InternalCheckThatBothCollaboratorsHaveBeenTerminated,
        WrappedSubscriberResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Start)))

    }

  "Once Started, the actor" should "stop the subscriber child if the currencyPairsSubscriber child fails and be able to complete the subscription" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatFailWithASpecialMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      val thisChildWillBeTerminated: ActorRef[SubscriberProtocol.Command] = subscriberContext.lastCreatedChild()

      val thisChildWillBeTerminatedToo: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      matchDistinctLogs(
        messages = justForAnIdeaSupportMessages
          .currencyPairsSubscriberTerminated(currencyPairsSubscriberContext.lastCreatedChild(), subscriberContext.lastCreatedChild()),
        justForAnIdeaSupportMessages.bothCollaboratorsTerminated(attempt = 1)
      ).expect {
        currencyPairsSubscriberContext.lastCreatedChild() ! CurrencyPairsSubscriberContext.aDummyMessageThatMakeTheChildFail
        waitALittleBit(soThat("new children can be created and old one can be stopped"))
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
      }

      val newCreatedCurrencyPairsSubscriber: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      fishExactlyOneMessageAndIgnoreOthers(subscriberContext.probe)(toBeMatched => {
        val SubscriberProtocol.Start(`aSocketIdToSubscribeTo`, `newCreatedCurrencyPairsSubscriber`, replyTo) = toBeMatched
        replyTo ! SubscriberProtocol.RunningSubscriptionNow(aSocketIdToSubscribeTo, attempt = 1)
        replyTo ! SubscriberProtocol.FullySubscribed(aSocketIdToSubscribeTo, attempt = 1, currencyPairs = Set.empty)

      })

      subscriberContext.probe.expectTerminated(thisChildWillBeTerminated)

      currencyPairsSubscriberContext.probe.expectTerminated(thisChildWillBeTerminatedToo)

      currencyPairsSubscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      subscriberContext.assertChildrenSpawnUntilNowAre(2)

      underTestClientProbe.expectMessage(SubscriptionRunning(aSocketIdToSubscribeTo))

      underTestClientProbe.expectMessage(SubscriptionDone(aSocketIdToSubscribeTo))

      testKit.stop(underTest)

    }

  "Once Started, the actor" should "stop the currencyPairsSubscriber child if the subscriber child fails and it should be able to complete the subscription" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatFailWithASpecialMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      val thisChildWillBeTerminated: ActorRef[SubscriberProtocol.Command] = subscriberContext.lastCreatedChild()

      val thisChildWillBeTerminatedToo: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      matchDistinctLogs(
        messages = justForAnIdeaSupportMessages.subscriberTerminated(currencyPairsSubscriberContext.lastCreatedChild(), subscriberContext.lastCreatedChild()),
        justForAnIdeaSupportMessages.bothCollaboratorsTerminated(1)
      ).expect {
        subscriberContext.lastCreatedChild() ! SubscriberContext.aDummyMessageThatMakeTheChildFail
        waitALittleBit(soThat("new children can be created and old one can be stopped"))
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
      }

      val newCreatedCurrencyPairsSubscriber: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      fishExactlyOneMessageAndIgnoreOthers(subscriberContext.probe)(toBeMatched => {
        val SubscriberProtocol.Start(`aSocketIdToSubscribeTo`, `newCreatedCurrencyPairsSubscriber`, replyTo) = toBeMatched
        replyTo ! SubscriberProtocol.RunningSubscriptionNow(aSocketIdToSubscribeTo, attempt = 1)
        replyTo ! SubscriberProtocol.FullySubscribed(aSocketIdToSubscribeTo, attempt = 1, currencyPairs = Set.empty)

      })

      subscriberContext.probe.expectTerminated(thisChildWillBeTerminated)

      currencyPairsSubscriberContext.probe.expectTerminated(thisChildWillBeTerminatedToo)

      currencyPairsSubscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      subscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      underTestClientProbe.expectMessage(SubscriptionRunning(aSocketIdToSubscribeTo))

      underTestClientProbe.expectMessage(SubscriptionDone(aSocketIdToSubscribeTo))

      testKit.stop(underTest)

    }

  "When the actor detects that subscriber failed, it" should "accept only InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatFailWithASpecialMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      subscriberContext.lastCreatedChild() ! SubscriberContext.aDummyMessageThatMakeTheChildFail

      waitALittleBit(soThat("failed child can be detected"))

      List(
        Start(null, SocketId(randomString))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse)))

      assertSubscriberResponseIsLoggedAsIgnored(someSubscriberResponse)

      testKit.stop(underTest)

    }

  "When the actor detects that CurrencyPairsSubscriber child failed, it" should "accept only InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatFailWithASpecialMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      currencyPairsSubscriberContext.lastCreatedChild() ! CurrencyPairsSubscriberContext.aDummyMessageThatMakeTheChildFail

      waitALittleBit(soThat("failed child can be detected"))

      List(
        Start(null, SocketId(randomString))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse)))

      assertSubscriberResponseIsLoggedAsIgnored(someSubscriberResponse)

      testKit.stop(underTest)

    }

  "Given currencyPairsSubscriber child failed but Subscriber child takes 3 timer intervals before terminate, the actor" should "keep checking that both children are terminated before continue" in {

    val timeNeededToPostStopToSucceed = 600.millis

    new TestContext(
      CurrencyPairsSubscriberContext.thatFailWithASpecialMessage(),
      SubscriberContext.thatTakesSomeTimeBeforeTerminate(timeNeededToPostStopToSucceed),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      val thisChildWillBeTerminated: ActorRef[SubscriberProtocol.Command] = subscriberContext.lastCreatedChild()

      val thisChildWillBeTerminatedToo: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      currencyPairsSubscriberContext.lastCreatedChild() ! CurrencyPairsSubscriberContext.aDummyMessageThatMakeTheChildFail

      waitALittleBit(soThat("the actor can detect CurrencyPairsSubscriber child failed"))

      matchDistinctLogs(
        messages = justForAnIdeaSupportMessages.oneCollaboratorStillNotTerminated(Iterable("subscriberContext-1"), attempt = 1),
        justForAnIdeaSupportMessages.oneCollaboratorStillNotTerminated(Iterable("subscriberContext-1"), attempt = 2),
        justForAnIdeaSupportMessages.bothCollaboratorsTerminated(attempt = 3)
      ).expect {
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)
        waitFor(timeNeededToPostStopToSucceed, soThat("finally Subscriber child stopped"))
        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)
      }

      val newCreatedCurrencyPairsSubscriber: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      fishExactlyOneMessageAndIgnoreOthers(subscriberContext.probe)(toBeMatched => {
        val SubscriberProtocol.Start(`aSocketIdToSubscribeTo`, `newCreatedCurrencyPairsSubscriber`, replyTo) = toBeMatched
        replyTo ! SubscriberProtocol.RunningSubscriptionNow(aSocketIdToSubscribeTo, attempt = 1)
        replyTo ! SubscriberProtocol.FullySubscribed(aSocketIdToSubscribeTo, attempt = 1, currencyPairs = Set.empty)
      })

      subscriberContext.probe.expectTerminated(thisChildWillBeTerminated)

      currencyPairsSubscriberContext.probe.expectTerminated(thisChildWillBeTerminatedToo)

      currencyPairsSubscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      subscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      underTestClientProbe.expectMessage(SubscriptionRunning(aSocketIdToSubscribeTo))

      underTestClientProbe.expectMessage(SubscriptionDone(aSocketIdToSubscribeTo))

      testKit.stop(underTest)
    }

  }

  "Given Subscriber child failed but CurrencyPairsSubscriber child takes 3 timer intervals before terminate, the actor" should "keep checking that both children are terminated before continue" in {

    val timeNeededToPostStopToSucceed = 500.millis

    new TestContext(
      CurrencyPairsSubscriberContext.thatTakesSomeTimeBeforeStop(timeNeededToPostStopToSucceed),
      SubscriberContext.thatFailWithASpecialMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      val thisChildWillBeTerminated: ActorRef[SubscriberProtocol.Command] = subscriberContext.lastCreatedChild()

      val thisChildWillBeTerminatedToo: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      subscriberContext.lastCreatedChild() ! SubscriberContext.aDummyMessageThatMakeTheChildFail

      waitALittleBit(soThat("the actor can detect Subscriber child failed"))

      matchDistinctLogs(
        messages = justForAnIdeaSupportMessages.oneCollaboratorStillNotTerminated(Iterable("currencyPairsSubscriber-1"), attempt = 1),
        justForAnIdeaSupportMessages.oneCollaboratorStillNotTerminated(Iterable("currencyPairsSubscriber-1"), attempt = 2),
        justForAnIdeaSupportMessages.bothCollaboratorsTerminated(attempt = 3)
      ).expect {
        manualClock.timeAdvances(JumpAfterFirstIntervalHasExpired)
        manualClock.timeAdvances(JumpAfterSecondIntervalHasExpired)
        waitFor(timeNeededToPostStopToSucceed, soThat("finally Subscriber child stopped"))
        manualClock.timeAdvances(JumpAfterThirdIntervalHasExpired)
      }

      val newCreatedCurrencyPairsSubscriber: ActorRef[CurrencyPairsSubscriberProtocol.Command] = currencyPairsSubscriberContext.lastCreatedChild()

      fishExactlyOneMessageAndIgnoreOthers(subscriberContext.probe)(toBeMatched => {
        val SubscriberProtocol.Start(`aSocketIdToSubscribeTo`, `newCreatedCurrencyPairsSubscriber`, replyTo) = toBeMatched
        replyTo ! SubscriberProtocol.RunningSubscriptionNow(aSocketIdToSubscribeTo, attempt = 1)
        replyTo ! SubscriberProtocol.FullySubscribed(aSocketIdToSubscribeTo, attempt = 1, currencyPairs = Set.empty)

      })

      subscriberContext.probe.expectTerminated(thisChildWillBeTerminated)

      currencyPairsSubscriberContext.probe.expectTerminated(thisChildWillBeTerminatedToo)

      currencyPairsSubscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      subscriberContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

      underTestClientProbe.expectMessage(SubscriptionRunning(aSocketIdToSubscribeTo))

      underTestClientProbe.expectMessage(SubscriptionDone(aSocketIdToSubscribeTo))

    }

  }

  "During children termination, the actor" should "accept only InternalCheckThatBothCollaboratorsHaveBeenTerminated and WrappedSubscriberResponse" in {

    val timeNeededToPostStopToSucceed = 500.millis

    new TestContext(
      CurrencyPairsSubscriberContext.thatTakesSomeTimeBeforeStop(timeNeededToPostStopToSucceed),
      SubscriberContext.thatFailWithASpecialMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      val manualClock = manualClockBuilder.withInterval(timeoutBeforeCheckingCollaboratorsTermination)

      underTest ! aDefaultStartMessage

      subscriberContext.lastCreatedChild() ! SubscriberContext.aDummyMessageThatMakeTheChildFail

      waitALittleBit(soThat("the actor can detect Subscriber child termination"))

      assertSubscriberResponseIsLoggedAsIgnored(someSubscriberResponse)

      List(
        Start(null, SocketId(randomString))
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(InternalCheckThatBothCollaboratorsHaveBeenTerminated, WrappedSubscriberResponse)))

    }

  }

  "The actor" should "not fails when subscriber stop, because it means subscriber has done its job and it terminated itself" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatStopAfterReceivedFirstMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! aDefaultStartMessage

      waitFor(100.millis, soThat("under test has time to die"))

      deadWatcher.assertThatIsStillAlive(underTest.path.name, 1.second)

    }

  "The actor" should "not fails when currencyPairsSubscriber stop, because it means currencyPairsSubscriber has done its job and it terminated itself" in
    new TestContext(
      CurrencyPairsSubscriberContext.thatIgnoreAnyMessage(),
      SubscriberContext.thatIgnoreAnyMessage(),
      justForAnIdeaSupportMessages = SupportMessagesContext.justForAnIdeaSupport
    ) {

      underTest ! aDefaultStartMessage

      currencyPairsSubscriberContext.lastCreatedChild() ! CurrencyPairsSubscriberContext.aDummyMessageThatStopTheChild

      waitFor(100.millis, soThat("under test has time to die"))

      deadWatcher.assertThatIsStillAlive(underTest.path.name, 1.second)

    }

  object CurrencyPairsSubscriberContext {

    val aDummyMessageThatMakeTheChildFail: CurrencyPairsSubscriberProtocol.Command =
      CurrencyPairsSubscriberProtocol.Subscribe(Set.empty, SocketId(randomString), null)
    val aDummyMessageThatStopTheChild: CurrencyPairsSubscriberProtocol.Command =
      CurrencyPairsSubscriberProtocol.Subscribe(Set.empty, SocketId(randomString), null)

    val actorPrefixName = "currencyPairsSubscriber"

    type theType = NewActorCollaboratorTestContext[Message, CurrencyPairsSubscriberProtocol.Command]

    abstract class Base(smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = CurrencyPairsSubscriberContext.actorPrefixName

      override val probe: TestProbe[CurrencyPairsSubscriberProtocol.Command] = createTestProbe()

      override def behavior: Behavior[CurrencyPairsSubscriberProtocol.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, CurrencyPairsSubscriberProtocol.Command] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[CurrencyPairsSubscriberProtocol.Command] = throw new IllegalAccessException("Not used in this case")

    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CurrencyPairsSubscriberProtocol.Command] = Behaviors.ignore
      }
    }

    def thatStopWithASpecialMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CurrencyPairsSubscriberProtocol.Command] =
          Behaviors.receiveMessage({ m =>
            if (m == aDummyMessageThatStopTheChild)
              Behaviors.stopped
            else
              Behaviors.same
          })
      }
    }

    def thatFailWithASpecialMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CurrencyPairsSubscriberProtocol.Command] =
          Behaviors.receiveMessage({ m =>
            if (m == aDummyMessageThatMakeTheChildFail)
              throwIAmGoingToFailedException
            else
              Behaviors.same
          })
      }
    }

    def thatTakesSomeTimeBeforeStop(timeNeededToPostStopToSucceed: FiniteDuration): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[CurrencyPairsSubscriberProtocol.Command] =
          Behaviors
            .receiveMessage[CurrencyPairsSubscriberProtocol.Command]({ m =>
              if (m == aDummyMessageThatMakeTheChildFail)
                throwIAmGoingToFailedException
              else
                Behaviors.same
            })
            .receiveSignal({
              case (_, PostStop) =>
                Thread.sleep(timeNeededToPostStopToSucceed.toMillis)
                Behaviors.same
            })

      }

    }
  }

  object SubscriberContext {

    val actorPrefixName = "subscriberContext"

    val aDummyMessageThatMakeTheChildFail: SubscriberProtocol.Command = SubscriberProtocol.Start(SocketId(randomString), null, null)

    type theType = NewActorCollaboratorTestContext[Message, SubscriberProtocol.Command]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

      override val name: String = SubscriberContext.actorPrefixName

      override val probe: TestProbe[SubscriberProtocol.Command] = createTestProbe()

      override def behavior: Behavior[SubscriberProtocol.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, SubscriberProtocol.Command] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[SubscriberProtocol.Command] = throw new IllegalAccessException("Not used in this case")

    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SubscriberProtocol.Command] = Behaviors.ignore
      }
    }

    def thatTakesSomeTimeBeforeTerminate(timeNeededToPostStopToSucceed: FiniteDuration): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SubscriberProtocol.Command] =
          Behaviors
            .receiveMessage[SubscriberProtocol.Command]({ m =>
              if (m == aDummyMessageThatMakeTheChildFail)
                throwIAmGoingToFailedException
              else
                Behaviors.same
            })
            .receiveSignal({
              case (_, PostStop) =>
                Thread.sleep(timeNeededToPostStopToSucceed.toMillis)
                Behaviors.same
            })
      }
    }

    def thatStopAfterReceivedFirstMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SubscriberProtocol.Command] =
          Behaviors.receiveMessage({ _ =>
            Behaviors.stopped
          })
      }
    }

    def thatFailWithASpecialMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
      new Base(smartDeadWatcher) {
        override def behavior: Behavior[SubscriberProtocol.Command] =
          Behaviors.receiveMessage({ m =>
            if (m == aDummyMessageThatMakeTheChildFail)
              throwIAmGoingToFailedException
            else
              Behaviors.same
          })

      }
    }
  }

  object SupportMessagesContext {

    val justForAnIdeaSupport: Support[String] = new Support[String] {

      val format: SubscriberProtocol.Response => String = response => s"received subscription ${response.getClass.getSimpleName}"

      override def newSubscriptionResponse(response: SubscriberProtocol.Response): String = format(response)

      override def collaboratorsTerminationInProgressMessageIgnored(subscriberResponse: SubscriberProtocol.Response): String =
        s"collaborators under termination $subscriberResponse ignored ${format(subscriberResponse)}"

      override def currencyPairsSubscriberTerminated(
          currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
          subscriberRef: ActorRef[SubscriberProtocol.Command]
      ): String = s"currencyPairsSubscribeTerminated ${currencyPairsSubscriberRef.path.name} ${subscriberRef.path.name}"

      override def subscriberTerminated(
          currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
          subscriberRef: ActorRef[SubscriberProtocol.Command]
      ): String = s"subscriberTerminated ${currencyPairsSubscriberRef.path.name} ${subscriberRef.path.name}"

      override def oneCollaboratorStillNotTerminated(iterable: Iterable[String], attempt: Int): String =
        s"oneCollaboratorStillNotTerminated ${iterable.mkString("-")} attempt $attempt"

      override def bothCollaboratorsTerminated(attempts: Int): String = s"bothCollaboratorsTerminated after $attempts"

    }

    def supportMaker(support: Support[String]): SUPPORT_MAKER =
      context =>
        new Support[Unit] {
          override def newSubscriptionResponse(response: SubscriberProtocol.Response): Unit = context.log.info(support.newSubscriptionResponse(response))

          override def collaboratorsTerminationInProgressMessageIgnored(subscriberResponse: SubscriberProtocol.Response): Unit =
            context.log.info(support.collaboratorsTerminationInProgressMessageIgnored(subscriberResponse))

          override def currencyPairsSubscriberTerminated(
              currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
              subscriberRef: ActorRef[SubscriberProtocol.Command]
          ): Unit = context.log.info(support.currencyPairsSubscriberTerminated(currencyPairsSubscriberRef, subscriberRef))

          override def subscriberTerminated(
              currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command],
              subscriberRef: ActorRef[SubscriberProtocol.Command]
          ): Unit = context.log.info(support.subscriberTerminated(currencyPairsSubscriberRef, subscriberRef))

          override def oneCollaboratorStillNotTerminated(iterable: Iterable[String], attempt: Int): Unit =
            context.log.info(support.oneCollaboratorStillNotTerminated(iterable, attempt))

          override def bothCollaboratorsTerminated(attempts: Int): Unit = context.log.info(support.bothCollaboratorsTerminated(attempts))
        }

  }

  case class TestContext(
      currencyPairsSubscriberContextMaker: SmartDeadWatcher => CurrencyPairsSubscriberContext.theType,
      subscriberContextMaker: SmartDeadWatcher => SubscriberContext.theType,
      justForAnIdeaSupportMessages: Support[String]
  )(implicit underTestNaming: UnderTestNaming)
      extends ManualTimeBaseTestContext(underTestNaming) {

    val currencyPairsSubscriberContext: CurrencyPairsSubscriberContext.theType = currencyPairsSubscriberContextMaker(actorsWatchers)

    val subscriberContext: SubscriberContext.theType = subscriberContextMaker(actorsWatchers)

    val aSocketIdToSubscribeTo = SocketId("1")

    val underTestClientProbe = createTestProbe[Response]("underTest-client")

    val underTest: ActorRef[Message] = spawn(
      CollaboratorsWatcher(
        currencyPairsSubscriberContext.childMakerFactory(actorsWatchers)(),
        subscriberContext.childMakerFactory(actorsWatchers)(),
        timeoutBeforeCheckingCollaboratorsTermination,
        SupportMessagesContext.supportMaker(justForAnIdeaSupportMessages)
      )
    )

    deadWatcher.watch(underTest)

    val aDefaultStartMessage = Start(underTestClientProbe.ref, aSocketIdToSubscribeTo)

    testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

    object SubscriberProtocolSamples {
      val fullySubscribed = SubscriberProtocol.FullySubscribed(aSocketIdToSubscribeTo, attempt = 1, Set(CurrencyPair.EurUsd, CurrencyPair.Us30))
      val subscriptionRunning = SubscriberProtocol.RunningSubscriptionNow(aSocketIdToSubscribeTo, attempt = 1)
    }

    val someSubscriberResponse = SubscriberProtocol.FullySubscribed(null, 1, null)

    def assertSubscriberResponseIsLoggedAsIgnored(someSubscriberResponse: SubscriberProtocol.Response) = {
      matchDistinctLogs(messages = justForAnIdeaSupportMessages.collaboratorsTerminationInProgressMessageIgnored(someSubscriberResponse)).expect {
        underTest ! WrappedSubscriberResponse(someSubscriberResponse)
      }
    }

  }

}
