package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import cats.data.Validated.Valid
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.core.MakeItUnsafe
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities}
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{Assertion, GivenWhenThen}

import scala.concurrent.duration._

class ConfigReaderSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with ConfigurationSpecFacilities {

  object ConfigKeys {
    val contactPoint = "datastax-java-driver.basic.contact-points"
    val dataCenter = "datastax-java-driver.basic.load-balancing-policy.local-datacenter"
    val retryDelay = "org.binqua.forex.feed.socketio.connectionregistry.healthcheck.retryDelay"
  }

  case class ConfigKeys(theExpectedConfig: Config) {

    import ConfigKeys._

    def configWith(host: String, port: Int, localDataCenter: String, retryDelayInSecs: Long): Set[String] =
      Set(
        s"$retryDelay = $retryDelayInSecs s ",
        s"""$contactPoint = ["$host:$port"]""",
        s"$dataCenter = $localDataCenter"
      )

    val validRows: Set[String] = configWith(
      theExpectedConfig.host,
      theExpectedConfig.port,
      theExpectedConfig.localDataCenter,
      theExpectedConfig.retryDelay.toSeconds
    )

    val akkaConfig: AkkaConfig = toAkkaConfig(validRows)
  }

  private val validReferenceData: ConfigKeys = ConfigKeys(
    Config.validated(cassandraHost = "localhost", cassandraPort = 9042, localDataCenter = "dc", retryDelay = 11.seconds).unsafe
  )

  "a valid akka config" should {

    "create a valid healthcheck config if read with unsafeConfigReader" in {

      val actualConfig = UnsafeConfigReader(validReferenceData.akkaConfig)

      actualConfig.host shouldBe "localhost"
      actualConfig.port shouldBe 9042
      actualConfig.localDataCenter shouldBe "dc"
      actualConfig.retryDelay shouldBe 11.seconds

    }

    "create a valid configValidator config if read with unsafeConfigReader" in {
      CassandraHealthCheckExternal.configValidator(toAkkaConfig(validReferenceData.validRows)) match {
        case Valid(actualConfig) => actualConfig should be
        case _                   => thisTestShouldNotHaveArrivedHere(because = "the configuration is valid")
      }
    }
  }
  "a config file with datastax-java-driver.basic.contact-points value's key" should {

    "be invalid if is not = to [host:port]" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"${ConfigKeys.contactPoint} = 1")),
        expError =
          s"cannot find cassandra host value ... key ${ConfigKeys.contactPoint} = [host:port] is missing - cannot find cassandra port value ... key ${ConfigKeys.contactPoint} = [host:port] is missing"
      )
    }

    "be invalid if = [host:]" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"""${ConfigKeys.contactPoint} = ["host:"]""")),
        expError = s"cannot find cassandra port value ... key ${ConfigKeys.contactPoint} = [host:port] is missing"
      )
    }

    "be invalid if = [:12]" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"""${ConfigKeys.contactPoint} = [":12"]""")),
        expError = s"host in ${ConfigKeys.contactPoint} has to be non empty"
      )
    }

    "be invalid if = [host:-1]" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"""${ConfigKeys.contactPoint} = ["host:-1"]""")),
        expError = s"port in ${ConfigKeys.contactPoint} has to be > 0"
      )
    }

    "be invalid if key is missing" in {
      readersWorks(
        configUnderTest = toAkkaConfig(
          removeConfigurationFor(
            RowsFilter(
              validReferenceData.validRows, {
                ConfigKeys.contactPoint
              }
            )
          )
        ),
        expError =
          s"cannot find cassandra host value ... key ${ConfigKeys.contactPoint} = [host:port] is missing - cannot find cassandra port value ... key ${ConfigKeys.contactPoint} = [host:port] is missing"
      )
    }
  }

  "a config file with datastax-java-driver.basic.load-balancing-policy.local-datacenter's key" should {

    "be invalid if key is missing" in {
      readersWorks(
        configUnderTest = toAkkaConfig(removeConfigurationFor(RowsFilter(validReferenceData.validRows, ConfigKeys.dataCenter))),
        expError = s"cannot find cassandra dataCenter value ... key ${ConfigKeys.dataCenter} is missing"
      )
    }

    "be invalid if = \"\"" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"""${ConfigKeys.dataCenter} = "" """)),
        expError = s"dataCenter in ${ConfigKeys.dataCenter} has to be non empty"
      )
    }

  }

  def readersWorks(configUnderTest: AkkaConfig, expError: String): Assertion = {
    val exp: String = s"Cassandra health check configuration is invalid. - $expError"
    the[IllegalArgumentException] thrownBy UnsafeConfigReader(configUnderTest) should have message exp
    CassandraHealthCheckExternal.configValidator(configUnderTest).swap.getOrElse(thisTestShouldNotHaveArrivedHere(because = "config is invalid")) should be(
      exp.split(" - ").toList
    )
  }

  "a config file with org.binqua.forex.feed.socketio.connectionregistry.healthcheck.retryDelay value's key" should {

    "be invalid if key is missing" in {

      readersWorks(
        configUnderTest = toAkkaConfig(removeConfigurationFor(RowsFilter(validReferenceData.validRows, ConfigKeys.retryDelay))),
        expError = s"cannot find a duration value ... key ${ConfigKeys.retryDelay} is missing"
      )

    }

    "be invalid if cannot be parse to a duration" in {
      readersWorks(
        configUnderTest = toAkkaConfig(replaceEntry(validReferenceData.validRows, newKeyValue = s"${ConfigKeys.retryDelay} = something ")),
        expError = s"key ${ConfigKeys.retryDelay} does not have a valid duration value"
      )
    }
  }

}
