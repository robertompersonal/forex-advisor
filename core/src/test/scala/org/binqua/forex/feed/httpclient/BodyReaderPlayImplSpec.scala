package org.binqua.forex.feed.httpclient

import org.scalatest.flatspec.AnyFlatSpec
import play.api.libs.json.{JsError, JsSuccess, Json}

class BodyReaderPlayImplSpec extends AnyFlatSpec {

  "a valid json" should "be parsed correctly" in {
    assertResult(JsSuccess(true))(BodyReaderPlayImpl.parse("{\"response\":{\"executed\":true,\"error\":\"\"},\"pairs\":[{\"Symbol\":\"EUR/USD\"}]}"))
  }

  "a json with invalid currencyPair" should "be invalid" in {
    val incomingJson = "{\"response\":{\"executed\":true,\"error\":\"\"},\"pairs\":[{\"Symbol\":\"crappy\"}]}"
    assertResult(JsError(s"Parse unsuccessful. $incomingJson is wrong"))(BodyReaderPlayImpl.parse(incomingJson))
  }

  "an json with an error" should "be invalid" in {
    val incomingJson = "{\"response\":{\"executed\":true,\"error\":\"something\"},\"pairs\":[{\"Symbol\":\"EUR/USD\"}]}"
    assertResult(JsError(s"Parse unsuccessful. $incomingJson is wrong"))(BodyReaderPlayImpl.parse(incomingJson))
  }

  "an json with execute false " should "be invalid" in {
    val incomingJson = "{\"response\":{\"executed\":false,\"error\":\"\"},\"pairs\":[{\"Symbol\":\"EUR/USD\"}]}"
    assertResult(JsError(s"Parse unsuccessful. $incomingJson is wrong"))(BodyReaderPlayImpl.parse(incomingJson))
  }

  "a valid executed json" should "be parsed correctly" in {
    assertResult(true)(Json.parse("{\"response\":{\"executed\":true}}").validate(BodyReaderPlayImpl.executedRead).isSuccess)
    assertResult(true)(Json.parse("{\"response\":{\"executed\":false}}").validate(BodyReaderPlayImpl.executedRead).isSuccess)
    assertResult(false)(Json.parse("{\"response\":123}").validate(BodyReaderPlayImpl.executedRead).isSuccess)
  }

  "a valid error json" should "be parsed correctly" in {
    assertResult(true)(Json.parse("{\"response\":{\"error\":\"\"}}").validate(BodyReaderPlayImpl.errorRead).isSuccess)
    assertResult(true)(Json.parse("{\"response\":{\"error\":\"somethingElse\"}}").validate(BodyReaderPlayImpl.errorRead).isSuccess)
    assertResult(false)(Json.parse("{\"response\":123}").validate(BodyReaderPlayImpl.errorRead).isSuccess)
  }

  "a body response that cannot be parsed" should "be an error" in {
    val incomingJson = "bla bla"
    assertResult(JsError(s"Parse unsuccessful. $incomingJson is wrong"))(BodyReaderPlayImpl.parse(incomingJson))
  }
}
