package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

import scala.concurrent.duration._

class SupportSpec
    extends ScalaTestWithActorTestKit
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  val subscriberRef: ActorRef[SubscriberProtocol.Command] = createTestProbe[SubscriberProtocol.Command]("s").ref

  val currencyPairsSubscriberRef: ActorRef[CurrencyPairsSubscriberProtocol.Command] = createTestProbe[CurrencyPairsSubscriberProtocol.Command]("cps").ref

  val supportMakerUnderTest: SUPPORT_MAKER = collaboratorswatcher.logAsInfoSupportMaker(13.seconds)

  "bothCollaboratorsTerminated log message" should "be implemented" in {
    assertLogMessageIsCorrect(
      expMessage = s"Both collaborator have been terminated after 2 attempts",
      contextClient = supportMakerUnderTest(_).bothCollaboratorsTerminated(2)
    )
  }

  "currencyPairsSubscriberTerminated log message" should "be implemented" in {
    assertLogMessageIsCorrect(
      expMessage =
        s"$currencyPairsSubscriberRef has been terminated! Going to terminate $subscriberRef too and then wait 13 seconds before checking if they have been terminated.",
      contextClient = supportMakerUnderTest(_).currencyPairsSubscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
    )
  }

  "subscriberTerminated log message" should "be implemented" in {
    assertLogMessageIsCorrect(
      expMessage =
        s"$subscriberRef has been terminated! Going to terminate $currencyPairsSubscriberRef too and then wait 13 seconds before checking if they have been terminated.",
      contextClient = supportMakerUnderTest(_).subscriberTerminated(currencyPairsSubscriberRef, subscriberRef)
    )
  }

  "oneCollaboratorStillNotTerminated log message" should "be implemented" in {
    assertLogMessageIsCorrect(
      expMessage = s"After waiting 13 seconds children [a,b] are still alive. I am going to wait again 13 seconds. Attempt number 10.",
      contextClient = supportMakerUnderTest(_).oneCollaboratorStillNotTerminated(Iterable("a", "b"), attempt = 10)
    )
  }

  "collaboratorsTerminationInProgressMessageIgnored log message" should "be implemented" in {
    val aMessageToBeIgnored = SubscriberProtocol.RunningSubscriptionNow(SocketId("1"), attempt = 1)

    assertLogMessageIsCorrect(
      expMessage = s"Collaborators termination in progress. Message $aMessageToBeIgnored is going to be ignored.",
      contextClient = supportMakerUnderTest(_).collaboratorsTerminationInProgressMessageIgnored(aMessageToBeIgnored)
    )
  }

  "newSubscriptionResponse log message" should "be implemented for RunningSubscriptionNow" in {
    val aResponse = SubscriberProtocol.RunningSubscriptionNow(SocketId("1"), attempt = 1)

    assertLogMessageIsCorrect(
      expMessage = s"Subscription for socket id 1 started right now. Attempt number 1.",
      contextClient = supportMakerUnderTest(_).newSubscriptionResponse(aResponse)
    )
  }

  "newSubscriptionResponse log message" should "be implemented for FullySubscribed" in {
    val aResponse = SubscriberProtocol.FullySubscribed(SocketId("1"), attempt = 2, Set(CurrencyPair.EurUsd, CurrencyPair.Us30))

    assertLogMessageIsCorrect(
      expMessage = s"Subscription for socket id 1 and currency pairs [EurUsd,Us30] is completed successfully after 2 attempts.",
      contextClient = supportMakerUnderTest(_).newSubscriptionResponse(aResponse)
    )
  }

  private def assertLogMessageIsCorrect(expMessage: String, contextClient: ActorContext[CollaboratorsWatcherProtocol.Message] => Unit): Any = {
    matchOnlyMessage(expMessage).expect {
      spawn(Behaviors.setup[CollaboratorsWatcherProtocol.Message](actorContext => {
        contextClient(actorContext)
        Behaviors.ignore
      }))
    }
  }

}
