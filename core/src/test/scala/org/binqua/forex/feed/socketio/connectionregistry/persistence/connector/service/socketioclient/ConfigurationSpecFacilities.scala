package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import cats.data.Validated
import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.util.TestingFacilities
import org.scalatest.matchers.should.Matchers._

trait ConfigurationSpecFacilities {

  this: TestingFacilities =>

  def removeConfigurationFor(rowsFilter: RowsFilter): Set[String] = {
    removeConfigurationFor(rowsFilter.validRows, rowsFilter.entryToBeRemoved)
  }

  def removeConfigurationFor(validConfiguration: Set[String], valueToBeRemoved: String): Set[String] = {
    val newRows = validConfiguration.filterNot(_.contains(valueToBeRemoved))
    newRows.size shouldBe (validConfiguration.size - 1)
    newRows
  }

  def replaceEntry(validConfiguration: Set[String], newKeyValue: String): Set[String] = {
    val valueToBeAmend = newKeyValue.split(" = ")(0)
    val underConstruction = removeConfigurationFor(validConfiguration, valueToBeAmend)
    val newRows = underConstruction + s"$valueToBeAmend = ${newKeyValue.split(" = ")(1)}"
    newRows.size shouldBe validConfiguration.size
    newRows
  }

  def assertConfigurationIsInvalid[C](rowsFilter: RowsFilter, f: Config => Validated[List[String], C]) = {
    val actualError = f(toAkkaConfig(removeConfigurationFor(rowsFilter))).swap.getOrElse(thisTestShouldNotHaveArrivedHere)
    actualError.tail.head should include(s"No setting at '${rowsFilter.entryToBeRemoved}'")
    actualError.size shouldBe 2
  }

  def toANewAkkaConfig[C](rowsFilter: RowsFilter): Config = {
    toAkkaConfig(removeConfigurationFor(rowsFilter))
  }

  def toConfigFormat(rows: Set[String]): String = s"{${rows.mkString("\n", "\n", "\n")}}"

  def toAkkaConfig(validRows: Set[String]): Config = ConfigFactory.parseString(toConfigFormat(validRows))

  case class EntryToBeRemoved(entryToBeRemoved: String)

  case class RowsFilter(validRows: Set[String], entryToBeRemoved: String)

  case class KeyValue(validRows: Set[String], entryToBeRemoved: String)

}
