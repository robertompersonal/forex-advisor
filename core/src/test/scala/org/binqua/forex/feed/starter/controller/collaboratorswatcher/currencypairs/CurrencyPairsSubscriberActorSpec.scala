package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModel.State
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.{WrappedServicesFinderResponse, _}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder.ServicesFinderProtocol
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.{CHILD_MAKER, CHILD_MAKER_FACTORY}
import org.binqua.forex.util.Moments.{JustAfterFirstIntervalHasExpired, JustBeforeFirstIntervalExpires}
import org.binqua.forex.util.{BaseManualTimeActorTestKit, ManualClock}

import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.duration._

class CurrencyPairsSubscriberActorSpec extends BaseManualTimeActorTestKit {

  private val socketId = SocketId("1")

  val tooLongWillNeverHappen: FiniteDuration = 50.second

  private val currencyPairsToSubscribe: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)

  private val supportMessage: SupportMessageUtil.NewSubscriptionSummaryMessages = (result, cp, summary) =>
    s"subscription result=$result currency pair = $cp summary = $summary"

  val justAnIdeaSupportMessages = new SupportMessage {
    override def jobDone(): String = "jobdone"

    override def startSubscription(timeout: FiniteDuration, currencyPairs: Set[CurrencyPair]): String = s"startSubscription $timeout $currencyPairs"

    override def timeAvailablePassed(timeoutBeforeReportingSubscriptionResults: FiniteDuration, waitingResult: Set[CurrencyPair]) =
      s"timeAvailablePassed $timeoutBeforeReportingSubscriptionResults $waitingResult"
  }

  "It takes less than timeout to the actor to receive subscription results from all children and all currency pairs are subscribed." +
    "the actor" should "find all needed services and subscribe to all currency pairs: as soon as all currency pairs subscription result" +
    " are received, it notifies back the happened subscription. Then the actor stop itself" in
    new TestContext(ServicesFinderTestContext.thatIgnoreAnyMessage()) {

      val underTest = spawn(
        CurrencyPairsSubscriberActor(
          timeoutBeforeReportingSubscriptionResults = tooLongWillNeverHappen,
          servicesFinderTestContext.childMakerFactory()(),
          currencyPairSubscriberChildMaker,
          supportMessage,
          justAnIdeaSupportMessages
        )
      )

      waitALittleBit(soThat("child can be created"))

      servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
        val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
        replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
      }

      underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

      matchDistinctLogs(
        messages = justAnIdeaSupportMessages.startSubscription(tooLongWillNeverHappen, currencyPairsToSubscribe),
        supportMessage(
          true,
          CurrencyPair.EurUsd,
          State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
        ),
        supportMessage(
          true,
          CurrencyPair.Us30,
          State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurUsd, CurrencyPair.Us30), unsubscribed = Set())
        )
      ).expect {

        fishExactlyOneMessageAndIgnoreOthers(eurUsdProbe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
        }

        fishExactlyOneMessageAndIgnoreOthers(us30Probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.Us30, socketId = socketId)
        }

      }

      underTestClientProbe.expectTerminated(eurUsdRefRepo.get())
      underTestClientProbe.expectTerminated(us30RefRepo.get())

      underTestClientProbe.expectMessage(Subscriptions(socketId, subscribe = currencyPairsToSubscribe, unsubscribe = Set.empty))

      underTestClientProbe.expectNoMessage()

      underTestClientProbe.expectTerminated(underTest)

      unhandledMessageSubscriber.expectNoMessage()

    }

  "It takes more than timeout to the actor to subscribed all currency pairs. " +
    "Details: after received Subscribe, CurrencyPairsSubscriberActor" should "tries to subscribe eurUsd and us30 but us30 cannot be subscribed before timeout. " +
    "When timeout happens, then, only eurUsd subscription will be notified back to replyTo and the actor will fetch again the ServiceRef to be ready for next subscription" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage()
  ) {

    val manualClock: ManualClock = manualClockBuilder.withInterval(theIntervalUnderTest = 400.millis)

    val underTest: ActorRef[Message] = spawn(
      CurrencyPairsSubscriberActor(
        manualClock.theIntervalUnderTest,
        servicesFinderTestContext.childMakerFactory()(),
        currencyPairSubscriberChildMaker,
        supportMessage,
        justAnIdeaSupportMessages
      )
    )

    waitALittleBit(soThat("child can be created"))

    servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    matchDistinctLogs(
      messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, currencyPairsToSubscribe),
      supportMessage(
        true,
        CurrencyPair.EurUsd,
        State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
      )
    ).expect {

      underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

      fishExactlyOneMessageAndIgnoreOthers(eurUsdProbe) { toBeMatched =>
        val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
        replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
      }
    }

    matchDistinctLogs(messages =
      justAnIdeaSupportMessages
        .timeAvailablePassed(timeoutBeforeReportingSubscriptionResults = manualClock.theIntervalUnderTest, waitingResult = Set(CurrencyPair.Us30))
    ).expect {
      underTestClientProbe.expectTerminated(eurUsdRefRepo.get())
      manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)
      underTestClientProbe.expectMessage(Subscriptions(socketId, subscribe = Set(CurrencyPair.EurUsd), unsubscribe = Set(CurrencyPair.Us30)))
    }

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched

      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 2)

    underTestClientProbe.expectTerminated(us30RefRepo.get())

    matchDistinctLogs(
      messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, Set(CurrencyPair.Us30)),
      supportMessage(true, CurrencyPair.Us30, State.Summary(socketId, waitingResult = Set.empty, subscribed = Set(CurrencyPair.Us30), unsubscribed = Set()))
    ).expect {

      underTest ! Subscribe(Set(CurrencyPair.Us30), socketId, underTestClientProbe.ref)

      manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

      fishExactlyOneMessageAndIgnoreOthers(us30Probe) { toBeMatched =>
        val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
        replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.Us30, socketId = socketId)
      }
    }

  }

  "Given it takes less than timeout to the actor to receive all subscription result (one currency pair is unsubscribed), the actor" should "notifies back the happened subscription before timeout" in
    new TestContext(ServicesFinderTestContext.thatIgnoreAnyMessage()) {

      val manualClock: ManualClock = manualClockBuilder.withInterval(theIntervalUnderTest = 400.millis)

      val underTest: ActorRef[Message] = spawn(
        CurrencyPairsSubscriberActor(
          timeoutBeforeReportingSubscriptionResults = manualClock.theIntervalUnderTest,
          servicesFinderTestContext.childMakerFactory()(),
          currencyPairSubscriberChildMaker,
          supportMessage,
          justAnIdeaSupportMessages
        )
      )

      waitALittleBit(soThat("child can be created"))

      servicesFinderTestContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
        val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
        replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
      }

      matchDistinctLogs(
        messages = justAnIdeaSupportMessages.startSubscription(manualClock.theIntervalUnderTest, currencyPairsToSubscribe),
        supportMessage(
          true,
          CurrencyPair.EurUsd,
          State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set())
        ),
        supportMessage(
          false,
          CurrencyPair.Us30,
          State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.EurUsd), unsubscribed = Set(CurrencyPair.Us30))
        ),
        justAnIdeaSupportMessages.jobDone()
      ).expect {

        underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

        fishExactlyOneMessageAndIgnoreOthers(eurUsdProbe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.EurUsd, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = true, currencyPair = CurrencyPair.EurUsd, socketId = socketId)
        }

        fishExactlyOneMessageAndIgnoreOthers(us30Probe) { toBeMatched =>
          val CurrencyPairSubscriberProtocol.StartSubscription(replyTo, CurrencyPair.Us30, socketId, _, _, _) = toBeMatched
          replyTo ! SocketIOClientProtocol.Subscribed(result = false, currencyPair = CurrencyPair.Us30, socketId = socketId)
        }

        underTestClientProbe.expectTerminated(eurUsdRefRepo.get())

        underTestClientProbe.expectTerminated(us30RefRepo.get())

        underTestClientProbe.expectTerminated(underTest)
      }

    }

  "A newly spawned actor" should "not accept PrivateReportSubscriptionResult and WrappedSocketIOClientResponse" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage()
  ) {
    val underTest = spawn(
      CurrencyPairsSubscriberActor(
        timeoutBeforeReportingSubscriptionResults = 500.millis,
        servicesFinderChildMaker = servicesFinderTestContext.childMakerFactory()(),
        childMaker = aSingleSubscribedChildMaker(Set(CurrencyPair.EurUsd).head),
        supportMessageUtil = (_: Boolean, _: CurrencyPair, _: State.Summary) => "",
        supportMessage = justAnIdeaSupportMessages
      ),
      randomString
    )

    List(
      PrivateReportSubscriptionResult(SocketId("1"), null),
      WrappedSocketIOClientResponse(SocketIOClientProtocol.Subscribed(true, CurrencyPair.Us30, SocketId("1")))
    ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Subscribe)))

  }

  "After received Subscribe, the actor" should "treat Subscribe and WrappedServicesFinderResponse as unhandled" in new TestContext(
    ServicesFinderTestContext.thatIgnoreAnyMessage()
  ) {
    val underTest = spawn(
      CurrencyPairsSubscriberActor(
        timeoutBeforeReportingSubscriptionResults = tooLongWillNeverHappen,
        servicesFinderTestContext.childMakerFactory()(),
        childMaker = theIgnoreAllMessages(),
        supportMessageUtil = (_: Boolean, _: CurrencyPair, _: State.Summary) => "",
        supportMessage = justAnIdeaSupportMessages
      ),
      randomString
    )

    fishExactlyOneMessageAndIgnoreOthers(servicesFinderTestContext.probe) { toBeMatched =>
      val ServicesFinderProtocol.GetServiceReferences(replyTo) = toBeMatched
      replyTo ! ServicesFinderProtocol.ServicesReferences(feedContentRef, httpClientSubscriberServiceRef, socketIOClientRef)
    }

    underTest ! Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref)

    List(
      Subscribe(currencyPairsToSubscribe, socketId, underTestClientProbe.ref),
      WrappedServicesFinderResponse(null)
    ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(PrivateReportSubscriptionResult, WrappedSocketIOClientResponse)))

  }

  def aSingleSubscribedChildMaker(currencyPair: CurrencyPair): CurrencyPair => CHILD_MAKER[Message, CurrencyPairSubscriberProtocol.Command] =
    currencyPair =>
      context => {
        context.spawn(
          Behaviors.setup[CurrencyPairSubscriberProtocol.Command](_ => {
            Behaviors.receiveMessage[CurrencyPairSubscriberProtocol.Command]({
              case CurrencyPairSubscriberProtocol.Stop =>
                Behaviors.stopped
              case sub: CurrencyPairSubscriberProtocol.StartSubscription =>
                sub.whoWantsToKnoAboutSubscriptionResult ! SocketIOClientProtocol.Subscribed(result = true, currencyPair, sub.socketId)
                Behaviors.same
            })
          }),
          name = currencyPair.toString
        )
      }

  def theIgnoreAllMessages(): CurrencyPair => CHILD_MAKER[Message, CurrencyPairSubscriberProtocol.Command] =
    currencyPair =>
      context => {
        context.spawn(
          Behaviors.setup[CurrencyPairSubscriberProtocol.Command](_ => {
            Behaviors.ignore
          }),
          name = currencyPair.toString
        )
      }

  object ServicesFinderTestContext {

    val actorPrefixName = "ServicesFinder"

    type TheType = NewActorCollaboratorTestContext[CurrencyPairsSubscriberProtocol.Message, ServicesFinderProtocol.Command]

    abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends TheType(smartDeadWatcher) {

      override val name: String = ServicesFinderTestContext.actorPrefixName

      override val probe: TestProbe[ServicesFinderProtocol.Command] = createTestProbe()

      override def behavior: Behavior[ServicesFinderProtocol.Command]

      override def childMakerFactory(
          actorsWatchers: SmartDeadWatcher
      ): CHILD_MAKER_FACTORY[CurrencyPairsSubscriberProtocol.Message, ServicesFinderProtocol.Command] =
        throw new IllegalAccessException("Not used in this case")

      override def childMakerFactory(): CHILD_MAKER_FACTORY[CurrencyPairsSubscriberProtocol.Message, ServicesFinderProtocol.Command] =
        smartDeadWatcher.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[ServicesFinderProtocol.Command] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    def thatIgnoreAnyMessage(): SmartDeadWatcher => TheType =
      smartDeadWatcher => {
        new Base(smartDeadWatcher) {
          override def behavior: Behavior[ServicesFinderProtocol.Command] = Behaviors.ignore
        }

      }
  }

  case class TestContext(servicesFinderTestContextMaker: TEST_CONTEXT_MAKER[ServicesFinderTestContext.TheType])(implicit underTestNaming: UnderTestNaming)
      extends ManualTimeBaseTestContext(underTestNaming) {

    val underTestClientProbe: TestProbe[Subscriptions] = createTestProbe[Subscriptions]()

    val servicesFinderTestContext = servicesFinderTestContextMaker(actorsWatchers)

    val feedContentRef: ActorRef[SocketIOClientProtocol.FeedContent] = createTestProbe[SocketIOClientProtocol.FeedContent]("feedContent").ref

    val httpClientSubscriberServiceRef: ActorRef[HttpClientSubscriberServiceProtocol.SubscribeTo] =
      createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]("httpClientSubscriber").ref

    val socketIOClientRef: ActorRef[SocketIOClientProtocol.Command] = createTestProbe[SocketIOClientProtocol.Command]("socketIOClient").ref

    val eurUsdRefRepo = new AtomicReference[ActorRef[CurrencyPairSubscriberProtocol.Command]]()

    val us30RefRepo = new AtomicReference[ActorRef[CurrencyPairSubscriberProtocol.Command]]()

    val eurUsdProbe = createTestProbe[CurrencyPairSubscriberProtocol.Command]("eurUsd")

    val us30Probe = createTestProbe[CurrencyPairSubscriberProtocol.Command]("us30")

    val eurUsd = spawn(
      Behaviors.monitor(
        eurUsdProbe.ref,
        Behaviors.receiveMessage[CurrencyPairSubscriberProtocol.Command]({
          case CurrencyPairSubscriberProtocol.Stop =>
            Behaviors.stopped
          case _ => Behaviors.same
        })
      ),
      name = s"eurUsd-$randomString"
    )
    eurUsdRefRepo.set(eurUsd)

    val us30 = spawn(
      Behaviors.monitor(
        us30Probe.ref,
        Behaviors.receiveMessage[CurrencyPairSubscriberProtocol.Command]({
          case CurrencyPairSubscriberProtocol.Stop =>
            Behaviors.stopped
          case _ => Behaviors.same
        })
      ),
      name = s"us30-$randomString"
    )
    us30RefRepo.set(us30)

    val currencyPairSubscriberChildMaker: CurrencyPair => CHILD_MAKER[Message, CurrencyPairSubscriberProtocol.Command] = currencyPair =>
      context => {
        currencyPair match {
          case CurrencyPair.EurUsd => eurUsd
          case CurrencyPair.Us30   => us30
          case m                   => throw new IllegalArgumentException(s"$m is not recognised")
        }
      }

  }

}
