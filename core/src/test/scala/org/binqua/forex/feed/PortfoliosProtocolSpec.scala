package org.binqua.forex.feed

import cats.Eq
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.numeric.NonNegInt
import org.binqua.forex.advisor.model.CurrencyPair.{EurUsd, GbpUsd}
import org.binqua.forex.advisor.model.{PosBigDecimal, _}
import org.binqua.forex.feed.UpdatingAccountGen.anUpdatingAccountFor
import org.binqua.forex.feed.notifier.collaborators.UpdatingAccount
import org.binqua.forex.feed.protocol.UpdatedCommandFactoryRepository.{
  CurrencyPairNotFound,
  UpdatedCommandFactoryRepositoryMismatchArguments,
  UpdatedCommandNotFound
}
import org.binqua.forex.feed.protocol._
import org.binqua.forex.util.{TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfoliosProtocolSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities with Validation {

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration = PropertyCheckConfiguration(minSuccessful = 20)

  property("EurUsdUpdatedForEurAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(CurrencyPair.EurUsd, AccountCurrency.Eur)

    actualValidUpdatedCommand shouldBe a[EurUsdUpdatedForEurAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)

  }

  property(
    "I cannot build a UpdatedCommand using a valid updatingAccount inconsistent with it: " +
      "UpdatedCommand currency pair and account currency have to match the UpdatingAccount"
  ) {
    import cats.syntax.show._
    import org.binqua.forex.implicits.instances.accountCurrency._
    import org.binqua.forex.implicits.instances.currencyPair._
    import org.binqua.forex.implicits.instances.updatingAccount._

    val currencyPairs =
      (cp: CurrencyPair) => List(CurrencyPair.EurUsd, CurrencyPair.GbpUsd, CurrencyPair.EurGbp, CurrencyPair.Spx500, CurrencyPair.Us30).exists(Eq.eqv(_, cp))
    val accountCurrencies = (ac: AccountCurrency) => List(AccountCurrency.Eur, AccountCurrency.Gbp, AccountCurrency.Usd).exists(Eq.eqv(_, ac))

    forAll(UpdatingAccountGen.allIncompatibleUpdatingAccounts(currencyPairs, accountCurrencies)) { (invalidUpdatingAccount, currencyPairAndAccountCurrency) =>
      val (wantedCurrencyPair, wantedAccountCurrency) = currencyPairAndAccountCurrency

      mapBasedUpdatedCommandFactory
        .by(wantedCurrencyPair, wantedAccountCurrency)
        .getOrElse(thisTestShouldNotHaveArrivedHere)(invalidUpdatingAccount) match {
        case Left(errorSituation) =>
          info("---")
          Given(s"${wantedCurrencyPair.show} and ${wantedAccountCurrency.show}")
          When(s"I tried to create a UpdatedCommand with an invalid ${invalidUpdatingAccount.show}")
          val expErrorSituation = UpdatedCommandFactoryRepositoryMismatchArguments(invalidUpdatingAccount, wantedCurrencyPair, wantedAccountCurrency)
          assertResult(expErrorSituation)(errorSituation)
          Then(s"I got a nice error situation $expErrorSituation")

        case value => thisTestShouldNotHaveArrivedHere(Map("details" -> value))
      }

    }
  }

  property("I cannot build an UpdatedCommand if the currency pair is not registered. I get a nice error situation") {

    val notFoundCurrencyPair = new CurrencyPair {
      override val baseCurrency: BaseCurrency = new BaseCurrency {
        override val name: String = "x"
        override val index: Boolean = false
      }
      override val quoteCurrency: QuoteCurrency = new QuoteCurrency {
        override val baseCurrency: BaseCurrency = new BaseCurrency {
          override val name: String = "y"
          override val index: Boolean = false
        }
        override val pipPosition: NonNegInt = refineMV[NonNegative](1)
        override val pipValue: PosBigDecimal = PosBigDecimal.unsafeFrom(1)
        override val scale: Scale = Scale(refineMV[NonNegative](2))
      }
    }
    assertResult(CurrencyPairNotFound(notFoundCurrencyPair))(
      mapBasedUpdatedCommandFactory.by(notFoundCurrencyPair, AccountCurrency.Eur).left.getOrElse(thisTestShouldNotHaveArrivedHere)
    )
  }

  property("I cannot build an UpdatedCommand if the account currency is not registered. I get a nice error situation") {
    val notFoundAccountCurrency = new AccountCurrency {
      override val name: String = "x"
    }
    assertResult(UpdatedCommandNotFound(GbpUsd, notFoundAccountCurrency))(
      mapBasedUpdatedCommandFactory.by(GbpUsd, notFoundAccountCurrency).left.getOrElse(thisTestShouldNotHaveArrivedHere)
    )
  }

  property("EurUsdUpdatedForGbpAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(EurUsd, AccountCurrency.Gbp)

    actualValidUpdatedCommand shouldBe a[EurUsdUpdatedForGbpAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)
  }

  property("EurUsdUpdatedForUsdAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(EurUsd, AccountCurrency.Usd)

    actualValidUpdatedCommand shouldBe a[EurUsdUpdatedForUsdAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)
  }

  property("GbpUsdUpdatedForEurAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(CurrencyPair.GbpUsd, AccountCurrency.Eur)

    actualValidUpdatedCommand shouldBe a[GbpUsdUpdatedForEurAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)

  }

  property("GbpUsdUpdatedForGbpAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(GbpUsd, AccountCurrency.Gbp)

    actualValidUpdatedCommand shouldBe a[GbpUsdUpdatedForGbpAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)
  }

  property("GbpUsdUpdatedForUsdAccount is created correctly") {

    val (expectedUpdatingAccount, actualValidUpdatedCommand) = testerForCreationSuccessFull(GbpUsd, AccountCurrency.Usd)

    actualValidUpdatedCommand shouldBe a[GbpUsdUpdatedForUsdAccount]

    assertResult(expectedUpdatingAccount)(actualValidUpdatedCommand.updatingAccount)
  }

  def testerForCreationSuccessFull(currencyPair: CurrencyPair, accountCurrency: AccountCurrency): (UpdatingAccount, UpdatedCommand) = {

    val updatingAccount = anUpdatingAccountFor(currencyPair, accountCurrency).sample.get

    val actualValidUpdatedCommand = mapBasedUpdatedCommandFactory
      .by(toCurrencyPair(updatingAccount), updatingAccount.theAccountCurrency)
      .getOrElse(fail())(updatingAccount)
      .getOrElse(fail())

    (updatingAccount, actualValidUpdatedCommand)
  }

  def testerForCreationFailure(currencyPair: CurrencyPair, accountCurrency: AccountCurrency): (UpdatingAccount, UpdatedCommand) = {

    val updatingAccount = anUpdatingAccountFor(currencyPair, accountCurrency).sample.get

    val actualValidUpdatedCommand = mapBasedUpdatedCommandFactory
      .by(toCurrencyPair(updatingAccount), updatingAccount.theAccountCurrency)
      .getOrElse(fail())(updatingAccount)
      .getOrElse(fail())

    (updatingAccount, actualValidUpdatedCommand)
  }

  private def toCurrencyPair(updatingAccount: UpdatingAccount) = {
    updatingAccount.incomingUpdatedQuote.rates.currencyPair
  }

}
