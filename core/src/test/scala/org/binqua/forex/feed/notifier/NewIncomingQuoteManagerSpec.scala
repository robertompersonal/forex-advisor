package org.binqua.forex.feed.notifier

import akka.actor.testkit.typed.Effect.Spawned
import akka.actor.testkit.typed.scaladsl.BehaviorTestKit
import akka.actor.typed.scaladsl.Behaviors
import org.binqua.forex.advisor.model.AccountCurrency.{Eur, Gbp, Usd}
import org.binqua.forex.advisor.model.CurrencyPair.{EurUsd, GbpUsd}
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair}
import org.binqua.forex.feed.notifier
import org.binqua.forex.feed.notifier.collaborators.{DueToPipValue, DueToUpdatedQuoteDirectly, UpdatingAccount}
import org.binqua.forex.feed.notifier.publisher.MultipleAccountCurrenciesPublisher._
import org.binqua.forex.feed.reader.QuoteGen
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.util.ChildMaker.CHILD_MAKER
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.Gen
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.prop.TableDrivenPropertyChecks

class NewIncomingQuoteManagerSpec extends AnyFlatSpecLike with MockFactory with TestingFacilities with TableDrivenPropertyChecks {

  val quoteUpdatedNotifierMockedActorName = "quoteUpdatedNotifierMockedActor"

  private val quoteUpdatedNotifierMockedActor = Behaviors.receiveMessage[publisher.MultipleAccountCurrenciesPublisher.Command] { _ =>
    Behaviors.same
  }

  private val quoteUpdatedNotifierActorMaker: CHILD_MAKER[NewIncomingQuoteManager.Command, publisher.MultipleAccountCurrenciesPublisher.Command] = context => context.spawn(quoteUpdatedNotifierMockedActor, quoteUpdatedNotifierMockedActorName)

  val scenarios = org.scalatest.prop.Tables.Table(
    ("quotes already received", "the updated quote currency pair", "Accounts handled by the system", "Accounts to be notified", "reason"),
    (Nil, CurrencyPair.GbpUsd, List(Gbp), List(Gbp), "I can calculate 10 usd pip value in Gbp"),
    (Nil, CurrencyPair.GbpUsd, List(Gbp, Usd), List(Gbp, Usd), "I can calculate 10 usd pip value in Gbp and Usd"),
    (Nil, CurrencyPair.EurUsd, List(Gbp), Nil, "I cannot calculate 10 usd pip value in Gbp. I dont have GbpUsd or UsdGbp quote"),
    (Nil, CurrencyPair.Spx500, List(Eur, Gbp), Nil, "I cannot calculate usd pip value in Eur or Gbp. I dont have GbpUsd and EurUsd quote"),
    (Nil, CurrencyPair.EurUsd, List(Eur, Gbp, Usd), List(Eur, Usd), "I can calculate 10 usd pip value in Eur and Usd, but I cannot calculate usd pip value in Gbp because I dont have GbpUsd"),
    (Nil, CurrencyPair.Spx500, List(Eur, Gbp), Nil, "Spx500 is usd related so I dont know how much eur or gbp x pip"),
    (Nil, CurrencyPair.Spx500, List(Usd, Gbp), List(Usd), "Spx500 is usd related so I dont know how much gbp x pip but I know how many usd x pip"),
    (Nil, CurrencyPair.Us30, List(Eur, Gbp, Usd), List(Usd), "Us30 is usd related so I dont know how much eur or gbp x pip")
  )

  "Given no preexisting received updatedQuote then actor" should "notified quoteUpdatedNotifierActor with simple notifications" in {

    forAll(scenarios) {
      (
        _,
        theNewIncomingQuote,
        accountCurrenciesHandled,
        expAccountToBeNotified,
        _
      ) => {

        val testKit = BehaviorTestKit(
          notifier.NewIncomingQuoteManager(accountCurrenciesHandled, quoteUpdatedNotifierActorMaker, UpdatingAccount, Nil),
          Gen.alphaChar.toString
        )

        testKit.expectEffect(Spawned(quoteUpdatedNotifierMockedActor, quoteUpdatedNotifierMockedActorName))

        val theUpdatedQuote: Quote = QuoteGen.quotesOf(theNewIncomingQuote === _).sample.get

        testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(theUpdatedQuote))

        val quoteUpdatedNotifierMockedActorInBox = testKit.childInbox[Command]("quoteUpdatedNotifierMockedActor")

        val expUpdatingAccountDueToQuoteUpdated = expAccountToBeNotified.map(UpdatingAccount.create(theUpdatedQuote, _).getOrElse(thisTestShouldNotHaveArrivedHere)).zip(0 until expAccountToBeNotified.size)

        val actualReceivedMessages = quoteUpdatedNotifierMockedActorInBox.receiveAll()

        expAccountToBeNotified match {
          case Nil => assertResult(0, s"$actualReceivedMessages is wrong")(actualReceivedMessages.size)
          case _ =>
            val allReceivedMessages = actualReceivedMessages
            assertResult(1, s"I should have received only 1 message. $allReceivedMessages")(allReceivedMessages.size)
            val NotifyAccounts(actualEntries) = allReceivedMessages(0).asInstanceOf[NotifyAccounts]
            expUpdatingAccountDueToQuoteUpdated.zip(actualEntries).foreach(i => assertResult(i._1._1, clue = s"\nentry ${i._1._2} is wrong in:\n${actualEntries.mkString("\n")}")(i._2))
        }
      }
    }

  }

  "Given preexisting received updatedQuotes then QuoteUpdatedAccountsOrganiserActor" should "notified quoteUpdatedNotifierActor using previous received quotes because pip value may have changed" in {

    val testKit = BehaviorTestKit(
      notifier.NewIncomingQuoteManager(
        allAccountCurrencies = List(Gbp, Eur, Usd),
        multipleAccountCurrenciesPublisher = quoteUpdatedNotifierActorMaker,
        UpdatingAccount,
        CurrencyPair.values
      ),
      name = Gen.alphaChar.toString
    )

    testKit.expectEffect(Spawned(quoteUpdatedNotifierMockedActor, "quoteUpdatedNotifierMockedActor"))

    val aEurUsdUpdatedQuote: Quote = QuoteGen.quotesOf(EurUsd === _).sample.get

    testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(aEurUsdUpdatedQuote))

    val quoteUpdatedNotifierMockedActorInBox = testKit.childInbox[Command]("quoteUpdatedNotifierMockedActor")

    quoteUpdatedNotifierMockedActorInBox.receiveAll()

    val aGbpUsdUpdatedQuote: Quote = QuoteGen.quotesOf(GbpUsd === _).sample.get

    testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(aGbpUsdUpdatedQuote))

    val actualReceivedMessages = quoteUpdatedNotifierMockedActorInBox.receiveAll()

    assertResult(1, actualValueIsWrong(actualReceivedMessages))(actualReceivedMessages.size)

    actualReceivedMessages(0).asInstanceOf[NotifyAccounts] match {
      case nac: NotifyAccounts => nac.updatingAccount match {
        case l@List(m1, m2, m3, m4) =>
          assertResult(UpdatingAccount.create(aGbpUsdUpdatedQuote, AccountCurrency.Gbp).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m1)
          assertResult(UpdatingAccount.validate(aEurUsdUpdatedQuote, aGbpUsdUpdatedQuote, AccountCurrency.Gbp, DueToPipValue).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m2)
          assertResult(UpdatingAccount.validate(aGbpUsdUpdatedQuote, aEurUsdUpdatedQuote, AccountCurrency.Eur, DueToUpdatedQuoteDirectly).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m3)
          assertResult(UpdatingAccount.create(aGbpUsdUpdatedQuote, AccountCurrency.Usd).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m4)
        case s@List(_*) => failBecauseActualValueIsWrong("actualReceivedMessages", s.mkString("\n"))
        case somethingElse => failBecauseActualValueIsWrong("actualReceivedMessages", somethingElse)
      }
      case somethingElse => failBecauseActualValueIsWrong("actualReceivedMessages", somethingElse)
    }

  }

  "Actor state" should "be updated with last received QuoteUpdated" in {

    val testKit = BehaviorTestKit(
      notifier.NewIncomingQuoteManager(
        allAccountCurrencies = List(Gbp, Eur, Usd),
        multipleAccountCurrenciesPublisher = quoteUpdatedNotifierActorMaker,
        UpdatingAccount,
        CurrencyPair.values
      ),
      name = Gen.alphaChar.toString
    )

    testKit.expectEffect(Spawned(quoteUpdatedNotifierMockedActor, "quoteUpdatedNotifierMockedActor"))

    val aEurUsdUpdatedQuote: Quote = QuoteGen.quotesOf(EurUsd === _).sample.get

    testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(aEurUsdUpdatedQuote))

    val quoteUpdatedNotifierMockedActorInBox = testKit.childInbox[Command]("quoteUpdatedNotifierMockedActor")

    quoteUpdatedNotifierMockedActorInBox.receiveAll()

    testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(QuoteGen.quotesOf(GbpUsd === _).sample.get))

    quoteUpdatedNotifierMockedActorInBox.receiveAll()

    val latestGbpUsd: Quote = QuoteGen.quotesOf(GbpUsd === _).sample.get

    testKit.run(notifier.NewIncomingQuoteManager.QuoteUpdated(latestGbpUsd))

    val actualReceivedMessages = quoteUpdatedNotifierMockedActorInBox.receiveAll()

    assertResult(1, actualValueIsWrong(actualReceivedMessages))(actualReceivedMessages.size)

    actualReceivedMessages(0).asInstanceOf[NotifyAccounts] match {
      case nac: NotifyAccounts => nac.updatingAccount match {
        case l@List(m1, m2, m3, m4) =>
          assertResult(UpdatingAccount.create(latestGbpUsd, AccountCurrency.Gbp).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m1)
          assertResult(UpdatingAccount.validate(aEurUsdUpdatedQuote, latestGbpUsd, AccountCurrency.Gbp, DueToPipValue).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m2)
          assertResult(UpdatingAccount.validate(latestGbpUsd, aEurUsdUpdatedQuote, AccountCurrency.Eur, DueToUpdatedQuoteDirectly).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m3)
          assertResult(UpdatingAccount.create(latestGbpUsd, AccountCurrency.Usd).getOrElse(thisTestShouldNotHaveArrivedHere), actualValueIsWrong(l))(m4)
        case s@List(_*) => failBecauseActualValueIsWrong("actualReceivedMessages", s.mkString("\n"))
        case somethingElse => failBecauseActualValueIsWrong("actualReceivedMessages", somethingElse)
      }
      case somethingElse => failBecauseActualValueIsWrong("actualReceivedMessages", somethingElse)
    }

  }

}
