package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.testkit.typed.FishingOutcome
import akka.actor.testkit.typed.scaladsl.{FishingOutcomes, LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.config.ConfigFactory
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol._
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.core.{MakeItUnsafe, UnsafeConfigReader}
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Util, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Seconds, Span}
import org.scalatest.{Assertions, BeforeAndAfter, BeforeAndAfterEach, GivenWhenThen}

import java.io.File
import java.util.concurrent.CopyOnWriteArrayList
import scala.concurrent.duration._
import scala.sys.process.{Process, ProcessLogger}
import scala.util.{Failure, Success, Try}

class CustomisableSocketIOClientActorSpec
    extends ScalaTestWithActorTestKit(ConfigFactory.load("application-test"))
    with AnyFlatSpecLike
    with BeforeAndAfterEach
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation
    with Eventually
    with BeforeAndAfter {

  "Connect connects the actor to the SocketIO server. The actor" should "reply with Connecting. Then the actor can handle SubscribeTo messages" in new ASocketIOServerUpAndRunning
    with DefaultActorConfig
    with ASocketIOClientUnderTest {

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    replyToProbe.expectMessageType[Connecting.type]

    eventually(timeout(Span(3, Seconds)))(aSingletonSocketIOServer.lastSocketId.isDefined shouldBe true)

    private val lastSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.getOrElse(thisTestShouldNotHaveArrivedHere)

    aSingletonSocketIOServer.assertClientIsConnected(lastSocketId)

    replyToProbe.expectMessage(Connected(lastSocketId))

    replyToProbe.expectNoMessage(200.millis)

    socketIOClientUnderTest ! SubscribeTo(CurrencyPair.Spx500, lastSocketId, subscribeToRequester.ref, internalFeedProbe.ref)

    fishExactlyOneMessageAndIgnoreOthers(logActorProbe)(incomingMessage => {
      val LogProtocol.Info(message) = incomingMessage
      message shouldBe s"${CurrencyPair.Spx500} has been successfully subscribed to socket id ${lastSocketId.id}"
    })

    subscribeToRequester.expectMessage(Subscribed(result = true, CurrencyPair.Spx500, lastSocketId))

    subscribeToRequester.expectNoMessage(200.millis)

    private val collectedQuotesWithoutDuplicates: Set[Response] =
      internalFeedProbe.fishForMessage(1.seconds)(allCurrenciesQuotesCollector(numberOfEntriesToBeCollected = 3)).toSet

    collectedQuotesWithoutDuplicates.size shouldBe 3

    matchDistinctLogs(messages = s"Socket with id ${lastSocketId.id} disconnected. SocketIOClientActor Actor Terminated").expect(
      testKit.stop(socketIOClientUnderTest)
    )

    aSingletonSocketIOServer.assertClientIsDisconnected

  }

  "A SocketIOClientActor connected to the SocketIO server that never received a subscribeTo message" should "be stoppable and the socket disconnected from the server" in new ASocketIOServerUpAndRunning
    with ADefaultSocketIOClientUnderTest {

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    replyToProbe.fishForMessage(500.millis)({
      case Connected(socketId) =>
        aSingletonSocketIOServer.assertClientIsConnected(socketId)
        FishingOutcomes.complete
      case _ =>
        FishingOutcomes.continue
    })

    private val lastSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.getOrElse(thisTestShouldNotHaveArrivedHere)

    matchDistinctLogs(messages = s"Socket with id ${lastSocketId.id} disconnected. SocketIOClientActor Actor Terminated").expect(
      testKit.stop(socketIOClientUnderTest)
    )

    aSingletonSocketIOServer.assertClientIsDisconnected

  }

  "A SocketIOClientActor connected to the SocketIO server" should "treat InternalRetryToConnectToSocketIOServer as unhandled" in new ASocketIOServerUpAndRunning
    with ADefaultSocketIOClientUnderTest {

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    replyToProbe.fishForMessage(500.millis)({
      case Connected(socketId) =>
        aSingletonSocketIOServer.assertClientIsConnected(socketId)
        FishingOutcomes.complete
      case _ =>
        FishingOutcomes.continue
    })

    List(
      InternalRetryToConnectToSocketIOServer(1)
    ).foreach(
      BaseTestContext
        .assertIsUnhandled(socketIOClientUnderTest, _)(TheOnlyHandledMessages(InternalConnected, InternalConnectionErrorMustToReconnect, SubscribeTo))
    )

    testKit.stop(socketIOClientUnderTest)

    aSingletonSocketIOServer.assertClientIsDisconnected

  }

  "A SocketIOClientActor not yet connected" should "treat as unhandled InternalConnected, InternalConnectionError, InternalRetryToConnect and SubscribeTo commands" in new ADefaultSocketIOClientUnderTest {

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    List(
      SubscribeTo(CurrencyPair.Spx500, SocketId("1"), subscribeToRequester.ref, internalFeedProbe.ref),
      InternalConnected(createTestProbe().ref),
      InternalConnectionErrorMustToReconnect(1),
      InternalRetryToConnectToSocketIOServer(1)
    ).foreach(BaseTestContext.assertIsUnhandled(socketIOClientUnderTest, _)(TheOnlyHandledMessages(Connect)))

    deadWatcher.assertThatIsAliveAfter(1.second, socketIOClientUnderTest)

    testKit.stop(socketIOClientUnderTest)

    aSingletonSocketIOServer.stop()

  }

  "A SocketIOClientActor just created (not even connecting)" should "be stoppable" in new ASocketIOServerUpAndRunning with ADefaultSocketIOClientUnderTest {

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    deadWatcher.assertThatIsStillAlive(socketIOClientUnderTest, 500.millis)

    LoggingTestKit
      .info("SocketIOClientActor Actor terminated without connecting to SocketIOServer")
      .expect(
        testKit.stop(socketIOClientUnderTest)
      )

    deadWatcher.assertThatIsDead(socketIOClientUnderTest, 500.millis)

    testKit.stop(socketIOClientUnderTest)

    waitALittleBit(soThat("client can disconnect"))

    aSingletonSocketIOServer.stop()

    waitALittleBit(soThat("client can disconnect"))

  }

  "A connecting SocketIOClientActor" should "be stoppable" in new ADefaultSocketIOClientUnderTest {

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    replyToProbe.expectMessage(Connecting)

    deadWatcher.assertThatIsStillAlive(socketIOClientUnderTest, 500.millis)

    LoggingTestKit
      .info("SocketIOClientActor Actor Terminated")
      .expect(
        testKit.stop(socketIOClientUnderTest)
      )

    deadWatcher.assertThatIsDead(socketIOClientUnderTest, 500.millis)

    testKit.stop(socketIOClientUnderTest)

  }

  "Given logActor crashed, it" should "resume working" in new TestProbes {

    object UnderTestSocketIOClientActor extends CustomisableSocketIOClientActor with LogBehavior {
      override val logBehavior: Behavior[LogProtocol.Log] = Behaviors.receive((_, _) => {
        throw new IllegalArgumentException("I am going to crash")
      })
    }

    private val theConfigurationReader = testConfigFileButReturn(createConfigWithServerPort(Util.findRandomOpenPortOnAllLocalInterfaces()))

    val socketIOClientUnderTest = testKit.spawn(UnderTestSocketIOClientActor(theConfigurationReader, ResumableLogActor.defaultLogActorMaker))

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    LoggingTestKit
      .error("Supervisor ResumeSupervisor saw failure: I am going to crash")
      .withOccurrences(1)
      .expect {
        socketIOClientUnderTest ! Connect(replyToProbe.ref)
      }

    testKit.stop(socketIOClientUnderTest)

  }

  "Given a connected and subscribed socketIOClient, when the socketIO server goes down and goes up again, the socketIOClient" should "connect again and then notify the new socketId." +
    "Incoming subscription with old socketId will be considered failed and incoming subscription with new socketId will be handled correctly." in new ASocketIOServerUpAndRunning
    with TestProbes
    with ADefaultSocketIOClientUnderTest {

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    fishOneMessageAndIgnoreOthers(replyToProbe, 500.millis) { incomingMessage =>
      val Connected(socketId) = incomingMessage
      logActorProbe.expectMessage(LogProtocol.Info(s"connect with args [${socketId.id}]"))
      aSingletonSocketIOServer.assertClientIsConnected(socketId)
    }

    waitFor(100.millis, soThat("server start to emit some values"))

    val oldSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.getOrElse(thisTestShouldNotHaveArrivedHere)

    socketIOClientUnderTest ! SubscribeTo(CurrencyPair.Us30, oldSocketId, subscribeToRequester.ref, internalFeedProbe.ref)

    logActorProbe.expectMessage(LogProtocol.Info(s"Us30 has been successfully subscribed to socket id ${oldSocketId.id}"))

    aSingletonSocketIOServer.stop()

    logActorProbe.expectMessage(LogProtocol.Error("error with args [io.socket.engineio.client.EngineIOException: websocket error]"))
    logActorProbe.expectMessage(LogProtocol.Info("reconnect attempt with args [1]"))
    logActorProbe.expectMessage(LogProtocol.Info("reconnecting with args [1]"))
    logActorProbe.expectMessage(LogProtocol.Error("connect error with args [io.socket.engineio.client.EngineIOException: xhr poll error]"))

    waitFor(200.millis, soThat("only to collect some error messages"))

    aSingletonSocketIOServer.startNewSocketServer(serverPort)

    eventually(timeout(Span(3, Seconds)))(aSingletonSocketIOServer.lastSocketId.isDefined shouldBe true)

    val newSocketId = eventually {
      val newSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.getOrElse(thisTestShouldNotHaveArrivedHere)
      logActorProbe.expectMessage(LogProtocol.Info(s"connect with args [${newSocketId.id}]"))
      newSocketId
    }

    eventually {
      fishExactlyOneMessageAndIgnoreOthers(replyToProbe) { toBeMatched =>
        val Connected(`newSocketId`) = toBeMatched
        aSingletonSocketIOServer.assertClientIsConnected(newSocketId)
      }
    }

    LoggingTestKit
      .info(s"Ignoring subscription request for Us30 with old socket id ${oldSocketId.id}. New socket id ${newSocketId.id}")
      .withOccurrences(1)
      .expect {

        socketIOClientUnderTest ! SubscribeTo(CurrencyPair.Us30, oldSocketId, subscribeToRequester.ref, internalFeedProbe.ref)

        eventually(
          subscribeToRequester.expectMessage(Subscribed(result = false, CurrencyPair.Us30, socketId = oldSocketId))
        )

      }

    socketIOClientUnderTest ! SubscribeTo(CurrencyPair.Us30, newSocketId, subscribeToRequester.ref, internalFeedProbe.ref)

    val quotesWithoutDuplicateEntries: Seq[Response] = {
      internalFeedProbe.fishForMessage(1.seconds)(allCurrenciesQuotesCollector(numberOfEntriesToBeCollected = 4))
    }

    quotesWithoutDuplicateEntries should contain(FeedContent(s"quote for socket id ${newSocketId.id} US30 number 1"))
    quotesWithoutDuplicateEntries should contain(FeedContent(s"quote for socket id ${newSocketId.id} US30 number 2"))
    quotesWithoutDuplicateEntries should contain(FeedContent(s"quote for socket id ${newSocketId.id} US30 number 3"))

    quotesWithoutDuplicateEntries.toSet.size shouldBe quotesWithoutDuplicateEntries.size

    deadWatcher.assertThatIsStillAlive(socketIOClientUnderTest, 500.millis)

    testKit.stop(socketIOClientUnderTest)

    aSingletonSocketIOServer.assertClientIsDisconnected

  }

  "Given a socketIOClient received error event from serverIO as first response without ever connect (due for example to authentication error), it" should "try again because server treat temporary problem in this way" in
    new ASocketIOServerThatKeepRespondingWithError with TestProbes with ACustomSocketIOClientUnderTest {

      val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

      socketIOClientUnderTest ! Connect(replyToProbe.ref)

      logActorProbe.expectMessage(LogProtocol.Error("error with args [Authentication error]"))
      logActorProbe.expectMessage(LogProtocol.Info("Retrying to connect in 300 milliseconds"))
      logActorProbe.expectMessage(LogProtocol.Info("Retrying to connect now. Attempt number 1"))
      logActorProbe.expectMessage(LogProtocol.Error("error with args [Authentication error]"))
      logActorProbe.expectMessage(LogProtocol.Info("Retrying to connect in 300 milliseconds"))
      logActorProbe.expectMessage(LogProtocol.Info("Retrying to connect now. Attempt number 2"))

      eventually(timeout(Span(3, Seconds))) {
        val newSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.getOrElse(thisTestShouldNotHaveArrivedHere)
        logActorProbe.expectMessage(LogProtocol.Info(s"connect with args [${newSocketId.id}]"))
      }

      deadWatcher.assertThatIsStillAlive(socketIOClientUnderTest, 500.millis)

      testKit.stop(socketIOClientUnderTest)

    }

  "connect timeout, reconnect, reconnecting, reconnect attempt, reconnect failed" should "be logged correctly" in new TestProbes
    with ADefaultSocketIOClientUnderTest {

    aSingletonSocketIOServer.startNewSocketServerByFileName("aServerThatAfterConnectEmitAllErrorMessage", serverPort)

    val deadWatcher = DeadWatcher(testKit, socketIOClientUnderTest)

    socketIOClientUnderTest ! Connect(replyToProbe.ref)

    replyToProbe.expectMessageType[Connecting.type]

    eventually(timeout(Span(3, Seconds)))(aSingletonSocketIOServer.lastSocketId.isDefined shouldBe true)

    private val lastSocketId: SocketId = aSingletonSocketIOServer.lastSocketId.get

    aSingletonSocketIOServer.assertClientIsConnected(lastSocketId)

    replyToProbe.expectMessage(Connected(lastSocketId))

    replyToProbe.expectNoMessage()

    logActorProbe.expectMessage(LogProtocol.Info(s"connect with args [${lastSocketId.id}]"))

    Array("connect timeout", "reconnect", "reconnecting", "reconnect attempt", "reconnect failed").foreach { m =>
      logActorProbe.expectMessage(LogProtocol.Info(s"$m with args [1,2]"))
      logActorProbe.expectMessage(LogProtocol.Info(s"$m with args []"))
      logActorProbe.expectMessage(LogProtocol.Info(s"$m with no args"))
    }

    deadWatcher.assertThatIsStillAlive(socketIOClientUnderTest, 500.millis)

    testKit.stop(socketIOClientUnderTest)

    aSingletonSocketIOServer.assertClientIsDisconnected

  }

  "the actor" should "log its configuration when spawn" in new TestProbes with ADefaultSocketIOClientUnderTest {

    object underTest extends CustomisableSocketIOClientActor with LogBehavior {
      override val logBehavior: Behavior[LogProtocol.Log] = logAndProbeBehavior
    }

    matchDistinctLogs(messages = s"Using following config for connect to socketIO: loginToken theLoggingToken connectionUrl http://localhost:$serverPort")
      .expect(
        testKit.spawn(underTest(configReader, ResumableLogActor.defaultLogActorMaker))
      )

  }

  def allCurrenciesQuotesCollector[M](numberOfEntriesToBeCollected: Int): M => FishingOutcome = {
    var counter: Int = 0
    m => {
      m match {
        case FeedContent(_) =>
          counter += 1
          if (counter >= numberOfEntriesToBeCollected)
            FishingOutcomes.complete
          else
            FishingOutcomes.continue
        case _ => FishingOutcomes.fail("this should not happen")
      }
    }
  }

  trait ADefaultSocketIOClientUnderTest extends ASocketIOClientUnderTest with DefaultActorConfig with TestProbes {}

  trait ACustomSocketIOClientUnderTest extends ASocketIOClientUnderTest with CustomActorConfig with TestProbes {}

  trait ASocketIOClientUnderTest extends ActorConfig with TestProbes {

    val logActorProbe = createTestProbe[LogProtocol.Log]()

    val logAndProbeBehavior = Behaviors.monitor[LogProtocol.Log](
      logActorProbe.ref,
      Behaviors.receive((context, message) => {
        context.log.info(message.toString)
        Behaviors.same
      })
    )

    object UnderTestSocketIOClientActor extends CustomisableSocketIOClientActor with LogBehavior {
      override val logBehavior: Behavior[LogProtocol.Log] = logAndProbeBehavior
    }

    lazy val socketIOClientUnderTest = testKit.spawn(UnderTestSocketIOClientActor(configReader, logActorFactory = ResumableLogActor.defaultLogActorMaker))

  }

  trait ActorConfig {

    val retryToConnectInterval: FiniteDuration

    lazy val serverPort: Int = Util.findRandomOpenPortOnAllLocalInterfaces()

    lazy val configReader: UnsafeConfigReader[Config] = {
      val config = createConfigWithServerPort(serverPort)
      testConfigFileButReturn(Config.validated(config.connectionUrl, config.loginToken, retryToConnectInterval).unsafe)
    }

  }

  def testConfigFileButReturn(returnThisConfig: Config): UnsafeConfigReader[Config] =
    akkaConfig => {

      val actualConfig: Config = ConfigValidator(akkaConfig).getOrElse(thisTestShouldNotHaveArrivedHere)

      actualConfig.connectionUrl shouldBe "thisIsTheConnectionUrl"
      actualConfig.loginToken shouldBe "thisIsAFakeToken"
      actualConfig.retryToConnectInterval shouldBe 30.seconds

      returnThisConfig
    }

  trait CustomActorConfig extends ActorConfig {

    lazy val retryToConnectInterval: FiniteDuration = 300.millis

  }

  trait DefaultActorConfig extends ActorConfig {

    lazy val retryToConnectInterval: FiniteDuration = 15.seconds

  }

  trait TestProbes {

    val replyToProbe: TestProbe[Response] = testKit.createTestProbe[Response]("replyTo")

    val subscribeToRequester = testKit.createTestProbe[Response]("subscribeToRequester")

    val internalFeedProbe: TestProbe[Response] = testKit.createTestProbe[Response]("internalFeedActor")

  }

  class ASocketIOServerUpAndRunning {

    this: ActorConfig =>

    aSingletonSocketIOServer.stop()

    aSingletonSocketIOServer.startNewSocketServer(serverPort)
  }

  class ASocketIOServerThatKeepRespondingWithError {

    this: ActorConfig =>

    aSingletonSocketIOServer.stop()

    aSingletonSocketIOServer.startNewSocketServerByFileName("aServerThatRefuseConnection3TimesBeforeConnectTheClient", serverPort)
  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      aSingletonSocketIOServer.stop()
    }
  }

  before {
    Try(
      Process(Seq("npm", "--version"), None, scala.sys.env.toSeq: _*)
        .run(ProcessLogger(out => println(s"npm version $out is installed "), error => println(error)))
    ) match {
      case Failure(exception) => throw new IllegalStateException("Ops!! it looks like npm is not installed. I need npm and node to run this test", exception)
      case Success(process)   => process.exitValue()
    }

    Try(
      Process(Seq("node", "--version"), None, scala.sys.env.toSeq: _*)
        .run(ProcessLogger(out => println(s"node version $out is installed "), error => println(error)))
    ) match {
      case Failure(exception) => throw new IllegalStateException("Ops!! it looks like node is not installed. I need npm and node to run this test", exception)
      case Success(process)   => process.exitValue()
    }

    Try(
      Process(Seq("npm", "install"), Some(new File(aSingletonSocketIOServer.nodeServerSourceLocation)), scala.sys.env.toSeq: _*)
        .run(ProcessLogger(out => println(s"node version $out is installed "), error => println(error)))
    ) match {
      case Failure(exception) => throw new IllegalStateException("Ops!!! installing npm did not succeeded", exception)
      case Success(process) => {
        println("Installing npm packages!!.... it will take a little bit if it is the first time")
        process.exitValue()
      }
    }

  }

  private def createConfigWithServerPort(serverPort: Int) =
    Config.validated(connectionUrl = s"http://localhost:$serverPort", loginToken = "theLoggingToken", retryToConnectInterval = 15.seconds).unsafe

}

object aSingletonSocketIOServer extends Eventually {

  import scala.jdk.CollectionConverters._

  val nodeServerSourceLocation = "./src/test/resources"

  private val serverLogMessages: CopyOnWriteArrayList[String] = new java.util.concurrent.CopyOnWriteArrayList[String]()

  private var serverSocket: Option[Process] = None

  def stop(): Unit = {

    serverSocket.foreach(ss => {
      ss.destroy()
      eventually(!ss.isAlive())
      ss.exitValue()
    })
    serverLogMessages.clear()
    serverSocket = None
  }

  import org.scalatest.Assertions._

  def assertClientIsDisconnected: Any =
    eventually(timeout(Span(3, Seconds)))(
      assertResult(true, "socket should be disconnected ")(serverLogMessages.asScala.exists(line => line.contains("socket.io:socket got disconnect packet")))
    )

  def assertClientIsConnected(socketId: SocketId): Unit = {
    val expectedLog = s"handleUpgrade actual.request.url=/socket.io/?access_token=theLoggingToken&EIO=3&transport=websocket&sid=${socketId.id}"
    eventually(
      Assertions.assertResult(true, s"socket server log should contains $expectedLog with the same id $socketId sent to the client")(
        serverLogMessages.contains(expectedLog)
      )
    )
  }

  def lastSocketId: Option[SocketId] = {
    import scala.jdk.CollectionConverters._
    val toBeFound = "handleUpgrade actual.request.url=/socket.io/?access_token=theLoggingToken&EIO=3&transport=websocket&sid="

    def toASocketId(connectedEntries: List[String]): Option[SocketId] =
      connectedEntries.reverse match {
        case Nil       => None
        case head :: _ => Some(SocketId(head.replace(toBeFound, "")))
      }

    toASocketId(serverLogMessages.asScala.toList.filter(line => {
      line.startsWith(toBeFound)
    }))
  }

  def assertIsRunning(expectedServerPort: Int) = {
    eventually(timeout(Span(3, Seconds)))(
      Assertions.assertResult(true, "socket server is not running")(serverLogMessages.contains(s"Socket.IO server listening on port $expectedServerPort"))
    )
  }

  def add(line: String) = serverLogMessages.add(line)

  def startNewSocketServer(serverPort: Int): Option[Process] = startNewSocketServerByFileName(nodeJsServerName = "server", serverPort: Int)

  def startNewSocketServerByFileName(nodeJsServerName: String, serverPort: Int): Option[Process] = {

    serverSocket.foreach(ss => {
      ss.destroy()
      eventually(!ss.isAlive())
      ss.exitValue()
    })

    serverLogMessages.clear()

    serverSocket = Some(
      Process(Seq("node", s"$nodeServerSourceLocation/$nodeJsServerName.js", serverPort.toString), None, (scala.sys.env + ("DEBUG" -> "socket.io:*")).toSeq: _*)
        .run(
          ProcessLogger(
            line => {
              serverLogMessages.add(line)
              println(s"SERVER OUTPUT: $line")
            },
            line => {
              serverLogMessages.add(line)
              println(s"SERVER ERROR: $line")
            }
          )
        )
    )

    assertIsRunning(serverPort)

    serverSocket
  }

}
