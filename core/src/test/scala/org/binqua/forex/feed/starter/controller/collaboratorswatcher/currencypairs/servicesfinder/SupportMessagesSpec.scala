package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey
import akka.actor.typed.scaladsl.Behaviors
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class SupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "Log text for allServicesNotAvailableYet" should "be implemented" in {
    SupportMessagesImpl.allServicesNotAvailableYet() shouldBe "All required services are not available yet. Give me some time to find them"
  }

  "Log text for httpClientSubscriberServiceDiscoverSubscribed" should "be implemented" in {
    SupportMessagesImpl.httpClientSubscriberServiceDiscoverySubscribed() shouldBe "Subscribed to httpClientSubscriber service discovery"
  }

  "Log text for internalFeedServiceDiscoverySubscribed" should "be implemented" in {
    SupportMessagesImpl.internalFeedServiceDiscoverySubscribed() shouldBe "Subscribed to internalFeedService service discovery"
  }

  "Log text for serviceDiscovered" should "be implemented" in {
    val name = "example"
    val ref: ActorRef[String] = spawn(Behaviors.ignore[String], name)

    SupportMessagesImpl.serviceDiscovered(ref) shouldBe s"Service $name ($ref) has been discovered"
  }

  "Log text for socketIOClientServiceDiscoverySubscribed" should "be implemented" in {
    SupportMessagesImpl.socketIOClientServiceDiscoverySubscribed() shouldBe "Subscribed to socketIOClientService service discovery"
  }

  "Log text for tooManyServices" should "be implemented" in {
    val ref1: ActorRef[String] = spawn(Behaviors.ignore[String], "1")
    val ref2: ActorRef[String] = spawn(Behaviors.ignore[String], "2")
    val serviceKey: ServiceKey[String] = ServiceKey[String]("keyId")

    val refs = List(ref1, ref2)

    SupportMessagesImpl.tooManyServices(serviceKey, List(ref1, ref2)) shouldBe s"Too many actors services $refs with same key ${serviceKey.id}. Maybe some service has not been deregister yet. I will wait for other receptionist events until there is only one service x key"
  }


}
