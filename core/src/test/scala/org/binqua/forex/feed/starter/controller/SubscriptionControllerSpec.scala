package org.binqua.forex.feed.starter.controller

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.SubscriptionControllerProtocol.{WrappedCollaboratorsWatcherResponse, _}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.CollaboratorsWatcherProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.Moments._
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration.{FiniteDuration, _}

class SubscriptionControllerSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "On receiving Start, the actor" should "spawn a child and start the subscription" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! Start(underTestClientProbe.ref, socketId)

      underTestClientProbe.expectMessage(ExecutionStarted(socketId))

      assertNumberOfActorMadeWithNamePrefixedBy(collaboratorsWatcherContext.name)(_ shouldBe 1)(because("spawn a child for every start message"))

      testKit.stop(underTest)

    }

  "Once received Start, the actor" should "handle only PrivateCheckSubscriptionIsStillRunning and WrappedCollaboratorsWatcherResponse" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val handledMessages = TheOnlyHandledMessages(
        PrivateCheckSubscriptionIsStillRunning(socketId),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(socketId))
      )

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      underTest ! Start(underTestClientProbe.ref, socketId)

      List(
        Start(underTestClientProbe.ref, socketId),
        PrivateCheckSubscriptionIsStillRunning(SocketId(randomString)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(SocketId(randomString))),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(SocketId(randomString)))
      ).foreach(assertIsUnhandled(underTest, _)(handledMessages))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Start" should "be the only handled message of a newly spawn actor" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val underTest = spawnInstanceUnderTest.withDefaultParameters()

      List(
        PrivateCheckSubscriptionIsStillRunning(SocketId("12")),
        WrappedCollaboratorsWatcherResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Start)))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Once Started the actor" should "check that the collaborator send periodically SubscriptionRunning otherwise signal ExecutionFailed to its parent" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(heartBeatInterval)

      val underTest = spawnInstanceUnderTest.withSubscriptionRunningTimeout(heartBeatInterval)

      matchDistinctLogs(
        messages = justAnIdeaLogMessages.running(heartBeatChecksCounter = 1),
        justAnIdeaLogMessages.running(heartBeatChecksCounter = 2),
        justAnIdeaLogMessages.heartBeatMissing()
      ).expect {

        underTest ! Start(underTestClientProbe.ref, socketId)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionStarted(socketId))

        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

        underTestClientProbe.expectNoMessage()

        manualClock.timeAdvances(JustAfterThirdIntervalHasExpired)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionFailed(socketId))

        unhandledMessageSubscriber.expectNoMessage()

      }

      List(
        Start(underTestClientProbe.ref, socketId),
        PrivateCheckSubscriptionIsStillRunning(SocketId("12")),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId)),
        WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(socketId)),
        WrappedCollaboratorsWatcherResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages.NoMessagesBecauseIAmWaitingToBeStopped))

      unhandledMessageSubscriber.expectNoMessage()

      testKit.stop(underTest)

    }

  "Once Started the actor" should "check that the collaborator sends periodically SubscriptionRunning. Once it receives SubscriptionDone it signals to its parent ExecutionCompleted and terminate itself" in
    new TestContext(CollaboratorsWatcherContext.thatIgnoreAnyMessage(), SupportMessagesContext.justForAnIdea, underTestNaming) {

      val manualClock = manualClockBuilder.withInterval(heartBeatInterval)

      matchDistinctLogs(
        messages = justAnIdeaLogMessages.running(heartBeatChecksCounter = 1),
        justAnIdeaLogMessages.running(heartBeatChecksCounter = 2),
        justAnIdeaLogMessages.completed()
      ).expect {

        val underTest = spawnInstanceUnderTest.withSubscriptionRunningTimeout(heartBeatInterval)

        underTest ! Start(underTestClientProbe.ref, socketId)

        underTestClientProbe.expectMessage(SubscriptionControllerProtocol.ExecutionStarted(socketId))

        manualClock.timeAdvances(JustBeforeFirstIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterFirstIntervalHasExpired)

        manualClock.timeAdvances(JustBeforeSecondIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionRunning(socketId))

        manualClock.timeAdvances(JustAfterSecondIntervalHasExpired)

        underTestClientProbe.expectNoMessage()

        manualClock.timeAdvances(JustBeforeThirdIntervalExpires)

        underTest ! WrappedCollaboratorsWatcherResponse(CollaboratorsWatcherProtocol.SubscriptionDone(socketId))

        unhandledMessageSubscriber.expectNoMessage()

        collaboratorsWatcherContext.probe.expectTerminated(underTest)

        testKit.stop(underTest)
      }

    }

  object CollaboratorsWatcherContext {

    val actorPrefixName = "collaboratorsWatcher"

    trait Base extends ActorCollaboratorTestContext[Message, CollaboratorsWatcherProtocol.Start] {

      override val name: String = CollaboratorsWatcherContext.actorPrefixName

      override val probe: TestProbe[CollaboratorsWatcherProtocol.Start] = createTestProbe()

      override def behavior: Behavior[CollaboratorsWatcherProtocol.Start]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, CollaboratorsWatcherProtocol.Start] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[CollaboratorsWatcherProtocol.Start] = throw new IllegalAccessException("Not used in this case")
    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[CollaboratorsWatcherProtocol.Start] = Behaviors.ignore

    }

  }

  object SupportMessagesContext {

    val justForAnIdea: (SocketId, FiniteDuration) => SupportMessages = (socketId, timeout) =>
      new SupportMessages {

        override def running(hearBeatChecksCounter: Int): String = s"running $socketId $hearBeatChecksCounter"

        override def heartBeatMissing(): String = s"heartBeatMissing $socketId $timeout"

        override def completed(): String = s"completed $socketId"
      }

  }

  case class TestContext(
      collaboratorsWatcherContext: ActorCollaboratorTestContext[Message, CollaboratorsWatcherProtocol.Start],
      justForAnIdeaSupportMessageMaker: (SocketId, FiniteDuration) => SupportMessages,
      underTestNaming: UnderTestNaming
  ) extends ManualTimeBaseTestContext(underTestNaming) {

    val socketId = SocketId("1234")

    val heartBeatInterval = 500.millis

    val underTestClientProbe = createTestProbe[Response]()

    def lastChildSpawned() = eventually(actorsWatchers.lastCreatedInstanceOf[CollaboratorsWatcherProtocol.Command](CollaboratorsWatcherContext.actorPrefixName))

    val justAnIdeaLogMessages = justForAnIdeaSupportMessageMaker(socketId, heartBeatInterval)

    object spawnInstanceUnderTest {

      private def spawnInstanceUnderTest(heartBeatInterval: FiniteDuration, name: String) =
        spawn(
          SubscriptionController(
            heartBeatInterval = heartBeatInterval,
            collaboratorsWatcherChildMaker = collaboratorsWatcherContext.childMakerFactory(actorsWatchers)(),
            supportMessagesMaker = justForAnIdeaSupportMessageMaker
          ),
          name
        )

      def withDefaultParameters = () => spawnInstanceUnderTest(heartBeatInterval, underTestNaming.next())

      def withSubscriptionRunningTimeout(heartBeatInterval: FiniteDuration) =
        spawnInstanceUnderTest(heartBeatInterval, underTestNaming.next())

    }

  }

}
