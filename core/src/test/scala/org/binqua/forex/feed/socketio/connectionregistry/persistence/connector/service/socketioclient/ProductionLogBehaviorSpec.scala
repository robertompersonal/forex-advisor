package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.util.PreciseLoggingTestKit._
import org.binqua.forex.util.{AkkaTestingFacilities, PreciseLoggingTestKit, SmartMatcher}
import org.scalatest.wordspec.AnyWordSpecLike

class ProductionLogBehaviorSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike with AkkaTestingFacilities {

  "A ProductionLogBehavior" should {

    val messageToBeSent = "message"

    "log the right info " + messageToBeSent in {

      val actor = testKit.spawn(new ProductionLogBehavior {}.logBehavior, randomString)

      PreciseLoggingTestKit.expectExactlyTheFollowingSeqOfMessages(messageToBeSent).whileRunning {
        actor ! LogProtocol.Info(messageToBeSent)
      }
    }

    "log the right error " + messageToBeSent in {
      val actor = testKit.spawn(new ProductionLogBehavior {}.logBehavior, randomString)
      PreciseLoggingTestKit.expectExactlyTheFollowingSeqOfMessages(SmartMatcher.errorLog.equal(messageToBeSent)).whileRunning {
        actor ! LogProtocol.Error(messageToBeSent)
      }
    }
  }

}
