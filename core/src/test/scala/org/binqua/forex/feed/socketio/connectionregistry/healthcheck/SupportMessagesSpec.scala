package org.binqua.forex.feed.socketio.connectionregistry.healthcheck

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.util.core.MakeItUnsafe
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration._

class SupportMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  "Log text for cassandraIsHealthy" should "be implemented" in new TestContext() {

    underTest.cassandraIsHealthy() should be("Cassandra located at host:2020 with local datacenter datacenter1 has all tables akka needs")

  }

  "Log text for cassandraNotInitYet" should "be implemented" in new TestContext() {

    underTest.cassandraNotInitYet() should be(
      "Cassandra located at host:2020 with local datacenter datacenter1 does not have all tables akka needs yet. Retry in 1 second"
    )

  }

  case class TestContext() {

    val underTest: SupportMessages = SimpleSupportMessagesFactory.createSupportMessages(Config.validated("host", 2020, "datacenter1", 1.second).unsafe)

  }

}
