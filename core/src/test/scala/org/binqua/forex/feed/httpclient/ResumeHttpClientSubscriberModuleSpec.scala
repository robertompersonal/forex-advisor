package org.binqua.forex.feed.httpclient

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit}
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.util.AkkaTestingFacilities
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._

class ResumeHttpClientSubscriberModuleSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with Matchers with AkkaTestingFacilities {

  "CustomizableSupervisedHttpSubscriberModule" should "return a behavior that resume the actor for any Exception because the actor is stateless" in {

    trait CrashableHttpSubscriber extends HttpClientSubscriberModule {

      def httpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo] = Behaviors.receiveMessage(_ => throw new Exception("crash"))

    }

    val underTest = testKit.spawn(new ResumeHttpSubscriberModule with CrashableHttpSubscriber {}.supervisedHttpClientSubscriber())

    val deadWatcher = DeadWatcher(testKit, underTest)

    LoggingTestKit
      .error("Supervisor ResumeSupervisor saw failure: crash")
      .withOccurrences(1)
      .expect {
        underTest ! HttpClientSubscriberProtocol.SubscribeTo(CurrencyPair.Us30, SocketId("1"), createTestProbe[HttpClientSubscriberProtocol.Response]().ref)
      }

    deadWatcher.assertThatIsAliveAfter(1.second, underTest)

  }

}
