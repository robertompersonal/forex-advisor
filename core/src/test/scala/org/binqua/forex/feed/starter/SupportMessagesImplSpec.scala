package org.binqua.forex.feed.starter

import cats.syntax.option._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration._

class SupportMessagesImplSpec extends AnyFlatSpec {

  "newSocketIdReceived" should "be implemented for old socket id none" in {
    SupportMessagesImpl.newSocketIdReceived(SocketId("1"), None) shouldBe "First socket id 1 received"
  }

  "newSocketIdReceived" should "be implemented for an old socket id defined" in {
    SupportMessagesImpl.newSocketIdReceived(
      mostRecentSocketId = SocketId("2"),
      maybeAnOlderSocketId = SocketId("1").some
    ) shouldBe "New socket id 2 received. It replaces socket if 1"
  }

  "registrationCompleted" should "be implemented" in {
    SupportMessagesImpl.registrationCompleted() shouldBe "Registration completed. Ready to start a subscription as soon as a socket Id is available"
  }

  "registrationStarted" should "be implemented" in {
    SupportMessagesImpl.registrationStarted(registrationAttempt = 1) shouldBe "Registration started. Attempt number 1"
  }

  "registrationTimeout" should "be implemented" in {
    SupportMessagesImpl.registrationTimeout(
      attempt = 1,
      registrationTimeout = 1.second
    ) shouldBe s"Registration attempt number 1 times out after ${1.second}. I am going to retry now."
  }

  "subscriptionExecutionFailed" should "be implemented" in {
    SupportMessagesImpl.subscriptionExecutionFailed(SocketId("2")) shouldBe s"Subscription execution for socket id 2 failed. I am going to restart it."
  }

  "subscriptionExecutorFailed" should "be implemented" in {
    SupportMessagesImpl.subscriptionExecutorFailed(
      SocketId("2")
    ) shouldBe s"Subscription executor child for socket id 2 crashed. I am going to start a new subscription."
  }

  "subscriptionExecutionCompleted" should "be implemented" in {
    SupportMessagesImpl.subscriptionExecutionCompleted(SocketId("2")) shouldBe s"Subscription execution for socket id 2 completed successfully."
  }

  "subscriptionExecutionStarted" should "be implemented" in {
    SupportMessagesImpl.subscriptionExecutionStarted(SocketId("2")) shouldBe s"Subscription execution for socket id 2 started."
  }

  "subscriptionStarted" should "be implemented" in {
    SupportMessagesImpl.subscriptionStarted() shouldBe "Subscription started."
  }

}
