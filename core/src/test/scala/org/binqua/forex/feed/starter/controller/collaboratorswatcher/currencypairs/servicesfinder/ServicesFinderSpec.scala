package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.servicesfinder

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration._
import scala.util.Random

class ServicesFinderSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "the actor" should "logs subscription and discovery of the services" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessage) {

    val underTest = matchDistinctLogs(messages =
      justForAnIdeaSupportMessage.internalFeedServiceDiscoverySubscribed(),
      justForAnIdeaSupportMessage.httpClientSubscriberServiceDiscoverySubscribed(),
      justForAnIdeaSupportMessage.socketIOClientServiceDiscoverySubscribed(),
    ).expect {
      spawn(ServicesFinder(justForAnIdeaSupportMessage), underTestNaming.next())
    }

    matchDistinctLogs(messages =
      justForAnIdeaSupportMessage.allServicesNotAvailableYet(),
      justForAnIdeaSupportMessage.serviceDiscovered(feedContentProbe.ref),
      justForAnIdeaSupportMessage.serviceDiscovered(httpClientSubscriberServiceProbe.ref),
      justForAnIdeaSupportMessage.serviceDiscovered(socketIOClientProbe.ref),
    ).expect {

      deadWatcher.watch(underTest)

      underTest ! ServicesFinderProtocol.GetServiceReferences(underTestClientProbe.ref)

      underTestClientProbe.expectNoMessage()

      testKit.system.receptionist ! Receptionist.Register(ServicesKeys.InternalFeedServiceKey, feedContentProbe.ref)

      underTestClientProbe.expectNoMessage()

      testKit.system.receptionist ! Receptionist.Register(ServicesKeys.HttpClientSubscriberServiceKey, httpClientSubscriberServiceProbe.ref)

      underTestClientProbe.expectNoMessage()

      deadWatcher.assertThatIsAliveAfter(100.millis, underTest)

      testKit.system.receptionist ! Receptionist.Register(ServicesKeys.SocketIOClientServiceKey, socketIOClientProbe.ref)

      waitALittleBit(soThat("services can be register"))

      underTestClientProbe.expectMessage(ServicesFinderProtocol.ServicesReferences(feedContentProbe.ref, httpClientSubscriberServiceProbe.ref, socketIOClientProbe.ref))

    }

    underTestClientProbe.expectTerminated(underTest)

    deregisterAllServices()

  }

  "the actor" should "do the right things independently from services order availability" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessage) {

    (1 to 20).foreach { _ =>

      val underTest = spawn(ServicesFinder(justForAnIdeaSupportMessage), underTestNaming.next())

      deadWatcher.watch(underTest)

      val steps: Seq[() => Unit] = List[() => Unit](
        () => underTest ! ServicesFinderProtocol.GetServiceReferences(underTestClientProbe.ref),
        () => testKit.system.receptionist ! Receptionist.Register(ServicesKeys.InternalFeedServiceKey, feedContentProbe.ref),
        () => testKit.system.receptionist ! Receptionist.Register(ServicesKeys.HttpClientSubscriberServiceKey, httpClientSubscriberServiceProbe.ref),
        () => testKit.system.receptionist ! Receptionist.Register(ServicesKeys.SocketIOClientServiceKey, socketIOClientProbe.ref)
      )

      Random.shuffle(steps)
        .zipWithIndex
        .foreach(input => {
          val (sendAMessage, stepIndex: Int) = input
          sendAMessage()
          if (stepIndex <= 2) {
            underTestClientProbe.expectNoMessage()
            deadWatcher.assertThatIsAliveAfter(100.millis, underTest)
          }
        })

      waitALittleBit(soThat("services can be register"))

      underTestClientProbe.expectMessage(ServicesFinderProtocol.ServicesReferences(feedContentProbe.ref, httpClientSubscriberServiceProbe.ref, socketIOClientProbe.ref))

      underTestClientProbe.expectTerminated(underTest)

      testKit.stop(underTest)

      deregisterAllServices()
    }

  }

  "given there are more services of the same type available, the actor" should "wait, because they could be associated to old service actors that are going to die soon and it means they will deregister the service" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessage) {

      val anOldFeedContentServiceProbe = createTestProbe[SocketIOClientProtocol.FeedContent]("a")
      val anotherOldFeedContentServiceProbe = createTestProbe[SocketIOClientProtocol.FeedContent]("b")
      val feedContentServices = List(feedContentProbe.ref, anOldFeedContentServiceProbe.ref, anotherOldFeedContentServiceProbe.ref)

      feedContentServices.foreach(testKit.system.receptionist ! Receptionist.Register(ServicesKeys.InternalFeedServiceKey, _))

      val anOldHttpClientSubscriberProbe = createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]("a")
      val anotherOldHttpClientSubscriberProbe = createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]("b")
      val httpClientSubscriberServices = List(httpClientSubscriberServiceProbe.ref, anOldHttpClientSubscriberProbe.ref, anotherOldHttpClientSubscriberProbe.ref)

      httpClientSubscriberServices.foreach(testKit.system.receptionist ! Receptionist.Register(ServicesKeys.HttpClientSubscriberServiceKey, _))

      val anOldSocketIOClientProbe = createTestProbe[SocketIOClientProtocol.Command]("a")
      val anotherOldSocketIOClientProbe = createTestProbe[SocketIOClientProtocol.Command]("b")
      val socketIOClientServices = List(socketIOClientProbe.ref, anOldSocketIOClientProbe.ref, anotherOldSocketIOClientProbe.ref)
      socketIOClientServices.foreach(testKit.system.receptionist ! Receptionist.Register(ServicesKeys.SocketIOClientServiceKey, _))

      val underTest = matchDistinctLogs(messages =
        justForAnIdeaSupportMessage.tooManyServices(ServicesKeys.HttpClientSubscriberServiceKey, httpClientSubscriberServices.sortBy(_.path.name)),
        justForAnIdeaSupportMessage.tooManyServices(ServicesKeys.InternalFeedServiceKey, feedContentServices.sortBy(_.path.name)),
        justForAnIdeaSupportMessage.tooManyServices(ServicesKeys.SocketIOClientServiceKey, socketIOClientServices.sortBy(_.path.name))
      ).expect {
        spawn(ServicesFinder(justForAnIdeaSupportMessage), underTestNaming.next())
      }

      deadWatcher.watch(underTest)

      deadWatcher.assertThatIsAliveAfter(100.millis, underTest)

      underTest ! ServicesFinderProtocol.GetServiceReferences(underTestClientProbe.ref)

      underTestClientProbe.expectNoMessage()

      feedContentServices.tail.foreach(testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.InternalFeedServiceKey, _))
      httpClientSubscriberServices.tail.foreach(testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.HttpClientSubscriberServiceKey, _))
      socketIOClientServices.tail.foreach(testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.SocketIOClientServiceKey, _))

      underTestClientProbe.expectMessage(ServicesFinderProtocol.ServicesReferences(feedContentProbe.ref, httpClientSubscriberServiceProbe.ref, socketIOClientProbe.ref))

      underTestClientProbe.expectTerminated(underTest)

    }

  object SupportMessagesTestContext {

    val justForAnIdeaSupportMessage = new SupportMessages {

      override def internalFeedServiceDiscoverySubscribed(): String = s"internalFeedServiceDiscover Subscribed"

      override def httpClientSubscriberServiceDiscoverySubscribed(): String = s"httpClientSubscriberServiceDiscover Subscribed"

      override def serviceDiscovered[T](head: ActorRef[T]): String = s"serviceDiscovered ${head.path.name}"

      override def socketIOClientServiceDiscoverySubscribed(): String = s"socketIOClientServiceDiscover Subscribed"

      override def allServicesNotAvailableYet(): String = "allServicesNotAvailableYet"

      override def tooManyServices[T](serviceKey: ServiceKey[T], refs: List[ActorRef[T]]): String = s"tooManyServices ${serviceKey.id} ${refs.map(_.path.name).mkString(" - ")}"
    }
  }

  case class TestContext(justForAnIdeaSupportMessage: SupportMessages)(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(underTestNaming) {

    val feedContentProbe: TestProbe[SocketIOClientProtocol.FeedContent] = createTestProbe[SocketIOClientProtocol.FeedContent]("feedContent")

    val httpClientSubscriberServiceProbe: TestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo] = createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]("httpClientSubscriberService")

    val socketIOClientProbe: TestProbe[SocketIOClientProtocol.Command] = createTestProbe[SocketIOClientProtocol.Command]("socketIOClient")

    val underTestClientProbe = createTestProbe[ServicesFinderProtocol.Response]("underTestClient")


    def deregisterAllServices(): Unit = {
      testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.InternalFeedServiceKey, feedContentProbe.ref)
      testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.HttpClientSubscriberServiceKey, httpClientSubscriberServiceProbe.ref)
      testKit.system.receptionist ! Receptionist.Deregister(ServicesKeys.SocketIOClientServiceKey, socketIOClientProbe.ref)
    }
  }

}
