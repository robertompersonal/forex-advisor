package org.binqua.forex.feed.starter.controller.collaboratorswatcher

import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.subscriber.SubscriberModel
import org.binqua.forex.util.Validation
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class SubscriberModelSpec extends AnyFlatSpecLike with MockFactory with Matchers with GivenWhenThen with Validation {

  val someCurrencyPairs: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)

  val partialSubscribed = SubscriberModel.State(someCurrencyPairs, someCurrencyPairs.tail, attempt = 1)

  s"$partialSubscribed" should "be partialSubscribed" in {
    partialSubscribed.isPartiallySubscribed shouldBe true
  }

  val anotherPartialSubscribed = SubscriberModel.State(someCurrencyPairs, Set.empty, attempt = 1)

  s"$anotherPartialSubscribed" should "be partialSubscribed" in {
    anotherPartialSubscribed.isPartiallySubscribed shouldBe true
  }

  val fullySubscribed = SubscriberModel.State(someCurrencyPairs, someCurrencyPairs, attempt = 1)

  s"$fullySubscribed" should "be fullySubscribed" in {
    fullySubscribed.isPartiallySubscribed shouldBe false
  }

  "no currency pairs" should "not be allowed" in {
    the[IllegalArgumentException] thrownBy {
      SubscriberModel.State(Set.empty, Set.empty, 1)
    } should have message "requirement failed: empty currency pairs is not allowed"
  }

  "no extra currency pairs in subscribed" should "not be allowed" in {
    the[IllegalArgumentException] thrownBy {
      SubscriberModel.State(someCurrencyPairs.tail, someCurrencyPairs, 1)
    } should have message s"requirement failed: cannot have subscribed ${CurrencyPair.EurUsd} that are not in currency pairs ${someCurrencyPairs.tail}"
  }

  "subscriptionAttempts" should "be >= 1" in {
    the[IllegalArgumentException] thrownBy {
      SubscriberModel.State(currencyPairs = someCurrencyPairs, subscribed = someCurrencyPairs, attempt = -1)
    } should have message "requirement failed: subscriptionAttempts must be >= 1. -1 is wrong"

    the[IllegalArgumentException] thrownBy {
      SubscriberModel.State(currencyPairs = someCurrencyPairs, subscribed = someCurrencyPairs, attempt = 0)
    } should have message "requirement failed: subscriptionAttempts must be >= 1. 0 is wrong"

    SubscriberModel.State(currencyPairs = someCurrencyPairs, subscribed = someCurrencyPairs, attempt = 1)
  }

  "we" should "be able to increaseNumberOfAttempts" in {
    SubscriberModel.State(someCurrencyPairs, someCurrencyPairs, attempt = 1).increaseNumberOfAttempts().increaseNumberOfAttempts().attempt shouldBe 3
  }

}
