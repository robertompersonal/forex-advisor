package org.binqua.forex.feed

import cats.Eq
import cats.implicits.toShow
import eu.timepit.refined.auto.autoUnwrap
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.scalacheck.all.chooseRefinedNum
import eu.timepit.refined.types.numeric.{NonNegInt, NonNegLong}
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.advisor.model.PosBigDecimal.{TryingToRemoveTooManyPips, unsafeFrom}
import org.binqua.forex.advisor.model.Scale._
import org.binqua.forex.advisor.model.TradingPair.PriceScaleTooBigForCurrencyPair
import org.binqua.forex.advisor.model.{PosBigDecimal, TradingPair, TradingPairGen, _}
import org.binqua.forex.feed.reader.parsers.ScaleGen
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.scalacheck.Gen.chooseNum
import org.scalacheck.{Gen, Shrink}
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatest.{Assertion, GivenWhenThen}
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import scala.annotation.tailrec
import scala.math.BigDecimal.RoundingMode.HALF_UP

class TradingPairPropertiesSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  val priceWithGreaterScaleThanCurrency: Gen[(CurrencyPair, PosBigDecimal)] = for {
    posBigDecimal <- PosBigDecimalGen.posBigDecimal
    currencyPair <- TradingPairGen.currencyPair
    priceScaleGreaterThanCurrencyScale <- ScaleGen.greaterThanUntil(currencyPair)(until = refineMV[NonNegative](11))
  } yield (
    currencyPair,
    PosBigDecimal.unsafeFrom(posBigDecimal.value.setScale(priceScaleGreaterThanCurrencyScale.value.value, HALF_UP))
  )

  property("maybeATradingPair can create a trading pair given a price with scale smaller or equal to the currency pair scale") {

    val priceWithScaleSmallerOrEqualThanCurrency = for {
      posBigDecimal <- PosBigDecimalGen.posBigDecimal
      currencyPair <- TradingPairGen.currencyPair
      smallerScale <- ScaleGen.smallerOrEqualThan(currencyPair)
    } yield (currencyPair, unsafeFrom(posBigDecimal.value.setScale(smallerScale.value.value, HALF_UP)))

    forAll(priceWithScaleSmallerOrEqualThanCurrency) { args =>
      val (currencyPair, price) = args
      TradingPair.maybeATradingPair(currencyPair, price) match {
        case scala.util.Right(tradingPair) => assertThatItIsValid(tradingPair, currencyPair, price)
        case Left(_)                       => thisTestShouldNotHaveArrivedHere(because = "Trading pair should have been created in this case")
      }
    }
  }

  property(testName =
    "maybeATradingPair cannot create a new trading pair given a price with scale greater than the currency pair scale because we will lose precision"
  ) {

    forAll((priceWithGreaterScaleThanCurrency, "priceWithGreaterScaleThanCurrencyScale")) { args =>
      val (currencyPair, price: PosBigDecimal) = args

      TradingPair.maybeATradingPair(currencyPair, price) should be(
        Left(
          TradingPair
            .PriceScaleTooBigForCurrencyPair(
              currencyPair,
              price,
              PosBigDecimal.RescaleFailedBecauseNewScaleTooSmall(price, currencyPair.quoteCurrency.scale).show
            )
            .show
        )
      )
    }
  }

  property(testName = "given wrong data, then validationMessage is correct ") {
    forAll((priceWithGreaterScaleThanCurrency, "priceWithGreaterScaleThanCurrencyScale")) { args =>
      val (currencyPair: CurrencyPair, price: PosBigDecimal) = args
      PriceScaleTooBigForCurrencyPair(
        currencyPair,
        price,
        reason = PosBigDecimal.RescaleFailedBecauseNewScaleTooSmall(price, currencyPair.quoteCurrency.scale).show
      ).show should be(
        s"Cannot build a trading pair for ${currencyPair.show} with price ${price.show}. Details: Scale of ${price.show} is ${price.scale.show} and it is too big compare to new scale ${currencyPair.quoteCurrency.scale.show}. I will lose precision"
      )
    }
  }

  property(testName =
    "priceIncreasedByPips increases or decreases the number of pips of a tradingPair if pipsToBeAdded is less than maxNumberOfPipsToBeRemoved. Digits on the right of pip position are not touched"
  ) {

    forAll((TradingPairGen.tradingPairs, "tradingPair")) { initialTradingPair =>
      val maxNumberOfPipsToBeRemoved: NonNegLong = findMaxNumberOfPipsToBeRemoved(initialTradingPair)

      info("----")

      Given(s"an initial TradingPair $initialTradingPair with maxNumberOfPipsToBeRemoved of $maxNumberOfPipsToBeRemoved")

      forAll((chooseNum(-maxNumberOfPipsToBeRemoved, maxT = 1000L), "pipsToBeAdded")) { (pipsToBeAdded: Long) =>
        When(
          s"priceIncreasedByPipUnits adds $pipsToBeAdded pips to $initialTradingPair I get ${TradingPair.priceIncreasedByPipUnits(initialTradingPair, pipsToBeAdded)}"
        )

        TradingPair.priceIncreasedByPipUnits(initialTradingPair, pipsToBeAdded) match {
          case Right(newPrice) =>
            Then(s"new Price increased of $pipsToBeAdded pips")
            numberOfPipsAsUnits(newPrice).numberOfPips should be(numberOfPipsAsUnits(initialTradingPair).numberOfPips + pipsToBeAdded)

            And(s"the digits on the right of pip position stay unchanged")
            numberOfPipsAsUnits(newPrice).fractionalPart should be(numberOfPipsAsUnits(initialTradingPair).fractionalPart)
          case Left(error) =>
            thisTestShouldNotHaveArrivedHere(because(s"we removed $pipsToBeAdded pips less then max $maxNumberOfPipsToBeRemoved. Details: $error"))
        }

      }

    }
  }

  property(testName = "priceIncreasedByPips returns right error if you try to removed too many pips") {

    forAll((TradingPairGen.tradingPairs, "tradingPair")) { initialTradingPair =>
      val maxNumberOfPipsToBeRemoved: NonNegLong = findMaxNumberOfPipsToBeRemoved(initialTradingPair)

      info("----")

      Given(s"an initial TradingPair $initialTradingPair with maxNumberOfPipsToBeRemoved of $maxNumberOfPipsToBeRemoved")

      forAll((chooseRefinedNum(refineMV[NonNegative](1L), refineMV[NonNegative](1000L)), "extraPipsToBeRemoved")) { (extraPipsToBeRemoved: NonNegLong) =>
        val pipsToBeRemoved: NonNegLong = refineV[NonNegative].unsafeFrom(maxNumberOfPipsToBeRemoved.value + extraPipsToBeRemoved.value)

        When(s"we try to removed $pipsToBeRemoved")

        val underTest: Either[String, TradingPair] = TradingPair.priceIncreasedByPipUnits(initialTradingPair, pipsToBeAdded = -1 * pipsToBeRemoved.value)

        underTest match {
          case Right(newPrice) =>
            thisTestShouldNotHaveArrivedHere(because(s"we removed $pipsToBeRemoved pips but the max was $maxNumberOfPipsToBeRemoved. Details: $newPrice"))
          case Left(error) =>
            Then(s"we get a nice error $error")
            error should be(
              TryingToRemoveTooManyPips(
                initialTradingPair.price,
                maxNumberOfPipsToBeRemoved,
                pipsToBeRemoved,
                NonNegInt.unsafeFrom(initialTradingPair.currencyPair.quoteCurrency.pipPosition)
              ).show
            )
        }

      }

    }
  }

  def assertThatItIsValid(tradingPair: TradingPair, expCurrencyPair: CurrencyPair, expPrice: PosBigDecimal): Assertion = {
    tradingPair.currencyPair should be(expCurrencyPair)
    tradingPair.price should be(expPrice)
  }

  def findMaxNumberOfPipsToBeRemoved(tradingPair: TradingPair): NonNegLong = {
    val priceWithAllPipsOnTheIntegerPart = multiplyBy10XTimes(tradingPair.price, numberOfTimes = tradingPair.currencyPair.quoteCurrency.pipPosition)
    NonNegLong.unsafeFrom(SplitIntegerAndFractionalPart(priceWithAllPipsOnTheIntegerPart).numberOfPips)
  }

  def numberOfPipsAsUnits(tradingPair: TradingPair): SplitIntegerAndFractionalPart =
    SplitIntegerAndFractionalPart(multiplyBy10XTimes(tradingPair.price, numberOfTimes = tradingPair.currencyPair.quoteCurrency.pipPosition))

  @tailrec private def multiplyBy10XTimes(toBeMultiplied: PosBigDecimal, numberOfTimes: NonNegInt): PosBigDecimal = {
    if (Eq.eqv(numberOfTimes.value, 0)) toBeMultiplied
    else multiplyBy10XTimes(toBeMultiplied * PosBigDecimal.ten, NonNegInt.unsafeFrom(numberOfTimes - 1))
  }

  case class SplitIntegerAndFractionalPart(private val toBeSplit: PosBigDecimal) {
    val numberOfPips: Long = toBeSplit.value.toInt
    val fractionalPart: BigDecimal = toBeSplit.value - numberOfPips
  }

}
