package org.binqua.forex.feed.notifier.publisher

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.eventstream.EventStream
import org.binqua.forex.advisor.model.CurrencyPair.GbpUsd
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair}
import org.binqua.forex.feed.UpdatingAccountGen.anUpdatingAccountFor
import org.binqua.forex.feed.notifier.publisher
import org.binqua.forex.feed.notifier.publisher.SingleAccountCurrencyEventStreamPublisher.{AccountCurrencyMismatch, CurrencyPairNotFound, PublishCommand}
import org.binqua.forex.feed.protocol.{GbpUsdUpdatedForGbpAccount, withoutValidation}
import org.binqua.forex.feed.protocol.withoutValidation.UpdatedCommandCreator
import org.binqua.forex.util.{ErrorSituationHandler, TestingFacilities, Validation}
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import scala.reflect.ClassTag

class SingleAccountCurrencyEventStreamPublisherSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with MockFactory with Matchers with GivenWhenThen with TestingFacilities with Validation {

  private val errorSituationHandlerMock = mock[ErrorSituationHandler]

  "After receiving a PublishCommand for a compatible account currency the actor" should "publish the right message" in {
    val gbpAccountCurrencyMapping: Map[CurrencyPair, UpdatedCommandCreator] = Map(
      CurrencyPair.EurGbp -> withoutValidation.eurGbpUpdatedForGbpAccount,
      CurrencyPair.EurUsd -> withoutValidation.eurUsdUpdatedForGbpAccount,
      CurrencyPair.GbpUsd -> withoutValidation.gbpUsdUpdatedForGbpAccount,
      CurrencyPair.Spx500 -> withoutValidation.spx500UpdatedForGbpAccount,
      CurrencyPair.Us30 -> withoutValidation.us30UpdatedForGbpAccount,
    )

    val anUpdatingAccount = anUpdatingAccountFor(GbpUsd, AccountCurrency.Gbp).sample.get

    val gbpAccountInterestedInGbpUsd = subscribeTo[GbpUsdUpdatedForGbpAccount]

    val actorUnderTest = spawn(publisher.SingleAccountCurrencyEventStreamPublisher(AccountCurrency.Gbp, gbpAccountCurrencyMapping, errorSituationHandlerMock))

    actorUnderTest ! PublishCommand(anUpdatingAccount)

    gbpAccountInterestedInGbpUsd.expectMessage(withoutValidation.gbpUsdUpdatedForGbpAccount(anUpdatingAccount).asInstanceOf[GbpUsdUpdatedForGbpAccount])

  }

  "After receiving a PublishCommand for an incompatible account currency the actor" should "log an error and publish anything" in {

    val gbpAccountCurrencyMapping: Map[CurrencyPair, UpdatedCommandCreator] = Map(
      CurrencyPair.EurGbp -> withoutValidation.eurGbpUpdatedForGbpAccount
    )

    val anUpdatingAccountForEur = anUpdatingAccountFor(GbpUsd, AccountCurrency.Eur).sample.get

    val gbpAccountInterestedInGbpUsd = subscribeTo[GbpUsdUpdatedForGbpAccount]

    val expectedErrorMessage = "Did not find anything"
    (errorSituationHandlerMock.show _)
      .expects(AccountCurrencyMismatch(AccountCurrency.Gbp, anUpdatingAccountForEur))
      .returning(expectedErrorMessage)

    val actorUnderTest = spawn(SingleAccountCurrencyEventStreamPublisher(AccountCurrency.Gbp, gbpAccountCurrencyMapping, errorSituationHandlerMock))

    LoggingTestKit.error(expectedErrorMessage).expect {
      actorUnderTest ! PublishCommand(anUpdatingAccountForEur)
    }

    gbpAccountInterestedInGbpUsd.expectNoMessage()

  }

  "After receiving a PublishCommand for an incompatible currency pair the actor" should "log an error and publish anything" in {

    val gbpAccountCurrencyMapping: Map[CurrencyPair, UpdatedCommandCreator] = Map(
      CurrencyPair.EurGbp -> withoutValidation.eurGbpUpdatedForGbpAccount,
    )

    val expAccountCurrency = AccountCurrency.Gbp

    val anIncompatibleUpdatingAccount = anUpdatingAccountFor(GbpUsd, expAccountCurrency).sample.get

    val gbpAccountInterestedInGbpUsd = subscribeTo[GbpUsdUpdatedForGbpAccount]

    val expectedErrorMessage = "Did not find anything for the currency pair"
    (errorSituationHandlerMock.show _)
      .expects(CurrencyPairNotFound(expAccountCurrency, anIncompatibleUpdatingAccount))
      .returning(expectedErrorMessage)

    val actorUnderTest = spawn(publisher.SingleAccountCurrencyEventStreamPublisher(expAccountCurrency, gbpAccountCurrencyMapping, errorSituationHandlerMock))

    LoggingTestKit.error(expectedErrorMessage).expect {
      actorUnderTest ! PublishCommand(anIncompatibleUpdatingAccount)
    }

    gbpAccountInterestedInGbpUsd.expectNoMessage()

  }

  private def subscribeTo[M](implicit classTag: ClassTag[M]): TestProbe[M] = {
    val testProbe = createTestProbe[M]()
    system.eventStream ! EventStream.Subscribe[M](testProbe.ref)(classTag)
    testProbe
  }
}
