package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.util.SerializeAndDeserializeSpec
import org.scalatest.flatspec.AnyFlatSpecLike

class ProtocolSerializationSpec extends ScalaTestWithActorTestKit
  with AnyFlatSpecLike
  with SerializeAndDeserializeSpec {

  "PersistenceConnectionRegistryProtocol response" can "be serialize and deserialize" in {
    val toBeTested: Set[AnyRef] = Set(
      Persistence.Registered,
    )

    toBeTested.foreach(canBeSerialiseAndDeserialize)
  }


}
