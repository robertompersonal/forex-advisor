package org.binqua.forex.feed

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.AccountCurrency.Eur
import org.binqua.forex.advisor.model.CurrencyPair.GbpUsd
import org.binqua.forex.advisor.model.{CurrencyPair, UglyQuoteSubscriptionFinder}
import org.binqua.forex.advisor.portfolios.PortfoliosGen.aPositionWithGivenPair
import org.binqua.forex.feed.protocol._
import org.scalatest.flatspec.AnyFlatSpecLike

class QuoteListenerActorSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike {

  "A newly created portfolios" should "have no positions" in {

    val actorToBeNotifiedOfQuoteUpdates = createTestProbe[Notification]("PortfolioActor")

    val listenerActorUnderTest = testKit.spawn(QuoteListenerActor(CurrencyPair.GbpUsd, Eur, actorToBeNotifiedOfQuoteUpdates.ref, UglyQuoteSubscriptionFinder))

    val firstPosition = aPositionWithGivenPair(GbpUsd).sample.get
    listenerActorUnderTest ! NewPosition("p0", firstPosition)

    //    val secondPosition = aPositionWithPriceRelatedTo(firstPosition).sample.get
    //    listenerActorUnderTest ! NewPosition("p1", secondPosition)
    //
    //    val thirdPosition = aPositionWithPriceRelatedTo(firstPosition).sample.get
    //    listenerActorUnderTest ! NewPosition("p2", thirdPosition)

    //    listenerActorUnderTest !
    //
    //    actorToBeNotifiedOfQuoteUpdates.expectMessage(Notification())

  }

}
