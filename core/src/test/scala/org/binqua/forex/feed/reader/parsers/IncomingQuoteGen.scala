package org.binqua.forex.feed.reader.parsers

import org.binqua.forex.advisor.model.PosBigDecimalGen.{greaterThan, posBigDecimal, smallerThan, _}
import org.binqua.forex.advisor.model.{CurrencyPair, PosBigDecimal, TradingPair, TradingPairGen}
import org.binqua.forex.advisor.portfolios.PortfoliosModel
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{Buy, RecordedPosition, Sell}
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.model.Rates
import org.scalacheck.Gen
import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json.{JsNumber, Json}

import java.time.{LocalDateTime, ZoneOffset}
import scala.math.pow

object IncomingQuoteGen {

  val minimumNonZeroDecimalPart: String = "0.000001"

  private val localDateTime13LongWithMillsGen: Gen[String] = for {
    year <- Gen.chooseNum(2018, 2030)
    month <- Gen.chooseNum(1, 12)
    hours <- Gen.chooseNum(0, 23)
    minutes <- Gen.chooseNum(1, 59)
    secs <- Gen.chooseNum(1, 59)
    mills <- Gen.chooseNum(1, 999)
  } yield LocalDateTime
    .of(year, month, 1, hours, minutes, secs, mills * pow(10, 6).toInt)
    .toInstant(ZoneOffset.UTC)
    .toEpochMilli
    .toString

  val maxBuyOfTheDaySmallerOrEqualThanMinSellOfTheDay: Gen[String] = for {
    (List(sell: PosBigDecimal, buy: PosBigDecimal, _: PosBigDecimal, minOfTheDaySellRate: PosBigDecimal), currencyPair) <-
      IncomingRatesGen.withValidIncomingRates
    newMaxBuyOfTheDay <- minOfTheDaySellRate or smallerThan(minOfTheDaySellRate, currencyPair)
  } yield Json.stringify(Json.obj(ratesAsJsonObj(List(sell, buy, newMaxBuyOfTheDay, minOfTheDaySellRate))))

  val minSellOfTheDayGreaterThanSell: Gen[String] = for {
    (List(sellRate: PosBigDecimal, buyRate: PosBigDecimal, maxOfTheDay: PosBigDecimal, _: PosBigDecimal), currencyPair) <-
      IncomingRatesGen.withValidIncomingRates
    newMinSellOfTheDay <- greaterThan(sellRate, currencyPair)
  } yield Json.stringify(Json.obj(ratesAsJsonObj(List(sellRate, buyRate, maxOfTheDay, newMinSellOfTheDay))))

  val maxBuyOfTheDaySmallerThenBuy: Gen[String] = for {
    (List(sellRate: PosBigDecimal, buyRate: PosBigDecimal, _: PosBigDecimal, minOfTheDaySellRate: PosBigDecimal), currencyPair) <-
      IncomingRatesGen.withValidIncomingRates
    newMaxBuyOfTheDay <- smallerThan(buyRate, currencyPair)
  } yield Json.stringify(Json.obj(ratesAsJsonObj(List(sellRate, buyRate, newMaxBuyOfTheDay, minOfTheDaySellRate))))

  val instantWithMills13Long: Gen[(LocalDateTime, String, (String, JsValueWrapper))] = for {
    localDateTime13LongWithMills <- localDateTime13LongWithMillsGen
  } yield {
    val localDateTime = LocalDateTime.ofInstant(java.time.Instant.ofEpochMilli(localDateTime13LongWithMills.toLong), ZoneOffset.UTC)
    val asJsonObj = updatedAsJsonObj(localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli.toString)
    (localDateTime, Json.stringify(Json.obj(asJsonObj)), asJsonObj)
  }

  val instantWithMillsNon13Long: Gen[(String, String)] = for {
    asString <- localDateTime13LongWithMillsGen
    extraNum <- Gen.chooseNum(1, 10000)
    toBeDropped <- Gen.chooseNum(1, 12)
    toBeJsonify <- Gen.oneOf(asString ++ extraNum.toString, asString.drop(toBeDropped))
  } yield (Json.stringify(Json.obj(updatedAsJsonObj(toBeJsonify))), toBeJsonify)

  val symbols = org.scalatest.prop.Tables.Table(
    ("currencyPair", "symbol"),
    (CurrencyPair.EurGbp, "EUR/GBP"),
    (CurrencyPair.EurUsd, "EUR/USD"),
    (CurrencyPair.EurJpy, "EUR/JPY"),
    (CurrencyPair.GbpUsd, "GBP/USD"),
    (CurrencyPair.GbpJpy, "GBP/JPY"),
    (CurrencyPair.Spx500, "SPX500"),
    (CurrencyPair.UsdJpy, "USD/JPY"),
    (CurrencyPair.Us30, "US30")
  )

  val fromCurrencyToExternalSymbol: Map[CurrencyPair, String] = symbols.toMap

  val validIncomingQuotes: Gen[(IncomingQuoteRecordedEvent, String)] = validIncomingQuotesByCurrencyPair(allCurrencyPairs)

  val buyEqualOrSmallerThanSell: Gen[String] = for {
    (initialRates, currencyPair) <- IncomingRatesGen.withValidIncomingRates
    newWrongBuy <- initialRates(0) or smallerThan(initialRates(0), currencyPair)
  } yield Json.stringify(Json.obj(ratesAsJsonObj(List(initialRates(0), newWrongBuy, initialRates(2), initialRates(3)))))

  val validIncomingRatesGen: Gen[(IncomingRates, String, List[PosBigDecimal])] =
    IncomingRatesGen.withValidIncomingRates.flatMap(ratesAndCurrencyPair => {
      val (rawRates: List[PosBigDecimal], _) = ratesAndCurrencyPair
      val List(sellRate, buyRate, maxOfTheDay, minOfTheDay) = rawRates

      IncomingRates
        .validatedX(rawRates)
        .fold(_ => Gen.fail, (_, Json.stringify(Json.obj(ratesAsJsonObj(List(sellRate, buyRate, maxOfTheDay, minOfTheDay)))), rawRates))

    })

  def withIncomingRates(incomingRatesGen: TradingPair => Gen[(List[PosBigDecimal], Rates)]): Gen[(IncomingQuoteRecordedEvent, List[PosBigDecimal], Rates)] =
    for {
      (localDateTime, _, _) <- instantWithMills13Long
      tradingPair <- TradingPairGen.tradingPairs
      (incomingRateValues, expRates) <- incomingRatesGen(tradingPair)
    } yield (
      IncomingQuoteRecordedEvent(
        localDateTime,
        IncomingRates.unsafe(incomingRateValues),
        tradingPair.currencyPair
      ),
      incomingRateValues,
      expRates
    )

  def validIncomingQuotesOf(wantedCurrencyPair: CurrencyPair => Boolean): Gen[(IncomingQuoteRecordedEvent, String)] =
    validIncomingQuotesByCurrencyPair(wantedCurrencyPair)

  val anyCurrencyPair: CurrencyPair => Boolean = _ => true

  private def validIncomingQuotesByCurrencyPair(wantedCurrencyPair: CurrencyPair => Boolean): Gen[(IncomingQuoteRecordedEvent, String)] =
    for {
      updated <- instantWithMills13Long
      (rates, _, rawRates, currencyPair) <- validIncomingRatesCurrencyPairBasedGen(wantedCurrencyPair)
    } yield {
      (
        IncomingQuoteRecordedEvent(updated._1, rates, currencyPair),
        Json.stringify(
          Json.obj(
            updated._3,
            ratesAsJsonObj(rawRates),
            symbolAsJsonObj(fromCurrencyToExternalSymbol(currencyPair))
          )
        )
      )
    }

  def validIncomingRatesCurrencyPairBasedGen(wantedCurrencyPair: CurrencyPair => Boolean): Gen[(IncomingRates, String, List[PosBigDecimal], CurrencyPair)] =
    IncomingRatesGen
      .withValidIncomingRatesCurrencyPairBased(wantedCurrencyPair)
      .map(ratesAndCurrencyPair => {
        val (rates: List[PosBigDecimal], currencyPair) = ratesAndCurrencyPair
        val List(sellRate, buyRate, maxOfTheDay, minOfTheDay) = rates
        (
          IncomingRates.unsafe(rates),
          Json.stringify(Json.obj(ratesAsJsonObj(List(sellRate, buyRate, maxOfTheDay, minOfTheDay)))),
          rates,
          currencyPair
        )
      })

  def incomingQuoteRecordedEventDifferentFrom(toBeRemoved: CurrencyPair): Gen[IncomingQuoteRecordedEvent] =
    for {
      updated <- instantWithMills13Long
      rates <- validIncomingRatesGen
      currencyPair <- TradingPairGen.currencyPair.filter(_ != toBeRemoved)
    } yield IncomingQuoteRecordedEvent(updated._1, rates._1, currencyPair)

  def incomingQuoteRecordedEventCloseTo(positionGenerator: Gen[RecordedPosition]): Gen[IncomingQuoteRecordedEvent] =
    for {
      updated <- instantWithMills13Long
      pipsGained <- Gen.choose(-100, 100)
      openedPosition <- positionGenerator
      newBuyAtTradingPair <- TradingPair.priceIncreasedByPipUnits(openedPosition.data.tradingPair, pipsGained).fold(_ => Gen.fail[TradingPair], Gen.const)
      incomingRatesWithBuyPriceAt <- IncomingRatesGen.withBuyAt(newBuyAtTradingPair)
      newSellAtTradingPair <- TradingPair.priceIncreasedByPipUnits(openedPosition.data.tradingPair, pipsGained).fold(_ => Gen.fail[TradingPair], Gen.const)
      incomingRatesWithSellPriceAt <- IncomingRatesGen.withMinSellRateAt(newSellAtTradingPair)
    } yield IncomingQuoteRecordedEvent(
      updated._1,
      chooseIncomingRatesBasedOn(openedPosition.data.positionType, incomingRatesWithBuyPriceAt, incomingRatesWithSellPriceAt),
      openedPosition.data.tradingPair.currencyPair
    )

  private def chooseIncomingRatesBasedOn(
      positionType: PortfoliosModel.PositionType,
      ratesWithGivenBuyPriceAt: List[PosBigDecimal],
      ratesWithGivenSellPriceAt: List[PosBigDecimal]
  ): IncomingRates = {
    val rates: List[PosBigDecimal] = positionType match {
      case Buy  => ratesWithGivenSellPriceAt
      case Sell => ratesWithGivenBuyPriceAt
    }
    IncomingRates.unsafe(rates)
  }

  def tooFewRates: Gen[String] =
    for {
      (validRatesList, _) <- IncomingRatesGen.withValidIncomingRates
      elementToBeDropped <- Gen.chooseNum(1, validRatesList.size)
    } yield Json.stringify(Json.obj(ratesAsJsonObj(makeItShorter(elementToBeDropped, validRatesList))))

  private def makeItShorter[E](elementToBeDrop: Int, xs: List[E]): List[E] = {

    def randomIndex(toBeShortened: List[E]) = Gen.chooseNum(0, toBeShortened.size - 1).sample.get

    def removeIndex(toBeShortened: List[E], indexToBeRemoved: Int): List[E] = toBeShortened.zipWithIndex.filter(_._2 != indexToBeRemoved).map(_._1)

    def makeItShorter(toBeShortened: List[E], elementToBeDrop: Int): List[E] =
      if (elementToBeDrop == 0)
        toBeShortened
      else
        makeItShorter(removeIndex(toBeShortened, randomIndex(toBeShortened)), elementToBeDrop - 1)

    makeItShorter(xs, elementToBeDrop)
  }

  private def ratesAsJsonObj(validaRatesList: List[PosBigDecimal]): (String, JsValueWrapper) = "Rates" -> validaRatesList.map(_.value)

  def tooManyRates: Gen[String] =
    for {
      (validRatesList, _) <- IncomingRatesGen.withValidIncomingRates
      elementToBeAdded <- Gen.nonEmptyListOf(posBigDecimal)
    } yield Json.stringify(Json.obj(ratesAsJsonObj(validRatesList ++ elementToBeAdded)))

  def symbolAsJson(symbol: String): String = Json.stringify(Json.obj(symbolAsJsonObj(symbol)))

  def symbolAsJsonObj(symbol: String): (String, JsValueWrapper) = "Symbol" -> symbol

  private def allCurrencyPairs: CurrencyPair => Boolean = _ => true

  private def updatedAsJsonObj(toBeJsonify: String): (String, JsValueWrapper) = "Updated" -> JsNumber(BigDecimal(toBeJsonify))
}
