package org.binqua.forex.feed.socketio.connectionregistry.persistence

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.typesafe.config.ConfigFactory
import org.apache.commons.io.FileUtils
import org.binqua.forex.feed.socketio.connectionregistry.persistence
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import org.scalatest.{DoNotDiscover, GivenWhenThen}

import java.io.File

object PersistenceDontRunAlone {

  private val baseDir = s"${FileUtils.getTempDirectoryPath}/forexAdvisorProject/test"

  val healthCheckConfig = ConfigFactory.parseString(s"""|akka {
        |  loglevel = "INFO"
        |  actor {
        |    serializers {jackson-json = "akka.serialization.jackson.JacksonJsonSerializer" }
        |    serialization-bindings { "org.binqua.forex.JsonSerializable" = jackson-json}
        |  }
        |
        |   persistence.journal.plugin = "akka.persistence.journal.leveldb"
        |   persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
        |
        |   persistence.journal.leveldb.dir = "$baseDir/journal"
        |   persistence.snapshot-store.local.dir = "$baseDir/snapshots"
        |
        |}
        |
        |""".stripMargin)
}

@DoNotDiscover
class PersistenceDontRunAlone
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(PersistenceDontRunAlone.healthCheckConfig))
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  val storageLocations = List(
    new File(system.settings.config.getString("akka.persistence.journal.leveldb.dir")),
    new File(system.settings.config.getString("akka.persistence.snapshot-store.local.dir"))
  )

  override def beforeAll(): Unit = {
    super.beforeAll()
    storageLocations.foreach(FileUtils.deleteDirectory)
  }

  override def afterAll(): Unit = {
    storageLocations.foreach(FileUtils.deleteDirectory)
    super.afterAll()
  }

  val socketId = SocketId("12")

  "As soon as the actor is spawn, it" should "create a child socketIOManager and Connect it" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      waitFor(500.millis, soThat("we have time to find the child"))

      assertNumberOfChildSpawnIs(expNumber = 1)

      socketIOManagerContext.probe.expectMessage(Connector.Connect)
    }

  "Register" should "persist the actor ref that start the subscription and notify the socketIOManager" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      matchDistinctLogs(messages = supportMessages.actorRegistered(expectedActorStateAfterOneRegistration)).expect {

        underTest ! registerMessage

        whoRequestToRegisterProbe.expectMessage(Registered)

        socketIOManagerContext.probe.expectMessage(Notify(registerMessage.subscriptionStarter))

      }
    }

  "Register" should "not register the same actor reference twice because it could come simply from a client that is retrying" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      matchDistinctLogs(
        messages = supportMessages.actorRegistered(expectedActorStateAfterOneRegistration),
        supportMessages.actorAlreadyRegistered(registerMessage, expectedActorStateAfterOneRegistration)
      ).expect {

        underTest ! registerMessage

        whoRequestToRegisterProbe.expectMessage(Registered)

        socketIOManagerContext.probe.expectMessage(Notify(registerMessage.subscriptionStarter))

        underTest ! registerMessage

        whoRequestToRegisterProbe.expectMessage(Registered)

        socketIOManagerContext.probe.expectNoMessage()

      }
    }

  "The actor" should "register same alias with different actorThatNeedsTheConnection reference because could come from actor that have been stopped and restarted" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      val anotherSubscriptionStarter = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("anotherSubscriptionStarter")

      val anotherWhoRequestToRegisterProbe = createTestProbe[Response]("registrationRequester")

      val newRegisterMessage = Register(
        registrationRequester = anotherWhoRequestToRegisterProbe.ref,
        subscriptionStarter = anotherSubscriptionStarter.ref,
        registerMessage.requesterAlias
      )

      val newStateAfterSecondRegistration = State.empty
        .register(Registered(registerMessage.subscriptionStarter, registerMessage.requesterAlias))
        .register(Registered(newRegisterMessage.subscriptionStarter, registerMessage.requesterAlias))

      matchDistinctLogs(
        messages = supportMessages.actorRegistered(expectedActorStateAfterOneRegistration),
        supportMessages.actorRegistered(newStateAfterSecondRegistration)
      ).expect {

        underTest ! registerMessage

        whoRequestToRegisterProbe.expectMessage(Registered)

        socketIOManagerContext.probe.expectMessage(Notify(registerMessage.subscriptionStarter))

        underTest ! newRegisterMessage

        anotherWhoRequestToRegisterProbe.expectMessage(Registered)

        socketIOManagerContext.probe.expectMessage(Notify(newRegisterMessage.subscriptionStarter))

      }
    }

  "given an actor with a state with a single entry, it" should "restore its state when restarted and notify the subscriptionStarter" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      underTest ! registerMessage

      whoRequestToRegisterProbe.expectMessage(Registered)

      socketIOManagerContext.probe.expectMessage(Notify(registerMessage.subscriptionStarter))

      testKit.stop(underTest)

      matchDistinctLogs(
        messages = supportMessages.stateRestored(expectedActorStateAfterOneRegistration),
        supportMessages.actorAlreadyRegistered(registerMessage, expectedActorStateAfterOneRegistration)
      ).expect {
        spawn(underTestBehavior) ! registerMessage
      }

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      socketIOManagerContext.probe.expectMessage(Notify(registerMessage.subscriptionStarter))

      assertNumberOfChildSpawnIs(expNumber = 2)

    }

  "given an actor with a state multiple entries, it" should "restore its state when restarted and notify the last subscriptionStarter" in
    new TestContext(
      SocketIOManagerTestContext.thatIgnoreAnyMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      val messages: IndexedSeq[Register] = (1 to 10).map(_ =>
        Register(registrationRequester = createTestProbe().ref, subscriptionStarter = createTestProbe().ref, requesterAlias = "subscriptionStarter")
      )

      messages.foreach(m => {
        underTest ! m
        socketIOManagerContext.probe.expectMessage(Notify(m.subscriptionStarter))
      })

      testKit.stop(underTest)

      spawn(underTestBehavior)

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      socketIOManagerContext.probe.expectMessage(Notify(messages.last.subscriptionStarter))

    }

  "the actor" should "watch the socketIOManager child in case it fails and recreate it and then connect and notify it again" in
    new TestContext(
      SocketIOManagerTestContext.thatFailsAfterReceivingAGivenMessage(),
      supportMessages = SupportMessagesTestContext.justForAnIdea,
      underTestNaming = underTestNaming
    ) {

      private def theChildThrowAnException: Unit = {
        lastSocketIOManagerChildSpawned() ! SocketIOManagerTestContext.aDummyMessageThatTerminateTheChild
        socketIOManagerContext.probe.expectMessage(SocketIOManagerTestContext.aDummyMessageThatTerminateTheChild)
        waitALittleBit(soThat("the child can be recreated"))
      }

      private val theExpNotifyMessage = Notify(registerMessage.subscriptionStarter)

      socketIOManagerContext.probe.expectMessage(Connector.Connect)

      underTest ! registerMessage

      whoRequestToRegisterProbe.expectMessage(Registered)

      socketIOManagerContext.probe.expectMessage(theExpNotifyMessage)

      theChildThrowAnException

      assertNumberOfChildSpawnIs(expNumber = 2)

      actorsWatchers.assertThatIsStillAlive(underTest)

      socketIOManagerContext.probe.expectMessage(Connector.Connect)
      socketIOManagerContext.probe.expectMessage(theExpNotifyMessage)

      theChildThrowAnException

      assertNumberOfChildSpawnIs(expNumber = 3)

      socketIOManagerContext.probe.expectMessage(Connector.Connect)
      socketIOManagerContext.probe.expectMessage(theExpNotifyMessage)

      actorsWatchers.assertThatIsStillAlive(underTest)

    }

  object SocketIOManagerTestContext {

    val aDummyMessageThatTerminateTheChild: Connector.Command = Connector.Notify(createTestProbe().ref)

    val actorPrefixName = "socketIOManagerChildMaker"

    type theType = ActorCollaboratorTestContext[Persistence.Message, Connector.Command]

    trait Base extends theType {

      override val name: String = SocketIOManagerTestContext.actorPrefixName

      override val probe: TestProbe[Connector.Command] = createTestProbe()

      override def behavior: Behavior[Connector.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Persistence.Message, Connector.Command] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[Connector.Command] = spawn(Behaviors.monitor(probe.ref, behavior))
    }

    case class thatFailsAfterReceivingAGivenMessage() extends Base {

      override def behavior: Behavior[Connector.Command] =
        Behaviors.receiveMessage[Connector.Command]({
          case Connector.Connect =>
            Behaviors.same
          case s: Connector.Notify =>
            if (s == aDummyMessageThatTerminateTheChild)
              throw new IllegalArgumentException("I am going to die")
            else Behaviors.same
          case _ =>
            throw new IllegalArgumentException("This should not happened")
        })

    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[Connector.Command] = Behaviors.ignore

    }

  }

  object SupportMessagesTestContext {

    val justForAnIdea = new SupportMessages {

      private def actorRefLog[T](actorRef: ActorRef[T]): String = s"ref ${actorRef.path.name.split("-")(0)}"

      def toAlias(state: State) = s"alias ${state.theMostRecent.get.requesterAlias}"

      def toActorRef(state: State) = state.theMostRecent.get.subscriptionStarter

      override def actorRegistered(state: State): String =
        state.theMostRecent match {
          case None => s"actor ${actorRefLog(toActorRef(state))} with ${toAlias(state)} registered"
          case _    => s"actor with ${toAlias(state)} re-registered with reference ${actorRefLog(toActorRef(state))}. Old reference replaced"
        }

      override def actorAlreadyRegistered(register: Register, state: State): String =
        s"actor ${actorRefLog(register.subscriptionStarter)} with ${toAlias(state)} already registered"

      override def stateRestored(restoredState: State): String = s"state restored $restoredState"
    }
  }

  case class TestContext(socketIOManagerContext: SocketIOManagerTestContext.theType, supportMessages: SupportMessages, underTestNaming: UnderTestNaming)
      extends BaseTestContext(underTestNaming) {

    val whoRequestToRegisterProbe = createTestProbe[Response]("registrationRequester")

    val subscriptionStarterProbe = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter")

    val underTestBehavior = persistence.Persistence(
      uniqueId = randomString,
      socketIOManagerContext.childMakerFactory(actorsWatchers)(),
      supportMessages = supportMessages
    )

    val underTest = spawn(underTestBehavior, underTestNaming.next())

    val registerMessage = Register(
      registrationRequester = whoRequestToRegisterProbe.ref,
      subscriptionStarter = subscriptionStarterProbe.ref,
      requesterAlias = "subscriptionStarter"
    )

    val expectedActorStateAfterOneRegistration = State.empty.register(Registered(registerMessage.subscriptionStarter, registerMessage.requesterAlias))

    def lastSocketIOManagerChildSpawned() = eventually(actorsWatchers.lastCreatedInstanceOf[Connector.Command](SocketIOManagerTestContext.actorPrefixName))

    def assertNumberOfChildSpawnIs(expNumber: Int): Unit = assertChildSpawnAreExactly(SocketIOManagerTestContext.actorPrefixName, expNumber, because(""))

  }

}
