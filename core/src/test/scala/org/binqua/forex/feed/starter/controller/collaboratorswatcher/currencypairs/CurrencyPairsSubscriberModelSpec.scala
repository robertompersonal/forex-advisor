package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberModel.{State, StateUnderConstruction}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.CurrencyPairsSubscriberProtocol.Subscriptions
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class CurrencyPairsSubscriberModelSpecextends
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  private val eurUsdActorRef: ActorRef[CurrencyPairSubscriberProtocol.Command] = createTestProbe().ref
  private val us30ActorRef: ActorRef[CurrencyPairSubscriberProtocol.Command] = createTestProbe().ref
  private val eurGbpActorRef: ActorRef[CurrencyPairSubscriberProtocol.Command] = createTestProbe().ref

  private val replyTo: ActorRef[Subscriptions] = createTestProbe().ref

  private val socketId = SocketId("1")

  "An state with only currency pairs recorded" should "have all subscriptions still missing" in new TestContext {

    assertResult(true, "hasSomeSubscriptionsStillMissing")(initialState.someSubscriptionsAreStillMissing)

    assertResult(Set(CurrencyPair.EurUsd, CurrencyPair.Us30), "should be unsubscribed")(initialState.waitingForResult)

  }

  "A state with some currency pairs unsubscribed" should "be unsubscribed. It cannot be subscribed" in new TestContext {

    val unsubscribedState = initialState.currencyPairSubscribed(false, CurrencyPair.Us30)

    assertResult(Set(CurrencyPair.Us30), "unsubscribed are wrong")(unsubscribedState.unsubscribed)

    assertResult(Set.empty, "subscribed are wrong")(unsubscribedState.subscribed)

  }

  "A state with all currency pairs subscribed" should "be subscribed" in new TestContext {

    val newState = initialState.currencyPairSubscribed(true, CurrencyPair.EurUsd).currencyPairSubscribed(true, CurrencyPair.Us30)

    assertResult(Set.empty, "unsubscribed are wrong")(newState.unsubscribed)

    assertResult(Set(CurrencyPair.EurUsd, CurrencyPair.Us30), "subscribed are wrong")(newState.subscribed)

  }

  "A state" should "have some currency pairs subscribed some unsubscribed and some waitingForResult" in new TestContext {

    val newState = initialState.currencyPairSubscribed(true, CurrencyPair.EurUsd)

    assertResult(true, "should be waiting")(newState.someSubscriptionsAreStillMissing)

    assertResult(Set.empty, "unsubscribed are wrong")(newState.unsubscribed)

    newState.subscribed shouldBe Set(CurrencyPair.EurUsd)

    newState.waitingForResult shouldBe Set(CurrencyPair.Us30)

    newState.isAlreadySubscribed(CurrencyPair.Us30) shouldBe true

    newState.isAlreadySubscribed(CurrencyPair.EurUsd) shouldBe false

    val finalState = newState.currencyPairSubscribed(false, CurrencyPair.Us30)

    assertResult(false, "should not be waiting for result")(finalState.someSubscriptionsAreStillMissing)

    assertResult(Set.empty, "should not be waiting for result")(finalState.waitingForResult)

  }

  "subscriberOf" should "return the right actorRef before any subscription attempt. Once subscribed no actorRef is return" in new TestContext {

    assertResult(Some(us30ActorRef), "Us30 actorRef is wrong")(initialState.subscriberOf(CurrencyPair.Us30))
    assertResult(Some(eurUsdActorRef), "EurUsd actorRef is wrong")(initialState.subscriberOf(CurrencyPair.EurUsd))
    assertResult(None, "CurrencyPair.EurJpy should have not a actorRef associated to it")(initialState.subscriberOf(CurrencyPair.EurJpy))

    var newState = initialState.currencyPairSubscribed(true, CurrencyPair.EurUsd)
    assertResult(None, "EurUsd actorRef is wrong")(newState.subscriberOf(CurrencyPair.EurUsd))

    newState = newState.currencyPairSubscribed(false, CurrencyPair.Us30)
    assertResult(None, "Us30 actorRef is wrong")(newState.subscriberOf(CurrencyPair.Us30))

    newState = newState.currencyPairSubscribed(false, CurrencyPair.EurJpy)
    assertResult(None, "EurJpy actorRef is wrong")(newState.subscriberOf(CurrencyPair.EurJpy))
  }

  "childrenWaitingForResult" should "return the right actorRef" in new TestContext {

    assertResult(Set(eurUsdActorRef, us30ActorRef), "childrenWaitingForResult")(initialState.childrenWaitingForResult)

    var newState = initialState.currencyPairSubscribed(true, CurrencyPair.EurUsd)
    assertResult(Set(us30ActorRef), "childrenWaitingForResult")(newState.childrenWaitingForResult)

    assertResult(Set.empty, "childrenWaitingForResult")(newState.currencyPairSubscribed(false, CurrencyPair.Us30).childrenWaitingForResult)
  }

  "asSubscriptionOf" should "take in account currency waiting for subscription as unsubscribed" in {

    val aPartiallyConstructedState = StateUnderConstruction.emptyState
      .record(CurrencyPair.EurGbp, eurGbpActorRef)
      .record(CurrencyPair.EurUsd, eurUsdActorRef)
      .record(CurrencyPair.Us30, us30ActorRef)
      .done()

    val state = State
      .createAState(
        aPartiallyConstructedState,
        CurrencyPairsSubscriberProtocol.Subscribe(Set(CurrencyPair.EurGbp, CurrencyPair.EurUsd, CurrencyPair.Us30), socketId, replyTo)
      )
      .currencyPairSubscribed(true, CurrencyPair.EurUsd)
      .currencyPairSubscribed(false, CurrencyPair.EurGbp)

    assertResult(Subscriptions(socketId, subscribe = Set(CurrencyPair.EurUsd), unsubscribe = Set(CurrencyPair.EurGbp, CurrencyPair.Us30)), "subscriptions")(
      state.asSubscription
    )

  }

  trait TestContext {

    val aPartiallyConstructedState =
      StateUnderConstruction.emptyState.record(CurrencyPair.EurUsd, eurUsdActorRef).record(CurrencyPair.Us30, us30ActorRef).done()

    val initialState =
      State.createAState(aPartiallyConstructedState, CurrencyPairsSubscriberProtocol.Subscribe(Set(CurrencyPair.EurUsd, CurrencyPair.Us30), socketId, replyTo))

    val allExistingCurrencies: Set[CurrencyPair] = Set(CurrencyPair.EurUsd, CurrencyPair.Us30)
  }

  "Given all currency pairs still missing, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = allExistingCurrencies, subscribed = Set(), unsubscribed = Set())

    assertResult(actualSummary, "summary")(initialState.summary)

    assertResult(actualSummary.currencyPairs, "currencyPairs")(allExistingCurrencies)

  }

  "Given all currency pairs subscribed, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = Set(), subscribed = allExistingCurrencies, unsubscribed = Set())

    assertResult(actualSummary, "summary")(
      initialState.currencyPairSubscribed(true, CurrencyPair.EurUsd).currencyPairSubscribed(true, CurrencyPair.Us30).summary
    )

    assertResult(actualSummary.currencyPairs, "currencyPairs")(allExistingCurrencies)

  }

  "Given all currency pairs unsubscribed, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = Set(), subscribed = Set(), unsubscribed = allExistingCurrencies)

    assertResult(actualSummary, "summary")(
      initialState.currencyPairSubscribed(false, CurrencyPair.EurUsd).currencyPairSubscribed(false, CurrencyPair.Us30).summary
    )

    assertResult(actualSummary.currencyPairs, "currencyPairs")(allExistingCurrencies)

  }

  "Given some currency pairs missing and some subscribed, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = Set(CurrencyPair.EurUsd), subscribed = Set(CurrencyPair.Us30), unsubscribed = Set())

    assertResult(actualSummary, "summary")(initialState.currencyPairSubscribed(true, CurrencyPair.Us30).summary)

    assertResult(actualSummary.currencyPairs, "currencyPairs")(Set(CurrencyPair.EurUsd, CurrencyPair.Us30))

  }

  "Given some currency pairs subscribed and some unsubscribed, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = Set(), subscribed = Set(CurrencyPair.Us30), unsubscribed = Set(CurrencyPair.EurUsd))

    assertResult(actualSummary, "summary")(
      initialState.currencyPairSubscribed(true, CurrencyPair.Us30).currencyPairSubscribed(false, CurrencyPair.EurUsd).summary
    )

    assertResult(actualSummary.currencyPairs, "currencyPairs")(Set(CurrencyPair.EurUsd, CurrencyPair.Us30))

  }

  "Given some currency pairs missing and some unsubscribed, summary" should "be correct" in new TestContext {

    val actualSummary = State.Summary(socketId, waitingResult = Set(CurrencyPair.Us30), subscribed = Set(), unsubscribed = Set(CurrencyPair.EurUsd))

    assertResult(actualSummary, "summary")(initialState.currencyPairSubscribed(false, CurrencyPair.EurUsd).summary)

    assertResult(actualSummary.currencyPairs, "currencyPairs")(Set(CurrencyPair.EurUsd, CurrencyPair.Us30))

  }

  "actorNameDecorator" should "be correct for a currency pair" in {
    CurrencyPairsSubscriberModel.actorNameDecorator(toBeDecorated = "actorName", currencyPair = CurrencyPair.EurUsd) shouldBe "actorName-eur_usd"
  }

  "actorNameDecorator" should "be correct for an index" in {
    CurrencyPairsSubscriberModel.actorNameDecorator(toBeDecorated = "actorName", currencyPair = CurrencyPair.Us30) shouldBe "actorName-us30"
  }

}
