package org.binqua.forex.feed.notifier

import cats.kernel.Eq
import org.binqua.forex.advisor.model.{AccountCurrency, CurrencyPair, PosBigDecimal}
import org.binqua.forex.feed.notifier.collaborators.{DueToPipValue, DueToUpdatedQuoteDirectly, UpdatingAccount}
import org.binqua.forex.feed.reader.QuoteGen
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.implicits.instances.accountCurrency._
import org.binqua.forex.implicits.instances.baseCurrency._
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.implicits.instances.id._
import org.binqua.forex.implicits.instances.quoteCurrency._
import org.binqua.forex.util.Grammar.in
import org.binqua.forex.util.{Bug, TestingFacilities}
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import scala.util.{Failure, Success, Try}

class UpdatingAccountSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 20)

  type DOUBLE_PAIR_ASSERT_PATTERN = (Quote, Quote, PosBigDecimal, AccountCurrency) => Any

  type SINGLE_PAIR_ASSERT_PATTERN = (Quote, PosBigDecimal) => Any


  val runAndDecorate: (=> Any, Quote, Quote, PosBigDecimal, AccountCurrency) => Any = (toRun, updateQuote, extraQuote, actualPipValue, ac) =>
    Try(toRun) match {
      case Success(_) =>
      case Failure(error) => thisTestShouldNotHaveArrivedHere(Map("updatedQuote" -> updateQuote, "extraQuote" -> extraQuote, "actualPipValue" -> actualPipValue, "accountCurrency" -> ac, "error" -> error))
    }

  val assertFor_c1c2_c2ac_pattern: DOUBLE_PAIR_ASSERT_PATTERN = (updateQuote, extraQuote, actualPipValue, ac) =>
    runAndDecorate((updateQuote.rates.currencyPair.quoteCurrency.pipValue * extraQuote.rates.buy) should ===(actualPipValue), updateQuote, extraQuote, actualPipValue, ac)

  val assertFor_c1c2_acC2_pattern: DOUBLE_PAIR_ASSERT_PATTERN = (updateQuote, extraQuote, actualResult, ac) =>
    runAndDecorate((actualResult * extraQuote.rates.buy) should ===(updateQuote.rates.currencyPair.quoteCurrency.pipValue), updateQuote, extraQuote, actualResult, ac)

  val assertFor_c1c2_left_ac_pattern: SINGLE_PAIR_ASSERT_PATTERN = (updateQuote, actualResult) =>
    (actualResult * updateQuote.rates.buy) should ===(updateQuote.rates.currencyPair.quoteCurrency.pipValue)

  val assertFor_c1c2_right_ac_pattern: SINGLE_PAIR_ASSERT_PATTERN = (updateQuote, actualResult) =>
    actualResult should ===(updateQuote.rates.currencyPair.quoteCurrency.pipValue)

  property("Given a quote with a base currency or a quote currency that is associated to some account currency," +
    "then an instance of Updating account can be created correctly") {

    def accountCurrencyEqvToBaseCurrencyOf: Quote => AccountCurrency => Boolean = quote => ac => Eq.eqv(ac.toId, quote.rates.currencyPair.baseCurrency.toId)

    def accountCurrencyEqvToQuoteCurrencyOf: Quote => AccountCurrency => Boolean = quote => ac => Eq.eqv(ac.toId, quote.rates.currencyPair.quoteCurrency.toId)

    forAll((QuoteGen.quotes, "quote")) { quote =>

      AccountCurrency.values.find(accountCurrencyEqvToBaseCurrencyOf(quote)) match {
        case Some(accountCurrency) => assertICanBuildAUpdatingAccountInstance(quote, accountCurrency, assertFor_c1c2_left_ac_pattern)
        case _ =>
      }

      AccountCurrency.values.find(accountCurrencyEqvToQuoteCurrencyOf(quote)) match {
        case Some(accountCurrency) => assertICanBuildAUpdatingAccountInstance(quote, accountCurrency, assertFor_c1c2_right_ac_pattern)
        case _ =>
      }
    }

  }

  property("Given a quote with a currencyPair that is not associated to any account currency " +
    "then an instance of Updating account cannot be created because we would need an extra quote for the pip value") {

    val quoteSuitableForUsdOnlyAccountCurrency = List(CurrencyPair.Us30, CurrencyPair.Spx500)

    forAll((QuoteGen.quotesOf(quoteSuitableForUsdOnlyAccountCurrency.contains(_)), "quote")) { quote =>

      List(AccountCurrency.Gbp, AccountCurrency.Eur).foreach(ac => {
        info("----")
        Given(s"A quote $quote")
        And(s"an account currency $ac")
        assertResult(None, s"account currency $ac quote $quote")(UpdatingAccount.create(quote, ac))
        Then("I cannot create an UpdatingAccount with them")
      })
    }

  }

  def chooseAsserter(updatedQuoteRole: Quote, extraQuoteCurrencyPair: CurrencyPair): DOUBLE_PAIR_ASSERT_PATTERN = {
    if (Eq.eqv(updatedQuoteRole.rates.currencyPair.quoteCurrency.toId, extraQuoteCurrencyPair.quoteCurrency.toId)) {
      assertFor_c1c2_acC2_pattern
    } else if (Eq.eqv(updatedQuoteRole.rates.currencyPair.quoteCurrency.toId, extraQuoteCurrencyPair.baseCurrency.toId)) {
      assertFor_c1c2_c2ac_pattern
    } else {
      throw new Bug(s"this should not be possible:\nupdatedQuoteRole:\n$updatedQuoteRole\nextraQuoteCurrencyPair:\n$extraQuoteCurrencyPair")
    }
  }

  property("Given an updatedQuoteRole I need a specific extra quote to create the UpdatingAccount depending on the account currency. If I dont have it I cannot create the UpdatingAccount") {

    forAll((QuoteGen.quotes, "updatedQuoteRole")) { updatedQuoteRole =>

      val (baseCurrencyId, quoteCurrencyId) = updatedQuoteRole.rates.currencyPair.toIds.ids
      val idsToBeFilteredOut = List(baseCurrencyId, quoteCurrencyId)

      val accountCurrenciesThatNeedExtraQuote = AccountCurrency.values.filterNot(ac => idsToBeFilteredOut.contains(ac.toId))

      accountCurrenciesThatNeedExtraQuote.foreach { ac => {

        val (validExtraQuoteCurrencyPair, invalidExtraQuoteCurrencyPair) = findExtraQuoteCurrencyPair(updatedQuoteRole, ac)

        assertCreationSuccessful(updatedQuoteRole, validExtraQuoteCurrencyPair, ac, chooseAsserter(updatedQuoteRole, validExtraQuoteCurrencyPair))

        assertCreationUnsuccessful(updatedQuoteRole, invalidExtraQuoteCurrencyPair, ac)
      }
      }
    }
  }

  private def assertCreationUnsuccessful(updatedQuoteRole: Quote, invalidExtraQuoteCurrencyPair: CurrencyPair, ac: AccountCurrency) = {

    info("----")
    Given(s"A quote $updatedQuoteRole")

    And(s"a account currency $ac")

    val wrongExtraQuote = QuoteGen.quotesOf(Eq.eqv(invalidExtraQuoteCurrencyPair, _)).sample.get
    And(s"an extra quote $wrongExtraQuote")

    val error: String = UpdatingAccount.validate(updatedQuoteRole, extraQuoteForPipCalculation = wrongExtraQuote, accountCurrency = ac, updateDueTo = DueToPipValue).swap
      .getOrElse(failureMessage(updatedQuoteRole, ac, wrongExtraQuote))

    Then(s"I cannot create an UpdatingAccount and I get a nice message:\n$error")

    assertResult(s"Cannot create an instance of UpdatingAccount with:\nupdatedQuote= $updatedQuoteRole\nextraQuoteForPipCalculation= $wrongExtraQuote\naccountCurrency= $ac\nupdateDueTo= DueToPipValue")(error)

  }

  private def failureMessage(updatedQuoteRole: Quote, ac: AccountCurrency, wrongExtraQuote: Quote) = {
    thisTestShouldNotHaveArrivedHere(Map("updatedQuoteRole" -> updatedQuoteRole, "extraQuoteForPipCalculation" -> wrongExtraQuote, "accountCurrency" -> ac, "updateDueTo" -> DueToPipValue))
  }

  private def assertCreationSuccessful(updatedQuoteRole: Quote, extraQuoteCurrencyPair: CurrencyPair, ac: AccountCurrency, pipValueAsserter: DOUBLE_PAIR_ASSERT_PATTERN): Unit = {

    val extraQuote = QuoteGen.quotesOf(Eq.eqv(extraQuoteCurrencyPair, _)).sample.get

    info("----")
    Given(s"A quote $updatedQuoteRole")

    And(s"a account currency $ac")

    And(s"an extra quote $extraQuote")

    val actualInstance: UpdatingAccount = UpdatingAccount.validate(updatedQuoteRole, extraQuote, ac, updateDueTo = DueToPipValue)
      .getOrElse(failureMessage(updatedQuoteRole, ac, extraQuote))

    assertResult(updatedQuoteRole)(actualInstance.incomingUpdatedQuote)
    assertResult(Some(extraQuote))(actualInstance.maybeAnExtraQuote)
    assertResult(DueToPipValue)(actualInstance.updateDueTo)
    assertResult(ac)(actualInstance.theAccountCurrency)

    pipValueAsserter(updatedQuoteRole, extraQuote, actualInstance.pipValue, ac)

    Then(s"I can create an UpdatingAccount:\n$actualInstance")
  }

  private def findExtraQuoteCurrencyPair(updatedQuote: Quote, accountCurrencyThatNeedExtraQuote: AccountCurrency): (CurrencyPair, CurrencyPair) = {

    val validExtraQuoteCurrencyPair = CurrencyPair.currencyPairNeededForPipValueCalculation(in(accountCurrencyThatNeedExtraQuote))(updatedQuote.rates.currencyPair.quoteCurrency)(CurrencyPair.values)

    validExtraQuoteCurrencyPair match {
      case Right(validCurrencyPair) =>
        val invalidExtraQuote = CurrencyPair.values.filter(Eq.neqv(validCurrencyPair, _))
        (validCurrencyPair, Gen.oneOf(invalidExtraQuote).sample.get)
      case Left(error) => throw new IllegalArgumentException(error)
    }
  }

  private def assertICanBuildAUpdatingAccountInstance(quote: Quote, accountCurrency: AccountCurrency, asserter: SINGLE_PAIR_ASSERT_PATTERN): Any = {
    info("----")
    Given(s"A quote $quote")
    And(s"an account currency $accountCurrency")
    val instance = UpdatingAccount.create(quote, accountCurrency).getOrElse(thisTestShouldNotHaveArrivedHere(Map("quote" -> quote, "accountCurrency" -> accountCurrency)))
    assertResult(quote)(instance.incomingUpdatedQuote)
    assertResult(None)(instance.maybeAnExtraQuote)
    assertResult(DueToUpdatedQuoteDirectly)(instance.updateDueTo)
    assertResult(accountCurrency)(instance.theAccountCurrency)
    asserter(quote, instance.pipValue)
    Then(s"I can create an UpdatingAccount with them:\n$instance")
  }
}
