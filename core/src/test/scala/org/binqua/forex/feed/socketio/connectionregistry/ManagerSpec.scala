package org.binqua.forex.feed.socketio.connectionregistry

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.feed.socketio.connectionregistry.Manager.{Message, Register, WrappedHealthy}
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck.CassandraHealthCheck.NotifyWhenHealthy
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.NewChildMaker.{ChildNamePrefix, ChildrenFactory}
import org.binqua.forex.util.{AkkaTestingFacilities, NewChildMaker, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class ManagerSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given registry in under initialization, it" should "log it without forwarding the message to the registry. No registry child is created!!" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {

      matchDistinctLogs(messages = supportMessages.connectionRegisterUnderInitialization()).expect {
        underTest ! registerMessage
      }

      persistenceConnectionRegistryContext.probe.expectNoMessage()

      matchDistinctLogs(messages = supportMessages.connectionRegisterUnderInitialization()).expect {
        underTest ! registerMessage
      }

      persistenceConnectionRegistryContext.probe.expectNoMessage()

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 0)

    }

  "Given health check is passed, it" should "log it and start forwarding messages to the registry" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 0)

      underTest ! registerMessage

      matchDistinctLogs(messages = supportMessages.connectionRegisterHealthy()).expect {
        fishExactlyOneMessageAndIgnoreOthers(healthCheckContext.probe) { messageToBeFished =>
          val NotifyWhenHealthy(actorRef) = messageToBeFished
          actorRef ! CassandraHealthCheck.Healthy
        }
      }

      persistenceConnectionRegistryContext.assertChildrenSpawnUntilNowAre(expNumber = 1)

      underTest ! registerMessage

      persistenceConnectionRegistryContext.probe.expectMessage(
        Persistence.Register(registerMessage.registrationRequester, registerMessage.subscriptionStarter, registerMessage.requesterAlias)
      )

      underTest ! registerMessage

      persistenceConnectionRegistryContext.probe.expectMessage(
        Persistence.Register(registerMessage.registrationRequester, registerMessage.subscriptionStarter, registerMessage.requesterAlias)
      )

    }

  "Once healthy" should "accept only register" in
    new TestContext(
      TestContext.PersistenceConnectionRegistryContext.thatIgnoreAnyMessage(),
      TestContext.CassandraHealthCheckContext.thatIgnoreAnyMessage(),
      supportMessages = TestContext.SupportMessages.justForAnIdea
    ) {
      underTest ! WrappedHealthy(CassandraHealthCheck.Healthy)
      List(WrappedHealthy(null)).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Register)))
    }

  object TestContext {

    object PersistenceConnectionRegistryContext {

      type theType = NewActorCollaboratorTestContext[Message, Persistence.Command]

      abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

        override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("PersistenceConnectionRegistry"))

        override val name: String = childNamePrefix.prefix.value

        override val probe: TestProbe[Persistence.Command] = createTestProbe()

        override val childrenFactoryContext: NewChildMaker.ChildrenFactory[Message, Persistence.Command] =
          ChildrenFactory(childMakerFactory()(), childNamePrefix)

        override def behavior: Behavior[Persistence.Command]

        override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, Persistence.Command] =
          throw new IllegalAccessException("Not used in this case")

        override def childMakerFactory(): CHILD_MAKER_FACTORY[Message, Persistence.Command] =
          smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

        override def ref: ActorRef[Persistence.Command] = spawn(Behaviors.monitor(probe.ref, behavior))
      }

      def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
        new Base(smartDeadWatcher) {
          override def behavior: Behavior[Persistence.Command] = Behaviors.ignore
        }
      }

    }

    object CassandraHealthCheckContext {

      type theType = NewActorCollaboratorTestContext[Message, CassandraHealthCheck.NotifyWhenHealthy]

      abstract class Base(val smartDeadWatcher: SmartDeadWatcher) extends theType(smartDeadWatcher) {

        override val childNamePrefix: ChildNamePrefix = ChildNamePrefix(refineMV[NonEmpty]("CassandraHealthCheck"))

        override val name: String = childNamePrefix.prefix.value

        override val probe: TestProbe[CassandraHealthCheck.NotifyWhenHealthy] = createTestProbe()

        override val childrenFactoryContext: NewChildMaker.ChildrenFactory[Message, CassandraHealthCheck.NotifyWhenHealthy] =
          ChildrenFactory(childMakerFactory()(), childNamePrefix)

        override def behavior: Behavior[CassandraHealthCheck.NotifyWhenHealthy]

        override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, CassandraHealthCheck.NotifyWhenHealthy] =
          throw new IllegalAccessException("Not used in this case")

        override def childMakerFactory(): CHILD_MAKER_FACTORY[Manager.Message, CassandraHealthCheck.NotifyWhenHealthy] =
          smartDeadWatcher.createAChildMakerX(childMakerNaming(name), probe.ref, behavior)

        override def ref: ActorRef[CassandraHealthCheck.NotifyWhenHealthy] = spawn(Behaviors.monitor(probe.ref, behavior))
      }

      def thatIgnoreAnyMessage(): SmartDeadWatcher => theType = { smartDeadWatcher =>
        new Base(smartDeadWatcher) {
          override def behavior: Behavior[CassandraHealthCheck.NotifyWhenHealthy] = Behaviors.ignore
        }
      }

    }

    object SupportMessages {

      val justForAnIdea = new SupportMessages {
        override def connectionRegisterUnderInitialization() = "Not ready yet"

        override def connectionRegisterHealthy() = "Healthy"
      }

    }

  }

  case class TestContext(
      persistenceConnectionRegistryContextMaker: SmartDeadWatcher => TestContext.PersistenceConnectionRegistryContext.theType,
      healthCheckContextMaker: SmartDeadWatcher => TestContext.CassandraHealthCheckContext.theType,
      supportMessages: SupportMessages
  ) extends BaseTestContext(underTestNaming) {

    val persistenceConnectionRegistryContext = persistenceConnectionRegistryContextMaker(actorsWatchers)

    val healthCheckContext = healthCheckContextMaker(actorsWatchers)

    val whoRequestToRegisterProbe = createTestProbe[Persistence.Response]("registrationRequester")

    val subscriptionStarterProbe = createTestProbe[SubscriptionStarterProtocol.NewSocketId]("subscriptionStarter")

    private val behaviourUnderTest = Manager(
      persistenceChildMaker = persistenceConnectionRegistryContext.childrenFactoryContext,
      healthCheckChildMaker = healthCheckContext.childrenFactoryContext,
      supportMessages = supportMessages
    )

    val underTest: ActorRef[Manager.Message] = spawn(behaviourUnderTest, underTestNaming.next())

    val registerMessage = Register(
      registrationRequester = whoRequestToRegisterProbe.ref,
      subscriptionStarter = subscriptionStarterProbe.ref,
      requesterAlias = "subscriptionStarter"
    )

  }

}
