package org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol.{ServerError, SubscriptionFailure, SubscriptionSuccess}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberModel.RetryingDueToHttpServerFailure
import org.binqua.forex.feed.starter.controller.collaboratorswatcher.currencypairs.currencypair.CurrencyPairSubscriberProtocol._
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.{AkkaTestingFacilities, AkkaUtil, ErrorSituationHandler, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._

class CurrencyPairSubscriberActorSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given StartSubscription is received, the actor" should "contact httpSubscriber and after a successful response, socketIOClient. Then should wait to be stopped" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

      val (deadWatcher, underTest) = specialSpawn(CurrencyPairSubscriberActor(retryInterval, justForAnIdeaSupportMessages))

      underTest ! startSubscriptionMessage

      matchDistinctLogs(messages = justForAnIdeaSupportMessages.waitingToBeStoppedBehavior(startSubscriptionMessage.currencyPair))
        .expect(fishExactlyOneMessageAndIgnoreOthers(httpSubscriberServiceProbe) { toBeMatched =>
          val HttpClientSubscriberServiceProtocol.SubscribeTo(actualCurrencyPair, actualSocketId, replyTo) = toBeMatched
          assertResult(startSubscriptionMessage.currencyPair)(actualCurrencyPair)
          assertResult(startSubscriptionMessage.socketId)(actualSocketId)
          replyTo ! SubscriptionSuccess(startSubscriptionMessage.currencyPair, startSubscriptionMessage.socketId)
        })

      socketIOClientProbe.expectMessage(
        SocketIOClientProtocol.SubscribeTo(
          startSubscriptionMessage.currencyPair,
          startSubscriptionMessage.socketId,
          whoWantsToKnoAboutSubscriptionResult.ref,
          internalFeedProbe.ref
        )
      )

      underTestCanBeStoppedBy[Command](_ ! Stop, underTest, deadWatcher)

    }

  "Once received StartSubscription, the actor" should "treat PrivateRetryNow and extra StartSubscription as unhandled" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

      val (deadWatcher, underTest) = specialSpawn(CurrencyPairSubscriberActor(retryInterval, justForAnIdeaSupportMessages))

      underTest ! startSubscriptionMessage

      List(
        PrivateRetryNow,
        startSubscriptionMessage
      ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Stop, WrappedHttpClientSubscriberResponse)))

      underTestCanBeStoppedBy[Command](_ ! Stop, underTest, deadWatcher)

    }

  "In waitToBeStopped, the actor" should "handel only stop" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

      val (deadWatcher, underTest) = specialSpawn(CurrencyPairSubscriberActor(retryInterval, justForAnIdeaSupportMessages))

      underTest ! startSubscriptionMessage

      fishExactlyOneMessageAndIgnoreOthers(httpSubscriberServiceProbe) { toBeMatched =>
        val HttpClientSubscriberServiceProtocol.SubscribeTo(_, _, replyTo) = toBeMatched
        replyTo ! SubscriptionSuccess(startSubscriptionMessage.currencyPair, startSubscriptionMessage.socketId)
      }

      List(
        PrivateRetryNow,
        startSubscriptionMessage,
        WrappedHttpClientSubscriberResponse(SubscriptionSuccess(currencyPair, socketId))
      ).foreach(BaseTestContext.assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Stop)))

      underTestCanBeStoppedBy[Command](_ ! Stop, underTest, deadWatcher)

    }
  "Given StartSubscription is received, the actor" should "send SubscribeTo to the httpSubscriberActor. When it receives HttpServerError back, it should start retrying and ignore any message that is not a PrivateRetryNow." +
    "When retrying succeeded then subscription process continue with socketIOClientActor" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

      val httpRetryInterval = 300.millis

      val tooEarlyNotRetriedYet = httpRetryInterval - 200.millis

      val (deadWatcher, underTest) = specialSpawn(CurrencyPairSubscriberActor(httpRetryInterval, justForAnIdeaSupportMessages))

      underTest ! startSubscriptionMessage

      val subscriptionSuccessAreUnhandledDuringRetry = WrappedHttpClientSubscriberResponse(SubscriptionSuccess(currencyPair, socketId))
      val incomingHttpServerErrorAreUnhandledDuringRetry = WrappedHttpClientSubscriberResponse(incomingHttpServerError)

      matchDistinctLogs(
        messages = justForAnIdeaSupportMessages.message(RetryingDueToHttpServerFailure(1, httpRetryInterval, incomingHttpServerError)),
        justForAnIdeaSupportMessages.message(RetryingDueToHttpServerFailure(2, httpRetryInterval, incomingHttpServerError))
      ).expect {

        fishOneMessageAndIgnoreOthers(httpSubscriberServiceProbe, httpRetryInterval) { toBeMatched =>
          val HttpClientSubscriberServiceProtocol.SubscribeTo(_, _, replyToTheActorUnderTest) = toBeMatched
          replyToTheActorUnderTest ! incomingHttpServerError
        }

        httpSubscriberServiceProbe.expectNoMessage(tooEarlyNotRetriedYet)

        matchLogsWithRepetition(AkkaUtil.commandIgnoredMessage(startSubscriptionMessage)(TheOnlyHandledMessages(PrivateRetryNow))).expect {
          underTest ! startSubscriptionMessage
        }

        matchDistinctLogs(
          messages = AkkaUtil.commandIgnoredMessage(subscriptionSuccessAreUnhandledDuringRetry)(TheOnlyHandledMessages(PrivateRetryNow)),
          AkkaUtil.commandIgnoredMessage(incomingHttpServerErrorAreUnhandledDuringRetry)(TheOnlyHandledMessages(PrivateRetryNow))
        ).expect {
          fishOneMessageAndIgnoreOthers(httpSubscriberServiceProbe, httpRetryInterval) { toBeMatched =>
            val HttpClientSubscriberServiceProtocol.SubscribeTo(_, _, replyToTheActorUnderTest) = toBeMatched
            replyToTheActorUnderTest ! incomingHttpServerError
            replyToTheActorUnderTest ! subscriptionSuccessAreUnhandledDuringRetry.response
            replyToTheActorUnderTest ! incomingHttpServerErrorAreUnhandledDuringRetry.response
          }
        }

        httpSubscriberServiceProbe.expectNoMessage(tooEarlyNotRetriedYet)

        fishOneMessageAndIgnoreOthers(httpSubscriberServiceProbe, httpRetryInterval) { toBeMatched =>
          val HttpClientSubscriberServiceProtocol.SubscribeTo(_, _, replyToTheActorUnderTest) = toBeMatched
          replyToTheActorUnderTest ! SubscriptionSuccess(currencyPair, socketId)
        }

        socketIOClientProbe.expectMessage(
          SocketIOClientProtocol.SubscribeTo(currencyPair, socketId, whoWantsToKnoAboutSubscriptionResult.ref, internalFeedProbe.ref)
        )

      }

      underTestCanBeStoppedBy[Command](_ ! Stop, underTest, deadWatcher)

    }

  "Given the actor is retrying because of a SubscriptionFailure, it" should "be stoppable" in
    new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

      val (deadWatcher, underTest) = specialSpawn(CurrencyPairSubscriberActor(100.millis, justForAnIdeaSupportMessages))

      underTest ! startSubscriptionMessage

      fishExactlyOneMessageAndIgnoreOthers(httpSubscriberServiceProbe) { toBeMatched =>
        val HttpClientSubscriberServiceProtocol.SubscribeTo(_, _, replyTo) = toBeMatched
        replyTo ! incomingHttpServerError
        replyTo ! incomingHttpServerError
        replyTo ! incomingHttpServerError
        replyTo ! incomingHttpServerError
      }

      underTestCanBeStoppedBy[Command](_ ! Stop, underTest, deadWatcher)

    }

  object SupportMessagesTestContext {

    private val justForAnIdeaErrorSituationHandler: ErrorSituationHandler = {
      case m: RetryingDueToHttpServerFailure => s"RetryingDueToHttpServerFailure $m"
    }

    val justForAnIdeaSupportMessages = new SupportMessages {

      override def message(failure: RetryingDueToHttpServerFailure): String = justForAnIdeaErrorSituationHandler.show(failure)

      override def waitingToBeStoppedBehavior(currencyPair: CurrencyPair): String = s"waitingToBeStoppedBehavior $currencyPair"
    }
  }

  case class TestContext(justForAnIdeaSupportMessages: SupportMessages) {

    def specialSpawn[T]: Behavior[T] => (DeadWatcher, ActorRef[T]) = DeadWatcher.spawn(testKit)

    val retryInterval: FiniteDuration = 100.millis
    val socketId = SocketId("123")
    val currencyPair: CurrencyPair = CurrencyPair.Us30

    val whoWantsToKnoAboutSubscriptionResult: TestProbe[SocketIOClientProtocol.SubscriptionsResult] =
      createTestProbe[SocketIOClientProtocol.SubscriptionsResult]()
    val httpSubscriberServiceProbe: TestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo] =
      createTestProbe[HttpClientSubscriberServiceProtocol.SubscribeTo]()
    val socketIOClientProbe: TestProbe[SocketIOClientProtocol.Command] = createTestProbe[SocketIOClientProtocol.Command]("socketIOClient-1")
    val internalFeedProbe: TestProbe[SocketIOClientProtocol.Response] = createTestProbe[SocketIOClientProtocol.Response]()

    val incomingHttpServerError = SubscriptionFailure(currencyPair, socketId, ServerError)
    val startSubscriptionMessage = StartSubscription(
      whoWantsToKnoAboutSubscriptionResult.ref,
      currencyPair,
      socketId,
      socketIOClientProbe.ref,
      httpSubscriberServiceProbe.ref,
      internalFeedProbe.ref
    )

    def underTestCanBeStoppedBy[T](f: ActorRef[T] => Unit, underTest: ActorRef[T], deadWatcher: DeadWatcher) = {
      deadWatcher.assertThatIsStillAlive(underTest)
      f(underTest)
      createTestProbe().expectTerminated(underTest)
    }

  }

}
