package org.binqua.forex.feed.socketio.connectionregistry.persistence.connector

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.Connector.{Connect, Message, Notify, WrappedSocketIOClientServiceResponse}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.notifier.ConnectionNotifierProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.SocketIOClientServiceProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.duration._

class ConnectorSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with MockFactory
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given Notify has not been received and given SocketIOClientService fails, the actor" should "spawn a new SocketIOClientService, watch it and connect it. If SocketIOClientService fails again, the actor keep to recreated it." in
    new TestContext(SocketIOClientServiceTestContext.thatFails(), ConnectionNotifierTestContext.thatIgnoreAnyMessage(), underTestNaming = underTestNaming) {

      private def makeTheSocketIOClientServiceChildFail = {
        lastSocketIOClientChildSpawned ! SocketIOClientServiceTestContext.aMessageThatMakeTheChildFailed
        socketIOClientContext.probe.expectMessageType[SocketIOClientServiceProtocol.Connect]
        waitALittleBit(soThat("child can be recreated"))
      }

      val aNewActorUnderTest = spawnActorUnderTest()

      aNewActorUnderTest ! Connect

      waitALittleBit(soThat("child can be recreated"))

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 1, because("created soon after spawn"))

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      makeTheSocketIOClientServiceChildFail

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 2, because("just recreated"))

      makeTheSocketIOClientServiceChildFail

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      socketIOClientContext.probe.expectNoMessage()

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 3, because("just recreated"))

    }

  "Given Notify has been received and then SocketIOClientService fails, the actor" should "spawn a new SocketIOClientService, watch it, connect it and Notify the ConnectionNotifier actor. If SocketIOClientService fails again, the actor keep recreating it." in
    new TestContext(SocketIOClientServiceTestContext.thatFails(), ConnectionNotifierTestContext.thatIgnoreAnyMessage(), underTestNaming = underTestNaming) {

      private def makeTheSocketIOClientServiceChildFail = {
        lastSocketIOClientChildSpawned ! SocketIOClientServiceTestContext.aMessageThatMakeTheChildFailed
        socketIOClientContext.probe.expectMessageType[SocketIOClientServiceProtocol.Connect]
        waitALittleBit(soThat("child can be recreated"))
      }

      val aNewActorUnderTest = spawnActorUnderTest()

      aNewActorUnderTest ! Connect

      private val aSubscriptionStarterRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

      aNewActorUnderTest ! Notify(aSubscriptionStarterRef)

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(aSubscriptionStarterRef))

      waitALittleBit(soThat("child can be recreated"))

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 1, because("created soon after spawn"))

      makeTheSocketIOClientServiceChildFail

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 2, because("just recreated"))
      assertNumberOfConnectionNotifierSpawnedIs(expNumber = 1, because("it did not fail"))

      makeTheSocketIOClientServiceChildFail

      val anotherSocketId = SocketId(randomString)
      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(anotherSocketId)
      })
      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(anotherSocketId))

      assertNumberOfSocketIOClientSpawnedIs(expNumber = 3, because("just recreated"))
      assertNumberOfConnectionNotifierSpawnedIs(expNumber = 1, because("did not fail it"))

    }

  "A soon as the actor received Connect, it" should "spawn a ConnectionNotifier child and SocketIOClientService child" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      waitALittleBit(soThat("child could be created if it has too, but it should not!!!"))

      assertNumberOfConnectionNotifierSpawnedIs(expNumber = 0, because("did not received Connect yet"))
      assertNumberOfSocketIOClientSpawnedIs(expNumber = 0, because("did not received Connect yet"))

      spawnActorUnderTest() ! Connect

      waitALittleBit(soThat("child can be recreated"))

      assertNumberOfConnectionNotifierSpawnedIs(expNumber = 1, because("just recreated"))
      assertNumberOfSocketIOClientSpawnedIs(expNumber = 1, because("just recreated"))

    }

  "The actor" should "accept only Connect as first message" in
    new TestContext(
      SocketIOClientServiceTestContext.thatIgnoreAnyMessage(),
      ConnectionNotifierTestContext.thatIgnoreAnyMessage(),
      underTestNaming = underTestNaming
    ) {

      private val underTest: ActorRef[Message] = spawnActorUnderTest()

      List(
        Notify(null),
        WrappedSocketIOClientServiceResponse(null),
        WrappedSocketIOClientServiceResponse(null)
      ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(Connect)))

      underTest ! Connect

      unhandledMessageSubscriber.expectNoMessage()
    }

  "The actor" should "treat Connect messages after the first one as unhandled. Once connected is connected ;-)" in
    new TestContext(
      SocketIOClientServiceTestContext.thatIgnoreAnyMessage(),
      ConnectionNotifierTestContext.thatIgnoreAnyMessage(),
      underTestNaming = underTestNaming
    ) {

      implicit val handled: TheOnlyHandledMessages = TheOnlyHandledMessages(Notify, WrappedSocketIOClientServiceResponse)

      private val underTest: ActorRef[Message] = spawnActorUnderTest()

      underTest ! Connect

      {
        LoggingTestKit.info(commandIgnoredMessage(Connect)).expect {
          underTest ! Connect
        }
      }

      unhandledMessageSubscriber.expectNoMessage()
    }

  "Given ConnectionNotifier child has received a NewSocketIdAvailable and a Notify message. If this child fails, the actor" should "recreate it and send the same messages again" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      private def makeConnectionNotifierChildFail = {
        lastConnectionNotifierChildSpawned ! ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed)
        waitALittleBit(soThat("child can be recreated"))
      }

      val underTest = deadWatcher.watch(spawnActorUnderTest())

      underTest ! Connect

      fishExactlyOneMessageAndIgnoreOthers(socketIOClientContext.probe)(toBeMatched => {
        val SocketIOClientServiceProtocol.Connect(replyTo) = toBeMatched
        replyTo ! SocketIOClientServiceProtocol.NewSocketId(theSocketId)
      })

      connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(theSocketId))

      {
        val lastSocketIdReceived: SocketId = theSocketId
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref
        underTest ! Notify(lastSubscriptionStarterReceivedRef)
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))
      }

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref
        underTest ! Notify(lastSubscriptionStarterReceivedRef)

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        val lastSocketIdReceived: SocketId = SocketId(randomString)
        underTest ! WrappedSocketIOClientServiceResponse(SocketIOClientServiceProtocol.NewSocketId(lastSocketIdReceived))
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))

        makeConnectionNotifierChildFail

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.NewSocketIdAvailable(lastSocketIdReceived))
      }

      unhandledMessageSubscriber.expectNoMessage()

      deadWatcher.assertThatIsStillAlive(underTest, 1.second)
    }

  "given ConnectionNotifier child has received only a Notify message. If the child fails, the actor" should "recreate it and send the same messages again" in
    new TestContext(SocketIOClientServiceTestContext.thatIgnoreAnyMessage(), ConnectionNotifierTestContext.thatFails(), underTestNaming = underTestNaming) {

      private def makeConnectionNotifierChildFail = {
        lastConnectionNotifierChildSpawned ! ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierTestContext.aMessageThatMakeTheChildFailed)
        waitALittleBit(soThat("child can be recreated"))
      }

      val underTest = deadWatcher.watch(spawnActorUnderTest())

      underTest ! Connect

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

        underTest ! Notify(lastSubscriptionStarterReceivedRef)

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))
      }

      {
        val lastSubscriptionStarterReceivedRef: ActorRef[SubscriptionStarterProtocol.NewSocketId] = aNewSubscriptionStarterProbe().ref

        underTest ! Notify(lastSubscriptionStarterReceivedRef)
        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

        makeConnectionNotifierChildFail

        connectionNotifierContext.probe.expectMessage(ConnectionNotifierProtocol.Notify(lastSubscriptionStarterReceivedRef))

      }

      unhandledMessageSubscriber.expectNoMessage()

      deadWatcher.assertThatIsStillAlive(underTest, 500.millis)
    }

  object SocketIOClientServiceTestContext {

    val actorPrefixName = "SocketIOClientService"

    val aMessageThatMakeTheChildFailed = SocketIOClientServiceProtocol.Connect(null)

    type SocketIOClientContextType = ActorCollaboratorTestContext[Message, SocketIOClientServiceProtocol.Connect]

    def lastChildSpawned(actorsWatchers: SmartDeadWatcher) =
      eventually(actorsWatchers.lastCreatedInstanceOf[SocketIOClientServiceProtocol.Command](actorPrefixName))

    trait Base extends SocketIOClientContextType {

      override val name: String = SocketIOClientServiceTestContext.actorPrefixName

      override val probe: TestProbe[SocketIOClientServiceProtocol.Connect] = createTestProbe()

      override def behavior: Behavior[SocketIOClientServiceProtocol.Connect]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, SocketIOClientServiceProtocol.Connect] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[SocketIOClientServiceProtocol.Connect] = throw new IllegalAccessException("Not used in this case")
    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[SocketIOClientServiceProtocol.Connect] = Behaviors.ignore

    }

    case class thatFails() extends Base {

      override def behavior: Behavior[SocketIOClientServiceProtocol.Connect] =
        Behaviors.setup(_ => {
          Behaviors.receiveMessage({
            case m: SocketIOClientServiceProtocol.Connect =>
              if (m == aMessageThatMakeTheChildFailed)
                throwIAmGoingToFailedException(name, m)
              Behaviors.same
          })
        })

    }

  }

  object ConnectionNotifierTestContext {

    val actorPrefixName = "ConnectionNotifier"

    val aMessageThatMakeTheChildFailed = ConnectionNotifierProtocol.Notify(null)

    def lastChildSpawned(actorsWatchers: SmartDeadWatcher) =
      eventually(actorsWatchers.lastCreatedInstanceOf[ConnectionNotifierProtocol.Command](actorPrefixName))

    type ConnectionNotifierContextType = ActorCollaboratorTestContext[Message, ConnectionNotifierProtocol.Command]

    trait Base extends ConnectionNotifierContextType {

      override val name: String = ConnectionNotifierTestContext.actorPrefixName

      override val probe: TestProbe[ConnectionNotifierProtocol.Command] = createTestProbe()

      override def behavior: Behavior[ConnectionNotifierProtocol.Command]

      override def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[Message, ConnectionNotifierProtocol.Command] =
        actorsWatchers.createAChildMaker(name, probe.ref, behavior)

      override def ref: ActorRef[ConnectionNotifierProtocol.Command] = throw new IllegalAccessException("Not used in this case")
    }

    case class thatIgnoreAnyMessage() extends Base {

      override def behavior: Behavior[ConnectionNotifierProtocol.Command] = Behaviors.ignore

    }

    case class thatFails() extends Base {

      override def behavior: Behavior[ConnectionNotifierProtocol.Command] =
        Behaviors.receiveMessage({ m =>
          if (m == aMessageThatMakeTheChildFailed)
            throwIAmGoingToFailedException
          Behaviors.same
        })
    }

  }

  case class TestContext(
      socketIOClientContext: SocketIOClientServiceTestContext.SocketIOClientContextType,
      connectionNotifierContext: ConnectionNotifierTestContext.ConnectionNotifierContextType,
      underTestNaming: UnderTestNaming
  ) extends BaseTestContext(underTestNaming) {

    val theSocketId = SocketId("1")

    val connectionNotifierChildMaker = connectionNotifierContext.childMakerFactory(actorsWatchers)()

    val socketIoClientChildMaker = socketIOClientContext.childMakerFactory(actorsWatchers)()

    def aNewSubscriptionStarterProbe() = createTestProbe[SubscriptionStarterProtocol.NewSocketId]()

    def spawnActorUnderTest: () => ActorRef[Message] =
      () =>
        spawn(
          Connector(
            socketIoClientChildMaker,
            connectionNotifierChildMaker
          ),
          name = underTestNaming.next()
        )

    def lastSocketIOClientChildSpawned = SocketIOClientServiceTestContext.lastChildSpawned(actorsWatchers)

    def lastConnectionNotifierChildSpawned = ConnectionNotifierTestContext.lastChildSpawned(actorsWatchers)

    def assertNumberOfSocketIOClientSpawnedIs(expNumber: Int, because: because) =
      assertChildSpawnAreExactly(SocketIOClientServiceTestContext.actorPrefixName, expNumber, because)

    def assertNumberOfConnectionNotifierSpawnedIs(expNumber: Int, because: because) =
      assertChildSpawnAreExactly(ConnectionNotifierTestContext.actorPrefixName, expNumber, because)

  }

}
