package org.binqua.forex.feed.reader

import akka.actor.testkit.typed.FishingOutcome
import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.Behaviors.monitor
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import cats.kernel.Eq
import com.typesafe.config.ConfigFactory
import monocle.macros.GenLens
import org.apache.commons.io.FileUtils
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.model.CurrencyPair.{EurUsd, GbpUsd}
import org.binqua.forex.feed.notifier.NewIncomingQuoteManager
import org.binqua.forex.feed.notifier.NewIncomingQuoteManager._
import org.binqua.forex.feed.reader
import org.binqua.forex.feed.reader.Reader.IncomingQuoteRecordedEvent
import org.binqua.forex.feed.reader.model.Quote
import org.binqua.forex.feed.reader.parsers.IncomingQuoteGen.anyCurrencyPair
import org.binqua.forex.feed.reader.parsers.{IncomingQuoteGen, IncomingQuoteReader}
import org.binqua.forex.util.AkkaTestingFacilities
import org.binqua.forex.util.ChildMaker.CHILD_MAKER
import org.scalacheck.Gen
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.{DoNotDiscover, GivenWhenThen}
import play.api.libs.json.{JsError, JsSuccess}

import java.io.File
import java.time.LocalDateTime
import java.util.concurrent.atomic.{AtomicInteger, AtomicReference}
import scala.concurrent.duration._

object ReaderDontRunAlone {

  private val baseDir = s"${FileUtils.getTempDirectoryPath}/forexAdvisorProject/test"

  val healthCheckConfig = ConfigFactory.parseString(s"""|akka {
        |  loglevel = "INFO"
        |  actor {
        |    serializers {jackson-json = "akka.serialization.jackson.JacksonJsonSerializer" }
        |    serialization-bindings { "org.binqua.forex.JsonSerializable" = jackson-json }
        |  }
        |
        |   persistence.journal.plugin = "akka.persistence.journal.leveldb"
        |   persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
        |
        |   persistence.journal.leveldb.dir = "$baseDir/journal"
        |   persistence.snapshot-store.local.dir = "$baseDir/snapshots"
        |
        |}
        |
        |org.binqua.forex.number.of.messages.for.snapshot = 3
        |org.binqua.forex.number.of.snapshots.to.keep = 2
        |
        |""".stripMargin)
}

@DoNotDiscover
class ReaderDontRunAlone
    extends ScalaTestWithActorTestKit(ReaderDontRunAlone.healthCheckConfig)
    with AnyFlatSpecLike
    with MockFactory
    with GivenWhenThen
    with AkkaTestingFacilities {

  val incomingQuoteReaderMock = mock[IncomingQuoteReader]

  val storageLocations = List(
    new File(system.settings.config.getString("akka.persistence.journal.leveldb.dir")),
    new File(system.settings.config.getString("akka.persistence.snapshot-store.local.dir"))
  )

  val snapshotsLocation = new File(system.settings.config.getString("akka.persistence.snapshot-store.local.dir"))

  override def beforeAll(): Unit = {
    super.beforeAll()
    storageLocations.foreach(FileUtils.deleteDirectory)
  }

  override def afterAll(): Unit = {
    storageLocations.foreach(FileUtils.deleteDirectory)
    super.afterAll()
  }

  private val quoteUpdatedAccountsOrganiserStubbedActor = Behaviors.receiveMessage[Command] { msg =>
    Behaviors.same
  }

  private val quoteUpdatedAccountsOrganiserStubbedActorProbe = testKit.createTestProbe[Command]()

  private val quoteUpdatedManagerChildMaker = (context: ActorContext[Reader.Command]) =>
    context.spawn(monitor(quoteUpdatedAccountsOrganiserStubbedActorProbe.ref, quoteUpdatedAccountsOrganiserStubbedActor), "quoteUpdatedAccountsOrganiserActor")

  "QuotesFeedActor" should "log info data when it has been spawned" in {

    val persistenceId = uniqueRandomPersistenceId
    LoggingTestKit.info(s"Starting QuotesFeedActor with persistenceId $persistenceId").expect {
      spawn(
        Reader(
          persistenceId,
          mock[IncomingQuoteReader],
          quoteUpdatedManagerChildMaker
        )
      )
    }
  }

  "Given QuotesFeedActor receives a valid IncomingQuote, it" should "parsed it and contact the quoteUpdatedAccountsOrganiserActor" in {

    val quotesFeedActorUnderTest = spawn(
      reader.Reader(
        uniqueRandomPersistenceId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    val anIncomingQuoteRecordedEvent: IncomingQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(anyCurrencyPair).sample.get._1

    val incomingJson = "incomingJson"

    (incomingQuoteReaderMock.parse _)
      .expects(incomingJson)
      .returning(JsSuccess(anIncomingQuoteRecordedEvent))

    quotesFeedActorUnderTest ! Reader.RecordIncomingQuoteCommand(incomingJson)

    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      QuoteUpdated(Quote.from(anIncomingQuoteRecordedEvent))
    )
  }

  "Given an empty feed and an incoming gbp/usd quote," +
    "when the json parse failed then child actor" should "not be notified and an error log reported" in {

    val incomingGbpUsdQuoteJson = "incomingGbpUsdQuoteAsJson"

    val expErrorMessage = "some error"
    (incomingQuoteReaderMock.parse _)
      .expects(incomingGbpUsdQuoteJson)
      .returning(JsError(expErrorMessage))

    val quotesFeedActorUnderTest = spawn(
      reader.Reader(
        uniqueRandomPersistenceId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    LoggingTestKit.error(expErrorMessage).expect {
      quotesFeedActorUnderTest ! Reader.RecordIncomingQuoteCommand(incomingGbpUsdQuoteJson)
      quoteUpdatedAccountsOrganiserStubbedActorProbe.expectNoMessage()
    }

  }

  "It" should "persist its state and notify the new child when the recovery is completed" in {

    val anIncomingGbpUsdQuoteRecordedEvent: IncomingQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(_ == GbpUsd).sample.get._1

    val incomingGbpUsdQuoteJson = "incomingGbpUsdQuoteAsJson"

    (incomingQuoteReaderMock.parse _)
      .expects(incomingGbpUsdQuoteJson)
      .returning(JsSuccess(anIncomingGbpUsdQuoteRecordedEvent))

    val anIncomingEurUsdQuoteRecordedEvent: IncomingQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(_ == EurUsd).sample.get._1

    val incomingEurUsdQuoteJson = "incomingEurUsdQuoteAsJson"

    (incomingQuoteReaderMock.parse _)
      .expects(incomingEurUsdQuoteJson)
      .returning(JsSuccess(anIncomingEurUsdQuoteRecordedEvent))

    val thePersistentId = uniqueRandomPersistenceId

    val aQuotesFeedActor = spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(incomingGbpUsdQuoteJson)

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(incomingEurUsdQuoteJson)

    val lastMessageToWaitingFor = QuoteUpdated(Quote.from(anIncomingEurUsdQuoteRecordedEvent))

    quoteUpdatedAccountsOrganiserStubbedActorProbe.fishForMessage(
      1.seconds,
      s"I was waiting for last message $lastMessageToWaitingFor to have a complete state"
    )(message => {
      if (message == lastMessageToWaitingFor) {
        FishingOutcome.Complete
      } else
        FishingOutcome.Continue
    })

    testKit.stop(aQuotesFeedActor, 1.seconds)

    spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    waitALittleBit(soThat("wait we are looking for will happen"))

    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      QuoteUpdated(Quote.from(anIncomingGbpUsdQuoteRecordedEvent))
    )

    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      QuoteUpdated(Quote.from(anIncomingEurUsdQuoteRecordedEvent))
    )
  }

  "It" should "persist messages depending on updated field: older messages will not replace most recent messages" in {

    val anIncomingGbpUsdQuoteRecordedEvent: IncomingQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(_ == GbpUsd).sample.get._1

    val theMostUpdatedIncomingGbpUsdQuoteAsJson = "incomingGbpUsdQuoteAsJson"
    (incomingQuoteReaderMock.parse _).expects(theMostUpdatedIncomingGbpUsdQuoteAsJson).returning(JsSuccess(anIncomingGbpUsdQuoteRecordedEvent))

    val anOlderIncomingGbpUsdQuoteJson = "olderIncomingGbpUsdQuoteAsJson"
    (incomingQuoteReaderMock.parse _)
      .expects(anOlderIncomingGbpUsdQuoteJson)
      .returning(JsSuccess(messageOlderThan(anIncomingGbpUsdQuoteRecordedEvent)))

    val anIncomingEurUsdQuoteRecordedEvent: IncomingQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(_ == EurUsd).sample.get._1

    val theMostUpdatedIncomingEurUsdQuoteAsJson = "incomingEurUsdQuoteAsJson"
    (incomingQuoteReaderMock.parse _).expects(theMostUpdatedIncomingEurUsdQuoteAsJson).returning(JsSuccess(anIncomingEurUsdQuoteRecordedEvent))

    val anOlderIncomingEurUsdQuoteJson = "olderIncomingEurUsdQuoteAsJson"
    (incomingQuoteReaderMock.parse _)
      .expects(anOlderIncomingEurUsdQuoteJson)
      .returning(JsSuccess(messageOlderThan(anIncomingEurUsdQuoteRecordedEvent)))

    val thePersistentId = uniqueRandomPersistenceId

    val aQuotesFeedActor = spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(theMostUpdatedIncomingGbpUsdQuoteAsJson)

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(theMostUpdatedIncomingEurUsdQuoteAsJson)

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(anOlderIncomingGbpUsdQuoteJson)

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(anOlderIncomingEurUsdQuoteJson)

    val lastMessageToWaitingForACompleteState = QuoteUpdated(Quote.from(anIncomingEurUsdQuoteRecordedEvent))

    quoteUpdatedAccountsOrganiserStubbedActorProbe.fishForMessage(
      1.second,
      s"I was waiting for last message $lastMessageToWaitingForACompleteState to have a complete state"
    )(message => {
      if (message == lastMessageToWaitingForACompleteState) {
        FishingOutcome.Complete
      } else
        FishingOutcome.ContinueAndIgnore
    })

    testKit.stop(aQuotesFeedActor, 500.milliseconds)

    spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      QuoteUpdated(Quote.from(anIncomingGbpUsdQuoteRecordedEvent))
    )
    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      QuoteUpdated(Quote.from(anIncomingEurUsdQuoteRecordedEvent))
    )
  }

  def aNumberOfMessagesWithIncrementalUpdatedDateTime(value: Int, updated: LocalDateTime): List[(String, IncomingQuoteRecordedEvent)] = {
    val dateTimes = (1 to value).map(index => updated.plusMinutes(1 * index))

    dateTimes
      .map(newDateTime => {

        val incomingGbpUsdQuoteRecordedEvent = IncomingQuoteGen.validIncomingQuotesOf(_ == GbpUsd).sample.get._1

        updatedDateTimeHook.modify(_ => newDateTime)(incomingGbpUsdQuoteRecordedEvent)

      })
      .sortWith((l, r) => l.updated.isBefore(r.updated))
      .zipWithIndex
      .map(x => (x._2.toString, x._1))
      .toList
  }

  private def updatedDateTimeHook = {
    GenLens[IncomingQuoteRecordedEvent](_.updated)
  }

  "It" should "use snapshot to persist its state and the number of snapshots and how many to keep depends on the configuration" in {

    val aDatetime = LocalDateTime.of(2020, 12, 25, 0, 0, 0)

    val messages4Snapshots = system.settings.config.getInt("org.binqua.forex.number.of.messages.for.snapshot")
    val snapshotsToKeep = system.settings.config.getInt("org.binqua.forex.number.of.snapshots.to.keep")

    val allMessages: List[(String, IncomingQuoteRecordedEvent)] = aNumberOfMessagesWithIncrementalUpdatedDateTime(messages4Snapshots * 3 + 1, aDatetime)

    allMessages.foreach(message => {
      val (json, parsedResult) = message
      (incomingQuoteReaderMock.parse _)
        .expects(json)
        .returning(JsSuccess(parsedResult))
    })

    val thePersistentId = uniqueRandomPersistenceId

    val aQuotesFeedActor = spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    allMessages.foreach(message => {
      val (json, _) = message
      aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(json)
    })

    val lastMessageToWaitingFor = QuoteUpdated(Quote.from(allMessages.reverse.head._2))

    quoteUpdatedAccountsOrganiserStubbedActorProbe.fishForMessage(
      500.milliseconds,
      s"I was waiting for last message $lastMessageToWaitingFor to have a complete state"
    )(message => {
      if (message == lastMessageToWaitingFor) {
        FishingOutcome.Complete
      } else
        FishingOutcome.Continue
    })

    testKit.stop(aQuotesFeedActor, 500.milliseconds)

    spawn(
      reader.Reader(
        thePersistentId,
        incomingQuoteReaderMock,
        quoteUpdatedManagerChildMaker
      )
    )

    quoteUpdatedAccountsOrganiserStubbedActorProbe.expectMessage(
      lastMessageToWaitingFor
    )

    val actualSnapshotFiles: java.util.Collection[_] = org.apache.commons.io.FileUtils.listFiles(snapshotsLocation, null, false)

    assertResult(snapshotsToKeep)(actualSnapshotFiles.size())

  }

  "Give a child that will crash then QuotesFeedActor" should "restart it every time the child fails" in {
    info("----")
    import org.binqua.forex.implicits.instances.currencyPair._

    def childBehaviourCrashedByGbpUsd(counter: Int) =
      Behaviors.receiveMessage[Command] { msg =>
        msg match {
          case QuoteUpdated(updatedQuote) =>
            if (Eq.eqv(updatedQuote.rates.currencyPair, CurrencyPair.GbpUsd)) {
              throw new IllegalStateException(s"Simulating a crash number $counter")
            }
        }
        Behaviors.same
      }

    val firsChildMonitoringProbe = testKit.createTestProbe[Command]("childMonitoringProbe")

    val secondChildMonitoringProbe = testKit.createTestProbe[Command]("secondChildMonitoringProbe")

    val thirdChildMonitoringProbe = testKit.createTestProbe[Command]("thirdChildMonitoringProbe")

    val theChildActorRefRepo = new AtomicReference[ActorRef[Command]]()

    def aMakerForAChildThatWillCrash(): CHILD_MAKER[Reader.Command, NewIncomingQuoteManager.Command] = {

      val childCounter = new AtomicInteger(1)

      context: ActorContext[Reader.Command] => {

        def spawnAChildThatMayCrash(actorRefProbe: ActorRef[Command], counter: Int) =
          context.spawn(monitor(actorRefProbe, childBehaviourCrashedByGbpUsd(counter)), "quoteUpdatedAccountsOrganiserActor")

        val aChild = childCounter.get() match {
          case 1 =>
            spawnAChildThatMayCrash(firsChildMonitoringProbe.ref, childCounter.get())
          case 2 =>
            spawnAChildThatMayCrash(secondChildMonitoringProbe.ref, childCounter.get())
          case 3 =>
            spawnAChildThatMayCrash(thirdChildMonitoringProbe.ref, childCounter.get())
          case _ =>
            throw new IllegalArgumentException("too many children creation")
        }
        childCounter.getAndIncrement()
        theChildActorRefRepo.set(aChild)
        aChild
      }
    }

    val (anIncomingQuoteRecordedEventThatWillCrashTheChild, aJsonThatWillCrashTheChild) = IncomingQuoteGen.validIncomingQuotesOf(_ == GbpUsd).sample.get

    Given(s"a message that will cause the child to crash ($anIncomingQuoteRecordedEventThatWillCrashTheChild")

    (incomingQuoteReaderMock.parse _)
      .expects(aJsonThatWillCrashTheChild)
      .returning(JsSuccess(anIncomingQuoteRecordedEventThatWillCrashTheChild))

    val aQuotesFeedActor = spawn(
      reader.Reader(
        uniqueRandomPersistenceId,
        incomingQuoteReaderMock,
        aMakerForAChildThatWillCrash()
      )
    )

    When(s"the actor receives it")

    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(aJsonThatWillCrashTheChild)

    fishForChildMessage(firsChildMonitoringProbe, QuoteUpdated(Quote.from(anIncomingQuoteRecordedEventThatWillCrashTheChild)))

    Then("the child terminates")
    firsChildMonitoringProbe.expectTerminated(theChildActorRefRepo.get())

    val (templateMessage, anotherJson) = IncomingQuoteGen.validIncomingQuotesOf(_ == EurUsd).sample.get

    val aSafeIncomingQuoteRecordedEvent = messageAfter(anIncomingQuoteRecordedEventThatWillCrashTheChild.updated, templateMessage)
    Given(s"a message that will not cause the child to crash ($aSafeIncomingQuoteRecordedEvent)")

    (incomingQuoteReaderMock.parse _)
      .expects(anotherJson)
      .returning(JsSuccess(aSafeIncomingQuoteRecordedEvent))

    When(s"the actor receives it")
    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(anotherJson)

    val firstExpChildMessage = QuoteUpdated(Quote.from(aSafeIncomingQuoteRecordedEvent))
    Then(s"... the new child (the second) receives a $firstExpChildMessage")
    secondChildMonitoringProbe.expectMessage(
      firstExpChildMessage
    )
    info("because it has been recreated!")

    val anotherCrashableMessageWithAscDate = messageAfter(aSafeIncomingQuoteRecordedEvent.updated, anIncomingQuoteRecordedEventThatWillCrashTheChild)
    Given(s"another message that will cause the second child to crash ($anotherCrashableMessageWithAscDate)")

    (incomingQuoteReaderMock.parse _)
      .expects(aJsonThatWillCrashTheChild)
      .returning(JsSuccess(anotherCrashableMessageWithAscDate))

    When("the actor receives it")
    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(aJsonThatWillCrashTheChild)

    fishForChildMessage(secondChildMonitoringProbe, QuoteUpdated(Quote.from(anotherCrashableMessageWithAscDate)))

    Then("the second child terminates")
    secondChildMonitoringProbe.expectTerminated(theChildActorRefRepo.get())

    val theLastSafeIncomingMessageWithAscDate = messageAfter(anotherCrashableMessageWithAscDate.updated, aSafeIncomingQuoteRecordedEvent)

    (incomingQuoteReaderMock.parse _)
      .expects(anotherJson)
      .returning(JsSuccess(theLastSafeIncomingMessageWithAscDate))

    When(s"the quotesFeedActor receives a third valid message ${Reader.RecordIncomingQuoteCommand(anotherJson)}")
    aQuotesFeedActor ! Reader.RecordIncomingQuoteCommand(anotherJson)

    val secondExpChildMessage = QuoteUpdated(Quote.from(theLastSafeIncomingMessageWithAscDate))
    Then(s"... the third new child will receive a $secondExpChildMessage")
    thirdChildMonitoringProbe.expectMessage(
      secondExpChildMessage
    )
  }

  private def fishForChildMessage(aProbe: TestProbe[Command], anotherMessageToWaitingFor: QuoteUpdated) = {
    aProbe.fishForMessage(500.milliseconds, s"I was waiting for last message $anotherMessageToWaitingFor to have a complete state")(message => {
      if (message.asInstanceOf[QuoteUpdated].updatedQuote.rates.currencyPair == anotherMessageToWaitingFor.updatedQuote.rates.currencyPair) {
        FishingOutcome.Complete
      } else
        FishingOutcome.Continue
    })
  }

  private def messageOlderThan(anIncomingGbpUsdQuoteRecordedEvent: IncomingQuoteRecordedEvent) = {
    updatedDateTimeHook.modify(_.minusSeconds(1))(anIncomingGbpUsdQuoteRecordedEvent)
  }

  private def messageAfter(localDateTime: LocalDateTime, newMessage: IncomingQuoteRecordedEvent) = {
    updatedDateTimeHook.modify(_ => localDateTime.plusMinutes(1))(newMessage)
  }

  private def uniqueRandomPersistenceId: String = {
    val max = 15
    val temp = Gen.alphaUpperStr.sample.get
    if (temp.length < max)
      temp
    else
      temp.substring(0, max - 1)
  }
}
