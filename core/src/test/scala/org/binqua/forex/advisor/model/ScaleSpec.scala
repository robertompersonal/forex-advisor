package org.binqua.forex.advisor.model

import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.refineMV
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class ScaleSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 30)

  property(testName = "Scale has the concept of ordering") {
    forAll((Gen.choose(0, 10), "scaleValue"), (Gen.choose(0, 10), "anotherScaleValue")) { (scaleValue, anotherScaleValue) =>
      if (scaleValue > anotherScaleValue) {
        Scale.unsafe(scaleValue) should be > Scale.unsafe(anotherScaleValue)
      } else if (scaleValue < anotherScaleValue) {
        Scale.unsafe(scaleValue) should be < Scale.unsafe(anotherScaleValue)
      }

      Scale.unsafe(scaleValue) should be(Scale.unsafe(scaleValue))
    }
  }

  property(testName = "multiplier has as much zero as the scale value itself") {
    forAll((Gen.choose(1, 10), "scaleValue")) { scaleValue =>
      assertResult(PosBigDecimal.unsafeFrom(BigDecimal(s"1${List.fill(scaleValue)("0").mkString}")))(Scale.unsafe(scaleValue).powerOf10)
    }
  }

  property(testName = "asFractionalPartUnit is the 1 / multiplier") {
    forAll((Gen.choose(1, 10), "scaleValue")) { scaleValue =>
      import PosBigDecimal._
      assertResult(refineMV[NonNegative](1) / Scale.unsafe(scaleValue).powerOf10)(Scale.unsafe(scaleValue).asFractionalPartUnit)
    }
  }

  property(testName = "Cannot create a Scale with a value <= 0") {
    forAll((Gen.choose(-100, -1), "wrongScaleValue")) { wrongScaleValue =>
      val caught = intercept[IllegalArgumentException](
        Scale.unsafe(wrongScaleValue)
      )

      assertResult(s"Scale value has to be >= 0. $wrongScaleValue is not allowed")(caught.getMessage)
    }
  }

}
