package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.model.{CurrencyPair, PosBigDecimal}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, StateGen}
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import play.api.libs.json.{JsError, JsResult, Json}

import java.time.LocalDateTime
import java.util.UUID

class RecordedPositionCustomJsonSerAndDesSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  property(testName = "PortfolioCreatedEvent can be serialized and deserialized by default by akka") {
    forAll(PortfoliosGen.newPortfolioCreatedEvent) { portfolioCreatedEvent: events.PortfolioCreated =>
      assertThatCanBeSerialiseAndDeserialize(portfolioCreatedEvent)
    }
  }

  property(testName = "portfoliosSummary can be serialized and deserialized by default by akka") {
    forAll(StateGen.portfoliosSummary) { portfoliosSummary: PortfoliosSummary =>
      assertThatCanBeSerialiseAndDeserialize(portfoliosSummary)
    }
  }

  property("portfoliosSummary can be serialized and deserialized using custom json ser and des") {
    forAll(StateGen.portfoliosSummary) { portfoliosSummary: PortfoliosSummary =>
      val toBeDeserialised = Json.prettyPrint(Json.toJson(portfoliosSummary))

      Given(s"a portfolioSummary json:\n $toBeDeserialised")

      When("it is parsed")
      val actualPortfolioSummary: PortfoliosSummary = Json.parse(toBeDeserialised).as[PortfoliosSummary]

      Then("it gives back the initial PortfoliosSummary:\n")

      actualPortfolioSummary should be(portfoliosSummary)

    }
  }

  property("idReads knows when id json field is not valid") {

    val actualError: JsResult[UUID] = Json.parse("""{"id" : "4badf091-4287-4e10-a1c7-54d962e6007e dsfdf" }""").validate[UUID](RecordedPosition.jsonReads.id)

    actualError should be(a[JsError])

    actualError.toString should include("Cannot parse json path /id into a valid UUID. 4badf091-4287-4e10-a1c7-54d962e6007e dsfdf is wrong")
  }

  property("openDateTimeRead knows when openDateTime json field is not valid") {

    val actualError: JsResult[LocalDateTime] =
      Json.parse("""{"openDateTime" : "2011-12-03T10:15:3a" }""").validate(RecordedPosition.jsonReads.openDateTime)

    actualError should be(a[JsError])

    actualError.toString should include("Cannot parse json path /openDateTime into a valid LocalDateTime. 2011-12-03T10:15:3a is wrong")
  }

  property("recordedDateTimeRead knows when recordedDateTime json field is not valid") {

    val actualError: JsResult[LocalDateTime] =
      Json.parse("""{"recordedDateTime" : "b2011-12-03T10:15:3" }""").validate(RecordedPosition.jsonReads.recordedDateTime)

    actualError should be(a[JsError])

    actualError.toString should include("Cannot parse json path /recordedDateTime into a valid LocalDateTime. b2011-12-03T10:15:3 is wrong")
  }

  property("priceReads knows when price json field is not valid") {

    val actualError: JsResult[PosBigDecimal] = Json.parse("""{"price" : "1.3456a" } """).validate(RecordedPosition.jsonReads.price)

    actualError should be(a[JsError])

    actualError.toString should include(
      "Cannot parse json path /price into a valid PosBigDecimal. A PosBigDecimal value has to be a decimal number >= 0. Cannot create a PositiveBigDecimal. Reason: Cannot parse 1.3456a into a PosBigDecimal"
    )
  }

  property("pair reads knows when pair json field is not valid") {

    val actualError = Json
      .parse("""{"pair" : "test" } """)
      .validate(RecordedPosition.jsonReads.pair(Map(CurrencyPair.fromExternalIdentifierMapping.head, CurrencyPair.fromExternalIdentifierMapping.tail.head)))

    actualError should be(a[JsError])

    actualError.toString should include(
      "Cannot parse json path /pair into a valid CurrencyPair. Cannot find test in the list of allowed identifiers: GBP/JPY -> GbpJpy,US30 -> Us30"
    )
  }

  property("tradingPair reads knows when pair and price combination is not valid") {

    val actualError = Json.parse("""{"pair" : "GBP/JPY", "price" : "1253.23344546" } """).validate(RecordedPosition.jsonReads.tradingPair)

    actualError should be(a[JsError])

    actualError.toString should include(
      "Cannot parse json path /pair , /price into a valid TradingPair. Cannot build a trading pair for currency pair gbp/jpy with price 1253.23344546. Details: Scale of 1253.23344546 is 8 and it is too big compare to new scale 3. I will lose precision"
    )
  }

  property("lotSize reads knows when amount and size combination is not valid") {

    val actualError = Json.parse("""{"amount" : -1 , "size" : "123" } """).validate(RecordedPosition.jsonReads.lotSize)

    actualError should be(a[JsError])

    actualError.toString should include(
      "Cannot parse json path /amount , /size into a valid LotSize. Amount has to be >= 0. -1 is wrong\nOnly micro,mini,standard are allowed. 123 as lot size identifier is wrong"
    )
  }
}
