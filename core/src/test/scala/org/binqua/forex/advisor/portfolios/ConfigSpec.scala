package org.binqua.forex.advisor.portfolios

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.{be, the, _}

class ConfigSpec extends AnyFlatSpec {

  "given all valid parameters, unsafe" should "return a correct Config" in {

    val maxNumberOfPortfolios = 1
    val maxNumberOfPositions = 2
    val actualConfig: Config = Config.unsafe(maxNumberOfPortfolios, maxNumberOfPositions)

    actualConfig.maxNumberOfPortfolios should be(maxNumberOfPortfolios)
    actualConfig.maxNumberOfPositions should be(maxNumberOfPositions)
  }

  "given all invalid parameters, unsafe" should "throw illegalArgumentException" in {
    the[IllegalArgumentException] thrownBy Config.unsafe(
      maxNumberOfPortfolios = -1,
      maxNumberOfPositions = -1
    ) should have message "maxNumberOfPortfolios host has to be > 0 - maxNumberOfPositions host has to be > 0"
  }

}
