package org.binqua.forex.advisor.model

import cats.kernel.Eq
import cats.syntax.show._
import org.binqua.forex.advisor.model.CurrencyPair.{EurUsd, GbpUsd}
import org.binqua.forex.implicits.instances.currencyPair._
import org.binqua.forex.implicits.instances.id._
import org.binqua.forex.util.Grammar._
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.Shrink
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class CurrencyPairSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with TestingFacilities with GivenWhenThen {

  implicit val noShrink: Shrink[Int] = Shrink.shrinkAny

  val toAccountCurrencyScenario = Table(
    ("currencyPairUnderTest", "expected identifiers"),
    (CurrencyPair.EurUsd, (Id.eur, Id.usd)),
    (CurrencyPair.EurGbp, (Id.eur, Id.gbp)),
    (CurrencyPair.GbpUsd, (Id.gbp, Id.usd)),
    (CurrencyPair.UsdJpy, (Id.usd, Id.jpy)),
    (CurrencyPair.Spx500, (Id.spx500, Id.usd)),
    (CurrencyPair.Us30, (Id.us30, Id.usd))
  )

  property("toIds returns the ids associated to the base and quote currency") {
    forAll(toAccountCurrencyScenario) { (currencyPairUnderTest, exp) =>
      {
        val actual: (Id, Id) = currencyPairUnderTest.toIds.ids
        assertResult(true, actualValueIsWrong("ids", Map("actual" -> actual.show, "exp" -> exp.show)))(Eq.eqv(exp, actual))
      }
    }
  }

  property("currencyPairNeededForPipValueCalculation returns right currency pair") {

    assertResult(Right(CurrencyPair.EurUsd))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Eur))(QuoteCurrency.Usd)(CurrencyPair.values)
    )

    assertResult(Right(CurrencyPair.EurUsd))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Usd))(QuoteCurrency.Eur)(CurrencyPair.values)
    )

    assertResult(Right(CurrencyPair.GbpUsd))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Usd))(QuoteCurrency.Gbp)(CurrencyPair.values)
    )

    assertResult(Right(CurrencyPair.GbpUsd))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Gbp))(QuoteCurrency.Usd)(CurrencyPair.values)
    )

    assertResult(Left("Sorry!!! I could not find (eur,gbp) or (gbp,eur) in currency pairs [EurUsd,GbpUsd]. Maybe some currency pair are not supported!"))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Gbp))(QuoteCurrency.Eur)(only(EurUsd) ::: only(GbpUsd))
    )

    assertResult(Left("Sorry!!! I could not find (gbp,usd) or (usd,gbp) in currency pairs [EurUsd]. Maybe some currency pair are not supported!"))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Usd))(QuoteCurrency.Gbp)(only(EurUsd))
    )
    assertResult(Left("Sorry!!! I could not find (usd,gbp) or (gbp,usd) in currency pairs [EurUsd]. Maybe some currency pair are not supported!"))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Gbp))(QuoteCurrency.Usd)(only(EurUsd))
    )

    assertResult(Left("Sorry!!! I could not find (eur,usd) or (usd,eur) in currency pairs [GbpUsd]. Maybe some currency pair are not supported!"))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Usd))(QuoteCurrency.Eur)(only(GbpUsd))
    )
    assertResult(Left("Sorry!!! I could not find (usd,eur) or (eur,usd) in currency pairs [GbpUsd]. Maybe some currency pair are not supported!"))(
      CurrencyPair.currencyPairNeededForPipValueCalculation(in(AccountCurrency.Eur))(QuoteCurrency.Usd)(only(GbpUsd))
    )

  }

  property("byBaseAndQuoteIdentifiersInAnyOrder returns right currencyPair") {
    forAll(TradingPairGen.currencyPair) { currencyPair =>
      {
        forAll(TradingPairGen.someCurrencyPairsBy(Eq.neqv(currencyPair, _))) { otherCurrencyPairs =>
          {

            assertResult(Some(currencyPair))(CurrencyPair.byBaseAndQuoteIdentifiersInAnyOrder(currencyPair.toIds.ids, currencyPair :: otherCurrencyPairs))

            assertResult(Some(currencyPair))(CurrencyPair.byBaseAndQuoteIdentifiersInAnyOrder(currencyPair.toIds.ids.swap, currencyPair :: otherCurrencyPairs))

            assertResult(None)(CurrencyPair.byBaseAndQuoteIdentifiersInAnyOrder(currencyPair.toIds.ids, otherCurrencyPairs))

            assertResult(None)(CurrencyPair.byBaseAndQuoteIdentifiersInAnyOrder(currencyPair.toIds.ids.swap, otherCurrencyPairs))
          }
        }
      }
    }
  }

  val toExternalIdentifierScenario = Table(
    ("currencyPairUnderTest", "expected identifier"),
    (CurrencyPair.EurGbp, "EUR/GBP"),
    (CurrencyPair.EurJpy, "EUR/JPY"),
    (CurrencyPair.EurUsd, "EUR/USD"),
    (CurrencyPair.GbpJpy, "GBP/JPY"),
    (CurrencyPair.GbpUsd, "GBP/USD"),
    (CurrencyPair.Spx500, "SPX500"),
    (CurrencyPair.Us30, "US30"),
    (CurrencyPair.UsdJpy, "USD/JPY")
  )

  property("toExternalIdentifier works") {
    forAll(toExternalIdentifierScenario) { (currencyPair, expIdentifier) =>
      {
        assertResult(CurrencyPair.toExternalIdentifier(currencyPair))(expIdentifier)
      }
    }
  }

  property("toCurrencyPair works") {
    forAll(toExternalIdentifierScenario) { (currencyPair, expIdentifier) =>
      {
        assertResult(CurrencyPair.toCurrencyPair(expIdentifier, CurrencyPair.values))(Some(currencyPair))
      }
    }
  }

  property("toCurrencyPair gives None if there is no match") {
    assertResult(CurrencyPair.toCurrencyPair("wrong", CurrencyPair.values))(None)
    assertResult(CurrencyPair.toCurrencyPair("", CurrencyPair.values))(None)
    assertResult(CurrencyPair.toCurrencyPair(null, CurrencyPair.values))(None)
  }

  property("toExternalIdentifier and toCurrencyPair bring me to the initial currencyPair") {
    CurrencyPair.values.foreach { cp =>
      info("-----")

      Given(s"a valid $cp")

      val actualExternalIdentifier = CurrencyPair.toExternalIdentifier(cp)
      And(s"its external identifier $actualExternalIdentifier representation")

      When("I apply toCurrencyPair to it, I obtain the initial currency pair")
      assertResult(Some(cp))(CurrencyPair.toCurrencyPair(actualExternalIdentifier, CurrencyPair.values))
    }
  }

  private def only(onlyThisCurrencyPair: CurrencyPair): List[CurrencyPair] = {
    CurrencyPair.values.filter(Eq.eqv(onlyThisCurrencyPair, _))
  }

}
