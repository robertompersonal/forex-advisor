package org.binqua.forex.advisor.portfolios.service

import akka.actor.testkit.typed.scaladsl.{FishingOutcomes, ManualTime, ScalaTestWithActorTestKit}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.cluster.sharding.typed.testkit.scaladsl.TestEntityRef
import cats.Eq
import cats.data.NonEmptySeq
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.numeric.{NonNegative, Positive}
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.Response
import org.binqua.forex.advisor.newportfolios.{DeliveryGuaranteedPortfolios, PortfoliosEntityFinderModule, PortfoliosSummary}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.CreatePortfolio
import org.binqua.forex.advisor.portfolios.service.PortfoliosService._
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel, StateGen}
import org.binqua.forex.advisor.util.Model
import org.binqua.forex.util.core.UnsafeConfigReader
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.duration._

class PortfoliosServiceSpec
    extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test")))
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  "Given the service does not receive a response before a specified retryInterval, it" should "retry for maxRetryAttempts and then send back a status reply error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport(TestContext.theActualConfiguration),
    TestContext.theActualConfiguration
  ) {
    matchDistinctLogs(
      messages = "retryIntervalExpired 1",
      "retryIntervalExpired 2",
      "retryIntervalExpired 3",
      s"maxRetriesReached ${theConfiguration.maxRetryAttempts.value} for id 1"
    ).expect {

      underTest ! portfolioToBeCreated

      shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

      manualTime.timePasses(theConfiguration.retryIntervalInMillis)

      shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

      manualTime.timePasses(theConfiguration.retryIntervalInMillis)

      shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

      manualTime.timePasses(theConfiguration.retryIntervalInMillis)

      shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

      manualTime.timePasses(theConfiguration.retryIntervalInMillis)

      shardedPortfoliosProbe.expectNoMessage()

      actorWhoAskTheServiceProbe.expectMessage(TooManyRetries(TestContext.theActualConfiguration.maxRetryAttempts))
    }
  }

  "Given the service does receive a response during a retry attempt, it" should "send back the response" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport(TestContext.theActualConfiguration),
    TestContext.theActualConfiguration
  ) {

    underTest ! portfolioToBeCreated

    shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

    manualTime.timePasses(theConfiguration.retryIntervalInMillis)

    shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

    manualTime.timePasses(theConfiguration.retryIntervalInMillis)

    shardedPortfoliosProbe.fishForMessage(2000.millis)({
      case DeliveryGuaranteedPortfolios.ExternalCommand(messageId, DeliveryGuaranteedPortfolios.CreatePortfolioPayload(actualPortfolioToBeCreated), replyTo) =>
        if (
          Eq[CreatePortfolio].eqv(actualPortfolioToBeCreated, portfolioRequestResponseContext.portfolioToBeCreatedRequest) &&
          Eq[MessageId].eqv(DummyMessageId(1), messageId)
        ) {
          replyTo ! Response(messageId, DeliveryGuaranteedPortfolios.Success(portfolioRequestResponseContext.theNewPortfolios))
          FishingOutcomes.complete
        } else
          FishingOutcomes.continue
      case _ =>
        FishingOutcomes.continue
    })

    actorWhoAskTheServiceProbe.expectMessage(DeliverySucceeded(portfolioRequestResponseContext.theNewPortfolios))

    manualTime.timePasses(theConfiguration.retryIntervalInMillis)

    shardedPortfoliosProbe.expectNoMessage()

  }

  "Given the service does receive a response before timer expires, it" should "send back the response and cancel the timer" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport(TestContext.theActualConfiguration),
    TestContext.theActualConfiguration
  ) {

    val messageId = 1

    matchDistinctLogs(messages = s"message confirmed $messageId").expect {

      underTest ! portfolioToBeCreated

      shardedPortfoliosProbe.fishForMessage(2000.millis)({
        case DeliveryGuaranteedPortfolios
              .ExternalCommand(actualMessageId, DeliveryGuaranteedPortfolios.CreatePortfolioPayload(actualPortfolioToBeCreated), portfoliosResponseAdapter) =>
          if (
            Eq[CreatePortfolio].eqv(actualPortfolioToBeCreated, portfolioRequestResponseContext.portfolioToBeCreatedRequest) &&
            Eq[MessageId].eqv(DummyMessageId(messageId), actualMessageId)
          ) {
            portfoliosResponseAdapter ! Response(actualMessageId, DeliveryGuaranteedPortfolios.Success(portfolioRequestResponseContext.theNewPortfolios))
            FishingOutcomes.complete
          } else
            FishingOutcomes.continue
        case _ =>
          FishingOutcomes.continue
      })

      actorWhoAskTheServiceProbe.expectMessage(DeliverySucceeded(portfolioRequestResponseContext.theNewPortfolios))

      manualTime.timePasses(theConfiguration.retryIntervalInMillis)

      shardedPortfoliosProbe.expectNoMessage()
    }

  }

  "Given the service does receive a number of requests greater then maxInFlightMessages, it" should "send back a reject response" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport(TestContext.theActualConfiguration),
    TestContext.theActualConfiguration
  ) {

    matchDistinctLogs(messages = s"maxInFlightMessagesReached ${theConfiguration.maxInFlightMessages.value}").expect {

      (1 to theConfiguration.maxInFlightMessages.value).foreach(_ => {
        underTest ! portfolioToBeCreated
        shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]
        actorWhoAskTheServiceProbe.expectNoMessage()
      })

      underTest ! portfolioToBeCreated
      shardedPortfoliosProbe.expectNoMessage()
      actorWhoAskTheServiceProbe.expectMessage(Rejected(theConfiguration.maxInFlightMessages))
    }
  }

  "Given the service does receive a number of requests greater then maxInFlightMessages, it" should "start forward messages again as soon as number of inFlightMessages goes below maxInFlightMessages" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport(TestContext.theActualConfiguration),
    TestContext.theActualConfiguration
  ) {

    (1 to theConfiguration.maxInFlightMessages.value).foreach(_ => {
      underTest ! portfolioToBeCreated
      shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]
      actorWhoAskTheServiceProbe.expectNoMessage()
    })

    underTest ! portfolioToBeCreated
    shardedPortfoliosProbe.expectNoMessage()
    actorWhoAskTheServiceProbe.expectMessage(Rejected(theConfiguration.maxInFlightMessages))

    underTest ! WrappedDeliveryConfirm(
      Response(DummyMessageId(1), DeliveryGuaranteedPortfolios.Error("not important because we need just a message back"))
    )

    actorWhoAskTheServiceProbe.expectMessageType[ValidationFailed]

    underTest ! portfolioToBeCreated
    shardedPortfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

  }

  object SupportMessagesContext {

    def justForAnIdeaSupport(conf: Config): SupportMessages = {
      new SupportMessages {

        override def messageConfirmed(inFlightCommand: InFlightCommandRecord): String = s"message confirmed ${inFlightCommand.command.messageId.id}"

        override def retryIntervalExpired(inFlightCommand: InFlightCommandRecord): String = s"retryIntervalExpired ${inFlightCommand.numberOfRetriesStarted}"

        override def maxRetriesReached(inFlightCommand: InFlightCommandRecord): String =
          s"maxRetriesReached ${conf.maxRetryAttempts.value} for id ${inFlightCommand.command.messageId.id}"

        override def maxInFlightMessagesReached(): String = s"maxInFlightMessagesReached ${conf.maxInFlightMessages.value}"
      }
    }

  }

  object TestContext {

    val theActualConfiguration: Config = Config.safe(2000.millis, maxRetryAttempts = refineMV[NonNegative](3), refineMV[Positive](10))

  }

  case class TestContext(supportMessages: SupportMessages, theConfiguration: Config) extends ManualTimeBaseTestContext(underTestNaming) {

    trait ConfigReaderTester {
      val configurationReader: UnsafeConfigReader[Config]

      def assertThatHasBeenCalled: Boolean
    }

    val shardedPortfoliosProbe = testKit.createTestProbe[DeliveryGuaranteedPortfolios.ExternalCommand]()

    val actorWhoAskTheServiceProbe = testKit.createTestProbe[PortfoliosService.Response]()

    case class DummyMessageId(idx: Int) extends MessageId {
      override val id: String = idx.toString
    }

    val increaseBy1StubbedMessageIdGenerator = new MessageIdGenerator {
      var counter: Int = 0

      override def newId(): MessageId = {
        counter += 1
        DummyMessageId(counter)
      }
    }

    val shardedEntityId: Model.ShardedEntityId = refineMV[NonEmpty]("1234")

    val configurationReaderTester: ConfigReaderTester = configReaderTester(configToBeReturned = theConfiguration)

    val underTest =
      testKit.spawn(
        PortfoliosService(
          increaseBy1StubbedMessageIdGenerator,
          portfoliosEntityFinderModuleFor(shardedEntityId),
          (_: Config) => supportMessages,
          configurationReaderTester.configurationReader
        )
      )

    case class PortfolioRequestResponseContext(portfolioToBeCreatedRequest: PortfoliosModel.CreatePortfolio, theNewPortfolios: PortfoliosSummary) {
      val payload = DeliveryGuaranteedPortfolios.CreatePortfolioPayload(portfolioToBeCreatedRequest)
    }

    val portfolioRequestResponseContext = PortfolioRequestResponseContext(
      CreatePortfolio(
        PortfoliosGen.idempotentKey.sample.get,
        PortfoliosGen.portfolioName.sample.get,
        NonEmptySeq(PortfoliosGen.position.sample.get, Seq(PortfoliosGen.position.sample.get))
      ),
      StateGen.portfoliosSummary.sample.get
    )

    val portfolioToBeCreated = PortfoliosService.Envelope(
      shardedEntityId,
      DeliveryGuaranteedPortfolios.CreatePortfolioPayload(portfolioRequestResponseContext.portfolioToBeCreatedRequest),
      replyTo = actorWhoAskTheServiceProbe.ref
    )

    def configReaderTester(configToBeReturned: Config): ConfigReaderTester = {
      val hasBeenCalled = new AtomicBoolean(false)
      new ConfigReaderTester {
        override val configurationReader: UnsafeConfigReader[Config] = akkaConfig => {
          hasBeenCalled.set(true)
          val realConfig = unsafeConfigReader(akkaConfig)
          realConfig.retryIntervalInMillis should be(1500.millis)
          realConfig.maxRetryAttempts should be(refineMV[NonNegative](3))
          configToBeReturned
        }

        override def assertThatHasBeenCalled: Boolean = hasBeenCalled.get()
      }

    }

    def portfoliosEntityFinderModuleFor(expectedEntityId: Model.ShardedEntityId): PortfoliosEntityFinderModule =
      (entityId: Model.ShardedEntityId) => {
        import Model._
        if (Eq[Model.ShardedEntityId].eqv(expectedEntityId, entityId))
          TestEntityRef[DeliveryGuaranteedPortfolios.ExternalCommand](
            EntityTypeKey[DeliveryGuaranteedPortfolios.ExternalCommand]("portfolios"),
            entityId.value,
            shardedPortfoliosProbe.ref
          )
        else fail(message = s"Wrong entityId: waiting for $expectedEntityId received $entityId")
      }
  }

}
