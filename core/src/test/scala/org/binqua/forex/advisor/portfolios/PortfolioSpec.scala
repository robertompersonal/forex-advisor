package org.binqua.forex.advisor.portfolios

import cats.data.NonEmptySeq
import org.binqua.forex.advisor.portfolios.PortfoliosModel.Portfolio
import org.binqua.forex.advisor.portfolios.PortfoliosModel.events.{Added, Deleted, Updated}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import java.util.UUID

class PortfolioSpec extends AnyFlatSpec {

  "I" should "be able to add new positions to an empty portfolio" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val secondPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = PortfoliosModel.Portfolio(PortfoliosGen.portfolioName.sample.get, Set())

    val actualPortfolio = initialPortfolio.updated(NonEmptySeq(Added(firstPosition), Seq(Added(secondPosition))))

    actualPortfolio should be(
      Portfolio(
        initialPortfolio.portfolioName,
        Set(firstPosition, secondPosition)
      )
    )

  }

  "I" should "be able to add new positions to a non empty portfolio" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val secondPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val thirdPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val updatesToBeExecuted = NonEmptySeq(Added(firstPosition), Seq(Added(secondPosition)))

    val initialPortfolio = PortfoliosModel.Portfolio(PortfoliosGen.portfolioName.sample.get, Set()).updated(updatesToBeExecuted)

    val actualPortfolio = initialPortfolio.updated(NonEmptySeq.of(Added(thirdPosition)))

    actualPortfolio should be(
      Portfolio(
        initialPortfolio.portfolioName,
        Set(
          firstPosition,
          secondPosition,
          thirdPosition
        )
      )
    )

  }

  "I" should "be able to add delete positions from a non empty portfolio" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val secondPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = Portfolio(
      PortfoliosGen.portfolioName.sample.get,
      Set(
        firstPosition,
        secondPosition
      )
    )

    val updatesToBeExecuted = NonEmptySeq(Deleted(firstPosition.id), Seq(Deleted(secondPosition.id)))

    val actualPortfolio = initialPortfolio.updated(updatesToBeExecuted)

    actualPortfolio should be(Portfolio(initialPortfolio.portfolioName, Set()))

  }

  "Delete a position with a non existing id" should "have no effect" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = Portfolio(
      PortfoliosGen.portfolioName.sample.get,
      Set(
        firstPosition
      )
    )

    initialPortfolio.updated(NonEmptySeq.of(Deleted(UUID.randomUUID()))) should be(initialPortfolio)

  }

  "I" should "be able to update positions from a non empty portfolio" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val secondPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = Portfolio(
      PortfoliosGen.portfolioName.sample.get,
      Set(
        firstPosition,
        secondPosition
      )
    )

    val newUpdatedFirstPosition = PortfoliosGen.generateARecordedPositionWithId(firstPosition.id).sample.head
    val newUpdatedSecondPosition = PortfoliosGen.generateARecordedPositionWithId(secondPosition.id).sample.head

    val actualPortfolio = initialPortfolio.updated(NonEmptySeq(Updated(newUpdatedFirstPosition), Seq(Updated(newUpdatedSecondPosition))))

    actualPortfolio should be(
      Portfolio(
        initialPortfolio.portfolioName,
        Set(
          newUpdatedFirstPosition,
          newUpdatedSecondPosition
        )
      )
    )

  }

  "Update a position with non existing id" should "have no effect" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = Portfolio(PortfoliosGen.portfolioName.sample.get, Set(firstPosition))

    val positionWithNonExistingId = PortfoliosGen.generateARecordedPositionWithId(UUID.randomUUID()).sample.head

    initialPortfolio.updated(NonEmptySeq.of(Updated(positionWithNonExistingId))) should equal(initialPortfolio)

  }

  "I" should "be able to add, update and delete positions from a non empty portfolio" in {

    val firstPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head
    val secondPosition: PortfoliosModel.RecordedPosition = PortfoliosGen.recordedPosition.sample.head

    val initialPortfolio = Portfolio(
      PortfoliosGen.portfolioName.sample.get,
      Set(
        firstPosition,
        secondPosition
      )
    )

    val newThirdPosition = PortfoliosGen.recordedPosition.sample.head
    val newUpdatedSecondPosition = PortfoliosGen.generateARecordedPositionWithId(secondPosition.id).sample.head

    val actualPortfolio = initialPortfolio.updated(NonEmptySeq(Added(newThirdPosition), Seq(Updated(newUpdatedSecondPosition), Deleted(firstPosition.id))))

    actualPortfolio should equal(
      Portfolio(
        initialPortfolio.portfolioName,
        Set(
          newUpdatedSecondPosition,
          newThirdPosition
        )
      )
    )

  }

}
