package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.util.core.makeItUnsafe
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.prop.TableDrivenPropertyChecks.whenever
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks.forAll

import java.util.UUID
import scala.concurrent.duration._

class DefaultErrorMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyPropSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation
    with TestingFacilities {

  implicit val noShrink: Shrink[Int] = Shrink.shrinkAny

  private val config: Config = makeItUnsafe(Config.validated(maxNumberOfPortfolios = 3, maxNumberOfPositions = 2, garbageCollectorInterval = 20.seconds))

  val underTest = new DefaultErrorMessages(config)

  property(testName = "maxNumberOfPortfoliosReached") {
    forAll(PortfoliosGen.updatePortfolio) { updatePortfolio =>
      whenever(updatePortfolio.maybeAnIdempotentKey.isDefined) {
        underTest.maxNumberOfPortfoliosReached() should be(s"Sorry but you can create only ${config.maxNumberOfPortfolios}")
      }
    }
  }
  property(testName = "portfolioAlreadyExist") {
    forAll(PortfoliosGen.updatePortfolio) { updatePortfolio =>
      whenever(updatePortfolio.maybeAnIdempotentKey.isDefined) {
        underTest.portfolioAlreadyExist(updatePortfolio.portfolioName) should be(
          s"Portfolio ${updatePortfolio.portfolioName.name} cannot be created because it exist already"
        )
      }
    }
  }

  property(testName = "portfolioDoesNotExist for update") {
    forAll(PortfoliosGen.updatePortfolio) { updatePortfolio =>
      whenever(updatePortfolio.maybeAnIdempotentKey.isDefined) {
        underTest.portfolioDoesNotExist(updatePortfolio) should be(s"Portfolio ${updatePortfolio.portfolioName.name} to be updated does not exist")
      }
    }
  }

  property(testName = "positionsToBeUpdatedDoesNotExist") {
    forAll(Gen.listOfN(4, Gen.uuid)) { uuids =>
      val uuidSorted: Seq[UUID] = uuids.sortWith(_.compareTo(_) > 0)
      underTest.positionsToBeUpdatedDoesNotExist(uuids) should be(
        s"The following position ids to be updated do not exist: ${uuidSorted.mkString(" , ")}. Update failed"
      )
    }
  }

  property(testName = "tooManyPositionsForASinglePortfolio") {
    val tooManyPositionsGen = Gen.choose(config.maxNumberOfPositions + 1, config.maxNumberOfPositions + 10)
    forAll(PortfoliosGen.portfolioName, tooManyPositionsGen) { (portfolioName, tooManyPositions) =>
      underTest.tooManyPositionsForASinglePortfolio(portfolioName, tooManyPositions) should be(
        s"$tooManyPositions are too many positions per a single portfolio ${portfolioName.name}. Max allowed is ${config.maxNumberOfPositions}"
      )
    }
  }

}
