package org.binqua.forex.advisor.newportfolios

import cats.syntax.either._
import cats.syntax.option._
import org.binqua.forex.util.TestingFacilities
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class IdempotenceJournalSpec extends AnyFlatSpec with TestingFacilities {

  "given same key in both maps" should "not be possible built an IdempotenceRecords" in {

    val expectedError = "I cannot built a IdempotenceRecords with same key in both maps :("

    IdempotenceJournal
      .validate(mostRecent = Map("a" -> 1), nextToBeDeleted = Map("a" -> 1)) should be(
      expectedError.asLeft
    )
    IdempotenceJournal
      .validate(mostRecent = Map("z" -> 1, "a" -> 1), nextToBeDeleted = Map("b" -> 2, "a" -> 1, "c" -> 3)) should be(
      expectedError.asLeft
    )

  }

  "given a key is present in mostRecent map, find" should "return its hash" in {
    IdempotenceJournal
      .validate(mostRecent = Map("a" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .find("a") should be(1.some)
  }

  "given a key is present in nextToBeDeleted map, find" should "return its hash" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map("a" -> 1))
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .find("a") should be(1.some)
  }

  "clean" should "return the right message" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .written should be("Running idempotenceJournal deleteEntries: deleted 0 entries. 1 entries left")
  }

  "after 2 deleteEntries invocation, we" should "receive the right message" in {
    IdempotenceJournal
      .validate(mostRecent = Map("a" -> 1, "b" -> 1, "c" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .deleteEntries()
      .written should be("Running idempotenceJournal deleteEntries: deleted 3 entries. 0 entries left")
  }

  "after 1 deleteEntries invocation, find" should "still return a key value" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .find("b") should be(1.some)
  }

  "after 1 deleteEntries invocation, find" should "still return a key value added" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .add("a", 2)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .find("a") should be(2.some)
  }

  "adding an existing key in the mostRecent part" should "not be possible" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .add("b", 2) should be("key b already present".asLeft)
  }

  "adding an existing key in the nextToBeDeleted part" should "not be possible" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map("c" -> 1))
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .add("c", 2) should be("key c already present".asLeft)
  }

  "after 1 deleteEntries invocation, and some add it" should "be possible return an old value" in {
    IdempotenceJournal
      .validate(mostRecent = Map("z" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .add("a", 2)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .add("b", 3)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .add("c", 4)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .find("z") should be(1.some)
  }

  "after 2 deleteEntries invocations, find" should "not return a key value" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map.empty)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .deleteEntries()
      .value
      .find("b") should be(None)
  }

  "after deleteEntries invocation, it" should "be possible add an old key present into nextToBeDeleted part" in {
    IdempotenceJournal
      .validate(mostRecent = Map("b" -> 1), nextToBeDeleted = Map("c" -> 1))
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .deleteEntries()
      .value
      .add("c", 4)
      .getOrElse(thisTestShouldNotHaveArrivedHere)
      .find("c") should be(4.some)
  }

}
