package org.binqua.forex.advisor.portfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import cats.data.NonEmptySeq
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios
import org.binqua.forex.advisor.portfolios.PortfoliosModel.CreatePortfolio
import org.binqua.forex.advisor.portfolios.service.PortfoliosService
import org.scalatest.flatspec.AnyFlatSpecLike

class PortfoliosModelSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike {

  "Equal for CreatePortfolio" should "work" in {
    val portfolioToBeCreated = PortfoliosService.Envelope(
      refineMV[NonEmpty]("1234"),
      payload = DeliveryGuaranteedPortfolios.CreatePortfolioPayload(
        CreatePortfolio(
          PortfoliosGen.idempotentKey.sample.get,
          PortfoliosGen.portfolioName.sample.get,
          NonEmptySeq(PortfoliosGen.position.sample.get, Seq(PortfoliosGen.position.sample.get))
        )
      ),
      replyTo = testKit.createTestProbe[PortfoliosService.Response]().ref
    )

    portfolioToBeCreated.copy(shardedEntityId = refineMV[NonEmpty]("1234")) should be(portfolioToBeCreated)
  }
}
