package org.binqua.forex.advisor.model

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class AccountCurrencySpec extends AnyFlatSpec with Matchers with TableDrivenPropertyChecks {

  val currencyPairsRelatedToUsd: List[CurrencyPair] = List(CurrencyPair.EurUsd, CurrencyPair.GbpUsd, CurrencyPair.Spx500, CurrencyPair.Us30, CurrencyPair.UsdJpy)

  val currencyPairsNotRelatedToUsd = CurrencyPair.values.filterNot(currencyPairsRelatedToUsd.contains)

  "Currency pairs related to usd" should s"be $currencyPairsRelatedToUsd" in {

    currencyPairsRelatedToUsd.foreach { cp =>
      assertResult(true)(AccountCurrency.Usd.isEqualToACurrencyIn(cp))
      assertResult(false)(AccountCurrency.Usd.isNotEqualToACurrencyIn(cp))
    }

  }

  "Currency pairs not related to usd" should s"be $currencyPairsNotRelatedToUsd" in {

    currencyPairsNotRelatedToUsd.foreach { cp =>
      assertResult(false)(AccountCurrency.Usd.isEqualToACurrencyIn(cp))
      assertResult(true)(AccountCurrency.Usd.isNotEqualToACurrencyIn(cp))

    }
  }

  val currencyPairsRelatedToGbp: List[CurrencyPair] = List(CurrencyPair.GbpUsd, CurrencyPair.EurGbp, CurrencyPair.GbpJpy)

  "Currency pairs related to gbp" should s"be $currencyPairsRelatedToGbp" in {

    currencyPairsRelatedToGbp.foreach { cp =>
      assertResult(true)(AccountCurrency.Gbp.isEqualToACurrencyIn(cp))
      assertResult(false)(AccountCurrency.Gbp.isNotEqualToACurrencyIn(cp))

    }

  }

  val currencyPairsNotRelatedToGbp = CurrencyPair.values.filterNot(currencyPairsRelatedToGbp.contains)

  "Currency pairs not related to gbp" should s"be $currencyPairsNotRelatedToGbp" in {

    currencyPairsNotRelatedToGbp.foreach { cp =>
      assertResult(false)(AccountCurrency.Gbp.isEqualToACurrencyIn(cp))
      assertResult(true)(AccountCurrency.Gbp.isNotEqualToACurrencyIn(cp))
    }
  }

  val currencyPairsRelatedToEur: List[CurrencyPair] = List(CurrencyPair.EurGbp, CurrencyPair.EurUsd, CurrencyPair.EurJpy)

  "Currency pairs related to eur" should s"be $currencyPairsRelatedToEur" in {

    currencyPairsRelatedToEur.foreach { cp =>
      assertResult(true)(AccountCurrency.Eur.isEqualToACurrencyIn(cp))
      assertResult(false)(AccountCurrency.Eur.isNotEqualToACurrencyIn(cp))
    }

  }

  val currencyPairsNotRelatedToEur = CurrencyPair.values.filterNot(currencyPairsRelatedToEur.contains)

  "Currency pairs not related to eur" should s"be $currencyPairsNotRelatedToEur" in {

    currencyPairsNotRelatedToEur.foreach { cp =>
      assertResult(false)(AccountCurrency.Eur.isEqualToACurrencyIn(cp))
      assertResult(true)(AccountCurrency.Eur.isNotEqualToACurrencyIn(cp))

    }
  }

  val quoteCurrencyAffectedByAQuoteUpdateOfScenarios = org.scalatest.prop.Tables.Table(
    ("Account currency", "Currency pair updated", "Quote currency expected"),
    (AccountCurrency.Gbp, CurrencyPair.GbpUsd, Some(QuoteCurrency.Usd)),
    (AccountCurrency.Gbp, CurrencyPair.EurGbp, Some(QuoteCurrency.Eur)),
    (AccountCurrency.Gbp, CurrencyPair.EurUsd, None),
    (AccountCurrency.Gbp, CurrencyPair.Spx500, None),
    (AccountCurrency.Gbp, CurrencyPair.Us30, None),
    (AccountCurrency.Eur, CurrencyPair.EurGbp, Some(QuoteCurrency.Gbp)),
    (AccountCurrency.Eur, CurrencyPair.EurUsd, Some(QuoteCurrency.Usd)),
    (AccountCurrency.Eur, CurrencyPair.GbpUsd, None),
    (AccountCurrency.Eur, CurrencyPair.Spx500, None),
    (AccountCurrency.Eur, CurrencyPair.Us30, None),
    (AccountCurrency.Usd, CurrencyPair.EurUsd, Some(QuoteCurrency.Eur)),
    (AccountCurrency.Usd, CurrencyPair.UsdJpy, Some(QuoteCurrency.Jpy)),
    (AccountCurrency.Usd, CurrencyPair.GbpUsd, Some(QuoteCurrency.Gbp)),
    (AccountCurrency.Usd, CurrencyPair.Spx500, None),
    (AccountCurrency.Usd, CurrencyPair.Us30, None),
  )

  "given a currency pair, quoteCurrencyAffectedByAQuoteUpdateOf" should "return " +
    "the quote currency associated to the currency left" +
    " after removing the currency associated to the account from the given currency pair. Indexes dont have a quoteCurrency." +
    " The meaning of it, it is simple: in a Gbp account for example, if I received a EurGbp quote updated it means that the price of all my previous Eur quotes in the account ... usdEur abcEur defEur has to be updated" in {

    forAll(quoteCurrencyAffectedByAQuoteUpdateOfScenarios) { (accountCurrency, currencyPairUpdated, expQuoteCurrency) =>
      assertResult(expQuoteCurrency)(accountCurrency.extractQuoteCurrencyAffectedByQuoteUpdatedOf(currencyPairUpdated))
    }

  }


}
