package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.Response
import org.binqua.forex.advisor.portfolios.StateGen
import org.binqua.forex.advisor.portfolios.service.PortfoliosServiceGen.command
import org.binqua.forex.advisor.portfolios.service.{PortfoliosService, UUIDMessageId}
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfoliosCommandSerAndDesSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  val portfoliosResponseProbe = testKit.createTestProbe[DeliveryGuaranteedPortfolios.Response]().ref

  val portfoliosServiceProbe = testKit.createTestProbe[PortfoliosService.Response]().ref

  property(testName = s"All commands can be ser and des") {
    forAll(command(portfoliosResponseProbe)) { command =>
      assertThatCanBeSerialiseAndDeserialize(command)
    }
  }

  property(testName = s"All responses can be ser and des") {
    forAll(StateGen.portfoliosSummary, Gen.uuid, Gen.alphaStr) { (portfoliosSummary, uuid, error) =>
      assertThatCanBeSerialiseAndDeserialize(Response(UUIDMessageId(uuid), DeliveryGuaranteedPortfolios.Success(portfoliosSummary)))
      assertThatCanBeSerialiseAndDeserialize(Response(UUIDMessageId(uuid), DeliveryGuaranteedPortfolios.Error(error)))
    }
  }

}
