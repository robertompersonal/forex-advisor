package org.binqua.forex.advisor.portfolios

import cats.data.NonEmptySeq
import org.binqua.forex.advisor.portfolios.PortfoliosModel.RecordedPosition
import org.binqua.forex.advisor.portfolios.PortfoliosModel.events._
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import java.util.UUID
import scala.util.Random

class PortfolioUpdateSpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit val noShrinkRecordedPositions: Shrink[List[RecordedPosition]] = Shrink.shrinkAny

  property(testName = "PortfolioUpdated cannot be created with duplicated ids") {

    forAll(updatesWith2DuplicatedIds(maxNumberOfPositions = 10), PortfoliosGen.portfolioName) { (pair, portfolioName) =>
      val (updatesWith2Duplicates, idsWith2Duplicates) = pair

      val expectedMessage = s"It is not possible to have a duplicated ids in updates ids ${idsWith2Duplicates.mkString("[", ",", "]")}"

      the[IllegalArgumentException] thrownBy {
        PortfolioUpdated.unsafe(portfolioName, updatesWith2Duplicates)
      } should have message expectedMessage

    }

  }

  def recordedPositionsWith2DuplicatedIds(maxNumberOfPositions: Int): Gen[(NonEmptySeq[RecordedPosition], List[UUID])] = {
    require(maxNumberOfPositions > 1)
    for {
      recordedPositionsToBeAmended <- Gen.listOfN(maxNumberOfPositions, PortfoliosGen.recordedPosition)
      newIds <- Gen.const(Vector.fill(maxNumberOfPositions - 1)(UUID.randomUUID()).toList)
      anIndexInsideNewIds <- Gen.choose(min = 0, max = newIds.length - 1)
      idsWithOneDuplicated <- Gen.const(Random.shuffle(newIds(anIndexInsideNewIds) :: newIds))
    } yield (
      NonEmptySeq.fromSeqUnsafe(
        recordedPositionsToBeAmended
          .zip(idsWithOneDuplicated)
          .map(pair => {
            val (recordedPosition, newId) = pair
            RecordedPosition(newId, recordedPosition.data)
          })
      ),
      idsWithOneDuplicated
    )
  }

  def updatesWith2DuplicatedIds(maxNumberOfPositions: Int): Gen[(NonEmptySeq[SingleUpdate], List[UUID])] = {
    for {
      pair <- recordedPositionsWith2DuplicatedIds(maxNumberOfPositions = 5)
      (recordedPositions, idsWith2Duplicated) = pair
    } yield (
      PortfoliosGen.toPortfolioUpdates(recordedPositions),
      idsWith2Duplicated
    )
  }

}
