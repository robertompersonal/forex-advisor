package org.binqua.forex.advisor.portfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfoliosEventSerializeAndDeserializeSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  property(testName = "PortfolioCreatedEvent can be serialized and deserialized") {
    forAll(PortfoliosGen.portfolioCreatedEvent) { portfolioCreatedEvent =>
      assertThatCanBeSerialiseAndDeserialize(portfolioCreatedEvent)
    }
  }

  property(testName = "PortfolioUpdatedEvent can be serialized and deserialized") {
    forAll(PortfoliosGen.portfolioUpdated) { portfolioUpdatedEvent =>
      assertThatCanBeSerialiseAndDeserialize(portfolioUpdatedEvent)
    }
  }

}
