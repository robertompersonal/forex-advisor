package org.binqua.forex.advisor.model

import cats.syntax.validated._
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class LotSizeSpec extends AnyPropSpec with ScalaCheckPropertyChecks with Matchers with GivenWhenThen {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  property(testName = "conversion to micro lot size should be correct") {
    LotSize.toMicro(StandardLot(1)) should be(MicroLot(100))
    LotSize.toMicro(MiniLot(1)) should be(MicroLot(10))
    LotSize.toMicro(MicroLot(1)) should be(MicroLot(1))
  }

  property(testName = "given a wrong combination of id and amount, maybeALotSize should return an error") {
    LotSize.maybeALotSize("adfd", -10) should be(
      List("Amount has to be >= 0. -10 is wrong", "Only micro,mini,standard are allowed. adfd as lot size identifier is wrong").invalid
    )

    LotSize.maybeALotSize("adfd", 2) should be(
      List("Only micro,mini,standard are allowed. adfd as lot size identifier is wrong").invalid
    )

    LotSize.maybeALotSize("micro", -11) should be(
      List("Amount has to be >= 0. -11 is wrong").invalid
    )
  }

  property(testName = "valid combination of id and amount generate the right LotSize") {
    val lotSize = for {
      amount <- Gen.chooseNum(0, 1000)
      lotSize <-
        Gen.oneOf(("micro", (amount: Int) => MicroLot(amount)), ("mini", (amount: Int) => MiniLot(amount)), ("standard", (amount: Int) => StandardLot(amount)))
    } yield (lotSize._1, amount, lotSize._2)

    forAll(lotSize) { values =>
      val (size, amount, expLotSize) = values
      LotSize.maybeALotSize(size, amount) should be(expLotSize(amount).valid)
    }
  }

}
