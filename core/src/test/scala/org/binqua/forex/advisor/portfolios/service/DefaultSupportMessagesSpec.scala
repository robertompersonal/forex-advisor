package org.binqua.forex.advisor.portfolios.service

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import eu.timepit.refined.numeric.{NonNegative, Positive}
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios
import org.binqua.forex.advisor.portfolios.service.PortfoliosServiceGen.command
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import scala.concurrent.duration._

class DefaultSupportMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val underTest: SupportMessages = new DefaultSupportMessages(
    Config.safe(20.millis, maxRetryAttempts = refineMV[NonNegative](3), maxInFlightMessages = refineMV[Positive](5))
  )

  val portfoliosResponseProbe = testKit.createTestProbe[DeliveryGuaranteedPortfolios.Response]().ref

  val portfoliosServiceProbe = testKit.createTestProbe[PortfoliosService.Response]().ref

  property(testName = "messageConfirmed") {
    forAll(PortfoliosServiceGen.inFlightCommandRecord(command(portfoliosResponseProbe), portfoliosServiceProbe)) { inFlightCommandRecord =>
      Given(message = s"an inFlightCommandRecord $inFlightCommandRecord")

      val actual = underTest.messageConfirmed(inFlightCommandRecord)

      Then(message = s"messageConfirmed log is: $actual")

      actual shouldBe s"message with id ${inFlightCommandRecord.command.messageId.id} confirmed after ${inFlightCommandRecord.numberOfRetriesStarted} retries"
    }
  }

  property(testName = "retryIntervalExpired") {
    forAll(PortfoliosServiceGen.inFlightCommandRecord(command(portfoliosResponseProbe), portfoliosServiceProbe)) { inFlightCommandRecord =>
      Given(message = s"an inFlightCommandRecord $inFlightCommandRecord")

      val actual = underTest.retryIntervalExpired(inFlightCommandRecord)

      Then(message = s"retryIntervalExpired log is: $actual")

      actual shouldBe s"after 20 milliseconds I did not received a confirmation message for message with id ${inFlightCommandRecord.command.messageId.id}. I am going to retry now: retry number ${inFlightCommandRecord.numberOfRetriesStarted} of 3"
    }
  }

  property(testName = "maxInFlightMessagesReached") {
    underTest.maxInFlightMessagesReached() shouldBe s"5 max inflight messages reached. Going to reject extra messages until inflight messages number is below 5"
  }

  property(testName = "maxRetriesReached") {
    forAll(PortfoliosServiceGen.inFlightCommandRecord(command(portfoliosResponseProbe), portfoliosServiceProbe)) { inFlightCommandRecord =>
      Given(message = s"an inFlightCommandRecord $inFlightCommandRecord")

      val actual = underTest.maxRetriesReached(inFlightCommandRecord)

      Then(message = s"maxRetriesReached log is: $actual")

      actual shouldBe s"3 max retries reached for message with id ${inFlightCommandRecord.command.messageId.id}. Message has not be confirmed and I am going to stop retrying"
    }
  }

}
