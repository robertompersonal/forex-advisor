package org.binqua.forex.advisor.newportfolios

import com.typesafe.config.ConfigFactory
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{Assertion, GivenWhenThen}

import scala.concurrent.duration._

class ConfigValidatorSpec extends AnyFlatSpecLike with GivenWhenThen with TestingFacilities with ConfigurationSpecFacilities {

  object keys {
    val garbageCollectorInterval = "org.binqua.forex.advisor.portfolios.garbageCollectorInterval"
    val maxNumberOfPortfolios = "org.binqua.forex.advisor.portfolios.maxNumberOfPortfolios"
    val maxNumberOfPositions = "org.binqua.forex.advisor.portfolios.maxNumberOfPositions"
  }

  object expectedErrors {
    val maxNumberOfPortfolios = s"${keys.maxNumberOfPortfolios} has to exist and has to be a positive integer"
    val maxNumberOfPositions = s"${keys.maxNumberOfPositions} has to exist and has to be a positive integer"
    val wrongGarbageCollectorInterval = s"key ${keys.garbageCollectorInterval} does not have a valid duration value"
  }

  private val validRows: Set[String] = Set(
    s"${keys.maxNumberOfPortfolios}  = 1 ",
    s"${keys.maxNumberOfPositions}  = 11 ",
    s"${keys.garbageCollectorInterval} = 10s "
  )

  "Given all properties values are valid, we" should "get a valid config" in {
    val actualConfig = Config.Validator(toAkkaConfig(validRows)).getOrElse(thisTestShouldNotHaveArrivedHere)

    actualConfig.maxNumberOfPortfolios should be(1)
    actualConfig.maxNumberOfPositions should be(11)
    actualConfig.garbageCollectorInterval should be(10.seconds)
  }

  "Given config values are all wrong, we" should "get valid errors" in {
    val actualErrors: Seq[String] = Config
      .Validator(toAkkaConfig(validRows.map((row: String) => row.replace("1", "a"))))
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    actualErrors.head should be("Configuration for Portfolios is invalid:")

    actualErrors.tail should contain(expectedErrors.maxNumberOfPortfolios)
    actualErrors.tail should contain(expectedErrors.maxNumberOfPositions)
    actualErrors.tail should contain(expectedErrors.wrongGarbageCollectorInterval)
    actualErrors.size shouldBe 4
  }

  "an empty config" should "be spotted" in {
    val actualErrors: Seq[String] = Config.Validator(ConfigFactory.parseString("{}")).swap.getOrElse(thisTestShouldNotHaveArrivedHere)
    actualErrors.head should be("Configuration for Portfolios is invalid:")
    actualErrors.size shouldBe 4
  }

  "A config without maxNumberOfPortfolios" should "be invalid" in {
    Config
      .Validator(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.maxNumberOfPortfolios)))
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere) should contain(expectedErrors.maxNumberOfPortfolios)
  }

  "A config without maxNumberOfPositions" should "be invalid" in {
    Config
      .Validator(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.maxNumberOfPositions)))
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere) should contain(expectedErrors.maxNumberOfPositions)
  }

  "A config without garbageCollectorInterval" should "be invalid" in {
    Config
      .Validator(toANewAkkaConfig(RowsFilter(validRows, entryToBeRemoved = keys.garbageCollectorInterval)))
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere) should contain(s"cannot find a duration value ... key ${keys.garbageCollectorInterval} is missing")
  }

  "Config" should "know when its arguments are valid" in {
    def isValid(actualConfig: Config): Assertion = {
      actualConfig.maxNumberOfPortfolios shouldBe 1
      actualConfig.maxNumberOfPositions shouldBe 2
      actualConfig.garbageCollectorInterval shouldBe 100.second
    }

    isValid(
      Config
        .validated(
          maxNumberOfPortfolios = 1,
          maxNumberOfPositions = 2,
          garbageCollectorInterval = 100.second
        )
        .getOrElse(thisTestShouldNotHaveArrivedHere)
    )
  }

  "Config" should "know when its arguments are invalid" in {
    val errors: Seq[String] = Config
      .validated(maxNumberOfPortfolios = -1, maxNumberOfPositions = -2, garbageCollectorInterval = 100.second)
      .swap
      .getOrElse(thisTestShouldNotHaveArrivedHere)

    errors should contain("maxNumberOfPortfolios has to be > 0")
    errors should contain("maxNumberOfPositions has to be > 0")
    errors should have size 2
  }

}
