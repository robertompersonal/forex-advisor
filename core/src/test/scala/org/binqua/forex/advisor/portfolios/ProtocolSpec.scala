package org.binqua.forex.advisor.portfolios

import cats.data.NonEmptySeq
import cats.data.Validated.{Invalid, Valid}
import org.binqua.forex.advisor.portfolios.PortfoliosModel._
import org.binqua.forex.util.{StringUtils, TestingFacilities}
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import java.util.UUID
import scala.util.{Failure, Success, Try}

class ProtocolSpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  val aPositionId = UUID.randomUUID()

  val aValidPortfolioName = PortfoliosGen.portfolioName.sample.get

  val expErrorMessage = "Portfolio name has to be minimum 3 chars and maximum 30 without leading or trailing whitespaces"

  property(testName = "Portfolio name has to be minimum 3 chars and maximum 30") {

    forAll(StringUtils.ofLength(lessOrEqualThan = 2, greaterOrEqualThan = 31)) { invalidPortfolioName =>
      info("-----")

      PortfolioName.maybeAPortfolioName(invalidPortfolioName)

      Given(message = s"An invalid name $invalidPortfolioName with name length ${invalidPortfolioName.length}")

      When(message = "we create a PortfolioName")

      PortfolioName.maybeAPortfolioName(invalidPortfolioName) should be({
        Then(message = s"we get a valid error $expErrorMessage")
        Left(expErrorMessage)
      })

      the[IllegalArgumentException] thrownBy {
        PortfolioName.unsafe(invalidPortfolioName)
      } should have message expErrorMessage

    }
  }

  property("Portfolio name has to have length >= 3 and <= 30 without leading or trailing whitespace") {

    forAll(StringUtils.ofLengthBetween(3, 30)) { validName =>
      info("-----")

      Given(message = s"A valid name $validName with name length ${validName.length}")

      When(message = "we create a PortfolioName")

      PortfolioName.maybeAPortfolioName(validName) match {
        case Right(actual) =>
          Then(message = s"we get a valid portfolio name $actual")
          actual.name.value should be(validName)
        case Left(_) =>
          thisTestShouldNotHaveArrivedHere
      }

      PortfolioName.unsafe(validName).name.value should be(validName)

    }
  }

  property("Portfolio name with whitespaces at the beginning and/or at the end is invalid") {

    forAll(StringUtils.ofLengthBetween(10, 20)) { validName =>
      forAll(StringUtils.nonTrimmed(validName, maxWhitespaces = 10)) { nameWithWhitespaces =>
        info("-----")

        Given(message = s"An invalid name <$nameWithWhitespaces> with whitespaces")

        When(message = "we create a PortfolioName")

        PortfolioName.maybeAPortfolioName(nameWithWhitespaces) should be({
          Then(message = s"we get a valid error $expErrorMessage")
          Left(expErrorMessage)
        })

        the[IllegalArgumentException] thrownBy {
          PortfolioName.unsafe(nameWithWhitespaces)
        } should have message expErrorMessage

      }
    }
  }

  property("positions in CreatePortfolio cannot be empty") {

    val message = List(s"Positions cannot be empty")

    CreatePortfolio.validated(PortfoliosGen.idempotentKey.sample.get, PortfoliosGen.portfolioName.sample.get, List.empty) match {
      case Valid(_) =>
        thisTestShouldNotHaveArrivedHere
      case Invalid(e) =>
        Then(message = s"we get a valid error $e")
        e should be(message)
    }

  }

  property("positions in UpdatePortfolio cannot be empty") {
    UpdatePortfolio.validated(None, PortfoliosGen.portfolioName.sample.get, positions = List()) match {
      case Valid(_) =>
        thisTestShouldNotHaveArrivedHere
      case Invalid(errors) =>
        Then(message = s"we get a valid error $errors")
        errors should contain("Positions cannot be empty")
    }
  }

  property("delete positions in UpdatePortfolio cannot have duplicated ids") {

    val invalidPositionIds: Seq[UUID] = List(aPositionId, aPositionId)

    UpdatePortfolio.validated(None, aValidPortfolioName, positions = invalidPositionIds.map(Delete).toList) match {
      case Valid(_) =>
        thisTestShouldNotHaveArrivedHere
      case Invalid(errors) =>
        errors should contain(s"In all position ids to be deleted $invalidPositionIds there are duplicated entries")
    }
  }

  property("update positions in UpdatePortfolio cannot have duplicated ids") {

    val invalidPositionIds: Seq[UUID] = List(aPositionId, aPositionId)

    val invalidPositions: List[PortfoliosModel.Update] = invalidPositionIds.map(PortfoliosModel.Update(_, PortfoliosGen.position.sample.get)).toList

    UpdatePortfolio.validated(None, aValidPortfolioName, positions = invalidPositions) match {
      case Valid(_) =>
        thisTestShouldNotHaveArrivedHere
      case Invalid(errors) =>
        errors should contain(s"In all position ids to be updates $invalidPositionIds there are duplicated entries")
    }
  }

  property("update and delete positions combined in UpdatePortfolio cannot have duplicated ids") {

    val invalidPositionIds: Seq[UUID] = List(aPositionId, aPositionId)

    val invalidPositions: List[PortfoliosModel.PortfolioSingleUpdate] =
      List(PortfoliosModel.Update(aPositionId, PortfoliosGen.position.sample.get), Delete(aPositionId))

    UpdatePortfolio.validated(None, aValidPortfolioName, positions = invalidPositions) match {
      case Valid(_) =>
        thisTestShouldNotHaveArrivedHere
      case Invalid(errors) =>
        errors should contain(s"In all position ids to be deleted and updated combined $invalidPositionIds there are duplicated entries")
    }
  }

  property("UpdatePortfolio with no idempotent key cannot have add updates") {

    def deletes(min: Int, max: Int): Gen[Seq[Delete]] =
      (for {
        numberOfDeletes <- Gen.choose(min, max)
        deletes <- Gen.listOfN(numberOfDeletes, Gen.uuid.map(Delete))
      } yield deletes).map(_.toSeq)

    val updateGen: Gen[Update] = for {
      x <- Gen.uuid
      y <- PortfoliosGen.position
    } yield Update(x, y)

    def updates(min: Int, max: Int): Gen[Seq[Update]] =
      (for {
        numberOfDeletes <- Gen.choose(min, max)
        deletes <- Gen.listOfN(numberOfDeletes, updateGen)
      } yield deletes).map(_.toSeq)

    val addGen: Gen[Add] = for {
      position <- PortfoliosGen.position
    } yield Add(position)

    def adds(min: Int, max: Int): Gen[Seq[Add]] =
      (for {
        numberOfDeletes <- Gen.choose(min, max)
        deletes <- Gen.listOfN(numberOfDeletes, addGen)
      } yield deletes).map(_.toSeq)

    forAll(adds(1, 3), deletes(0, 3), updates(0, 3), PortfoliosGen.portfolioName) {
      (adds: Seq[Add], deletes: Seq[Delete], updates: Seq[Update], portfolioName) =>
        val expectedErrorMessage = "No adds update are allowed if idempotent key is not present"

        UpdatePortfolio.validated(maybeAnIdempotentKey = None, portfolioName, positions = adds ++ deletes ++ updates) match {
          case Valid(_) => thisTestShouldNotHaveArrivedHere
          case Invalid(errors) =>
            errors should have(size(1))
            errors.head should be(expectedErrorMessage)
        }

        the[IllegalArgumentException] thrownBy {
          UpdatePortfolio.unsafe(maybeAnIdempotentKey = None, portfolioName, NonEmptySeq.fromSeq(adds ++ deletes ++ updates).get)
        } should have message expectedErrorMessage

    }

    forAll(deletes(1, 3), updates(1, 3), PortfoliosGen.portfolioName) { (deletes: Seq[Delete], updates: Seq[Update], portfolioName) =>
      UpdatePortfolio.validated(maybeAnIdempotentKey = None, portfolioName, positions = deletes ++ updates) match {
        case Valid(_)   =>
        case Invalid(_) => thisTestShouldNotHaveArrivedHere
      }

      Try(UpdatePortfolio.unsafe(maybeAnIdempotentKey = None, portfolioName, NonEmptySeq.fromSeq(deletes ++ updates).get)) match {
        case Success(_) =>
        case Failure(_) => thisTestShouldNotHaveArrivedHere
      }

    }

  }

}
