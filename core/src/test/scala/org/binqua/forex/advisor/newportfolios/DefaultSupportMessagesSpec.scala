package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.prop.TableDrivenPropertyChecks.whenever
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks.forAll

class DefaultSupportMessagesSpec extends ScalaTestWithActorTestKit with AnyPropSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  property(testName = "idempotentKeyMismatch for creation") {
    forAll(PortfoliosGen.createPortfolio) { createPortfolio =>
      DefaultSupportMessages.idempotentKeyMismatch(createPortfolio) should be(
        s"It seems that the idempotent key ${createPortfolio.idempotentKey.key.toString} of this portfolio creation is already associated with a command hash that is different from the new one. This happens when you reuse the same idempotent key with different data"
      )

    }
  }

  property(testName = "idempotentKeyMismatch for update") {
    forAll(PortfoliosGen.updatePortfolio) { updatePortfolio =>
      whenever(updatePortfolio.maybeAnIdempotentKey.isDefined) {
        DefaultSupportMessages.idempotentKeyMismatch(updatePortfolio) should be(
          s"It seems that the idempotent key ${updatePortfolio.maybeAnIdempotentKey.get.key.toString} of this portfolio update is already associated with a command hash that is different from the new one. This happens when you reuse the same idempotent key with different data"
        )
      }
    }
  }
}
