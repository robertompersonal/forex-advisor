package org.binqua.forex.advisor.portfolios

import cats.data.Validated.{Invalid, Valid}
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.ConfigurationSpecFacilities
import org.binqua.forex.util.TestingFacilities
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, _}

class ConfigReaderAndValidatorSpec extends AnyFlatSpec with TestingFacilities with ConfigurationSpecFacilities {

  private val errorPrefix = "Portfolios configuration is invalid."
  private val maxNumberOfPortfoliosKey = "org.binqua.forex.advisor.portfolios.maxNumberOfPortfolios"
  private val maxNumberOfPositionsKey = "org.binqua.forex.advisor.portfolios.maxNumberOfPositions"

  private val referenceData: TesterData = TesterData(Config.unsafe(maxNumberOfPortfolios = 1, maxNumberOfPositions = 2))

  case class TesterData(theExpectedConfig: Config) {
    val validRows: Set[String] =
      configWith(maxNumberOfPortfoliosValue = theExpectedConfig.maxNumberOfPortfolios, maxNumberOfPositionsValue = theExpectedConfig.maxNumberOfPositions)
  }

  "given a valid akka config, unsafeConfigReader" should "returns a valid config" in {
    assertAreEquals(unsafeConfigReader(toAkkaConfig(referenceData.validRows)), referenceData.theExpectedConfig)
  }

  "given a valid akka config, configValidator" should "returns a valid config" in {
    ConfigValidator(toAkkaConfig(referenceData.validRows)) match {
      case Valid(actualConfig) => assertAreEquals(actualConfig, referenceData.theExpectedConfig)
      case _                   => thisTestShouldNotHaveArrivedHere(because = "the configuration is valid")
    }
  }

  "maxNumberOfPortfolios" should "be > 0" in {
    assertItWorks(
      toBeTested = toAkkaConfig(configWith(maxNumberOfPortfoliosValue = -1, maxNumberOfPositionsValue = 2)),
      expectedErrors = s"$errorPrefix - $maxNumberOfPortfoliosKey has to be > 0. -1 is not valid."
    )
  }

  "maxNumberOfPositions" should "be > 0" in {
    assertItWorks(
      toBeTested = toAkkaConfig(configWith(maxNumberOfPortfoliosValue = 10, maxNumberOfPositionsValue = -2)),
      expectedErrors = s"$errorPrefix - $maxNumberOfPositionsKey has to be > 0. -2 is not valid."
    )
  }

  "a config file without maxNumberOfPortfolios" should "be invalid" in {
    assertItWorks(
      toBeTested = toAkkaConfig(removeConfigurationFor(RowsFilter(referenceData.validRows, maxNumberOfPortfoliosKey))),
      expectedErrors = s"$errorPrefix - $maxNumberOfPortfoliosKey has to exist and has to be an integer > 0."
    )
  }

  "a config file without maxNumberOfPositions" should "be invalid" in {
    assertItWorks(
      toBeTested = toAkkaConfig(removeConfigurationFor(RowsFilter(referenceData.validRows, maxNumberOfPositionsKey))),
      expectedErrors = s"$errorPrefix - $maxNumberOfPositionsKey has to exist and has to be an integer > 0."
    )
  }

  "an empty file" should "be invalid" in {
    assertItWorks(
      toBeTested = toAkkaConfig(Set()),
      expectedErrors =
        s"$errorPrefix - $maxNumberOfPortfoliosKey has to exist and has to be an integer > 0. - $maxNumberOfPositionsKey has to exist and has to be an integer > 0."
    )
  }

  private def assertConfigValidatorWorks(toBeTested: AkkaConfig, expectedErrors: String) = {
    ConfigValidator(toBeTested) match {
      case Invalid(errors) => errors should be(expectedErrors.split(" - ").toList)
      case _               => thisTestShouldNotHaveArrivedHere(because = "config is invalid")
    }
  }

  private def assertUnsafeConfigReaderWorks(toBeTested: AkkaConfig, expectedErrors: String) = {
    the[IllegalArgumentException] thrownBy {
      unsafeConfigReader(toBeTested)
    } should have message expectedErrors
  }

  def assertItWorks(toBeTested: AkkaConfig, expectedErrors: String): Any = {
    assertUnsafeConfigReaderWorks(toBeTested, expectedErrors)
    assertConfigValidatorWorks(toBeTested, expectedErrors)
  }

  def configWith(maxNumberOfPortfoliosValue: Int, maxNumberOfPositionsValue: Int): Set[String] =
    Set(
      s"$maxNumberOfPortfoliosKey = $maxNumberOfPortfoliosValue ",
      s"$maxNumberOfPositionsKey = $maxNumberOfPositionsValue "
    )

  private def assertAreEquals(actualConfig: Config, expectedConfig: Config) = {
    actualConfig.maxNumberOfPortfolios should be(expectedConfig.maxNumberOfPortfolios)
    actualConfig.maxNumberOfPositions should be(expectedConfig.maxNumberOfPositions)
  }

}
