package org.binqua.forex.advisor.portfolios

import org.binqua.forex.advisor.portfolios.PortfoliosModel.events.PortfolioCreated
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{PositionId, RecordedPosition}
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.Gen
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfolioCreatedSpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with Matchers with GivenWhenThen with TestingFacilities {

  property(testName = "PortfolioCreated cannot be created with duplicated ids") {

    forAll(PortfoliosGen.portfolioCreatedEvent(minNumberOfPositions = 2)) { portfolioCreated =>
      val recordedPositionsWithDuplicatedIds = portfolioCreated.recordedPositions.concat(
        Seq(RecordedPosition(id = findAnExistingIdIn(portfolioCreated), data = PortfoliosGen.position.sample.get))
      )

      val expectedMessage =
        s"It is not possible to have a duplicated ids in record position ids ${recordedPositionsWithDuplicatedIds.map(_.id).toSeq.mkString("[", ",", "]")}"

      the[IllegalArgumentException] thrownBy {
        PortfolioCreated.unsafe(portfolioCreated.portfolioName, recordedPositionsWithDuplicatedIds)
      } should have message expectedMessage

    }

  }

  def findAnExistingIdIn(portfolioCreated: PortfolioCreated): PositionId = {
    val randomIndex = Gen.choose(min = 0, max = portfolioCreated.recordedPositions.length - 1).sample.get
    portfolioCreated.recordedPositions.map(_.id).getUnsafe(randomIndex)
  }

}
