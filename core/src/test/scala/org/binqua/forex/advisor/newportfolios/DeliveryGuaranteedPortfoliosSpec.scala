package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.PersistenceId
import cats.Id
import cats.data.{NonEmptySeq, NonEmptySet, Reader}
import cats.implicits.catsSyntaxOptionId
import com.typesafe.config.{ConfigFactory, Config => AkkaConfig}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineMV
import org.apache.commons.io.FileUtils
import org.binqua.forex.advisor.model.TradingPairGen
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.{ResponseType, _}
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.advisor.portfolios.PortfoliosGen.{dateGen, lotSizeGen}
import org.binqua.forex.advisor.portfolios.PortfoliosModel._
import org.binqua.forex.advisor.portfolios.service.{IdempotentKey, MessageId, UUIDMessageId}
import org.binqua.forex.util.core.{UnsafeConfigReader, _}
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.{DoNotDiscover, GivenWhenThen}

import java.io.File
import java.util.UUID
import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.duration._
import scala.util.Random

object DeliveryGuaranteedPortfoliosSpec {

  private val baseDir =
    s"${FileUtils.getTempDirectoryPath}/forexAdvisorProject/test"

  val healthCheckConfig = ConfigFactory.parseString(s"""|akka {
        |  loglevel = "INFO"
        |  actor {
        |    serializers {jackson-json = "akka.serialization.jackson.JacksonJsonSerializer" }
        |    serialization-bindings { "org.binqua.forex.JsonSerializable" = jackson-json }
        |  }
        |
        |   persistence.journal.plugin = "akka.persistence.journal.leveldb"
        |   persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
        |
        |   persistence.journal.leveldb.dir = "$baseDir/journal"
        |   persistence.snapshot-store.local.dir = "$baseDir/snapshots"
        |
        |}
        |
        |  org.binqua.forex.advisor.portfolios.maxNumberOfPortfolios = 3
        |  org.binqua.forex.advisor.portfolios.maxNumberOfPositions = 100
        |  org.binqua.forex.advisor.portfolios.garbageCollectorInterval = 3600s
        |
        |""".stripMargin)
}

@DoNotDiscover
class DeliveryGuaranteedPortfoliosSpec
    extends ScalaTestWithActorTestKit(DeliveryGuaranteedPortfoliosSpec.healthCheckConfig)
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with Validation {

  implicit val actorConfiguration: Config = Config.validated(maxNumberOfPortfolios = 3, maxNumberOfPositions = 4, garbageCollectorInterval = 20.seconds).unsafe

  val storageLocations = List(
    new File(system.settings.config.getString("akka.persistence.journal.leveldb.dir")),
    new File(system.settings.config.getString("akka.persistence.snapshot-store.local.dir"))
  )

  override def beforeAll(): Unit = {
    super.beforeAll()
    storageLocations.foreach(FileUtils.deleteDirectory)
  }

  override def afterAll(): Unit = {
    storageLocations.foreach(FileUtils.deleteDirectory)
    super.afterAll()
  }

  "A portfolios" should "read the configuration" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    (for {
      _ <- Reader((configurationTester: ConfigurationReaderTester) =>
        testKit.spawn(
          behaviors.portfolios(
            initialPersistenceId,
            positionIdFactory(uuidGenerator(numberOfUUIDsToBeGenerated = refineMV[Positive](1))),
            configurationTester
          )
        )
      )
      _ <- Reader((configurationTester: ConfigurationReaderTester) => configurationTester.assertThatConfigurationHasBeenRead())
    } yield ()).run(configReaderTester(configToBeReturned = actorConfiguration))

  }

  "A portfolios newly created" should "be empty. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(
      behaviors.portfolios(initialPersistenceId, positionIdFactory(uuidGenerator(numberOfUUIDsToBeGenerated = refineMV[Positive](1))))
    )

    (for {
      _ <- Reader(cmd => portfoliosActor ! cmd)
      _ <- Reader((cmd: ExternalCommand) => responseProbe.expectMessage(Response(cmd.messageId, Success(PortfoliosSummary.empty))))
    } yield ()).run(ExternalCommand(aMessageId(), ShowPortfoliosPayload, responseProbe.ref))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = Success(PortfoliosSummary.empty),
      responseProbe = responseProbe
    )

  }

  "Create a new portfolio" should "be possible. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val uuidToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV[Positive](2))

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, positionIdFactory(uuidToBeGenerated)))

    val createPortfolio = createAPortfolio(numberOfPositions = 2, aPortfolioName)

    val createPortfolioCmd = ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), replyTo = responseProbe.ref)

    portfoliosActor ! createPortfolioCmd

    val recordedPositions: List[RecordedPosition] = List(
      calculateExpectedPosition(
        uuidToBeGenerated(0),
        createPortfolio.positions.head
      ),
      calculateExpectedPosition(
        uuidToBeGenerated(1),
        createPortfolio.positions.tail.head
      )
    )

    val expectedState: PortfoliosSummary = PortfoliosSummary.unsafe(Set(Portfolio(createPortfolio.portfolioName, recordedPositions.toSet)))

    responseProbe.expectMessage(Response(createPortfolioCmd.messageId, Success(expectedState)))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = Success(expectedState),
      responseProbe = responseProbe
    )

  }

  "Portfolio creation" should "be idempotent. It returns actual state if applied more than one" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val positionIdToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV(2))

    val portfoliosActor = testKit.spawn(
      behaviors.portfolios(
        initialPersistenceId,
        positionIdFactory(positionIdToBeGenerated)
      )
    )

    val createPortfolio: CreatePortfolio = createAPortfolio(numberOfPositions = 2, aPortfolioName)

    val createPortfolioCmd: ExternalCommand =
      ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), replyTo = responseProbe.ref)

    portfoliosActor ! createPortfolioCmd
    portfoliosActor ! createPortfolioCmd
    portfoliosActor ! createPortfolioCmd

    val expectedPortfoliosSummary: PortfoliosSummary = PortfoliosSummary.unsafe(
      Set(
        Portfolio(
          createPortfolio.portfolioName,
          Set(
            calculateExpectedPosition(
              positionIdToBeGenerated(0),
              createPortfolio.positions.head
            ),
            calculateExpectedPosition(
              positionIdToBeGenerated(1),
              createPortfolio.positions.tail.head
            )
          )
        )
      )
    )

    responseProbe.expectMessage(Response(createPortfolioCmd.messageId, Success(expectedPortfoliosSummary)))
    responseProbe.expectMessage(Response(createPortfolioCmd.messageId, Success(expectedPortfoliosSummary)))
    responseProbe.expectMessage(Response(createPortfolioCmd.messageId, Success(expectedPortfoliosSummary)))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = Success(expectedPortfoliosSummary),
      responseProbe = responseProbe
    )

  }

  "Given 2 commands with same idempotent key and different command hash, then Portfolio creation" should "generate an error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    val createPortfolio: CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

    val firstMessageId: MessageId = aMessageId()

    val createPortfolioCmd: ExternalCommand = ExternalCommand(firstMessageId, payload = CreatePortfolioPayload(createPortfolio), replyTo = responseProbe.ref)

    portfoliosActor ! createPortfolioCmd

    val firstResponse: Response = responseProbe.expectMessageType[Response]

    val suspiciousCreatePortfolio: CreatePortfolio = PortfoliosGen.createPortfolio.sample.get.copy(idempotentKey = createPortfolio.idempotentKey)

    val suspiciousCommand: ExternalCommand = ExternalCommand(
      aMessageId(),
      CreatePortfolioPayload(suspiciousCreatePortfolio),
      replyTo = responseProbe.ref
    )
    portfoliosActor ! suspiciousCommand

    responseProbe.expectMessage(Response(suspiciousCommand.messageId, Error(supportMessages.idempotentKeyMismatch(suspiciousCreatePortfolio))))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = firstResponse.responseType,
      responseProbe = responseProbe
    )

  }

  "Delete" should "be idempotent. Delete same portfolios multiple times has the same effect that delete the portfolio once. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    val createFirstPortfolio: CreatePortfolio = createAPortfolio(numberOfPositions = 2, aPortfolioName = PortfoliosGen.portfolioName.sample.get)
    val createSecondPortfolio: CreatePortfolio = createAPortfolio(numberOfPositions = 2, aPortfolioName = PortfoliosGen.portfolioName.sample.get)

    List(createFirstPortfolio, createSecondPortfolio).foreach { toBeCreated =>
      portfoliosActor ! ExternalCommand(aMessageId(), CreatePortfolioPayload(toBeCreated), replyTo = responseProbe.ref)
      responseProbe.expectMessageType[Response]
    }

    val toBeDeleted = NonEmptySet.of(createFirstPortfolio.portfolioName, createSecondPortfolio.portfolioName)

    val deletePortfolioCommand = ExternalCommand(aMessageId(), payload = DeletePortfoliosPayload(toBeDeleted), replyTo = responseProbe.ref)

    val expectedPortfoliosSummary: PortfoliosSummary = PortfoliosSummary.unsafe(Set())

    (1 to 3).foreach(_ => {
      portfoliosActor ! deletePortfolioCommand
      responseProbe.expectMessage(Response(deletePortfolioCommand.messageId, Success(expectedPortfoliosSummary)))
    })

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = Success(expectedPortfoliosSummary),
      responseProbe = responseProbe
    )

  }

  "Delete a portfolio that does not exist" should "not be considered an error and it returns actual state. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActorUnderTest = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    portfoliosActorUnderTest ! ExternalCommand(
      messageId = aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 2, aPortfolioName = aPortfolioName)),
      replyTo = responseProbe.ref
    )

    private val expectedStatusReply: ResponseType = responseProbe.expectMessageType[Response].responseType

    private val IDontExistPortfolioName: PortfolioName = PortfoliosGen.portfolioName.sample.get

    val deletePortfolioCommand = ExternalCommand(
      messageId = aMessageId(),
      payload = DeletePortfoliosPayload(NonEmptySet.of(IDontExistPortfolioName)),
      replyTo = responseProbe.ref
    )

    portfoliosActorUnderTest ! deletePortfolioCommand

    responseProbe.expectMessage(Response(deletePortfolioCommand.messageId, expectedStatusReply))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActorUnderTest,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = expectedStatusReply,
      responseProbe = responseProbe
    )

  }

  "update an existing portfolio with add, delete and update" should "be possible. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val the4UUIDsToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV(4))

    object initialPositions {
      val toBeLeftUnchanged: Position = aPosition()
      val toBeUpdated: Position = aPosition()
      val toBeDeleted: Position = aPosition()
    }

    val createPortfolio: CreatePortfolio = CreatePortfolio(
      PortfoliosGen.idempotentKey.sample.get,
      aPortfolioName,
      NonEmptySeq.fromSeqUnsafe(
        Seq(
          initialPositions.toBeLeftUnchanged,
          initialPositions.toBeUpdated,
          initialPositions.toBeDeleted
        )
      )
    )

    val firstCreatePortfolioCmd = ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, positionIdFactory(the4UUIDsToBeGenerated)))

    portfoliosActor ! firstCreatePortfolioCmd

    responseProbe.expectMessageType[Response]

    val positionToBeAdded: Add = Add(aPosition())
    val positionToBeUpdated: Update = Update(the4UUIDsToBeGenerated(1), aPosition())
    val positionToBeDelete: Delete = Delete(the4UUIDsToBeGenerated(2))

    val updatePortfolio = UpdatePortfolio.unsafe(
      PortfoliosGen.idempotentKey.sample.get.some,
      createPortfolio.portfolioName,
      NonEmptySeq(positionToBeAdded, Seq(positionToBeUpdated, positionToBeDelete))
    )

    val updatePortfolioCmd = ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

    portfoliosActor ! updatePortfolioCmd

    val expectedPortfoliosSummary = PortfoliosSummary.unsafe(
      Set(
        Portfolio(
          createPortfolio.portfolioName,
          Set(
            calculateExpectedPosition(the4UUIDsToBeGenerated(0), initialPositions.toBeLeftUnchanged),
            calculateExpectedPosition(the4UUIDsToBeGenerated(1), positionToBeUpdated.thePositionData),
            calculateExpectedPosition(the4UUIDsToBeGenerated(3), positionToBeAdded.thePositionData)
          )
        )
      )
    )

    responseProbe.expectMessage(Response(updatePortfolioCmd.messageId, Success(expectedPortfoliosSummary)))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = Success(expectedPortfoliosSummary),
      responseProbe = responseProbe
    )

  }

  "Update a portfolio by adding more positions than maxNumberOfPositions config property" should "not be possible" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    private val initialPositions = Seq(aPosition())

    val createPortfolio: CreatePortfolio = CreatePortfolio(PortfoliosGen.idempotentKey.sample.get, aPortfolioName, NonEmptySeq.fromSeqUnsafe(initialPositions))

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    private val expectedResponseType: Success = responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    val testWeGetRightErrorIfWeTryToCreateExtraAdds: Int => Unit = extraAddPositionsToBeAdded => {
      val updatePortfolio = UpdatePortfolio.unsafe(
        PortfoliosGen.idempotentKey.sample.get.some,
        createPortfolio.portfolioName,
        NonEmptySeq.fromSeqUnsafe(Vector.fill(extraAddPositionsToBeAdded)(Add(aPosition())))
      )

      (for {
        _ <- Reader(cmd => portfoliosActor ! cmd)
        _ <- Reader((cmd: ExternalCommand) =>
          responseProbe.expectMessage(
            Response(
              cmd.messageId,
              Error(errorMessages.tooManyPositionsForASinglePortfolio(createPortfolio.portfolioName, extraAddPositionsToBeAdded + initialPositions.size))
            )
          )
        )
      } yield ()).run(ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref))

    }

    testWeGetRightErrorIfWeTryToCreateExtraAdds(actorConfiguration.maxNumberOfPositions - initialPositions.size + 1)

    testWeGetRightErrorIfWeTryToCreateExtraAdds(actorConfiguration.maxNumberOfPositions - initialPositions.size + 2)

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = expectedResponseType,
      responseProbe = responseProbe
    )

  }

  "Update a portfolio by adding a number of positions equals to maxNumberOfPositions config property" should "be possible" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    private val initialPositions = Seq(aPosition())

    val createPortfolio: CreatePortfolio = CreatePortfolio(PortfoliosGen.idempotentKey.sample.get, aPortfolioName, NonEmptySeq.fromSeqUnsafe(initialPositions))

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    val updatePortfolio = UpdatePortfolio.unsafe(
      PortfoliosGen.idempotentKey.sample.get.some,
      createPortfolio.portfolioName,
      NonEmptySeq.fromSeqUnsafe(Vector.fill(actorConfiguration.maxNumberOfPositions - initialPositions.size)(Add(aPosition())))
    )

    val updateCommand: ExternalCommand = ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

    portfoliosActor ! updateCommand

    val stateAfterLastUpdate: Success = responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    stateAfterLastUpdate.portfolioSummary.portfolios.head.positions.size should be(actorConfiguration.maxNumberOfPositions)

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = stateAfterLastUpdate,
      responseProbe = responseProbe
    )

  }

  "Update an existing portfolio with only delete and update but no adds" should "be possible. This operation is idempotent by nature and does not require idempotent id. Events are persisted" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val the3UUIDsToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV(3))

    object initialPositions {
      val toBeLeftUnchanged: Position = aPosition()
      val toBeUpdated: Position = aPosition()
      val toBeDeleted: Position = aPosition()
    }

    val createPortfolio: CreatePortfolio = CreatePortfolio(
      PortfoliosGen.idempotentKey.sample.get,
      aPortfolioName,
      NonEmptySeq.fromSeqUnsafe(
        Seq(
          initialPositions.toBeLeftUnchanged,
          initialPositions.toBeUpdated,
          initialPositions.toBeDeleted
        )
      )
    )

    val firstCreatePortfolioCmd = ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, positionIdFactory(the3UUIDsToBeGenerated)))

    portfoliosActor ! firstCreatePortfolioCmd

    responseProbe.expectMessageType[Response]

    val positionToBeUpdated: Update = Update(the3UUIDsToBeGenerated(1), aPosition())
    val positionToBeDelete: Delete = Delete(the3UUIDsToBeGenerated(2))
    val positionToBeDeleteThatDoesNotExist: Delete = Delete(UUID.randomUUID())

    val updatePortfolio = UpdatePortfolio.unsafe(
      None,
      createPortfolio.portfolioName,
      NonEmptySeq(positionToBeUpdated, Seq(positionToBeDelete, positionToBeDeleteThatDoesNotExist))
    )

    val updatePortfolioCmd = ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

    portfoliosActor ! updatePortfolioCmd

    val expectedPortfoliosSummary = PortfoliosSummary.unsafe(
      Set(
        Portfolio(
          createPortfolio.portfolioName,
          Set(
            calculateExpectedPosition(the3UUIDsToBeGenerated(0), initialPositions.toBeLeftUnchanged),
            calculateExpectedPosition(the3UUIDsToBeGenerated(1), positionToBeUpdated.thePositionData)
          )
        )
      )
    )

    responseProbe.expectMessage(Response(updatePortfolioCmd.messageId, Success(expectedPortfoliosSummary)))

    portfoliosActor ! updatePortfolioCmd
    responseProbe.expectMessage(Response(updatePortfolioCmd.messageId, Success(expectedPortfoliosSummary)))

    portfoliosActor ! updatePortfolioCmd
    responseProbe.expectMessage(Response(updatePortfolioCmd.messageId, Success(expectedPortfoliosSummary)))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = Success(expectedPortfoliosSummary),
      responseProbe = responseProbe
    )

  }

  "Given 2 update commands with same idempotent key and different command hash, then Portfolio Update" should "generate an error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val the2UUIDsToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV(2))

    val createPortfolio: CreatePortfolio = createAPortfolio(numberOfPositions = 1, aPortfolioName)

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, positionIdFactory(the2UUIDsToBeGenerated)))

    portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    responseProbe.expectMessageType[Response]

    val firstUpdatePortfolio = UpdatePortfolio.unsafe(
      PortfoliosGen.idempotentKey.sample.get.some,
      createPortfolio.portfolioName,
      NonEmptySeq(Add(aPosition()), Seq())
    )

    val firstUpdatePortfolioCmd = ExternalCommand(aMessageId(), UpdatePortfolioPayload(firstUpdatePortfolio), responseProbe.ref)

    portfoliosActor ! firstUpdatePortfolioCmd

    private val updateResponse: Response = responseProbe.expectMessageType[Response]

    updateResponse.responseType shouldBe a[Success]

    val wrongUpdatePortfolio = UpdatePortfolio.unsafe(
      firstUpdatePortfolio.maybeAnIdempotentKey,
      createPortfolio.portfolioName,
      NonEmptySeq(Add(aPosition()), Seq())
    )

    private val wrongUpdateCommand: ExternalCommand = ExternalCommand(aMessageId(), UpdatePortfolioPayload(wrongUpdatePortfolio), responseProbe.ref)

    portfoliosActor ! wrongUpdateCommand

    responseProbe.expectMessage(Response(wrongUpdateCommand.messageId, Error(supportMessages.idempotentKeyMismatch(wrongUpdatePortfolio))))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = updateResponse.responseType,
      responseProbe = responseProbe
    )

  }

  "An update with no validation errors" should "be idempotent. It returns actual state if applied more than one" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val the4UUIDsToBeGenerated: Vector[PositionId] = uuidGenerator(numberOfUUIDsToBeGenerated = refineMV(4))

    val createPortfolio: CreatePortfolio = createAPortfolio(3, aPortfolioName)

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, positionIdFactory(the4UUIDsToBeGenerated)))

    portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    responseProbe.expectMessageType[Response]

    val updatePortfolio = UpdatePortfolio.unsafe(
      PortfoliosGen.idempotentKey.sample.get.some,
      createPortfolio.portfolioName,
      NonEmptySeq(Add(aPosition()), Seq(Update(the4UUIDsToBeGenerated(1), aPosition()), Delete(the4UUIDsToBeGenerated(2))))
    )

    val updatePortfolioCmd = ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

    portfoliosActor ! updatePortfolioCmd

    private val firstResponse: Response = responseProbe.expectMessageType[Response]

    portfoliosActor ! updatePortfolioCmd

    private val secondResponse: Response = responseProbe.expectMessageType[Response]

    portfoliosActor ! updatePortfolioCmd

    private val thirdResponse: Response = responseProbe.expectMessageType[Response]

    Set(firstResponse, secondResponse, thirdResponse).size should be(1)

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = thirdResponse.responseType,
      responseProbe = responseProbe
    )

  }

  "An update with idempotent key for a portfolio that does not exist" should "return always does not exist error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val createPortfolio: CreatePortfolio = createAPortfolio(numberOfPositions = 1, aPortfolioName)

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, constIdFactory))

    portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), responseProbe.ref)

    private val responseTypeAfterPortfolioCreation: ResponseType = responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    {
      val updatePortfolio = UpdatePortfolio.unsafe(
        PortfoliosGen.idempotentKey.sample.get.some,
        portfolioName = PortfoliosGen.portfolioName.sample.get,
        NonEmptySeq(Add(aPosition()), Seq())
      )

      (1 to 3).foreach(_ => {
        portfoliosActor ! ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)
        responseProbe.expectMessageType[Response].responseType should be(Error(errorMessages.portfolioDoesNotExist(updatePortfolio)))
      })
    }

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = responseTypeAfterPortfolioCreation,
      responseProbe = responseProbe
    )

  }

  "An update without idempotent key for a portfolio that does not exist" should "return always does not exist error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, constIdFactory))

    portfoliosActor ! ExternalCommand(
      aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, aPortfolioName)),
      responseProbe.ref
    )

    private val responseTypeAfterPortfolioCreation: ResponseType = responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    {
      val updatePortfolio = UpdatePortfolio.unsafe(
        None,
        portfolioName = PortfoliosGen.portfolioName.sample.get,
        NonEmptySeq(Delete(UUID.randomUUID()), Seq())
      )

      (1 to 3).foreach(_ => {

        portfoliosActor ! ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

        responseProbe.expectMessageType[Response].responseType should be(Error(errorMessages.portfolioDoesNotExist(updatePortfolio)))
      })
    }

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = responseTypeAfterPortfolioCreation,
      responseProbe = responseProbe
    )

  }

  "An update without idempotent key" should "return does not exist error if contains non existing position ids to be updated. Delete operations with non existing ids are ignored" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, constIdFactory))

    portfoliosActor ! ExternalCommand(
      aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, aPortfolioName)),
      responseProbe.ref
    )

    private val responseTypeAfterPortfolioCreation: ResponseType = responseProbe.expectMessageType[Response].responseType.asInstanceOf[Success]

    private val wrongUpdatePositionId = UUID.randomUUID()
    private val anotherWrongUpdatePositionId = UUID.randomUUID()
    private val wrongDeletePositionId = UUID.randomUUID()

    {
      val updatePortfolio = UpdatePortfolio.unsafe(
        None,
        portfolioName = createAPortfolio(numberOfPositions = 1, aPortfolioName).portfolioName,
        updates = NonEmptySeq(
          Update(wrongUpdatePositionId, aPosition()),
          Seq(Delete(wrongDeletePositionId), Update(anotherWrongUpdatePositionId, aPosition()))
        )
      )

      (1 to 3).foreach(_ => {

        portfoliosActor ! ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

        responseProbe.expectMessageType[Response].responseType should be(
          Error(errorMessages.positionsToBeUpdatedDoesNotExist(List(anotherWrongUpdatePositionId, wrongUpdatePositionId)))
        )
      })
    }

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = responseTypeAfterPortfolioCreation,
      responseProbe = responseProbe
    )

  }

  "An update with idempotent key" should "return does not exist error if contains non existing position ids to be updated. Delete operations with non existing ids are ignored" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, constIdFactory))

    portfoliosActor ! ExternalCommand(
      aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, aPortfolioName)),
      responseProbe.ref
    )

    private val actualResponse: Response = responseProbe.expectMessageType[Response]

    actualResponse.responseType should be(a[Success])

    private val responseTypeAfterPortfolioCreation: Success = actualResponse.responseType.asInstanceOf[Success]

    private val wrongUpdatePositionId = UUID.randomUUID()
    private val anotherWrongUpdatePositionId = UUID.randomUUID()
    private val wrongDeletePositionId = UUID.randomUUID()

    {
      val updatePortfolio = UpdatePortfolio.unsafe(
        PortfoliosGen.idempotentKey.sample.get.some,
        portfolioName = createAPortfolio(numberOfPositions = 1, aPortfolioName).portfolioName,
        updates = NonEmptySeq(
          Update(wrongUpdatePositionId, aPosition()),
          Seq(Delete(wrongDeletePositionId), Update(anotherWrongUpdatePositionId, aPosition()), Add(aPosition()))
        )
      )

      (1 to 3).foreach(_ => {

        portfoliosActor ! ExternalCommand(aMessageId(), UpdatePortfolioPayload(updatePortfolio), responseProbe.ref)

        responseProbe.expectMessageType[Response].responseType should be(
          Error(errorMessages.positionsToBeUpdatedDoesNotExist(List(anotherWrongUpdatePositionId, wrongUpdatePositionId)))
        )
      })
    }

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, newPositionIdFactory),
      expectedResponseType = responseTypeAfterPortfolioCreation,
      responseProbe = responseProbe
    )

  }

  "Create a number of portfolios greater than maxNumberOfPortfolios config property" should "not be possible" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    (1 to actorConfiguration.maxNumberOfPortfolios).foreach(numberOfPortfolios => {
      portfoliosActor ! ExternalCommand(
        aMessageId(),
        payload = CreatePortfolioPayload(
          createAPortfolio(numberOfPositions = 1, PortfoliosGen.portfolioName.sample.get)
        ),
        replyTo = responseProbe.ref
      )
      val responseType = responseProbe.expectMessageType[Response].responseType

      responseType should be(a[Success])

      responseType.asInstanceOf[Success].portfolioSummary.portfolios.size should be(numberOfPortfolios)
    })

    val actualStateWithMaxNumberOfPortfolios: Id[PortfoliosSummary] = (for {
      _ <- Reader(cmd => portfoliosActor ! cmd)
      actualState <- Reader((cmd: ExternalCommand) => {
        val actualResponse = responseProbe.expectMessageType[Response]
        actualResponse.retryId should be(cmd.messageId)
        val Success(state) = actualResponse.responseType
        state
      })
    } yield actualState).run(ExternalCommand(aMessageId(), ShowPortfoliosPayload, responseProbe.ref))

    portfoliosActor ! ExternalCommand(
      aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, PortfoliosGen.portfolioName.sample.get)),
      replyTo = responseProbe.ref
    )

    responseProbe.expectMessageType[Response].responseType should be(Error(errorMessages.maxNumberOfPortfoliosReached()))

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = Success(actualStateWithMaxNumberOfPortfolios),
      responseProbe = responseProbe
    )

  }

  "Create a portfolio with a number of positions greater than maxNumberOfPositions config property" should "not be able possible" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    (1 to 20).foreach { _ =>
      val tooManyPositions = Gen.choose(actorConfiguration.maxNumberOfPositions + 1, actorConfiguration.maxNumberOfPositions + 100).sample.get

      val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

      val createPortfolio = createAPortfolio(numberOfPositions = tooManyPositions, PortfoliosGen.portfolioName.sample.get)

      portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), replyTo = responseProbe.ref)

      val expectedResponseType: Error = Error(errorMessages.tooManyPositionsForASinglePortfolio(createPortfolio.portfolioName, tooManyPositions))

      responseProbe.expectMessageType[Response].responseType should be(expectedResponseType)

      testActorStateHasBeenPersisted(
        actorToBeTerminated = portfoliosActor,
        howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
        expectedResponseType = Success(PortfoliosSummary.empty),
        responseProbe = responseProbe
      )
    }
  }

  "Create a portfolio with a number of positions less or equal to the maxNumberOfPositions config property" should "be able possible" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    (1 to actorConfiguration.maxNumberOfPortfolios).foreach { _ =>
      val noTooManyPositions = Gen.choose(1, actorConfiguration.maxNumberOfPositions).sample.get

      val createPortfolio = createAPortfolio(numberOfPositions = noTooManyPositions, PortfoliosGen.portfolioName.sample.get)

      val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

      portfoliosActor ! ExternalCommand(aMessageId(), payload = CreatePortfolioPayload(createPortfolio), replyTo = responseProbe.ref)

      responseProbe.expectMessageType[Response].responseType should be(a[Success])
    }

  }

  "Create 2 portfolios with same name" should "give back an error" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    val portfoliosActor = testKit.spawn(behaviors.portfolios(initialPersistenceId, newPositionIdFactory))

    val createPortfolioCmd = ExternalCommand(
      aMessageId(),
      payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, aPortfolioName)),
      replyTo = responseProbe.ref
    )

    portfoliosActor ! createPortfolioCmd

    private val response: ResponseType = responseProbe.receiveMessage().responseType

    response should be(a[Success])

    val expectedState = response.asInstanceOf[Success]

    (for {
      _ <- Reader(cmd => portfoliosActor ! cmd)
      _ <- Reader((cmd: ExternalCommand) => responseProbe.expectMessage(Response(cmd.messageId, Error(errorMessages.portfolioAlreadyExist(aPortfolioName)))))
    } yield ()).run(
      ExternalCommand(
        aMessageId(),
        payload = CreatePortfolioPayload(createAPortfolio(numberOfPositions = 1, aPortfolioName)),
        replyTo = responseProbe.ref
      )
    )

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = expectedState,
      responseProbe = responseProbe
    )

  }

  "After 2 garbageCollectorIntervals expire" should "be possible to reuse an old command" in new TestContext(
    supportMessages = SupportMessagesContext.justForAnIdeaSupport,
    errorMessagesFactory = ErrorMessagesContext.justForAnIdea
  ) {

    private val garbageCollectorIntervalUnderTest: FiniteDuration = 1.second

    private val configurationTester = configReaderTester(configToBeReturned =
      Config.validated(maxNumberOfPortfolios = 3, maxNumberOfPositions = 10, garbageCollectorInterval = garbageCollectorIntervalUnderTest).unsafe
    )

    val portfoliosActor = testKit.spawn(
      behaviors.portfolios(initialPersistenceId, newPositionIdFactory, configurationTester)
    )

    configurationTester.assertThatConfigurationHasBeenRead()

    private val firstPayload: CreatePortfolioPayload = CreatePortfolioPayload(
      createAPortfolio(numberOfPositions = 1, aPortfolioName, PortfoliosGen.idempotentKey.sample.get)
    )

    val createPortfolioCmd = ExternalCommand(aMessageId(), payload = firstPayload, replyTo = responseProbe.ref)

    portfoliosActor ! createPortfolioCmd

    responseProbe.receiveMessage().responseType should be(a[Success])

    private val sameIdempotentKeyDifferentHashPayload = CreatePortfolioPayload(
      createAPortfolio(numberOfPositions = 2, PortfoliosGen.portfolioName.sample.get, firstPayload.portfolioToBeCreated.idempotentKey)
    )

    private val sameIdempotentKeyDifferentHashCommand: ExternalCommand = ExternalCommand(
      aMessageId(),
      sameIdempotentKeyDifferentHashPayload,
      replyTo = responseProbe.ref
    )

    portfoliosActor ! sameIdempotentKeyDifferentHashCommand

    responseProbe.receiveMessage().responseType should be(
      Error(
        supportMessages.idempotentKeyMismatch(sameIdempotentKeyDifferentHashPayload.portfolioToBeCreated)
      )
    )

    matchDistinctLogs(
      messages = "Running idempotenceJournal deleteEntries: deleted 0 entries. 1 entries left",
      "Running idempotenceJournal deleteEntries: deleted 1 entries. 0 entries left"
    ).expect {
      waitFor(garbageCollectorIntervalUnderTest * 2 + garbageCollectorIntervalUnderTest / 2: FiniteDuration, soThat("old entries can be deleted"))
    }

    val expectedState = {

      portfoliosActor ! sameIdempotentKeyDifferentHashCommand

      val newResponse: ResponseType = responseProbe.receiveMessage().responseType

      newResponse should be(a[Success])

      newResponse.asInstanceOf[Success]
    }

    testActorStateHasBeenPersisted(
      actorToBeTerminated = portfoliosActor,
      howCreateTheNewActor = () => behaviors.portfolios(initialPersistenceId, constIdFactory),
      expectedResponseType = expectedState,
      responseProbe = responseProbe
    )

  }

  object SupportMessagesContext {

    val justForAnIdeaSupport: SupportMessages = new SupportMessages {
      override def idempotentKeyMismatch(actualCreatePortfolio: CreatePortfolio): String =
        s"CreatePortfolio idempotentKeyMissMatch ${actualCreatePortfolio.idempotentKey.key}"

      override def idempotentKeyMismatch(actualUpdatePortfolio: UpdatePortfolio): String =
        s"UpdatePortfolio idempotentKeyMismatch ${actualUpdatePortfolio.maybeAnIdempotentKey.get.key}"

    }
  }

  object ErrorMessagesContext {
    val justForAnIdea: ErrorMessagesFactory =
      (config: Config) =>
        new ErrorMessages {
          override def maxNumberOfPortfoliosReached(): String = s"maxNumberOfPortfoliosReached ${config.maxNumberOfPortfolios}"

          override def portfolioAlreadyExist(portfolioName: PortfolioName): String = s"portfolioAlreadyExist ${portfolioName.name}"

          override def portfolioDoesNotExist(updatePortfolio: UpdatePortfolio): String = s"portfolioDoesNotExist ${updatePortfolio.portfolioName.name}"

          override def positionsToBeUpdatedDoesNotExist(value: List[PositionId]): String = s"positionsToBeUpdatedDoesNotExist ${value.mkString(" , ")}"

          override def tooManyPositionsForASinglePortfolio(portfolioName: PortfolioName, tooManyPositions: Int): String =
            s"tooManyPositionsForASinglePortfolioReached ${config.maxNumberOfPositions} $tooManyPositions"

        }
  }

  case class TestContext(supportMessages: SupportMessages, errorMessagesFactory: ErrorMessagesFactory)(implicit actorConfiguration: Config) {

    def createAPortfolio(numberOfPositions: Int, aPortfolioName: PortfolioName): CreatePortfolio =
      createAPortfolio(numberOfPositions, aPortfolioName, PortfoliosGen.idempotentKey.sample.get)

    def createAPortfolio(numberOfPositions: Int, aPortfolioName: PortfolioName, idempotentKey: IdempotentKey): CreatePortfolio = {
      require(numberOfPositions >= 1)
      CreatePortfolio(
        idempotentKey,
        aPortfolioName,
        NonEmptySeq(
          PortfoliosGen.position.sample.get,
          (1 until numberOfPositions).map(_ => PortfoliosGen.position.sample.get)
        )
      )
    }

    def uuidGenerator(numberOfUUIDsToBeGenerated: Int Refined Positive) = {
      (0 until numberOfUUIDsToBeGenerated.value).map(_ => UUID.randomUUID()).toVector
    }

    trait ConfigurationReaderTester {
      val configurationReader: UnsafeConfigReader[Config]
      def assertThatConfigurationHasBeenRead(): Boolean
    }

    def configReaderTester(configToBeReturned: Config): ConfigurationReaderTester = {
      val hasBeenCalled = new AtomicBoolean(false)
      new ConfigurationReaderTester {
        override val configurationReader: UnsafeConfigReader[Config] = akkaConfig => {
          hasBeenCalled.set(true)

          val configFromConfigFile: Config = unsafeConfigReader(akkaConfig)

          configFromConfigFile.maxNumberOfPortfolios should be(3)
          configFromConfigFile.maxNumberOfPositions should be(100)
          configFromConfigFile.garbageCollectorInterval should be(3600.seconds)

          configToBeReturned
        }

        override def assertThatConfigurationHasBeenRead(): Boolean = hasBeenCalled.get()
      }
    }

    val initialPersistenceId: PersistenceId = randomPersistenceId

    val errorMessages = ErrorMessagesContext.justForAnIdea.newErrorMessages(actorConfiguration)

    val aPortfolioName: PortfolioName = PortfoliosGen.portfolioName.sample.get

    def randomPersistenceId: PersistenceId = PersistenceId("portfolios", Random.nextLong().toString)

    val newPositionIdFactory = () => UUID.randomUUID()

    val constIdFactory: () => PositionId = {
      val someUUID: PositionId = UUID.randomUUID()
      () => someUUID
    }
    val terminationProbe: TestProbe[String] = testKit.createTestProbe[String]()

    val responseProbe: TestProbe[Response] = testKit.createTestProbe[Response]()

    def aPosition(): Position = {
      val positionType = PortfoliosGen.soldOrBoughtGen.sample.get
      Position(
        positionType,
        TradingPairGen.tradingPairs.sample.get,
        lotSizeGen.sample.get,
        dateGen.sample.get,
        dateGen.sample.get.plusDays(2)
      )
    }

    def calculateExpectedPosition(uuidOfTheExistingPosition: PositionId, position: Position): RecordedPosition = {
      RecordedPosition(
        id = uuidOfTheExistingPosition,
        Position(
          position.positionType,
          position.tradingPair,
          position.lotSize,
          position.transactionDateTime,
          position.recordedDateTime
        )
      )
    }

    def aMessageId(): MessageId = Gen.const(UUIDMessageId(UUID.randomUUID())).sample.get

    object behaviors {

      def portfolios(persistenceId: PersistenceId, idFactory: () => PositionId): Behavior[Command] =
        portfolios(persistenceId, idFactory, configReaderTester(configToBeReturned = actorConfiguration))

      def portfolios(
          persistenceId: PersistenceId,
          idFactory: () => PositionId,
          configurationReaderTester: ConfigurationReaderTester
      ): Behavior[Command] =
        DeliveryGuaranteedPortfolios(persistenceId)(
          idFactory,
          supportMessages,
          errorMessagesFactory,
          (akkaConfig: AkkaConfig) => configurationReaderTester.configurationReader(akkaConfig),
          DefaultHasher
        )

    }
    def positionIdFactory(uuidToBeGenerated: Vector[PositionId]): () => PositionId = {
      var numberOfIdsCreated = -1
      () => {
        numberOfIdsCreated += 1
        uuidToBeGenerated(numberOfIdsCreated)
      }
    }

    def testActorStateHasBeenPersisted(
        actorToBeTerminated: ActorRef[Command],
        howCreateTheNewActor: () => Behavior[Command],
        expectedResponseType: DeliveryGuaranteedPortfolios.ResponseType,
        responseProbe: TestProbe[Response]
    ): Response = {

      testKit.stop(actorToBeTerminated)

      testKit.createTestProbe[String]().expectTerminated(actorToBeTerminated)

      val recreatedActor = testKit.spawn(howCreateTheNewActor())

      val showPortfoliosCommand: ExternalCommand = ExternalCommand(aMessageId(), ShowPortfoliosPayload, responseProbe.ref)

      recreatedActor ! showPortfoliosCommand

      responseProbe.expectMessage(Response(showPortfoliosCommand.messageId, expectedResponseType))
    }
  }
}
