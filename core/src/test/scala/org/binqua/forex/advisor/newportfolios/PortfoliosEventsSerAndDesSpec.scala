package org.binqua.forex.advisor.newportfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, StateGen}
import org.binqua.forex.util.DocumentedSerializeAndDeserializeSpec
import org.scalatest.GivenWhenThen
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfoliosEventsSerAndDesSpec
    extends ScalaTestWithActorTestKit
    with DocumentedSerializeAndDeserializeSpec
    with AnyPropSpecLike
    with ScalaCheckPropertyChecks
    with GivenWhenThen {

  property(testName = "PortfolioCreatedEvent can be serialized and deserialized by default by akka") {
    forAll(PortfoliosGen.newPortfolioCreatedEvent) { portfolioCreatedEvent: events.PortfolioCreated =>
      assertThatCanBeSerialiseAndDeserialize(portfolioCreatedEvent)
    }
  }

  property(testName = "portfoliosSummary can be serialized and deserialized by default by akka") {
    forAll(StateGen.portfoliosSummary) { portfoliosSummary: PortfoliosSummary =>
      assertThatCanBeSerialiseAndDeserialize(portfoliosSummary)
    }
  }
}
