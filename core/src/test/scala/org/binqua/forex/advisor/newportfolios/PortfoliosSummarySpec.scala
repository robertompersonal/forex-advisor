package org.binqua.forex.advisor.newportfolios

import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{Portfolio, PortfolioName}
import org.binqua.forex.util.TestingFacilities
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers._
import org.scalatest.propspec.AnyPropSpecLike
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class PortfoliosSummarySpec extends AnyPropSpecLike with ScalaCheckPropertyChecks with GivenWhenThen with TestingFacilities {

  implicit def noShrink[Any]: Shrink[Any] = Shrink.shrinkAny

  property("PortfoliosSummary cannot contain duplicated name portfolio") {

    val portfolioWithDuplicatedNames: Gen[(List[PortfolioName], List[Portfolio])] = for {
      numberOfDuplicatedNames <- Gen.chooseNum(2, 5)
      nameToBeDuplicated <- PortfoliosGen.portfolioName
      identicalNames <- Gen.listOfN(numberOfDuplicatedNames, Gen.const(nameToBeDuplicated))
      numberOfNonDuplicatedNames <- Gen.chooseNum(0, 5)
      nonIdenticalNames <- Gen.listOfN(numberOfNonDuplicatedNames, PortfoliosGen.portfolioName)
      recordPositions <- Gen.listOfN((identicalNames ::: nonIdenticalNames).size, PortfoliosGen.recordedPositions)
    } yield {
      val portfolioNames: List[PortfolioName] = (identicalNames ::: nonIdenticalNames).sorted
      (portfolioNames, portfolioNames.zip(recordPositions).map(inp => Portfolio(inp._1, inp._2.toSeq.toSet)))
    }

    forAll(portfolioWithDuplicatedNames) { pair =>
      {
        val (portfolioNamesSorted, portfolios) = pair
        whenever(portfolios.nonEmpty) {

          PortfoliosSummary.internalValidate(portfolios.toSet).swap.getOrElse(thisTestShouldNotHaveArrivedHere) should be(portfolioNamesSorted)

          PortfoliosSummary.validate(portfolios.toSet).swap.getOrElse(thisTestShouldNotHaveArrivedHere) should be(
            PortfoliosSummary.toNames(portfolioNamesSorted)
          )
        }

        the[IllegalArgumentException] thrownBy PortfoliosSummary.unsafe(portfolios.toSet) should have message PortfoliosSummary.toNames(portfolioNamesSorted)

      }
    }
  }

  property("a portfoliosSummary can be empty") {
    PortfoliosSummary.validate(Set.empty).isRight should be(true)
  }

  property("toNames works") {
    PortfoliosSummary.toNames(List("aaa", "zzz", "bbb", "ttt").map(PortfolioName.unsafe)) should be(
      s"Portfolios cannot contain portfolios with duplicated names but this list of portfolio names aaa-zzz-bbb-ttt it does"
    )
  }

}
