package org.binqua.forex.advisor.portfolios

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.persistence.typed.PersistenceId
import org.binqua.forex.util.{AkkaTestingFacilities, TestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import java.util.UUID

class DefaultSupportMessagesSpec
    extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  val underTest: SupportMessages = DefaultSupportMessages

  val portfolioName = PortfoliosGen.portfolioName.sample.get

  "portfolioAlreadyExist" should "be implemented" in {
    underTest.portfolioAlreadyExist(portfolioName) should be(s"Portfolio with name ${portfolioName.name} already exist.")
  }

  "portfolioDoesNotExist" should "be implemented" in {
    underTest.portfolioDoesNotExist(portfolioName) should be(s"Portfolio with name ${portfolioName.name} does not exist")
  }

  "portfolioToBeDeletedDoesNotExist" should "be implemented" in {
    underTest.portfolioToBeDeletedDoesNotExist(portfolioName) should be(s"Portfolio to be deleted with name ${portfolioName.name} does not exist")
  }

  "positionToBeDeletedDoesNotExist" should "be implemented" in {
    val uuid = UUID.randomUUID()
    underTest.positionToBeDeletedDoesNotExist(toBeDeleted = uuid) should be(s"PositionId to be deleted ${uuid.toString} does not exist")
  }

  "positionToBeUpdatedDoesNotExist" should "be implemented" in {
    val uuid = UUID.randomUUID()
    underTest.positionToBeUpdatedDoesNotExist(wrongPositionId = uuid) should be(s"PositionId to be updated ${uuid.toString} does not exist")
  }

  "startingPortfolios" should "be implemented" in {
    val persistenceId = PersistenceId("Portfolios", "123")
    underTest.startingPortfolios(persistenceId) should be(s"Portfolio actors with persistenceId ${persistenceId.id} started")
  }

  "tooManyPositions" should "be implemented" in {
    val maxNumberOfPositions = 10
    underTest.tooManyPositions(maxNumberOfPositions = maxNumberOfPositions) should be(
      s"Sorry!!! Too many positions for one portfolios. You can create max $maxNumberOfPositions per portfolio"
    )
  }

  "tooManyPortfolios" should "be implemented" in {
    val maxNumberOfPortfoliosAllowed = 10
    underTest.maxNumberOfPortfoliosReached(maxNumberOfPortfoliosAllowed) should be(
      s"Sorry!!! Too many portfolios. You can create max $maxNumberOfPortfoliosAllowed portfolios"
    )
  }

}
