package org.binqua.forex.running.services.httpclient

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol.SubscribeTo
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class SupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  private val ref: ActorRef[SubscribeTo] = createTestProbe[SubscribeTo]("test").ref

  "Log text for serviceRegistered" should "be implemented" in {
    SupportMessages.serviceRegistered(ServicesKeys.HttpClientSubscriberServiceKey, ref) shouldBe s"Service with key HttpClientSubscriberService name ${ref.path.name} path ${ref.path} has been registered"
  }

  "Log text for serviceStopped" should "be implemented" in {
    SupportMessages.serviceStopped(ServicesKeys.HttpClientSubscriberServiceKey, ref) shouldBe s"Service with key HttpClientSubscriberService name ${ref.path.name} path ${ref.path} has been stopped"
  }

}
