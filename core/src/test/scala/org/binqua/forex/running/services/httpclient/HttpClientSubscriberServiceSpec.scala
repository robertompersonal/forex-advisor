package org.binqua.forex.running.services.httpclient

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.feed.httpclient.HttpClientSubscriberProtocol
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.{SocketIOClientProtocol, SocketId}
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.httpclient.HttpClientSubscriberServiceProtocol.SubscribeTo
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class HttpClientSubscriberServiceSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "When the service is registered, the actor" should "log it" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

    val name = randomString

    val underTest =
      matchDistinctLogs(messages = s"serviceRegistered HttpClientSubscriberService $name", s"serviceDeregistered HttpClientSubscriberService $name").expect {

        val underTest = spawn(HttpClientSubscriberService(theHttpSubscriberProbe.ref, justForAnIdeaSupportMessages), name)

        waitALittleBit(soThat("the service can be registered"))

        testKit.stop(underTest)

        underTest

      }

  }

  "Given the service has been published, it" should "be visible in an other actor subscribed to it" in new TestContext(
    SupportMessagesTestContext.justForAnIdeaSupportMessages
  ) {

    val serviceUnderTest =
      spawn(HttpClientSubscriberService(theHttpSubscriberServiceRef = theHttpSubscriberProbe.ref, supportMessages = justForAnIdeaSupportMessages))

    val actorUsingTheService = spawn(ActorWhoWillSubscribeToTheService(ServicesKeys.HttpClientSubscriberServiceKey))

    waitALittleBit(soThat("the service can be retrieved"))

    private val messageForTheService = HttpClientSubscriberServiceProtocol.SubscribeTo(CurrencyPair.EurUsd, SocketId("1"), null)

    actorUsingTheService ! ActorWhoWillSubscribeToTheService.EnvelopeToWrapAServiceMessage(messageForTheService)

    theHttpSubscriberProbe.expectMessage(
      HttpClientSubscriberProtocol.SubscribeTo(messageForTheService.currencyPair, messageForTheService.socketId, messageForTheService.replyTo)
    )

    testKit.stop(serviceUnderTest)

    testKit.stop(actorUsingTheService)

  }

  "The actor" should "proxy SubscribeTo to the httpClientSubscriberClient" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

    val underTest: ActorRef[HttpClientSubscriberServiceProtocol.Message] =
      spawn(HttpClientSubscriberService(theHttpSubscriberProbe.ref, justForAnIdeaSupportMessages))

    underTest ! sampleOfSubscribeToMessage

    waitALittleBit(soThat("the messages can travel"))

    theHttpSubscriberProbe.expectMessage(
      HttpClientSubscriberProtocol.SubscribeTo(sampleOfSubscribeToMessage.currencyPair, sampleOfSubscribeToMessage.socketId, sampleOfSubscribeToMessage.replyTo)
    )

    testKit.stop(underTest)

  }

  "only one actor service" should "be allowed otherwise actor registered to the service will fail" in new TestContext(
    SupportMessagesTestContext.justForAnIdeaSupportMessages
  ) {

    val service = spawn(HttpClientSubscriberService(theHttpSubscriberProbe.ref, justForAnIdeaSupportMessages), randomString)

    val sameService = spawn(HttpClientSubscriberService(theHttpSubscriberProbe.ref, justForAnIdeaSupportMessages), randomString)

    LoggingTestKit
      .error("too many services")
      .expect(
        spawn(ActorWhoWillSubscribeToTheService(ServicesKeys.HttpClientSubscriberServiceKey))
      )

    testKit.stop(service)

    testKit.stop(sameService)

  }

  object SupportMessagesTestContext {
    val justForAnIdeaSupportMessages: SupportMessages = new SupportMessages {
      override def serviceRegistered(key: ServiceKey[_], self: ActorRef[SubscribeTo]): String = s"serviceRegistered ${key.id} ${self.path.name}"

      override def serviceStopped(key: ServiceKey[_], self: ActorRef[SubscribeTo]): String = s"serviceDeregistered ${key.id} ${self.path.name}"
    }
  }

  case class TestContext(justForAnIdeaSupportMessages: SupportMessages)(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(underTestNaming) {

    val theHttpSubscriberProbe: TestProbe[HttpClientSubscriberProtocol.Command] = createTestProbe[HttpClientSubscriberProtocol.Command]("httpSubscriber")

    val internalFeedProbe: TestProbe[SocketIOClientProtocol.FeedContent] = createTestProbe[SocketIOClientProtocol.FeedContent]("internalFeed")

    val sampleOfSubscribeToMessage =
      SubscribeTo(CurrencyPair.EurUsd, SocketId("1"), createTestProbe[HttpClientSubscriberProtocol.Response]("httpClientSubscriberServiceClient").ref)

  }

}
