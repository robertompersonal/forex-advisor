package org.binqua.forex.running.services.internalfeed

import akka.actor.testkit.typed.scaladsl.{LoggingTestKit, ScalaTestWithActorTestKit}
import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.ServiceKey
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.running.services.internalfeed.InternalFeedService.{WrappedReceptionistResponse, WrappedSocketIOClientFeedContent}
import org.binqua.forex.util.AkkaUtil.TheOnlyHandledMessages
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class InternalFeedServiceSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "The actor" should "register the service" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

    val adapterActorName = "$$a-adapter"

    matchDistinctLogs(messages =
      s"serviceRegistered ${ServicesKeys.InternalFeedServiceKey.id} $adapterActorName",
      contentForTheService1,
      contentForTheService2
    ).expect {

      val underTest = spawn(InternalFeedService(justForAnIdeaSupportMessages))

      val IShouldKnowTheService = spawn(ActorWhoWillSubscribeToTheService(ServicesKeys.InternalFeedServiceKey))

      waitALittleBit(soThat("the service can be registered"))

      IShouldKnowTheService ! ActorWhoWillSubscribeToTheService.EnvelopeToWrapAServiceMessage(SocketIOClientProtocol.FeedContent(contentForTheService1))
      IShouldKnowTheService ! ActorWhoWillSubscribeToTheService.EnvelopeToWrapAServiceMessage(SocketIOClientProtocol.FeedContent(contentForTheService2))

      waitALittleBit(soThat("The messages can be delivered"))

      testKit.stop(underTest)

      testKit.stop(IShouldKnowTheService)

    }
  }

  "Once spawned, the actor" should "treat as unhandled extra WrappedReceptionistResponse" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

    val underTest = spawn(InternalFeedService(justForAnIdeaSupportMessages))

    waitALittleBit(soThat("the service can be registered"))

    List(
      WrappedReceptionistResponse(null),
    ).foreach(assertIsUnhandled(underTest, _)(TheOnlyHandledMessages(WrappedSocketIOClientFeedContent)))

  }

  "only one actor service" should "be allowed otherwise actor registered to the service will fail" in new TestContext(SupportMessagesTestContext.justForAnIdeaSupportMessages) {

    val service = spawn(InternalFeedService(justForAnIdeaSupportMessages), randomString)

    val sameService = spawn(InternalFeedService(justForAnIdeaSupportMessages), randomString)

    LoggingTestKit.error("too many services").expect(
      spawn(ActorWhoWillSubscribeToTheService(ServicesKeys.InternalFeedServiceKey))
    )

    testKit.stop(service)

    testKit.stop(sameService)

  }

  object SupportMessagesTestContext {
    val justForAnIdeaSupportMessages: SupportMessages = new SupportMessages {

      override def serviceRegistered(key: ServiceKey[_], value: ActorRef[SocketIOClientProtocol.FeedContent]): String = s"serviceRegistered ${key.id} ${value.path.name}"

      override def serviceStopped(serviceKey: ServiceKey[SocketIOClientProtocol.FeedContent], self: ActorRef[InternalFeedService.PrivateMessage]): String = s"serviceRegistered ${serviceKey.id} ${self.path.name}"
    }
  }

  case class TestContext(justForAnIdeaSupportMessages: SupportMessages)(implicit underTestNaming: UnderTestNaming) extends BaseTestContext(underTestNaming) {
    val contentForTheService1 = "great"
    val contentForTheService2 = "even better"
  }

}
