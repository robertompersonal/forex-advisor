package org.binqua.forex.running.services.internalfeed

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketIOClientProtocol
import org.binqua.forex.running.common.ServicesKeys
import org.binqua.forex.util.{AkkaTestingFacilities, Validation}
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

class SupportMessagesSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen with AkkaTestingFacilities with Validation {

  "Log text for serviceRegistered" should "be implemented" in {
    val ref: ActorRef[SocketIOClientProtocol.FeedContent] = createTestProbe[SocketIOClientProtocol.FeedContent]().ref
    SupportMessages.serviceRegistered(ServicesKeys.InternalFeedServiceKey, ref) shouldBe s"Service with key InternalFeedService name ${ref.path.name} path ${ref.path} has been registered"
  }

  "Log text for serviceStopped" should "be implemented" in {
    val ref: ActorRef[InternalFeedService.PrivateMessage] = createTestProbe[InternalFeedService.PrivateMessage]().ref

    SupportMessages.serviceStopped(ServicesKeys.InternalFeedServiceKey, ref) shouldBe s"Service with key InternalFeedService name ${ref.path.name} path ${ref.path} has been stopped"
  }

}
