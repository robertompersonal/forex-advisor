package org.binqua.forex.running.subscriber

import com.typesafe.config.ConfigFactory
import org.binqua.forex.util.TestingFacilities
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers.{not, _}
import org.scalatest.wordspec.AnyWordSpec

class CommonIOConfigBuilderSpec extends AnyWordSpec with BeforeAndAfterEach with TestingFacilities {

  "CommonIOConfigBuilder" should {

    "create right configuration for a valid file" in {

      val actualValue = new CommonIOConfigBuilder {}
        .buildConfiguration(Array("-configFileName=./src/test/resources/application-test.conf"))

      actualValue shouldBe Right(ConfigFactory.load("application-test"))

    }

    "clear config library cache between configuration reading" in {
      for {
        config1 <- new CommonIOConfigBuilder {}
          .buildConfiguration(Array("-configFileName=./src/test/resources/application-test.conf"))
        config2 <- new CommonIOConfigBuilder {}
          .buildConfiguration(Array("-configFileName=./src/test/resources/a-different-application-test.conf"))
      } yield config1 should (not be config2)

    }

  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      System.clearProperty("config.file")
    }
  }

}
