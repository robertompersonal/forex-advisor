package org.binqua.forex.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration._

class TimeCalculatorSpec extends AnyFlatSpec {

  "TimeCalculator" should "calculate right time" in {
    {
      val calculator = AkkaUtil.TimeCalculator(timeInterval = 200.millis)
      calculator.incrementJustBeforeTimeout should be(199.millis)
      calculator.incrementJustAfterTimeout should be(1.millis + 1.millis)
      calculator.incrementJustBeforeTimeout should be(198.millis)
      calculator.incrementJustAfterTimeout should be(1.millis + 1.millis)
      calculator.incrementJustBeforeTimeout should be(198.millis)
    }
    {
      val calculator = AkkaUtil.TimeCalculator(timeInterval = 2000.millis)
      calculator.incrementJustBeforeTimeout should be(1999.millis)
      calculator.incrementJustAfterTimeout should be(1.millis + 1.millis)
      calculator.incrementJustBeforeTimeout should be(1998.millis)
      calculator.incrementJustAfterTimeout should be(1.millis + 1.millis)
      calculator.incrementJustBeforeTimeout should be(1998.millis)
    }
  }

  "using only justBeforeFirstInterval and justAfterFirstInterval TimePasses" should "calculate right increment " in {
    val timePasses = AkkaUtil.TimePassesIncrement(timeInterval = 200.millis)
    timePasses.justBeforeFirstInterval should be(199.millis)
    timePasses.justAfterFirstInterval should be(1.millis + 1.millis)
  }

  "using only justBeforeFirstInterval, justAfterFirstInterval, justBeforeSecondInterval and justAfterSecondInterval, TimePasses" should "calculate right increment " in {
    val increment = AkkaUtil.TimePassesIncrement(timeInterval = 200.millis)
    increment.justBeforeFirstInterval should be(199.millis)
    increment.justAfterFirstInterval should be(1.millis + 1.millis)
    increment.justBeforeSecondInterval should be(198.millis)
    increment.justAfterSecondInterval should be(2.millis)
  }

  "using all interval until the third, TimePasses" should "calculate right increment " in {
    val timePasses = AkkaUtil.TimePassesIncrement(timeInterval = 200.millis)

    timePasses.justBeforeFirstInterval should be(199.millis)
    timePasses.justBeforeFirstInterval should be(199.millis)
    timePasses.justBeforeFirstInterval should be(199.millis)

    timePasses.justAfterFirstInterval should be(1.millis + 1.millis)
    timePasses.justAfterFirstInterval should be(1.millis + 1.millis)
    timePasses.justAfterFirstInterval should be(1.millis + 1.millis)

    timePasses.justBeforeSecondInterval should be(198.millis)
    timePasses.justBeforeSecondInterval should be(198.millis)
    timePasses.justBeforeSecondInterval should be(198.millis)

    timePasses.justAfterSecondInterval should be(2.millis)
    timePasses.justAfterSecondInterval should be(2.millis)
    timePasses.justAfterSecondInterval should be(2.millis)

    timePasses.justBeforeThirdInterval should be(198.millis)
    timePasses.justBeforeThirdInterval should be(198.millis)
    timePasses.justBeforeThirdInterval should be(198.millis)

    timePasses.justAfterThirdInterval should be(2.millis)
    timePasses.justAfterThirdInterval should be(2.millis)
    timePasses.justAfterThirdInterval should be(2.millis)
  }


}
