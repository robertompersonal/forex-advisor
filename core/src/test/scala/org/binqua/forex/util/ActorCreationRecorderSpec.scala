package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.Behaviors
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class ActorCreationRecorderSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with Matchers {

  "Given key name-<number> MyMap" should "allow to record and retrieve number of entries and actor ref" in {

    val lastC1: ActorRef[C1] = spawn[C1](Behaviors.empty, "namePrefix1-1")

    val lastC2: ActorRef[C2] = spawn[C2](Behaviors.empty, "namePrefix2-2")

    val lastC3: ActorRef[C3] = spawn[C3](Behaviors.empty, "namePrefix3-3")

    val actualResult = ActorCreationRecorder.empty
      .record(lastC1)
      .record(spawn[C2](Behaviors.empty, "namePrefix2-1").ref)
      .record(spawn[C3](Behaviors.empty, "namePrefix3-1").ref)
      .record(lastC2)
      .record(spawn[C3](Behaviors.empty, "namePrefix3-2").ref)
      .record(lastC3)

    actualResult.numberOfEntries("namePrefix1") shouldBe 1
    actualResult.numberOfEntries("namePrefix2") shouldBe 2
    actualResult.numberOfEntries("namePrefix3") shouldBe 3

    actualResult.lastActor("namePrefix1") shouldBe Some(lastC1)
    actualResult.lastActor("namePrefix2") shouldBe Some(lastC2)
    actualResult.lastActor("namePrefix3") shouldBe Some(lastC3)

  }

  "ActorCreationRecorder" should "allow to record and increment by 1 to the same key" in {

    val b1: ActorRef[C1] = spawn[C1](Behaviors.empty, "b-1")
    val b2: ActorRef[C1] = spawn[C1](Behaviors.empty, "b-2")
    val lastB: ActorRef[C1] = spawn[C1](Behaviors.empty, "b-3")

    ActorCreationRecorder.empty.record(b1).record(b2).record(lastB).numberOfEntries("b") shouldBe 3

  }

  "name starting with -" should "not be allowed" in {
    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "-1"))
    } should have message "name format should be name-<number> -1 is not valid"

  }
  "name without -" should "not be allowed" in {

    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "a1"))
    } should have message "name format should be name-<number> a1 is not valid"

    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "1"))
    } should have message "name format should be name-<number> 1 is not valid"

  }

  "name with 2, 3 etc -" should "not be allowed" in {

    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "a1-1-2"))
    } should have message "name format should be name-<number> a1-1-2 is not valid"

    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "a1-1-2-3"))
    } should have message "name format should be name-<number> a1-1-2-3 is not valid"

  }

  "increment" should "be by 1" in {
    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "a-1")).record(spawn[C1](Behaviors.empty, "a-3"))
    } should have message "requirement failed: new counter value is 3 but should be 2 because the oldest is 1"

  }

  "first counter entry" should "be 1" in {
    the[IllegalArgumentException] thrownBy {
      ActorCreationRecorder.empty.record(spawn[C1](Behaviors.empty, "a-2"))
    } should have message "requirement failed: first added counter should be 1 not 2"

  }

  trait C1

  trait C2

  trait C3

}
