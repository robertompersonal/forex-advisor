package org.binqua.forex.util

import akka.actor.UnhandledMessage
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, LoggingTestKit, ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.testkit.typed.{FishingOutcome, LoggingEvent}
import akka.actor.typed.eventstream.EventStream
import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors.monitor
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import org.binqua.forex.util.AkkaUtil.{TheOnlyHandledMessages, commandIgnoredMessage}
import org.binqua.forex.util.ChildMaker.CHILD_MAKER_FACTORY
import org.binqua.forex.util.NewChildMaker.{ChildNamePrefix, ChildrenFactory}
import org.binqua.forex.util.core.UnsafeConfigReader
import org.scalacheck.Gen
import org.scalatest.Assertions
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Span}

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.{AtomicInteger, AtomicReference}
import scala.collection.immutable
import scala.concurrent.duration.{FiniteDuration, _}
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

trait AkkaTestingFacilities extends Eventually with Assertions {

  this: ScalaTestWithActorTestKit =>

  def throwIAmGoingToFailedException = throw new RuntimeException("I am going to fail")

  def throwIAmGoingToFailedException[T](whoAmI: String, any: T) = throw new RuntimeException(s"I am $whoAmI and I am going to fail due to a dummy message $any")

  implicit val underTestNaming = new IncrementalUnderTestNaming(new AtomicInteger(1))

  object extraFishingOutcome {
    def failBecauseOnlyAcceptedMessageIs(onlyAccpetedMessage: Any) = FishingOutcome.Fail(s"Only $onlyAccpetedMessage is accepted here")
  }

  trait Watcher {
    val actorsWatchers = SmartDeadWatcher(testKit)
  }

  val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

  testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

  def expectUnhandledMessageFor(msg: Any): immutable.Seq[UnhandledMessage] = expectUnhandledMessageFor(msg, 200.millis)

  def expectUnhandledMessageFor(msg: Any, max: FiniteDuration): immutable.Seq[UnhandledMessage] = {
    unhandledMessageSubscriber.fishForMessage(max, "a message should be unhandled ... it means match UnhandledMessage(msg, _, _) but I did not fish messages")({
      case UnhandledMessage(msg, _, _) => FishingOutcome.Complete
      case m                           => FishingOutcome.Fail(s"I was fishing for unhandled message but I found $m")
    })
  }

  object DeadWatcher {
    def apply(testKit: ActorTestKit) = new DeadWatcher(testKit)

    def apply[T](testKit: ActorTestKit, actorRef: ActorRef[T]): DeadWatcher = {
      require(actorRef != null)
      val deadWatcher = new DeadWatcher(testKit)
      deadWatcher.watch(actorRef)
      deadWatcher
    }

    def spawn[T](testKit: ActorTestKit): Behavior[T] => (DeadWatcher, ActorRef[T]) =
      (behavior: Behavior[T]) => {
        val actorRef = testKit.spawn(behavior)
        val deadWatcher = new DeadWatcher(testKit)
        deadWatcher.watch(actorRef)
        (deadWatcher, actorRef)
      }

    def spawn[T](behavior: Behavior[T], testKit: ActorTestKit): (DeadWatcher, ActorRef[T]) = {
      val actorRef = testKit.spawn(behavior)
      val deadWatcher = new DeadWatcher(testKit)
      deadWatcher.watch(actorRef)
      (deadWatcher, actorRef)
    }
  }

  class DeadWatcher(testKit: ActorTestKit) {
    private val threadSafeTerminatedActorNames = new CopyOnWriteArrayList[String]

    private val watcher = testKit.spawn(testActor(threadSafeTerminatedActorNames))

    private def prefix(t: List[String]): String = s"\nAll registered terminated actor name are :\n${t.mkString(" , ")}\n"

    def assertThatIsStillAlive[T](name: String, finiteDuration: FiniteDuration): Unit =
      eventually(timeout(Span(finiteDuration.toMillis, Millis)))(
        internalAssertThatIsStillAlive(
          name,
          terminatedActorNames =>
            assertResult(
              false,
              s"${prefix(terminatedActorNames)}.This list should not contains actor $name but it does. It means maybe actor is dead and it should not!!"
            )(terminatedActorNames.contains(name))
        )
      )

    def assertThatIsStillAlive[T](actorRef: ActorRef[T]): Unit = assertThatIsStillAlive(actorRef, 500.millis)

    def assertThatIsStillAlive[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit = assertThatIsStillAlive(actorRef.path.name, finiteDuration)

    def assertThatIsAliveAfter[T](finiteDuration: FiniteDuration, actorRef: ActorRef[T]): Unit = {
      Thread.sleep(finiteDuration.toMillis)
      assertThatIsStillAlive(actorRef.path.name, 0.millis)
    }

    private def internalAssertThatIsStillAlive[T](name: String, f: List[String] => Unit): Unit = {
      import scala.jdk.CollectionConverters._
      f(threadSafeTerminatedActorNames.listIterator().asScala.toList)
    }

    def assertThatIsDead[T](name: String, finiteDuration: FiniteDuration): Unit =
      eventually(timeout(Span(finiteDuration.toMillis, Millis)))(
        internalAssertThatIsStillAlive(
          name,
          t =>
            assertResult(true, s"${prefix(t)}should contains actor $name but it does not. It means maybe actor is still alive and it should not!!")(
              t.contains(name)
            )
        )
      )

    def assertThatIsDead[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit = assertThatIsDead(actorRef.path.name, finiteDuration)

    def watch[T](actorRef: ActorRef[T]): ActorRef[T] = {
      watcher ! WatchActor(actorRef)
      actorRef
    }

    trait Command

    case class WatchActor[U](actor: ActorRef[U]) extends Command

    private case object Stop extends Command

    def testActor(terminatedActorNames: CopyOnWriteArrayList[String]): Behavior[Command] =
      Behaviors
        .receive[Command] { (context, msg) =>
          msg match {
            case WatchActor(ref) =>
              require(ref != null, "actor ref to watch cannot be null")
              context.watch(ref)
              Behaviors.same
            case Stop =>
              Behaviors.stopped
            case _ =>
              Behaviors.ignore
          }
        }
        .receiveSignal {
          case (_, Terminated(ref)) =>
            terminatedActorNames.add(ref.path.name)
            Behaviors.same
        }
  }

  def matchOnly(message: String): Function[LoggingEvent, Boolean] =
    event =>
      event.message match {
        case `message` => true
      }

  def matchOnlyMessage(message: String) = LoggingTestKit.custom(matchOnly(message)).withOccurrences(1)

  def matchOnlyMessages(messages: Seq[String]): Function[LoggingEvent, Boolean] = event => messages.contains(event.message)

  def matchDistinctLogs(messages: String*) = LoggingTestKit.custom(matchOnlyMessages(messages)).withOccurrences(messages.size)

  def matchLogsWithRepetition(messages: String*) = LoggingTestKit.custom(matchOnlyMessages(messages))

  object SmartDeadWatcher {
    def apply(testKit: ActorTestKit) = new SmartDeadWatcher(testKit)

    def apply[T](testKit: ActorTestKit, actorRef: ActorRef[T]) = {
      require(actorRef != null)
      val deadWatcher = new SmartDeadWatcher(testKit)
      deadWatcher.recordAndWatch(actorRef)
      deadWatcher
    }
  }

  type TEST_CONTEXT_MAKER[TO] = SmartDeadWatcher => TO

  class SmartDeadWatcher(testKit: ActorTestKit) {
    private val threadSafeTerminations = new CopyOnWriteArrayList[String]
    private val invocationsCounterMap = new AtomicReference(ActorCreationRecorder.empty)

    private val watcher = testKit.spawn(testActor(threadSafeTerminations, invocationsCounterMap))

    private def prefix(t: List[String]): String = s"\nRegistered terminated actor name are :\n${t.mkString(" , ")}\n"

    def assertThatIsStillAlive[T](name: String, finiteDuration: FiniteDuration): Unit =
      eventually(timeout(Span(finiteDuration.toMillis, Millis)))(
        internalAssertThatIsStillAlive(
          name,
          t =>
            assertResult(false, s"${prefix(t)}should not contains actor $name but it does. It means maybe actor is dead and it should not!!")(t.contains(name))
        )
      )

    def assertThatIsStillAlive[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit = assertThatIsStillAlive(actorRef.path.name, finiteDuration)

    def assertThatIsStillAlive[T](actorRef: ActorRef[T]): Unit = assertThatIsStillAlive(actorRef.path.name, 500.millis)

    def assertThatIsAliveAfter[T](finiteDuration: FiniteDuration, actorRef: ActorRef[T]): Unit = {
      Thread.sleep(finiteDuration.toMillis)
      assertThatIsStillAlive(actorRef.path.name, 0.millis)
    }

    private def internalAssertThatIsStillAlive[T](name: String, f: List[String] => Unit): Unit = {
      import scala.jdk.CollectionConverters._
      f(threadSafeTerminations.listIterator().asScala.toList)
    }

    def assertThatIsDead[T](name: String, finiteDuration: FiniteDuration): Unit =
      eventually(timeout(Span(finiteDuration.toMillis, Millis)))(
        internalAssertThatIsStillAlive(
          name,
          t =>
            assertResult(true, s"${prefix(t)}should contains actor $name but it does not. It means maybe actor is still alive and it should not!!")(
              t.contains(name)
            )
        )
      )

    def assertThatIsDead[T](actorRef: ActorRef[T], finiteDuration: FiniteDuration): Unit = assertThatIsDead(actorRef.path.name, finiteDuration)

    def recordAndWatch[T](actorRef: ActorRef[T]): ActorRef[T] = {
      watcher ! WatchActor(actorRef)
      actorRef
    }

    def assertNumberOfActorMade(actorPrefix: String)(asserter: Int => Unit): Unit = asserter(invocationsCounterMap.get().numberOfEntries(actorPrefix))

    def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => Unit): because => Unit =
      _ => asserter(invocationsCounterMap.get().numberOfEntries(actorPrefix))

    def lastCreatedInstanceOf[T](name: String): ActorRef[T] = invocationsCounterMap.get().lastActor(name).get.unsafeUpcast[T]

    trait Command

    case class WatchActor[U](actor: ActorRef[U]) extends Command

    private case object Stop extends Command

    def testActor(terminations: CopyOnWriteArrayList[String], invocations: AtomicReference[ActorCreationRecorder]): Behavior[Command] =
      Behaviors.setup[Command] { context =>
        Behaviors
          .receive[Command] { (context, msg) =>
            msg match {
              case WatchActor(ref) =>
                require(ref != null, "actor ref to watch cannot be null")
                context.log.debug(s"WatchActor in AkkaTestingFacilities received message $msg")
                invocations.set(invocations.get().record(ref))
                context.watch(ref)
                Behaviors.same
              case Stop =>
                Behaviors.stopped
              case _ =>
                Behaviors.same
            }
          }
          .receiveSignal {
            case (_, t: Terminated) =>
              terminations.add(t.ref.path.name)
              Behaviors.same
          }

      }

    def createAChildMaker[FROM, TO: ClassTag](
        namePrefix: String,
        probeRef: ActorRef[TO],
        childBehavior: Behavior[TO]
    ): () => ChildMaker.CHILD_MAKER[FROM, TO] =
      () => {
        var instanceCounter = 0
        context: ActorContext[FROM] => {
          instanceCounter += 1
          val ref = context.spawn[TO](monitor[TO](probeRef, childBehavior), name = s"$namePrefix-$instanceCounter")
          context.log.info(s"created child ${ref.path.name}")
          this.recordAndWatch(ref)
        }
      }

    def createAChildMakerX[FROM, TO: ClassTag](
        f: Int => String,
        probeRef: ActorRef[TO],
        childBehavior: Behavior[TO]
    ): () => ChildMaker.CHILD_MAKER[FROM, TO] =
      () => {
        var instanceCounter = 0
        context: ActorContext[FROM] => {
          instanceCounter += 1
          val ref = context.spawn[TO](monitor[TO](probeRef, childBehavior), name = f(instanceCounter))
          context.log.info(s"created child ${ref.path.name}")
          this.recordAndWatch(ref)
        }
      }

  }

  val childMakerNaming: String => Int => String = prefix => index => s"$prefix-$index"

  case class because(reason: String)

  case class soThat(reason: String)

  def waitFor(finiteDuration: FiniteDuration, soThat: soThat): Unit = Thread.sleep(finiteDuration.toMillis)

  def waitALittleBit(soThat: soThat): Unit = waitFor(100.millis, soThat = soThat)

  def fishOnlyOneMessage[M](probe: TestProbe[M])(f: Any => Unit): Seq[M] = fishExactlyOneMessageAndFailOnOthers(probe, 1.seconds)(f)

  def fishExactlyOneMessageAndFailOnOthers[M](probe: TestProbe[M], max: FiniteDuration)(f: Any => Unit): Seq[M] =
    probe.fishForMessage(max) { p =>
      Try(f(p)) match {
        case Success(_) =>
          FishingOutcome.Complete
        case Failure(e) =>
          FishingOutcome.Fail(e.toString)
      }
    }

  def fishOneMessageAndIgnoreOthers[M](probe: TestProbe[M], max: FiniteDuration)(f: Any => Unit): Seq[M] =
    probe.fishForMessage(max) { p =>
      Try(f(p)) match {
        case Success(m) =>
          FishingOutcome.Complete
        case Failure(m) =>
          FishingOutcome.Continue
      }
    }

  def fishExactlyOneMessageAndIgnoreOthers[M](probe: TestProbe[M])(f: Any => Unit): Seq[M] = fishOneMessageAndIgnoreOthers(probe, 500.millis)(f)

  trait ActorCollaboratorTestContext[FROM, TO] {

    val name: String

    val probe: TestProbe[TO]

    def behavior: Behavior[TO]

    def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[FROM, TO]

    def ref: ActorRef[TO]

    def lastCreatedChild(): ActorRef[TO] = ???

  }

  abstract class NewActorCollaboratorTestContext[FROM, TO](smartDeadWatcher: SmartDeadWatcher) {

    val name: String

    def childNamePrefix: ChildNamePrefix = ???

    val probe: TestProbe[TO]

    def behavior: Behavior[TO]

    def childMakerFactory(actorsWatchers: SmartDeadWatcher): CHILD_MAKER_FACTORY[FROM, TO]

    def childrenFactoryContext: ChildrenFactory[FROM, TO] = ???

    def childMakerFactory(): CHILD_MAKER_FACTORY[FROM, TO] = ???

    def ref: ActorRef[TO]

    def lastCreatedChild(): ActorRef[TO] = eventually(smartDeadWatcher.lastCreatedInstanceOf[TO](name))

    private def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => Unit): because => Unit =
      eventually(smartDeadWatcher.assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(asserter))

    def assertChildrenSpawnUntilNowAre(expNumber: Int): Unit = assertNumberOfActorMadeWithNamePrefixedBy(name)(_ shouldBe expNumber)(because(""))

    def childNameByIndex(childIndex: Int): String = childMakerNaming(name)(childIndex)

  }

  class IncrementalUnderTestNaming(instanceUnderCounter: AtomicInteger) extends UnderTestNaming {
    override def next(): String = s"underTest-${instanceUnderCounter.getAndIncrement()}-$randomString"

    private def randomString: String = {
      val max = 10
      val temp = Gen.alphaUpperStr.sample.get
      if (temp.length < max)
        temp
      else
        temp.substring(0, max - 1)
    }
  }

  trait UnderTestNaming {

    def next(): String

  }

  abstract class ManualTimeBaseTestContext(underTestNaming: UnderTestNaming) {

    val deadWatcher = DeadWatcher(testKit)

    val actorsWatchers = SmartDeadWatcher(testKit)

    val manualTime: ManualTime = ManualTime()

    val manualClockBuilder: ManualClockBuilder = ManualClockBuilder(ManualTime())

    val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

    testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

    def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => Unit): because => Unit =
      eventually(actorsWatchers.assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(asserter))

    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages) = {
      LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
        underTest ! unhandled
      }
    }

  }

  object BaseTestContext {
    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages) = {
      LoggingTestKit.info(commandIgnoredMessage(unhandled)).expect {
        underTest ! unhandled
      }
    }
  }

  abstract class BaseTestContext(underTestNaming: UnderTestNaming) {

    val deadWatcher = DeadWatcher(testKit)

    val actorsWatchers = SmartDeadWatcher(testKit)

    val unhandledMessageSubscriber = createTestProbe[UnhandledMessage]()

    testKit.system.eventStream ! EventStream.Subscribe[UnhandledMessage](unhandledMessageSubscriber.ref)

    def assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix: String)(asserter: Int => Unit): because => Unit =
      eventually(actorsWatchers.assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(asserter))

    def assertChildSpawnAreExactly(actorPrefix: String, expNumber: Int, because: because): Unit =
      assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(_ shouldBe expNumber)(because)

    def assertChildSpawnAreExactly(actorPrefix: String, expNumber: Int): Unit =
      assertNumberOfActorMadeWithNamePrefixedBy(actorPrefix)(_ shouldBe expNumber)(because(""))

    def assertIsUnhandled[T](underTest: ActorRef[T], unhandled: T)(implicit theOnlyHandledMessages: TheOnlyHandledMessages) =
      BaseTestContext.assertIsUnhandled(underTest, unhandled)

  }

  def randomString: String = {
    val max = 15
    val temp = Gen.alphaUpperStr.sample.get
    if (temp.length <= max && temp.length >= 6)
      temp
    else
      randomString
  }

  object ActorWhoWillSubscribeToTheService {

    val testProbe = createTestProbe[String]()

    val testSuccessful = "test successful"

    val testFailed = "test failed"

    sealed trait Message

    case class WrappedReceptionistResponse(listings: Receptionist.Listing) extends Message

    case class EnvelopeToWrapAServiceMessage[T](messageToTheService: T) extends Message

    def apply[T](serviceKey: ServiceKey[T]): Behavior[Message] =
      Behaviors.setup[Message](context => {

        def setUp(thisAsReceptionistResponseReceiver: ActorRef[Receptionist.Listing]): Behavior[Message] = {

          context.system.receptionist ! Receptionist.Subscribe(serviceKey, thisAsReceptionistResponseReceiver)

          def runningBehavior(mayBeAServiceReference: Option[ActorRef[T]]): Behavior[Message] = {
            Behaviors.receiveMessage[Message] {
              case WrappedReceptionistResponse(serviceKey.Listing(setOfActorReferences: Set[ActorRef[T]])) =>
                setOfActorReferences.toList match {
                  case Nil =>
                    runningBehavior(None)
                  case head :: Nil =>
                    import cats.syntax.option._
                    runningBehavior(head.some)
                  case list => throw new IllegalStateException(s"too many services $list")
                }
              case e: EnvelopeToWrapAServiceMessage[_] =>
                mayBeAServiceReference.foreach(_ ! e.messageToTheService.asInstanceOf[T])
                Behaviors.same
            }
          }

          runningBehavior(None)

        }

        setUp(context.messageAdapter(WrappedReceptionistResponse))

      })
  }

  trait ConfigurationReaderTester[T] {
    val configurationReader: UnsafeConfigReader[T]

    def assertThatConfigurationHasBeenRead(): Boolean
  }

}
