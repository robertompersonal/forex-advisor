package org.binqua.forex.util

import akka.actor.typed.ActorRef

import scala.util.{Failure, Success, Try}

object ActorCreationRecorder {
  def empty: ActorCreationRecorder = new ActorCreationRecorder(Map())
}

case class Value(ref: ActorRef[Nothing], numberOfEntries: Int)

class ActorCreationRecorder private(private var invocationsCounterMap: Map[String, Value]) {

  def lastActor[Any](namePrefix: String): Option[ActorRef[Nothing]] = invocationsCounterMap.get(namePrefix).map(_.ref)

  def numberOfEntries(namePrefix: String): Int = invocationsCounterMap.get(namePrefix).map(_.numberOfEntries).getOrElse(0)

  private def add[T](name: String, ref: ActorRef[T]): ActorCreationRecorder = {
    val (namePrefix, incomingCounter) = extractValues(name)
    invocationsCounterMap.get(namePrefix) match {
      case None =>
        require(incomingCounter == 1, s"first added counter should be 1 not $incomingCounter")
        new ActorCreationRecorder(invocationsCounterMap.updated(namePrefix, Value(ref, 1)))
      case Some(storedCounter) =>
        require(storedCounter.numberOfEntries + 1 == incomingCounter, s"new counter value is $incomingCounter but should be ${storedCounter.numberOfEntries + 1} because the oldest is ${storedCounter.numberOfEntries}")
        new ActorCreationRecorder(invocationsCounterMap.updated(namePrefix, Value(ref, storedCounter.numberOfEntries + 1)))
    }
  }

  def record[T](actorRef: ActorRef[Nothing]): ActorCreationRecorder = add(actorRef.path.name, actorRef)

  private def extractValues(name: String): (String, Int) = {
    val NameRegEx = """(\w+)-(\d+)""".r
    Try[(String, Int)]({
      val NameRegEx(a, b) = name
      (a, b.toInt)
    }) match {
      case Success(value) => value
      case Failure(e) => throw new IllegalArgumentException(s"name format should be name-<number> $name is not valid")
    }
  }


}
