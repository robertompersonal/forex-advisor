package org.binqua.forex.util

import akka.actor.testkit.typed.LoggingEvent
import akka.actor.testkit.typed.scaladsl.LoggingTestKit
import akka.actor.typed.ActorSystem
import cats.data.Reader
import cats.kernel.Eq
import org.slf4j.event.Level

import java.util.concurrent.atomic.AtomicBoolean
import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}

object PreciseLoggingTestKit {

  implicit def fromStringToInfoEqualMatcher(messageToBeMatched: String): (Level, SmartMatcher) = (Level.INFO, SmartMatcher.Equal(messageToBeMatched))

  sealed trait MatchingType

  case object ExpectExactlyFollowingSeqOfMessages extends MatchingType

  case object ExpectAtLeastTheFollowingMessages extends MatchingType

  private def amendErrorMessage(
      alreadyMatched: Seq[(Level, SmartMatcher)],
      expectedMessages: Seq[(Level, SmartMatcher)],
      originalAssertionError: String
  ): Reader[String, String] =
    Reader((beforeThatPart: String) =>
      s"Test failed:\nBefore that I $beforeThatPart${expectedMessages.size - alreadyMatched.size} logs are still missing:\n\n${missingMessagesFormatted(alreadyMatched, expectedMessages)}\n\n originalAssertionError:\n$originalAssertionError"
    )

  private def missingMessagesFormatted(alreadyMatched: Seq[(Level, SmartMatcher)], expectedMessages: Seq[(Level, SmartMatcher)]): String =
    expectedMessages
      .diff(alreadyMatched)
      .zipWithIndex
      .map(x => s"${x._2 + 1}) ${x._1}")
      .mkString("\n")

  private def testThrownExceptionMessage(
      throwable: Throwable,
      alreadyMatched: Seq[(Level, SmartMatcher)],
      expectedMessages: Seq[(Level, SmartMatcher)],
      originalAssertionError: String
  ): Reader[String, String] =
    Reader((beforeThatPart: String) =>
      s"Test did not failed but thrown an exception $throwable.\nBefore that I $beforeThatPart${expectedMessages.size - alreadyMatched.size} logs are still missing:\n${missingMessagesFormatted(alreadyMatched, expectedMessages)})\n\n originalAssertionError:\n$originalAssertionError"
    )

  private def beforeThatPart(alreadyMatched: Seq[(Level, SmartMatcher)]): String =
    if (alreadyMatched.isEmpty) "did not match any logs and " else s"matched ${alreadyMatched.size} logs:\n\n${alreadyMatched.mkString("\n")}\n\nbut "

  def expectExactlyTheFollowingSeqOfMessages[T](expectedMessages: (Level, SmartMatcher)*): RunnablePreciseLoggingTestKit =
    expect(ExpectExactlyFollowingSeqOfMessages, expectedMessages)

  def expectAtLeast[T](expectedMessages: (Level, SmartMatcher)*): RunnablePreciseLoggingTestKit =
    expect(ExpectAtLeastTheFollowingMessages, expectedMessages)

  private def expect[T](matchingType: MatchingType, expectedMessages: Seq[(Level, SmartMatcher)]): RunnablePreciseLoggingTestKit = {
    require(expectedMessages.nonEmpty)
    new RunnablePreciseLoggingTestKit {
      var entriesAlreadyMatched: Seq[(Level, SmartMatcher)] = Seq.empty
      val testFailed: AtomicBoolean = new AtomicBoolean(false)

      override def whileRunning[T](code: => T)(implicit system: ActorSystem[_]): T =
        Try {
          LoggingTestKit
            .custom(new Function[LoggingEvent, Boolean] {
              var counter: Int = 0

              override def apply(loggingEvent: LoggingEvent): Boolean = {
                if (testFailed.get()) false
                else {
                  val expectedEntry: (Level, SmartMatcher) = expectedMessages(counter)
                  val singleMatchResult: SingleMatchResult = matchingType match {
                    case ExpectAtLeastTheFollowingMessages =>
                      if (matched(loggingEvent, expectedEntry)) {
                        SingleMatchResult(
                          matched = true,
                          nextCounter = counter + 1,
                          entriesAlreadyMatched = entriesAlreadyMatched.:+(expectedEntry),
                          testFailed = false
                        )
                      } else
                        SingleMatchResult(matched = false, nextCounter = counter, entriesAlreadyMatched = entriesAlreadyMatched, testFailed = false)
                    case ExpectExactlyFollowingSeqOfMessages =>
                      if (matched(loggingEvent, expectedEntry)) {
                        SingleMatchResult(
                          matched = true,
                          nextCounter = counter + 1,
                          entriesAlreadyMatched = entriesAlreadyMatched.:+(expectedEntry),
                          testFailed = false
                        )
                      } else {
                        SingleMatchResult(
                          matched = false,
                          nextCounter = counter,
                          entriesAlreadyMatched = entriesAlreadyMatched,
                          testFailed = entriesAlreadyMatched.nonEmpty
                        )
                      }
                  }
                  counter = singleMatchResult.nextCounter
                  entriesAlreadyMatched = singleMatchResult.entriesAlreadyMatched
                  testFailed.set(singleMatchResult.testFailed)
                  singleMatchResult.matched
                }
              }
            })
            .withOccurrences(expectedMessages.size)
            .expect {
              code
            }
        } match {
          case Success(value) => value
          case Failure(throwable) =>
            if (throwable.isInstanceOf[AssertionError])
              throw new AssertionError(
                amendErrorMessage(entriesAlreadyMatched, expectedMessages, throwable.getMessage).run(beforeThatPart(entriesAlreadyMatched))
              )
            else
              throw new IllegalStateException(
                testThrownExceptionMessage(throwable, entriesAlreadyMatched, expectedMessages, throwable.getMessage).run(beforeThatPart(entriesAlreadyMatched)),
                throwable
              )
        }

    }
  }

  case class SingleMatchResult(matched: Boolean, nextCounter: Int, entriesAlreadyMatched: Seq[(Level, SmartMatcher)], testFailed: Boolean)

  private def matched(loggingEvent: LoggingEvent, expectedEntry: (Level, SmartMatcher)): Boolean = {
    expectedEntry._2.test(loggingEvent.message) && (loggingEvent.level == expectedEntry._1)
  }
}

trait SmartMatcher {
  def test(messageFromAkka: String): Boolean
}

object SmartMatcher {

  case class Equal(logMessageToFind: String) extends SmartMatcher {
    override def test(messageFromAkka: String): Boolean = Eq.eqv(messageFromAkka, logMessageToFind)
  }

  case class StartWith(logMessageToFind: String) extends SmartMatcher {
    override def test(messageFromAkka: String): Boolean = messageFromAkka.startsWith(logMessageToFind)
  }

  case class Contains(logMessageToFind: String) extends SmartMatcher {
    override def test(messageFromAkka: String): Boolean = messageFromAkka.contains(logMessageToFind)
  }

  case class SupervisorRestartErrorLog(throwable: Throwable) extends SmartMatcher {
    override def test(messageFromAkka: String): Boolean = messageFromAkka.startsWith(throwable.getMessage)
  }

  object infoLog {
    def contains(logMessageToFind: String): (Level, SmartMatcher) = (Level.INFO, Contains(logMessageToFind))
  }

  object errorLog {
    def supervisorRestart(throwable: Throwable): (Level, SmartMatcher) = (Level.ERROR, SmartMatcher.SupervisorRestartErrorLog(throwable))

    def equal(logMessageToFind: String): (Level, SmartMatcher) = (Level.ERROR, Equal(logMessageToFind))
  }
}

trait RunnablePreciseLoggingTestKit {
  def whileRunning[T](code: => T)(implicit system: ActorSystem[_]): T
}
