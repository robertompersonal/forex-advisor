package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.{GivenWhenThen, Informing}
import play.api.libs.json.Json

trait DocumentedSerializeAndDeserializeSpec extends SerializeAndDeserializeSpec {

  this: ScalaTestWithActorTestKit with GivenWhenThen with Informing =>

  val assertThatCanBeSerialiseAndDeserialize: AnyRef => Unit =
    assertionBuilder(canBeSerialiseAndDeserialize, (actualJson: String) => Json.prettyPrint(Json.parse(actualJson)))

  def assertionBuilder[A](serialiseAndDeserialize: AnyRef => String, prettyJson: String => String): AnyRef => Unit =
    messageToBeTested => {
      info("---")

      Given(message = s"a\n$messageToBeTested")

      When("we serialise and deserialize it")

      val actualJson: String = serialiseAndDeserialize(messageToBeTested)

      Then(message = s"we get the same object back and the json format is:\n${prettyJson(actualJson)}")

      info("---")
    }

}
