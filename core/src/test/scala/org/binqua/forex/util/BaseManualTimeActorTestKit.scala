package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit}
import com.typesafe.config.ConfigFactory
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}

abstract class BaseManualTimeActorTestKit extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(ConfigFactory.load("application-test"))) with BeforeAndAfterEach with AnyFlatSpecLike with MockFactory with Matchers with GivenWhenThen with AkkaTestingFacilities with Validation
