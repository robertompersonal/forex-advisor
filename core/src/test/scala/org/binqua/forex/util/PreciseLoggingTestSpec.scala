package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import org.slf4j.event.Level

import scala.util.{Failure, Success, Try}

class PreciseLoggingTestSpec
    extends ScalaTestWithActorTestKit
    with BeforeAndAfterEach
    with AnyFlatSpecLike
    with Matchers
    with GivenWhenThen
    with AkkaTestingFacilities
    with TestingFacilities
    with Validation {

  "expectExactlyTheFollowingSeqOfMessages b c with a b c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectExactlyTheFollowingSeqOfMessages(
        (Level.INFO, SmartMatcher.Equal("b")),
        (Level.INFO, SmartMatcher.Equal("c"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectAtLeastTheFollowingMessages b c with a b z c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("z")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectAtLeast(
        (Level.INFO, SmartMatcher.Equal("b")),
        (Level.INFO, SmartMatcher.Equal("c"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectAtLeastTheFollowingMessages b c with a b c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectAtLeast(
        (Level.INFO, SmartMatcher.Equal("b")),
        (Level.INFO, SmartMatcher.Equal("c"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectAtLeastTheFollowingMessages b c with b c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("b")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectAtLeast(
        (Level.INFO, SmartMatcher.Equal("b")),
        (Level.INFO, SmartMatcher.Equal("c"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectExactlyTheFollowingSeqOfMessages b c with a b d e messages (AssertionError is thrown)" should "fail" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("d")
            context.log.info("e")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )
    Try {
      PreciseLoggingTestKit
        .expectExactlyTheFollowingSeqOfMessages(
          (Level.INFO, SmartMatcher.Equal("b")),
          (Level.INFO, SmartMatcher.Equal("c"))
        )
        .whileRunning {
          underTest ! "log"
        }
    } match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        val message1 = throwable.getMessage
        message1 should be(
          "Test failed:\nBefore that I matched 1 logs:\n\n(INFO,Equal(b))\n\nbut 1 logs are still missing:\n\n1) (INFO,Equal(c))\n\n originalAssertionError:\nTimeout (3 seconds) waiting for 1 messages on LoggingTestKitImpl(2,None,None,None,None,None,None,Map(),Some(<function1>))."
        )
    }

  }

  "expectExactlyTheFollowingSeqOfMessages a b c d with a b c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectExactlyTheFollowingSeqOfMessages(
        (Level.INFO, SmartMatcher.Equal("a")),
        (Level.INFO, SmartMatcher.Equal("b")),
        (Level.INFO, SmartMatcher.Equal("c")),
        (Level.INFO, SmartMatcher.Equal("d"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectExactlyTheFollowingSeqOfMessages a b with a b c d messages" should "pass" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    PreciseLoggingTestKit
      .expectExactlyTheFollowingSeqOfMessages(
        (Level.INFO, SmartMatcher.Equal("a")),
        (Level.INFO, SmartMatcher.Equal("b"))
      )
      .whileRunning {
        underTest ! "log"
      }

  }

  "expectExactlyTheFollowingSeqOfMessages a b c d with a b z c d (AssertionError is thrown)" should "fail" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            context.log.info("z")
            context.log.info("c")
            context.log.info("d")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    Try(
      PreciseLoggingTestKit
        .expectExactlyTheFollowingSeqOfMessages(
          (Level.INFO, SmartMatcher.Equal("a")),
          (Level.INFO, SmartMatcher.Equal("b")),
          (Level.INFO, SmartMatcher.Equal("c")),
          (Level.INFO, SmartMatcher.Equal("d"))
        )
        .whileRunning {
          underTest ! "log"
        }
    ) match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        throwable.getMessage should be(
          "Test failed:\nBefore that I matched 2 logs:\n\n(INFO,Equal(a))\n(INFO,Equal(b))\n\nbut 2 logs are still missing:\n\n1) (INFO,Equal(c))\n2) (INFO,Equal(d))\n\n originalAssertionError:\nTimeout (3 seconds) waiting for 2 messages on LoggingTestKitImpl(4,None,None,None,None,None,None,Map(),Some(<function1>))."
        )
    }
  }

  "expectExactlyTheFollowingSeqOfMessages b with a b messages when an exception is thrown and not messages match" should "fail" in {

    Try(
      PreciseLoggingTestKit
        .expectExactlyTheFollowingSeqOfMessages(
          (Level.INFO, SmartMatcher.Equal("b")),
          (Level.INFO, SmartMatcher.Equal("c"))
        )
        .whileRunning {
          throw new RuntimeException("Ops!!")
        }
    ) match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        throwable.getMessage should be(
          "Test did not failed but thrown an exception java.lang.RuntimeException: Ops!!.\nBefore that I did not match any logs and 2 logs are still missing:\n1) (INFO,Equal(b))\n2) (INFO,Equal(c)))\n\n originalAssertionError:\nOps!!"
        )
    }
  }

  "expectExactlyTheFollowingSeqOfMessages b c with a b messages when an exception is thrown and 1 message is matched" should "fail" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            context.log.info("b")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    Try(
      PreciseLoggingTestKit
        .expectExactlyTheFollowingSeqOfMessages(
          (Level.INFO, SmartMatcher.Equal("b")),
          (Level.INFO, SmartMatcher.Equal("c"))
        )
        .whileRunning {
          underTest ! "log"
          waitALittleBit(soThat("b can be delivered"))
          throw new RuntimeException("Ops!!")
        }
    ) match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        throwable.getMessage should be(
          "Test did not failed but thrown an exception java.lang.RuntimeException: Ops!!.\nBefore that I matched 1 logs:\n\n(INFO,Equal(b))\n\nbut 1 logs are still missing:\n1) (INFO,Equal(c)))\n\n originalAssertionError:\nOps!!"
        )
    }

  }

  "expectExactlyTheFollowingSeqOfMessages b with a messages when no messages is matched (AssertionError is thrown)" should "fail with right error" in {

    val underTest = spawn(
      Behaviors.receive[String]((context: ActorContext[_], message) =>
        message match {
          case "log" =>
            context.log.info("a")
            Behaviors.same
          case _ =>
            Behaviors.same
        }
      )
    )

    Try(
      PreciseLoggingTestKit
        .expectExactlyTheFollowingSeqOfMessages(
          (Level.INFO, SmartMatcher.Equal("b"))
        )
        .whileRunning {
          underTest ! "log"
          waitALittleBit(soThat("log can be delivered"))
        }
    ) match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        throwable.getMessage should be(
          "Test failed:\nBefore that I did not match any logs and 1 logs are still missing:\n\n1) (INFO,Equal(b))\n\n originalAssertionError:\nTimeout (3 seconds) waiting for 1 messages on LoggingTestKitImpl(1,None,None,None,None,None,None,Map(),Some(<function1>))."
        )
    }

  }

  "expectAtLeastTheFollowingMessages b c when an exception is thrown and not messages match" should "fail" in {

    Try(
      PreciseLoggingTestKit
        .expectAtLeast(
          (Level.INFO, SmartMatcher.Equal("b")),
          (Level.INFO, SmartMatcher.Equal("c"))
        )
        .whileRunning {
          throw new RuntimeException("Ops!!")
        }
    ) match {
      case Success(_) => thisTestShouldNotHaveArrivedHere(TestingFacilities.because("it should have failed"))
      case Failure(throwable) =>
        val message1 = throwable.getMessage
        message1 should be(
          "Test did not failed but thrown an exception java.lang.RuntimeException: Ops!!.\nBefore that I did not match any logs and 2 logs are still missing:\n1) (INFO,Equal(b))\n2) (INFO,Equal(c)))\n\n originalAssertionError:\nOps!!"
        )
    }
  }

}
