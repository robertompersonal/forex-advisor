import DockerCommon.theDockerBuildOptions
import Npm.{`ui-test`, executeUiTests, _}
import com.typesafe.sbt.packager.Keys.{dist, dockerRepository}
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{
  dockerBaseImage => _,
  dockerBuildOptions => _,
  dockerCommands => _,
  dockerExposedVolumes => _,
  dockerRepository => _,
  _
}
import com.typesafe.sbt.packager.docker.{DockerChmodType, DockerPermissionStrategy, ExecCmd}
import com.typesafe.sbt.packager.universal.UniversalPlugin.autoImport.stage
import play.sbt.PlayImport
import sbt.Keys.{libraryDependencies, scalacOptions, test, version}
import sbt.{Test, file}

Global / excludeLintKeys += Docker / dockerBuildOptions

ThisBuild / dockerBaseImage := "java"
ThisBuild / dockerRepository := Some("torebor")
ThisBuild / daemonUser := "forexAdvisor"
ThisBuild / organization := "com.binqua"
ThisBuild / organizationName := "binqua"
ThisBuild / scalaVersion := "2.13.3"
ThisBuild / scalacOptions := Seq("-deprecation", "-unchecked", "-feature")

ThisBuild / testForkedParallel := true
ThisBuild / Test / fork := true
ThisBuild / Test / run / javaOptions := Seq("-Xmx1G", "-Xms1G", "-verbose:gc", "-XX:+PrintGCDetails", "-Xloggc:./jvmgc.log")

ThisBuild / version := "0.1.0-SNAPSHOT"

lazy val core = project
  .in(file("core"))
  .settings(
    libraryDependencies ++= Dependencies.bundles.commonDependencies ++ Seq(
      Dependencies.io.socket,
      Dependencies.io.engine
    ),
    dockerExposedPorts := Seq(2501),
    DockerCommon.actorSystemRunningSettings,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .enablePlugins(JavaAppPackaging)
  .dependsOn(testUtil)

lazy val clientsToExternalServices = project
  .in(file("clients-to-external-systems"))
  .settings(
    libraryDependencies ++= Dependencies.bundles.commonDependencies ++ Seq(
      Dependencies.io.socket,
      Dependencies.io.engine
    ),
    dockerExposedPorts := Seq(2500),
    DockerCommon.actorSystemRunningSettings,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .enablePlugins(JavaAppPackaging)
  .dependsOn(testUtil, core % "test->test", core % "compile->compile")

lazy val testUtil = project
  .in(file("test-util"))
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.akka.slf4j,
      Dependencies.akka.management.core,
      Dependencies.akka.management.clusterHttp,
      Dependencies.akka.actor,
      Dependencies.akka.typed.actor,
      Dependencies.akka.typed.cluster,
      Dependencies.akka.typed.clusterSharding,
      Dependencies.akka.persistence.cassandra,
      Dependencies.akka.persistence.core,
      Dependencies.akka.persistence.query,
      Dependencies.akka.serializationJackson,
      Dependencies.akka.httpSpray,
      Dependencies.cats.core,
      Dependencies.logBack,
      Dependencies.scalaCheck,
      Dependencies.scalaTest,
      Dependencies.scalaTestPlus.scalaCheck,
      Dependencies.akka.testKit % Test,
      Dependencies.akka.typed.testKit % Test,
      Dependencies.flexmark % Test
    ),
    publish / skip := true,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )

lazy val testClient = project
  .in(file("test-client"))
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.akka.actor,
      Dependencies.akka.typed.actor,
      Dependencies.cats.kittens,
      Dependencies.akka.http,
      Dependencies.akka.httpSpray,
      Dependencies.akka.slf4j,
      Dependencies.play.json,
      Dependencies.logBack,
      Dependencies.scalaCheck,
      Dependencies.scalactic,
      Dependencies.scalaTest,
      Dependencies.scalaTestPlus.scalaCheck,
      Dependencies.akka.testKit % Test,
      Dependencies.akka.typed.testKit % Test
    ),
    DockerCommon.actorSystemRunningSettings,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .enablePlugins(JavaAppPackaging)
  .dependsOn(core % "test->test", testUtil, webTestUtil, webCore)

lazy val webCore = project
  .in(file("web-core"))
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.cats.core,
      Dependencies.scalaCheck % Test,
      Dependencies.scalaMock % Test,
      Dependencies.scalaTestPlus.mockito % Test,
      Dependencies.scalaTestPlus.scalaCheck % Test
    ),
    publish / skip := true,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .dependsOn(core, webTestUtil)

lazy val web = project
  .in(file("web"))
  .settings(
    libraryDependencies ++= Seq(
      PlayImport.guice,
      Dependencies.akka.actor,
      Dependencies.akka.typed.cluster,
      Dependencies.akka.slf4j,
      Dependencies.logBack,
      PlayImport.clusterSharding,
      Dependencies.flexmark % Test,
      Dependencies.scalaCheck % Test,
      Dependencies.scalaMock % Test,
      Dependencies.scalaTestPlus.mockito % Test,
      Dependencies.scalaTestPlus.play % Test,
      Dependencies.scalaTestPlus.scalaCheck % Test
    ),
    PlayKeys.playRunHooks += baseDirectory.map(file => FrontendRunHook.apply(file)).value,
    `ui-prod-build` := (if (executeProdBuild(baseDirectory.value / "ui") != Success) throw new Exception("Oops! UI Build crashed.")),
    `ui-test` := (if (executeUiTests(baseDirectory.value / "ui") != Success) throw new Exception("UI tests failed!")),
    dist := (dist dependsOn `ui-prod-build`).value,
    stage := (stage dependsOn `ui-prod-build`).value,
    test := ((Test / test) dependsOn `ui-test`).value,
    dockerBuildOptions := theDockerBuildOptions(dockerBuildOptions.value),
    Docker / daemonUser := (ThisBuild / daemonUser).value,
    Docker / packageName := DockerCommon.toDockerPackageName(baseDirectory.value),
    dockerChmodType := DockerChmodType.UserGroupWriteExecute,
    dockerCommands ++= Seq(
      ExecCmd(
        "CMD",
        s"-Dlogback.debug=true",
        s"-Dconfig.file=${(Docker / defaultLinuxInstallLocation).value}/conf/web-prod.conf",
        s"-Dlogger.file=${(Docker / defaultLinuxInstallLocation).value}/conf/web-prod-logger.xml"
      )
    ),
    dockerExposedPorts := Seq(2503),
    dockerPermissionStrategy := DockerPermissionStrategy.CopyChown,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .enablePlugins(PlayScala)
  .dependsOn(core)
  .dependsOn(testUtil)
  .dependsOn(webTestUtil)
  .dependsOn(webCore)

lazy val webTestUtil = project
  .in(file("web-test-util"))
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.logBack,
      Dependencies.play.json,
      Dependencies.scalaCheck,
      Dependencies.scalactic,
      Dependencies.scalaTest,
      Dependencies.scalaTestPlus.scalaCheck
    ),
    publish / skip := true,
    Test / testOptions += common.scalaTestArgs(baseDirectory.value)
  )
  .dependsOn(core)

lazy val `forex-advisor` = (project in file("."))
  .settings(
    publish / skip := true
  )
  .aggregate(core, clientsToExternalServices, testUtil, web, webTestUtil, testClient)
