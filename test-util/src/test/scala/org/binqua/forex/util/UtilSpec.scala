package org.binqua.forex.util

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.{Http, HttpExt}
import akka.stream.Materializer
import akka.util.ByteString
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.concurrent.{Await, ExecutionContextExecutor, Future}

class UtilSpec extends ScalaTestWithActorTestKit with AnyFlatSpecLike with GivenWhenThen {

  "findRandomOpenPortOnAllLocalInterfaces" should "return an available open port" in {

    import akka.actor.typed.scaladsl.adapter._

    implicit val mater: Materializer = Materializer.matFromSystem(system.toClassic)

    implicit val ec: ExecutionContextExecutor = system.executionContext

    val body = "Hello world!"

    val handler = get {
      complete(body)
    }

    val http: HttpExt = Http(system.toClassic)

    try (1 to 10).foreach { _ =>
      val (host, port) = ("localhost", Util.findRandomOpenPortOnAllLocalInterfaces())

      val bindingFuture: Future[ServerBinding] = Http().newServerAt(host, port).bindFlow(handler)
      bindingFuture.failed.foreach { _ => fail("this should not happen") }

      val eventualAHttpResponseBody: Future[String] = for {
        response <- http.singleRequest(HttpRequest(HttpMethods.GET, s"http://$host:$port"))
        body <- response.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      } yield body

      import scala.concurrent.duration._

      Await.result(eventualAHttpResponseBody, 5.second) shouldBe body

      bindingFuture.flatMap(_.unbind())
    }
    finally {
      http.shutdownAllConnectionPools()
    }
  }

}
