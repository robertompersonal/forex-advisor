package org.binqua.forex.util

import java.net.ServerSocket

object Util {

  def findRandomOpenPortOnAllLocalInterfaces(): Int = {
    val socket = new ServerSocket(0)
    try {
      socket.getLocalPort
    } finally {
      if (socket != null) socket.close()
    }
  }

}
