package org.binqua.forex.util

import akka.actor.UnhandledMessage
import org.scalactic.Equality

trait TestingFacilities {

  def actualValueIsWrong(tag: Any, actualValue: Any): String = s"\n\nActual $tag is wrong:\n$actualValue\n"

  def actualValueIsWrong(tag: Any, actualValue: Any, details: Map[String, Any]): String =
    s"Actual \n\n$tag is wrong:${details.mkString("\n\n", "\n\n", "\n\n")}"

  def actualValueIsWrong(actualValue: Any): String = actualValueIsWrong("Actual value", actualValue)

  def thisTestShouldNotHaveArrivedHere: Nothing = org.scalatest.Assertions.fail("\n\nThis test should not arrived at this point .... something went wrong")

  def thisTestShouldNotHaveArrivedHere(exception: Throwable): Nothing =
    org.scalatest.Assertions.fail(s"\n\nThis test should not arrived at this point .... something went wrong.\n${exception.getMessage}")

  def thisTestShouldNotHaveArrivedHere(because: String): Nothing =
    org.scalatest.Assertions.fail(s"\n\nThis test should not arrived at this point because:\n$because\n.... something went wrong")

  def thisTestShouldNotHaveArrivedHere(because: TestingFacilities.because): Nothing = thisTestShouldNotHaveArrivedHere(because.reason)

  def thisTestShouldNotHaveArrivedHere(details: Map[String, Any]): Nothing =
    org.scalatest.Assertions.fail(s"\n\nThis test should not arrived at this point .... something went wrong:${details.mkString("\n\n", "\n\n", "\n\n")}")

  def failBecauseThisTestShouldHavePassed: Nothing = org.scalatest.Assertions.fail("\n\nThis test should have passed")

  def failBecauseActualValueIsWrong(custom: String, actualValue: Any): Nothing = org.scalatest.Assertions.fail(s"\n\n$custom is wrong:\n\n$actualValue\n")
}

object TestingFacilities extends TestingFacilities {

  final case class because(reason: String)

  implicit val unhandledMessageOnlyEnvelopedMessageEquality = new Equality[UnhandledMessage] {
    override def areEqual(first: UnhandledMessage, second: Any): Boolean = {
      first.message == second.asInstanceOf[UnhandledMessage].message
    }
  }

}
