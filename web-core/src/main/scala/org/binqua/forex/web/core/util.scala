package org.binqua.forex.web.core

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.Seq

object util {

  private implicit val jsonValidationErrorWrites: play.api.libs.json.Writes[JsonValidationError] = (o: JsonValidationError) => JsArray(o.messages.map(JsString))

  private implicit val jsPathWrites: play.api.libs.json.Writes[JsPath] = (o: JsPath) => JsString(o.toJsonString)

  private implicit val singleJsPathError: play.api.libs.json.Writes[(JsPath, Seq[JsonValidationError])] = (
    (JsPath \ "path").write[JsPath] and
      (JsPath \ "errors").write[Seq[JsonValidationError]]
  ) { t => (t._1, t._2) }

  private implicit val jsErrorWrites: play.api.libs.json.Writes[JsError] = (o: JsError) =>
    (JsPath \ "errors").write[Seq[(JsPath, Seq[JsonValidationError])]].writes(o.errors)

  def singleError(content: String): JsObject = Json.obj("errors" -> content)

  def singleMessage(content: String): JsObject = Json.obj("message" -> content)

  def fromJsError(content: JsError): JsValue = Json.toJson(content)(jsErrorWrites)

  val expectingJson: JsObject = singleError(content = "Expecting json request body")
}
