package org.binqua.forex.web.core.portfolio

import cats.data
import cats.data.Validated.{Invalid, Valid}
import cats.data.{NonEmptySet, Validated}
import cats.implicits.catsSyntaxOptionId
import org.binqua.forex.advisor.model.{CurrencyPair, MicroLot, PosBigDecimal, TradingPair}
import org.binqua.forex.advisor.portfolios.PortfoliosModel
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{Buy, CreatePortfolio, PortfolioName, Position, PositionType, Sell, UpdatePortfolio}
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import org.binqua.forex.web.core.Field
import org.binqua.forex.web.core.validation.{defaultReadWithCustomErrorMessage, pathHasToExist}
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads.{bigDecReads, _}
import play.api.libs.json.{Reads, _}

import java.time.LocalDateTime
import java.util.{Calendar, TimeZone, UUID}
import scala.collection.Seq
import scala.collection.immutable.TreeSet
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

object PortfoliosJson {

  object fields {
    val pair = Field("Pair")
  }

  val readIdempotentKey: Reads[IdempotentKey] = {
    val field = Field("IdempotentKey")
    val anIdempotentKey = "a uuid value"
    val errorMessage = field.hasToBe(anIdempotentKey)

    pathHasToExist[String](field.jsPath, field.hasToExist.hasToBe(anIdempotentKey))(
      defaultReadWithCustomErrorMessage[String](errorMessage)
        .filter(JsonValidationError(errorMessage))(str =>
          Try(UUID.fromString(str)) match {
            case Success(_) => true
            case Failure(_) => false
          }
        )
    ).map(str => IdempotentKey(UUID.fromString(str)))
  }

  val readMaybeAnIdempotentKey: Reads[Option[IdempotentKey]] = (JsPath \ "idempotentKey")
    .readNullable[String](JsPath.read[String])
    .flatMap({
      case Some(value) =>
        Try(UUID.fromString(value)) match {
          case Success(validKey) => (_: JsValue) => JsSuccess(IdempotentKey(validKey).some)
          case Failure(_)        => Reads.failed(s"idempotentKey field has to be a valid UUID but its value is $value")
        }
      case None => (_: JsValue) => JsSuccess(None)
    })

  val readAmount: Reads[Int] = {

    val field = Field("Amount")
    val biggerThanMessage = "a positive integer"
    val errorMessage = field.hasToBe(biggerThanMessage)

    pathHasToExist[Int](field.jsPath, field.hasToExist.hasToBe(biggerThanMessage))(
      defaultReadWithCustomErrorMessage[Int](errorMessage)
        .filter(JsonValidationError(errorMessage))(_ > 0)
    )
  }

  val readPrice: Reads[PosBigDecimal] = {
    val field = Field("Price")
    val positiveDoubleMessage = "a double > than 0"
    val errorMessage = field.hasToBe(positiveDoubleMessage)

    pathHasToExist[BigDecimal](field.jsPath, field.hasToExist.hasToBe(positiveDoubleMessage))(
      defaultReadWithCustomErrorMessage[BigDecimal](errorMessage)
        .filter(JsonValidationError(errorMessage))(_ > 0)
    ).map(PosBigDecimal.unsafeFrom)
  }

  val readPositionType: Reads[PositionType] = {
    val sellId = "sell"
    val validValues = Seq("buy", sellId)
    val field = Field("PositionType")
    val oneOf = validValues.mkString(" or ")
    val errorMessage = field.hasToBe(oneOf)

    pathHasToExist[String](field.jsPath, field.hasToExist.hasToBe(oneOf))(
      defaultReadWithCustomErrorMessage[String](errorMessage)
        .filter(JsonValidationError(errorMessage))(validValues.contains(_))
    ).map({
      case `sellId` => Sell
      case _        => Buy
    })
  }

  val readPositionId: Reads[PortfoliosModel.PositionId] = {
    val field = Field("PositionId")
    val oneOf = "a valid UUID"
    val errorMessage = field.hasToBe(oneOf)

    pathHasToExist[String](field.jsPath, field.hasToExist.hasToBe(oneOf))(
      defaultReadWithCustomErrorMessage[String](errorMessage)
        .filter(JsonValidationError(errorMessage))(raw =>
          Try(UUID.fromString(raw)) match {
            case Success(_) => true
            case Failure(_) => false
          }
        )
    ).map(UUID.fromString)
  }

  val readName: Reads[PortfolioName] = {
    val field = Field("Name")

    for {
      nameToBeValidated <- pathHasToExist[String](field.jsPath, field.hasToExist)
      validPortfolioName <- PortfolioName.maybeAPortfolioName(nameToBeValidated) match {
        case Right(validPortfolioName) => Reads(_ => JsSuccess(validPortfolioName))
        case Left(error)               => Reads { _ => toJsError(List(error)) }
      }
    } yield validPortfolioName
  }

  private def toJsError(errors: List[String]) = JsError(JsonValidationError(errors))

  def readPair(currencyPairs: List[CurrencyPair]): Reads[CurrencyPair] = {
    val field = fields.pair
    val allowedValues = currencyPairs.map(CurrencyPair.toExternalIdentifier)
    val oneOf = s"one of ${allowedValues.mkString(" ")}"
    val errorMessage = field.hasToBe(oneOf)

    pathHasToExist[String](field.jsPath, field.hasToExist.hasToBe(oneOf))(
      defaultReadWithCustomErrorMessage[String](errorMessage)
        .filter(JsonValidationError(errorMessage))(allowedValues.contains)
    ).map(CurrencyPair.toCurrencyPair(_, currencyPairs).get)
  }

  val readOpenDate: Reads[LocalDateTime] = {

    val datePattern = raw"""(\d{2}) (\d{2}) (\d{2}) at (\d{2}):(\d{2}):(\d{2})""".r

    def jsErrorDueToInvalidDatePattern(customMessage: String) = JsError(Seq(JsPath -> Seq(JsonValidationError(customMessage))))

    def tryToParse(incomingDate: String, customMessage: String): JsResult[LocalDateTime] with Serializable = {
      val dateToBeParsed: Regex.Match = datePattern.findFirstMatchIn(incomingDate).get
      val calendar = Calendar.getInstance()
      calendar.setLenient(false)
      calendar.set(
        s"20${dateToBeParsed.group(3)}".toInt,
        dateToBeParsed.group(2).toInt - 1,
        dateToBeParsed.group(1).toInt,
        dateToBeParsed.group(4).toInt,
        dateToBeParsed.group(5).toInt,
        dateToBeParsed.group(6).toInt
      )
      calendar.set(Calendar.MILLISECOND, 0)
      calendar.setTimeZone(TimeZone.getTimeZone("UTC"))

      Try(LocalDateTime.ofInstant(calendar.toInstant, calendar.getTimeZone.toZoneId)) match {
        case Success(value) => JsSuccess(value)
        case Failure(_)     => jsErrorDueToInvalidDatePattern(customMessage)
      }
    }

    def dateReads(errorMessage: String): Reads[LocalDateTime] = {
      case JsString(incomingDate) if datePattern.matches(incomingDate) => tryToParse(incomingDate, errorMessage)
      case _                                                           => jsErrorDueToInvalidDatePattern(errorMessage)
    }

    val field = Field("OpenDateTime")
    val aSpecificPatternMessage = "pattern 'dd mm hh at hh:mm:ss'"

    pathHasToExist[LocalDateTime](field.jsPath, errorMessage = field.hasToExist.hasToHave(aSpecificPatternMessage))(nextReads =
      dateReads(errorMessage = field.hasToHave(aSpecificPatternMessage))
    )
  }

  def readTradingPair(currencyPairs: List[CurrencyPair]): Reads[TradingPair] = {
    val maybeATradingPair: Reads[Either[String, TradingPair]] = (
      readPair(currencyPairs) and
        PortfoliosJson.readPrice
    )((pair, price) => TradingPair.maybeATradingPair(pair, price))
    maybeATradingPair.flatMap {
      case Right(tradingPair) => Reads[TradingPair](_ => JsSuccess(tradingPair))
      case Left(error)        => Reads[TradingPair](_ => JsError(fields.pair.jsPath, error))
    }
  }

  def positionReads(recordedLocalDateTime: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[Position] =
    (readAmount and
      readOpenDate and
      readTradingPair(currencyPairs) and
      readPositionType)((amount, openDateTime, tradingPair, sellOrBuy) => {
      Position(sellOrBuy, tradingPair, MicroLot(amount), openDateTime, recordedLocalDateTime)
    })

  def readSinglePositionToBeAdded(recordedLocalDateTime: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[PortfoliosModel.Add] =
    positionReads(recordedLocalDateTime, currencyPairs).map(PortfoliosModel.Add)

  def readAllPositionsToBeAdded(recordedPositionData: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[scala.Seq[PortfoliosModel.Add]] = {
    implicit val singleAddReader: Reads[PortfoliosModel.Add] = readSinglePositionToBeAdded(recordedPositionData, currencyPairs)
    val fieldId = "toBeAdded"
    arrayReadWithErrorRemap(
      originalReads = (JsPath \ fieldId).readWithDefault[scala.Seq[PortfoliosModel.Add]](List()),
      errorPrefix = fieldId
    )

  }

  val readAllPositionsToBeDeleted: Reads[scala.Seq[PortfoliosModel.Delete]] = {
    implicit val singleDeleteReader = readPositionId.map(PortfoliosModel.Delete)
    val fieldId = "toBeDeleted"
    arrayReadWithErrorRemap(
      originalReads = (JsPath \ fieldId).readWithDefault[scala.Seq[PortfoliosModel.Delete]](List()),
      errorPrefix = fieldId
    )
  }

  private def arrayReadWithErrorRemap[A](originalReads: Reads[A], errorPrefix: String): Reads[A] =
    (json: JsValue) => {
      originalReads.reads(json) match {
        case success @ JsSuccess(_, _) => success
        case e @ JsError(_)            => e
      }
    }

  def readSinglePositionToBeUpdated(recordedLocalDateTime: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[PortfoliosModel.Update] =
    (readPositionId and (JsPath \ "position").read[Position](positionReads(recordedLocalDateTime, currencyPairs)))(PortfoliosModel.Update)

  def readAllPositionsToBeUpdated(recordedPositionData: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[scala.Seq[PortfoliosModel.Update]] = {
    implicit val update: Reads[PortfoliosModel.Update] = readSinglePositionToBeUpdated(recordedPositionData, currencyPairs)
    val fieldId = "toBeUpdated"
    arrayReadWithErrorRemap(
      originalReads = (JsPath \ fieldId).readWithDefault[scala.Seq[PortfoliosModel.Update]](List()),
      errorPrefix = fieldId
    )
  }

  def updatePortfolio(dateFactory: () => LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[PortfoliosModel.UpdatePortfolio] = {
    val validatedUpdatePortfolioReads: Reads[Validated[List[String], UpdatePortfolio]] = (readMaybeAnIdempotentKey and
      readName and
      readAllPositionsToBeAdded(dateFactory(), currencyPairs) and
      readAllPositionsToBeDeleted and
      readAllPositionsToBeUpdated(dateFactory(), currencyPairs))((idempotentKey, name, toBeAdded, toBeDeleted, toBeUpdated) =>
      UpdatePortfolio.validated(idempotentKey, name, toBeAdded.toList ::: toBeDeleted.toList ::: toBeUpdated.toList)
    )

    validatedUpdatePortfolioReads.flatMap {
      case Valid(theUpdatePortfolio: UpdatePortfolio) => Reads { _ => JsSuccess(theUpdatePortfolio) }
      case Invalid(errors: List[String])              => Reads { _ => toJsError(errors) }
    }
  }

  def positionsReads(recordedPositionData: LocalDateTime, currencyPairs: List[CurrencyPair]): Reads[scala.Seq[Position]] = {
    val field = Field("Positions")
    val atLeast1Position = "at least 1 position"

    implicit val newPositionReads: Reads[Position] = PortfoliosJson.positionReads(recordedPositionData, currencyPairs)

    val newPositionRead: Reads[scala.Seq[Position]] = implicitly[Reads[scala.Seq[Position]]]

    pathHasToExist[scala.Seq[Position]](field.jsPath, field.hasToExist.hasToHave(atLeast1Position))(newPositionRead)
  }

  def createPortfolio(dateFactory: () => LocalDateTime, knewCurrencyPairs: List[CurrencyPair]): Reads[CreatePortfolio] = {
    val validated: Reads[Validated[List[String], CreatePortfolio]] =
      (PortfoliosJson.readIdempotentKey and
        PortfoliosJson.readName and
        positionsReads(dateFactory(), knewCurrencyPairs))((uniqueId, name, positions) => CreatePortfolio.validated(uniqueId, name, positions))

    validated.flatMap {
      case Valid(createPortfolio)   => Reads[CreatePortfolio](_ => JsSuccess(createPortfolio))
      case Invalid(errors: List[_]) => Reads.failed(errors.mkString("[", " - ", "]"))
    }
  }

  def delete(): Reads[NonEmptySet[PortfolioName]] =
    (json: JsValue) => {
      val error = JsError(s"cannot create a NonEmptySet of portfolio names from this json: $json")
      for {
        aSet <-
          Reads
            .seq[PortfolioName](PortfoliosJson.readName)
            .reads(json) match {
            case originalError: JsError => originalError ++ error
            case jss @ JsSuccess(_, _)  => jss
          }
        result <- data.NonEmptySet.fromSet(TreeSet.from(aSet)(PortfolioName.portfolioNameOrdering)) match {
          case Some(portfolioNames) => JsSuccess(portfolioNames)
          case None                 => error
        }
      } yield result
    }
}
