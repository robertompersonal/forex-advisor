package org.binqua.forex.web.core.portfolio

import cats.data.NonEmptySeq
import cats.syntax.option._
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.portfolios.PortfoliosModel.UpdatePortfolio
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel}
import org.binqua.forex.util.{StringUtils, TestingFacilities}
import org.binqua.forex.web.core.portfolio.PortfoliosJson.updatePortfolio
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.scalacheck.{Gen, Shrink}
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import play.api.libs.json.{JsError, _}

import java.time.LocalDateTime
import java.util.UUID

class FromJsonToUpdatePortfolioSpec
    extends AnyPropSpec
    with ScalaCheckPropertyChecks
    with Matchers
    with GivenWhenThen
    with TestingFacilities
    with PortfoliosJsonTestContext {

  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  property("An empty json should not be a valid update because name is missing") {

    info("-----")

    val invalidJsonWithoutUpdates = "{}"
    Given(message = s"An invalid json $invalidJsonWithoutUpdates")

    When(message = "we parse it")
    updatePortfolio(() => LocalDateTime.now(), currencyPairs = List()).reads(Json.parse(invalidJsonWithoutUpdates)) match {
      case JsSuccess(_, _) =>
        thisTestShouldNotHaveArrivedHere
      case JsError(errors) =>
        Then(message = s"we get a valid error $errors")
        errors should be(Seq((JsPath \ "name", Seq(JsonValidationError(Seq("Name field has to exist"))))))
    }

  }

  property("A json should not be valid if portfolio name is not valid") {

    val invalidPortfolioNames = StringUtils.ofLength(lessOrEqualThan = 2, greaterOrEqualThan = 31)

    forAll(invalidPortfolioNames) { invalidPortfolioName =>
      info("-----")

      val validPositions = List(PortfoliosModel.Delete(UUID.randomUUID()))

      val theJson: String = Json.prettyPrint(
        Json.toJson(NewUpdatePortfolioJsonBuilder(UUID.randomUUID().toString.some, invalidPortfolioName, validPositions))(jsonwrites.updatePortfolioWrites)
      )

      Given(message = s"An invalid json $theJson with portfolio name length ${invalidPortfolioName.length}")

      When(message = "we parse it")

      updatePortfolio(() => LocalDateTime.now(), currencyPairs = List()).reads(Json.parse(theJson)) match {
        case JsSuccess(_, _) =>
          thisTestShouldNotHaveArrivedHere
        case JsError(errors) =>
          Then(message = s"we get a valid error ${toListOfSimpleErrors(errors)}")
          toListOfSimpleErrors(errors) should contain("Portfolio name has to be minimum 3 chars and maximum 30 without leading or trailing whitespaces")
      }
    }
  }

  property("To be valid a json has to have also a portfolio name valid") {

    val validPortfolioName = StringUtils.ofLengthBetween(min = 3, max = 30)

    forAll(validPortfolioName) { validPortfolioName =>
      info("-----")

      val validPositions = List(PortfoliosModel.Delete(UUID.randomUUID()))

      val jsonBuilder = NewUpdatePortfolioJsonBuilder(None, validPortfolioName, validPositions)

      val theJson = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))

      Given(message = s"A valid json $theJson with name length ${validPortfolioName.length}")

      When(message = "we parse it")

      updatePortfolio(() => LocalDateTime.now(), currencyPairs = List()).reads(Json.parse(theJson)) match {

        case JsSuccess(actualUpdate, _) =>
          Then(message = s"we get a valid updatePortfolio $actualUpdate")
          actualUpdate should be(PortfoliosModel.UpdatePortfolio.unsafe(None, jsonBuilder.portfolioName, NonEmptySeq.fromSeqUnsafe(jsonBuilder.updates)))
        case JsError(errors) =>
          thisTestShouldNotHaveArrivedHere(s"this json $theJson produce the following errors ${errors.toString()}")
      }
    }
  }

  property("A json should not be a valid if toBeAdded, toBeDeleted and toBeUpdated fields combined have at least 1 entry in total") {
    info("-----")

    val invalidJsonWithoutUpdates =
      Json.prettyPrint(
        Json
          .toJson(NewUpdatePortfolioJsonBuilder(UUID.randomUUID().toString.some, aValidaPortfolioName.name.value, Seq.empty))(jsonwrites.updatePortfolioWrites)
      )

    Given(message = s"An invalid json $invalidJsonWithoutUpdates")

    When(message = "we parse it")

    updatePortfolio(() => LocalDateTime.now(), currencyPairs = List()).reads(Json.parse(invalidJsonWithoutUpdates)) match {

      case JsSuccess(_, _) =>
        thisTestShouldNotHaveArrivedHere
      case JsError(errors) =>
        Then(message = s"we get a valid error ${toListOfSimpleErrors(errors)}")
        toListOfSimpleErrors(errors) should contain("Positions cannot be empty")
    }

  }

  property("A valid newPosition json should be parsed correctly") {

    forAll(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime)) { aValidNewPosition =>
      {
        info("-----")

        val aValidNewPositionJson: String = Json.prettyPrint(Json.toJson(aValidNewPosition)(jsonwrites.newPositionWrites))

        Given(message = s"A valid json $aValidNewPositionJson")

        When(message = "we parse it")
        PortfoliosJson
          .positionReads(aValidNewPosition.recordedDateTime, currencyPairs = CurrencyPair.values)
          .reads(Json.parse(aValidNewPositionJson)) match {
          case JsSuccess(actualNewPosition, _) =>
            Then(message = s"we get a valid updatePortfolio $aValidNewPosition")
            actualNewPosition should be(aValidNewPosition)
          case JsError(errors) =>
            thisTestShouldNotHaveArrivedHere(s"this json $aValidNewPositionJson produce the following errors ${errors.toString()}")
        }
      }

    }
  }

  property("A newPosition json with invalid values should create right errors") {

    info("-----")
    val aValidNewPosition = PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get

    val aValidNewPositionJson: JsObject = Json.toJson(aValidNewPosition)(jsonwrites.newPositionWrites).asInstanceOf[JsObject]

    val wrongAmount = replaceField(aValidNewPositionJson, jsonTags.amount.name -> "a")
    val wrongPair = replaceField(wrongAmount, jsonTags.pair.name -> "a")
    val wrongPositionType = replaceField(wrongPair, jsonTags.positionType.name -> "a")
    val invalidJson = replaceField(wrongPositionType, jsonTags.openDateTime.name -> "a")

    Given(message = s"An invalid json ${Json.prettyPrint(invalidJson)}")

    When(message = "we parse it")

    PortfoliosJson.positionReads(aValidNewPosition.recordedDateTime, currencyPairs = CurrencyPair.values).reads(invalidJson) match {
      case JsSuccess(actualNewPosition, _) =>
        thisTestShouldNotHaveArrivedHere(because =
          s"this json ${Json.prettyPrint(invalidJson)} produce the following actualNewPosition $actualNewPosition but it should not"
        )
      case JsError(errors) =>
        val prettyErrors = toListOfSimpleErrors(errors)

        Then(message = s"we get a correct errors list:\n${prettyErrors.mkString("\n")}")

        prettyErrors should have size 4

        prettyErrors should contain("Amount field has to be a positive integer")
        prettyErrors should contain("OpenDateTime field has to have pattern 'dd mm hh at hh:mm:ss'")
        prettyErrors should contain("PositionType field has to be buy or sell")
        prettyErrors should contain(s"Pair field has to be one of EUR/USD EUR/GBP EUR/JPY GBP/USD GBP/JPY SPX500 USD/JPY US30")
    }

  }

  property("A json should be valid if it has 1 delete") {
    info("-----")

    val jsonBuilder = NewUpdatePortfolioJsonBuilder(None, aValidaPortfolioName.name.value, List(PortfoliosModel.Delete(UUID.randomUUID())))

    val theJson = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))

    Given(message = s"A valid json $theJson")

    When(message = "we parse it")

    assertWeCanParseTheJson(
      recordedDateTime = LocalDateTime.now(),
      theJson = theJson,
      theExpected = PortfoliosModel.UpdatePortfolio.unsafe(None, jsonBuilder.portfolioName, NonEmptySeq.fromSeqUnsafe(jsonBuilder.updates))
    )

  }

  property("A json should be invalid if it has 1 add but not a valid idempotent key") {
    info("-----")

    val toBeAdded = PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get

    val wrongIdempotentKey = "rabish"

    val jsonBuilder = NewUpdatePortfolioJsonBuilder(
      wrongIdempotentKey.some,
      aValidaPortfolioName.name.value,
      List(PortfoliosModel.Add(toBeAdded))
    )

    val theJson: String = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))
    Given(message = s"A valid json $theJson")

    When(message = "we parse it")

    assertParserProduceRightError(
      theJson = theJson,
      jsonReadUnderTest = updatePortfolio(() => recordedDateTime, currencyPairs = CurrencyPair.values),
      expectedError = s"idempotentKey field has to be a valid UUID but its value is $wrongIdempotentKey"
    )

  }

  property("A json should be valid if it has 1 add and an idempotent key") {
    info("-----")

    val toBeAdded = PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get

    val jsonBuilder = NewUpdatePortfolioJsonBuilder(
      UUID.randomUUID().toString.some,
      aValidaPortfolioName.name.value,
      List(PortfoliosModel.Add(toBeAdded))
    )

    val theJson: String = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))
    Given(message = s"A valid json $theJson")

    When(message = "we parse it")

    assertWeCanParseTheJson(
      recordedDateTime = toBeAdded.recordedDateTime,
      theJson = theJson,
      theExpected = PortfoliosModel.UpdatePortfolio
        .unsafe(jsonBuilder.maybeAnIdempotentKey, jsonBuilder.portfolioName, NonEmptySeq.fromSeqUnsafe(jsonBuilder.updates))
    )

  }

  property("A json should be valid if it has 1 valid update") {
    info("-----")

    val toBeUpdated = PortfoliosModel.Update(UUID.randomUUID(), PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)

    val jsonBuilder = NewUpdatePortfolioJsonBuilder(None, name = aValidaPortfolioName.name.value, updates = List(toBeUpdated))

    val theJson = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))

    Given(message = s"A valid json $theJson")

    When(message = "we parse it")

    assertWeCanParseTheJson(
      recordedDateTime = toBeUpdated.thePositionData.recordedDateTime,
      theJson = theJson,
      theExpected = PortfoliosModel.UpdatePortfolio.unsafe(None, jsonBuilder.portfolioName, NonEmptySeq.fromSeqUnsafe(jsonBuilder.updates))
    )

  }

  property("A json should be valid if it has 1 add 1 delete and 1 update and an idempotent key") {
    info("-----")

    forAll(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime)) { newPosition =>
      {

        val jsonBuilder = NewUpdatePortfolioJsonBuilder(
          UUID.randomUUID().toString.some,
          aValidaPortfolioName.name.value,
          List(
            PortfoliosModel.Add(newPosition),
            PortfoliosModel.Delete(UUID.randomUUID()),
            PortfoliosModel.Update(UUID.randomUUID(), PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)
          )
        )

        val theJson = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))
        Given(message = s"A valid json $theJson")

        When(message = "we parse it")

        assertWeCanParseTheJson(
          recordedDateTime = recordedDateTime,
          theJson = theJson,
          theExpected =
            PortfoliosModel.UpdatePortfolio.unsafe(jsonBuilder.maybeAnIdempotentKey, jsonBuilder.portfolioName, NonEmptySeq.fromSeqUnsafe(jsonBuilder.updates))
        )
      }
    }
  }

  property("A json should be invalid if it has deletes and updates and idempotent key") {

    val zeroOrMoreDeletes: Gen[List[PortfoliosModel.PortfolioSingleUpdate]] =
      for {
        numberOfPositions <- Gen.chooseNum(0, 4)
        deletes <- Gen.listOfN(numberOfPositions, Gen.uuid.map(PortfoliosModel.Delete))
      } yield deletes

    val updateGen: Gen[PortfoliosModel.Update] = for {
      uuid <- Gen.uuid
      position <- PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime)
    } yield PortfoliosModel.Update(uuid, position)

    val zeroOrMoreUpdates: Gen[List[PortfoliosModel.PortfolioSingleUpdate]] = for {
      numberOfPositions <- Gen.chooseNum(0, 4)
      result <- Gen.listOfN(numberOfPositions, updateGen)
    } yield result

    info("-----")

    forAll(zeroOrMoreDeletes, zeroOrMoreUpdates) { (zeroOrMoreDeletes, zeroOrMoreUpdates) =>
      {
        whenever((zeroOrMoreUpdates ::: zeroOrMoreDeletes).nonEmpty) {
          val jsonBuilder = NewUpdatePortfolioJsonBuilder(
            UUID.randomUUID().toString.some,
            aValidaPortfolioName.name.value,
            zeroOrMoreUpdates ::: zeroOrMoreDeletes
          )

          val theJson = Json.prettyPrint(Json.toJson(jsonBuilder)(jsonwrites.updatePortfolioWrites))
          Given(message = s"An invalid json $theJson")

          When(message = "we parse it")

          assertParserProduceRightError(
            theJson = theJson,
            jsonReadUnderTest = updatePortfolio(() => recordedDateTime, currencyPairs = CurrencyPair.values),
            expectedError = s"At least one add update has to be present if idempotent key is present"
          )
        }

      }
    }
  }

  property("toBeAdded toBeDeleted and toBeUpdated have to have the right format") {

    info("-----")

    val aValidJsonBuilder = NewUpdatePortfolioJsonBuilder(
      UUID.randomUUID().toString.some,
      aValidaPortfolioName.name.value,
      List(
        PortfoliosModel.Add(PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get),
        PortfoliosModel.Delete(UUID.randomUUID()),
        PortfoliosModel.Update(UUID.randomUUID(), PortfoliosGen.newPositionWithMicroLotWithRecordedDateTime(recordedDateTime).sample.get)
      )
    )

    val theFinalInvalidJson = {
      val aValidJsonToBeTransformed: JsObject = Json.toJson(aValidJsonBuilder)(jsonwrites.updatePortfolioWrites).asInstanceOf[JsObject]
      val jsonWithWrongToBeAdded: JsObject = transformWith(aValidJsonToBeTransformed, jsonTags.toBeAdded.path.json.update(jsonwrites.addAStringAsArrayElement))

      val jsonWithWrongToBeDeleted: JsObject = transformWith(jsonWithWrongToBeAdded, jsonTags.toBeDeleted.json.update(jsonwrites.addAStringAsArrayElement))

      val jsonWithWrongToUpdate = transformWith(jsonWithWrongToBeDeleted, jsonTags.toBeUpdated.json.update(jsonwrites.addAStringAsArrayElement))

      Json.prettyPrint(
        transformWith(
          jsonWithWrongToUpdate,
          jsonTags.toBeUpdated.json.update(
            jsonwrites.addAnArrayElement(JsObject(Map(jsonTags.positionId.name -> JsString("1"), jsonTags.position.name -> JsObject(Seq()))))
          )
        )
      )
    }

    Given(message = s"A wrong json $theFinalInvalidJson")

    When(message = "we parse it")

    updatePortfolio(() => recordedDateTime, currencyPairs = CurrencyPair.values).reads(Json.parse(theFinalInvalidJson)) match {

      case JsSuccess(actualNewPosition, _) =>
        thisTestShouldNotHaveArrivedHere(because =
          s"this json $theFinalInvalidJson produce the following actualNewPosition $actualNewPosition but it should not"
        )

      case JsError(errors) =>
        val prettyErrors = toMapOfSingleErrors(errors)

        Then(message = s"we get a correct errors list:\n${prettyErrors.mkString("\n")}")

        prettyErrors should contain(jsonTags.toBeAdded.path(1) \ jsonTags.amount.name -> fieldsError.amount)
        prettyErrors should contain(jsonTags.toBeAdded.path(1) \ jsonTags.openDateTime.name -> fieldsError.openDateTime)
        prettyErrors should contain(jsonTags.toBeAdded.path(1) \ jsonTags.pair.name -> fieldsError.pairs)
        prettyErrors should contain(jsonTags.toBeAdded.path(1) \ jsonTags.positionType.name -> fieldsError.positionTypeMissing)
        prettyErrors should contain(jsonTags.toBeAdded.path(1) \ jsonTags.price.name -> fieldsError.price)

        prettyErrors should contain(jsonTags.toBeDeleted(1) \ jsonTags.positionId.name -> fieldsError.positionIdMissing)

        prettyErrors should contain(jsonTags.toBeUpdated(1) \ jsonTags.positionId.name -> fieldsError.positionIdMissing)

        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.positionId.name -> fieldsError.positionIdInvalid)

        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.position.name \ jsonTags.amount.name -> fieldsError.amount)
        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.position.name \ jsonTags.openDateTime.name -> fieldsError.openDateTime)
        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.position.name \ jsonTags.pair.name -> fieldsError.pairs)
        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.position.name \ jsonTags.positionType.name -> fieldsError.positionTypeMissing)
        prettyErrors should contain(jsonTags.toBeUpdated(2) \ jsonTags.position.name \ jsonTags.price.name -> fieldsError.price)

    }

  }

  property("A json should be invalid if positions are invalid") {
    info("-----")

    val ids = Vector.fill(2)(aPositionId).toList

    val theJson = Json.prettyPrint(
      Json.toJson(NewUpdatePortfolioJsonBuilder(UUID.randomUUID().toString.some, aValidaPortfolioName.name.value, ids.map(PortfoliosModel.Delete)))(
        jsonwrites.updatePortfolioWrites
      )
    )

    Given(message = s"An invalid json $theJson")

    When(message = "we parse it")

    assertParserProduceRightError(
      theJson = theJson,
      jsonReadUnderTest = updatePortfolio(() => recordedDateTime, currencyPairs = CurrencyPair.values),
      expectedError = s"In all position ids to be deleted $ids there are duplicated entries"
    )
  }

  private def assertParserProduceRightError(theJson: String, jsonReadUnderTest: Reads[PortfoliosModel.UpdatePortfolio], expectedError: String): Any = {
    jsonReadUnderTest.reads(Json.parse(theJson)) match {
      case JsSuccess(actualUpdate, _) =>
        thisTestShouldNotHaveArrivedHere(because = s"The json $theJson is invalid and parse should fail but instead produce the following update $actualUpdate")
      case JsError(errors) =>
        Then(message = s"we get a valid error list ${toListOfSimpleErrors(errors)} containing the right error $errors")
        toListOfSimpleErrors(errors) should contain(s"$expectedError")
    }
  }

  private def assertWeCanParseTheJson(recordedDateTime: LocalDateTime, theJson: String, theExpected: => UpdatePortfolio): Any = {

    updatePortfolio(() => recordedDateTime, currencyPairs = CurrencyPair.values).reads(Json.parse(theJson)) match {

      case JsSuccess(actualUpdate, _) =>
        Then(message = s"we get a valid updatePortfolio $theExpected")
        actualUpdate should be(theExpected)
      case JsError(errors) =>
        thisTestShouldNotHaveArrivedHere(s"this json:\n$theJson\nproduce the following errors\n${errors.toString()}")

    }
  }

}
