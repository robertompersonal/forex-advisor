package org.binqua.forex.web.core.portfolio

import cats.data
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.TestingFacilities.because
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.scalactic.anyvals.NonEmptySet
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.{be, _}
import play.api.libs.json.Json._
import play.api.libs.json.{JsError, _}

class FromJsonToDeletePortfoliosSpec extends AnyFlatSpec with TestingFacilities with GivenWhenThen {

  "deletePortfolios with an empty json" should "produce right error" in new TestContext() {
    thisJsonIsNotCorrect(parse(input = "{}"))
      .andThisProveIt(prettyErrors =>
        prettyErrors should contain(JsPath -> List("error.expected.jsarray", "cannot create a NonEmptySet of portfolio names from this json: {}"))
      )
  }

  "deletePortfolios with a wrong json" should "produce right error" in new TestContext() {
    thisJsonIsNotCorrect(parse(input = """ { "x": 1 } """))
      .andThisProveIt(prettyErrors =>
        prettyErrors should contain(JsPath -> List("error.expected.jsarray", """cannot create a NonEmptySet of portfolio names from this json: {"x":1}"""))
      )
  }

  "a deletePortfolios json with 2 portfolios" should "produce right value" in new TestContext() {
    thisJsonIsCorrect(parse(input = """ [ {"name": "portofolioB"}, {"name": "portofolioA"}  ] """))
      .andThisProveIt(parsed => parsed should be(NonEmptySet(PortfolioName(refineMV("portofolioA")), PortfolioName(refineMV("portofolioB"))).toSet))
  }

  "a deletePortfolios json with 1 portfolio" should "produce right value" in new TestContext() {
    thisJsonIsCorrect(parse(input = """ [ {"name": "portofolioB"} ] """))
      .andThisProveIt(parsed => parsed should be(NonEmptySet(PortfolioName(refineMV("portofolioB"))).toSet))
  }

  "a deletePortfolios json with 1 invalid portfolio name" should "produce right error" in new TestContext() {
    private val theWrongJson: JsValue = Json.parse(input = """[ {"name": "abcd"}, {"name": "ab"} ]""")
    thisJsonIsNotCorrect(theWrongJson)
      .andThisProveIt { prettyErrors =>
        prettyErrors should contain(JsPath(1) -> List("Portfolio name has to be minimum 3 chars and maximum 30 without leading or trailing whitespaces"))
        prettyErrors should contain(JsPath -> List(s"""cannot create a NonEmptySet of portfolio names from this json: $theWrongJson"""))
        prettyErrors.size should be(2)
      }
  }

  "deletePortfoliosWrites" should "create the right json" in new TestContext() {
    private val portfolioNames: data.NonEmptySet[PortfolioName] = data.NonEmptySet.of(
      PortfolioName(refineMV("portofolioB")),
      PortfolioName(refineMV("portofolioC")),
      PortfolioName(refineMV("portofolioA"))
    )(PortfolioName.portfolioNameOrder)

    thisJsonIsCorrect(Json.toJson(portfolioNames)(jsonwrites.deletePortfoliosWrites))
      .andThisProveIt(parsed => parsed should be(portfolioNames))
  }

  case class TestContext() extends PortfoliosJsonTestContext {

    info("new test")

    implicit val underTest: Reads[data.NonEmptySet[PortfolioName]] = PortfoliosJson.delete()

    trait IProveItRight {
      def andThisProveIt(f: Any => Any)
    }

    trait IProveItWrong {
      def andThisProveIt(f: Map[JsPath, collection.Seq[String]] => Any)
    }

    def thisJsonIsNotCorrect(theWrongJson: JsValue)(implicit underTest: Reads[data.NonEmptySet[PortfolioName]]): IProveItWrong = {
      Given(s"A wrong json:\n${Json.prettyPrint(theWrongJson)}")
      When(s"we parse it")
      underTest.reads(theWrongJson) match {
        case JsError(errors) =>
          val prettyErrors: Map[JsPath, collection.Seq[String]] = toMapOfSeqOfErrors(errors)

          Then(message = s"we get a correct errors list:\n${prettyErrors.mkString("\n")}")
          (f: Map[JsPath, collection.Seq[String]] => Any) => f(prettyErrors)

        case JsSuccess(someData, _) => thisIsABug(theWrongJson, someData)
      }
    }

    def thisJsonIsCorrect(theCorrectJson: JsValue)(implicit underTest: Reads[data.NonEmptySet[PortfolioName]]): IProveItRight = {
      Given(s"A correct json:\n${Json.prettyPrint(theCorrectJson)}")
      When(s"we parse it")
      underTest.reads(theCorrectJson) match {
        case JsError(errors) =>
          thisTestShouldNotHaveArrivedHere(because(s"the json $theCorrectJson correct but I got $errors"))
        case JsSuccess(someData, _) =>
          (f: Any => Any) => {
            Then(message = s"we get valid data :\n$someData")
            f(someData)
          }
      }
    }

    def thisIsABug(theJson: JsValue, someData: Any): Nothing =
      thisTestShouldNotHaveArrivedHere(because =
        s"json:\n${prettyPrint(theJson)}\nis invalid and should not produce a correct object but it produced a:\n$someData"
      )

  }

}
