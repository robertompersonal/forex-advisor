package org.binqua.forex.running.external

import cats.implicits._
import com.typesafe.config.{Config => AkkaConfig}
import org.binqua.forex.advisor.newportfolios
import org.binqua.forex.feed.httpclient
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service._
import org.binqua.forex.util.core.ConfigValidator

object ClientsToExternalSystemConfigValidator {

  def validate(
      akkaConfig: AkkaConfig,
      socketIOConfigConfigValidator: ConfigValidator[socketioclient.Config],
      httpClientSubscriberConfigValidator: ConfigValidator[httpclient.Config],
      cassandraHealthCheckConfigValidator: ConfigValidator[healthcheck.Config],
      portfoliosConfigValidator: ConfigValidator[newportfolios.Config]
  ): Either[String, Boolean] = {
    (
      socketIOConfigConfigValidator(akkaConfig),
      httpClientSubscriberConfigValidator(akkaConfig),
      cassandraHealthCheckConfigValidator(akkaConfig),
      portfoliosConfigValidator(akkaConfig)
    ).mapN((_, _, _, _) => true)
      .leftMap(_.mkString(" - "))
      .toEither

  }

}
