package org.binqua.forex.running.external

import akka.NotUsed
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.management.scaladsl.AkkaManagement
import org.binqua.forex.advisor._
import org.binqua.forex.feed.httpclient.ShardedHttpClientSubscriberModule
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.running.common.MainBehavior

trait ClientsToExternalSystemsBehavior extends MainBehavior[NotUsed] {

  this: ShardedHttpClientSubscriberModule with connectionregistry.ShardedModule with newportfolios.PortfoliosShardingModule =>

  override def mainBehavior(): Behavior[NotUsed] =
    Behaviors.setup[NotUsed] { context =>
      def setUp(sharding: ClusterSharding): Behavior[NotUsed] = {

        initialisedHttpSubscriber(sharding)

        initialiseConnectionRegistry(sharding)

        initialisePortfoliosSharding(sharding)

        Behaviors.ignore

      }

      AkkaManagement(context.system).start()

      setUp(ClusterSharding(context.system))
    }

}
