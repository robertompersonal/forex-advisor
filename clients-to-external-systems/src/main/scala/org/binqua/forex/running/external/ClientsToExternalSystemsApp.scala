package org.binqua.forex.running.external

import akka.NotUsed
import org.binqua.forex.advisor.newportfolios.NewPortfoliosShardingProductionModule
import org.binqua.forex.feed.httpclient.ProductionShardedModule
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.running.common.{ActorSystemCreator, ActorSystemSpawner}

object ClientsToExternalSystemsApp
    extends ActorSystemCreator[NotUsed]
    with ActorSystemSpawner[NotUsed]
    with ProductionShardedModule
    with connectionregistry.ProductionShardedModule
    with CommonIOClientsToExternalSystemConfigValidator
    with NewPortfoliosShardingProductionModule
    with ClientsToExternalSystemsBehavior {

  def main(args: Array[String]): Unit = runApplicationWith(args)

}
