package org.binqua.forex.running.external

import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.advisor.newportfolios
import org.binqua.forex.feed._
import org.binqua.forex.feed.socketio.connectionregistry.healthcheck
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient
import org.binqua.forex.running.common.{AppConfigValidator, CommonIoRunnerParameters}

trait CommonIOClientsToExternalSystemConfigValidator extends AppConfigValidator {

  override def buildConfiguration(args: Array[String]): Either[String, Config] = {

    val runnerParameters = new CommonIoRunnerParameters(args)

    for {
      configFileName <- runnerParameters.configFileName
      actorSystemConfig <- {
        System.setProperty("config.file", configFileName)
        ConfigFactory.invalidateCaches()
        Right(ConfigFactory.load())
      }
      _ <- ClientsToExternalSystemConfigValidator.validate(
        actorSystemConfig,
        socketioclient.ConfigValidator,
        httpclient.ConfigValidator,
        healthcheck.Config.Validator,
        newportfolios.Config.Validator
      )
    } yield actorSystemConfig

  }

}
