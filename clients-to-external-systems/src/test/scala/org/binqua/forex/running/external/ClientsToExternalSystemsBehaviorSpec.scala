package org.binqua.forex.running.external

import akka.NotUsed
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorSystem, Behavior}
import akka.cluster.MemberStatus
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.typed.{Cluster, Join}
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse}
import akka.http.scaladsl.{Http, HttpExt}
import akka.persistence.typed.PersistenceId
import akka.stream.Materializer
import akka.util.ByteString
import com.typesafe.config.{Config, ConfigFactory}
import eu.timepit.refined.refineMV
import org.binqua.forex.advisor.model.CurrencyPair
import org.binqua.forex.advisor.newportfolios.{DeliveryGuaranteedPortfolios, PortfoliosShardingInitializationModuleWithoutBehavior}
import org.binqua.forex.advisor.portfolios.service.UUIDMessageId
import org.binqua.forex.feed.httpclient.{HttpClientSubscriberModule, HttpClientSubscriberProtocol, ShardedHttpClientSubscriberModuleImpl}
import org.binqua.forex.feed.socketio.connectionregistry
import org.binqua.forex.feed.socketio.connectionregistry.persistence.Persistence.Response
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient.SocketId
import org.binqua.forex.feed.starter.SubscriptionStarterProtocol
import org.binqua.forex.util.{AkkaTestingFacilities, Util}
import org.scalatest.wordspec.AnyWordSpecLike

import java.util.UUID
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

object Configurations {

  val system2HttpPort: Int = Util.findRandomOpenPortOnAllLocalInterfaces()
  val system3HttpPort: Int = Util.findRandomOpenPortOnAllLocalInterfaces()

  val baseConfig: Config = ConfigFactory.parseString({
    s"""
       |akka {
       |  loglevel = "DEBUG"
       |  actor {
       |    provider = cluster
       |    serializers {
       |      jackson-cbor = "akka.serialization.jackson.JacksonJsonSerializer"
       |    }
       |    serialization-bindings {
       |      "org.binqua.forex.JsonSerializable" = jackson-cbor
       |    }
       |  }
       |  cluster {
       |     jmx.multi-mbeans-in-same-jvm = on
       |     role {
       |            httpClientSubscriber.min-nr-of-members = 2
       |            connectionRegistry.min-nr-of-members = 2
       |            portfolios.min-nr-of-members = 2
       |          }
       |  }
       | remote {
       |   artery {
       |     enabled = on
       |     transport = tcp
       |     canonical.hostname = "127.0.0.1"
       |     canonical.port = 0
       |   }
       | }
       |}
    """.stripMargin
  })

  val configWithRolesInfo = ConfigFactory.parseString(s"""
       |akka {
       |   cluster {
       |     roles = ["httpClientSubscriber","connectionRegistry","portfolios"]
       |  }
       |}
    """.stripMargin)

  def httpManagementConfigPart(port: Int): Config =
    ConfigFactory.parseString({
      s"""
         |akka {
         |   management {
         |    http {
         |      hostname = localhost
         |      port = $port
         |      route-providers-read-only = false
         |    }
         |  }
         |}
    """.stripMargin
    })
}

class ClientsToExternalSystemsBehaviorSpec extends ScalaTestWithActorTestKit(Configurations.baseConfig) with AnyWordSpecLike with AkkaTestingFacilities {

  val expectedPortfoliosMessageId: UUIDMessageId = UUIDMessageId(UUID.randomUUID())

  trait StubbedCollaborators
      extends ShardedHttpClientSubscriberModuleImpl
      with HttpClientSubscriberModule
      with connectionregistry.ShardedModuleImpl
      with PortfoliosShardingInitializationModuleWithoutBehavior
      with connectionregistry.Module
      with org.binqua.forex.advisor.newportfolios.PortfoliosBehaviorModule {

    override def httpClientSubscriber(): Behavior[HttpClientSubscriberProtocol.SubscribeTo] =
      Behaviors.monitor(theHttpClientSubscriberProbe.ref, Behaviors.ignore)

    override def connectionRegistryBehavior(): Behavior[connectionregistry.Manager.Command] =
      Behaviors.monitor(theConnectionRegistryProbe.ref, Behaviors.ignore)

    override def portfoliosBehavior(persistenceId: PersistenceId): Behavior[DeliveryGuaranteedPortfolios.ExternalCommand] =
      Behaviors.monitor(portfoliosProbe.ref, Behaviors.ignore)
  }

  val theConnectionRegistryProbe = createTestProbe[connectionregistry.Manager.Command]()

  val theHttpClientSubscriberProbe = createTestProbe[HttpClientSubscriberProtocol.SubscribeTo]()

  val portfoliosProbe: TestProbe[DeliveryGuaranteedPortfolios.ExternalCommand] = createTestProbe[DeliveryGuaranteedPortfolios.ExternalCommand]()

  val behaviourUnderTest = new ClientsToExternalSystemsBehavior with StubbedCollaborators {}

  val actorSystem2WithRolesToHostAllShardedInstances: ActorSystem[NotUsed] = ActorSystem(
    behaviourUnderTest.mainBehavior(),
    name = system.name,
    config = Configurations.configWithRolesInfo.withFallback(
      Configurations.httpManagementConfigPart(Configurations.system2HttpPort).withFallback(system.settings.config)
    )
  )

  val actorSystem3WithRolesToHostAllShardedInstances: ActorSystem[NotUsed] = ActorSystem(
    behaviourUnderTest.mainBehavior(),
    name = system.name,
    config = Configurations.configWithRolesInfo.withFallback(
      Configurations.httpManagementConfigPart(Configurations.system3HttpPort).withFallback(system.settings.config)
    )
  )

  object ISendMessagesToTheBehaviorUnderTest extends StubbedCollaborators {

    def apply(): Behavior[NotUsed] =
      Behaviors.setup[NotUsed] { context: ActorContext[NotUsed] =>
        initialisedHttpSubscriber(ClusterSharding(context.system)).ref ! createASubscribeToMessage(context)

        initialiseConnectionRegistry(ClusterSharding(context.system)).ref ! createARegisterMessage(context)

        initialisePortfoliosSharding(ClusterSharding(context.system)).ref(entityId = refineMV("ref123")) ! DeliveryGuaranteedPortfolios.ExternalCommand(
          expectedPortfoliosMessageId,
          DeliveryGuaranteedPortfolios.ShowPortfoliosPayload,
          context.messageAdapter[DeliveryGuaranteedPortfolios.Response](_ => NotUsed)
        )

        Behaviors.ignore
      }

    private def createARegisterMessage(context: ActorContext[NotUsed]): connectionregistry.Manager.Command =
      connectionregistry.Manager.Register(
        registrationRequester = context.spawn(Behaviors.ignore[Response], "registrationRequester"),
        context.spawn(Behaviors.ignore[SubscriptionStarterProtocol.NewSocketId], "subscriptionStarter"),
        requesterAlias = "subscriptionStarter"
      )

    private def createASubscribeToMessage(context: ActorContext[NotUsed]): HttpClientSubscriberProtocol.SubscribeTo =
      HttpClientSubscriberProtocol.SubscribeTo(
        CurrencyPair.EurUsd,
        SocketId("123"),
        context.spawn(Behaviors.ignore[HttpClientSubscriberProtocol.Response], "replyToWhenHttpSubscribed")
      )

  }

  val actorSystemToSendMessagesToShardedInstances: ActorSystem[NotUsed] =
    ActorSystem(ISendMessagesToTheBehaviorUnderTest(), name = system.name, config = Configurations.baseConfig)

  "2 Actor systems with role httpClientSubscriber, connectionRegistry and portfolios" should {

    "create a cluster with other 2 actor systems" in {

      val clusterMembers = List(
        system,
        actorSystem2WithRolesToHostAllShardedInstances,
        actorSystem3WithRolesToHostAllShardedInstances,
        actorSystemToSendMessagesToShardedInstances
      )

      clusterMembers.foreach(Cluster(_).manager ! Join(Cluster(system).selfMember.address))

      clusterMembers.foreach((as: ActorSystem[Nothing]) => {
        eventually {
          Cluster(as).state.members.unsorted.map(_.status) should ===(Set[MemberStatus](MemberStatus.Up))
          Cluster(as).state.members.size should ===(4)
        }
      })
    }

    "and they should be wired properly because they received messages sent by the testClient to sharded instances" in {

      theHttpClientSubscriberProbe.expectMessageType[HttpClientSubscriberProtocol.SubscribeTo]

      theConnectionRegistryProbe.expectMessageType[connectionregistry.Manager.Command]

      val actualPortfoliosCommand = portfoliosProbe.expectMessageType[DeliveryGuaranteedPortfolios.ExternalCommand]

      actualPortfoliosCommand.messageId should be(expectedPortfoliosMessageId)

    }

    "and AkkaManagement should be started" in {
      import akka.actor.typed.scaladsl.adapter._

      implicit val mater: Materializer = Materializer.matFromSystem(system.toClassic)

      implicit val ec: ExecutionContextExecutor = system.executionContext

      val http: HttpExt = Http(system.toClassic)

      val url =
        s"http://${actorSystem2WithRolesToHostAllShardedInstances.settings.config.getString("akka.management.http.hostname")}:${Configurations.system2HttpPort}/cluster/members"

      val eventuallyAnHttpResponse: Future[HttpResponse] = http.singleRequest(HttpRequest(HttpMethods.GET, url))

      val eventualAHttpResponseBody: Future[String] = for {
        response <- eventuallyAnHttpResponse
        body <- response.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      } yield body

      Await.result(eventualAHttpResponseBody, 5.second) should include(s"akka://${this.getClass.getSimpleName}@127.0.0.1")

      http.shutdownAllConnectionPools()

    }
  }

  override def afterAll(): Unit = {
    ActorTestKit.shutdown(actorSystemToSendMessagesToShardedInstances, 15.seconds)
    ActorTestKit.shutdown(actorSystem2WithRolesToHostAllShardedInstances, 15.seconds)
    ActorTestKit.shutdown(actorSystem3WithRolesToHostAllShardedInstances, 15.seconds)
    super.afterAll()
  }

}
