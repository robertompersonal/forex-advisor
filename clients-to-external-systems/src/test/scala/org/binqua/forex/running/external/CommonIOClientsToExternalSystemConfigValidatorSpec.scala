package org.binqua.forex.running.external

import com.typesafe.config.ConfigFactory
import org.binqua.forex.util.TestingFacilities
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers._
import org.scalatest.wordspec.AnyWordSpec

class CommonIOClientsToExternalSystemConfigValidatorSpec extends AnyWordSpec with BeforeAndAfterEach with TestingFacilities {

  val anInvalidConfigFile = "./src/test/resources/anInvalidExternalSystemClientsApp.conf"

  val aValidConfigFile = "./src/test/resources/aValidExternalSystemClientsApp.conf"

  "CommonIOExternalSystemClientsConfigBuilder" should {
    "create right configuration for a valid file" in {

      val actualValue = new CommonIOClientsToExternalSystemConfigValidator {}.buildConfiguration(Array("-configFileName=" + aValidConfigFile))
      actualValue shouldBe Right(ConfigFactory.load("aValidExternalSystemClientsApp"))

    }

    "clear config library cache between configuration reading" in {

      for {
        config1 <- new CommonIOClientsToExternalSystemConfigValidator {}.buildConfiguration(Array("-configFileName=" + aValidConfigFile))
        config2 <- new CommonIOClientsToExternalSystemConfigValidator {}.buildConfiguration(Array("-configFileName=" + anInvalidConfigFile))
      } yield config1 should (not be config2)

    }

    "create right error message for an invalid file" in {

      val actualValue = new CommonIOClientsToExternalSystemConfigValidator {}.buildConfiguration(Array("-configFileName=" + anInvalidConfigFile))

      actualValue.isLeft shouldBe true

      val actualError = actualValue.swap.getOrElse(thisTestShouldNotHaveArrivedHere)

      actualError should include("Configuration for SocketIOClient is invalid.")
      actualError should include("Configuration for HttpClientSubscriber is invalid.")
      actualError should include("Cassandra health check configuration is invalid.")
      actualError should include("No all configuration values have been found:")
      actualError should include("Configuration for Portfolios is invalid:")

    }

  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      System.clearProperty("config.file")
    }
  }

}
