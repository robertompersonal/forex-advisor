package org.binqua.forex.running.external

import cats.data.Validated.{Invalid, Valid}
import com.typesafe.config.{Config, ConfigFactory}
import org.binqua.forex.advisor.newportfolios
import org.binqua.forex.feed.httpclient
import org.binqua.forex.feed.socketio.connectionregistry._
import org.binqua.forex.feed.socketio.connectionregistry.persistence.connector.service.socketioclient
import org.binqua.forex.util.TestingFacilities
import org.binqua.forex.util.core.MakeItUnsafe
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.concurrent.duration._

class ClientsToExternalSystemConfigValidatorSpec extends AnyFlatSpec with TestingFacilities {

  private val akkaDummyConfiguration: Config = ConfigFactory.load(ConfigFactory.parseString("{ a = 1 }"))

  "given configs validator are happy with their config, whole configuration" should "be valid" in {

    ClientsToExternalSystemConfigValidator
      .validate(
        akkaDummyConfiguration,
        socketIOConfigConfigValidator = _ => socketioclient.Config.validated(connectionUrl = "a", loginToken = "b", retryToConnectInterval = 2.seconds),
        httpClientSubscriberConfigValidator = _ => httpclient.Config.validated(apiHost = "a", apiPort = "b", connectionUrl = "c", loginToken = "d"),
        cassandraHealthCheckConfigValidator =
          _ => Valid(healthcheck.Config.validated(cassandraHost = "localhost", cassandraPort = 1234, localDataCenter = "dc", retryDelay = 11.seconds)).unsafe,
        portfoliosConfigValidator = _ => Valid(newportfolios.Config.validated(maxNumberOfPortfolios = 1, maxNumberOfPositions = 2, 24.hours).unsafe)
      )
      .getOrElse(thisTestShouldNotHaveArrivedHere) shouldBe true
  }

  "given configs reader are unhappy with their config, whole configuration" should "be invalid" in {
    val maxNumberOfPortfoliosMissing = "maxNumberOfPortfolios missing"
    val maxNumberOfPositionsMissing = "maxNumberOfPositions missing"
    val cassandraConfigProblems = "cassandra config problems"

    ClientsToExternalSystemConfigValidator
      .validate(
        akkaDummyConfiguration,
        socketIOConfigConfigValidator = _ => socketioclient.Config.validated(connectionUrl = "", loginToken = "b", retryToConnectInterval = 2.seconds),
        httpClientSubscriberConfigValidator = _ => httpclient.Config.validated(apiHost = "", apiPort = "b", connectionUrl = "c", loginToken = "d"),
        cassandraHealthCheckConfigValidator = _ => Invalid(List(cassandraConfigProblems)),
        portfoliosConfigValidator = _ => Invalid(List(maxNumberOfPortfoliosMissing, maxNumberOfPositionsMissing))
      )
      .swap
      .getOrElse(
        thisTestShouldNotHaveArrivedHere
      ) shouldBe s"connectionUrl has to be not empty - apiHost has to be not empty - $cassandraConfigProblems - $maxNumberOfPortfoliosMissing - $maxNumberOfPositionsMissing"
  }

}
