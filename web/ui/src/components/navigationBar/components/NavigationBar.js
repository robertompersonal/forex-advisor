import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import React from "react";

export const NavigationBar = (props) => {
    return (
        <Navbar bg="light" variant="light" >
            <Nav className="mr-auto" onSelect={selectedKey => props.showPortfolioEditor()}>
                <Nav.Link eventKey="create Port">Create Portfolio</Nav.Link>
            </Nav>
        </Navbar>
    )
}

