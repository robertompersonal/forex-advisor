import React from "react";
import {Button} from 'reactstrap';
import isEqual from 'lodash.isequal'

const SaveButton = (props) => {

    return (
        <Button className="m-1"
                color="primary"
                onClick={props.onClick}
                disabled={isEqual(props.touched, {}) || (!isEqual(props.touched, {}) && !props.isValid)}>
            {SaveButtonLabel}
        </Button>
    )
}

export const SaveButtonLabel = "Save"

export default SaveButton
