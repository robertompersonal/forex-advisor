import React from "react";
import {Button} from 'reactstrap';

const DeleteRowsButton = (props) => {

    return (
        <Button className="m-1" color="primary" onClick={props.onClick} disabled={props.disabled}>
            {DeleteRowsButtonLabel}
        </Button>
    )

}

export const DeleteRowsButtonLabel = 'Delete Rows'

export default DeleteRowsButton
