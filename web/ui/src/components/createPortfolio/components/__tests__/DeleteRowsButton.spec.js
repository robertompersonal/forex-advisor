import React from 'react';
import DeleteRowsButton, {DeleteRowsButtonLabel} from "../DeleteRowsButton";
import {fireEvent, render} from '@testing-library/react'

import '@testing-library/jest-dom/extend-expect'

describe('DeleteRowsButton', () => {

    const enabledButtonPropsWith = f => ({onClick: f, disabled: false})

    const enabledButtonProps = enabledButtonPropsWith(jest.fn())

    const deleteRowsButtonWithProps = (props) => <DeleteRowsButton {...props}/>

    const elementInWithText = label => rendered => {
        const {getByText} = rendered;
        return getByText(label)
    }

    const deleteRowsButtonIn = rendered => elementInWithText(DeleteRowsButtonLabel)(rendered)

    it("can be rendered and has text 'Delete Rows' ;-)", () => {

        const rendered = render(deleteRowsButtonWithProps(enabledButtonProps));

        const expectedLabel = 'Delete Rows'

        expect(elementInWithText(expectedLabel)(rendered)).toBeVisible();

        expect(DeleteRowsButtonLabel).toBe(expectedLabel);

    })

    it("can be disabled", () => {

        const rendered = render(deleteRowsButtonWithProps(enabledButtonPropsWith))

        expect(deleteRowsButtonIn(rendered)).not.toBeDisabled()

        const {rerender} = rendered

        rerender(deleteRowsButtonWithProps({
            ...enabledButtonPropsWith,
            disabled: true
        }))

        expect(deleteRowsButtonIn(rendered)).toBeDisabled()

    })


    it("onclick prop works", () => {

        const f = jest.fn()

        const rendered = render(deleteRowsButtonWithProps({disabled: false, onClick: f}));

        fireEvent.click(deleteRowsButtonIn(rendered))

        expect(f.mock.calls.length).toBe(1);

    })


})
