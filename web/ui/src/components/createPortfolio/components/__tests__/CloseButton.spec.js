import React from 'react';
import {fireEvent, render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import CloseButton, {CloseButtonLabel} from "../CloseButton";

describe('CloseButton', () => {

    const buttonUnderTestWithProps = (f) => <CloseButton onClick={f}/>

    const elementInWithText = label => rendered => {
        const {getByText} = rendered;
        return getByText(label)
    }

    const closeButtonIn = rendered => elementInWithText(CloseButtonLabel)(rendered)

    it("can be rendered and has text 'Close' ;-)", () => {

        const rendered = render(buttonUnderTestWithProps(jest.fn()));

        const expectedLabel = 'Close'

        expect(elementInWithText(expectedLabel)(rendered)).toBeVisible();

        expect(CloseButtonLabel).toBe(expectedLabel);

    })

    it("onClick prop works", () => {

        const f = jest.fn()

        const rendered = render(buttonUnderTestWithProps(f));

        fireEvent.click(closeButtonIn(rendered))

        expect(f.mock.calls.length).toBe(1);

    })

})
