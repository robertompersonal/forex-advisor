import React from 'react';
import ConnectedCreatePortfolio from "../CreatePortfolio";
import '@testing-library/jest-dom/extend-expect'
import {fireEvent, waitFor, within} from '@testing-library/react'
import {createANewStore} from "../../../../redux/configureStore";
import {renderToStoreModal} from "../../../../testSupport/RenderedComponentTestUtil";
import {Provider} from 'react-redux'
import {SaveButtonLabel} from "../SaveButton";
import {CloseButtonLabel} from "../CloseButton";

function changePortfolioNameValue(getByPlaceholderText) {
    const portfolioName = getByPlaceholderText('Please insert a portfolio name')
    fireEvent.change(portfolioName, {target: {value: 'test'}})
    fireEvent.blur(portfolioName)
}

const buttonIn = (label) => (rendered) => {
    const {getByText} = rendered;
    return getByText(label)
}

const saveButtonIn = buttonIn(SaveButtonLabel)

const closeButtonIn = buttonIn(CloseButtonLabel)

const rowNumber = (rowNumber, container) => container.querySelector(`tbody > tr:nth-child(${rowNumber})`)

describe('CreatePortfolio when dialog is shown', () => {

    it("By default table positions has only one row. Some buttons are disable and some are enabled", () => {
        const amendedState = {
            shown: true
        }

        const toBeRendered = (
            <Provider store={createANewStore(amendedState, {})}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const rendered = renderToStoreModal(toBeRendered)
        const {container, getByText} = rendered

        expect(container.querySelectorAll("tbody tr").length).toBe(1)

        expect(getByText('Add New Row')).not.toBeDisabled()
        expect(getByText('Delete Rows')).toBeDisabled()

        expect(closeButtonIn(rendered)).not.toBeDisabled()
        expect(getByText('Reset')).toBeDisabled()
        expect(saveButtonIn(rendered)).toBeDisabled()

    })

    it("When we type a portfolio name value then Reset Button becomes enabled but Save button stays disabled", async () => {
        const amendedState = {
            shown: true
        }

        const toBeRendered = (
            <Provider store={createANewStore(amendedState, {})}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {getByText, getByPlaceholderText} = renderToStoreModal(toBeRendered)

        changePortfolioNameValue(getByPlaceholderText)

        await waitFor(() => {
            expect(getByText('Reset')).not.toBeDisabled()
            expect(getByText('Save')).toBeDisabled()
        })
    })

    it("Given we gives focus to portfolio name input when we blur it then Required message is displayed", async () => {

        const portfolioNameErrorMessage = 'Required'

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {getByPlaceholderText, getByText, queryByText} = renderToStoreModal(toBeRendered)

        expect(queryByText(portfolioNameErrorMessage)).toBe(null)

        const portfolioName = getByPlaceholderText('Please insert a portfolio name')

        fireEvent.focus(portfolioName)
        fireEvent.blur(portfolioName)

        await waitFor(() => {
            expect(getByText(portfolioNameErrorMessage)).toBeVisible()
        })

        changePortfolioNameValue(getByPlaceholderText)

        await waitFor(() => {
            expect(queryByText(portfolioNameErrorMessage)).toBe(null)
        })

    })

    it("Given we type a portfolio name and all positions values then Save button becomes enabled and not error messages are displayed. As soon as we type a position field then an error message is displayed to remind to complete all the row", async () => {

        const positionsErrorMessage = 'Please fill all the columns';

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByPlaceholderText, getByText, queryByText} = renderToStoreModal(toBeRendered)

        expect(container.querySelectorAll("tbody tr").length).toBe(1)

        expect(queryByText(positionsErrorMessage)).toBe(null)

        changePortfolioNameValue(getByPlaceholderText)

        await waitFor(() => {
            expect(getByText('Save')).toBeDisabled()
        })

        await changePositionsTableSelectFieldValueOf('pair', 0, 1, 'US30', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        expect(getByText('Save')).toBeDisabled()

        await changePositionsTableTextFieldValueOf('amount', 0, 2, '2', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        expect(getByText('Save')).toBeDisabled()

        await changePositionsTableTextFieldValueOf('price', 0, 3, '3', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        expect(getByText('Save')).toBeDisabled()

        await changePositionsTableSelectFieldValueOf('type', 0, 4, 'sell', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        expect(getByText('Save')).toBeDisabled()

        await changePositionsTableTextFieldValueOf('openedDateTime', 0, 5, '12 03 20 at 20:30:01', container)

        expect(getByText('Save')).not.toBeDisabled()

        expect(queryByText(positionsErrorMessage)).toBe(null)

    })

    it("Given a field is incorrect Save button is not displayed and error message is displayed", async () => {

        const positionsErrorMessage = 'Please fill all the columns';

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByPlaceholderText, getByText, queryByText} = renderToStoreModal(toBeRendered)

        expect(queryByText(positionsErrorMessage)).toBe(null)

        changePortfolioNameValue(getByPlaceholderText)

        await changePositionsTableSelectFieldValueOf('pair', 0, 1, 'US30', container)

        await waitFor(() => {
            expect(getByText(positionsErrorMessage)).toBeVisible()
        })

        await changePositionsTableTextFieldValueOf('amount', 0, 2, '2', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        await changePositionsTableTextFieldValueOf('price', 0, 3, '3', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        await changePositionsTableSelectFieldValueOf('type', 0, 4, 'sell', container)

        expect(getByText(positionsErrorMessage)).toBeVisible()

        await changePositionsTableTextFieldValueOf('openedDateTime', 0, 5, 'wrong', container)

        expect(getByText('Save')).toBeDisabled()

        expect(getByText(positionsErrorMessage)).toBeVisible()

    })

    it("Add new row button allow to add a new default position row", async () => {

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByText, queryByText} = renderToStoreModal(toBeRendered)

        expect(getByText('1')).toBeVisible()
        expect(queryByText('2')).toBe(null)
        expect(queryByText('3')).toBe(null)

        fireEvent.click(getByText('Add New Row'))

        expect(getByText('1')).toBeVisible()
        expect(getByText('2')).toBeVisible()
        expect(queryByText('3')).toBe(null)

        fireEvent.click(getByText('Add New Row'))

        expect(getByText('1')).toBeVisible()
        expect(getByText('2')).toBeVisible()
        expect(getByText('3')).toBeVisible()

        expect(container.querySelectorAll("tbody tr").length).toBe(3)

    })

    it("I can selects all rows, delete all of them and reset the form to the original aspect with 1 row", async () => {

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByText} = renderToStoreModal(toBeRendered)

        const deleteAllRowsButton = getByText('Delete Rows');

        expect(deleteAllRowsButton).toBeDisabled()

        const selectAllRows = container.querySelector("table > thead > tr > th:nth-child(1) > input");

        fireEvent.click(selectAllRows)

        expect(deleteAllRowsButton).not.toBeDisabled()

        fireEvent.click(deleteAllRowsButton)

        expect(container.querySelectorAll("tbody tr").length).toBe(0)

        expect(getByText('Save')).toBeDisabled()

        fireEvent.click(getByText('Reset'))

        expect(container.querySelectorAll("tbody tr").length).toBe(1)

    })

    it("I can selects the first row, and delete it", async () => {

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByText} = renderToStoreModal(toBeRendered)

        const deleteAllRowsButton = getByText('Delete Rows');

        expect(deleteAllRowsButton).toBeDisabled()

        const selectFirstRow = container.querySelector("tbody > tr:nth-child(1) > td:nth-child(1) > input ");

        fireEvent.click(selectFirstRow)

        expect(deleteAllRowsButton).not.toBeDisabled()

        fireEvent.click(deleteAllRowsButton)

        expect(container.querySelectorAll("tbody tr").length).toBe(0)

    })

    it("I can add 2 rows, select the second one and delete it. Row number will be correct!", async () => {

        const amendedState = {
            shown: true
        }

        const aStore = createANewStore(amendedState, {});

        const toBeRendered = (
            <Provider store={aStore}>
                <ConnectedCreatePortfolio/>
            </Provider>
        )

        const {container, getByText, queryByText} = renderToStoreModal(toBeRendered)

        const deleteAllRowsButton = getByText('Delete Rows');

        expect(deleteAllRowsButton).toBeDisabled()

        fireEvent.click(getByText('Add New Row'))

        fireEvent.click(getByText('Add New Row'))

        expect(within(rowNumber(1, container)).getByText('1')).toBeVisible()
        expect(within(rowNumber(2, container)).getByText('2')).toBeVisible()
        expect(within(rowNumber(3, container)).getByText('3')).toBeVisible()

        const selectFirstRow = container.querySelector("tbody > tr:nth-child(2) > td:nth-child(1) > input ");

        fireEvent.click(selectFirstRow)

        expect(deleteAllRowsButton).not.toBeDisabled()

        fireEvent.click(deleteAllRowsButton)

        expect(container.querySelectorAll("tbody tr").length).toBe(2)

        expect(within(rowNumber(1, container)).getByText('1')).toBeVisible()
        expect(within(rowNumber(2, container)).getByText('2')).toBeVisible()

        expect(queryByText('3')).not.toBeInTheDocument()

    })

    async function changePositionsTableTextFieldValueOf(fieldName, rowIndex, columnIndex, newValue, container) {
        fireEvent.click(container.querySelector(`.${fieldName}-id-rowIndex-${rowIndex}-colIndex-${columnIndex}`))

        await waitFor(() => {
            const editor = container.querySelector('input.edit-text')
            fireEvent.change(editor, {target: {value: newValue}})
            fireEvent.blur(editor)
        })

    }

    async function changePositionsTableSelectFieldValueOf(fieldName, rowIndex, columnIndex, newValue, container) {

        fireEvent.click(container.querySelector(`.${fieldName}-id-rowIndex-${rowIndex}-colIndex-${columnIndex}`))

        await waitFor(() => {
            const editor = container.querySelector('select.form-control.editor.edit-select')
            fireEvent.change(editor, {target: {value: newValue}})
            fireEvent.blur(editor)
        })

    }

})
