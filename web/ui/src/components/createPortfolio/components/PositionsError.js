import React from "react";

const PositionsError = (props) => {

    if (!props.hasBeenTouched) {
        return null
    }

    if (!props.arePositionsValid) {
        return (
            <div className="error" data-testid='positions-error'>{props.errorMessage}</div>
        )
    } else {
        return null
    }
}

export default PositionsError
