import React, {Component} from "react";
import {Col, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row} from 'reactstrap';
import ConnectedPortfolioPositions from "./PortfolioPositions";
import {ErrorMessage, Field, Form, Formik} from 'formik';
import createPortfolioReduxConnector from '../CreatePortfolioReduxToReactConnector'
import AddNewRowButton from "./AddNewRowButton";
import DeleteRowsButton from "./DeleteRowsButton";
import createFormValidator from "../PortfolioFormValidationFactory";
import SaveButton from "./SaveButton";
import CloseButton from "./CloseButton";
import ResetButton from "./ResetButton";

export class CreatePortfolio extends Component {

    handleClose = (resetForm) => {
        this.props.actions.hidePortfolioEditor()
        this.handleResetForm(resetForm)
    }

    handleResetForm = (resetForm) => {
        resetForm()
        this.props.actions.resetEditor()
    }

    render() {
        return (
            <Formik
                initialValues={this.props.initialValues}
                validationSchema={this.props.validator}
                onSubmit={(values, {resetForm}) => {
                    console.log('onSubmit called')
                    console.log('values to be submitted -------  ' + JSON.stringify(values, null, 4))
                    resetForm()
                }}

                children={({
                               dirty,
                               touched,
                               errors,
                               setFieldValue,
                               setTouched,
                               submitForm,
                               resetForm,
                               isValid
                           }) => {

                    return (
                        <Modal size="xl" isOpen={this.props.shown}>
                            <ModalHeader toggle={this.handleClose}>Create Portfolio</ModalHeader>
                            <ModalBody>
                                <Form>
                                    <Container>
                                        <Row>
                                            <Col> <label htmlFor="portfolioName">Portfolio Name:</label></Col>
                                        </Row>
                                        <Row>
                                            <Col> <Field name="portfolioName"
                                                         className="form-input"
                                                         placeholder="Please insert a portfolio name"/>
                                                <ErrorMessage name="portfolioName"/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <ConnectedPortfolioPositions
                                                    setFieldValue={setFieldValue}
                                                    setTouched={setTouched}
                                                    hasBeenTouched={touched.positionsValid}
                                                    errorMessage={errors.positionsValid}/>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Form>
                            </ModalBody>
                            <ModalFooter>
                                <Container>
                                    <Row>
                                        <Col md={9}>
                                            <AddNewRowButton onClick={this.props.actions.addNewDefaultRow}/>
                                            <DeleteRowsButton disabled={this.props.noRowsSelected}
                                                              onClick={this.props.actions.deleteSelectedRows}/>
                                        </Col>
                                        <Col md={3}>
                                            <CloseButton onClick={() => this.handleClose(resetForm)}/>
                                            <ResetButton onClick={() => this.handleResetForm(resetForm)}
                                                         disabled={!dirty}/>
                                            <SaveButton onClick={submitForm} touched={touched} isValid={isValid}/>
                                        </Col>
                                    </Row>
                                </Container>
                            </ModalFooter>
                        </Modal>
                    )
                }}
            />

        )
    }

}

const ConnectedCreatePortfolio = createPortfolioReduxConnector(createFormValidator)(CreatePortfolio)

export default ConnectedCreatePortfolio;
