import {actions} from "../../ducks/portfolioEditor";
import {connect} from 'react-redux';

const mapDispatchToProps = actions => dispatch => {
    return ({
        actions: {
            selectAllRows: (data) => dispatch(actions.selectAllRows(data)),
            selectRow: (data) => dispatch(actions.selectRow(data)),
            updateCellValue: (data) => dispatch(actions.updateCellValue(data))
        }
    });
}

export const mapStateToProps = state => {
    return {
        positionsAreValid: state.portfolioEditorModel.positionsAreValid,
        rows: state.portfolioEditorModel.rows,
        selectedRow: state.portfolioEditorModel.selectedRows
    }
}

const portfolioPositionsReduxConnector = component => connect(mapStateToProps, mapDispatchToProps(actions))(component)

export default portfolioPositionsReduxConnector
