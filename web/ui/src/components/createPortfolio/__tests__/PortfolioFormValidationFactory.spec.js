import createFormValidator from '../PortfolioFormValidationFactory'

describe('PortfolioFormValidationFactory', () => {

    describe("Invalid form", () => {

        it("portfolioName invalid: empty, positionsValid: false", async () => {

            const actualResult = await createFormValidator(null).validate({
                portfolioName: '',
                positionsValid: false
            }).catch(error => error)

            expect(actualResult.errors).toStrictEqual(["Please fill all the columns"])
        })

        it("portfolioName valid: non empty, positionsValid:false", async () => {

            const actualResult = await createFormValidator(null).validate({
                portfolioName: 'test',
                positionsValid: false
            }).catch(error => error)

            expect(actualResult.errors).toStrictEqual(["Please fill all the columns"])
        })

        it("portfolioName invalid: empty, positionsValid:true", async () => {

            const actualResult = await createFormValidator(null).validate({
                portfolioName: '',
                positionsValid: true
            }).catch(error => error)

            expect(actualResult.errors).toStrictEqual(["Required"])
            expect(actualResult.path).toStrictEqual('portfolioName')
        })

        it("portfolioName already exist, positionsValid:true", async () => {

            const state = {portfolios: [{name: 'p1'}, {name: 'p2'}]}

            const actualResult = await createFormValidator(state).validate({
                portfolioName: 'p1',
                positionsValid: true
            }).catch(error => error)

            expect(actualResult.errors).toStrictEqual(["Portfolio name already exist"])
            expect(actualResult.path).toStrictEqual('portfolioName')
        })
    })
    describe("Valid form", () => {

        it("portfolioName valid: non empty, positionsValid: true", async () => {

            const toBeValidated = {
                portfolioName: 'test',
                positionsValid: true
            };
            const actualResult = await createFormValidator(null).validate(toBeValidated)

            expect(actualResult).toStrictEqual(toBeValidated)
        })

        it("portfolioName valid: non empty and non existing, positionsValid: true", async () => {

            const state = {portfolios: [{name: 'p1'}, {name: 'p2'}]}

            const toBeValidated = {
                portfolioName: 'p3',
                positionsValid: true
            };
            const actualResult = await createFormValidator(state).validate(toBeValidated)

            expect(actualResult).toStrictEqual(toBeValidated)
        })
    })


})
