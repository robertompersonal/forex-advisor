import React from 'react';
import 'jest-enzyme';
import 'enzyme-react-16-adapter-setup'
import {connectASimpleComponentWithARealStore} from "../../../testSupport/ReduxComponentConnectorTestUtil"
import {createANewStore} from "../../../redux/configureStore";
import portfolioPositionsReduxConnector from "../PortfolioPositionsReduxToReactConnector";
import {actions} from "../../../ducks/portfolioEditor";

describe('should decorate a component with the right props', () => {

    it('default props are correct', () => {

        connectASimpleComponentWithARealStore(createANewStore())(portfolioPositionsReduxConnector)
            .andThenWithPropsAndState((propsAfterRemount, state) => {
                expect(propsAfterRemount.positionsAreValid).toBe(false)
                expect(propsAfterRemount.rows.length).toBe(1)
                expect(propsAfterRemount.selectedRow).toStrictEqual([])
            })
    })

    it('selectAllRows action control selectedRow prop', () => {

        connectASimpleComponentWithARealStore(createANewStore())(portfolioPositionsReduxConnector)
            .andThenWithPropsAndStore((propsAfterRemount, theStore) => {
                theStore.dispatch(actions.addNewDefaultRow)
                theStore.dispatch(actions.addNewDefaultRow)
                propsAfterRemount.actions.selectAllRows({allSelected: true})
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.selectedRow).toStrictEqual([1, 2, 3])
                propsAfterRemount.actions.selectAllRows({allSelected: false})
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.selectedRow).toStrictEqual([])
            })
    })

    it('selectRow action control selectedRow prop', () => {

        connectASimpleComponentWithARealStore(createANewStore())(portfolioPositionsReduxConnector)
            .andThenWithProps((propsAfterRemount) => {
                propsAfterRemount.actions.selectRow({visualRowId: 1, select: true})
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.selectedRow).toStrictEqual([1])
                propsAfterRemount.actions.selectRow({visualRowId: 1, select: false})
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.selectedRow).toStrictEqual([])
            })
    })

    it('updateCellValue action control rows prop', () => {

        connectASimpleComponentWithARealStore(createANewStore())(portfolioPositionsReduxConnector)
            .andThenWithProps((propsAfterRemount) => {
                propsAfterRemount.actions.updateCellValue({visualRowId: 1, dataField: "price", newValue: 10})
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.rows[0].price).toBe(10)
            })
    })


})
