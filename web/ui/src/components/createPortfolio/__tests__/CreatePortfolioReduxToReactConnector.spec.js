import React from 'react';
import 'jest-enzyme';
import 'enzyme-react-16-adapter-setup'
import createPortfolioReduxConnector from "../CreatePortfolioReduxToReactConnector";
import {connectASimpleComponentWithARealStore} from "../../../testSupport/ReduxComponentConnectorTestUtil"

import {actions} from '../../../ducks/portfolioEditor.js'
import {createANewStore} from "../../../redux/configureStore";

describe('should decorate a component with the right props', () => {

    const expectedFormValidator = {a: 1}

    const probeToCaptureFunctionCalls = jest.fn(state => "test");

    //jest seems not working properly if I use jest.fn directly
    const formValidatorFactory = function (state) {
        probeToCaptureFunctionCalls(state)
        return expectedFormValidator
    }

    it('default props are correct', () => {
        connectASimpleComponentWithARealStore(createANewStore())(createPortfolioReduxConnector(formValidatorFactory))
            .andThenWithPropsAndState((propsAfterRemount, state) => {
                expect(propsAfterRemount.shown).toBe(false)
                expect(propsAfterRemount.noRowsSelected).toBe(true)
                expect(propsAfterRemount.initialRows.length).toBe(1)
                expect(propsAfterRemount.initialValues).toStrictEqual({
                    portfolioName: '',
                    positionsValid: false,
                    positions: propsAfterRemount.initialRows
                })
                expect(propsAfterRemount.validator).toBe(expectedFormValidator)
                expect(probeToCaptureFunctionCalls.mock.calls.length).toBe(1);
                expect(probeToCaptureFunctionCalls.mock.calls[0][0]).toBe(state);
            })
    })

    it('showPortfolioEditor and hidePortfolioEditor actions control shown prop', () => {

        connectASimpleComponentWithARealStore(createANewStore())(createPortfolioReduxConnector(formValidatorFactory))
            .andThenWithProps((propsAfterRemount) => {
                propsAfterRemount.actions.showPortfolioEditor()
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.shown).toBe(true)

                propsAfterRemount.actions.hidePortfolioEditor()
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.shown).toBe(false)
            })
    })

    it('after resetEditor actions actions no rows are selected', () => {

        connectASimpleComponentWithARealStore(createANewStore())(createPortfolioReduxConnector(formValidatorFactory))
            .andThenWithPropsAndStore((propsAfterRemount, theStore) => {
                theStore.dispatch(actions.addNewDefaultRow)
                theStore.dispatch(actions.addNewDefaultRow)
                theStore.dispatch(actions.selectAllRows({allSelected: true}))
            })
            .andThenWithProps((propsAfterRemount) => {
                propsAfterRemount.actions.resetEditor()
            })
            .andThenWithProps((propsAfterRemount) => {
                expect(propsAfterRemount.noRowsSelected).toBe(true)
            })

    })

    it('after deleteSelectedRows action no rows are selected', () => {

        connectASimpleComponentWithARealStore(createANewStore())(createPortfolioReduxConnector(formValidatorFactory))
            .andThenWithPropsAndStore((newProps, theStore) => {
                theStore.dispatch(actions.selectAllRows({allSelected: true}))
            })
            .andThenWithProps((propsAfterRemount) => propsAfterRemount.actions.deleteSelectedRows())
            .andThenWithProps((propsAfterRemount) => expect(propsAfterRemount.noRowsSelected).toBe(true))

    })

    it('addNewDefaultRow action adds new rows', () => {

        connectASimpleComponentWithARealStore(createANewStore())(createPortfolioReduxConnector(formValidatorFactory))
            .andThenWithProps((propsAfterRemount) => {
                propsAfterRemount.actions.addNewDefaultRow()
                propsAfterRemount.actions.addNewDefaultRow()
                propsAfterRemount.actions.addNewDefaultRow()
            })
            .andThenWithState((state) => {
                expect(state.portfolioEditorModel.rows.length).toBe(1 + 3)
            })

    })


})
