import * as Yup from "yup";

function portfolioNameValidator(state) {
    const validator = Yup.string().required('Required')
    if (state &&
        state.portfolios &&
        state.portfolios.length > 1) {
        return validator.notOneOf(state.portfolios.map(p => p.name), 'Portfolio name already exist')
    }
    return validator
}

const createFormValidator = (state) => (Yup.object({
    portfolioName: portfolioNameValidator(state),
    positionsValid: Yup
        .boolean()
        .oneOf([true], 'Please fill all the columns')
}))

export default createFormValidator
