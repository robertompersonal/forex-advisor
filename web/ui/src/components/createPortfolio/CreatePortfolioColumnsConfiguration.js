import {Type} from 'react-bootstrap-table2-editor'
import {dateTimeValidation} from "../../util/positionsValidator";

const columnClasses = (columngIdentifier, cell, row, rowIndex, colIndex) => columngIdentifier + "-id-rowIndex-" + rowIndex + "-colIndex-" + colIndex

const createPortfolioColumnsConfiguration = [
    {
        dataField: 'id',
        text: 'Row Num.'
    },
    {
        dataField: 'pair',
        text: 'Pair',
        editor: {
            type: Type.SELECT,
            options: [
                {
                    'value': 'SPX500',
                    'label': 'SPX500'
                },
                {
                    'value': 'US30',
                    'label': 'US30'
                }
            ]
        },
        classes: function callback(cell, row, rowIndex, colIndex) {
            return columnClasses('pair', cell, row, rowIndex, colIndex)
        }
    },
    {
        dataField: 'amount',
        text: 'Amount',
        type: "string",
        validator: (newValue, row, column) => {
            if (newValue == null || newValue.includes(" ") || isNaN(newValue)) {
                console.log("validation failed")
                return {
                    valid: false,
                    message: 'Amount should be numeric'
                }
            } else {
                console.log("amount validation succeeded <" + newValue + ">")
                return true
            }
        },
        classes: function callback(cell, row, rowIndex, colIndex) {
            return columnClasses('amount', cell, row, rowIndex, colIndex)
        }
    },
    {
        dataField: 'price',
        text: 'Price',
        type: "string",
        validator: (newValue, row, column) => {
            console.log("price validation")
            console.log("new value" + newValue)
            console.log("row " + JSON.stringify(row))
            console.log("column " + JSON.stringify(column))
            if (newValue == null || newValue.includes(" ") || isNaN(newValue)) {
                return {
                    valid: false,
                    message: 'Price should be numeric'
                }
            } else {
                console.log("price validation succeeded <" + newValue + ">")
                return true
            }
        },
        classes: function callback(cell, row, rowIndex, colIndex) {
            return columnClasses('price', cell, row, rowIndex, colIndex)
        }
    },
    {
        dataField: 'type',
        text: 'Type',
        editor: {
            'type': Type.SELECT,
            'options': [
                {
                    'value': 'sell',
                    'label': 'sell'
                },
                {
                    'value': 'buy',
                    'label': 'buy'
                }
            ]
        },
        classes: function callback(cell, row, rowIndex, colIndex) {
            return columnClasses('type', cell, row, rowIndex, colIndex)
        }
    },
    {
        dataField: 'openedDateTime',
        text: 'Opened Date Time',
        validator: (newValue, row, column) => {
            const validation = dateTimeValidation(newValue)
            if (validation.valid) {
                return true
            } else return validation
        },
        classes: function callback(cell, row, rowIndex, colIndex) {
            return columnClasses('openedDateTime', cell, row, rowIndex, colIndex)
        }
    }
]

export default createPortfolioColumnsConfiguration
