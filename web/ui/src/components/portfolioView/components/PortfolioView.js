import React from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import {showPortfolioColumnsConfiguration} from "../showPortfolioColumnsConfiguration"

export const PortfolioView = (props) => {
    if (props.selectedPortfolio) {
        return (
            <BootstrapTable
                bootstrap4
                className="align-self-center"
                keyField='id'
                data={props.selectedPortfolio.positions}
                columns={showPortfolioColumnsConfiguration}/>
        )
    } else {
        return null
    }
}

