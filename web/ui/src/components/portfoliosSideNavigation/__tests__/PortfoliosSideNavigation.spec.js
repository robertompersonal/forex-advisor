import React from 'react';
import {shallow} from 'enzyme'
import 'jest-enzyme';
import 'enzyme-react-16-adapter-setup'
import ListGroup from "react-bootstrap/ListGroup"
import {PortfoliosSideNavigation} from "../components/PortfoliosSideNavigation";
import {Loading} from "../../loadingSpinner/components/Loading";

describe('PortfoliosSideNavigation shows a portfolios', () => {

    it("given a loaded portfolioModel, should show a list of names of all single-portfolio contained inside a portfolios", () => {

        const portfoliosModel = {
            loading: false,
            portfolios: [
                {name: 'portfolio 1', id: 'a'},
                {name: 'portfolio 2', id: 'b'},
                {name: 'portfolio 3', id: 'c'}
            ],
            errorMessage: null
        }

        const wrapper = shallow(<PortfoliosSideNavigation portfoliosModel={portfoliosModel}/>);

        expect(wrapper.find(ListGroup.Item).length).toBe(3)

        expect(wrapper.find('#a')).toHaveText('portfolio 1');
        expect(wrapper.find('#b')).toHaveText('portfolio 2');
        expect(wrapper.find('#c')).toHaveText('portfolio 3');

    })

    it("given a loading portfolioModel, should show no names but a Loading component", () => {
        const portfoliosModel = {
            loading: true,
            portfolios: [
                {name: 'portfolio 1', id: 'a'}
            ],
            errorMessage: null
        }

        const wrapper = shallow(<PortfoliosSideNavigation portfoliosModel={portfoliosModel}/>);

        expect(wrapper.find(ListGroup.Item).length).toBe(0)

        expect(wrapper.find(Loading)).toExist()

    })
})
