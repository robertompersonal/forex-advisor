import ListGroup from "react-bootstrap/ListGroup"
import React from "react";
import {Loading} from "../../loadingSpinner/components/Loading";

export const PortfoliosSideNavigation = (props) => {
    const portfoliosModel = props.portfoliosModel;
    if (portfoliosModel.loading) {
        return (
            <Loading/>
        )
    } else {
        const result = portfoliosModel.portfolios.map(p => (
                <ListGroup.Item key={p.id} id={p.id}>
                    {p.name}
                </ListGroup.Item>
            )
        )
        return (
            <ListGroup>
                {result}
            </ListGroup>
        )
    }
}
