import * as yup from 'yup'

let singlePositionSchema = yup.object().shape({
    amount: yup
        .number()
        .positive()
        .integer()
        .required('Amount has to be a positive number'),
    openedDateTime: yup.string().test('openedDateTime', '', value => dateTimeValidation(value).valid),
    pair: yup.mixed().oneOf(['US30', 'SPX500']).required('Pair is required'),
    price: yup
        .number()
        .positive()
        .integer()
        .required('Price has to be a positive number'),
    type: yup.mixed().oneOf(['buy', 'sell']),
});

const validate = (positionsToBeValidated) => yup
    .array()
    .min(1)
    .of(singlePositionSchema)
    .isValidSync(positionsToBeValidated)

const dateTimeValidationResult = () => {
    const createAResult = (valid, message) => (dateTime) => ({
        valid: valid,
        message: message,
        dateTime: dateTime
    })

    const invalidResult = createAResult(false, 'Please use a format dd mm yy at hh:mm:ss')(undefined)

    const aValidPartialResult = createAResult(true, undefined)

    return {
        invalid: invalidResult,
        validWithDate: (date) => aValidPartialResult(date)
    }
}

const toDate = (result) => new Date('20' + result[3] + '-' + result[2] + '-' + result[1] + 'T' + result[4] + ':' + result[5] + ':' + result[6] + 'Z')

export const dateTimeValidation = (dateTime) => {

    const result = dateTime.match(/(\d\d) (\d\d) (\d\d) at (\d\d):(\d\d):(\d\d)/)

    if (!result) {
        return dateTimeValidationResult().invalid
    }

    const aDate = toDate(result)

    if (isNaN(aDate)) {
        return dateTimeValidationResult().invalid
    }
    return dateTimeValidationResult().validWithDate(aDate)
}

export const singlePositionValidation = (position) => {
    return singlePositionSchema.isValidSync(position)
}

export default validate
