import {createANewStore} from "../configureStore";
import {DEFAULT_STATE as portfolioEditorDefaultState} from "../../ducks/portfolioEditor";
import {DEFAULT_STATE as portfoliosDefaultState} from "../../ducks/portfolios";

describe("Store", () => {

    it("given non external initial state, then state combine portfolios and portfolioEditor defaults state", () => {

        const actualState = createANewStore({}, {}).getState()

        expect(actualState.portfoliosModel).toStrictEqual(portfoliosDefaultState)

        expect(actualState.portfolioEditorModel).toStrictEqual(portfolioEditorDefaultState)

    })

    it("Initial PortfolioEditorModel state should be amended with an external value", () => {

        expect(
            createANewStore(
                {},
                {}
            )
                .getState()
                .portfolioEditorModel
                .shown
        ).toBe(false)

        expect(
            createANewStore(
                {shown: true},
                {}
            )
                .getState()
                .portfolioEditorModel
                .shown
        ).toBe(true)

    })

    it("Initial PortfoliosModel state should be amended with an external value", () => {

        expect(
            createANewStore(
                {},
                {}
            )
                .getState()
                .portfoliosModel
                .errorMessage
        ).toBe(null)

        expect(
            createANewStore(
                {},
                {errorMessage:'test'}
            )
                .getState()
                .portfoliosModel
                .errorMessage
        ).toBe('test')

    })


})
