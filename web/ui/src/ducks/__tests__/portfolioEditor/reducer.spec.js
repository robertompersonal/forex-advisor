import {actions, configurableReducer, defaultFirstRow, types} from '../../portfolioEditor'

function someDummyUpdate() {
    return actions.updateCellValue({visualRowId: 1, dataField: "price", newValue: 10});
}

describe("Reducer", () => {

    const alwaysInvalidValidator = (positions) => false

    const reducerUnderTest = configurableReducer({validate: alwaysInvalidValidator})

    describe('DELETE_SELECTED_ROWS action', () => {

        it("deletes all selected and re-set the id of the remaining rows starting from 1", () => {

            const oneRowSelectedState = reducerUnderTest({
                    rows: [
                        {id: 1, v: '1'},
                        {id: 2, v: '2'},
                        {id: 3, v: '3'},
                        {id: 4, v: '4'}
                    ],
                    selectedRows: []
                },
                actions.selectRow({visualRowId: 1, select: true})
            )

            const stateWithRow1And3Selected = reducerUnderTest(
                oneRowSelectedState,
                actions.selectRow({visualRowId: 3, select: true})
            )

            expect(reducerUnderTest(stateWithRow1And3Selected, actions.deleteSelectedRows).rows)
                .toStrictEqual([
                    {id: 1, v: '2'},
                    {id: 2, v: '4'}
                ])

        })

        it("triggers a validation", () => {

            const stateWithInvalidPositions = reducerUnderTest(undefined, {type: "some action"})

            expect(stateWithInvalidPositions.positionsAreValid).toBe(false)

            const alwaysValidValidator = jest.fn().mockImplementation(positions => true);

            const reducer = configurableReducer({validate: alwaysValidValidator})

            const actualStateWithValidPositions = reducer(stateWithInvalidPositions, actions.deleteSelectedRows)

            expect(actualStateWithValidPositions.positionsAreValid).toBe(true)

            expect(alwaysValidValidator.mock.calls[0][0]).toStrictEqual(actualStateWithValidPositions.rows)

        })

        it("set selectedRows to []", () => {

            const twoRowsState = reducerUnderTest(undefined, actions.addNewDefaultRow);

            const oneRowSelectedState = reducerUnderTest(twoRowsState, {
                type: types.SELECT_ROW,
                payload: {visualRowId: 1, select: true}
            })

            expect(oneRowSelectedState.selectedRows).toStrictEqual([1])

            const oneRowNonSelectedState = reducerUnderTest(oneRowSelectedState, actions.deleteSelectedRows);

            expect(oneRowNonSelectedState.selectedRows).toStrictEqual([])

        })

        it("preserves the state", () => {

            const newState = reducerUnderTest({
                a: 1,
                b: 2,
                rows: [{id: 1}],
                selectedRows: [1]
            }, actions.deleteSelectedRows)

            expect(newState.a).toBe(1)
            expect(newState.b).toBe(2)

        })

    })

    it("HIDE_PORTFOLIO_EDITOR action hides portfolio editor", () => {

        expect(reducerUnderTest(undefined, actions.hidePortfolioEditor).shown).toBe(false)

        let someState = {a: 1, shown: true};
        expect(reducerUnderTest(someState, actions.hidePortfolioEditor))
            .toStrictEqual({
                ...someState,
                shown: false
            })

    })

    it("SHOW_PORTFOLIO_EDITOR action preserves state but shown portfolio editor", () => {

        expect(reducerUnderTest(undefined, actions.showPortfolioEditor).shown).toBe(true)

        let someState = {a: 1, shown: false};

        expect(reducerUnderTest(someState, actions.showPortfolioEditor))
            .toStrictEqual({
                ...someState,
                shown: true
            })

    })

    describe('Default behaviour', () => {
        it("selectedRows are empty", () => {
            expect(reducerUnderTest(undefined, {}).selectedRows).toStrictEqual([])
        })

        it("rows are invalid", () => {
            expect(reducerUnderTest(undefined, {}).positionsAreValid).toBe(false)
        })

        it("portfolio editor is hidden", () => {

            const givenState = reducerUnderTest(undefined, {})

            expect(givenState.shown).toBe(false)

        })

        it("rows has 1 row with all null values except for the id = 1", () => {

            const givenState = reducerUnderTest(undefined, {})

            expect(givenState.rows).toStrictEqual([{
                id: 1,
                pair: null,
                amount: null,
                price: null,
                type: null,
                openedDateTime: null
            }])
        })

        it("state has 1 row and it is equals to initialRows", () => {

            const givenState = reducerUnderTest(undefined, {})

            expect(givenState.rows).toStrictEqual([defaultFirstRow])

            expect(givenState.initialRows).toStrictEqual([defaultFirstRow])

        })

    })

    it("ADD_A_DEFAULT_ROW action add a default row to the end of the editor's rows", () => {

        const newState = reducerUnderTest(undefined, actions.addNewDefaultRow)

        expect(newState.rows.length).toBe(2)

        expect(newState.rows[0]).toStrictEqual(defaultFirstRow)
        expect(newState.rows[1]).toStrictEqual({...defaultFirstRow, id: 2})

        const nextState = reducerUnderTest(newState, actions.addNewDefaultRow)

        expect(nextState.rows.length).toBe(3)
        expect(nextState.rows[2]).toStrictEqual({...defaultFirstRow, id: 3})

    })

    it("ADD_A_DEFAULT_ROW action makes the state invalid", () => {

        const alwaysValidValidator = (positions) => true

        const stateWithValidPositions = configurableReducer({validate: alwaysValidValidator})(undefined, someDummyUpdate())

        expect(stateWithValidPositions.positionsAreValid).toBe(true)

        const stateWithInValidPositions = reducerUnderTest(stateWithValidPositions, actions.addNewDefaultRow)

        expect(stateWithInValidPositions.positionsAreValid).toBe(false)

    })

    it("UPDATE_CELL_VALUE action can update the price field in rows with different id", () => {

        const firstUpdate = actions.updateCellValue({visualRowId: 1, dataField: "price", newValue: 10})

        const updatedState = reducerUnderTest({rows: [{price: 0}]}, firstUpdate)

        expect(updatedState.rows).toStrictEqual([{price: firstUpdate.payload.newValue}])

        const secondUpdate = actions.updateCellValue({visualRowId: 2, dataField: "price", newValue: 20})

        const finalUpdatedState = reducerUnderTest({rows: [{price: 10}, {price: 0}]}, secondUpdate)

        expect(finalUpdatedState.rows).toStrictEqual([{price: 10}, {price: secondUpdate.payload.newValue}])

    })

    it("RESET_EDITOR action preserves the state, sets rows to the single default row and clear all selected rows", () => {

        const initialState = {a: 1, rows: [1, 2, 3], selectedRows: [5, 6, 7]}

        expect(reducerUnderTest(initialState, actions.resetEditor)).toStrictEqual(
            {
                ...initialState,
                positionsAreValid:false,
                rows: [defaultFirstRow],
                selectedRows: []
            })

    })

    it("RESET_EDITOR action makes the state invalid", () => {

        const alwaysValidValidator = (positions) => true

        const stateWithValidPositions = configurableReducer({validate: alwaysValidValidator})(undefined, someDummyUpdate())

        expect(stateWithValidPositions.positionsAreValid).toBe(true)

        const stateWithInValidPositions = reducerUnderTest(stateWithValidPositions, actions.resetEditor)

        expect(stateWithInValidPositions.positionsAreValid).toBe(false)

    })

    describe('Selection actions', () => {
        it("SELECT_ROW action controls selectedRows attribute. SelectedRows contains only selected rows visual row id: it is not empty if there is at least one selected row", () => {

            const oneRowSelectedState = reducerUnderTest(undefined, actions.selectRow({visualRowId: 1, select: true}))

            expect(oneRowSelectedState.selectedRows).toStrictEqual([1])

            const oneRowNonSelectedState = reducerUnderTest(oneRowSelectedState, actions.selectRow({
                visualRowId: 1,
                select: false
            }))

            expect(oneRowNonSelectedState.selectedRows.length).toBe(0)

            const twoRowsNonSelectedState = reducerUnderTest(oneRowNonSelectedState, actions.addNewDefaultRow)

            expect(twoRowsNonSelectedState.selectedRows.length).toBe(0)

            const oneRowSelectedFinalState = reducerUnderTest(twoRowsNonSelectedState, actions.selectRow({
                visualRowId: 2,
                select: true
            }))

            expect(oneRowSelectedFinalState.selectedRows).toStrictEqual([2])

            const threeRowsState = reducerUnderTest(oneRowSelectedFinalState, actions.addNewDefaultRow)

            const twoRowsSelectedFinalState = reducerUnderTest(threeRowsState, actions.selectRow({
                visualRowId: 3,
                select: true
            }))

            expect(twoRowsSelectedFinalState.selectedRows).toStrictEqual([2, 3])

        })

        it("SELECT_ROW action is idempotent", () => {

            const visualRowId1Selected = actions.selectRow({visualRowId: 1, select: true});
            const visualRowId1SelectedState = reducerUnderTest(undefined, visualRowId1Selected)
            const alwaysVisualRowId1SelectedState = reducerUnderTest(visualRowId1SelectedState, visualRowId1Selected)
            const againVisualRowId1SelectedState = reducerUnderTest(alwaysVisualRowId1SelectedState, visualRowId1Selected)

            expect(againVisualRowId1SelectedState.selectedRows).toStrictEqual([1])

        })

        it("SELECT_ALL_ROWS action should selects and deselect all rows based on its payload value", () => {

            const twoRowsState = reducerUnderTest(undefined, actions.addNewDefaultRow)

            const allSelectedRowsState = reducerUnderTest(twoRowsState, actions.selectAllRows({allSelected: true}))

            expect(allSelectedRowsState.selectedRows).toStrictEqual([1, 2])

            const allRowsNonSelectedState = reducerUnderTest(allSelectedRowsState, actions.selectAllRows({allSelected: false}))

            expect(allRowsNonSelectedState.selectedRows).toStrictEqual([])

        })

        it("SELECT_ALL_ROWS action is idempotent with payload equals to true. It returns always the same identical state", () => {

            const selectAllRowsAction = actions.selectAllRows({allSelected: true})

            const oneRowState = reducerUnderTest(undefined, {})

            const allSelectedRowsState = reducerUnderTest(oneRowState, selectAllRowsAction)

            const alwaysAllSelectedRowsState = reducerUnderTest(allSelectedRowsState, selectAllRowsAction)

            expect(allSelectedRowsState.selectedRows).toBe(alwaysAllSelectedRowsState.selectedRows)

        })

        it("SELECT_ALL_ROWS action is idempotent with payload equals to false. It returns always the same identical state", () => {

            const deselectAllRowsAction = actions.selectAllRows({allSelected: false})

            const oneRowState = reducerUnderTest(undefined, {})

            const allDeselectedRowsState = reducerUnderTest(oneRowState, deselectAllRowsAction)

            const alwaysAllDeselectedRowsState = reducerUnderTest(allDeselectedRowsState, deselectAllRowsAction)

            expect(allDeselectedRowsState.selectedRows).toBe(alwaysAllDeselectedRowsState.selectedRows)

        })

    })

})
