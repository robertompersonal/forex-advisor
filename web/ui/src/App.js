import React from "react";

import ConnectedMain from "./components/main/components/Main";
import {Provider} from "react-redux";
import store from "./redux/configureStore";

const App = () => (
    <Provider store={store}>
        <ConnectedMain/>
    </Provider>
);

export default App;
