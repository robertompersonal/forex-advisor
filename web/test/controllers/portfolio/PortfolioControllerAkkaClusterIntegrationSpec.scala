package controllers.portfolio

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import org.binqua.forex.advisor.newportfolios.{NewPortfoliosShardingProductionModule, PortfoliosSummary}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel}
import org.binqua.forex.util.Util
import org.binqua.forex.web.test.util.PortfoliosJsonTestContext
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.MimeTypes
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import play.api.test.Helpers.{contentAsString, contentType, status, _}
import play.api.{Application, Configuration}

import scala.concurrent.Future

class PortfolioControllerAkkaClusterIntegrationSpec extends PlaySpec with GuiceOneAppPerSuite with GivenWhenThen {

  val akkClusterName = "forex-cluster"

  object akkaClusterNodesPort {
    val playAppNode = Util.findRandomOpenPortOnAllLocalInterfaces()
    val nodeThatWillHostTheEntities = Util.findRandomOpenPortOnAllLocalInterfaces()
  }

  override def fakeApplication(): Application = {
    GuiceApplicationBuilder()
      .configure(
        Configuration(
          TestingConfiguration
            .config(
              remoteArteryPort = akkaClusterNodesPort.playAppNode,
              akkaClusterSeedPort = akkaClusterNodesPort.nodeThatWillHostTheEntities,
              shouldThisActorSystemHostPersistedInstances = false,
              akkClusterName
            )
        )
      )
      .build()
  }

  "PortfolioController" should {

    "first create a new portfolio, second find all portfolios and third update them deleting all of them" in new ControllerTestContext(app) {

      object ActorToInitializePortfoliosSharding extends NewPortfoliosShardingProductionModule {
        def apply(): Behavior[Any] =
          Behaviors.setup { context =>
            initialisePortfoliosSharding(ClusterSharding(context.system))
            Behaviors.ignore
          }
      }

      ActorSystem(
        ActorToInitializePortfoliosSharding(),
        name = akkClusterName,
        config = TestingConfiguration
          .config(
            remoteArteryPort = akkaClusterNodesPort.nodeThatWillHostTheEntities,
            akkaClusterSeedPort = akkaClusterNodesPort.nodeThatWillHostTheEntities,
            shouldThisActorSystemHostPersistedInstances = true,
            akkClusterName
          )
      )

      private val initialCreatePortfolio: PortfoliosModel.CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

      private val createPortfolioBuilder = PortfoliosJsonTestContext.CreatePortfolioJsonBuilder.from(initialCreatePortfolio)

      private val createPortfolioJson: JsValue = Json.toJson(createPortfolioBuilder)(PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites)

      {
        val findResult: Future[Result] = invokeRoute.findAllPortfolios()

        status(findResult) mustEqual OK

        contentType(findResult) mustBe Some(MimeTypes.JSON)

        val actualState = Json.parse(contentAsString(findResult)).as[PortfoliosSummary]

        actualState.portfolios mustBe Set()
      }

      {

        Given(s"a json to create a portfolios:\n${Json.prettyPrint(createPortfolioJson)}")

        When(s"we create a portfolio")

        val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(createPortfolioJson)

        status(eventualResult) mustEqual OK

        contentType(eventualResult) mustBe Some(MimeTypes.JSON)

        val jsonStateResponse: JsValue = contentAsJson(eventualResult)

        Then(s"we receive back a nice json ${Json.prettyPrint(jsonStateResponse)}")

      }

      {
        val eventuallyFindResult: Future[Result] = invokeRoute.findAllPortfolios()

        status(eventuallyFindResult) mustEqual OK

        contentType(eventuallyFindResult) mustBe Some(MimeTypes.JSON)

        val jsonStateResponse = contentAsJson(eventuallyFindResult)

        When(s"we received back:\n${Json.prettyPrint(jsonStateResponse)}")

        val actualState = jsonStateResponse.as[PortfoliosSummary]

        actualState.portfolios.head.portfolioName mustBe PortfolioName.unsafe(createPortfolioBuilder.name)

        actualState.portfolios.head.positions.size mustBe initialCreatePortfolio.positions.length

        val deleteAllPositions: Seq[PortfoliosModel.Delete] =
          actualState.portfolios.head.positions.map(p => PortfoliosModel.Delete(p.id)).toList

        val deleteAllPositionsJson: JsValue =
          Json.toJson(
            PortfoliosJsonTestContext.NewUpdatePortfolioJsonBuilder(idempotentKey = None, name = createPortfolioBuilder.name, updates = deleteAllPositions)
          )(
            PortfoliosJsonTestContext.jsonwrites.updatePortfolioWrites
          )

        Given(s"a json:\n${Json.prettyPrint(deleteAllPositionsJson)} to delete all positions for portfolio ${createPortfolioBuilder.name}")
        When(s"we update the portfolio")
        val eventualUpdateResult: Future[Result] = invokeRoute.updatePortfolioWithBody(deleteAllPositionsJson)

        status(eventualUpdateResult) mustEqual OK

      }

      {
        val findResult: Future[Result] = invokeRoute.findAllPortfolios()

        status(findResult) mustEqual OK

        val actualState: JsValue = contentAsJson(findResult)

        When(s"we received back:\n${Json.prettyPrint(actualState)}")

        actualState.as[PortfoliosSummary].portfolios.head.positions.size mustBe 0

      }
    }

    "returns bad request and an error message given an invalid portfolio to be created" in new ControllerTestContext(app) {

      private val aValidJson = PortfoliosJsonTestContext.CreatePortfolioJsonBuilder.from(PortfoliosGen.createPortfolio.sample.get)

      val badRequestBodyDueToNoPositions: JsValue = Json.toJson(aValidJson.copy(positions = Nil))(PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites)

      val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(badRequestBodyDueToNoPositions)

      status(eventualResult) mustEqual BAD_REQUEST

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsString(eventualResult) should include("Positions cannot be empty")
    }

    "returns bad request and an error message given a portfolio creation with no idempotent key" in new ControllerTestContext(app) {

      private val aValidJson = PortfoliosJsonTestContext.CreatePortfolioJsonBuilder.from(PortfoliosGen.createPortfolio.sample.get)

      val badRequestBodyDueToMissingIdempotentKey: JsValue =
        Json.toJson(aValidJson.copy(idempotentKey = ""))(PortfoliosJsonTestContext.jsonwrites.createPortfolioWrites)

      Given(s"an invalid json to create a portfolios:\n${Json.prettyPrint(badRequestBodyDueToMissingIdempotentKey)}")

      When(s"we create a portfolio")

      val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(badRequestBodyDueToMissingIdempotentKey)

      status(eventualResult) mustEqual BAD_REQUEST

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      When(s"we received back a valid error:\n${Json.prettyPrint(contentAsJson(eventualResult))}")

      contentAsString(eventualResult) should include("IdempotentKey field has to be a uuid value")
    }

  }
}
