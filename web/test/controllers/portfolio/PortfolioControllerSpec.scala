package controllers.portfolio

import cats.data.NonEmptySet
import cats.syntax.either._
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.string.NonEmptyString
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{CreatePortfolio, UpdatePortfolio}
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel, StateGen}
import org.binqua.forex.web.core.util.expectingJson
import org.mockito.Mockito.{reset, verifyNoInteractions, when}
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import org.scalatestplus.mockito.MockitoSugar.mock
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.http.MimeTypes
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json._
import play.api.mvc.Result
import play.api.test.Helpers.{contentType, status, _}
import play.inject.Bindings
import services.portfolios.{PlayPortfoliosService, RepoServiceConfig, UserId, ValidationFailed}

import scala.concurrent.duration._
import scala.concurrent.{Future, TimeoutException}

class PortfolioControllerSpec extends PlaySpec with GivenWhenThen with GuiceOneAppPerSuite with BeforeAndAfterEach {

  val loggedInUser = new UserId {
    override val id: NonEmptyString = refineMV[NonEmpty]("1")
  }

  object mocks {
    val portfoliosService: PlayPortfoliosService = mock[PlayPortfoliosService]
    val newPortfolioParserFactory: NewPortfolioParserFactory = mock[NewPortfolioParserFactory]
    val updatePortfolioParserFactory: UpdatePortfolioParserFactory = mock[UpdatePortfolioParserFactory]
    val deletePortfoliosParserFactory: DeletePortfoliosParserFactory = mock[DeletePortfoliosParserFactory]
    def resetAll(): Unit = {
      reset(portfoliosService, newPortfolioParserFactory, updatePortfolioParserFactory, deletePortfoliosParserFactory)
    }
  }

  override def afterEach(): Unit = {
    try super.afterEach()
    finally {
      mocks.resetAll()
    }
  }

  override def fakeApplication(): Application = {
    val application = GuiceApplicationBuilder()
      .configure()
      .overrides(Bindings.bind(classOf[PlayPortfoliosService]).to(mocks.portfoliosService))
      .overrides(
        Bindings
          .bind(classOf[RepoServiceConfig])
          .to(new RepoServiceConfig {
            val longEnoughSoDoesNotHappen: FiniteDuration = 10.seconds
            override val timeout: FiniteDuration = longEnoughSoDoesNotHappen
          })
      )
      .overrides(
        Bindings
          .bind(classOf[NewPortfolioParserFactory])
          .to(mocks.newPortfolioParserFactory)
      )
      .overrides(
        Bindings
          .bind(classOf[UpdatePortfolioParserFactory])
          .to(mocks.updatePortfolioParserFactory)
      )
      .overrides(
        Bindings
          .bind(classOf[DeletePortfoliosParserFactory])
          .to(mocks.deletePortfoliosParserFactory)
      )
      .build()
    application
  }

  "findAll" should {

    "returns ok response if portfolios service succeeds" in new ControllerTestContext(app) {

      val thePortfoliosSummary: PortfoliosSummary = StateGen.portfoliosSummary.sample.get

      when(mocks.portfoliosService.findBy(loggedInUser)).thenReturn(Future {
        thePortfoliosSummary.asRight
      }(app.actorSystem.dispatcher))

      val eventualResult: Future[Result] = invokeRoute.findAllPortfolios()

      status(eventualResult) mustEqual OK

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsJson(eventualResult) mustEqual Json.toJson(thePortfoliosSummary)(PortfoliosSummary.jsonWrites)

    }

    "returns service unavailable if portfolios service times out" in new ControllerTestContext(app) {

      when(mocks.portfoliosService.findBy(loggedInUser)).thenReturn(Future.failed(new TimeoutException()))

      assertServiceUnavailableWithTimeoutAfter(route(app, requests.findAllPortfolios).get)

    }

    "returns service unavailable if an exception occurs" in new ControllerTestContext(app) {

      when(mocks.portfoliosService.findBy(loggedInUser)).thenReturn(Future.failed(new RuntimeException("Boom")))

      assertGenericServiceUnavailableAfter(route(app, requests.findAllPortfolios).get)

    }

  }

  "crateANewPortfolio" should {

    "creates a portfolio if body request can be parsed successfully" in new ControllerTestContext(app) {

      val portfolioToBeCreated: CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

      val thePortfoliosSummary: PortfoliosSummary = StateGen.portfoliosSummary.sample.get

      when(mocks.newPortfolioParserFactory.parser).thenReturn(Reads[CreatePortfolio] { incomingJson: JsValue =>
        incomingJson should be(someIncomingFakeJson)
        JsSuccess(portfolioToBeCreated)
      })

      when(mocks.portfoliosService.create(loggedInUser, portfolioToBeCreated)).thenReturn(Future.successful(thePortfoliosSummary.asRight))

      val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(someIncomingFakeJson)

      status(eventualResult) mustEqual OK

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsJson(eventualResult) mustEqual Json.toJson(thePortfoliosSummary)

    }

    "return BadRequest if body request can be parsed successfully but portfolio cannot be created" in new ControllerTestContext(app) {

      val portfolioToBeCreated: CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

      when(mocks.newPortfolioParserFactory.parser).thenReturn(Reads[CreatePortfolio] { _ =>
        JsSuccess(portfolioToBeCreated)
      })

      private val validationFailed: ValidationFailed = ValidationFailed(refineMV("someError"))

      when(mocks.portfoliosService.create(loggedInUser, portfolioToBeCreated)).thenReturn(Future.successful(validationFailed.asLeft))

      val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(someIncomingFakeJson)

      status(eventualResult) mustEqual BAD_REQUEST

      contentType(eventualResult) mustBe Some(MimeTypes.TEXT)

      contentAsString(eventualResult) mustEqual validationFailed.message.value

    }

    "return BadRequest if body request cannot be parsed successfully" in new ControllerTestContext(app) {

      val someJsonErrors: JsError = JsError(JsPath \ "aPath" -> JsonValidationError("someError"))

      when(mocks.newPortfolioParserFactory.parser).thenReturn(Reads[CreatePortfolio] { _ =>
        someJsonErrors
      })

      val eventualResult: Future[Result] = invokeRoute.crateANewPortfolio(someIncomingFakeJson)

      status(eventualResult) mustEqual BAD_REQUEST

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsJson(eventualResult) mustEqual JsError.toJson(someJsonErrors)

      verifyNoInteractions(mocks.portfoliosService)

    }

    "returns service unavailable if portfolios service times out" in new ControllerTestContext(app) {

      val portfolioToBeCreated: CreatePortfolio = PortfoliosGen.createPortfolio.sample.get

      when(mocks.newPortfolioParserFactory.parser).thenReturn(Reads[CreatePortfolio] { _ =>
        JsSuccess(portfolioToBeCreated)
      })

      when(mocks.portfoliosService.create(loggedInUser, portfolioToBeCreated)).thenReturn(Future.failed(new TimeoutException()))

      assertServiceUnavailableWithTimeoutAfter(invokeRoute.crateANewPortfolio(someIncomingFakeJson))

    }
  }

  "updatePortfolios" should {

    "returns bad request in case of empty body" in new ControllerTestContext(app) {

      val result = route(app, requests.updatePortfolioWithoutBody).get

      contentAsJson(result) mustEqual expectingJson

      status(result) mustEqual BAD_REQUEST

      contentType(result) mustBe Some(MimeTypes.JSON)

    }

    "update a portfolio if body request can be parsed successfully" in new ControllerTestContext(app) {

      private val theIncomingUpdatePortfolioRequest: UpdatePortfolio = PortfoliosGen.updatePortfolio.sample.get

      val theUpdatedPortfolio: PortfoliosSummary = StateGen.portfoliosSummary.sample.get

      when(mocks.updatePortfolioParserFactory.parser).thenReturn(Reads[UpdatePortfolio] { someJson =>
        someJson should be(someIncomingFakeJson)
        JsSuccess(theIncomingUpdatePortfolioRequest)
      })

      when(mocks.portfoliosService.update(loggedInUser, theIncomingUpdatePortfolioRequest)).thenReturn(Future.successful(theUpdatedPortfolio.asRight))

      val eventualResult: Future[Result] = invokeRoute.updatePortfolio(someIncomingFakeJson)

      status(eventualResult) mustEqual OK

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsJson(eventualResult) mustEqual Json.toJson(theUpdatedPortfolio)

    }

    "returns service unavailable if portfolios service times out" in new ControllerTestContext(app) {

      private val updatePortfolioFromTheParser: UpdatePortfolio = PortfoliosGen.updatePortfolio.sample.get

      when(mocks.updatePortfolioParserFactory.parser).thenReturn(Reads[UpdatePortfolio] { incomingJson: JsValue =>
        incomingJson should be(someIncomingFakeJson)
        JsSuccess(updatePortfolioFromTheParser)
      })

      when(mocks.portfoliosService.update(loggedInUser, updatePortfolioFromTheParser)).thenReturn(Future.failed(new TimeoutException()))

      assertServiceUnavailableWithTimeoutAfter(invokeRoute.updatePortfolio(someIncomingFakeJson))

    }
  }

  "deletePortfolios" should {

    "returns bad request in case of empty body" in new ControllerTestContext(app) {

      val result = route(app, requests.deletePortfolioWithoutBody).get

      contentAsJson(result) mustEqual expectingJson

      status(result) mustEqual BAD_REQUEST

      contentType(result) mustBe Some(MimeTypes.JSON)

    }

    "deletes all specified portfolios if body request can be parsed successfully" in new ControllerTestContext(app) {

      private val portfoliosToBeDeleted: NonEmptySet[PortfoliosModel.PortfolioName] = NonEmptySet.of(PortfoliosGen.portfolioName.sample.get)

      val thePortfolioSummary: PortfoliosSummary = StateGen.portfoliosSummary.sample.get

      when(mocks.deletePortfoliosParserFactory.parser).thenReturn({ incomingJson: JsValue =>
        incomingJson should be(someIncomingFakeJson)
        JsSuccess(portfoliosToBeDeleted)
      })

      when(mocks.portfoliosService.delete(loggedInUser, portfoliosToBeDeleted)).thenReturn(Future.successful(thePortfolioSummary.asRight))

      val eventualResult: Future[Result] = invokeRoute.delete(someIncomingFakeJson)

      status(eventualResult) mustEqual OK

      contentType(eventualResult) mustBe Some(MimeTypes.JSON)

      contentAsJson(eventualResult) mustEqual Json.toJson(thePortfolioSummary)

    }

    "returns service unavailable if portfolios service times out" in new ControllerTestContext(app) {

      private val portfoliosToBeDeleted: NonEmptySet[PortfoliosModel.PortfolioName] = NonEmptySet.of(PortfoliosGen.portfolioName.sample.get)

      when(mocks.deletePortfoliosParserFactory.parser).thenReturn({ incomingJson: JsValue =>
        incomingJson should be(someIncomingFakeJson)
        JsSuccess(portfoliosToBeDeleted)
      })

      when(mocks.portfoliosService.delete(loggedInUser, portfoliosToBeDeleted)).thenReturn(Future.failed(new TimeoutException()))

      assertServiceUnavailableWithTimeoutAfter(invokeRoute.delete(someIncomingFakeJson))

    }

  }
}
