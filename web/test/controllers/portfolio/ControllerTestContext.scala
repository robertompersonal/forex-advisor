package controllers.portfolio

import akka.stream.Materializer
import org.scalatest.matchers.must.Matchers.convertToAnyMustWrapper
import play.api.Application
import play.api.http.{HeaderNames, HttpVerbs, MimeTypes}
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.mvc.{AnyContentAsEmpty, Headers, Result}
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, contentType, status, writeableOf_AnyContentAsEmpty, _}

import scala.concurrent.Future

class ControllerTestContext(app: Application) {

  val someIncomingFakeJson: JsObject = Json.obj(fields = "a" -> 1)

  implicit val mat: Materializer = app.materializer

  val controllerUnderTest: PortfolioController = app.injector.instanceOf[PortfolioController]

  object requests {

    val findAllPortfolios: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.GET, "/api/portfolios.json").withHeaders(
      Headers(
        HeaderNames.HOST -> "localhost"
      )
    )

    val updatePortfolioWithoutBody: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.PATCH, "/api/portfolios")
      .withHeaders(
        Headers(
          HeaderNames.CONTENT_TYPE -> MimeTypes.JSON,
          HeaderNames.HOST -> "localhost"
        )
      )

    val deletePortfolioWithoutBody: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(HttpVerbs.DELETE, "/api/portfolios")
      .withHeaders(
        Headers(
          HeaderNames.CONTENT_TYPE -> MimeTypes.JSON,
          HeaderNames.HOST -> "localhost"
        )
      )

    val createANewPortfolioWithoutBody: FakeRequest[AnyContentAsEmpty.type] =
      FakeRequest(HttpVerbs.POST, "/api/portfolios")
        .withHeaders(
          Headers(
            HeaderNames.CONTENT_TYPE -> MimeTypes.JSON,
            HeaderNames.HOST -> "localhost"
          )
        )

    def addHeader(request: FakeRequest[AnyContentAsEmpty.type], header: (String, String)): FakeRequest[AnyContentAsEmpty.type] = {
      request.withHeaders(request.headers.add(header))
    }
  }

  object invokeRoute {

    def delete(someIncomingJson: JsObject): Future[Result] =
      route(
        app,
        requests.deletePortfolioWithoutBody.withBody(someIncomingJson)
      ).get

    def updatePortfolio(someIncomingJson: JsObject): Future[Result] =
      route(
        app,
        requests.updatePortfolioWithoutBody.withBody(someIncomingJson)
      ).get

    val updatePortfolioWithBody: JsValue => Future[Result] = (updatePortfolio: JsValue) =>
      route(
        app,
        requests.updatePortfolioWithoutBody.withBody(updatePortfolio)
      ).get

    val findAllPortfolios: () => Future[Result] = () => route(app, requests.findAllPortfolios).get

    val crateANewPortfolio: JsValue => Future[Result] = (createPortfolioJson: JsValue) => {
      val value = requests.createANewPortfolioWithoutBody.withBody(createPortfolioJson)
      route(app, value).get
    }

  }

  def assertServiceUnavailableWithTimeoutAfter(eventualResult: Future[Result]): Unit = {

    assertServiceUnavailableWithErrorAfterCalling(eventualResult, "timeout")
  }

  def assertGenericServiceUnavailableAfter(eventualResult: Future[Result]): Unit = {

    assertServiceUnavailableWithErrorAfterCalling(eventualResult, "Ops!! Something went wrong")
  }

  def assertServiceUnavailableWithErrorAfterCalling(eventualResult: Future[Result], expectedError: String): Unit = {

    status(eventualResult) mustEqual SERVICE_UNAVAILABLE

    contentType(eventualResult) mustBe Some(MimeTypes.JSON)

    contentAsString(eventualResult) mustEqual Json.stringify(org.binqua.forex.web.core.util.singleError(expectedError))
  }

}
