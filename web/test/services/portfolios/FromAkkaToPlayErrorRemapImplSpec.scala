package services.portfolios

import cats.syntax.either._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.numeric.{NonNegative, Positive}
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.StateGen
import org.binqua.forex.advisor.portfolios.service.PortfoliosService.DeliverySucceeded
import org.binqua.forex.advisor.portfolios.service.{Config, PortfoliosService}
import org.scalacheck.Gen
import org.scalatest.matchers.should.Matchers._
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks.forAll

class FromAkkaToPlayErrorRemapImplSpec extends AnyPropSpec {

  property("DeliverySucceeded should be mapped to Portfolios") {
    forAll(StateGen.portfoliosSummary) { (portfolios: PortfoliosSummary) =>
      FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(DeliverySucceeded(portfolios)) should be(portfolios.asRight)
    }
  }

  property("ValidationFailed should be mapped to a Play ValidationFailed") {
    val nonEmptyString: Gen[Refined[String, NonEmpty]] = for {
      aString <- Gen.alphaStr
    } yield refineV[NonEmpty](aString).getOrElse(refineMV("some string"))

    forAll(nonEmptyString) { (error: Refined[String, NonEmpty]) =>
      FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(PortfoliosService.ValidationFailed(error)) should be(ValidationFailed(error).asLeft)
    }
  }

  property("TooManyRetries should be mapped to a Play TheServerIsExperiencingSomeProblemTryLater") {
    val numOfRetries: Gen[Config.RetryAttempts] = for {
      retries <- Gen.choose(0, 100)
    } yield refineV[NonNegative](retries).getOrElse(refineMV(0))

    forAll(numOfRetries) { numOfRetries =>
      FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(PortfoliosService.TooManyRetries(numOfRetries)) should be(TheServerIsExperiencingSomeProblemTryLater.asLeft)
    }
  }

  property("Rejected should be mapped to a Play TheServerIsExperiencingSomeProblemTryLater") {
    val maxInFlightMessages: Gen[Config.MaxInFlightMessages] = for {
      retries <- Gen.choose(1, 100)
    } yield refineV[Positive](retries).getOrElse(refineMV(2))

    forAll(maxInFlightMessages) { maxInFlightMessages =>
      FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(PortfoliosService.Rejected(maxInFlightMessages)) should be(
        TheServerIsExperiencingSomeProblemTryLater.asLeft
      )
    }
  }

  property("TimedOut should be mapped to a Play TheServerIsExperiencingSomeProblemTryLater") {
    FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(PortfoliosService.TimedOut) should be(TheServerIsExperiencingSomeProblemTryLater.asLeft)
  }

  property("ServerError should be mapped to a Play TheServerIsExperiencingSomeProblemTryLater") {
    FromAkkaToPlayErrorRemapImpl.fromAkkaResponse(PortfoliosService.ServerError) should be(TheServerIsExperiencingSomeProblemTryLater.asLeft)
  }

}
