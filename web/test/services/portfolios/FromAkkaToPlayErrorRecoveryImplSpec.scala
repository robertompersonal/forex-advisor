package services.portfolios

import org.binqua.forex.advisor.portfolios.service.PortfoliosService
import org.scalatest.matchers.should.Matchers._
import org.scalatest.propspec.AnyPropSpec

import java.util.concurrent.TimeoutException

class FromAkkaToPlayErrorRecoveryImplSpec extends AnyPropSpec {

  property("TimeoutException should recover with TimedOut") {
    FromAkkaToPlayErrorRecoveryImpl.recoveryFromThrowable(new TimeoutException()) should be(PortfoliosService.TimedOut)
  }

  property("Throwable should recover with ServerError") {
    FromAkkaToPlayErrorRecoveryImpl.recoveryFromThrowable(new Throwable()) should be(PortfoliosService.ServerError)
  }

}
