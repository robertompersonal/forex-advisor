package services.portfolios

import akka.actor.typed.scaladsl.Behaviors
import cats.data
import cats.data.NonEmptySet
import controllers.portfolio.TestingConfiguration
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.{refineMV, refineV}
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.{CreatePortfolioPayload, DeletePortfoliosPayload, ShowPortfoliosPayload}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{PortfolioName, UpdatePortfolio}
import org.binqua.forex.advisor.portfolios.service.PortfoliosService.{DeliverySucceeded, Envelope}
import org.binqua.forex.advisor.portfolios.service.{PortfoliosService, PortfoliosServiceModule}
import org.binqua.forex.advisor.portfolios.{PortfoliosGen, PortfoliosModel, StateGen}
import org.binqua.forex.util.{Bug, Util}
import org.scalacheck.Gen
import org.scalatest.matchers.should.Matchers._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.{Application, Configuration}
import play.inject.Bindings._
import services.portfolios.AkkaPortfoliosServiceImplSpec.testContext

import scala.concurrent.duration._
import scala.concurrent.{Await, Awaitable}

class AkkaPortfoliosServiceImplSpec extends PlaySpec with GuiceOneAppPerSuite {

  def failBecauseThisShouldNotHappen: Nothing = fail(message = "ops! this should not happen")

  override def fakeApplication(): Application = {
    GuiceApplicationBuilder()
      .overrides(bind(classOf[PortfoliosServiceModule]).toProvider[StubbedAkkaPortfoliosServiceModuleWithDummyActor])
      .configure(
        Configuration(
          TestingConfiguration
            .config(
              Util.findRandomOpenPortOnAllLocalInterfaces(),
              Util.findRandomOpenPortOnAllLocalInterfaces(),
              shouldThisActorSystemHostPersistedInstances = true
            )
        )
      )
      .configure("services.portfolios.timeout" -> "200ms")
      .build()
  }

  val akkaPortfoliosServiceUnderTest = app.injector.instanceOf[AkkaPortfoliosService]

  def waitForResultFrom[T](awaitable: Awaitable[T]): T = Await.result(awaitable, 4.seconds)

  "AkkaPortfoliosService" should {

    "create the right portfolio" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.create(testContext.aPlayLoggedInUser, testContext.aCreatePortfolioPayloadThatSucceed.thePayload.portfolioToBeCreated)
      ) must be theSameInstanceAs testContext.aCreatePortfolioPayloadThatSucceed.deliverySucceeded
    }

    "received right error if portfolio creation failed" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.create(
          testContext.aPlayLoggedInUser,
          testContext.aPortfolioCreationWithValidationProblems.aPortfolioToBeCreated
        )
      ) must be(
        testContext.aPortfolioCreationWithValidationProblems.theError
      )
    }

    "received TimedOut if portfolio creation times out" in {

      val askTimedOutError: PortfoliosService.Response = waitForResultFrom(
        akkaPortfoliosServiceUnderTest.create(testContext.aPlayLoggedInUser, portfolioToBeCreated = testContext.aPayloadThatTimesOut.portfolioToBeCreated)
      )
      askTimedOutError must be(PortfoliosService.TimedOut)

    }

    "delete the right portfolio" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.delete(testContext.aPlayLoggedInUser, testContext.aPortfolioThatCanBeDeleted)
      ) must be theSameInstanceAs testContext.deliverySucceeded
    }

    "received right error if delete failed" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.delete(testContext.aPlayLoggedInUser, testContext.delete.aPortfolioWithValidationProblems.toBeDeleted)
      ) must be theSameInstanceAs testContext.delete.aPortfolioWithValidationProblems.theError
    }

    "received TimedOut if delete times out" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.delete(testContext.delete.aPlayLoggedInThatTimesOut, NonEmptySet.of(PortfoliosGen.portfolioName.sample.get))
      ) must be(PortfoliosService.TimedOut)
    }

    "find the right portfolio state" in {

      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.findBy(testContext.aPlayLoggedInUser)
      ) must be theSameInstanceAs testContext.deliverySucceeded
    }

    "received right error if find state failed" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.findBy(testContext.aFindByWithValidationProblems.aUserThatCannotFindState)
      ) must be(testContext.aFindByWithValidationProblems.theError)
    }

    "received TimedOut if findBy times out" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.findBy(testContext.aPlayUserThatFindByTimedOut)
      ) must be(PortfoliosService.TimedOut)
    }

    "play can restart the actor if it crashes" in {

      akkaPortfoliosServiceUnderTest.create(testContext.aPlayLoggedInUser, portfolioToBeCreated = testContext.aPayloadThatCrashesTheActor.portfolioToBeCreated)

      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.create(testContext.aPlayLoggedInUser, testContext.aCreatePortfolioPayloadThatSucceed.thePayload.portfolioToBeCreated)
      ) must be theSameInstanceAs testContext.aCreatePortfolioPayloadThatSucceed.deliverySucceeded
    }

    "update a portfolio" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.update(testContext.aPlayLoggedInUser, testContext.update.anUpdatePortfolioPayloadThatSucceed.thePayload.updatePortfolio)
      ) must be theSameInstanceAs testContext.update.anUpdatePortfolioPayloadThatSucceed.deliverySucceeded
    }

    "received right error if portfolio update fails" in {
      waitForResultFrom(
        akkaPortfoliosServiceUnderTest.update(testContext.aPlayLoggedInUser, testContext.update.anUpdatePortfolioPayloadThatFails.thePayload.updatePortfolio)
      ) must be(testContext.update.anUpdatePortfolioPayloadThatFails.theError)
    }

    "received TimedOut if portfolio update times out" in {
      val askTimedOutError: PortfoliosService.Response = waitForResultFrom(
        akkaPortfoliosServiceUnderTest
          .update(
            testContext.update.anUpdatePortfolioPayloadThatTimesOut.thePlayUser,
            testContext.update.anUpdatePortfolioPayloadThatTimesOut.thePayload.updatePortfolio
          )
      )
      askTimedOutError must be(PortfoliosService.TimedOut)

    }

  }
}

object AkkaPortfoliosServiceImplSpec {

  val aDeletePortfolioTimesOutUser: NonEmptyString =
    refineV[NonEmpty](Gen.choose(Int.MinValue, Int.MaxValue).sample.get.toString).getOrElse(throw new Bug(":("))

  object testContext {

    object delete {
      val aPlayLoggedInThatTimesOut = new UserId {
        override val id: NonEmptyString = aDeletePortfolioTimesOutUser
      }

      val aPortfolioWithValidationProblems = DeleteWithValidationProblems(
        aPortfolioThatCannotBeDeleted,
        PortfoliosService.ValidationFailed(refineMV[NonEmpty]("this portfolio cannot be deleted crappy reason"))
      )
    }

    object update {

      case class AValidPortfolioUpdate(thePayload: DeliveryGuaranteedPortfolios.UpdatePortfolioPayload, deliverySucceeded: DeliverySucceeded)

      val anUpdatePortfolioPayloadThatSucceed = AValidPortfolioUpdate(
        DeliveryGuaranteedPortfolios.UpdatePortfolioPayload(PortfoliosGen.updatePortfolio.sample.get),
        deliverySucceeded
      )

      case class APortfolioUpdateThatFails(thePayload: DeliveryGuaranteedPortfolios.UpdatePortfolioPayload, theError: PortfoliosService.ValidationFailed)

      val anUpdatePortfolioPayloadThatFails = APortfolioUpdateThatFails(
        DeliveryGuaranteedPortfolios.UpdatePortfolioPayload(PortfoliosGen.updatePortfolio.sample.get),
        PortfoliosService.ValidationFailed(refineMV[NonEmpty]("Portfolio validation failed"))
      )

      val anUpdatePortfolioPayloadThatTimesOut = ATimedOutPortfolioUpdate(
        userId = idOfAUserThatTimedOut,
        thePayload = DeliveryGuaranteedPortfolios.UpdatePortfolioPayload(PortfoliosGen.updatePortfolio.sample.get)
      )

    }

    val aPortfolioThatCanBeDeleted = NonEmptySet.of(PortfoliosGen.portfolioName.sample.get)

    val aPortfolioThatCannotBeDeleted = NonEmptySet.of(PortfoliosGen.portfolioName.sample.get)

    val deliverySucceeded: DeliverySucceeded = DeliverySucceeded(StateGen.portfoliosSummary.sample.get)

    val loggedInUserId: NonEmptyString = refineMV[NonEmpty]("1")

    val aPlayLoggedInUser = new UserId {
      override val id: NonEmptyString = loggedInUserId
    }

    val idOfAUserThatCannotFindState: NonEmptyString = refineMV[NonEmpty]("2")

    val aPlayUserThatCannotFindState = new UserId {
      override val id: NonEmptyString = idOfAUserThatCannotFindState
    }

    val idOfAUserThatTimedOut: NonEmptyString = refineMV[NonEmpty]("3")

    val aPlayUserThatFindByTimedOut = new UserId {
      override val id: NonEmptyString = idOfAUserThatTimedOut
    }

    def aPayload(): CreatePortfolioPayload = DeliveryGuaranteedPortfolios.CreatePortfolioPayload(PortfoliosGen.createPortfolio.sample.get)

    final case class AValidPortfolioCreation(thePayload: DeliveryGuaranteedPortfolios.CreatePortfolioPayload, deliverySucceeded: DeliverySucceeded)

    val aCreatePortfolioPayloadThatSucceed = AValidPortfolioCreation(
      DeliveryGuaranteedPortfolios.CreatePortfolioPayload(PortfoliosGen.createPortfolio.sample.get),
      deliverySucceeded
    )

    final case class PortfolioCreationWithValidationProblems(
        aPortfolioToBeCreated: PortfoliosModel.CreatePortfolio,
        theError: PortfoliosService.ValidationFailed
    )

    val aPortfolioCreationWithValidationProblems =
      PortfolioCreationWithValidationProblems(
        PortfoliosGen.createPortfolio.sample.get,
        PortfoliosService.ValidationFailed(refineMV[NonEmpty]("Portfolio validation failed"))
      )

    final case class FindByWithValidationProblems(aUserThatCannotFindState: UserId, theError: PortfoliosService.ValidationFailed)

    val aFindByWithValidationProblems = FindByWithValidationProblems(
      aPlayUserThatCannotFindState,
      PortfoliosService.ValidationFailed(refineMV[NonEmpty]("You are not allowed to findBy"))
    )

    final case class DeleteWithValidationProblems(toBeDeleted: data.NonEmptySet[PortfolioName], theError: PortfoliosService.ValidationFailed)

    val aPayloadThatTimesOut = aPayload()

    val aPayloadThatCrashesTheActor = aPayload()

    val updatePortfolioThatSucceed: PortfoliosModel.UpdatePortfolio = PortfoliosGen.updatePortfolio.sample.get

    val updatePortfolioThatFailed: PortfoliosModel.UpdatePortfolio = PortfoliosGen.updatePortfolio.sample.get

    val stubbedPortfoliosBehavior: Behaviors.Receive[PortfoliosService.Message] = Behaviors.receiveMessage {
      case envelope @ Envelope(_, DeletePortfoliosPayload(_), _) =>
        envelope match {
          case Envelope(`loggedInUserId`, DeletePortfoliosPayload(`aPortfolioThatCanBeDeleted`), replyTo) =>
            replyTo ! deliverySucceeded
            Behaviors.same
          case Envelope(`aDeletePortfolioTimesOutUser`, _, _) =>
            Behaviors.same
          case Envelope(`loggedInUserId`, DeletePortfoliosPayload(`aPortfolioThatCannotBeDeleted`), replyTo) =>
            replyTo ! delete.aPortfolioWithValidationProblems.theError
            Behaviors.same
          case msg => throw new Bug(details = s"I should not have received $msg")
        }
      case envelope @ Envelope(_, ShowPortfoliosPayload, _) =>
        envelope match {
          case Envelope(`loggedInUserId`, ShowPortfoliosPayload, replyTo) =>
            replyTo ! deliverySucceeded
            Behaviors.same
          case Envelope(`idOfAUserThatCannotFindState`, ShowPortfoliosPayload, replyTo) =>
            replyTo ! aFindByWithValidationProblems.theError
            Behaviors.same
          case Envelope(`idOfAUserThatTimedOut`, ShowPortfoliosPayload, _) =>
            Behaviors.same
          case msg => throw new Bug(details = s"I should not have received $msg")
        }
      case Envelope(`loggedInUserId`, `aPayloadThatCrashesTheActor`, _) =>
        throw new RuntimeException("I am going to crash, but dont worry because I should be recreated")
        Behaviors.same
      case Envelope(`loggedInUserId`, `aPayloadThatTimesOut`, _) =>
        Behaviors.same
      case Envelope(`loggedInUserId`, DeliveryGuaranteedPortfolios.CreatePortfolioPayload(portfolioToBeCreated), replyTo) =>
        if (portfolioToBeCreated == aCreatePortfolioPayloadThatSucceed.thePayload.portfolioToBeCreated)
          replyTo ! aCreatePortfolioPayloadThatSucceed.deliverySucceeded
        else if (portfolioToBeCreated == aPortfolioCreationWithValidationProblems.aPortfolioToBeCreated)
          replyTo ! aPortfolioCreationWithValidationProblems.theError
        Behaviors.same
      case Envelope(`idOfAUserThatTimedOut`, DeliveryGuaranteedPortfolios.UpdatePortfolioPayload(_), _) =>
        Behaviors.same
      case Envelope(`loggedInUserId`, DeliveryGuaranteedPortfolios.UpdatePortfolioPayload(actualUpdatePortfolio), replyTo) =>
        if (UpdatePortfolio.equality.eqv(actualUpdatePortfolio, update.anUpdatePortfolioPayloadThatSucceed.thePayload.updatePortfolio))
          replyTo ! update.anUpdatePortfolioPayloadThatSucceed.deliverySucceeded
        Behaviors.same
        if (UpdatePortfolio.equality.eqv(actualUpdatePortfolio, update.anUpdatePortfolioPayloadThatFails.thePayload.updatePortfolio))
          replyTo ! update.anUpdatePortfolioPayloadThatFails.theError
        Behaviors.same
      case msg => throw new Bug(details = s"I should not have received $msg")
    }

  }

  case class ATimedOutPortfolioUpdate(userId: Refined[String, NonEmpty], thePayload: DeliveryGuaranteedPortfolios.UpdatePortfolioPayload) {
    val thePlayUser = new UserId {
      override val id: NonEmptyString = userId
    }
  }

}

case class AValidPortfolioUpdate(thePayload: DeliveryGuaranteedPortfolios.UpdatePortfolioPayload, deliverySucceeded: DeliverySucceeded)
