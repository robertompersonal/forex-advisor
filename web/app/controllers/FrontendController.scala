package controllers

import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.{Configuration, Logger}

import javax.inject._

/**
  * Frontend controller managing all static resource associate routes.
  *
  * @param assets Assets controller reference.
  * @param cc     Controller components reference.
  */
@Singleton
class FrontendController @Inject() (assets: Assets, errorHandler: HttpErrorHandler, config: Configuration, cc: ControllerComponents)
    extends AbstractController(cc) {

  val logger: Logger = Logger(this.getClass)

  def index: Action[AnyContent] = assets.at("index.html")

  def assetOrDefault(resource: String): Action[AnyContent] = {
    logger.debug(s"incoming resource $resource")
    if (resource.startsWith(config.get[String]("apiPrefix"))) {
      Action.async(r => errorHandler.onClientError(r, NOT_FOUND, "Not found"))
    } else {
      if (resource.contains(".")) assets.at(resource) else index
    }
  }
}
