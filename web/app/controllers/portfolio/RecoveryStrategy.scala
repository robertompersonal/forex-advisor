package controllers.portfolio

import org.binqua.forex.web.core.util.singleError
import play.api.mvc.Result
import play.api.mvc.Results.ServiceUnavailable

import scala.concurrent.TimeoutException

trait RecoveryStrategy {
  val instance: PartialFunction[Throwable, Result]
}

object DefaultRecoveryStrategyImpl extends RecoveryStrategy {
  val instance: PartialFunction[Throwable, Result] = {
    case _: TimeoutException => ServiceUnavailable(singleError("timeout"))
    case _: Throwable        => ServiceUnavailable(singleError("Ops!! Something went wrong"))
  }
}
