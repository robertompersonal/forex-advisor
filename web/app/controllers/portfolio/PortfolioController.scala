package controllers.portfolio

import cats.data.NonEmptySet
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.refineMV
import eu.timepit.refined.types.string.NonEmptyString
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.PortfoliosModel.{CreatePortfolio, PortfolioName, UpdatePortfolio}
import org.binqua.utils.controllerUtils._
import play.api.libs.json._
import play.api.mvc.{AnyContent, _}
import services.portfolios
import services.portfolios._

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PortfolioController @Inject() (
    cc: ControllerComponents,
    portfoliosService: PlayPortfoliosService,
    newPortfolioParserFactory: NewPortfolioParserFactory,
    updatePortfolioParserFactory: UpdatePortfolioParserFactory,
    deletePortfolioParserFactory: DeletePortfoliosParserFactory,
    theRecoveryStrategy: RecoveryStrategy
)(implicit ec: ExecutionContext)
    extends AbstractController(cc) {

  val loggedInUser: UserId = new UserId {
    override val id: NonEmptyString = refineMV[NonEmpty]("1")
  }

  val createNewPortfolioHandler: JsValue => Future[Result] = (someJson: JsValue) => {
    badRequestIfNotValidJson(
      () => someJson.validate[CreatePortfolio](newPortfolioParserFactory.parser),
      (aPortfolioToBeCreated: CreatePortfolio) => {
        portfoliosService
          .create(loggedInUser, aPortfolioToBeCreated)
          .map({
            case Right(portfolioSummary) => Ok(Json.toJson(portfolioSummary)(PortfoliosSummary.jsonWrites))
            case Left(error)             => fromErrorToHttpError(error)
          })
          .recover(theRecoveryStrategy.instance)
      }
    )
  }

  val deletePortfoliosHandler: JsValue => Future[Result] = (someJson: JsValue) => {
    badRequestIfNotValidJson(
      () => someJson.validate[NonEmptySet[PortfolioName]](deletePortfolioParserFactory.parser),
      (toBeDeleted: NonEmptySet[PortfolioName]) => {
        portfoliosService
          .delete(loggedInUser, toBeDeleted)
          .map({
            case Right(portfolioSummary) => Ok(Json.toJson(portfolioSummary)(PortfoliosSummary.jsonWrites))
            case Left(error)             => fromErrorToHttpError(error)
          })
          .recover(theRecoveryStrategy.instance)
      }
    )
  }

  val updatePortfolioHandler: JsValue => Future[Result] = (someJson: JsValue) => {
    badRequestIfNotValidJson(
      () => someJson.validate[UpdatePortfolio](updatePortfolioParserFactory.parser),
      (updateInstructions: UpdatePortfolio) => {
        portfoliosService
          .update(loggedInUser, updateInstructions)
          .map({
            case Right(portfolioSummary) => Ok(Json.toJson(portfolioSummary)(PortfoliosSummary.jsonWrites))
            case Left(error)             => fromErrorToHttpError(error)
          })
          .recover(theRecoveryStrategy.instance)
      }
    )
  }

  def crateANewPortfolio(): Action[AnyContent] = Action.async({ request: Request[AnyContent] => onlyJsonBody(request, createNewPortfolioHandler) })

  def deletePortfolios(): Action[AnyContent] = Action.async({ request: Request[AnyContent] => onlyJsonBody(request, deletePortfoliosHandler) })

  def findAll(): Action[AnyContent] =
    Action.async({
      portfoliosService
        .findBy(loggedInUser)
        .map({
          case Right(portfolioSummary) => Ok(Json.toJson(portfolioSummary)(PortfoliosSummary.jsonWrites))
          case Left(error)             => fromErrorToHttpError(error)
        })
        .recover(theRecoveryStrategy.instance)
    })

  def updatePortfolio(): Action[AnyContent] = Action.async({ request: Request[AnyContent] => onlyJsonBody(request, updatePortfolioHandler) })

  private def fromErrorToHttpError(error: portfolios.Error): Result =
    error match {
      case ValidationFailed(error)                            => BadRequest(error.value)
      case _: TheRequestTimedOut.type                         => RequestTimeout
      case _: TheServerIsExperiencingSomeProblemTryLater.type => TooManyRequests
    }
}
