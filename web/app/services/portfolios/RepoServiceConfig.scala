package services.portfolios

import scala.concurrent.duration.FiniteDuration

trait RepoServiceConfig {
  val timeout: FiniteDuration
}
