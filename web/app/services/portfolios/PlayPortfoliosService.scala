package services.portfolios

import cats.data.NonEmptySet
import org.binqua.forex.advisor.newportfolios._
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios._

import scala.concurrent.Future

trait PlayPortfoliosService {
  def create(userId: UserId, portfolioToBeCreated: PortfoliosModel.CreatePortfolio): Future[Either[Error, PortfoliosSummary]]

  def delete(userId: UserId, toBeDeleted: NonEmptySet[PortfolioName]): Future[Either[Error, PortfoliosSummary]]

  def findBy(userId: UserId): Future[Either[Error, PortfoliosSummary]]

  def update(userId: UserId, updatePortfolio: PortfoliosModel.UpdatePortfolio): Future[Either[Error, PortfoliosSummary]]
}
