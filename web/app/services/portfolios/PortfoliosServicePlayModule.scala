package services.portfolios

import com.google.inject.AbstractModule
import org.binqua.forex.advisor.portfolios.service.PortfoliosServiceModule
import play.api.libs.concurrent.AkkaGuiceSupport

class PortfoliosServicePlayModule extends AbstractModule with AkkaGuiceSupport {

  override protected def configure(): Unit = {
    bind(classOf[PlayPortfoliosService]).to(classOf[FromPlayToAkkaPortfoliosServiceImpl])
    bind(classOf[AkkaPortfoliosService]).to(classOf[AkkaPortfoliosServiceImpl])
    bind(classOf[FromAkkaToPlayErrorRemap]).toInstance(FromAkkaToPlayErrorRemapImpl)
    bind(classOf[FromAkkaToPlayErrorRecovery]).toInstance(FromAkkaToPlayErrorRecoveryImpl)
    bind(classOf[RepoServiceConfig]).to(classOf[DefaultRepoServiceConfig])
    bind(classOf[PortfoliosServiceModule]).toProvider(classOf[PortfoliosServiceModuleProvider])
    bindTypedActor(PortfoliosServiceActor, name = "portfolios-service-actor")
  }

}
