package services.portfolios

import cats.data.NonEmptySet
import org.binqua.forex.advisor.portfolios.PortfoliosModel
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios.service.PortfoliosService

import scala.concurrent.Future

trait AkkaPortfoliosService {

  def create(userId: UserId, portfolioToBeCreated: PortfoliosModel.CreatePortfolio): Future[PortfoliosService.Response]

  def delete(userId: UserId, toBeDeleted: NonEmptySet[PortfolioName]): Future[PortfoliosService.Response]

  def findBy(userId: UserId): Future[PortfoliosService.Response]

  def update(userId: UserId, updatePortfolio: PortfoliosModel.UpdatePortfolio): Future[PortfoliosService.Response]

}
