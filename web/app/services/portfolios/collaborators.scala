package services.portfolios

import eu.timepit.refined.types.all.NonEmptyString

trait Error

final case class ValidationFailed(message: NonEmptyString) extends Error

object TheServerIsExperiencingSomeProblemTryLater extends Error

object TheRequestTimedOut extends Error
