package services.portfolios

import cats.data.NonEmptySet
import org.binqua.forex.advisor.newportfolios._
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios._

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class FromPlayToAkkaPortfoliosServiceImpl @Inject() (
    akkaPortfoliosService: AkkaPortfoliosService,
    fromAkkaToPlayErrorRemap: FromAkkaToPlayErrorRemap
)(implicit ex: ExecutionContext)
    extends PlayPortfoliosService {

  override def create(userId: UserId, portfolioToBeCreated: PortfoliosModel.CreatePortfolio): Future[Either[Error, PortfoliosSummary]] =
    akkaPortfoliosService
      .create(userId, portfolioToBeCreated)
      .map(fromAkkaToPlayErrorRemap.fromAkkaResponse)

  override def delete(userId: UserId, toBeDeleted: NonEmptySet[PortfolioName]): Future[Either[Error, PortfoliosSummary]] =
    akkaPortfoliosService
      .delete(userId, toBeDeleted)
      .map(fromAkkaToPlayErrorRemap.fromAkkaResponse)

  override def findBy(userId: UserId): Future[Either[Error, PortfoliosSummary]] =
    akkaPortfoliosService
      .findBy(userId)
      .map(fromAkkaToPlayErrorRemap.fromAkkaResponse)

  override def update(userId: UserId, updatePortfolio: PortfoliosModel.UpdatePortfolio): Future[Either[Error, PortfoliosSummary]] =
    akkaPortfoliosService
      .update(userId, updatePortfolio)
      .map(fromAkkaToPlayErrorRemap.fromAkkaResponse)

}
