package services.portfolios

import cats.syntax.either._
import org.binqua.forex.advisor.newportfolios.PortfoliosSummary
import org.binqua.forex.advisor.portfolios.service.PortfoliosService

import java.util.concurrent.TimeoutException

trait FromAkkaToPlayErrorRemap {
  def fromAkkaResponse(response: PortfoliosService.Response): Either[services.portfolios.Error, PortfoliosSummary]
}

object FromAkkaToPlayErrorRemapImpl extends FromAkkaToPlayErrorRemap {
  override def fromAkkaResponse(response: PortfoliosService.Response): Either[services.portfolios.Error, PortfoliosSummary] =
    response match {
      case PortfoliosService.DeliverySucceeded(portfoliosState) => portfoliosState.asRight
      case PortfoliosService.Rejected(_)                        => TheServerIsExperiencingSomeProblemTryLater.asLeft
      case PortfoliosService.TimedOut                           => TheServerIsExperiencingSomeProblemTryLater.asLeft
      case PortfoliosService.TooManyRetries(_)                  => TheServerIsExperiencingSomeProblemTryLater.asLeft
      case PortfoliosService.ServerError                        => TheServerIsExperiencingSomeProblemTryLater.asLeft
      case PortfoliosService.ValidationFailed(errorMessage)     => ValidationFailed(errorMessage).asLeft
    }
}

trait FromAkkaToPlayErrorRecovery {
  def recoveryFromThrowable(throwable: Throwable): PortfoliosService.Response
}

object FromAkkaToPlayErrorRecoveryImpl extends FromAkkaToPlayErrorRecovery {
  override def recoveryFromThrowable(throwable: Throwable): PortfoliosService.Response =
    throwable match {
      case _: TimeoutException => PortfoliosService.TimedOut
      case _: Throwable        => PortfoliosService.ServerError
    }
}
