package services.portfolios

import akka.actor.typed.scaladsl.AskPattern.Askable
import akka.actor.typed.{ActorRef, Scheduler}
import akka.util.Timeout
import cats.data.NonEmptySet
import org.binqua.forex.advisor.newportfolios.DeliveryGuaranteedPortfolios.{
  CreatePortfolioPayload,
  DeletePortfoliosPayload,
  ShowPortfoliosPayload,
  UpdatePortfolioPayload
}
import org.binqua.forex.advisor.portfolios.PortfoliosModel.PortfolioName
import org.binqua.forex.advisor.portfolios._
import org.binqua.forex.advisor.portfolios.service.PortfoliosService

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AkkaPortfoliosServiceImpl @Inject() (
    portfoliosServiceActor: ActorRef[PortfoliosService.Message],
    portfoliosServiceConfig: RepoServiceConfig,
    fromAkkaToPlayErrorRecovery: FromAkkaToPlayErrorRecovery
)(implicit
    scheduler: Scheduler,
    ex: ExecutionContext
) extends AkkaPortfoliosService {

  implicit val timeout: Timeout = portfoliosServiceConfig.timeout

  override def create(userId: UserId, portfolioToBeCreated: PortfoliosModel.CreatePortfolio): Future[PortfoliosService.Response] =
    portfoliosServiceActor
      .ask(theActorThatWillAsk => PortfoliosService.Envelope(userId.id, CreatePortfolioPayload(portfolioToBeCreated), theActorThatWillAsk))
      .recover(fromAkkaToPlayErrorRecovery.recoveryFromThrowable(_))

  override def delete(userId: UserId, toBeDeleted: NonEmptySet[PortfolioName]): Future[PortfoliosService.Response] =
    portfoliosServiceActor
      .ask(theActorThatWillAsk => PortfoliosService.Envelope(userId.id, DeletePortfoliosPayload(toBeDeleted), theActorThatWillAsk))
      .recover(fromAkkaToPlayErrorRecovery.recoveryFromThrowable(_))

  override def findBy(userId: UserId): Future[PortfoliosService.Response] =
    portfoliosServiceActor
      .ask(theActorThatWillAsk => PortfoliosService.Envelope(userId.id, ShowPortfoliosPayload, theActorThatWillAsk))
      .recover(fromAkkaToPlayErrorRecovery.recoveryFromThrowable(_))

  override def update(userId: UserId, updatePortfolio: PortfoliosModel.UpdatePortfolio): Future[PortfoliosService.Response] =
    portfoliosServiceActor
      .ask(theActorThatWillAsk => PortfoliosService.Envelope(userId.id, UpdatePortfolioPayload(updatePortfolio), theActorThatWillAsk))
      .recover(fromAkkaToPlayErrorRecovery.recoveryFromThrowable(_))

}
