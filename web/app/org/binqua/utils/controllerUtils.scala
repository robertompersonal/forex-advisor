package org.binqua.utils

import org.binqua.forex.web.core.util.expectingJson
import play.api.libs.json.{JsError, JsResult, JsSuccess, JsValue}
import play.api.mvc.Results.BadRequest
import play.api.mvc.{AnyContent, Request, Result}

import scala.concurrent.Future

object controllerUtils {

  def onlyJsonBody(request: Request[AnyContent], f: JsValue => Future[Result]): Future[Result] = {
    val body: Option[JsValue] = request.body.asJson
    body match {
      case Some(someJson) =>
        f(someJson)
      case None =>
        Future.successful {
          BadRequest(expectingJson)
        }
    }
  }

  def badRequestIfNotValidJson[A](parse: () => JsResult[A], toResult: A => Future[Result]): Future[Result] = {
    parse() match {
      case JsSuccess(valid, _) => toResult(valid)
      case jse: JsError        => Future.successful(BadRequest(JsError.toJson(jse)))
    }
  }

}
