package org.binqua.forex.web.test.util

import cats.data
import org.binqua.forex.advisor.model._
import org.binqua.forex.advisor.portfolios.PortfoliosGen
import org.binqua.forex.advisor.portfolios.PortfoliosModel._
import org.binqua.forex.advisor.portfolios.service.IdempotentKey
import play.api.libs.json.{Writes, _}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID
import scala.util.{Failure, Success, Try}

object PortfoliosJsonTestContext extends PortfoliosJsonTestContext {}

trait PortfoliosJsonTestContext {

  trait jsonTag {
    val name: String
    val path: JsPath
  }

  object jsonTags {

    object toBeAdded extends jsonTag {
      val name = "toBeAdded"
      val path = JsPath \ name
    }

    val toBeDeleted = __ \ Symbol("toBeDeleted")
    val toBeUpdated = __ \ Symbol("toBeUpdated")

    object idempotentKey extends jsonTag {
      val name = "idempotentKey"
      val path = JsPath \ name
    }

    object position extends jsonTag {
      val name = "position"
      val path = JsPath \ name
    }

    object amount extends jsonTag {
      val name = "amount"
      val path = JsPath \ name
    }

    object openDateTime extends jsonTag {
      val name = "openDateTime"
      val path = JsPath \ name
    }

    object pair extends jsonTag {
      val name = "pair"
      val path = JsPath \ name
    }

    object positionType extends jsonTag {
      val name = "positionType"
      val path = JsPath \ name
    }

    object name extends jsonTag {
      val name = "name"
      val path = JsPath \ name
    }

    object price extends jsonTag {
      val name = "price"
      val path = JsPath \ name
    }

    object positionId extends jsonTag {
      val name = "positionId"
      val path = JsPath \ name
    }

    object positions extends jsonTag {
      val name = "positions"
      val path = JsPath \ name
    }

  }

  object fieldsError {
    val amount = "Amount field has to exist and has to be a positive integer"
    val nameHasToExist = "Name field has to exist"
    val openDateTime = "OpenDateTime field has to exist and has to have pattern 'dd mm hh at hh:mm:ss'"
    val pairs = "Pair field has to exist and has to be one of EUR/USD EUR/GBP EUR/JPY GBP/USD GBP/JPY SPX500 USD/JPY US30"
    val positions = "Positions field has to exist and has to have at least 1 position"
    val positionTypeMissing = "PositionType field has to exist and has to be buy or sell"
    val positionTypeExistButWrongValue = "PositionType field has to be buy or sell"
    val positionIdInvalid = "PositionId field has to be a valid UUID"
    val positionIdMissing = "PositionId field has to exist and has to be a valid UUID"
    val price = "Price field has to exist and has to be a double > than 0"
  }

  case class NewUpdatePortfolioJsonBuilder(idempotentKey: Option[String], name: String, updates: Seq[PortfolioSingleUpdate]) {

    import cats.syntax.option._

    def portfolioName: PortfolioName = PortfolioName.unsafe(name)

    def maybeAnIdempotentKey: Option[IdempotentKey] =
      idempotentKey match {
        case Some(someString) =>
          Try(UUID.fromString(someString)) match {
            case Success(value: UUID) => IdempotentKey(value).some
            case Failure(error)       => throw new IllegalStateException(error)
          }
        case None => None
      }
  }

  case class UpdatePortfolioJsonBuilder(name: String, updates: Seq[PortfolioSingleUpdate]) {
    def portfolioName: PortfolioName = PortfolioName.unsafe(name)
  }

  object CreatePortfolioJsonBuilder {
    def from(createPortfolio: CreatePortfolio): CreatePortfolioJsonBuilder =
      CreatePortfolioJsonBuilder(createPortfolio.idempotentKey.key.toString, createPortfolio.portfolioName.name.value, createPortfolio.positions.toSeq)
  }

  case class CreatePortfolioJsonBuilder(idempotentKey: String, name: String, positions: Seq[Position]) {
    def portfolioName: PortfolioName = PortfolioName.unsafe(name)
  }

  val aPositionId = UUID.randomUUID()

  val aValidaPortfolioName: PortfolioName = PortfoliosGen.portfolioName.sample.get

  object jsonwrites {

    def addAnArrayElement(aWrongElement: JsValue): Reads[JsArray] =
      play.api.libs.json.Reads.of[JsArray].map { case JsArray(arr) => JsArray(arr :+ aWrongElement) }

    val addAStringAsArrayElement: Reads[JsArray] = addAnArrayElement(JsString("1"))

    implicit val newPositionWrites: Writes[Position] = (position: Position) =>
      Json.obj(
        fields =
          "amount" -> LotSize.toMicro(position.lotSize).amount,
        "openDateTime" -> position.transactionDateTime.format(DateTimeFormatter.ofPattern("dd MM yy 'at' HH:mm:ss")),
        "pair" -> CurrencyPair.toExternalIdentifier(position.tradingPair.currencyPair),
        "positionType" -> (if (position.positionType.isABuyPosition) "buy" else "sell"),
        "price" -> position.tradingPair.price.value
      )

    implicit val addWrites: Writes[Add] = (add: Add) => newPositionWrites.writes(add.thePositionData)

    implicit val updateWrites: Writes[Update] = (update: Update) =>
      Json.obj(
        fields =
          "positionId" -> update.thePositionId,
        "position" -> update.thePositionData
      )

    implicit val portfolioNameWrites: Writes[PortfolioName] = (name: PortfolioName) =>
      Json.obj(fields =
        jsonTags.name.name -> name.name.value
      )

    implicit val deleteWrites: Writes[Delete] = (update: Delete) =>
      Json.obj(fields =
        "positionId" -> update.thePositionId
      )

    implicit val updatePortfolioWrites: Writes[NewUpdatePortfolioJsonBuilder] = (update: NewUpdatePortfolioJsonBuilder) =>
      Json.obj(
        fields =
          "idempotentKey" -> update.idempotentKey,
        "name" -> update.name,
        jsonTags.toBeAdded.name -> update.updates
          .filter({
            case _: Add => true
            case _      => false
          })
          .map(_.asInstanceOf[Add]),
        "toBeDeleted" -> update.updates
          .filter({
            case _: Delete => true
            case _         => false
          })
          .map(_.asInstanceOf[Delete]),
        "toBeUpdated" -> update.updates
          .filter({
            case _: Update => true
            case _         => false
          })
          .map(_.asInstanceOf[Update])
      )

    implicit val createPortfolioWrites: Writes[CreatePortfolioJsonBuilder] = (createPortfolioJsonBuilder: CreatePortfolioJsonBuilder) =>
      Json.obj(
        "idempotentKey" -> JsString(createPortfolioJsonBuilder.idempotentKey),
        "name" -> JsString(createPortfolioJsonBuilder.name),
        "positions" -> createPortfolioJsonBuilder.positions
      )

    implicit val deletePortfoliosWrites: Writes[data.NonEmptySet[PortfolioName]] = (names: data.NonEmptySet[PortfolioName]) =>
      Json.toJson(names.toSortedSet.toSeq)(Writes.seq)

  }

  import play.api.libs.json._

  val recordedDateTime = LocalDateTime.of(2020, 3, 12, 20, 0, 1)

  val aValidPositionJson: JsObject = Json.toJson(PortfoliosGen.position.sample.get)(jsonwrites.newPositionWrites).asInstanceOf[JsObject]

  def replaceField(json: JsObject, tuple: (String, Json.JsValueWrapper)): JsObject = json ++ Json.obj(tuple)

  def transformWith(json: JsValue, transformer: Reads[JsObject]): JsObject = json.transform(transformer).get.as[JsObject]

  val validPortfolioName: PortfolioName = PortfoliosGen.portfolioName.sample.get

  def toListOfSimpleErrors(errors: collection.Seq[(JsPath, collection.Seq[JsonValidationError])]): Iterable[String] = toMapOfSingleErrors(errors).values

  def toMapOfSingleErrors(errors: collection.Seq[(JsPath, collection.Seq[JsonValidationError])]): Map[JsPath, String] =
    errors.map(keyValue => keyValue._1 -> keyValue._2.head.message).toMap

  def toMapOfSeqOfErrors(errors: collection.Seq[(JsPath, collection.Seq[JsonValidationError])]): Map[JsPath, collection.Seq[String]] =
    errors.map(keyValue => keyValue._1 -> keyValue._2.map(_.message)).toMap

}
