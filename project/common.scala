import sbt.{TestFrameworks, Tests}

import java.io.File

object common {

  def scalaTestArgs(parent: File): Tests.Argument = Tests.Argument(TestFrameworks.ScalaTest, "-h", toHtmlTestsDir(parent))

  def toHtmlTestsDir(parent: File): String = parent + "/target/htmldir-tests-report"
}
