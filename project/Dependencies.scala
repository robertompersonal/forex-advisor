import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{
  dockerBaseImage => _,
  dockerBuildOptions => _,
  dockerCommands => _,
  dockerExposedVolumes => _,
  dockerRepository => _
}
import sbt._

object Dependencies {

  object version {
    val akka = "2.6.15"
    val akkaManagement = "1.0.8"
    val cats = "2.3.0"
    val akkaCassandra = "1.0.5"
    val akkaHttp = "10.2.4"
    val monocle = "2.0.2"
    val refined = "0.9.19"
  }

  object akka {

    object management {
      val core: ModuleID = "com.lightbend.akka.management" %% "akka-management" % version.akkaManagement
      val clusterHttp: ModuleID = "com.lightbend.akka.management" %% "akka-management-cluster-http" % version.akkaManagement
    }

    object persistence {
      val cassandra: ModuleID = "com.typesafe.akka" %% "akka-persistence-cassandra" % version.akkaCassandra
      val cassandraLauncher: ModuleID = "com.typesafe.akka" %% "akka-persistence-cassandra-launcher" % version.akkaCassandra
      val core: ModuleID = "com.typesafe.akka" %% "akka-persistence" % version.akka
      val query: ModuleID = "com.typesafe.akka" %% "akka-persistence-query" % version.akka
      val typed: ModuleID = "com.typesafe.akka" %% "akka-persistence-typed" % version.akka
    }

    object typed {
      val actor: ModuleID = "com.typesafe.akka" %% "akka-actor-typed" % version.akka
      val cluster: ModuleID = "com.typesafe.akka" %% "akka-cluster-typed" % version.akka
      val clusterSharding: ModuleID = "com.typesafe.akka" %% "akka-cluster-sharding-typed" % version.akka
      val testKit: ModuleID = "com.typesafe.akka" %% "akka-actor-testkit-typed" % version.akka
    }

    val actor: ModuleID = "com.typesafe.akka" %% "akka-actor" % version.akka
    val clusterTools: ModuleID = "com.typesafe.akka" %% "akka-cluster-tools" % version.akka
    val http: ModuleID = "com.typesafe.akka" %% "akka-http" % version.akkaHttp
    val httpSpray: ModuleID = "com.typesafe.akka" %% "akka-http-spray-json" % version.akkaHttp
    val serializationJackson: ModuleID = "com.typesafe.akka" %% "akka-serialization-jackson" % version.akka
    val slf4j: ModuleID = "com.typesafe.akka" %% "akka-slf4j" % version.akka
    val testKit: ModuleID = "com.typesafe.akka" %% "akka-testkit" % version.akka
  }

  object cats {
    private val prefix: String = "org.typelevel"
    val core: ModuleID = prefix %% "cats-core" % version.cats
    val kittens: ModuleID = prefix %% "kittens" % version.cats
  }

  object commons {
    val cli: ModuleID = "commons-cli" % "commons-cli" % "1.4"
    val io: ModuleID = "commons-io" % "commons-io" % "2.6"
  }

  object datastax {
    val javaDriver: ModuleID = "com.datastax.oss" % "java-driver-core" % "4.6.1"
  }

  object monocle {
    val core: ModuleID = "com.github.julien-truffaut" %% "monocle-core" % version.monocle
    val law: ModuleID = "com.github.julien-truffaut" %% "monocle-law" % version.monocle
    val macros: ModuleID = "com.github.julien-truffaut" %% "monocle-macro" % version.monocle
  }

  object io {
    val socket: ModuleID = "io.socket" % "socket.io-client" % "1.0.0"
    val engine: ModuleID = "io.socket" % "engine.io-client" % "1.0.0"
  }

  object refined {
    val core: ModuleID = "eu.timepit" %% "refined" % version.refined
    val scalacheck: ModuleID = "eu.timepit" %% "refined-scalacheck" % version.refined
  }

  object scalaTestPlus {
    private val prefix: String = "org.scalatestplus"
    val scalaCheck: ModuleID = prefix %% "scalatestplus-scalacheck" % "3.1.0.0-RC2"
    val mockito: ModuleID = prefix %% "mockito-3-2" % "3.1.1.0"
    val play: ModuleID = "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0"

  }

  object play {
    val json: ModuleID = "com.typesafe.play" %% "play-json" % "2.8.1"
    val logback: ModuleID = "com.typesafe.play" %% "play-logback" % "2.8.2"
  }

  val logBack: ModuleID = "ch.qos.logback" % "logback-classic" % "1.2.3"
  val geometryApi: ModuleID = "com.esri.geometry" % "esri-geometry-api" % "1.2.1"
  val flexmark: ModuleID = "com.vladsch.flexmark" % "flexmark-all" % "0.36.8"
  val leveldb: ModuleID = "org.iq80.leveldb" % "leveldb" % "0.7"
  val leveldbjni: ModuleID = "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
  val scalaCheck: ModuleID = "org.scalacheck" %% "scalacheck" % "1.14.3"
  val scalactic: ModuleID = "org.scalactic" %% "scalactic" % "3.1.1"
  val scalaMock: ModuleID = "org.scalamock" % "scalamock_2.13" % "4.4.0"
  val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % "3.1.1"
  val shapeless: ModuleID = "com.chuusai" %% "shapeless" % "2.3.3"

  object bundles {

    val baseAkkaDependencies: Seq[ModuleID] = Seq(
      Dependencies.akka.actor,
      Dependencies.akka.clusterTools,
      Dependencies.akka.httpSpray,
      Dependencies.akka.http,
      Dependencies.akka.management.core,
      Dependencies.akka.management.clusterHttp,
      Dependencies.akka.persistence.cassandra,
      Dependencies.akka.persistence.core,
      Dependencies.akka.persistence.query,
      Dependencies.akka.persistence.typed,
      Dependencies.akka.serializationJackson,
      Dependencies.akka.slf4j,
      Dependencies.akka.typed.actor,
      Dependencies.akka.typed.cluster,
      Dependencies.akka.typed.clusterSharding,
      Dependencies.logBack,
      Dependencies.akka.persistence.cassandraLauncher % Test,
      Dependencies.akka.testKit % Test,
      Dependencies.akka.typed.testKit % Test
    )

    val commonDependencies: Seq[ModuleID] = Seq(
      Dependencies.cats.core,
      Dependencies.cats.kittens,
      Dependencies.commons.cli,
      Dependencies.commons.io,
      Dependencies.datastax.javaDriver,
      Dependencies.monocle.core,
      Dependencies.monocle.macros,
      Dependencies.play.logback,
      Dependencies.play.json,
      Dependencies.refined.core,
      Dependencies.refined.scalacheck,
      Dependencies.shapeless,
      Dependencies.scalactic,
      Dependencies.flexmark % Test,
      Dependencies.leveldb % Test,
      Dependencies.leveldbjni % Test,
      Dependencies.monocle.law % Test,
      Dependencies.scalaCheck % Test,
      Dependencies.scalaMock % Test,
      Dependencies.scalaTest % Test,
      Dependencies.scalaTestPlus.scalaCheck % Test
    ) ++ baseAkkaDependencies

  }
}
