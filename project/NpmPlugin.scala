import sbt.{File, taskKey, _}

import scala.sys.process.Process

object Npm {

  /*
   * UI Build hook Scripts
   */

  // Execution status success.
  val Success = 0

  // Execution status failure.
  val Error = 1

  // True if build running operating system is windows.
  val isWindows = System.getProperty("os.name").toLowerCase().contains("win")

  // Execute on commandline, depending on the operating system. Used to execute npm commands.
  def runOnCommandline(script: String)(implicit dir: File): Int = {
    if (isWindows) {
      Process("cmd /c set CI=true&&" + script, dir)
    } else {
      Process("env CI=false " + script, dir)
    }
  } !

  // Check of node_modules directory exist in given directory.
  def isNodeModulesInstalled(implicit dir: File): Boolean = (dir / "node_modules").exists()

  // Execute `npm install` command to install all node module dependencies. Return Success if already installed.
  def runNpmInstall(implicit dir: File): Int =
    if (isNodeModulesInstalled) Success else runOnCommandline(FrontendCommands.dependencyInstall)

  // Execute task if node modules are installed, else return Error status.
  def ifNodeModulesInstalled(task: => Int)(implicit dir: File): Int =
    if (runNpmInstall == Success) task
    else Error

  // Execute frontend test task. Update to change the frontend test task.
  def executeUiTests(implicit dir: File): Int = ifNodeModulesInstalled(runOnCommandline(FrontendCommands.test))

  // Execute frontend prod build task. Update to change the frontend prod build task.
  def executeProdBuild(implicit dir: File): Int = ifNodeModulesInstalled(runOnCommandline(FrontendCommands.build))

  // Create frontend build tasks for prod, dev and test execution.

  lazy val `ui-test` = taskKey[Unit]("Run UI tests when testing application.")

  lazy val `ui-prod-build` = taskKey[Unit]("Run UI build when packaging the application.")

}
