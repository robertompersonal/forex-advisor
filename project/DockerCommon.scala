import com.typesafe.sbt.packager.Keys._
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{
  dockerBaseImage => _,
  dockerBuildOptions => _,
  dockerCommands => _,
  dockerExposedVolumes => _,
  dockerRepository => _,
  _
}
import com.typesafe.sbt.packager.docker.{Cmd, DockerChmodType, ExecCmd}
import sbt.Keys.baseDirectory
import sbt._

object DockerCommon {

  lazy val akkaDir = settingKey[String]("Location where application files are stored inside docker container")
  lazy val userRunningTheApp = settingKey[String]("Location where application files are stored inside docker container")

  lazy val actorSystemRunningSettings: sbt.Def.SettingsDefinition = Seq(
    akkaDir := s"${(Docker / defaultLinuxInstallLocation).value}/akka",
    dockerChmodType := DockerChmodType.UserGroupWriteExecute,
    dockerCommands ++= Seq(
      Cmd("USER", "root"),
      ExecCmd("RUN", "mkdir", "-p", akkaDir.value),
      ExecCmd("RUN", "chown", "-R", s"${userRunningTheApp.value}:root", akkaDir.value),
      Cmd("USER", s"${userRunningTheApp.value}"),
      ExecCmd("RUN", "mkdir", "-p", s"${akkaDir.value}/clustering/ddata"),
      ExecCmd(
        "CMD",
        "-configFileName",
        s"${akkaDir.value}/app.conf",
        s"-Dlogback.configurationFile=${akkaDir.value}/logback.xml"
      )
    ),
    dockerExposedVolumes := Seq(s"${akkaDir.value}/logs"),
    userRunningTheApp := s"${(Docker / daemonUser).value}",
    Docker / dockerBuildOptions := theDockerBuildOptions((Docker / dockerBuildOptions).value),
    Docker / daemonUser := (ThisBuild / daemonUser).value,
    Docker / packageName := toDockerPackageName(baseDirectory.value)
  )

  def toDockerPackageName(projectDir: File): String = "forex-advisor-" + projectDir.getName

  def theDockerBuildOptions(options: Seq[String]): Seq[String] = options ++ Seq("--no-cache")
}
