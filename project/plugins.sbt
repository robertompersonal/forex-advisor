addSbtPlugin(dependency = "com.typesafe.sbt" % "sbt-native-packager" % "1.8.1")
addSbtPlugin(dependency = "com.typesafe.play" % "sbt-plugin" % "2.8.5")
addSbtPlugin(dependency = "net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")
